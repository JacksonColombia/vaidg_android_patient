package adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.PixelFormat;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.InCallOutCallTeleCallActivity;
import com.vaidg.incalloutcalltelecall.InCallOutCallTeleCallContract;
import com.vaidg.incalloutcalltelecall.symotomquestion.SymptomQuestionsContract;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.RoundedCornersTransformation;
import com.pojo.QuestionImage;
import com.utility.PicassoTrustAll;

import java.util.ArrayList;

/**
 * Created by Ali on 7/24/2018.
 */
public class ImageTypeAdapterGrid extends RecyclerView.Adapter {

  private Context mContext;
  private ArrayList<QuestionImage> questionImages = new ArrayList<>();
  //private OnItemonToTakeImageClickListener onItemonToTakeImageClickListener;
  private boolean isImageInfo = false;

  private Activity activity;
  private InCallOutCallTeleCallContract.InCallOutCallTeleCallView questionSelectionView;

  /*public ImageTypeAdapterGrid(Context mContext, ArrayList<QuestionImage> questionImages,
  SymptomQuestionsContract.SymptomQuestionsView symptomQuestionsView) {
      this.activity = (Activity) mContext;
      this.mContext = mContext;
      this.questionImages =questionImages;
      this.symptomQuestionsView = symptomQuestionsView;
  }*/
  public ImageTypeAdapterGrid(ArrayList<QuestionImage> questionImages, InCallOutCallTeleCallContract.InCallOutCallTeleCallView questionSelectionView) {
    this.questionImages = questionImages;
    this.questionSelectionView = questionSelectionView;
  }

  /**
   * Creates a simple dialog box with as many buttons as you want
   *
   * @param context     The context of the dialog
   * @param cancelable  whether the dialog can be closed/cancelled by the user
   * @param layoutResID the resource id of the layout you want within the dialog
   * @return the dialog
   */
  public static Dialog getBaseDialog(Context context, boolean cancelable, int layoutResID) {
    Dialog dialog = new Dialog(context);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setCancelable(cancelable);
    dialog.setCanceledOnTouchOutside(cancelable);
    dialog.setContentView(layoutResID);

    return dialog;
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View itemView;
    activity = (Activity) parent.getContext();
    mContext = parent.getContext();
    if (viewType == 0) {
      itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_taken_display, parent, false);
      return new ImageViewHolder(itemView);
    } else {
      itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_to_take, parent, false);
      return new ImageViewTakesHold(itemView);
    }

  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    if (questionImages.size() > 0) {
      if (questionImages.get(position).isImage()) {

        Log.d("TAG", "onBindViewHolder: " + questionImages.get(position).getImage());
        ImageViewHolder imageHold = (ImageViewHolder) holder;

        imageHold.imageViewTaken.setVisibility(View.VISIBLE);
        String url = questionImages.get(position).getImage();

        if (isImageInfo) {

          playallimage(questionImages, imageHold.imageViewTaken, position);
        } else {
          playallimage(questionImages, imageHold.imageViewTaken, position);

        }

        if (!isImageInfo) {
          imageHold.ivDelete.setVisibility(View.VISIBLE);
        }

      } else {
        if (questionImages.size() > 1) {
          // ImageViewTakesHold imageHold = (ImageViewTakesHold) holder;
        }
      }

    }
  }

  private void playallimage(ArrayList<QuestionImage> questionImages,
      ImageView imageViewTaken, int position) {

    if (!"".equals(questionImages.get(position).getImage())) {
      Glide.with(mContext)
          .load(questionImages.get(position).getImage())
          .apply(new RequestOptions().transforms(new CenterCrop(), new RoundedCorners(10)))
          .into(imageViewTaken);

    }


    imageViewTaken.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        // videoView.setBackground(null);
        showAlert(ImageTypeAdapterGrid.this.questionImages.get(position).getImage());

      }
    });

  }

  private void showAlert(String image) {
    getImageDialog(mContext, image);
  }

  public void getImageDialog(Context context, String videoLocation) {
    final Dialog dialog = getBaseDialog(context, true, R.layout.dialog_image);

    ((Activity) context).getWindow().setFormat(PixelFormat.TRANSLUCENT);
    final ImageView videoHolder = dialog.findViewById(R.id.videoDialog);
    ImageView image = dialog.findViewById(R.id.imageDialog);
    if (!"".equals(videoLocation)) {
      Glide.with(mContext)
          .load(videoLocation)
          .apply(new RequestOptions().transforms(new CenterCrop(), new RoundedCorners(10)))
          .into(videoHolder);
    }

    //videoHolder.setRotation(90);

    dialog.show();


    // if decline button is clicked, close the custom dialog
    image.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // Close dialog
        dialog.dismiss();
        notifyDataSetChanged();
      }
    });
    // return dialog;
  }

  @Override
  public int getItemCount() {

    return questionImages == null ? 0 : questionImages.size();
  }

  @Override
  public int getItemViewType(int position) {
        /*if(questionImages.get(position).isImage())
            return  0;
        else
            return 0;*/
    return 0;
  }

  public void onIsQuestionInfo(boolean isInfo) {
    isImageInfo = isInfo;
  }

  private class ImageViewHolder extends RecyclerView.ViewHolder {
    private ImageView imageViewTaken, ivDelete;

    ImageViewHolder(View itemView) {
      super(itemView);
      ivDelete = itemView.findViewById(R.id.ivDelete);
      imageViewTaken = itemView.findViewById(R.id.imageViewTaken);

      ivDelete.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          Log.i("ABCDEFG", "onClick: Delete");
          questionSelectionView.deleteImagePhoto(getAdapterPosition(),questionImages.get(getAdapterPosition()).getImagePostion());

          //questionSelectionView.deletePhoto(getAdapterPosition(),questionImages.get
            // (getAdapterPosition()).getImagePostion());
        }
      });
    }
  }

  private class ImageViewTakesHold extends RecyclerView.ViewHolder {
    private ImageView imageToTake;

    ImageViewTakesHold(View itemView) {
      super(itemView);
      imageToTake = itemView.findViewById(R.id.imageToTake);

      itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          // Log.i("ABCDEFG", "onClick: onToTakeImage");

          //symptomQuestionsView.onToTakeImage(getAdapterPosition());
        }
      });

    }
        /*public interface OnItemonToTakeImageClickListener {
            void onItemClick(View view, int layoutPosition, ArrayList<SymptomResponsePojo
            .SymptomCategory.QuestionArr.PreDefined> preDefineds, int pos, int position);

            void onItemLongClick(View view, int position);
        }

        // for both short and long click
        public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
            this.mItemClickListener = mItemClickListener;
        }*/
  }
}
