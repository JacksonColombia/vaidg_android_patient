package adapters;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.vaidg.R;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.ReadMoreSpannable;
import com.vaidg.utilities.SessionManager;
import com.vaidg.utilities.Utility;

import java.util.ArrayList;

import com.pojo.ProviderDetailsResponse;
import com.utility.PicassoTrustAll;

/**
 * <h2>RatingReviewAdapter</h2>
 * Created by Ali on 9/19/2017.
 */

public class RatingReviewAdapter extends RecyclerView.Adapter {
    private Context mcontext;
    private ArrayList<ProviderDetailsResponse.ProviderResponseDetails.ReviewList> mReviwList;
    private String readMore, readLess;
    private boolean isProfile;

    public RatingReviewAdapter( ArrayList<ProviderDetailsResponse.ProviderResponseDetails.ReviewList> rateReviewses, boolean isProfile) {
        mReviwList = rateReviewses;
        this.isProfile = isProfile;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.signlecellreviwadptr, parent, false);
        mcontext = parent.getContext();
        readMore = mcontext.getResources().getString(R.string.readMore);
        readLess = mcontext.getResources().getString(R.string.readLess);
        return new ViewHoldr(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ViewHoldr hldr = (ViewHoldr) holder;
        Log.e("REWIESBY", "onBindViewHolder: "+mReviwList.get(position).getReviewBy());
        hldr.tvReviwName.setTextSize(TypedValue.COMPLEX_UNIT_PX,mcontext.getResources().getDimensionPixelSize(R.dimen.sp_16));
        hldr.tvReview.setTextSize(TypedValue.COMPLEX_UNIT_PX,mcontext.getResources().getDimensionPixelSize(R.dimen.sp_16));
        hldr.tvReviewTime.setTextSize(TypedValue.COMPLEX_UNIT_PX,mcontext.getResources().getDimensionPixelSize(R.dimen.sp_12));
        hldr.tvReviwName.setText(mReviwList.get(position).getReviewBy());
        hldr.tvReview.setText(mReviwList.get(position).getReview());
       hldr.rtReviewadptr.setRating(mReviwList.get(position).getRating());
        hldr.rtReviewadptr.setIsIndicator(true);
        new ReadMoreSpannable(readMore, readLess);
        if (hldr.tvReview.getText().toString().length() > 100) {
            ReadMoreSpannable.makeTextViewResizable(hldr.tvReview, 3, readMore, true);
        }
        String url = mReviwList.get(position).getProfilePic().replace(" ", "%20");
        int[] dateValue = Utility.calculateTimeDifference(mReviwList.get(position).getReviewAt(), Constants.serverTime);
        String dateTimeValue = "";
        if (dateValue[0] >= 24) {
            int days = dateValue[0] / 24;
            if(days>1)
                dateTimeValue = days + " " + hldr.resources.getString(R.string.daysAgo);
            else
                dateTimeValue = "Yesterday";
        } else if (dateValue[0] > 0)
            dateTimeValue = dateValue[0] + " " + hldr.resources.getString(R.string.hoursAgo);
        else if (dateValue[1] > 0)
            dateTimeValue = dateValue[1] + " " + hldr.resources.getString(R.string.minutesAgo);
        else
            dateTimeValue = dateValue[2] + " " + hldr.resources.getString(R.string.secondsAgo);
        hldr.tvReviewTime.setText(dateTimeValue);
        if (isProfile) {
            if (url != null && !url.isEmpty()) {
               /* double size[] = Scaler.getScalingFactor(mcontext);
                double height = (40) * size[1];
                double width = (40) * size[0];
                Picasso.with(mcontext).load(url)
                        .resize((int) width, (int) height)
                        .transform(new PicassoCircleTransform())
                        .into(hldr.ivReviewImage);*/

                PicassoTrustAll.getInstance(mcontext)
                        .load(url)
                        .placeholder(R.drawable.register_profile_default_image)   // optional
                        .error(R.drawable.register_profile_default_image)// optional
                        .transform(new PicassoCircleTransform())
                        .into(hldr.ivReviewImage);


            }
        } else {
            if (url !=null &&!url.isEmpty()) {
              /*  double size[] = Scaler.getScalingFactor(mcontext);
                double height = (50) * size[1];
                double width = (50) * size[0];
                Picasso.with(mcontext).load(url)
                        .resize((int) width, (int) height)
                        .transform(new PicassoCircleTransform())
                        .into(hldr.ivReview);*/

                PicassoTrustAll.getInstance(mcontext)
                        .load(url)
                        .placeholder(R.drawable.register_profile_default_image)   // optional
                        .error(R.drawable.register_profile_default_image)// optional
                        .transform(new PicassoCircleTransform())
                        .into(hldr.ivReview);

            }
        }

        if(position < mReviwList.size()-1)
            hldr.viewReview.setVisibility(View.VISIBLE);

    }

    @Override
    public int getItemCount() {
        return mReviwList == null ? 0 : mReviwList.size();
    }

    private class ViewHoldr extends RecyclerView.ViewHolder {
        private TextView tvReviwName, tvReview, tvReviewTime;
        private ImageView ivReview, ivReviewImage;
        private RatingBar rtReviewadptr;
        private Resources resources;
        private View viewReview;
        private SessionManager manager;

        ViewHoldr(View itemView) {
            super(itemView);
            resources = mcontext.getResources();
            AppTypeface appTypeface = AppTypeface.getInstance(mcontext);
            rtReviewadptr = itemView.findViewById(R.id.rtReviewadptr);
            viewReview = itemView.findViewById(R.id.viewReview);
            manager = SessionManager.getInstance(mcontext);

            ivReview = itemView.findViewById(R.id.ivReview);
            if (isProfile) {
                ivReview.setVisibility(View.INVISIBLE);
            }
            ivReviewImage = itemView.findViewById(R.id.ivReviewImage);
            tvReviwName = itemView.findViewById(R.id.tvReviwName);
            tvReview = itemView.findViewById(R.id.tvReview);
            tvReviewTime = itemView.findViewById(R.id.tvReviewTime);
            rtReviewadptr.setIsIndicator(true);
            tvReviwName.setTypeface(appTypeface.getHind_regular());
            tvReview.setTypeface(appTypeface.getHind_regular());
            tvReviewTime.setTypeface(appTypeface.getHind_regular());

        }
    }
}
