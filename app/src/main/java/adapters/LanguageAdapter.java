package adapters;

import android.content.Context;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vaidg.R;
import com.vaidg.utilities.AppTypeface;
import com.pojo.ProviderDetailsResponse;
import java.util.ArrayList;

/**
 * @author Pramod
 * @since 19-01-2018.
 */

public class LanguageAdapter extends RecyclerView.Adapter {
  private Context mContext;
  private AppTypeface mAppTypeface;
  private ArrayList<ProviderDetailsResponse.PredefinedArray> mPredefinedArrays;
  private String mStringArrayList;

  public LanguageAdapter(AppTypeface appTypeface,
      String langData,
      ArrayList<ProviderDetailsResponse.PredefinedArray> preDefined) {
    this.mAppTypeface = appTypeface;
    this.mStringArrayList =langData;
    this.mPredefinedArrays =preDefined;

  }


  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_cell_with_bullet, parent,
        false);
    mContext = parent.getContext();
    return new ViewHolderRecycler(itemView);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    ViewHolderRecycler hold = (ViewHolderRecycler) holder;

      String[] items = mStringArrayList.split(",");
      for (String item : items) {
        System.out.println("item = " + item);
        if(mPredefinedArrays.get(position).getId().equals(item))
        {
          Log.w("Predefined", "onBindViewHolder: "+item + "  "+mPredefinedArrays.get(position).getName());
          hold.llDesc.setVisibility(View.VISIBLE);
          hold.tvDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
          hold.tvDesc.setText(mPredefinedArrays.get(position).getName());
        }
    }

    /*
        mPredefinedArrays.get(position).getId().equals(mStringArrayList.get(position));
        if (mStringArrayList.equals(predefinedArray.getId())) {
          hold.llDesc.setVisibility(View.VISIBLE);
          hold.tvDesc.setText(predefinedArray.getName());
        }*/

  }

  @Override
  public int getItemCount() {
    return mPredefinedArrays == null ? 0 : mPredefinedArrays.size();
  }


  /**
   * <h2>ViewHolder</h2>
   * This method is used to hold the views
   */
  public class ViewHolderRecycler extends RecyclerView.ViewHolder {

    @BindView(R.id.tvDesc)
    TextView tvDesc;
    @BindView(R.id.llDesc)
    LinearLayout llDesc;
    public ViewHolderRecycler(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      tvDesc.setTypeface(mAppTypeface.getHind_medium());
    }
  }

}
