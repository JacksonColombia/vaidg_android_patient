package adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vaidg.R;
import com.vaidg.providerList.DoctorListContract;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;

import com.vaidg.filter.model.FilterData;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * <h2>FilterAdapter</h2>
 * Created by Ali on 2/1/2018.
 */

public class FilterAdapter extends RecyclerView.Adapter
{
    private Context mContext;
    private ArrayList<FilterData>filteredResponces;
    private DoctorListContract.doctorView providerView;

    public FilterAdapter(ArrayList<FilterData> filteredResponces, DoctorListContract.doctorView providerView) {
        this.filteredResponces = filteredResponces;
        this.providerView = providerView;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filter_adapter,parent,false);
        mContext = parent.getContext();
        return new ViewHolderClass(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolderClass viewHolderClass = (ViewHolderClass) holder;

    }

    @Override
    public int getItemCount()
    {
        return filteredResponces == null ? 0 : filteredResponces.size();
    }

    class ViewHolderClass extends RecyclerView.ViewHolder
    {
        @BindView(R.id.tvFilterSelected)TextView tvFilterSelected;
       private AppTypeface appTypeface;
        ViewHolderClass(View itemView)
        {
            super(itemView);
            appTypeface = AppTypeface.getInstance(mContext);
            ButterKnife.bind(this,itemView);
            tvFilterSelected.setTypeface(appTypeface.getHind_regular());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   // notifyDataSetChanged();
                    providerView.onFilterRemoveArray(filteredResponces);

                }
            });
        }
    }
}
