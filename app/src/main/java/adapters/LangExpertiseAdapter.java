package adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import eu.janmuller.android.simplecropimage.Util;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.vaidg.R;
import com.vaidg.providerdetails.ProviderDetailsContract;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Utility;
import com.pojo.ProviderDetailsResponse;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * <h2>LangExpertiseAdapter</h2>
 * Created by Ali on 2/6/2018.
 */

public class LangExpertiseAdapter extends RecyclerView.Adapter {
  private static final int ONE = 1;
  private static final int TWO = 2;
  private static final int THREE = 3;
  private static final int FOUR = 4;
  private static final int FIVE = 5;
  private static final int SIX = 6;
  private static final int SEVEN = 7;
  private static final int EIGHT = 8;
  private static final int NINE = 9;
  private static final int TEN = 10;
  private static final int ELEVEN = 11;
  private static final int TWELVE = 12;
  private static final int THIRTEEN = 13;
  private Context mContext;
  private ArrayList<ProviderDetailsResponse.MetaDataArray> mMetaDataArrays = new ArrayList<>();
  private String TAG = LangExpertiseAdapter.class.getSimpleName();
  private ProviderDetailsContract.ProviderPresenter mProviderPresenter;
  private AppTypeface mAppTypeface;

  public LangExpertiseAdapter(
      AppTypeface appTypeface,
      ArrayList<ProviderDetailsResponse.MetaDataArray> metaDataArrays,
      ProviderDetailsContract.ProviderPresenter presenter) {
    this.mAppTypeface = appTypeface;
    this.mMetaDataArrays = metaDataArrays;
    this.mProviderPresenter = presenter;
  }

  @Override
  public int getItemViewType(int position) {

    if (mMetaDataArrays.get(position).getType() == ONE) {
      return ONE;
    } else if (mMetaDataArrays.get(position).getType() == TWO) {
      return TWO;
    } else if (mMetaDataArrays.get(position).getType() == THREE) {
      return THREE;
    } else if (mMetaDataArrays.get(position).getType() == FOUR) {
      return FOUR;
    } else if (mMetaDataArrays.get(position).getType() == FIVE) {
      return FIVE;
    } else if (mMetaDataArrays.get(position).getType() == SIX) {
      return SIX;
    } else if (mMetaDataArrays.get(position).getType() == SEVEN) {
      return SEVEN;
    } else if (mMetaDataArrays.get(position).getType() == EIGHT) {
      return EIGHT;
    } else if (mMetaDataArrays.get(position).getType() == NINE) {
      return NINE;
    } else if (mMetaDataArrays.get(position).getType() == TEN) {
      return TEN;
    } else if (mMetaDataArrays.get(position).getType() == ELEVEN) {
      return ELEVEN;
    } else if (mMetaDataArrays.get(position).getType() == TWELVE) {
      return TWELVE;
    } else {
      return THIRTEEN;
    }
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    // View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_cell_with_textview,
    // parent,false);
    View view;
    mContext = parent.getContext();
    if (viewType == SEVEN || viewType == EIGHT) {
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_cell_with_textview,
          parent,
          false);
      return new TextTypeViewHolder(view);
    } else if (viewType == FOUR || viewType == FIVE || viewType == SIX
        || viewType == NINE || viewType == TEN || viewType == ELEVEN) {
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_cell_with_imageview,
          parent,
          false);
      return new ImageTypeViewHolder(view);
    } else if (viewType == ONE || viewType == TWO || viewType == THREE || viewType == TWELVE || viewType == THIRTEEN) {
      view = LayoutInflater.from(parent.getContext()).inflate(
          R.layout.single_cell_withrecyclerview_bullet,
          parent,
          false);
      return new BulletTextTypeViewHolder(view);
    } else {
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_cell_with_demo,
          parent, false);
      return new NullViewHolder(view);
    }
    // return new ViewHolders(view);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
   /* ArrayList<String> elephantList = new ArrayList<>();
    if (mMetaDataArrays.get(position).getData().contains(",")) {
      elephantList = new ArrayList<>(Arrays.asList(mMetaDataArrays.get(
          position).getData().split(",")));

      for(int i = 0; i <elephantList.size();i++) {
        data = elephantList.get(i);
        Log.w(TAG, "onBindViewHolder: "+elephantList.get(i) );
      }

    } else {
      data = mMetaDataArrays.get(position).getData();
    }*/
    if (getItemViewType(position) == SEVEN || getItemViewType(position) == EIGHT) {
      if (!mMetaDataArrays.get(position).getName().isEmpty()) {
        ((TextTypeViewHolder) holder).llName.setVisibility(View.VISIBLE);
        ((TextTypeViewHolder) holder).tvName.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
        ((TextTypeViewHolder) holder).tvName.setText(mMetaDataArrays.get(position).getName());
      }
      if (!mMetaDataArrays.get(position).getData().isEmpty()) {
        ((TextTypeViewHolder) holder).llDesc.setVisibility(View.VISIBLE);
        ((TextTypeViewHolder) holder).tvDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
        ((TextTypeViewHolder) holder).tvDesc.setText(
            mMetaDataArrays.get(position).getData());
        mProviderPresenter.moreReadable(((TextTypeViewHolder) holder).tvDesc);
      }

      if (mMetaDataArrays.get(position).getIcon() != null && !mMetaDataArrays.get(position).getIcon().isEmpty()) {

        Utility.printLog("TAGADD", "  " + mMetaDataArrays.get(position).getIcon());
        Glide.with(mContext)
            .load(mMetaDataArrays.get(position).getIcon())
            //.apply(Utility.createGlideOptionMetadData(mContext))
            .into(new SimpleTarget<Drawable>() {
              @Override
              public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                ((TextTypeViewHolder) holder).imageIconMeta.setImageDrawable(resource);
              }
            });

      }

    } else if (getItemViewType(position) == FOUR || getItemViewType(position) == FIVE
        || getItemViewType(position) == SIX || getItemViewType(position) == NINE
        || getItemViewType(position) == TEN || getItemViewType(
        position) == ELEVEN) {


      if (!mMetaDataArrays.get(position).getName().isEmpty()) {
        ((ImageTypeViewHolder) holder).llName.setVisibility(View.VISIBLE);
        ((ImageTypeViewHolder) holder).tvName.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
        ((ImageTypeViewHolder) holder).tvName.setText(
            mMetaDataArrays.get(position).getName());
      }
      if (!mMetaDataArrays.get(position).getData().isEmpty()) {
        ((ImageTypeViewHolder) holder).llDesc.setVisibility(View.VISIBLE);
        ((ImageTypeViewHolder) holder).tvDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
        ((ImageTypeViewHolder) holder).tvDesc.setText(
            mMetaDataArrays.get(position).getData());
      }

      if (mMetaDataArrays.get(position).getIcon() != null && !mMetaDataArrays.get(position).getIcon().isEmpty()) {

        Utility.printLog("TAGADD", "  " + mMetaDataArrays.get(position).getIcon());
        Glide.with(mContext)
            .load(mMetaDataArrays.get(position).getIcon())
            //.apply(Utility.createGlideOptionMetadData(mContext))
            .into(new SimpleTarget<Drawable>() {
              @Override
              public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                ((ImageTypeViewHolder) holder).imageIconMeta.setImageDrawable(resource);
              }
            });

      }

    } else if (getItemViewType(position) == ONE || getItemViewType(position) == TWO
        || getItemViewType(position) == THREE || getItemViewType(position) == TWELVE
        || getItemViewType(position) == THIRTEEN) {
      if (!mMetaDataArrays.get(position).getName().isEmpty()) {
        ((BulletTextTypeViewHolder) holder).llName.setVisibility(View.VISIBLE);
        ((BulletTextTypeViewHolder) holder).tvName.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
        ((BulletTextTypeViewHolder) holder).tvName.setText(
            mMetaDataArrays.get(position).getName());
      }
      if (mMetaDataArrays.get(position).getIcon() != null && !mMetaDataArrays.get(position).getIcon().isEmpty()) {

        Utility.printLog("TAGADD", "  " + mMetaDataArrays.get(position).getIcon());
        Glide.with(mContext)
            .load(mMetaDataArrays.get(position).getIcon())
            //.apply(Utility.createGlideOptionMetadData(mContext))
            .into(new SimpleTarget<Drawable>() {
              @Override
              public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                ((BulletTextTypeViewHolder) holder).imageIconMeta.setImageDrawable(resource);
              }
            });

      }

      if (getItemViewType(position) == ONE || getItemViewType(position) == TWO
          || getItemViewType(position) == THREE) {
        if (mMetaDataArrays.get(position).getPreDefined() != null && mMetaDataArrays.get(
            position).getPreDefined().size() > 0) {
          ((BulletTextTypeViewHolder) holder).mRecyclerView.setVisibility(View.VISIBLE);
          ((BulletTextTypeViewHolder) holder).mRecyclerView.setHasFixedSize(true);
          ((BulletTextTypeViewHolder) holder).mLaguageAdapter = new LanguageAdapter(
              mAppTypeface, mMetaDataArrays.get(position).getData(),
              mMetaDataArrays.get(position).getPreDefined());
          ((BulletTextTypeViewHolder) holder).mRecyclerView.setLayoutManager(
              new LinearLayoutManager(mContext));
          ((BulletTextTypeViewHolder) holder).mRecyclerView.setItemAnimator(
              new DefaultItemAnimator());
          ((BulletTextTypeViewHolder) holder).mRecyclerView.setAdapter(
              ((BulletTextTypeViewHolder) holder).mLaguageAdapter);
        }
      } else {
        if (mMetaDataArrays.get(position).getData() != null && !mMetaDataArrays.get(position).getData().trim().isEmpty()) {
          ((BulletTextTypeViewHolder) holder).mRecyclerView.setVisibility(View.VISIBLE);
          ((BulletTextTypeViewHolder) holder).mRecyclerView.setHasFixedSize(true);
          String[] medias = mMetaDataArrays.get(position).getData().contains(",") ? mMetaDataArrays.get(position).getData().split(",") : new String[]{mMetaDataArrays.get(position).getData()};
          ArrayList<String> mediaList = new ArrayList<>(Arrays.asList(medias));
          ((BulletTextTypeViewHolder) holder).mediaDisplayAdapter = new MediaDisplayAdapter(mMetaDataArrays.get(position).getType(), mediaList);
          ((BulletTextTypeViewHolder) holder).mRecyclerView.setLayoutManager(
              new GridLayoutManager(mContext, 3));
          ((BulletTextTypeViewHolder) holder).mRecyclerView.setItemAnimator(
              new DefaultItemAnimator());
          ((BulletTextTypeViewHolder) holder).mRecyclerView.setAdapter(
              ((BulletTextTypeViewHolder) holder).mediaDisplayAdapter);
        }
      }

    }
  }

  @Override
  public int getItemCount() {
    return mMetaDataArrays == null ? 0 : mMetaDataArrays.size();

  }

  public class TextTypeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvDesc)
    TextView tvDesc;
    @BindView(R.id.imageIconMeta)
    ImageView imageIconMeta;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.llDesc)
    LinearLayout llDesc;
    @BindView(R.id.llName)
    LinearLayout llName;


    TextTypeViewHolder(
        View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);

      tvName.setTypeface(mAppTypeface.getHind_semiBold());
      tvDesc.setTypeface(mAppTypeface.getHind_medium());

    }
  }

  public class ImageTypeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvDesc)
    TextView tvDesc;
    @BindView(R.id.imageIconMeta)
    ImageView imageIconMeta;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.llDesc)
    LinearLayout llDesc;
    @BindView(R.id.llName)
    LinearLayout llName;

    ImageTypeViewHolder(
        View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      tvName.setTypeface(mAppTypeface.getHind_semiBold());
      tvDesc.setTypeface(mAppTypeface.getHind_medium());
    }
  }

  public class BulletTextTypeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.llName)
    LinearLayout llName;
    @BindView(R.id.imageIconMeta)
    ImageView imageIconMeta;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    LanguageAdapter mLaguageAdapter;
    MediaDisplayAdapter mediaDisplayAdapter;

    BulletTextTypeViewHolder(
        View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      tvName.setTypeface(mAppTypeface.getHind_semiBold());
    }

  }

  private class NullViewHolder extends RecyclerView.ViewHolder {
    public NullViewHolder(View view) {
      super(view);
    }
  }
}