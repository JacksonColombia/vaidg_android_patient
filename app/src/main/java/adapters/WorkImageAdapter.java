package adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.vaidg.R;
import com.vaidg.providerdetails.Image_Gallery;
import com.vaidg.utilities.AppTypeface;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * <h2>LangExpertiseAdapter</h2>
 * Created by Ali on 2/6/2018.
 */

public class WorkImageAdapter extends RecyclerView.Adapter<WorkImageAdapter.ViewHolders> {

    private Context mContext;
    private ArrayList<String> mStringArrayList = new ArrayList<>();
    private String TAG = WorkImageAdapter.class.getSimpleName();
    private AppTypeface mAppTypeface;

    public WorkImageAdapter(
            AppTypeface appTypeface, ArrayList<String> stringArrayList) {
        this.mAppTypeface = appTypeface;
        this.mStringArrayList = stringArrayList;
    }


    @NonNull
    @Override
    public ViewHolders onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.doctor_image,
                parent, false);
        mContext = parent.getContext();
        return new ViewHolders(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolders holder, int position) {

        if (!mStringArrayList.get(position).equals("")) {

            Glide.with(mContext)
                    .load(mStringArrayList.get(position))
                    .apply(new RequestOptions().transforms(new CenterCrop(), new RoundedCorners(1)))
                    .into(holder.ivmainjob);
        }

        holder.ivmainjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(mContext, Image_Gallery.class);
                intent.putStringArrayListExtra("Imagelist", mStringArrayList);
                mContext.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return mStringArrayList == null ? 0 : mStringArrayList.size();
    }

    class ViewHolders extends RecyclerView.ViewHolder {
        @BindView(R.id.ivmainjob)
        ImageView ivmainjob;

        ViewHolders(
                View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
