package adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vaidg.R;
import com.vaidg.utilities.PhotoFullPopupWindow;
import java.util.ArrayList;

public class MediaDisplayAdapter extends RecyclerView.Adapter<MediaDisplayAdapter.MediaSelectViewHolder> {
  private static final long CLICK_TIME_INTERVAL = 300;
  private static final int TWELVE = 12;
  private static final int THIRTEEN = 13;
  private Context mContext;
  private int type;
  private ArrayList<String> media;
  private long mLastClickTime = System.currentTimeMillis();

  public MediaDisplayAdapter(int type, ArrayList<String> media) {
    this.type = type;
    this.media = media;
  }

  @NonNull
  @Override
  public MediaSelectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    mContext = parent.getContext();
    View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_cell_with_media_display, parent, false);
    return new MediaSelectViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(@NonNull MediaSelectViewHolder holder, int position) {
    RequestOptions requestOptions = new RequestOptions();
    requestOptions.isMemoryCacheable();
    Glide.with(mContext)
        .setDefaultRequestOptions(requestOptions)
        .load(media.get(position))
        .into(holder.ivThumbnail);
  }

  @Override
  public int getItemCount() {
    return media == null ? 0 : media.size();
  }
  public static Dialog getBaseDialog(Context context, boolean cancelable, int layoutResID) {
    Dialog dialog = new Dialog(context);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setCancelable(cancelable);
    dialog.setCanceledOnTouchOutside(cancelable);
    dialog.setContentView(layoutResID);

    return dialog;
  }

  public void getVideoDialog(Context context, String videoLocation, boolean autoplay) {
    final Dialog dialog = getBaseDialog(context, true, R.layout.dialog);
    ((Activity) context).getWindow().setFormat(PixelFormat.TRANSLUCENT);
    final VideoView videoHolder = dialog.findViewById(R.id.videoDialog);
    ImageView image = dialog.findViewById(R.id.imageDialog);
    videoHolder.setVideoURI(Uri.parse(videoLocation));
    //videoHolder.setRotation(90);
    MediaController mediaController = new MediaController(context);
    videoHolder.setMediaController(mediaController);
    mediaController.setAnchorView(videoHolder);
    videoHolder.requestFocus();
    if (autoplay) {
      videoHolder.start();
    }
    dialog.show();
    videoHolder.setOnCompletionListener(mp -> {
      dialog.dismiss();
      notifyDataSetChanged();
    });
    // if decline button is clicked, close the custom dialog
    image.setOnClickListener(v -> {
      // Close dialog
      dialog.dismiss();
    });
    // return dialog;
  }

  class MediaSelectViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    @BindView(R.id.ivJobPhoto)
    AppCompatImageView ivThumbnail;
    @BindView(R.id.ivJobPlay)
    AppCompatImageView ivJobPlay;

    public MediaSelectViewHolder(@NonNull View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      ivThumbnail.setOnClickListener(this);
      if (type == THIRTEEN) {
        ivJobPlay.setVisibility(View.VISIBLE);
      }
    }

    @Override
    public void onClick(View view) {
      long now = System.currentTimeMillis();
      if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
        return;
      }
      mLastClickTime = now;
      if (type == THIRTEEN) {
        getVideoDialog(mContext, media.get(getAdapterPosition()), true);
      } else {
        new PhotoFullPopupWindow(mContext, R.layout.popup_photo_full, view, media.get(getAdapterPosition()), null);
      }
    }
  }
}