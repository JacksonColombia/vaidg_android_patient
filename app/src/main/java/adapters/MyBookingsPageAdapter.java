package adapters;

import static com.vaidg.utilities.Constants.W_31;
import static com.vaidg.utilities.Constants.W_320;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import com.bumptech.glide.request.RequestOptions;
import com.vaidg.R;
import com.vaidg.cancelledBooking.CancelledBooking;
import com.vaidg.jobDetailsStatus.JobDetailsActivity;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.Utility;
import com.pojo.AllBookingEventPojo;
import com.utility.PicassoTrustAll;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * <h2>MyBookingsPageAdapter</h2>
 * Created by Ali on 2/12/2018.
 */

public class MyBookingsPageAdapter extends RecyclerView.Adapter {


  private Context mContext;
  private Activity mActivity;
  private ArrayList<AllBookingEventPojo> allEvents;
  private boolean isActivityCalled = true;
  private int w31;

  public MyBookingsPageAdapter(ArrayList<AllBookingEventPojo> allEventsList) {
    this.allEvents = allEventsList;
    calculate();
  }

  private void calculate() {
    w31 = Utility.getScreenWidth() * W_31 / W_320;
  }


  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_booking_page_adapter, parent, false);
    mActivity = (Activity) parent.getContext();
    mContext = parent.getContext();
    return new ViewHolderHold(view);
  }

  public void isActivityCalled(boolean isCalled) {
    isActivityCalled = isCalled;
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    ViewHolderHold holderHold = (ViewHolderHold) holder;

    holderHold.tvMyEvnetProStatus.setText(allEvents.get(position).getAddLine1());
    holderHold.tvStatus.setText(allEvents.get(position).getStatusMsg());
    timeMethod(holderHold.tvMyEvnetDateNTime, allEvents.get(position).getBookingRequestedFor());
    // holderHold.tvBookingCategory.setText(allEvents.get(position).getCategory());
    if (allEvents.get(position).getCallType() == 1)
      holderHold.tvBookingCategory.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_incall, 0, 0, 0);
    else if (allEvents.get(position).getCallType() == 2)
      holderHold.tvBookingCategory.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_outcall, 0, 0, 0);
    else {
      holderHold.tvBookingCategory.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_telecall, 0, 0, 0);
      holderHold.tvMyEvnetProStatus.setVisibility(View.GONE);
    }

    if (allEvents.get(position).getTotalAmount() > 0) {
      String totalAmt = allEvents.get(position).getCurrencySymbol() + " " + allEvents
          .get(position).getTotalAmount();
      holderHold.tvBookingAmount.setText(totalAmt);

    }
    switch (allEvents.get(position).getStatus()) {
      case 1:
      case 2:
        break;
      case 3:
      case 6:
      case 7:
      case 8:
      case 9:
        holderHold.tvStatus.setTextColor(Utility.getColor(mContext, R.color.txtDrkGreen));
       // holderHold.tvStatus.setBackgroundResource(R.drawable.ic_circular_status);
        setPicInToImageView(position, holderHold);
        break;
      case 4:
        holderHold.tvStatus.setTextColor(Utility.getColor(mContext, R.color.redGoogle));
      //  holderHold.tvStatus.setBackgroundResource(R.drawable.ic_circular_status_cancel);
        holderHold.tvAsap.setText("------");
        break;
      case 5:
        holderHold.tvStatus.setTextColor(Utility.getColor(mContext, R.color.saffron));
       // holderHold.tvStatus.setBackgroundResource(R.drawable.ic_circular_status_expire);
        holderHold.tvAsap.setText("------");
        break;
      case 10:
        holderHold.tvStatus.setTextColor(Utility.getColor(mContext, R.color.txtDrkGreen));
      //  holderHold.tvStatus.setBackgroundResource(R.drawable.ic_circular_status);
        setPicInToImageView(position, holderHold);
      case 11:
        holderHold.tvStatus.setTextColor(Utility.getColor(mContext, R.color.txtDrkGreen));
      //  holderHold.tvStatus.setBackgroundResource(R.drawable.ic_circular_status);
        setPicInToImageView(position, holderHold);
        break;
      case 12:
        holderHold.tvStatus.setTextColor(Utility.getColor(mContext, R.color.txtDrkGreen));
      //  holderHold.tvStatus.setBackgroundResource(R.drawable.ic_circular_status);
        setPicInToImageView(position, holderHold);
        break;
      case 15:
        holderHold.tvStatus.setTextColor(Utility.getColor(mContext, R.color.txtDrkGreen));
       // holderHold.tvStatus.setBackgroundResource(R.drawable.ic_circular_status);
        holderHold.tvAsap.setText("------");
        break;
    }

  }

  private void setPicInToImageView(int position, ViewHolderHold holderHold) {
      holderHold.llProfileAccepted.setVisibility(View.VISIBLE);
      holderHold.tvAsap.setVisibility(View.GONE);
      String name = allEvents.get(position).getTitle()+ " " + allEvents.get(position).getFirstName() + " " + allEvents.get(position).getLastName();
      holderHold.tvMyEventProName.setText(name);
      holderHold.tvMyEventProNameDesc.setText(allEvents.get(position).getCategory());
    try {
      if ( allEvents.get(position).getProfilePic() !=null && !allEvents.get(position).getProfilePic().isEmpty()) {
        holderHold.ivMyEventProPic.setVisibility(View.VISIBLE);
        PicassoTrustAll.getInstance(mContext)
                .load(allEvents.get(position).getProfilePic())
                .placeholder(R.drawable.default_photo)   // optional
                .error(R.drawable.default_photo)      // optional
                .transform(new PicassoCircleTransform())
                .into(holderHold.ivMyEventProPic);


      } else {
        holderHold.ivMyEventProPic.setVisibility(View.GONE);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void timeMethod(TextView tvMyEvnetDateNTime, long bookingRequestedFor) {

    try {

     // Date date = new Date(bookingRequestedFor * 1000L);
      tvMyEvnetDateNTime.setText(String.valueOf(Utility.getFormattedDateLong(TimeUnit.SECONDS.toMillis(bookingRequestedFor))));
    } catch (Exception e) {
      Log.d("TAG", "timeMethodException: " + e.toString());
    }
  }

  @Override
  public int getItemCount() {
    return allEvents == null ? 0 : allEvents.size();
  }

  class ViewHolderHold extends RecyclerView.ViewHolder implements View.OnClickListener {
    AppTypeface appTypeface;
    @BindView(R.id.ivMyEventProPic)
    ImageView ivMyEventProPic;
    @BindView(R.id.tvMyEventProName)
    TextView tvMyEventProName;
    @BindView(R.id.tvMyEventProNameDesc)
    TextView tvMyEventProNameDesc;
    @BindView(R.id.tvBookingAmount)
    TextView tvBookingAmount;
    @BindView(R.id.tvMyEvnetDateNTime)
    TextView tvMyEvnetDateNTime;
    @BindView(R.id.tvMyEvnetProStatus)
    TextView tvMyEvnetProStatus;
    @BindView(R.id.tvAsap)
    TextView tvAsap;
    @BindView(R.id.tvStatus)
    TextView tvStatus;
    @BindView(R.id.tvBookingCategory)
    TextView tvBookingCategory;
    @BindView(R.id.llProfileAccepted)
    LinearLayout llProfileAccepted;

    private RequestOptions options;

    public ViewHolderHold(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      itemView.setOnClickListener(this);

      appTypeface = AppTypeface.getInstance(mContext);
      tvMyEventProName.setTypeface(appTypeface.getHind_medium());
      tvMyEventProNameDesc.setTypeface(appTypeface.getHind_regular());
      tvBookingAmount.setTypeface(appTypeface.getHind_semiBold());
      tvMyEvnetDateNTime.setTypeface(appTypeface.getHind_regular());
      tvMyEvnetProStatus.setTypeface(appTypeface.getHind_regular());
      tvAsap.setTypeface(appTypeface.getHind_medium());
      tvStatus.setTypeface(appTypeface.getHind_medium());
      tvBookingCategory.setTypeface(appTypeface.getHind_semiBold());
      tvBookingAmount.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_15));
      tvMyEvnetDateNTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_12));
      tvMyEvnetProStatus.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_12));
      tvStatus.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_12));
      tvMyEventProName.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_15));
      tvAsap.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_15));
      tvMyEventProNameDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_12));
      tvStatus.getLayoutParams().height = w31;
    }

    @Override
    public void onClick(View view) {
      Log.d("TAG", "onClick: " + allEvents.get(getAdapterPosition()).getStatus());
      AllBookingEventPojo allBookingEventPojo = allEvents.get(getAdapterPosition());
      ArrayList<AllBookingEventPojo> alPojo = new ArrayList<>();
      alPojo.add(allBookingEventPojo);
      Intent intent;
      switch (allEvents.get(getAdapterPosition()).getStatus()) {
        case 1:
        case 2:
        case 3:
        case 6:
        case 7:
        case 8:
        case 9:
          /* case 15:*/
        case 17:
          if (isActivityCalled) {
            intent = new Intent(mContext, JobDetailsActivity.class);
            callNextActivity(intent, alPojo, false);
          }

          break;

                   /* intent = new Intent(mContext, CompletedInvoiceInfo.class);
                    callNextActivity(intent,alPojo,true);*/
        case 5:
        case 4:
        case 11:
        case 10:
        case 12:
        case 15:
          if (isActivityCalled) {
            intent = new Intent(mContext, CancelledBooking.class);
            callNextActivity(intent, alPojo, true);
          }

          break;
      }
    }

    private void callNextActivity(Intent intent, ArrayList<AllBookingEventPojo> allBookingEventPojo, boolean b) {

      Constants.bookingcurrencySymbol = allBookingEventPojo.get(0).getCurrencySymbol();
      intent.putExtra("BID", allEvents.get(getAdapterPosition()).getBookingId());
      intent.putExtra("STATUS", allEvents.get(getAdapterPosition()).getStatus());
      intent.putExtra("BookingModel", allEvents.get(getAdapterPosition()).getBookingModel());
      intent.putExtra("CallType", allEvents.get(getAdapterPosition()).getCallType());
      if (b) {
        intent.putExtra("ALLPOJO", allBookingEventPojo);
        intent.putExtra("BID", allEvents.get(getAdapterPosition()).getBookingId());
        intent.putExtra("ImageUrl", allEvents.get(getAdapterPosition()).getProfilePic());
      }

      mContext.startActivity(intent);
      isActivityCalled = false;
      mActivity.overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
    }
  }

}
