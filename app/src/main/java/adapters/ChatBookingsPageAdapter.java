package adapters;

import android.content.Context;
import android.content.Intent;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vaidg.R;
import com.vaidg.chatting.ChattingActivity;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.SessionManager;
import com.vaidg.utilities.Utility;
import com.pojo.BookingChatHistory;
import com.utility.PicassoTrustAll;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Ali on 5/28/2018.
 */
public class ChatBookingsPageAdapter extends RecyclerView.Adapter
{

    private Context mContext;
    ArrayList<BookingChatHistory> bookingChatHistories;
    public ChatBookingsPageAdapter(ArrayList<BookingChatHistory> bookingChatHistories)
    {
        this.bookingChatHistories = bookingChatHistories;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_chat_customer_list,parent,false);
        mContext = parent.getContext();
        return new ChatViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        ChatViewHolder hold = (ChatViewHolder) holder;
        if(bookingChatHistories.get(position).getProfilePic() != null && !bookingChatHistories.get(position).getProfilePic().isEmpty())
        {
            PicassoTrustAll.getInstance(mContext)
                    .load(bookingChatHistories.get(position).getProfilePic())
                    .placeholder(R.drawable.register_profile_default_image)   // optional
                    .error(R.drawable.register_profile_default_image)// optional
                    .transform(new PicassoCircleTransform())
                    .into(hold.ivCustomer);

        }
        if(bookingChatHistories.size()-1==position)
            hold.view1.setVisibility(View.GONE);

        String name = bookingChatHistories.get(position).getTitle() + " " + bookingChatHistories.get(position).getFirstName() + " " + bookingChatHistories.get(position).getLastName();
        hold.tvCustomerName.setText(name);
        hold.tvJobDetails.setText(bookingChatHistories.get(position).getCatName());
        if(bookingChatHistories.get(position).getLastCahtMsgTimeStamp()>0)
            timeMethod(hold.tvDate,hold.tvTime, bookingChatHistories.get(position).getLastCahtMsgTimeStamp());
    }

    private void timeMethod(TextView tvDate, TextView tvTime, long bookingRequestedFor) {
        try {


            Log.d("TAGTIME", " expireTime " + bookingRequestedFor);
            Date date = new Date(TimeUnit.SECONDS.toMillis(bookingRequestedFor));
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
            sdf.setTimeZone(Utility.getTimeZone());
            String formattedDate = sdf.format(date);
            String[] splitDate = formattedDate.split(" ");
            Log.d("TAG", "timeMethod: "+formattedDate);
            //tvDate.setText(splitDate[0]);
            tvDate.setText(getFormattedDate(mContext,TimeUnit.SECONDS.toMillis(bookingRequestedFor)));
            String time = splitDate[1]+" "+splitDate[2];
           // tvTime.setText(time);
            tvTime.setText(getFormattedTime(mContext,TimeUnit.SECONDS.toMillis(bookingRequestedFor)));
          //  Log.d("TAG", "timeMethodException: " + tvDate+"  "+getFormattedDate(mContext,bookingRequestedFor*1000L));
        } catch (Exception e) {
            Log.d("TAG", "timeMethodException: " + e.toString());
        }
    }

    public String getFormattedDate(Context context, long smsTimeInMilis) {
        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeZone(Utility.getTimeZone());
        smsTime.setTimeInMillis(smsTimeInMilis);

        Calendar now = Calendar.getInstance();
        now.setTimeZone(Utility.getTimeZone());
        final String timeFormatString = "h:mm aa";
        final String dateTimeFormatString = "MM/dd/yyyy";
        final long HOURS = 60 * 60 * 60;
        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE) ) {
            return "Today ";
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1  ){
            return "Yesterday ";
        } else if (now.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR)) {
            return DateFormat.format(dateTimeFormatString, smsTime).toString();
        } else {
            return DateFormat.format("MM/dd/yyyy", smsTime).toString();
        }
    }
    public String getFormattedTime(Context context, long smsTimeInMilis) {
        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeZone(Utility.getTimeZone());
        smsTime.setTimeInMillis(smsTimeInMilis);

        Calendar now = Calendar.getInstance();
        now.setTimeZone(Utility.getTimeZone());
        final String timeFormatString = "h:mm aa";
        final String dateTimeFormatString = "hh:mm a";
        final long HOURS = 60 * 60 * 60;
        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE) ) {
            return DateFormat.format(timeFormatString, smsTime).toString();
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1  ){
            return DateFormat.format(timeFormatString, smsTime).toString();
        } else if (now.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR)) {
            return DateFormat.format(dateTimeFormatString, smsTime).toString();
        } else {
            return DateFormat.format("hh:mm a", smsTime).toString();
        }
    }

    @Override
    public int getItemCount() {
        return bookingChatHistories == null ? 0 : bookingChatHistories.size();
    }

    class ChatViewHolder extends RecyclerView.ViewHolder
    {
        private ImageView ivCustomer;
        private TextView tvCustomerName,tvJobDetails,tvDate,tvTime,tvCount;
        private View view1;
        private AppTypeface appTypeface;
        private SessionManager manager;
        public ChatViewHolder(View itemView) {
            super(itemView);
            appTypeface = AppTypeface.getInstance(mContext);
            manager = SessionManager.getInstance(mContext);
            ivCustomer = itemView.findViewById(R.id.ivCustomer);
            tvCustomerName = itemView.findViewById(R.id.tvCustomerName);
            tvJobDetails = itemView.findViewById(R.id.tvJobDetails);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvTime = itemView.findViewById(R.id.tvTime);
            tvCount = itemView.findViewById(R.id.tvCount);
            view1 = itemView.findViewById(R.id.view1);
            tvCustomerName.setTypeface(appTypeface.getHind_medium());
            tvJobDetails.setTypeface(appTypeface.getHind_regular());
            tvDate.setTypeface(appTypeface.getHind_medium());
            tvTime.setTypeface(appTypeface.getHind_regular());
            tvCount.setTypeface(appTypeface.getHind_regular());
            itemView.setOnClickListener(view -> {
                BookingChatHistory chat =  bookingChatHistories.get(getAdapterPosition());
                manager.setChatBookingID(chat.getBookingId());
                manager.setChatProId(chat.getProviderId());
                manager.setProName(chat.getTitle()+" "+chat.getFirstName()+" "+chat.getLastName());
                Intent intent = new Intent(mContext, ChattingActivity.class);
                if(chat.getStatus() == 1 || chat.getStatus() == 2
                        || chat.getStatus() == 3 || chat.getStatus() == 6 || chat.getStatus() == 7
                        || chat.getStatus() == 8 || chat.getStatus() == 9 || chat.getStatus() == 17)
                {
                    intent.putExtra("isChating",true);
                }
                intent.putExtra("STATUSCODE",chat.getStatus());
                intent.putExtra("CurrencySymbol",chat.getCurrencySymbol());
                intent.putExtra("AMOUNT",chat.getAmount());
                intent.putExtra("CallType",chat.getCallType());
                mContext.startActivity(intent);
            });
        }
    }
}
