package adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.vaidg.R;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.Utility;

import static com.vaidg.utilities.Utility.isFromIndia;

/**
 * <h2>SidescreenAdapter</h2>
 * Created by ${3Embed} on 4/10/17.
 */

public class SidescreenAdapter extends RecyclerView.Adapter {
    private String[] sideScreenNames;
    private int[] images;
   private Context mContext;
   private OnSideScreenClick onSideScreenClick;

    public SidescreenAdapter(String[] sideScreenNames, int[] images,OnSideScreenClick onSideScreenClick){
        this.sideScreenNames=sideScreenNames;
        this.images=images;
        this.onSideScreenClick = onSideScreenClick;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_sidescreen_layout,null);
        mContext = parent.getContext();
        VhSidescreen viewHolder = new VhSidescreen(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        VhSidescreen viewHolder= (VhSidescreen) holder;
        if(position == 0) {
            viewHolder.frameLayout.setVisibility(isFromIndia(mContext) ? View.GONE : View.VISIBLE);
            viewHolder.divider.setVisibility(isFromIndia(mContext) ? View.GONE : View.VISIBLE);
            viewHolder.ivImage.setVisibility(isFromIndia(mContext) ? View.GONE : View.VISIBLE);
        }else{
            viewHolder.frameLayout.setVisibility(View.VISIBLE);
            viewHolder.ivImage.setVisibility(View.VISIBLE);
            viewHolder.divider.setVisibility(View.VISIBLE);
        }
        viewHolder.tvScreen.setText(sideScreenNames[position]);
        if(sideScreenNames[position].equals("Wallet"))
        {
            Utility.setAmtOnRecept(Constants.walletAmount,viewHolder.tvWalletAmount,Constants.walletCurrency);
            String walletAmount = "("+viewHolder.tvWalletAmount.getText().toString()+")";

            viewHolder.tvWalletAmount.setText(walletAmount);

            //viewHolder.tvWalletAmount.setText("$ 5000.00");
        }
        viewHolder.ivImage.setImageDrawable(mContext.getResources().getDrawable(images[position]));
        if(position==(getItemCount()-1)){
            viewHolder.divider.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemCount() {
        return sideScreenNames == null ? 0 : sideScreenNames.length;
    }
    public interface OnSideScreenClick{
        void onSideScreenClicked(String sideScreenName);
    }

    class VhSidescreen extends RecyclerView.ViewHolder{
        ImageView ivImage;
        TextView tvScreen,tvWalletAmount;
        View divider;
        AppTypeface appTypeface;
        FrameLayout frameLayout;

        public VhSidescreen(View itemView) {
            super(itemView);
            frameLayout=itemView.findViewById(R.id.frameLayout);
            ivImage=itemView.findViewById(R.id.ivImage);
            tvScreen=itemView.findViewById(R.id.tvScreens);
            divider=itemView.findViewById(R.id.divider);
            tvWalletAmount=itemView.findViewById(R.id.tvWalletAmount);
            appTypeface = AppTypeface.getInstance(mContext);
            tvScreen.setTypeface(appTypeface.getHind_medium());
            tvWalletAmount.setTypeface(appTypeface.getHind_medium());

            tvScreen.setTypeface(AppTypeface.getInstance(mContext).getHind_medium());
            itemView.setOnClickListener(view -> {
                onSideScreenClick.onSideScreenClicked(sideScreenNames[getAdapterPosition()]);
            });
        }
    }
}
