package adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.vaidg.R;
import com.vaidg.home.ServiceFragContract;
import com.vaidg.incalloutcalltelecall.InCallOutCallTeleCallActivity;
import com.vaidg.model.Category;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.AutofitTextView;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.Utility;
import com.utility.AlertProgress;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

/**
 * Created by Ali on 7/31/2018.
 */
public class NewHomeSubCategory extends RecyclerView.Adapter {
  private ArrayList<Category> categoryList;
  // private OnServiceClicked context;
  private Context mContext;
  private Activity mActivity;
  private ServiceFragContract.ServiceView serviceView;
  private int w110;

  public NewHomeSubCategory(ArrayList<Category> categoryList,
                            ServiceFragContract.ServiceView serviceView) {
    this.categoryList = categoryList;
    this.serviceView = serviceView;
    calculate();
  }

  private void calculate() {
    w110 = Utility.getScreenWidth() * 110 / 320;
  }


  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.trending_services_item,
        parent, false);
    mActivity = (Activity) parent.getContext();
    mContext = parent.getContext();
    return new NewHomeSubHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    NewHomeSubHolder hold = (NewHomeSubHolder) holder;
    hold.cardViewCat.getLayoutParams().width = w110;
    hold.cardViewCat.getLayoutParams().height = w110;
    if (categoryList.size() > 0) {
      hold.tvTrendingServiceName.setText(categoryList.get(position).getCatName());
      if (categoryList.get(position).getBannerImageApp() != null) {
        Glide.with(mContext)
            .load(categoryList.get(position).getBannerImageApp())
            .into(hold.ivTrendingImage);
      }
    }
  }

  @Override
  public int getItemCount() {
    return categoryList == null ? 0 : categoryList.size();
  }


  private void callIntent(Intent intent, Category category, Bundle bundle) {
    bundle.putString("CatId", category.getId());
    bundle.putDouble("MinAmount", category.getMinimumFees());
    bundle.putDouble("MaxAmount", category.getMaximumFees());
    bundle.putSerializable("BookingTypeAction", category.getBookingTypeAction());
    bundle.putSerializable("CallType", category.getCallType());
    intent.putExtras(bundle);
    mContext.startActivity(intent);
    mActivity.overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
  }


  class NewHomeSubHolder extends RecyclerView.ViewHolder {
    ImageView ivTrendingImage;
    AutofitTextView tvTrendingServiceName;
    AppTypeface appTypeface;
    AlertProgress alertProgress;
    private Bundle bundle;
    private CardView cardViewCat;

    NewHomeSubHolder(View itemView) {
      super(itemView);
      appTypeface = AppTypeface.getInstance(mContext);
      alertProgress = new AlertProgress(mContext);
      bundle = new Bundle();
      ivTrendingImage = itemView.findViewById(R.id.ivTrendingImage);
      cardViewCat = itemView.findViewById(R.id.cardViewCat);
      tvTrendingServiceName = itemView.findViewById(R.id.tvTrendingServiceName);
      tvTrendingServiceName.setTypeface(appTypeface.getHind_medium());
      itemView.setOnClickListener(view -> {
        if (Utility.isNetworkAvailable(mContext)) {
        HashMap<Integer, Boolean> hashMap = new HashMap<>();
        hashMap.put(1, categoryList.get(getAdapterPosition()).getCallType().isIncall());
        hashMap.put(2, categoryList.get(getAdapterPosition()).getCallType().isOutcall());
        hashMap.put(3, categoryList.get(getAdapterPosition()).getCallType().isTelecall());
        Constants.catId = categoryList.get(getAdapterPosition()).getId();
        Constants.catName = categoryList.get(getAdapterPosition()).getCatName();
        Constants.bookingModel = categoryList.get(getAdapterPosition()).getBillingModel();
        Constants.serviceType = categoryList.get(getAdapterPosition()).getServiceType();
        Constants.callType = categoryList.get(getAdapterPosition()).getCallType();
        Constants.minFee = categoryList.get(getAdapterPosition()).getMinimumFeesForConsultancy();
        Constants.maxFee = categoryList.get(getAdapterPosition()).getMaximumFeesForConsultancy();
        Constants.proId = "0";
        Constants.visitFee = 0;
        Intent intent = new Intent(mActivity, InCallOutCallTeleCallActivity.class);//BookingType
        callIntent(intent, categoryList.get(getAdapterPosition()), bundle);
        } else {
          alertProgress.tryAgain(mContext, mContext.getResources().getString(R.string.pleaseCheckInternet),mContext.getResources().getString(R.string.system_error), isClicked -> {

          });
        }
        });

    }
  }
}
