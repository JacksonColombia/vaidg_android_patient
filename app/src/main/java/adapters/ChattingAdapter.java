package adapters;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.R;
import com.vaidg.chatting.ChattingPresenter;
import com.vaidg.encryptiondecryption.CryptographyExample;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.PhotoFullPopupWindow;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.RoundedCornersTransformation;
import com.vaidg.utilities.SessionManager;
import com.vaidg.utilities.Utility;
import com.pojo.ChatData;
import com.utility.PicassoTrustAll;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilPrivateKey;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;

import java.io.File;
import java.util.ArrayList;


/**
 * <h2>ChattingAdapter</h2>
 * Created by Ali on 12/22/2017.
 */

public class ChattingAdapter extends RecyclerView.Adapter {
    private static final String TAG = ChattingAdapter.class.getName();
    private Context mContext;
    private ArrayList<ChatData> chatData;
    private String customerId, privateKey = "", publicKey = "";
    private ChattingPresenter.Presenter presenter;
    private  File dir;

    public ChattingAdapter(ArrayList<ChatData> chatData, String customerId, ChattingPresenter.Presenter presenter) {
        this.chatData = chatData;
        this.customerId = customerId;
        this.presenter = presenter;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.privateKey = presenter.decryptPrivateKeyPostLollipop();
            this.publicKey = presenter.decryptPublicKeyPostLollipop();
        } else {
            this.privateKey = presenter.decryptPrivateKeyPreMarshMallow();
            this.publicKey = presenter.decryptPublicKeyPreMarshMallow();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chatting_adapter, parent, false);
        mContext = parent.getContext();
        return new ViewHolders(view);
    }

    private String encryptData(String privateKey, String publicKey, String messageFromDecrypt, String messageToDecrypt) {

        try {
            CryptographyExample cryptographyExample = new CryptographyExample();
            VirgilCrypto crypto = new VirgilCrypto();
            Log.w(TAG, "encryptData: " + privateKey);
            // Import private key
            VirgilPrivateKey importedPrivateKey = cryptographyExample.importPrivateKey(privateKey, crypto);
            // Decrypt data
            //  byte[] encryptedData = Base64.decode(messageToDecrypt, Base64.DEFAULT);/*Base64.getMimeDecoder().decode(chatData.get(position).getContent());*/
            messageFromDecrypt = cryptographyExample.dataDecryption(messageToDecrypt, importedPrivateKey);

        } catch (CryptoException e) {
            e.printStackTrace();
        }
        return messageFromDecrypt;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolders hldr = (ViewHolders) holder;
        String messageFromDecrypt = "";
        if (chatData.get(position).getType() == 1) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                messageFromDecrypt = encryptData(privateKey, publicKey, messageFromDecrypt, chatData.get(position).getContent());
            } else {
                messageFromDecrypt = encryptData(privateKey, publicKey, messageFromDecrypt, chatData.get(position).getContent());
            }
        } else {
            messageFromDecrypt = chatData.get(position).getContent();
        }
        SessionManager sessionManager = new SessionManager(mContext);
        //  if(chatData.get(position).getCustProType()==1)
        if (customerId.equals(chatData.get(position).getFromID())) {
            hldr.rlCustMsg.setVisibility(View.VISIBLE);
            hldr.rlProMsg.setVisibility(View.GONE);
            hldr.ivProProfilePic.setVisibility(View.GONE);
            hldr.ivCustProfilePic.setVisibility(View.VISIBLE);
            hldr.rlCustMsgChild.setVisibility(View.VISIBLE);

           /* SimpleDateFormat formatter = new SimpleDateFormat("hh:mm a", Locale.US);
            formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
            String text = formatter.format(new Date(chatData.get(position).getTimestamp()));
            hldr.tvCustTime.setText(text);*/
            if (sessionManager.getProfilePicUrl() != null && !sessionManager.getProfilePicUrl().isEmpty()) {
                PicassoTrustAll.getInstance(mContext)
                        .load(sessionManager.getProfilePicUrl())
                        .placeholder(R.drawable.default_photo)   // optional
                        .error(R.drawable.default_photo)      // optional
                        .transform(new PicassoCircleTransform())
                        .into(hldr.ivCustProfilePic);
            }
            if (chatData.get(position).getType() == 1) {
                hldr.tvCustMsg.setVisibility(View.VISIBLE);
                hldr.ivCustSendPic.setVisibility(View.GONE);
                hldr.tvCustDoc.setVisibility(View.GONE);
                hldr.tvCustMsg.setText(messageFromDecrypt);
            } else if (chatData.get(position).getType() == 3) {
                hldr.tvCustMsg.setVisibility(View.GONE);
                hldr.ivCustSendPic.setVisibility(View.GONE);
                hldr.tvCustDoc.setVisibility(View.VISIBLE);
                String fileName = messageFromDecrypt.substring(messageFromDecrypt.lastIndexOf('/') + 1, messageFromDecrypt.length());
                // String fileNameWithoutExtn = fileName.substring(0, fileName.lastIndexOf('.'));
                hldr.tvCustDoc.setText(fileName);
                switch (getMimeType(mContext, Uri.parse(messageFromDecrypt))) {
                    case "pdf":
                        hldr.tvCustDoc.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.icon_file_pdf), null, null, null);
                        break;
                    case "doc":
                    case "docx":
                    case "dot":
                    case "dotx":
                        hldr.tvCustDoc.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.icon_file_doc), null, null, null);
                        break;
                    case "ppt":
                    case "pptx":
                        hldr.tvCustDoc.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.icon_file_ppt), null, null, null);
                        break;
                    case "xls":
                    case "xlsx":
                        hldr.tvCustDoc.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.icon_file_xls), null, null, null);
                        break;
                    default:
                        hldr.tvCustDoc.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.icon_file_unknown), null, null, null);
                        break;
                }
                hldr.tvCustDoc.setOnClickListener(v -> {

                    if(Utility.isNetworkAvailable(mContext))
                    mContext.startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse(chatData.get(position).getContent())));
                    else
                        Toast.makeText(mContext, "Please check your internetConnection", Toast.LENGTH_SHORT).show();
                    /*    Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                    browserIntent.setDataAndType(Uri.parse(chatData.get(position).getContent()), getMimeType(mContext,Uri.parse(chatData.get(position).getContent())));

                    Intent chooser = Intent.createChooser(browserIntent, "Open Category");
                    chooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // optional

                    ((Activity)mContext).startActivity(chooser);*/
                    //openFile(new File(chatData.get(position).getContent()));
                });

            } else {
                hldr.tvCustMsg.setVisibility(View.GONE);
                hldr.tvCustDoc.setVisibility(View.GONE);
                hldr.ivCustSendPic.setVisibility(View.VISIBLE);
                String url = messageFromDecrypt;
                if (url != null && !url.isEmpty()) {
                    PicassoTrustAll.getInstance(mContext)
                            .load(url)
                            .transform(new RoundedCornersTransformation(10, 0))
                            .into(hldr.ivCustSendPic);

                }
                hldr.ivCustSendPic.setOnClickListener(view -> new PhotoFullPopupWindow(mContext, R.layout.popup_photo_full, view, url, null));
            }
        } else {
            hldr.rlCustMsg.setVisibility(View.GONE);
            hldr.rlProMsg.setVisibility(View.VISIBLE);
            hldr.ivProProfilePic.setVisibility(View.VISIBLE);
            if (sessionManager.getChatProPic() != null && !sessionManager.getChatProPic().isEmpty()) {
                PicassoTrustAll.getInstance(mContext)
                        .load(sessionManager.getChatProPic())
                        .placeholder(R.drawable.default_photo)   // optional
                        .error(R.drawable.default_photo)      // optional
                        .transform(new PicassoCircleTransform())
                        .into(hldr.ivProProfilePic);
            }
            if (chatData.get(position).getType() == 1) {
                hldr.tvProMsg.setVisibility(View.VISIBLE);
                hldr.tvProDoc.setVisibility(View.GONE);
                hldr.ivProReceivedPic.setVisibility(View.GONE);
                hldr.tvProMsg.setText(messageFromDecrypt);
            }else if(chatData.get(position).getType() == 3) {
                hldr.tvProMsg.setVisibility(View.GONE);
                hldr.tvProDoc.setVisibility(View.VISIBLE);
                hldr.ivProReceivedPic.setVisibility(View.GONE);
                String fileName = messageFromDecrypt.substring(messageFromDecrypt.lastIndexOf('/') + 1, messageFromDecrypt.length());
                // String fileNameWithoutExtn = fileName.substring(0, fileName.lastIndexOf('.'));
                hldr.tvProDoc.setText(fileName);
                switch (getMimeType(mContext, Uri.parse(messageFromDecrypt))) {
                    case "pdf":
                        hldr.tvProDoc.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.icon_file_pdf), null, null, null);
                        break;
                    case "doc":
                    case "docx":
                    case "dot":
                    case "dotx":
                        hldr.tvProDoc.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.icon_file_doc), null, null, null);
                        break;
                    case "ppt":
                    case "pptx":
                        hldr.tvProDoc.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.icon_file_ppt), null, null, null);
                        break;
                    case "xls":
                    case "xlsx":
                        hldr.tvProDoc.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.icon_file_xls), null, null, null);
                        break;
                    default:
                        hldr.tvProDoc.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.icon_file_unknown), null, null, null);
                        break;
                }

                hldr.tvProDoc.setOnClickListener(v -> {
                    if(Utility.isNetworkAvailable(mContext))
                        mContext.startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse(chatData.get(position).getContent())));
                    else
                        Toast.makeText(mContext, "Please check your internetConnection", Toast.LENGTH_SHORT).show();

                    /*    Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                    browserIntent.setDataAndType(Uri.parse(chatData.get(position).getContent()), getMimeType(mContext,Uri.parse(chatData.get(position).getContent())));

                    Intent chooser = Intent.createChooser(browserIntent, "Open Category");
                    chooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // optional

                    ((Activity)mContext).startActivity(chooser);*/
                    //openFile(new File(chatData.get(position).getContent()));
                });


            }else {
                hldr.tvProMsg.setVisibility(View.GONE);
                hldr.tvProDoc.setVisibility(View.GONE);
                hldr.ivProReceivedPic.setVisibility(View.VISIBLE);
                String url = messageFromDecrypt;
                if (url != null && !url.isEmpty()) {
                   /* Picasso.with(mContext)
                            .load(url)
                            .resize((int)hldr.width,(int)hldr.height)
                            .into(hldr.ivProReceivedPic);*/
                    PicassoTrustAll.getInstance(mContext)
                            .load(url)
                            .transform(new RoundedCornersTransformation(10, 0))
                            .into(hldr.ivProReceivedPic);

                }
                hldr.ivProReceivedPic.setOnClickListener(view -> new PhotoFullPopupWindow(mContext, R.layout.popup_photo_full, view, url, null));
            }
        }
    }



    @Override
    public int getItemCount() {
        return chatData == null ? 0 : chatData.size();
    }

    class ViewHolders extends RecyclerView.ViewHolder {
        private RelativeLayout rlCustMsg, rlProMsg,rlCustMsgChild;
        private ImageView ivChatSendStatus, ivCustSendPic, ivProReceivedPic, ivCustProfilePic, ivProProfilePic;
        private TextView tvCustMsg, tvProMsg, tvCustTime, tvCustDoc,tvProDoc;
        private AppTypeface appTypeface;
        private double width, height;

        public ViewHolders(View itemView) {
            super(itemView);
            appTypeface = AppTypeface.getInstance(mContext);

            ivChatSendStatus = itemView.findViewById(R.id.ivChatSendStatus);
            ivCustSendPic = itemView.findViewById(R.id.ivCustSendPic);
            // tvCustTime = itemView.findViewById(R.id.tvCustTime);
            tvCustDoc = itemView.findViewById(R.id.tvCustDoc);
            tvProDoc = itemView.findViewById(R.id.tvProDoc);
            ivProReceivedPic = itemView.findViewById(R.id.ivProReceivedPic);
            ivCustProfilePic = itemView.findViewById(R.id.ivCustProfilePic);
            rlCustMsgChild = itemView.findViewById(R.id.rlCustMsgChild);
            ivProProfilePic = itemView.findViewById(R.id.ivProProfilePic);
            tvCustMsg = itemView.findViewById(R.id.tvCustMsg);
            tvProMsg = itemView.findViewById(R.id.tvProMsg);
            rlCustMsg = itemView.findViewById(R.id.rlCustMsg);
            rlProMsg = itemView.findViewById(R.id.rlProMsg);
            tvCustMsg.setTypeface(appTypeface.getHind_regular());
            tvProMsg.setTypeface(appTypeface.getHind_regular());

        }
    }

    public static String getMimeType(Context context, Uri uri) {
        String extension;

        //Check uri format to avoid null
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //If scheme is a content
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());

        }

        return extension;
    }


}
