package adapters;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.vaidg.R;
import com.vaidg.utilities.AppTypeface;
import com.pojo.SymptomQuestionAnswer;
import java.util.ArrayList;

/**
 * Created by Ali on 7/24/2018.
 */
public class QuestionSymptomListAdapterGrid extends RecyclerView.Adapter {

  private Context mContext;
  private ArrayList<SymptomQuestionAnswer> questionAndAnswer;
  private int colour = 0;

  public QuestionSymptomListAdapterGrid(ArrayList<SymptomQuestionAnswer> bidQuestionAnswers) {
    this.questionAndAnswer = bidQuestionAnswers;
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.job_question_answer_list, parent, false);
    mContext = parent.getContext();
    return new ImageViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    int pos = position;
    ((ImageViewHolder) holder).tvJobAnswers.setText((pos + 1) + "-" + questionAndAnswer.size() + ". " + questionAndAnswer.get(position).getSymptomName());
    QuestionSymptomAdapterGrid questionSymptomAdapterGrid = new QuestionSymptomAdapterGrid(questionAndAnswer.get(position).getSymptomAnswers());
    ((ImageViewHolder) holder).recyclerViewQuestionImage.setLayoutManager(new LinearLayoutManager(mContext));
    ((ImageViewHolder) holder).recyclerViewQuestionImage.setAdapter(questionSymptomAdapterGrid);
  }

  @Override
  public int getItemCount() {
    return questionAndAnswer == null ? 0 : questionAndAnswer.size();
  }


  private class ImageViewHolder extends RecyclerView.ViewHolder {
    TextView tvJobAnswers;
    RecyclerView recyclerViewQuestionImage;
    AppTypeface appTypeface;

    ImageViewHolder(View itemView) {
      super(itemView);
      recyclerViewQuestionImage = itemView.findViewById(R.id.recyclerViewQuestionImage);
      tvJobAnswers = itemView.findViewById(R.id.tvJobAnswers);
      appTypeface = AppTypeface.getInstance(mContext);
      tvJobAnswers.setTypeface(appTypeface.getHind_regular());
      tvJobAnswers.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_15));
    }
  }

}
