package adapters;

import static com.vaidg.utilities.Constants.DIALOGMESSAGESHOW_YES;
import static com.vaidg.utilities.Utility.setTextSize12Sp;
import static com.vaidg.utilities.Utility.setTextSize14Sp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.vaidg.R;
import com.vaidg.add_address.AddAddressActivity;
import com.vaidg.incalloutcalltelecall.addaddress.YourAddressFragmentPresenter;
import com.vaidg.youraddress.model.YourAddrData;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Utility;
import com.utility.AlertProgress;

import java.util.List;

/**
 * @author Pramod
 * @since 19-01-2018.
 */
public class AddressListOneAdapter extends RecyclerView.Adapter {
  public int selectedItem = -1;
  private Context mContext;
  private YourAddressFragmentPresenter presenter;
  private String auth;
  private AppTypeface appTypeface;
  private boolean isBidding;
  private List<YourAddrData> items;

  public AddressListOneAdapter(List<YourAddrData> items, YourAddressFragmentPresenter presenter, String auth, boolean isBidding) {
    this.auth = auth;
    this.presenter = presenter;
    this.isBidding = isBidding;
    this.items = items;
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_your_address_list, parent, false);
    mContext = parent.getContext();
    return new ViewHolderRecycler(itemView);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    ViewHolderRecycler hold = (ViewHolderRecycler) holder;
    hold.tv_type.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
    hold.tv_address1.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_12));
    hold.tv_address2.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_12));

    hold.tv_edit_address.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (Utility.isNetworkAvailable(mContext)) {
        Intent intent = new Intent(mContext, AddAddressActivity.class);
        if (items.get(hold.getAdapterPosition()) != null) {
          Bundle bundle = new Bundle();
          bundle.putSerializable("data", items.get(hold.getAdapterPosition()));
          bundle.putString("edit_addr", DIALOGMESSAGESHOW_YES);
          intent.putExtras(bundle);
          Activity activity = (Activity) mContext;
          //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
          activity.startActivityForResult(intent, 101);
        }
        } else {
          hold.alertProgress.tryAgain(mContext, mContext.getResources().getString(R.string.pleaseCheckInternet), mContext.getResources().getString(R.string.system_error), isClicked -> {
          });
        }
      }
    });
    hold.tv_delete_address.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (Utility.isNetworkAvailable(mContext)) {
          if (items.get(hold.getAdapterPosition()) != null) {
            presenter.deleteAddress(auth, items.get(hold.getAdapterPosition()).getId(), items.get(hold.getAdapterPosition()), hold.getAdapterPosition());
          }
        } else {
          hold.alertProgress.tryAgain(mContext, mContext.getResources().getString(R.string.pleaseCheckInternet), mContext.getResources().getString(R.string.system_error), isClicked -> {
          });
        }
      }
    });
    String addressType = items.get(position).getTaggedAs();
    if (isBidding) {
      final int sdk = android.os.Build.VERSION.SDK_INT;
      if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
        hold.clContainer.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.dependents_unselection));
        hold.tv_type.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.rounded_unselection));
      } else {
        hold.clContainer.setBackground(ContextCompat.getDrawable(mContext, R.drawable.dependents_unselection));
        hold.tv_type.setBackground(ContextCompat.getDrawable(mContext, R.drawable.rounded_unselection));
      }
      if (position == selectedItem) {
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
          hold.clContainer.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.dependents_selection));
          hold.tv_type.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.rounded_selection));
        } else {
          hold.clContainer.setBackground(ContextCompat.getDrawable(mContext, R.drawable.dependents_selection));
          hold.tv_type.setBackground(ContextCompat.getDrawable(mContext, R.drawable.rounded_selection));
        }
      } else {
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
          hold.clContainer.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.dependents_unselection));
          hold.tv_type.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.rounded_unselection));
        } else {
          hold.clContainer.setBackground(ContextCompat.getDrawable(mContext, R.drawable.dependents_unselection));
          hold.tv_type.setBackground(ContextCompat.getDrawable(mContext, R.drawable.rounded_unselection));
        }
      }

      if (mContext.getResources().getString(R.string.home).equals(addressType)) {
        hold.tv_type.setText(mContext.getString(R.string.home));
      } else if (mContext.getResources().getString(R.string.office).equals(addressType)) {
        hold.tv_type.setText(mContext.getString(R.string.work));
      } else {
        hold.tv_type.setText(addressType);
      }
    } else {
      ((ViewHolderRecycler) holder).iv_address_type.setVisibility(View.VISIBLE);
      if (mContext.getResources().getString(R.string.home).equals(addressType)) {
        hold.iv_address_type.setImageResource(R.drawable.ic_home);
        hold.tv_type.setText(mContext.getString(R.string.home));
      } else if (mContext.getResources().getString(R.string.office).equals(addressType)) {
        hold.iv_address_type.setImageResource(R.drawable.ic_work);
        hold.tv_type.setText(mContext.getString(R.string.work));
      } else {
        hold.iv_address_type.setImageResource(R.drawable.ic_other_addr);
        hold.tv_type.setText(addressType);
      }
    }
    //to set Address
    String addressLine1;
    if (items.get(position).getHouseNo() != null && !items.get(position).getHouseNo().equals("")) {
      addressLine1 = items.get(position).getHouseNo() + ", " + items.get(position).getAddLine1();
    } else {
      addressLine1 = items.get(position).getAddLine1();
    }
    hold.tv_address1.setText(addressLine1);
    if (!TextUtils.isEmpty(items.get(position).getAddLine2()))
      hold.tv_address2.setText(items.get(position).getAddLine2());
    else
      hold.tv_address2.setVisibility(View.GONE);
  }

  @Override
  public int getItemCount() {
    return items == null ? 0 : items.size();
  }

  public void removeSelectedItem() {
    if (selectedItem != -1) {
      notifyItemChanged(selectedItem);
      selectedItem = -1;
      notifyItemChanged(selectedItem);
    }
  }

  public void deleteItem(int index) {
    items.remove(index);
    notifyItemRemoved(index);
    //    notifyItemRangeChanged(index, items.size());
  }

  /**
   * <h2>ViewHolder</h2>
   * This method is used to hold the views
   */
  private class ViewHolderRecycler extends RecyclerView.ViewHolder {
    ImageView iv_address_type;
    //iv_delete
    TextView tv_type, tv_address1, tv_address2;
    ImageView tv_edit_address, tv_delete_address;
    ConstraintLayout clContainer;
    AlertProgress alertProgress;

    public ViewHolderRecycler(View itemView) {
      super(itemView);
      appTypeface = AppTypeface.getInstance(mContext);
      alertProgress = new AlertProgress(mContext);
      iv_address_type = itemView.findViewById(R.id.ivAddType);
      tv_type = itemView.findViewById(R.id.tvAddType);
      tv_address1 = itemView.findViewById(R.id.tvAddLineOne);
      clContainer = itemView.findViewById(R.id.clContainer);
      tv_address2 = itemView.findViewById(R.id.tvAddLineTwo);
      tv_edit_address = itemView.findViewById(R.id.ivEdtAdd);
      tv_delete_address = itemView.findViewById(R.id.ivDelAdd);
      tv_type.setTypeface(appTypeface.getHind_regular());
      tv_address2.setTypeface(appTypeface.getHind_regular());
      tv_address1.setTypeface(appTypeface.getHind_regular());
      itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          presenter.onItemClicked(getAdapterPosition());
          if (isBidding) {
            notifyItemChanged(selectedItem);
            selectedItem = getAdapterPosition();
            notifyItemChanged(selectedItem);
          }
        }
      });
    }
  }

}
