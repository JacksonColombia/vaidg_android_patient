package adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.InCallOutCallTeleCallActivity;
import com.vaidg.model.Category;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.AutoResizeTextView;
import com.vaidg.utilities.AutofitTextView;
import com.vaidg.utilities.CircleTransform;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.Utility;
import com.utility.AlertProgress;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * <h2>SymptomSearchAdapter</h2>
 * Created by ${3Embed} on 5/10/17.
 */

public class SearchSymptomAdapter extends RecyclerView.Adapter {
    private ArrayList<Category> inComingList;
    // private OnServiceClicked context;
    private Context mContext;

    public SearchSymptomAdapter(ArrayList<Category> inComingList) {
        this.inComingList = inComingList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_list_item_v,
                parent, false);
        mContext = parent.getContext();
        return new ServicesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ServicesViewHolder viewHolder = (ServicesViewHolder) holder;
        if (inComingList.size() > 0) {
            viewHolder.serviceName.setText(inComingList.get(position).getCatName());
            viewHolder.tvSearchResult.setText(inComingList.get(position).getSearchSymptomName());
            Constants.searchSymptomSelectedID = inComingList.get(position).getSearchSymptomId();
        }else{

        }
    }

    @Override
    public int getItemCount() {
        return inComingList == null ? 0 : inComingList.size();
    }


    private void callIntent(Intent intent, Category category, Bundle bundle) {
        Constants.catId = category.getId();

        bundle.putString("CatId", category.getId());
        bundle.putDouble("MinAmount", category.getMinimumFees());
        bundle.putDouble("MaxAmount", category.getMaximumFees());
        bundle.putSerializable("BookingTypeAction", category.getBookingTypeAction());
        bundle.putSerializable("CallType", category.getCallType());
        intent.putExtras(bundle);
        //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mContext.startActivity(intent);
        ((Activity)mContext).overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
    }

    public interface OnServiceClicked {
        void onServiceClicked(String serviceName);
    }

    class ServicesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvServiceName)
        TextView serviceName;
        @BindView(R.id.tvSearchResult)
        TextView tvSearchResult;
        @BindView(R.id.ivServicePic)
        ImageView serviceImage;
        private AppTypeface appTypeface;
        private AlertProgress alertProgress;
        private Bundle bundle;

        ServicesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            appTypeface = AppTypeface.getInstance(mContext);
            alertProgress = new AlertProgress(mContext);
            serviceName.setTypeface(appTypeface.getHind_regular());
            tvSearchResult.setTypeface(appTypeface.getHind_medium());
            serviceName.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimension(R.dimen.sp_16));
            tvSearchResult.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimension(R.dimen.sp_16));
            bundle = new Bundle();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Utility.isNetworkAvailable(mContext)) {
                    HashMap<Integer, Boolean> hashMap = new HashMap<>();
                    hashMap.put(1, inComingList.get(getAdapterPosition()).getCallType().isIncall());
                    hashMap.put(2, inComingList.get(getAdapterPosition()).getCallType().isOutcall());
                    hashMap.put(3, inComingList.get(getAdapterPosition()).getCallType().isTelecall());

                    Constants.catId = inComingList.get(getAdapterPosition()).getId();
                    Constants.catName = inComingList.get(getAdapterPosition()).getCatName();
                    Constants.bookingModel = inComingList.get(getAdapterPosition()).getBillingModel();
                    Constants.serviceType = inComingList.get(getAdapterPosition()).getServiceType();
                    Constants.minFee = inComingList.get(getAdapterPosition()).getMinimumFeesForConsultancy();
                    Constants.maxFee = inComingList.get(getAdapterPosition()).getMaximumFeesForConsultancy();
                    Constants.visitFee = 0;
                    Constants.proId = "0";

                    Intent intent = new Intent(mContext, InCallOutCallTeleCallActivity.class);//BookingType
                    //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    callIntent(intent, inComingList.get(getAdapterPosition()), bundle);
                    } else {
                        alertProgress.tryAgain(mContext, mContext.getResources().getString(R.string.pleaseCheckInternet), mContext.getResources().getString(R.string.system_error), isClicked -> {

                        });
                    }
                    }
            });
        }

    }
}
