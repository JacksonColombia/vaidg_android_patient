package adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.questionselection.QuestionSelectionContract;
import com.vaidg.utilities.Utility;
import com.pojo.QuestionVideo;
import java.util.ArrayList;


/**
 * Created by Ali on 7/24/2018.
 */
public class VideoSelctionAdapterGrid extends RecyclerView.Adapter {

  private Context mContext;
  private ArrayList<QuestionVideo> questionImages = new ArrayList<>();
  private QuestionSelectionContract.QuestionSelectionView questionSelectionView;
  //private OnItemonToTakeImageClickListener onItemonToTakeImageClickListener;
  private boolean isImageInfo = false;
  private Activity mActivity;
  private LayoutInflater mLayoutInflater;
  private MediaController mediaControls;

  /*
      public ImageTypeAdapterGrid(Context mContext, ArrayList<QuestionVideo> questionImages,
      BiddingContractor.BiddingContractView biddingContractor) {
          this.mContext = mContext;
          this.questionImages = questionImages;
          this.biddingContractor = biddingContractor;
      }
  */
  public VideoSelctionAdapterGrid( ArrayList<QuestionVideo> questionImages/*,
      QuestionSelectionContract.QuestionSelectionView questionSelectionView*/) {
    this.questionImages = questionImages;
   // this.questionSelectionView = questionSelectionView;
    }

  /**
   * Creates a simple dialog box with as many buttons as you want
   *
   * @param context     The context of the dialog
   * @param cancelable  whether the dialog can be closed/cancelled by the user
   * @param layoutResID the resource id of the layout you want within the dialog
   * @return the dialog
   */
  public static Dialog getBaseDialog(Context context, boolean cancelable, int layoutResID) {
    Dialog dialog = new Dialog(context);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setCancelable(cancelable);
    dialog.setCanceledOnTouchOutside(cancelable);
    dialog.setContentView(layoutResID);

    return dialog;
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View itemView;
    mActivity = (Activity) parent.getContext();
    mContext = parent.getContext();
    mLayoutInflater = LayoutInflater.from(parent.getContext());

    if (viewType == 0) {
      itemView = mLayoutInflater.inflate(R.layout.video_taken_display, parent, false);
      return new ImageViewHolder(itemView);
    } else {
      itemView = mLayoutInflater.inflate(R.layout.image_to_take, parent, false);
      return new ImageViewTakesHold(itemView);
    }

  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    Context context = holder.itemView.getContext();
    ImageViewHolder imageHold = (ImageViewHolder) holder;
    /*Uri video = Uri.parse(questionImages.get(position).getImage());*/

    if (questionImages.size() > 0) {

      if (questionImages.get(position).isImage()) {
        if (isImageInfo) {
          /***get clicked view and play video url at this position**/
        /*  imageHold.videoView.setVideoPath(questionImages.get(position).getImage());
          MediaController mediaController = new MediaController(mContext);
          mediaController.setAnchorView(imageHold.videoView);
          imageHold.videoView.setMediaController(mediaController);
          imageHold.videoView.start();*/

          playallvideo(questionImages, imageHold.videoView, position);

        } else {

          playallvideo(questionImages, imageHold.videoView, position);
        }

        if (!isImageInfo) {
          imageHold.ivDelete.setVisibility(View.INVISIBLE);
        }

      } else {
        if (questionImages.size() > 1) {
          // ImageViewTakesHold imageHold = (ImageViewTakesHold) holder;
        }
      }
    }

  }

  private void playallvideo(ArrayList<QuestionVideo> questionImages,
                            ImageView videoView, int position) {

   /* Bitmap thumb = ThumbnailUtils.createVideoThumbnail(questionImages.get(position).getImage(),
        MediaStore.Images.Thumbnails.MINI_KIND);
    BitmapDrawable bitmapDrawable = new BitmapDrawable(mContext.getResources(), thumb);
    Glide.with(mContext)
            .load(bitmapDrawable)
            .into(videoView);*/
    RequestOptions requestOptions = new RequestOptions();
    requestOptions.isMemoryCacheable();
    Glide.with(mContext).setDefaultRequestOptions(requestOptions).load(questionImages.get(position).getImage()).into(videoView);

    videoView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
       // videoView.setBackground(null);
        showAlert(questionImages.get(position).getImage());

      }
    });
  }

  public void showAlert(String questionImages) {
    getVideoDialog(mContext, questionImages, true);
  }

  public void getVideoDialog(Context context, String videoLocation, boolean autoplay) {
    final Dialog dialog = getBaseDialog(context, true, R.layout.dialog);

    ((Activity) context).getWindow().setFormat(PixelFormat.TRANSLUCENT);
    final VideoView videoHolder = (VideoView) dialog.findViewById(R.id.videoDialog);
    ImageView image = dialog.findViewById(R.id.imageDialog);
    videoHolder.setVideoURI(Uri.parse(videoLocation));
    //videoHolder.setRotation(90);
    MediaController mediaController = new MediaController(context);
    videoHolder.setMediaController(mediaController);
    mediaController.setAnchorView(videoHolder);
    videoHolder.requestFocus();
    if (autoplay) {
      videoHolder.start();
    }
    dialog.show();
    videoHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

      @Override
      public void onCompletion(MediaPlayer mp) {
        dialog.dismiss();
        notifyDataSetChanged();
      }
    });


    // if decline button is clicked, close the custom dialog
    image.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // Close dialog
        dialog.dismiss();
        notifyDataSetChanged();
      }
    });
    // return dialog;
  }

  @Override
  public int getItemCount() {

    return questionImages == null ? 0 : questionImages.size();
  }


  @Override
  public int getItemViewType(int position) {
        /*if(questionImages.get(position).isImage())
            return  0;
        else
            return 0;*/
    return 0;
  }

  public void onIsQuestionInfo(boolean isInfo) {
    isImageInfo = isInfo;
  }

  private class ImageViewHolder extends RecyclerView.ViewHolder {
    private ImageView videoView;
    private ImageView imageViewTaken;
    private boolean isContinuously = false;
    private ImageView ivDelete;

    ImageViewHolder(View itemView) {
      super(itemView);
      ivDelete = itemView.findViewById(R.id.ivDelete);
      imageViewTaken = itemView.findViewById(R.id.imageViewTaken);
      videoView = itemView.findViewById(R.id.frame_video_view);

      ivDelete.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          Log.i("ABCDEFG", "onClick: Delete");
          questionSelectionView.deleteVideoPhoto(getAdapterPosition(),
              questionImages.get(getAdapterPosition()).getImagePostion());

        }
      });
    }
  }

  private class ImageViewTakesHold extends RecyclerView.ViewHolder {
    private ImageView imageToTake;

    ImageViewTakesHold(View itemView) {
      super(itemView);
      imageToTake = itemView.findViewById(R.id.imageToTake);

      itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          // Log.i("ABCDEFG", "onClick: onToTakeImage");

          //symptomQuestionsView.onToTakeImage(getAdapterPosition());
        }
      });

    }
        /*public interface OnItemonToTakeImageClickListener {
            void onItemClick(View view, int layoutPosition, ArrayList<SymptomResponsePojo
            .SymptomCategory.QuestionArr.PreDefined> preDefineds, int pos, int position);

            void onItemLongClick(View view, int position);
        }

        // for both short and long click
        public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
            this.mItemClickListener = mItemClickListener;
        }*/
  }
}



