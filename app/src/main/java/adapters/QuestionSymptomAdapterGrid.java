package adapters;

import android.content.Context;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.vaidg.R;
import com.vaidg.utilities.AppTypeface;
import com.pojo.BidQuestionAnswer;
import com.pojo.QuestionImage;
import com.pojo.QuestionVideo;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Ali on 7/24/2018.
 */
public class QuestionSymptomAdapterGrid extends RecyclerView.Adapter {

  private Context mContext;
  private ArrayList<BidQuestionAnswer> questionAndAnswer;
  private int colour = 0;
  private ArrayList<QuestionImage> questionImages = new ArrayList<>();
  private ArrayList<QuestionVideo> questionVideos = new ArrayList<>();

  public QuestionSymptomAdapterGrid(ArrayList<BidQuestionAnswer> bidQuestionAnswers) {
    this.questionAndAnswer = bidQuestionAnswers;
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.job_question_answer, parent, false);
    mContext = parent.getContext();
    return new ImageViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

    int qType = questionAndAnswer.get(position).getQuestionType();
    Log.w("TAG", "onBindViewHolder: "+questionAndAnswer.get(position).getQuestion());
    Log.w("TAG", "onBindViewHolder: "+questionAndAnswer.get(position).getSymptomName());
    switch (qType) {
      case 12:
     // case 13:
        ((ImageViewHolder) holder).recyclerViewQuestionImage.setVisibility(View.VISIBLE);
        ((ImageViewHolder) holder).tvJobAnswers.setVisibility(View.GONE);
      //  GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
        ((ImageViewHolder) holder).tvJobQuestions.setText(questionAndAnswer.get(position).getQuestion());
      /*  String[] splitImage = questionAndAnswer.get(position).getAnswer().split(",");
        if (splitImage.length > 0) {
          ArrayList<QuestionImage> questionImages = new ArrayList<>();
          for (int i = 0; i < splitImage.length; i++) {
            questionImages.add(new QuestionImage(splitImage[i], true, i, false));
          }*/
        questionImages.add(new QuestionImage(questionAndAnswer.get(position).getAnswer(), true, position, false));
        ImageSelctionAdapterGrid questionAdapterGrid = new ImageSelctionAdapterGrid(questionImages);
          questionAdapterGrid.onIsQuestionInfo(true);
        ((ImageViewHolder) holder).recyclerViewQuestionImage.setHasFixedSize(true);
          ((ImageViewHolder) holder).recyclerViewQuestionImage.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false));
          ((ImageViewHolder) holder).recyclerViewQuestionImage.setAdapter(questionAdapterGrid);
        break;
      case 13:
        ((ImageViewHolder) holder).recyclerViewQuestionImage.setVisibility(View.VISIBLE);
        ((ImageViewHolder) holder).tvJobAnswers.setVisibility(View.GONE);
      //  GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
        ((ImageViewHolder) holder).tvJobQuestions.setText(questionAndAnswer.get(position).getQuestion());
      /*  String[] splitImage = questionAndAnswer.get(position).getAnswer().split(",");
        if (splitImage.length > 0) {
          ArrayList<QuestionImage> questionImages = new ArrayList<>();
          for (int i = 0; i < splitImage.length; i++) {
            questionImages.add(new QuestionImage(splitImage[i], true, i, false));
          }*/
        questionVideos.add(new QuestionVideo(questionAndAnswer.get(position).getAnswer(), true, position, false));
        VideoSelctionAdapterGrid videoSelctionAdapterGrid = new VideoSelctionAdapterGrid(questionVideos);
        videoSelctionAdapterGrid.onIsQuestionInfo(true);
        ((ImageViewHolder) holder).recyclerViewQuestionImage.setHasFixedSize(true);
          ((ImageViewHolder) holder).recyclerViewQuestionImage.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false));
          ((ImageViewHolder) holder).recyclerViewQuestionImage.setAdapter(videoSelctionAdapterGrid);
        break;
      default:
        ((ImageViewHolder) holder).recyclerViewQuestionImage.setVisibility(View.GONE);
        ((ImageViewHolder) holder).tvJobAnswers.setVisibility(View.VISIBLE);
        ((ImageViewHolder) holder).tvJobQuestions.setText(questionAndAnswer.get(position).getQuestion());
        ((ImageViewHolder) holder).tvJobAnswers.setText(questionAndAnswer.get(position).getAnswer());
/*
            if (qType != 4 || qType != 2 || qType != 3) {
              llBookingFor.setVisibility(View.VISIBLE);
              viewIsVisible.setVisibility(View.VISIBLE);
              viewPayment.setVisibility(View.VISIBLE);
              lljobPaymentInfo.setVisibility(View.VISIBLE);
              viewAddress.setVisibility(View.VISIBLE);
            }
*/
        break;
    }

    if (getItemCount() == 0)
      ((ImageViewHolder) holder).viewJobQA.setVisibility(View.GONE);
  }

  @Override
  public int getItemCount() {
    return questionAndAnswer == null ? 0 : questionAndAnswer.size();
  }


  private class ImageViewHolder extends RecyclerView.ViewHolder {
    TextView tvJobQuestion, tvJobQuestions, tvJobAnswer, tvJobAnswers;
    LinearLayout llJobDetailsQA;
    RecyclerView recyclerViewQuestionImage;
    RelativeLayout rlQuestion;
    View viewJobQA;
    AppTypeface appTypeface;

    ImageViewHolder(View itemView) {
      super(itemView);
      tvJobQuestions = itemView.findViewById(R.id.tvJobQuestions);
      tvJobAnswers = itemView.findViewById(R.id.tvJobAnswers);
      viewJobQA = itemView.findViewById(R.id.viewJobQA);
      tvJobQuestion = itemView.findViewById(R.id.tvJobQuestion);
      tvJobAnswer = itemView.findViewById(R.id.tvJobAnswer);
      llJobDetailsQA = itemView.findViewById(R.id.llJobDetailsQA);
      recyclerViewQuestionImage = itemView.findViewById(R.id.recyclerViewQuestionImage);
      rlQuestion = itemView.findViewById(R.id.rlQuestion);
      //colour = Utility.getColor(mContext, R.color.searchBackground);
      //rlQuestion.setBackgroundColor(colour);
      appTypeface = AppTypeface.getInstance(mContext);
      tvJobQuestions.setTypeface(appTypeface.getHind_medium());
      tvJobAnswers.setTypeface(appTypeface.getHind_regular());
      tvJobQuestion.setTypeface(appTypeface.getHind_medium());
      tvJobAnswer.setTypeface(appTypeface.getHind_regular());

      tvJobAnswers.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_15));
      tvJobQuestions.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_13));

    }
  }

}
