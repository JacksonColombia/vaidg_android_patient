package adapters;


import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.dependent.DependentContract;
import com.vaidg.incalloutcalltelecall.dependent.model.DependentsData;
import com.vaidg.model.CategoryCity;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.Utility;
import com.utility.PicassoTrustAll;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * <h2>DependentAdapter</h2>
 *
 * <p>This is an inner class of adapter type, to integrate the dependents list on screen.</p>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */

public class CityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    
    private Context mContext;
    private ArrayList<CategoryCity> categoryCities = new ArrayList<>();
    private OnItemClickListener listener;
    private String cityId;


    public CityAdapter(ArrayList<CategoryCity> categoryCities, String cityId, OnItemClickListener listener) {
        this.categoryCities = categoryCities;
        this.cityId = cityId;
        this.listener = listener;
    }


    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_city,
                viewGroup, false);
        mContext = viewGroup.getContext();
        return new SingleViewHolder(view);
    }
    
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        SingleViewHolder singleViewHolder = (SingleViewHolder) viewHolder;
        singleViewHolder.tvCityName.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
        singleViewHolder.tvCityName.setText(categoryCities.get(position).getCity());
        if(categoryCities.get(position).getId().equals(cityId))
        {
            singleViewHolder.tvCityName.setTextColor(Utility.getColor(mContext,R.color.colorAccent));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                singleViewHolder.ivCity.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_check, mContext.getTheme()));
            } else {
                singleViewHolder.ivCity.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_check));
            }
        }else{
            singleViewHolder.tvCityName.setTextColor(Utility.getColor(mContext,R.color.black));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                singleViewHolder.ivCity.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_unselected, mContext.getTheme()));
            } else {
                singleViewHolder.ivCity.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_unselected));
            }

        }
    }



    @Override
    public int getItemCount() {
        return categoryCities == null ? 0 : categoryCities.size();
    }
    
    class SingleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        
        @BindView(R.id.lvLinear)
        LinearLayout lvLinear;
        @BindView(R.id.tvCityName)
        TextView tvCityName;
        @BindView(R.id.ivCity)
        ImageView ivCity;
        private AppTypeface mAppTypeface;
        
        SingleViewHolder(@NonNull View convertView) {
            super(convertView);
            ButterKnife.bind(this, convertView);
            mAppTypeface = AppTypeface.getInstance(mContext);
            itemView.setOnClickListener(this);
            tvCityName.setTypeface(mAppTypeface.getHind_medium());
        }

        @Override
        public void onClick(View view) {
                if(listener != null)
                {
                    listener.onItemClick(categoryCities.get(getAdapterPosition()));
                }
         }
    }

    public interface OnItemClickListener {
        void onItemClick(CategoryCity categoryCity);

    }

    // method for filtering our recyclerview items.
    public void filterList(ArrayList<CategoryCity> filterllist) {
        // below line is to add our filtered
        // list in our course array list.
        categoryCities = filterllist;
        // below line is to notify our adapter
        // as change in recycler view data.
        notifyDataSetChanged();
    }
}
