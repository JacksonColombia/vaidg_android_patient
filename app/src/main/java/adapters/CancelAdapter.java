package adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vaidg.R;
import com.vaidg.confirmbookactivity.ConfirmBookingContractImpl;
import com.vaidg.jobDetailsStatus.JobDetailsOnTheWayContract;
import com.vaidg.utilities.AppTypeface;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.pojo.CancelReasonPojo;

/**
 * <h2>CancelAdapter</h2>
 * Created by Ali on 3/14/2018.
 */
public class CancelAdapter extends RecyclerView.Adapter
{
    private Context mContext;
    private ArrayList<CancelReasonPojo.CancelReasonData> data;
    private int selectedPosition = -1;
    private JobDetailsOnTheWayContract.CancelBooking cancelInterface;

    public CancelAdapter(ArrayList<CancelReasonPojo.CancelReasonData> data, JobDetailsOnTheWayContract.CancelBooking cancelInterface)
    {
        this.data = data;
        this.cancelInterface = cancelInterface;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cancel_bookingadapter,parent,false);
        mContext = parent.getContext();
        return new ViewCancelHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewCancelHolder vHolder = (ViewCancelHolder) holder;

        if(selectedPosition == position)
        {
            vHolder.ivCheckSelector.setSelected(true);
            vHolder.ivCheckSelector.setImageResource(R.drawable.ic_check);
        }else
        {
            vHolder.ivCheckSelector.setSelected(false);
            vHolder.ivCheckSelector.setImageResource(R.drawable.ic_unselected);
        }
        vHolder.tvForCancel.setText(data.get(position).getReason());

    }

    @Override
    public int getItemCount()
    {
        return data == null ? 0 : data.size();
    }

    class ViewCancelHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.tvForCancel)TextView tvForCancel;
        @BindView(R.id.ivCheckSelector)ImageView ivCheckSelector;
        private AppTypeface appTypeface;
        public ViewCancelHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            appTypeface = AppTypeface.getInstance(mContext);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    notifyItemChanged(selectedPosition);
                    selectedPosition = getAdapterPosition();
                    notifyItemChanged(selectedPosition);

                    if(cancelInterface != null)
                    cancelInterface.onSelectedReason(data.get(selectedPosition).getRes_id());
                }
            });
        }
    }
}
