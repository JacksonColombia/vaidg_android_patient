package adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterInside;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.vaidg.R;
import com.vaidg.home.ServiceFragContract;
import com.vaidg.model.CatDataArray;
import com.vaidg.model.Category;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Utility;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Ali on 7/31/2018.
 */
public class NewHomeServiceAdapter extends RecyclerView.Adapter
{
    private Context mContext;
    private ArrayList<CatDataArray>catArray;
    private ServiceFragContract.ServiceView serviceView;
    private int selectedPosition = -1;

    public NewHomeServiceAdapter(ArrayList<CatDataArray> catArray, ServiceFragContract.ServiceView serviceView) {
        this.catArray = catArray;
        this.serviceView = serviceView;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_home_category,parent,false);
        mContext = parent.getContext();
        return new NewHomeHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        NewHomeHolder hold = (NewHomeHolder) holder;
        hold.tvHomeCatName.setText(catArray.get(position).getGroupName());
        hold.tvHomeSubCatName.setText(catArray.get(position).getDescription());
        if(!"".equals(catArray.get(position).getSelectImage()))
        {

            Glide.with(mContext)
                    .load(catArray.get(position).getSelectImage())
                    .apply(new RequestOptions().transforms(new CenterInside(), new RoundedCorners(10)))
                    .into(hold.imageBanner);
        }
        hold.categoryList.clear();
        hold.categoryList.addAll(catArray.get(position).getCategory());
        hold.trendingAdapter.notifyDataSetChanged();

    }



    private Palette createPaletteSync(Bitmap bitmap)
    {

        return Palette.from(bitmap).generate();

    }

    @Override
    public int getItemCount() {
        return catArray == null ? 0 : catArray.size();
    }

    class NewHomeHolder extends RecyclerView.ViewHolder
    {

        private ImageView ivOpenCategory,imageBanner;
        private TextView tvHomeCatName,tvHomeSubCatName;
        //  private View viewCategory;
        private AppTypeface appTypeface;
        private RecyclerView recyclerViewHomeCat;
        private NewHomeSubCategory trendingAdapter;
        private LinearLayout llHomeCategoryService,llLIstNewHOme;

        ArrayList<Category> categoryList = new ArrayList<>();


        NewHomeHolder(View itemView) {
            super(itemView);
            appTypeface = AppTypeface.getInstance(mContext);

            ivOpenCategory = itemView.findViewById(R.id.ivOpenCategory);
            tvHomeCatName = itemView.findViewById(R.id.tvHomeCatName);
            tvHomeSubCatName = itemView.findViewById(R.id.tvHomeSubCatName);
            recyclerViewHomeCat = itemView.findViewById(R.id.recyclerViewHomeCat);
            llHomeCategoryService = itemView.findViewById(R.id.llHomeCategoryService);
            imageBanner = itemView.findViewById(R.id.imageBanner);
            llLIstNewHOme = itemView.findViewById(R.id.llLIstNewHOme);

            GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext,3);
            trendingAdapter = new NewHomeSubCategory(categoryList,serviceView);
            final LayoutAnimationController controller =
                    AnimationUtils.loadLayoutAnimation(mContext, R.anim.layout_animation_up_to_down);

            recyclerViewHomeCat.setLayoutAnimation(controller);
            recyclerViewHomeCat.setLayoutManager(gridLayoutManager);
            recyclerViewHomeCat.setAdapter(trendingAdapter);
            recyclerViewHomeCat.scheduleLayoutAnimation();

            tvHomeCatName.setTypeface(appTypeface.getHind_bold());
            tvHomeSubCatName.setTypeface(appTypeface.getHind_regular());


            llHomeCategoryService.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    catArray.get(getAdapterPosition()).setExpanded(!catArray.get(getAdapterPosition()).isExpanded());

                    notifyItemChanged(selectedPosition);
                        selectedPosition = getAdapterPosition();

                    notifyItemChanged(selectedPosition);
                }
            });
        }
    }


}
