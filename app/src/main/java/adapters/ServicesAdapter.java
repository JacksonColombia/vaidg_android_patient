package adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.InCallOutCallTeleCallActivity;
import com.vaidg.model.Category;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.AutoResizeTextView;
import com.vaidg.utilities.AutofitTextView;
import com.vaidg.utilities.CircleTransform;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.Utility;
import com.utility.AlertProgress;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * <h2>SymptomSearchAdapter</h2>
 * Created by ${3Embed} on 5/10/17.
 */

public class ServicesAdapter extends RecyclerView.Adapter {
    private ArrayList<Category> inComingList;
    // private OnServiceClicked context;
    private Context mContext;
    private Activity mActivity;
    private boolean isVertical = false;
    private boolean inCall = false;
    private boolean outCall = false;
    private boolean teleCall = false;

    public ServicesAdapter(ArrayList<Category> inComingList,
                           boolean isVertical, boolean inCall, boolean outCall, boolean teleCall) {//OnServiceClicked ui,

        this.inComingList = inComingList;
        // this.context= ui;
        this.isVertical = isVertical;
        this.inCall = inCall;
        this.outCall = outCall;
        this.teleCall = teleCall;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_symptomlist,
                parent, false);
        mContext = parent.getContext();
        mActivity = (Activity) parent.getContext();
        return new ServicesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ServicesViewHolder viewHolder = (ServicesViewHolder) holder;
        if (inComingList.size() > 0) {
            viewHolder.serviceName.setText(inComingList.get(position).getCatName());

            if (inComingList.get(position).getBannerImageApp() != null) {
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.profile_price_bg)
                        .error(R.drawable.profile_price_bg)
                        .priority(Priority.HIGH);
                Glide.with(mContext)
                        .load(inComingList.get(position).getBannerImageApp())
                        .apply(options)
                        .apply(new RequestOptions().transform(new CircleTransform(mContext)))
                        .into(viewHolder.serviceImage);

            }
        }else{

        }
    }

    @Override
    public int getItemCount() {
        return inComingList == null ? 0 : inComingList.size();
    }


    private void callIntent(Intent intent, Category category, Bundle bundle) {
        Constants.catId = category.getId();
        if(inCall)
        {
            bundle.putBoolean("inCall", inCall);
            bundle.putBoolean("outCall", outCall);
            bundle.putBoolean("teleCall", teleCall);
        }else if(outCall)
        {
            bundle.putBoolean("inCall", inCall);
            bundle.putBoolean("outCall", outCall);
            bundle.putBoolean("teleCall", teleCall);
        }else if(teleCall){
            bundle.putBoolean("inCall", inCall);
            bundle.putBoolean("outCall", outCall);
            bundle.putBoolean("teleCall", teleCall);
        }else{
            bundle.putBoolean("inCall", inCall);
            bundle.putBoolean("outCall", outCall);
            bundle.putBoolean("teleCall", teleCall);
        }
        bundle.putString("CatId", category.getId());
        bundle.putDouble("MinAmount", category.getMinimumFees());
        bundle.putDouble("MaxAmount", category.getMaximumFees());
        bundle.putSerializable("BookingTypeAction", category.getBookingTypeAction());
        bundle.putSerializable("CallType", category.getCallType());
        intent.putExtras(bundle);
        //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        mContext.startActivity(intent);
        mActivity.overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
    }

    public interface OnServiceClicked {
        void onServiceClicked(String serviceName);
    }

    class ServicesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvServiceName)
        TextView serviceName;
        @BindView(R.id.relativeLayout)
        RelativeLayout relativeLayout;
        @BindView(R.id.ivServicePic)
        ImageView serviceImage;
        private AppTypeface appTypeface;
        private Bundle bundle;
        private AlertProgress alertProgress;

        ServicesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            appTypeface = AppTypeface.getInstance(mContext);
            alertProgress = new AlertProgress(mContext);
            serviceName.setTypeface(appTypeface.getHind_medium());
            serviceName.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimension(R.dimen.sp_13));
            relativeLayout.setBackgroundColor(mContext.getResources().getColor(R.color.lightPink));
            bundle = new Bundle();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Utility.isNetworkAvailable(mContext)) {
                        HashMap<Integer, Boolean> hashMap = new HashMap<>();
                        hashMap.put(1, inComingList.get(getAdapterPosition()).getCallType().isIncall());
                        hashMap.put(2, inComingList.get(getAdapterPosition()).getCallType().isOutcall());
                        hashMap.put(3, inComingList.get(getAdapterPosition()).getCallType().isTelecall());

                        Constants.catId = inComingList.get(getAdapterPosition()).getId();
                        Constants.catName = inComingList.get(getAdapterPosition()).getCatName();
                        Constants.bookingModel = inComingList.get(getAdapterPosition()).getBillingModel();
                        Constants.serviceType = inComingList.get(getAdapterPosition()).getServiceType();
                        Constants.minFee = inComingList.get(getAdapterPosition()).getMinimumFeesForConsultancy();
                        Constants.maxFee = inComingList.get(getAdapterPosition()).getMaximumFeesForConsultancy();
                        Constants.visitFee = 0;
                        Constants.proId = "0";

                        Intent intent = new Intent(mActivity, InCallOutCallTeleCallActivity.class);//BookingType
                        //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        if (inCall) {
                            Constants.callTypeInOutTele = 1;
                            Constants.bookingType = 2;
                            Constants.POSCOUNT = 3;
                            callIntent(intent, inComingList.get(getAdapterPosition()), bundle);
                        } else if (outCall) {
                            Constants.callTypeInOutTele = 2;
                            Constants.bookingType = 1;
                            Constants.POSCOUNT = 4;
                            callIntent(intent, inComingList.get(getAdapterPosition()), bundle);
                        } else if (teleCall) {
                            Constants.callTypeInOutTele = 3;
                            Constants.bookingType = 2;
                            Constants.POSCOUNT = 3;
                            callIntent(intent, inComingList.get(getAdapterPosition()), bundle);
                        } else {
                            callIntent(intent, inComingList.get(getAdapterPosition()), bundle);
                        }
                    } else {
                        alertProgress.tryAgain(mContext, mContext.getResources().getString(R.string.pleaseCheckInternet), mContext.getResources().getString(R.string.system_error), isClicked -> {

                        });
                    }
                }
            });
        }

    }
}
