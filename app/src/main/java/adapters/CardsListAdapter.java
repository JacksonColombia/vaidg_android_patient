package adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.appscrip.stripe.AccountsDelegate;
import com.appscrip.stripe.UserAccounts;
import com.vaidg.R;
import com.vaidg.model.card.DeleteCard;
import com.vaidg.model.payment_method.CardDetail;
import com.vaidg.payment_edit_card.CardEditActivity;
import com.vaidg.payment_method.PaymentMethodActivity;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.Utility;
import com.utility.AlertProgress;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * @author Pramod
 * @since 18-01-2018.
 */

public class CardsListAdapter extends BaseAdapter {
    private Context mContext;
    private List<CardDetail> cardDetails;

    public CardsListAdapter(List<CardDetail> items) {
       // super(resourceId,items);
        this.cardDetails = items;
    }

    @Override
    public int getCount() {
        return cardDetails == null ? 0 : cardDetails.size();
    }

    @Override
    public Object getItem(int i) {
        return cardDetails.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        mContext = parent.getContext();
         CardDetail rowItem = cardDetails.get(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder holder; // view lookup cache stored in tag
        final View result;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_card_list, parent,false);
            holder.tv_payment_card_number = convertView.findViewById(R.id.tv_payment_card_number);
            holder.tv_payment_card_exp_month_year = convertView.findViewById(R.id.tv_payment_card_exp_month_year);
            holder.iv_payment_card = convertView.findViewById(R.id.iv_payment_card);
            holder.iv_payment_tick = convertView.findViewById(R.id.iv_payment_tick);
            result=convertView;
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
            result = convertView;
        }
        //to set the image of card based on the card type
        holder.iv_payment_card.setImageBitmap(Utility.setCreditCardLogo(rowItem != null ? rowItem.getBrand() : "", mContext));
        //to set the card number
        holder.tv_payment_card_number.setText("**** **** **** " + (rowItem != null ? rowItem.getLast4() : ""));
        int month = rowItem != null ? rowItem.getExpMonth() : 0;
        int year = rowItem != null ? rowItem.getExpYear() : 0;
        holder.tv_payment_card_exp_month_year.setText(month+"/"+year);
        Log.w("getView", "getView: " + (rowItem != null ? rowItem.getBrand() : ""));
        holder.iv_payment_tick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext instanceof PaymentMethodActivity) {
                    ((PaymentMethodActivity)mContext).deleteCard(rowItem != null ? rowItem.getId() : null);
                }
            }
        });
        //to show/hide the tick depend on the default card/non default card
     /*   if (rowItem != null && rowItem.isIsDefault())
            holder.iv_payment_tick.setVisibility(View.VISIBLE);
        else
            holder.iv_payment_tick.setVisibility(View.GONE);
*/
        return convertView;
    }

    /**
     * <h2>ViewHolder</h2>
     * This method is used to hold the views
     */
    private class ViewHolder {
        ImageView iv_payment_card, iv_payment_tick;
        TextView tv_payment_card_number,tv_payment_card_exp_month_year;
    }
}
