package com.mqtt;

/**
 * <h2>MqttEvents</h2>
 * Created by ALi on 27/09/17.
 */


public enum MqttEvents {


    Provider("provider"),
    JobStatus("jobStatus"),
    LiveTrack("liveTrack"),
    Message("message"),
    Calls("Calls"),
    CallsAvailability("CallsAvailability"),
    PresenceTopic("PresenceTopic"),
    WillTopic("lastWill"),
    Call("call"),
    GoogleMapKey("googleMapKey");
    public String value;

    MqttEvents(String value) {
        this.value = value;
    }


}