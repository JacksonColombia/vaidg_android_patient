package com.utility;

/**
 * <h2>DialogInterfaceListner</h2>
 * Created by Ali on 8/10/2017.
 */

public interface DialogInterfaceListner
{
    void dialogClick(boolean isClicked);
}
