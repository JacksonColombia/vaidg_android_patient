package com.utility;



import android.content.Intent;
import android.util.Log;
import com.vaidg.utilities.LSPApplication;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.vaidg.utilities.SessionManager;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import org.jetbrains.annotations.NotNull;

/**
 * <h2>MyFirebaseInstanceIDService</h2>
 * Created by Ali on 7/11/16.
 */
public class MyFirebaseInstanceIDService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseIIDService";
   // private SharedPrefs sharedPrefs;
    SessionManager manager;

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onNewToken(@NotNull String s) {
      super.onNewToken(s);
    //  Log.e("NEW_TOKEN", refreshedToken);
      // Get updated InstanceID token.
      //   sharedPrefs = new SharedPrefs(this);
      manager = SessionManager.getInstance(this);
      FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
        @Override
        public void onSuccess(InstanceIdResult instanceIdResult) {
          String refreshedToken = instanceIdResult.getToken();
          Log.d(TAG, "Refreshed token: " + refreshedToken);

          // If you want to send messages to this application instance or
          // manage this apps subscriptions on the server side, send the
          // Instance ID token to your app server.
          sendRegistrationToServer(refreshedToken);

        }
      });

      // Get updated InstanceID token.
      // If you want to send messages to this application instance or
      // manage this apps subscriptions on the server side, send the
      // Instance ID token to your app server.

      //TODO: Send Token to Server
    }

  @Override
  public void onMessageReceived(RemoteMessage remoteMessage) {
    // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
    Log.d(TAG, "From: " + remoteMessage.getFrom());
    // Check if message contains a data payload.
  }
 /*   @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
     //   sharedPrefs = new SharedPrefs(this);
        manager = SessionManager.getInstance(this);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }*/

    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token)
    {
        // TODO: Implement this method to send token to your app server.
        manager.setRegistrationId(token);
      //  Log.i("Regist", "GCMTOKEN: " + sharedPrefs.getRegistrationId());

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "IDServiceonDestroy: ");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e(TAG, "IDServiceonTaskRemoved: " );
        //Code here
        stopSelf();
    }
}
