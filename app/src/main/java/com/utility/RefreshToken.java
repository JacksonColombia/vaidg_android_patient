package com.utility;

import android.util.Log;

import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.utilities.Constants.DATA;
import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;

/**
 * Created by murashid on 16-Oct-17.
 * <h2>RefreshToken</h2>
 * wrapper class for refreshing token
 */

public class RefreshToken {
    private static String TAG = "RefreshToken";

    /**
     * method for calling api for  refresh token
     *
     * @param token date token
     * @param imple RefreshTokenImple success , failure callback
     */
    private static RefreshTokenImple impl;
    private static String tokens = "";
    private static String refreshtokens = "";
    private static LSPServices lspService;

    public static void onRefreshToken(final String token, LSPServices lspServices, final RefreshTokenImple imple) {
        impl = imple;
        tokens = token;
        lspService = lspServices;
        RefreshToken refreshToken = new RefreshToken();
        refreshToken.refreshToken();
    }


    public static void onRefreshToken(final String token, final String refreshtoken, LSPServices lspServices, final RefreshTokenImple imple) {
        impl = imple;
        tokens = token;
        refreshtokens = refreshtoken;
        lspService = lspServices;
        RefreshToken refreshToken = new RefreshToken();
        refreshToken.refreshToken();
    }

    private void refreshToken() {

        Observable<Response<ResponseBody>> observable = lspService.getAccessToken(tokens, refreshtokens, Constants.selLang, PLATFORM_ANDROID);
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int responseCode = responseBodyResponse.code();
                        JSONObject jsonObject;
                        Log.d(TAG, "onNextaccessToken: " + responseBodyResponse + " responseCode " + responseCode + "   ");

                        try {
                            String responseBody = responseBodyResponse.body() != null
                                    ? responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;

                            switch (responseCode) {
                                case Constants.SUCCESS_RESPONSE:
                                    if (responseBody != null && !responseBody.isEmpty()) {
                                        jsonObject = new JSONObject(responseBody);
                                        if ( jsonObject.getJSONObject(DATA) != null ) {
                                            JSONObject json = jsonObject.getJSONObject(DATA);
                                            if (json.getString("accessToken") != null  && !json.getString("accessToken").isEmpty()) {
                                                String accessToken = json.getString("accessToken");
                                                Log.d(TAG, "onNextaccessToken: " + responseBody + " responseCode " + responseCode + "   " + accessToken);
                                                impl.onSuccessRefreshToken(accessToken);
                                            }
                                        }
                                    }
                                    break;
                                case Constants.SESSION_LOGOUT:
                                    if (errorBody != null && !errorBody.isEmpty()) {
                                        jsonObject = new JSONObject(errorBody);
                                        if (jsonObject.getString(MESSAGE) != null && !jsonObject.getString(MESSAGE).isEmpty()) {
                                            impl.sessionExpired(jsonObject.getString(MESSAGE));
                                        }
                                    }
                                    break;
                                default:
                                    impl.onFailureRefreshToken();
                                    break;
                            }

                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    /**
     * RefreshTokenImple callback for failure and succes refresh token
     */
    public interface RefreshTokenImple {
        void onSuccessRefreshToken(String newToken);

        void onFailureRefreshToken();

        void sessionExpired(String msg);
    }
}
