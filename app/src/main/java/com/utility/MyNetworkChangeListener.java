package com.utility;

/**
 * <h2>MyNetworkChangeListener</h2>
 * Created by Ali on 2/22/18.
 */
public interface MyNetworkChangeListener
{
     void onNetworkStateChanges(boolean nwStatus);
}
