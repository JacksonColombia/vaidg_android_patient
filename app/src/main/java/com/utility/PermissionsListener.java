package com.utility;

/**
 * <h2>PermissionsListener</h2>
 * Created by Ali on 11/29/2017.
 */

public interface PermissionsListener
{
    void onExplanationNeeded(String[] permissionsToExplain);

    void onPermissionResult(boolean granted);
}
