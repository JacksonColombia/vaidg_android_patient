package com.utility;



import static com.vaidg.utilities.Constants.AES_MODE;
import static com.vaidg.utilities.Constants.ANDROID_KEYSTORE;
import static com.vaidg.utilities.Constants.FIXED_IV;
import static com.vaidg.utilities.Constants.IV_PARAMETER_SPEC;
import static com.vaidg.utilities.Constants.KEY_PRIVATEALIAS;
import static com.vaidg.utilities.Constants.KEY_PUBLICALIAS;
import static com.vaidg.utilities.Constants.RSA_MODE;
import static com.vaidg.utilities.LSPApplication.*;
import static com.vaidg.utilities.Utility.bitrateValue;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import android.text.TextUtils;
import android.util.Log;


import com.vaidg.BuildConfig;
import com.vaidg.encryptiondecryption.CryptographyExample;
import com.vaidg.videocalling.UtilityVideoCall;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.vaidg.R;
import com.vaidg.chatting.ChattingActivity;
import com.vaidg.home.MainActivity;
import com.vaidg.invoice.InvoiceActivity;
import com.vaidg.jobDetailsStatus.JobDetailsActivity;
import com.vaidg.rateYourBooking.RateYourBooking;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManager;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.vaidg.zendesk.zendeskHelpIndex.ZendeskHelpIndex;

import com.pojo.NotificationPojo;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilPrivateKey;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.jetbrains.annotations.NotNull;
import org.jitsi.meet.sdk.JitsiMeetActivity;
import org.jitsi.meet.sdk.JitsiMeetConferenceOptions;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 * <h2>MyFirebaseMessagingService</h2>
 * Created by 3Embed on 10/26/2017.
 */

@SuppressLint("MissingFirebaseInstanceTokenRefresh")
public class MyFirebaseMessagingService extends FirebaseMessagingService {



    private String TAG = "MyFirebaseMessagingService";
    private String message;
    private int action, bookingModel = 0, callType;
    private NotificationUtils notificationUtils;
    private Gson gson = new Gson();
    private long bid;
    private String picUrl = "",name="";
    private String title = "";
    private TaskStackBuilder stackBuilder;
    private String timestamp;
    private SessionManager manager;
    private KeyStore keyStore;
    /**
     * <h2>checkAndAddEvent</h2>
     * Add the event in to the google calendar
     *
     * @param context     Context of the activity
     * @param bookingId   booking id of the booked service
     * @param bookingTime booking time of the booked service
     * @param manager     session manager object
     */

    private static int checkAndAddEvent(Context context, long bookingId, long bookingTime, SessionManagerImpl manager) {


        CalendarEventHelper calendarEventHelper = new CalendarEventHelper(context);
        Log.d("TAG", "checkAndAddEvent: ");
        int eventId = 0;
        eventId = calendarEventHelper.addEvent(bookingTime, bookingId);
      /*  if(manager.getBookingStatus(bookingId)==3){

        }*/
        return eventId;
    }

    
    public void onMessageReceived(@NotNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        manager = SessionManager.getInstance(this);
        timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());


        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());

        }
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Notification Data: " + remoteMessage.getData());
            stackBuilder = TaskStackBuilder.create(this);
            String title = getResources().getString(R.string.app_name);
            String s = remoteMessage.getData().get("action");
            if (s != null) {
                action = Integer.parseInt(s);
            }

            message = remoteMessage.getData().get("msg");
            String data = remoteMessage.getData().get("data");
            this.title = remoteMessage.getData().get("title");
            Log.d(TAG, "onMessageReceivedDATA: " + data);
            try {
                if (action == 23) {
                  //  handleDataMessage(action, picUrl, 12, null);
                  if (TextUtils.isEmpty(picUrl)) {
                    showNotificationMessage(getApplicationContext(), title, message, timestamp, null, true);
                  } else {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, null, picUrl, true);
                  }
                  Utility.setMAnagerWithBID(get(),manager);
                } else if (action == 70) {
                    if (!Constants.isHelpIndexOpen)
                        sendNotification(message);
                } else if (action == 111) {
                    information();
                } else if (action == 120) {
                    Log.w(TAG, "isAppIsInBackground outside: 120");
                    if(!Utility.appInForeground(getInstance())) {
                      //  if (!manager.isAppOpen()) {
                        if (!UtilityVideoCall.getInstance().isActiveOnACall()) {
                        //    startActivity(remoteMessage.getData());
                        }
                     //   }
                    }
                   /* if(!manager.isAppOpen()) {
                        if (Utility.isAppIsInBackgroundOne(this)) {
                            startActivity(remoteMessage.getData());
                        } else {
                            startActivity(remoteMessage.getData());
                        }
                    }*/
                }else if(action == 121)
                {
                    /*Clear call notification on call disconnect which has shown in android10*/
                    Log.d("log121 fcm", "action 4");

                    if(!Utility.appInForeground(getInstance())) {
                        NotificationManager notificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        if (notificationManager != null) {
                            //notificationManager.cancel(remoteMessage.getData().get(CALL_ID), ConstantsInteface.CallAndroid10.notification_id);
                        }
                        NotificationUtils.clearNotifications(this);
                        Log.w(TAG, "isAppIsInBackground outside: 121");
                    }
/*
                    if(!manager.isAppOpen()) {
                        if (Utility.isAppIsInBackgroundOne(this)) {
                            Log.w(TAG, "isAppIsInBackground yes: 121");
                            ExitActivity.exitApplicationAndRemoveFromRecent(this);
                        } else {
                            Log.w(TAG, "isAppIsInBackground no: 121");
                       */
/* if(Utility.isAppIsInBackground(this)) {
                            Intent intent = new Intent(this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }*//*

                            ExitActivity.exitApplicationAndRemoveFromRecent(this);

                        }
                    }
*/
                    }
                   /* if(Utility.isAppIsInBackground(this)) {
                        Intent intent = new Intent(this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }*/

                else if(action == 201)
                {
                    information();
                }else {
                    int index = data != null ? data.lastIndexOf("}") : 0;
                    String dataSubString = data.substring(0, index + 1);
                    NotificationPojo notificationPojo = gson.fromJson(dataSubString, NotificationPojo.class);
                    bid = notificationPojo.getBookingId();
                    picUrl = notificationPojo.getProfilePic();
                    bookingModel = notificationPojo.getBookingModel();
                    callType = notificationPojo.getCallType();
                    this.title = notificationPojo.getStatusMsg();
                    if (action == 112)
                        handleChatNotification(notificationPojo);
                    else
                        handleDataMessage(action, picUrl, bid, notificationPojo);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void calling(Map<String, String> dataSubString) {

        Log.d(TAG, "calling: " + dataSubString);


        Constants.isFromNotification = true;
        /*try {


         *//*
            if (!manager.isAppOpen()) {
                if (obj.getString("type").equals("0")) {
                    UtilityVideoCall.getInstance().setActiveOnACall(true, true);
                    manager.setChatBookingID(obj.getLong("bookingId"));
                    manager.setChatProId(obj.getString("callerId"));
                    manager.setProName(obj.getString("callerName"));
                    CallingApis.OpenIncomingCallScreen(obj, this);
                }
            }
*//*


            JSONObject tempObj = new JSONObject();
            tempObj.put("status", 0);

            Log.d(TAG, "Calling received: 2 ");


        } catch (JSONException e) {
            e.printStackTrace();
        }
*/
        JSONObject data = new JSONObject(dataSubString);
        //UtilityVideoCall.getInstance().setActiveOnACall(true, true);
        // JSONObject data = null;

        // UtilityVideoCall.getInstance().setActiveOnACall(true, true);
        // String isVideo = "0";

          /*  if( obj.getString("type").equals("video"))
            {
                isVideo = "1";
            }*/
        /*    Intent incomingScreen = new Intent(getApplicationContext(), IncomingCallScreen.class);
            stackBuilder.addParentStack(MainActivity.class);

            stackBuilder.addNextIntent(incomingScreen);
            incomingScreen.putExtra("callerName",  obj.getString("userName"));
            incomingScreen.putExtra("BookingId",  obj.getString("bookingId"));
            incomingScreen.putExtra("roomId",  obj.getString("room"));
            incomingScreen.putExtra("callId",  obj.getString("callId"));
            incomingScreen.putExtra("callerId", manager.getSID());
            incomingScreen.putExtra("callType",  obj.getString(isVideo));
            incomingScreen.putExtra("callerIdentifier",  obj.getString("userName"));*/
       /*     Intent incomingScreen = new Intent("com.vaidg.videocalling.IncomingCallScreen");
            incomingScreen.putExtra("callerImage", obj.getString("userImage"));
            incomingScreen.putExtra("callerName",  obj.getString("userName"));
            incomingScreen.putExtra("BookingId",  obj.getString("bookingId"));
            incomingScreen.putExtra("roomId",  obj.getString("room"));
            incomingScreen.putExtra("callId",  obj.getString("callId"));
            incomingScreen.putExtra("callerId", manager.getSID());
            incomingScreen.putExtra("callType",  obj.getString(isVideo));
            incomingScreen.putExtra("callerIdentifier",  obj.getString("userName"));

            incomingScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            UtilityVideoCall.getInstance().setActiveOnACall(true, true);
            CallingApis.OpenIncomingCallScreen(obj, this);*/



           /* sessionManager.setChatCount(obj.getString("bookingId"), sessionManager.getChatCount(obj.getString("bookingId")+1));
            sessionManager.setChatBookingID(obj.getString("bookingId"));
            sessionManager.setChatCustomerName(obj.getString("callerName"));
            sessionManager.setChatCustomerID(obj.getString("callerId"));*/

    }

    private void information() {

        if (!Utility.isAppIsInBackground(getApplicationContext())) {
            // app is in foreGround
            Intent pushNotification = new Intent(getApplicationContext(), MainActivity.class);
            stackBuilder.addParentStack(MainActivity.class);
            stackBuilder.addNextIntent(pushNotification);
            showNotificationMessage(getApplicationContext(), title, message, timestamp, pushNotification, false);
        } else {
            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);//MainActivity
            stackBuilder.addParentStack(MainActivity.class);
            stackBuilder.addNextIntent(resultIntent);
            showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent, false);
        }

    }

    // End onReceive

    private void handleChatNotification(NotificationPojo notificationPojo) {
        Log.d(TAG, "handleChatNotification: " + notificationPojo);

        this.title = notificationPojo.getName();
        timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());


        //  ChatData chatData = gson.fromJson(jsonRemoteMessage.getString("data"),ChatData.class);
        long timeStamp = /*Long.parseLong(String.valueOf(*/notificationPojo.getTimestamp();
        /*if(!ConstantsInteface.IS_CHATTING_OPENED)
        {
            db.addNewChat(String.valueOf(chatData.getBid()),chatData.getContent(), sessionManager.getProviderId(),
                    chatData.getFromID(), String.valueOf(chatData.getTimestamp()),chatData.getType(),"1");
        }*/

        Log.d(TAG, "handleChatNotification: getlasttimestampmsg: timeStamp: " + timeStamp + "manager.get" + manager.getLastTimeStampMsg());
        if (timeStamp > manager.getLastTimeStampMsg()) {
            manager.setLastTimeStampMsg(timeStamp);

            /*if(chatListener!=null)
            {
                chatListener.onMessageReceived(chatData);
            }*/

            Log.d(TAG, "handleChatNotification: ConstantsInteface.IS_CHATTING_RESUMED:" + Constants.IS_CHATTING_RESUMED);
          //  if (!Constants.IS_CHATTING_RESUMED) {
                Intent resultIntent = new Intent(getApplicationContext(), ChattingActivity.class);//MainActivity
                stackBuilder.addParentStack(ChattingActivity.class);
                stackBuilder.addNextIntent(resultIntent);
                resultIntent.putExtra("BID", notificationPojo.getBid());
                resultIntent.putExtra("PROIMAGE", notificationPojo.getProfilePic());
                resultIntent.putExtra("PROID", notificationPojo.getFromID());
                resultIntent.putExtra("PRONAME", notificationPojo.getName());
                resultIntent.putExtra("ISFROMNOTIFICATION", true);
                manager.setChatBookingID(notificationPojo.getBid());
                manager.setChatProId(notificationPojo.getFromID());
                manager.setProName(notificationPojo.getName());
                manager.setChatProPic(notificationPojo.getProfilePic());
                manager.setChatCount(notificationPojo.getBid(), manager.getChatCount(notificationPojo.getBid()) + 1);
                checkForKeystore();
                String messageFromDecrypt = "";
                if (!notificationPojo.getContent().contains("https://")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        decryptPrivateKeyPostLollipop();
                        decryptPublicKeyPostLollipop();
                        messageFromDecrypt =
                                encryptData(decryptPrivateKeyPostLollipop(), decryptPublicKeyPostLollipop(), messageFromDecrypt, notificationPojo.getContent());
                    } else {
                        decryptPrivateKeyPreMarshMallow();
                        decryptPublicKeyPreMarshMallow();
                        messageFromDecrypt = encryptData(decryptPrivateKeyPreMarshMallow(), decryptPublicKeyPreMarshMallow(), messageFromDecrypt, notificationPojo.getContent());
                    }
                }else{
                    messageFromDecrypt = notificationPojo.getContent();
                }
          /*  if(!VariableConstant.isChattingToSave)
            {

                db.addNewChat(message,notificationPojo.getTargetId(),notificationPojo.getFromID()
                        ,notificationPojo.getTimestamp(),notificationPojo.getType(),2,notificationPojo.getBid());
            }*/


                // check for image attachment
                if (messageFromDecrypt.contains("https://"))
                    showNotificationMessageWithBigImage(getApplicationContext(), title, "You got a new message", timestamp, resultIntent, messageFromDecrypt, true);
                else
                    showNotificationMessage(getApplicationContext(), title, messageFromDecrypt, timestamp, resultIntent, true);

           //}
        }


    }

    private void handleDataMessage(int action, String picUrl, long bid, NotificationPojo notificationPojo) {

       /* if(manager.getBookingStatus(bid)>=action){
            return;
        }*/
       /* if(notificationPojo!=null)
        {
            if(manager.getBookingStatus(notificationPojo.getBookingId())<action){
                manager.setBookingStatus(notificationPojo.getBookingId(),action);
            }
        }*/

        this.action = action;
        this.picUrl = picUrl;
        this.bid = bid;
        timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss", Locale.US).format(new Date());

        Constants.isJobBidDetailsOpen = false;
        Log.e(TAG, "action " + this.action);
        Log.e(TAG, "message: " + message);
        Log.e(TAG, "bid: " + this.bid);
        Log.e(TAG, "timestamp: " + timestamp);

//|| action == 9
        if (action == 3 || action == 6 || action == 7 || action == 8  || action == 17) {
            assert notificationPojo != null;
            Log.d(TAG, "handleDataMessage: " + action + " bookingTypeNotification " + notificationPojo.getBookingType());
            if (action == 6) {
                //ConstantsInteface.custLatLng = new LatLng(notificationPojo.get);
            }
            if (!Constants.isJobDetailsOpen) {
                Intent resultIntent;
                if (action == 3 && notificationPojo.getBookingType() == 3) {
                    Constants.isConfirmBook = true;
                    Log.d(TAG, "handleDataMessageInIf: " + action);

                    resultIntent = new Intent(getApplicationContext(), MainActivity.class);
                    stackBuilder.addParentStack(MainActivity.class);
                } else if (action == 9 && notificationPojo.getCallType() == 3) {
                    resultIntent = new Intent(getApplicationContext(), InvoiceActivity.class);
                    stackBuilder.addParentStack(InvoiceActivity.class);
                    Log.w("Check","Inovice 3");
                }else {
                    Log.w("Check","Inovice 4");
                    Log.d(TAG, "handleDataMessageInElse: " + action);

                    resultIntent = new Intent(getApplicationContext(), JobDetailsActivity.class);
                    stackBuilder.addParentStack(JobDetailsActivity.class);
                }
                stackBuilder.addNextIntent(resultIntent);
                resultIntent.putExtra("BID", this.bid);
                resultIntent.putExtra("STATUS", this.action);
                resultIntent.putExtra("BookingModel", bookingModel);
                resultIntent.putExtra("CallType", callType);
                // VariableConstant.isSplashCalled = true;
                Log.e(TAG, "initializehandleDataMessage: " + bid + " BID " + this.bid + " action " + this.action);
                // check for image attachment
                if (TextUtils.isEmpty(picUrl)) {
                    showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent, true);
                } else {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, picUrl, true);
                }
            }

        }else if (action == 130) {
            assert notificationPojo != null;
            Log.d(TAG, "handleDataMessage: " + action + " bookingTypeNotification " + notificationPojo.getBookingType());

           // if (!Constants.isJobDetailsOpen) {
                Intent resultIntent;
               // if(!Utility.appInForeground(getInstance())) {
                    Constants.isCallActivated = true;
                    resultIntent = new Intent(getApplicationContext(), JobDetailsActivity.class);
                    stackBuilder.addParentStack(JobDetailsActivity.class);
                    resultIntent.putExtra("isCallActive", true);
                    stackBuilder.addNextIntent(resultIntent);
                    resultIntent.putExtra("BID", this.bid);
                    resultIntent.putExtra("STATUS",3 );
                    resultIntent.putExtra("BookingModel", bookingModel);
                    resultIntent.putExtra("CallType", callType);
                    resultIntent.putExtra("isCallActive", true);
                    // VariableConstant.isSplashCalled = true;
                    Log.e(TAG, "initializehandleDataMessage: " + bid + " BID " + this.bid + " action " + this.action);
                    // check for image attachment
                    if (TextUtils.isEmpty(picUrl)) {
                        showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent, true);
                    } else {
                        // image is present, show notification with image
                        showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, picUrl, true);
                    }
               /* }else{
                   *//* if (TextUtils.isEmpty(picUrl)) {
                      //  showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent, true);
                    } else {
                        // image is present, show notification with image
                        showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, picUrl, true);
                    }*//*
                }*/
           // }
        }else if(action == 131)
        {
            Constants.isCallActivated = false;
        }else if(action == 52)
        {
            Constants.isJobBidDetailsOpen = true;
            Intent resultIntent = new Intent(getApplicationContext(), JobDetailsActivity.class);
            stackBuilder.addParentStack(JobDetailsActivity.class);
            stackBuilder.addNextIntent(resultIntent);
            resultIntent.putExtra("BID", this.bid);
            resultIntent.putExtra("STATUS", this.action);
            resultIntent.putExtra("PRONAME", notificationPojo.getName());

            // VariableConstant.isSplashCalled = true;
            Log.e(TAG, "initializehandleDataMessage: " + bid + " BID " + this.bid + " action " + this.action);
            // check for image attachment
            if (TextUtils.isEmpty(picUrl)) {
                showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent, true);
            } else {
                // image is present, show notification with image
                showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, picUrl, true);
            }
        } else if (action == 10) {
            Log.w("Check","Inovice 5");
            Intent resultIntent = new Intent(getApplicationContext(), RateYourBooking.class);
            stackBuilder.addParentStack(RateYourBooking.class);
            stackBuilder.addNextIntent(resultIntent);
            resultIntent.putExtra("BID", this.bid);
            resultIntent.putExtra("STATUS", this.action);
            resultIntent.putExtra("BookingModel", bookingModel);
            resultIntent.putExtra("CallType", callType);
            // VariableConstant.isSplashCalled = true;
            Log.e(TAG, "initializehandleDataMessage: " + bid + " BID " + this.bid + " action " + this.action);
            // check for image attachment
            if (TextUtils.isEmpty(picUrl)) {
                showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent, true);
            } else {
                // image is present, show notification with image
                showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, picUrl, true);
            }
        } else {
            //Added April 25 2018
            if (Constants.isJobDetailsOpen) {
                return;
            }
            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
            stackBuilder.addParentStack(MainActivity.class);
            stackBuilder.addNextIntent(resultIntent);
            resultIntent.putExtra("BID", this.bid);
            resultIntent.putExtra("STATUS", this.action);
            //    VariableConstant.isSplashCalled = true;
            Log.e(TAG, "initializehandleDataMessage: " + bid + " BID " + this.bid + " action " + this.action);
            // check for image attachment
            if (TextUtils.isEmpty(picUrl)) {
                showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent, true);
            } else {
                // image is present, show notification with image
                showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, picUrl, true);
            }

        }


        if (action == 3 && notificationPojo != null && (notificationPojo.getBookingType() == 3 || notificationPojo.getBookingType() == 2)) {
            int eventId = checkAndAddEvent(MyFirebaseMessagingService.this, notificationPojo.getBookingId()
                    , notificationPojo.getBookingRequestedFor(), manager);
            Log.d(TAG, "handleDataMessageEvent: " + eventId + " event " + manager.getBookingStatus(notificationPojo.getBid()));


            new Handler(Looper.getMainLooper()).post(new Runnable() {
                
                public void run() {
                    NotificationHandler notificationHandler = new NotificationHandler();
                    if (eventId != 0)
                        notificationHandler.addReminderEventId(eventId, notificationPojo.getBookingId(), manager);
                }
            });


        }
        //}
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent, boolean isChatting) {
        notificationUtils = new NotificationUtils(context);
        if(intent != null)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, isChatting, stackBuilder);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl, boolean isChatting) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl, isChatting, stackBuilder);
    }

    //    /**
//     * Create and show a simple notification containing the received FCM message.
//     *
//     * @param messageBody FCM message body received.
//     */
    private void sendNotification(String messageBody) {
        Bitmap icon1 = BitmapFactory.decodeResource(getResources(),
                R.mipmap.ic_launcher_round);
        int numMessages = 0;

        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        inboxStyle.setSummaryText("" + getResources().getString(R.string.livemAppLink));
        inboxStyle.setBigContentTitle(messageBody);
        String[] events = new String[6];
        stackBuilder.addParentStack(ZendeskHelpIndex.class);
        // Moves events into the expanded layout
        // Moves events into the expanded layout
        for (String event : events) {

            inboxStyle.addLine(event);
        }
        // mBuilder.setStyle(inboxStyle);

        Intent intent = new Intent(this, ZendeskHelpIndex.class);
        stackBuilder.addNextIntent(intent);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        /*PendingIntent pendingIntent = PendingIntent.getActivity(this, 0  , intent,
                PendingIntent.FLAG_ONE_SHOT);*/

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notification_logo)//splash_live_m_logo
                .setLargeIcon(icon1)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setStyle(inboxStyle)
                .setNumber(++numMessages)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null) {
            notificationManager.notify(1, notificationBuilder.build());
        }
    }

    public void genralmethod() {
        Intent pushNotification = new Intent(getApplicationContext(), NotificationHandler.class);

        pushNotification.putExtra("message", message);
        pushNotification.putExtra("statcode", this.action);
        pushNotification.putExtra("bid", this.bid);
        pushNotification.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(pushNotification);
        notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils.playNotificationSound();
    }

    private class callApi extends AsyncTask {
        
        protected Object doInBackground(Object[] objects) {
            return null;
        }
    }

/*
    private void startActivity(Map<String,String> pushData) {
        Log.w(TAG, "startActivity: "+pushData );
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && !Utility.appInForeground(LSPApplication.getInstance())) {
            NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            AudioAttributes attributes = new AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_ALARM)
                .build();

            String CHANNEL_ID = BuildConfig.APPLICATION_ID.concat("_notification_id");
            String CHANNEL_NAME = BuildConfig.APPLICATION_ID.concat("_notification_name");


            NotificationChannel mChannel = notificationManager != null ? notificationManager.getNotificationChannel(CHANNEL_ID) : null;
            if (mChannel == null) {
                mChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
                mChannel.setSound(sound, attributes);
                mChannel.enableLights(true);
                mChannel.enableVibration(true);
                if (notificationManager != null) {
                    notificationManager.createNotificationChannel(mChannel);
                }
            }

            String callTitle;

            if(AUDIO.equals(pushData.get(CALL_TYPE))){
                callTitle = getString(R.string.incoming_audio_call)+" "+pushData.get(USER_NAME);
            }else if(VIDEO.equals(pushData.get(CALL_TYPE))){
                callTitle = getString(R.string.incoming_video_call)+" "+pushData.get(USER_NAME);
            }else {
                callTitle = getString(R.string.call)+" "+pushData.get(USER_NAME);
            }

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
            builder.setSmallIcon(R.drawable.ic_notification_logo)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentTitle(callTitle)
                .setContentText(getString(R.string.tap_to_open))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_CALL)
                .setContentIntent(openCallScreen(pushData))
                .setAutoCancel(true)
                .setTimeoutAfter(60000)
                .setOngoing(true);

            Notification notification = builder.build();
            notificationManager.notify(pushData.get(CALL_ID),ConstantsInteface.CallAndroid10.notification_id, notification);
        } else {
            //todo below android 10
            try {
                openCallScreen(pushData).send();
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            }
        }

    }
*/

/*
    private PendingIntent openCallScreen(Map<String,String> pushData) {

        Log.w(TAG, "onMessageReceived:2 "+bitrateValue());


      Intent notifyIntent = new Intent(this, CallingActivity.class);
      notifyIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
      Bundle bundle = new Bundle();
      bundle.putString(USER_IMAGE, pushData.get(USER_IMAGE));
      bundle.putString(USER_NAME, pushData.get(USER_NAME));
      bundle.putString(USER_ID,  */
/*"5e97f858e5d129adf16afd7a"*//*
pushData.get(USER_ID));
      bundle.putSerializable(CALL_STATUS, CallStatus.NEW_CALL);
      bundle.putString(CALL_TYPE, pushData.get(CALL_TYPE));
      bundle.putString(CALL_ID, pushData.get(CALL_ID));
      bundle.putString(ROOM_ID, pushData.get(ROOM_ID));
      try {
        long l = Long.parseLong(pushData.get(BOOKING_ID).trim());  //<-- String to long here
        manager.setChatBookingID(l);
        bundle.putLong(BOOKING_ID,l);
        System.out.println("long l = " + l);
      } catch (NumberFormatException nfe) {
        System.out.println("NumberFormatException: " + nfe.getMessage());
      }
      bundle.putInt(BITRATE, bitrateValue());
      notifyIntent.putExtras(bundle);
        manager.setChatProId(pushData.get(USER_ID));
        manager.setProName(pushData.get(USER_NAME));
        manager.setChatProPic(pushData.get(USER_IMAGE));

        return PendingIntent.getActivity(
            LSPApplication.getInstance(), 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT
        );

    }
*/


    @SuppressLint("NewApi")

    public String decryptPrivateKeyPostLollipop() {
        String str = "";
        Log.i("TEST", "decryptData marsh melloa ");
        Cipher c;
        try {
            c = Cipher.getInstance(AES_MODE);
            c.init(Cipher.DECRYPT_MODE, getSecretPrivateKeyPostLollipop(), new GCMParameterSpec(128, FIXED_IV.getBytes()));
            byte[] encryptedBytes = Base64.decode(manager.getVirgilPrivateKey(), Base64.DEFAULT);

            byte[] decodedBytes = c.doFinal(encryptedBytes);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                str = new String(decodedBytes, StandardCharsets.UTF_8);
            } else {
                str = new String(decodedBytes);
            }
            Log.i("TEST", "decryptData srting " + str);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return str;
    }

    @SuppressLint("NewApi")
    
    public String decryptPublicKeyPostLollipop() {
        String str = "";
        Log.i("TEST", "decryptData marsh melloa ");
        Cipher c;
        try {
            c = Cipher.getInstance(AES_MODE);
            c.init(Cipher.DECRYPT_MODE, getSecretPublicKeyPostLollipop(), new GCMParameterSpec(128, FIXED_IV.getBytes()));
            byte[] encryptedBytes = Base64.decode(manager.getVirgilPublicKey(), Base64.DEFAULT);

            byte[] decodedBytes = c.doFinal(encryptedBytes);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                str = new String(decodedBytes, StandardCharsets.UTF_8);
            } else {
                str = new String(decodedBytes);
            }
            Log.i("TEST", "decryptData srting " + str);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    /**
     * <h2>getSecretPrivateKeyPreMarshMallow</h2>
     * This method is used to get the secret key for pre marsh mallow device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPrivateKeyPreMarshMallow() throws Exception {
        String enryptedKeyB64 = manager.getEncryptionPrivateKey();
        Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
        // need to check null, omitted here
        byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
        byte[] key = rsaPrivateKeyDecrypt(encryptedKey);
        Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
        return new SecretKeySpec(key, "AES");
    }

    /**
     * <h2>getSecretPublicKeyPreMarshMallow</h2>
     * This method is used to get the secret key for pre marsh mallow device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPublicKeyPreMarshMallow() throws Exception {
        String enryptedKeyB64 = manager.getEncryptionPublicKey();
        Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
        // need to check null, omitted here
        byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
        byte[] key = rsaPublicKeyDecrypt(encryptedKey);
        Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
        return new SecretKeySpec(key, "AES");
    }


    private byte[] rsaPrivateKeyDecrypt(byte[] encrypted) throws Exception {
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PRIVATEALIAS, null);
        Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
        CipherInputStream cipherInputStream = new CipherInputStream(
            new ByteArrayInputStream(encrypted), output);
        ArrayList<Byte> values = new ArrayList<>();
        int nextByte;
        while ((nextByte = cipherInputStream.read()) != -1) {
            values.add((byte) nextByte);
        }

        byte[] bytes = new byte[values.size()];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = values.get(i);
        }
        return bytes;
    }

    private byte[] rsaPublicKeyDecrypt(byte[] encrypted) throws Exception {
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PUBLICALIAS, null);
        Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
        output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
        CipherInputStream cipherInputStream = new CipherInputStream(
            new ByteArrayInputStream(encrypted), output);
        ArrayList<Byte> values = new ArrayList<>();
        int nextByte;
        while ((nextByte = cipherInputStream.read()) != -1) {
            values.add((byte) nextByte);
        }

        byte[] bytes = new byte[values.size()];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = values.get(i);
        }
        return bytes;
    }


    
    public String decryptPrivateKeyPreMarshMallow() {
        Log.i("TEST", "decryptData else  ");
        Cipher c;
        String str = "";
        try {
            c = Cipher.getInstance(AES_MODE, "BC");
            c.init(Cipher.DECRYPT_MODE, getSecretPrivateKeyPreMarshMallow(), IV_PARAMETER_SPEC);
            byte[] encryptedBytes = Base64.decode(manager.getEncryptionPrivateKey(), Base64.DEFAULT);
            byte[] decodedBytes = c.doFinal(encryptedBytes);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                str = new String(decodedBytes, StandardCharsets.UTF_8);
            } else {
                str = new String(decodedBytes);
            }
            Log.i("TEST", "decryptData srting " + str);


        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    
    public String decryptPublicKeyPreMarshMallow() {
        Log.i("TEST", "decryptData else  ");
        Cipher c;
        String str = "";
        try {
            c = Cipher.getInstance(AES_MODE, "BC");
            c.init(Cipher.DECRYPT_MODE, getSecretPublicKeyPreMarshMallow(), IV_PARAMETER_SPEC);
            byte[] encryptedBytes = Base64.decode(manager.getEncryptionPublicKey(), Base64.DEFAULT);
            byte[] decodedBytes = c.doFinal(encryptedBytes);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                str = new String(decodedBytes, StandardCharsets.UTF_8);
            } else {
                str = new String(decodedBytes);
            }
            Log.i("TEST", "decryptData srting " + str);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }


    /**
     * <h2>getSecretPrivateKeyPostLollipop</h2>
     * This method is used to get the secret key for post lollipop device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPrivateKeyPostLollipop() throws Exception {
        Key s = keyStore.getKey(KEY_PRIVATEALIAS, null);
        Log.i("TEST", "key " + s.toString());
        return keyStore.getKey(KEY_PRIVATEALIAS, null);
    }

    /**
     * <h2>getSecretPublicKeyPostLollipop</h2>
     * This method is used to get the secret key for post lollipop device
     *
     * @return returns the secret key needed to encrypt /decrypt
     * @throws Exception throws the exception to be handled
     */
    private java.security.Key getSecretPublicKeyPostLollipop() throws Exception {
        Key s = keyStore.getKey(KEY_PUBLICALIAS, null);
        Log.i("TEST", "key " + s.toString());
        return keyStore.getKey(KEY_PUBLICALIAS, null);
    }

    
    public void checkForKeystore() {
        try {
            keyStore = KeyStore.getInstance(ANDROID_KEYSTORE);
            keyStore.load(null);
        } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
            e.printStackTrace();
        }
        try {
            if (!keyStore.containsAlias(KEY_PRIVATEALIAS)) {
                //mSignUpView.checkForVersion();
            }
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
    }
    private String encryptData(String privateKey, String publicKey, String messageFromDecrypt, String messageToDecrypt) {

        try {
            CryptographyExample cryptographyExample = new CryptographyExample();
            VirgilCrypto crypto = new VirgilCrypto();
            // Import private key
            VirgilPrivateKey importedPrivateKey = cryptographyExample.importPrivateKey(privateKey, crypto);
            // Decrypt data
           // byte[] encryptedData = Base64.decode(messageToDecrypt, Base64.DEFAULT);/*Base64.getMimeDecoder().decode(chatData.get(position).getContent());*/
            messageFromDecrypt = cryptographyExample.dataDecryption(messageToDecrypt, importedPrivateKey);
        } catch (CryptoException e) {
            e.printStackTrace();
        }
        return messageFromDecrypt;
    }


}
