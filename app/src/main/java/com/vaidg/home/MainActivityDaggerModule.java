package com.vaidg.home;

import com.vaidg.Dagger2.ActivityScoped;
import com.vaidg.Dagger2.FragmentScoped;
import com.vaidg.sidescreens.SidescreensFrag;
import com.vaidg.sidescreens.SidescreensPresenter;
import com.vaidg.wallet.WalletActivityContract;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * <h2>MainActivityDaggerModule</h2>
 * Created by Ali on 1/24/2018.
 */

@Module
public interface MainActivityDaggerModule
{

    @Binds
    @ActivityScoped
    MainActivityContract.MainPresenter provideMainPresenter(MainActivityPresenter presenter);

    @FragmentScoped
    @ContributesAndroidInjector
    ServicesFrag provideServiceFragment();


    @FragmentScoped
    @ContributesAndroidInjector
    MyBookingsFrag provideProjectFrag();


    @FragmentScoped
    @ContributesAndroidInjector
    SidescreensFrag provideSideScreensFrag();



    @FragmentScoped
    @ContributesAndroidInjector
    ChatFragment provideChatFragment();

    @Binds
    @ActivityScoped
    ServiceFragContract.ServicePresenter provideServicePresenter(ServicesFragPresenter presenter);


 /*   @Binds
    @FragmentScoped
    ServiceFragContract.ServiceView provideServiceFrag(ServicesFrag servicesFrag);
*/
    @Binds
    @ActivityScoped
    MyBookingFragContract.MyProjectPresenter provideMyProjectPresenter(MyBookingPresenterImpl presenter);


    @Binds
    @ActivityScoped
    WalletActivityContract.WalletPresenterBalance providePresenterWallet(SidescreensPresenter sidescreensPresenter);

    @Binds
    @ActivityScoped
    ChattingFragPresenter.Presenter providePresenter(ChattingFragPresenterImpl chattingFragPresenter);
}
