package com.vaidg.home;

import static com.vaidg.utilities.Constants.MESSAGE;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.vaidg.jobDetailsStatus.JobDetailsActivity;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.google.gson.Gson;
import com.pojo.AllBookingEventPojo;
import com.pojo.ErrorHandel;
import com.pojo.MyBookingPojo;
import com.pojo.MyBookingStatus;
import com.utility.AlertProgress;
import com.utility.CalendarEventHelper;
import com.utility.NotificationUtils;
import com.utility.RefreshToken;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import java.util.ArrayList;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * <h2>MyBookingPresenterImpl</h2>
 * Created by Ali on 2/12/2018.
 */

public class MyBookingPresenterImpl implements MyBookingFragContract.MyProjectPresenter {

  @Inject
  SessionManagerImpl manager;
  @Inject
  LSPServices lspServices;
  @Inject
  AlertProgress alertProgress;
  @Inject
  Gson gson;
  private String TAG = MyBookingPresenterImpl.class.getSimpleName();
  private MyBookingFragContract.MyProjectView view;
  private boolean isPopUpShowing = true;
 // private Context mContext;

  @Inject
  public MyBookingPresenterImpl() {

  }

  private static void checkAndAddEvent(Context context, long bookingId, long bookingTime, SessionManagerImpl manager) {
    CalendarEventHelper calendarEventHelper = new CalendarEventHelper(context);
    Log.d("TAG", "checkAndAddEvent: ");
    if (manager.getBookingStatus(bookingId) < 3) {
      calendarEventHelper.addEvent(bookingTime, bookingId);
    }
  }

  @Override
  public void attachView(MyBookingFragContract.MyProjectView view) {
    this.view = view;
  }

  @Override
  public void detachView() {
    view = null;
  }

  @Override
  public void onBookingService() {
    Observable<Response<ResponseBody>> observable = lspServices.onToGetAllBookings(LSPApplication.getInstance().getAuthToken(manager.getSID())
        , Constants.selLang, Constants.PLATFORM_ANDROID);
    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            int code = responseBodyResponse.code();
            JSONObject jsonObject;
            try {
              String response = responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
              String error = responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null;
              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                  Log.d(TAG, "onNextBOOKINGIMPL: " + response);
                  Constants.isLoggedIn = true;
                  MyBookingPojo resp = gson.fromJson(response, MyBookingPojo.class);
                  if(view != null) {
                    view.onBookingResponseSuccess(resp.getData(),true);
                    view.onHideProgress();
                  }
                  if (resp.getData().getUpcoming().size() > 0) {
                    for (int i = 0; i < resp.getData().getUpcoming().size(); i++) {
                      if (resp.getData().getUpcoming().get(i).getBookingType() == 3) {
                        if (manager.getBookingStatus(resp.getData().getUpcoming().get(i).getBookingId()) < resp.getData().getUpcoming().get(i).getStatus()) {
                          manager.setBookingStatus(resp.getData().getUpcoming().get(i).getBookingId(), resp.getData().getUpcoming().get(i).getStatus());
                        }
                      }
                    }
                  }

                  break;
                case Constants.SESSION_EXPIRED:
                  if (error != null && !error.isEmpty()) {
                    ErrorHandel errorHandel = gson.fromJson(error, ErrorHandel.class);
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                      @Override
                      public void onSuccessRefreshToken(String newToken) {

                        LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                        onBookingService();
                      }


                      @Override
                      public void onFailureRefreshToken() {

                      }

                      @Override
                      public void sessionExpired(String msg) {
                        if(view != null) {
                          view.onHideProgress();
                          view.onLogout(msg, manager);
                        }
                      }
                    });
                  }
                  break;
                case Constants.SESSION_LOGOUT:
                  if (error != null && !error.isEmpty()) {
                    jsonObject = new JSONObject(error);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onHideProgress();
                        view.onLogout(jsonObject.getString(MESSAGE), manager);
                      }
                    }
                  }
                  break;
                default:
                  if (error != null && !error.isEmpty()) {
                    jsonObject = new JSONObject(error);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onHideProgress();
                        view.onError(jsonObject.getString(MESSAGE));
                      }
                    }
                  }
                  break;
              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
              if(view != null) {
                view.onHideProgress();
              }
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.onHideProgress();
              view.onConnectionError(e.getMessage(), true);
            }
          }

          @Override
          public void onComplete() {

          }
        });
  }

  @Override
  public void onUpcommingApi(long fromDate, long toDate) {
    long fDate = fromDate / 1000;
    long toDates = toDate / 1000;
    Log.d(TAG, "onUpcommingApi: " + fDate + " toDate " + toDates);
    Observable<Response<ResponseBody>> observable = lspServices.onToGetAllBookingsUpComing(LSPApplication.getInstance().getAuthToken(manager.getSID())
        , Constants.selLang, Constants.PLATFORM_ANDROID, fDate, toDates);
    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            int code = responseBodyResponse.code();
            JSONObject jsonObject;
            try {
              String response = responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
              String error = responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null;
              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                  Log.d(TAG, "onNextBOOKINGIMPLUP: " + response);
                  MyBookingPojo resp = gson.fromJson(response, MyBookingPojo.class);
                  //  view.onBookingResponseSuccess(resp.getData());
                  if(view != null) {
                    view.onHideProgress();
                    view.onBookingResponseSuccess(resp.getData(),false);
                  //  view.onUpComingBooking(resp.getData().getUpcoming());
                  }

                  break;
                case Constants.SESSION_EXPIRED:
                  if (error != null && !error.isEmpty()) {
                    ErrorHandel errorHandel = gson.fromJson(error, ErrorHandel.class);
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                      @Override
                      public void onSuccessRefreshToken(String newToken) {

                        LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                        onBookingService();
                      }


                      @Override
                      public void onFailureRefreshToken() {

                      }

                      @Override
                      public void sessionExpired(String msg) {
                        if(view != null) {
                          view.onHideProgress();
                          view.onLogout(msg, manager);
                        }
                      }
                    });
                  }
                  break;
                case Constants.SESSION_LOGOUT:
                  if (error != null && !error.isEmpty()) {
                    jsonObject = new JSONObject(error);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onHideProgress();
                        view.onLogout(jsonObject.getString(MESSAGE), manager);
                      }
                    }
                  }
                default:
                  if (error != null && !error.isEmpty()) {
                    jsonObject = new JSONObject(error);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onHideProgress();
                        view.onError(jsonObject.getString(MESSAGE));
                      }

                    }
                  }
                  break;
              }
            } catch (IOException | JSONException e) {
              if(view != null) {
                view.onHideProgress();
              }
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.onHideProgress();
              view.onConnectionError(e.getMessage(), false);
            }
          }

          @Override
          public void onComplete() {

          }
        });
  }

  @Override
  public void onMqttJobStatus(MyBookingStatus myBookingStatus, final ArrayList<AllBookingEventPojo> pendingPojo, final ArrayList<AllBookingEventPojo> upComingPojo, final ArrayList<AllBookingEventPojo> pastPojo, Context mContext, NotificationUtils notificationUtils) {

    Log.d("TAG", "onNext: " + myBookingStatus.getData().getStatus());
    int dataIndex = myBookingStatus.getData().getStatus();

    CalendarEventHelper calendarEventHelper = new CalendarEventHelper(mContext);
    // int index;
    Log.d("TAG", "onNextisPopUpShowing: " + isPopUpShowing + " isOpen " + Constants.isJobDetailsOpen);
    switch (dataIndex) {
      case 17:
        for (int i = 0; i < pendingPojo.size(); i++) {
          if (myBookingStatus.getData().getBookingId() == pendingPojo.get(i).getBookingId()) {
            //  index = i;

            pendingPojo.get(i).setBidDispatchLog(myBookingStatus.getData().getBidProvider());
            pendingPojo.get(i).setStatus(myBookingStatus.getData().getStatus());
            pendingPojo.get(i).setStatusMsg(myBookingStatus.getData().getStatusMsg());

            pendingPojo.get(i).setBookingId(myBookingStatus.getData().getBookingId());
            if(view != null) {
              view.onNotifyAdapter(pendingPojo, upComingPojo, pastPojo, true);
            }
            isPopUpShowing = true;
            break;
          }
        }
        break;
      case 3://booking accepted

        Log.d("TAG", "onNextBookingStatusCancel3: " + myBookingStatus.getData().getStatus()
            + " bookingId3 " + myBookingStatus.getData().getBookingId());
        if (!Constants.isJobDetailsOpen) {

          if (manager.getBookingStatus(myBookingStatus.getData().getBookingId()) < myBookingStatus.getData().getStatus()) {
            manager.setBookingStatus(myBookingStatus.getData().getBookingId(), myBookingStatus.getData().getStatus());
            Intent intent = new Intent(mContext, JobDetailsActivity.class);
            intent.putExtra("BID", myBookingStatus.getData().getBookingId());
            intent.putExtra("STATUS", myBookingStatus.getData().getStatus());
            intent.putExtra("ImageUrl", myBookingStatus.getData().getProProfilePic());
            intent.putExtra("CallType", myBookingStatus.getData().getCallType());
            intent.putExtra("BookingModel", myBookingStatus.getData().getBookingModel());
            notificationUtils.showJustNotification
                ("LIVESTATUS", myBookingStatus.getData().getStatusMsg(), myBookingStatus.getData().getMsg(), intent);
          }
                 /*   if(myEvent.getData().getBookingType()==2)
                        checkAndAddEvent(mContext,myEvent.getData().getBookingId(),myEvent.getData().getBookingRequestedFor(),manager);*/

        }
        for (int i = 0; i < pendingPojo.size(); i++) {
          if (myBookingStatus.getData().getBookingId() == pendingPojo.get(i).getBookingId()) {
            //  index = i;
            pendingPojo.get(i).setStatus(myBookingStatus.getData().getStatus());
            pendingPojo.get(i).setStatusMsg(myBookingStatus.getData().getStatusMsg());
            pendingPojo.get(i).setTitle(myBookingStatus.getData().getTitle());
            pendingPojo.get(i).setFirstName(myBookingStatus.getData().getFirstName());
            pendingPojo.get(i).setLastName(myBookingStatus.getData().getLastName());
            pendingPojo.get(i).setProfilePic(myBookingStatus.getData().getProProfilePic());
            pendingPojo.get(i).setBookingId(myBookingStatus.getData().getBookingId());
            pendingPojo.get(i).setPhone(myBookingStatus.getData().getPhone().get(0).getCountryCode() + "" +
                myBookingStatus.getData().getPhone().get(0).getPhone());
            upComingPojo.add(0, pendingPojo.get(i));
            pendingPojo.remove(i);
            if(view != null) {
              view.onNotifyAdapter(pendingPojo, upComingPojo, pastPojo, true);
            }
            isPopUpShowing = true;
            break;
          }
        }

        break;
      case 4:// booking rejected
      case 5:// booking ignored

                /*if(!ConstantsInteface.isJobDetailsOpen)
                {
                    if(isPopUpShowing)
                    {
                        isPopUpShowing = false;
                        alertProgress.alertinfo(this.mContext,myBookingStatus.getData().getStatusMsg());
                    }
                }*/
        if (manager.getBookingStatus(myBookingStatus.getData().getBookingId()) < myBookingStatus.getData().getStatus()) {
          manager.setBookingStatus(myBookingStatus.getData().getBookingId(), myBookingStatus.getData().getStatus());
          notificationUtils.showJustNotification
              ("", myBookingStatus.getData().getStatusMsg(), myBookingStatus.getData().getMsg(), new Intent());
        }
        for (int i = 0; i < pendingPojo.size(); i++) {
          if (myBookingStatus.getData().getBookingId() == pendingPojo.get(i).getBookingId()) {
            //   index = i;
            pendingPojo.get(i).setStatus(myBookingStatus.getData().getStatus());
            pendingPojo.get(i).setStatusMsg(myBookingStatus.getData().getStatusMsg());
            pendingPojo.get(i).setTitle(myBookingStatus.getData().getTitle());
            pendingPojo.get(i).setFirstName(myBookingStatus.getData().getFirstName());
            pendingPojo.get(i).setLastName(myBookingStatus.getData().getLastName());
            pendingPojo.get(i).setProfilePic(myBookingStatus.getData().getProProfilePic());
            //  pendingPojo.get(i).setPhone(myEvent.getData().getPhone());
            pastPojo.add(0, pendingPojo.get(i));
            pendingPojo.remove(i);
            if(view != null) {
              view.onNotifyAdapter(pendingPojo, upComingPojo, pastPojo, true);
            }
            isPopUpShowing = true;
            break;
          }

        }

        break;
      case 6:
      case 7:
      case 8:
      case 9:
        changeAssignedStatus(myBookingStatus, pendingPojo, upComingPojo, pastPojo);
        if (manager.getBookingStatus(myBookingStatus.getData().getBookingId()) < myBookingStatus.getData().getStatus()) {
          manager.setBookingStatus(myBookingStatus.getData().getBookingId(), myBookingStatus.getData().getStatus());
          Intent intent = new Intent(mContext, JobDetailsActivity.class);
          intent.putExtra("BID", myBookingStatus.getData().getBookingId());
          intent.putExtra("STATUS", myBookingStatus.getData().getStatus());
          intent.putExtra("ImageUrl", myBookingStatus.getData().getProProfilePic());
          intent.putExtra("BookingModel", myBookingStatus.getData().getBookingModel());
          intent.putExtra("CallType", myBookingStatus.getData().getCallType());
          notificationUtils.showJustNotification
              ("LIVESTATUS", myBookingStatus.getData().getStatusMsg(), myBookingStatus.getData().getMsg(), intent);
        }
        break;
      case 10:// booking completed
        break;
      case 11:
        if (!Constants.isJobDetailsOpen) {

          if (manager.getBookingStatus(myBookingStatus.getData().getBookingId()) < myBookingStatus.getData().getStatus()) {
            manager.setBookingStatus(myBookingStatus.getData().getBookingId(), myBookingStatus.getData().getStatus());
            notificationUtils.showJustNotification
                ("", myBookingStatus.getData().getStatusMsg(), myBookingStatus.getData().getMsg(), new Intent());
          }
        }

        Log.d("TAG", "onNextBookingStatusCancel: " + myBookingStatus.getData().getStatus()
            + " bookingId " + myBookingStatus.getData().getBookingId());

        for (int i = 0; i < upComingPojo.size(); i++) {
          if (myBookingStatus.getData().getBookingId() == upComingPojo.get(i).getBookingId()) {
            upComingPojo.get(i).setStatus(myBookingStatus.getData().getStatus());
            upComingPojo.get(i).setStatusMsg(myBookingStatus.getData().getStatusMsg());
            pastPojo.add(0, upComingPojo.get(i));
            upComingPojo.remove(i);
            if(view != null) {
              view.onNotifyAdapter(pendingPojo, upComingPojo, pastPojo, true);
            }
            isPopUpShowing = true;
            break;
          }
        }
        break;

      case 15:
        if (!Constants.isJobDetailsOpen) {
          if (isPopUpShowing) {
            isPopUpShowing = false;
            alertProgress.alertinfo(mContext, myBookingStatus.getData().getStatusMsg());
          }
        }
        for (int i = 0; i < upComingPojo.size(); i++) {
          if (myBookingStatus.getData().getBookingId() == upComingPojo.get(i).getBookingId()) {
            //  index = i;
            upComingPojo.get(i).setStatus(myBookingStatus.getData().getStatus());
            upComingPojo.get(i).setStatusMsg(myBookingStatus.getData().getStatusMsg());
            upComingPojo.get(i).setTitle("");
            upComingPojo.get(i).setFirstName("");
            upComingPojo.get(i).setLastName("");
            upComingPojo.get(i).setProfilePic("");
            upComingPojo.get(i).setPhone("");
            if (myBookingStatus.getData().getReminderId() != null && !"".equals(myBookingStatus.getData().getReminderId())) {
              calendarEventHelper.deleteEvent(Long.parseLong(myBookingStatus.getData().getReminderId()));

              // removeReminderId(myEvent.getData().getReminderId(),myEvent.getData().getBookingId());
            }
            pendingPojo.add(0, upComingPojo.get(i));
            upComingPojo.remove(i);
            if(view != null) {
              view.onNotifyAdapter(pendingPojo, upComingPojo, pastPojo, true);
            }
            //  isPopUpShowing = true;
            break;
          }
        }
        break;
    }
  }

  @Override
  public void onContextReceived(Context mContext) {
   // this.mContext = mContext;
  }

  private void changeAssignedStatus(MyBookingStatus myEvents, ArrayList<AllBookingEventPojo> pendingPojo, ArrayList<AllBookingEventPojo> upComingPojo, ArrayList<AllBookingEventPojo> pastPojo) {

    for (int i = 0; i < upComingPojo.size(); i++) {
      if (myEvents.getData().getBookingId() == upComingPojo.get(i).getBookingId()) {
        upComingPojo.get(i).setStatusMsg(myEvents.getData().getStatusMsg());
        upComingPojo.get(i).setStatus(myEvents.getData().getStatus());
        if(view != null) {
          view.onNotifyAdapter(pendingPojo, upComingPojo, pastPojo, true);
        }
        break;
      }
    }
  }
}
