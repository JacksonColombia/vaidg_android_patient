package com.vaidg.home;

import android.app.Activity;
import android.content.Context;
import android.widget.LinearLayout;

import com.vaidg.model.CategoryCity;
import com.vaidg.utilities.SessionManagerImpl;
import com.google.android.gms.maps.model.LatLng;
import com.vaidg.model.CatDataArray;
import com.vaidg.model.Category;

import java.util.ArrayList;

/**
 * <h2>ServiceFragContract</h2>
 * Created by Ali on 1/29/2018.
 */

public interface ServiceFragContract
{
    interface ServicePresenter extends BasePresenter<ServiceView>{
        void onGetCategory(String search);

        void onPendingBooking();

        void onPendingInvocieBooking();

        void onTogetServerTime();

        void showStatusBar(LinearLayout llHOmeAddress, boolean b);

        void categorySuccess(String resultBody);

        void onHideProgress();

        void getCalls();

        void getSession(SessionManagerImpl mSessionManager, Context mContext);
    }
    interface ServiceView extends BaseView{

        void onSuccess(ArrayList<CatDataArray> data);

        void onLessData();

        void onPendingBooking(long aLong);

        void onPendingInvocieBooking(long aLong);

        void onInCallCategory(ArrayList<Category> trendingArr);

        void onNotOperational(String message);

        void onConnectionError(String message, boolean isPendingBooking);

        void onLocationListener(boolean isAllowed);

        void onNoInCallService();

        void onNoOutCallService();

        void onNoTeleCallService();

        void onItemClick(Category category, Context activityContext, Activity mActivity);

        void onOutCallService(ArrayList<Category> outCallCategory);

        void onTeleCallService(ArrayList<Category> teleCallCategory);

        void onSuccessCity(ArrayList<CategoryCity> allCities);
    }

}
