package com.vaidg.home;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;

import com.vaidg.R;
import com.vaidg.model.Category;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Utility;

import java.util.ArrayList;

import adapters.ServicesAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ali on 4/4/2018.
 */
public class SearchFilterActivity extends AppCompatActivity {
    @BindView(R.id.recyclerviewSearch)
    AutoFitGridRecyclerView recyclerviewSearch;
    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSearchClear)
    ImageView ivSearchClear;
    @BindView(R.id.progressBarShow)
    ProgressBar progressBarShow;
    AppTypeface appTypeface;
    private ArrayList<Category> inComingList;
    private ArrayList<Category> inComingListFilter = new ArrayList<>();
    private ServicesAdapter servicesAdapter;
    private String searchType = "";
    private boolean isEditStop;
    private boolean inCall = false;
    private boolean outCall = false;
    private boolean teleCall = false;
    @BindView(R.id.llNoSymptomAvailable)
    LinearLayout llNoSymptomAvailable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_list);
        ButterKnife.bind(this);
        appTypeface = AppTypeface.getInstance(this);
        getIntentValue();
        setTypeFaceValue();
    }

    private void getIntentValue() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        TextView tvTbTitle = toolbar.findViewById(R.id.tv_center);
        tvTbTitle.setText(R.string.specialist);
        tvTbTitle.setTypeface(appTypeface.getHind_semiBold());
        tvTitle.setTypeface(appTypeface.getHind_medium());
        tvTitle.setText(R.string.search_specialitiy);

        Bundle bundle;
        bundle = getIntent().getExtras();
        if (bundle != null) {
            inComingList = (ArrayList<Category>) bundle.getSerializable("inComingList");
            inCall = bundle.getBoolean(
                    "inCall", false);
            outCall = bundle.getBoolean(
                    "outCall", false);
            teleCall = bundle.getBoolean(
                    "teleCall", false);

        }
        // LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerviewSearch.setHasFixedSize(true);
        GridLayoutManager linearLayoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerviewSearch.setLayoutManager(linearLayoutManager);
        servicesAdapter = new ServicesAdapter(inComingList, true, inCall, outCall, teleCall);
        recyclerviewSearch.setAdapter(servicesAdapter);
    }

    private void setTypeFaceValue() {
        etSearch.setCursorVisible(false);
        etSearch.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelOffset(R.dimen.sp_12));
        etSearch.setHint(getResources().getString(R.string.searchSpecialities));
        etSearch.setTypeface(appTypeface.getHind_regular());

        etSearch.setOnClickListener(view -> {
            etSearch.requestFocus();
            etSearch.setCursorVisible(true);
            etSearch.setFocusableInTouchMode(true);
            etSearch.setFocusable(true);
            InputMethodManager im = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            im.showSoftInput(etSearch, 0);
        });
        etSearch.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {

                searchType = query.toString().toLowerCase();

                final ArrayList<Category> filteredList = new ArrayList<>();

                for (int i = 0; i < inComingList.size(); i++) {

                    final String text = inComingList.get(i).getCatName().toLowerCase().trim();
                    if (text.contains(searchType)) {
                        filteredList.add(inComingList.get(i));
                    }
                }

                if(filteredList.size() > 0) {
                    llNoSymptomAvailable.setVisibility(View.GONE);
                    recyclerviewSearch.setVisibility(View.VISIBLE);
                    recyclerviewSearch.setHasFixedSize(true);
                    GridLayoutManager linearLayoutManager = new GridLayoutManager(SearchFilterActivity.this, 2, GridLayoutManager.VERTICAL, false);
                    recyclerviewSearch.setLayoutManager(linearLayoutManager);
                    servicesAdapter = new ServicesAdapter(filteredList, true, inCall, outCall, teleCall);
                    recyclerviewSearch.setAdapter(servicesAdapter);
                    servicesAdapter.notifyDataSetChanged();// data set changed
                }else{
                    llNoSymptomAvailable.setVisibility(View.VISIBLE);
                    recyclerviewSearch.setVisibility(View.GONE);
                }

            }
        });
    }

    private void filterMethod(String s) {
        if (isEditStop) {
            showProgress();
            filter(s);
        }

    }

    private void showProgress() {
        progressBarShow.setVisibility(View.VISIBLE);
    }


    private void filter(String s) {
        inComingListFilter.clear();
        servicesAdapter.notifyDataSetChanged();
        for (int i = 0; i < inComingList.size(); i++) {
            if (inComingList.get(i).getCatName().toLowerCase().contains(s.toLowerCase())) {
                inComingListFilter.add(inComingList.get(i));
                servicesAdapter.notifyDataSetChanged();
            }

        }
        progressBarShow.setVisibility(View.GONE);
    }

    @OnClick({R.id.ivSearchClear})
    public void clicked(View v) {
            if(!etSearch.getText().toString().trim().isEmpty()) {
                inComingListFilter.clear();
                servicesAdapter.notifyDataSetChanged();
                etSearch.setText("");
            }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);

    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        //Utility.checkAndShowNetworkError(this);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    etSearch.setCursorVisible(false);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

}
