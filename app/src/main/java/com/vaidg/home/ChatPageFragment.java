package com.vaidg.home;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vaidg.R;
import com.vaidg.utilities.AppTypeface;
import com.pojo.BookingChatHistory;

import java.util.ArrayList;

import adapters.ChatBookingsPageAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ali on 5/28/2018.
 */
public class ChatPageFragment extends Fragment
{
    private static final String TAG = "MyEVENTPAGEFRAG";
    private static final String PAGE_COUNT= "pagecount";
    private Context mcontext;
    private ArrayList<BookingChatHistory> bookingChatHistories = new ArrayList<>();
    private ChatBookingsPageAdapter adapterAssign;
    private int pageCount = 0;
    /*@BindView(R.id.recyclerChatBookings)*/RecyclerView recyclerChatBookings;
    /*@BindView(R.id.rlNoChatFound)*/RelativeLayout rlNoChatFound;
    /*@BindView(R.id.tvNoChatBooking)*/TextView tvNoChatBooking;

    public static ChatPageFragment newInstance(int pageValue)
    {
        Bundle args = new Bundle();
        // args.putSerializable(PENDINGALL_JOBS, bookingEvents);
        args.putInt(PAGE_COUNT,pageValue);
        ChatPageFragment fragment = new ChatPageFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle=getArguments();
        pageCount = bundle != null ? bundle.getInt(PAGE_COUNT, 0) : 0;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mcontext = context;
    }

    @Override
    public void onDetach() {
        this.mcontext = null;
        super.onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_bookings_page, container, false);
       // ButterKnife.bind(this,view);
        recyclerChatBookings = view.findViewById(R.id.recyclerChatBookings);
        rlNoChatFound = view.findViewById(R.id.rlNoChatFound);
        tvNoChatBooking = view.findViewById(R.id.tvNoChatBooking);
        initializeView();
        tvNoChatBooking.setTypeface(AppTypeface.getInstance(mcontext).getHind_regular());
        return view;
    }

    private void initializeView() {
        LinearLayoutManager llManager = new LinearLayoutManager(mcontext);
        AppTypeface typeface = AppTypeface.getInstance(mcontext);
        int resId = R.anim.layoutanimation_from_bottom;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(mcontext, resId);
        recyclerChatBookings.setLayoutAnimation(animation);
        recyclerChatBookings.setLayoutManager(llManager);
        adapterAssign = new ChatBookingsPageAdapter(bookingChatHistories);
        recyclerChatBookings.setAdapter(adapterAssign);
        textValueToBeSet();
    }

    public void notifyDataAdapter(ArrayList<BookingChatHistory> eventData) {
        if(eventData != null && eventData.size() > 0) {
            bookingChatHistories.clear();
            bookingChatHistories.addAll(eventData);
            rlNoChatFound.setVisibility(View.GONE);
            recyclerChatBookings.setVisibility(View.VISIBLE);
        } else {
            recyclerChatBookings.setVisibility(View.GONE);
            rlNoChatFound.setVisibility(View.VISIBLE);
        }
        if (adapterAssign != null)
            adapterAssign.notifyDataSetChanged();
    }

    private void textValueToBeSet()
    {
        if(pageCount==0)
        {
            tvNoChatBooking.setText(getString(R.string.youHaveNoOpenChat));
        }else if(pageCount==1)
        {
            tvNoChatBooking.setText(getString(R.string.youHaveNoPastChat));
        }
    }

}
