package com.vaidg.home;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.vaidg.Login.LoginActivity;
import com.vaidg.R;

import com.vaidg.sidescreens.SidescreensFrag;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.vaidg.videocalling.UtilityVideoCall;
import com.mqtt.MQTTManager;
import com.mqtt.MqttEvents;
import com.pojo.MyBookingObservable;
import com.pojo.MyBookingStatus;
import com.utility.AlertProgress;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


public class MainActivity extends DaggerAppCompatActivity implements MyBookingPageFrag.OnFragmentInteractionListener {

    private FragmentManager mFragmentManager;
    @BindView(R.id.tvService)TextView tvService;
    @BindView(R.id.tvMyProject)TextView tvMyProject;
    @BindView(R.id.tvProfile)TextView tvProfile;
    @BindView(R.id.tvMyChat)TextView tvMyChat;
    @Inject AppTypeface mAppTypeface;
    @Inject ServicesFrag mServicesFrag;
    @Inject MyBookingsFrag mMyBookingsFrag;
    @Inject SidescreensFrag  mSidescreensFrag;

    @Inject ChatFragment mChatFragment;
    @Inject MainActivityContract.MainPresenter mMainPresenter;
    @Inject MQTTManager mMQTTManager;
    @Inject SessionManagerImpl mSessionManager;
    @Inject CompositeDisposable mCompositeDisposable;
    @Inject
    AlertProgress mAlertProgress;
    private boolean  flag_guest_login = false;

    @Inject
    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initialize();
        onAppOpen();
    }

    private void onAppOpen() {
        mSessionManager.setAppOpenTime(mSessionManager.getAppOpenTime() + 1);
        if(mSessionManager.getAppOpenTime() % 5 == 0 && !mSessionManager.getDontShowRate())
        {
            mAlertProgress.rateApp(this, mSessionManager, isClicked -> {

            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        flag_guest_login = mSessionManager.getGuestLogin();

        if(!flag_guest_login)
        {
            mSessionManager.setAppOpen(true);
            if(mMQTTManager.isMQTTConnected())
            {
               publishCalls();
            }else
            {
                if(!mMQTTManager.isMQTTConnected())
                    mMQTTManager.createMQttConnection(mSessionManager.getSID(),false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        publishCalls();
                    }
                }, 1000);
            }
        }
    }

    private void publishCalls() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("status", 1);
            mMQTTManager.publish(MqttEvents.CallsAvailability.value + "/" + mSessionManager.getSID(), obj, 0, true);//UserId
            UtilityVideoCall.getInstance().setActiveOnACall(false, false);
            mMQTTManager.subscribeToTopic(MqttEvents.Calls.value+"/"+mSessionManager.getSID(),0);
            mMQTTManager.subscribeToTopic(MqttEvents.Call.value+"/"+mSessionManager.getSID(),0);
            mMQTTManager.subscribeToTopic(MqttEvents.GoogleMapKey.value,0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * <p>initialize</p>
     * Initialize the variable for the MainActivity
     */
    private void initialize() {
        mFragmentManager = getSupportFragmentManager();
        if(Constants.isConfirmBook)
        {
            mMainPresenter.onFragmentTransition("PROJECT",mFragmentManager,mServicesFrag,mMyBookingsFrag,mSidescreensFrag,mChatFragment,R.id.frameLayoutContainer);
            selectedService(1);
        }else
        {
            mMainPresenter.onFragmentTransition("HOME",mFragmentManager,mServicesFrag,mMyBookingsFrag,mSidescreensFrag,mChatFragment,R.id.frameLayoutContainer);
            selectedService(0);
        }
        tvService.setTypeface(mAppTypeface.getHind_regular());
        tvMyProject.setTypeface(mAppTypeface.getHind_regular());
        tvProfile.setTypeface(mAppTypeface.getHind_regular());
        tvMyChat.setTypeface(mAppTypeface.getHind_regular());

        InitializeObserver();

    }

    private void InitializeObserver()
    {
        Observer observer = new Observer<MyBookingStatus>() {
            @Override
            public void onSubscribe(Disposable d) {
               // Log.d("TAG", " onSubscribe: "+d.isDisposed());
                mCompositeDisposable.add(d);
            }

            @Override
            public void onNext(MyBookingStatus myBookingStatus) {

              //  Log.d("TAG", "onNextMain: "+myBookingStatus.getData().getStatus());
                int dataIndex = myBookingStatus.getData().getStatus();
                if( dataIndex == 15)
                {
                    if(mMyBookingsFrag!=null && mMyBookingsFrag.isAdded())
                    {
                        mMyBookingsFrag.onMqttStatusUpdate(myBookingStatus);
                    }else
                    {
                        mMainPresenter.onFragmentTransition("PROJECT",mFragmentManager,mServicesFrag,mMyBookingsFrag,mSidescreensFrag,mChatFragment,R.id.frameLayoutContainer);
                        selectedService(1);
                    }

                }
                if( dataIndex == 1)
                {
                    if(mMyBookingsFrag!=null && mMyBookingsFrag.isAdded())
                    {
                        mMyBookingsFrag.getBookingService();
                    }else
                    {
                        mMainPresenter.onFragmentTransition("PROJECT",mFragmentManager,mServicesFrag,mMyBookingsFrag,mSidescreensFrag,mChatFragment,R.id.frameLayoutContainer);
                        selectedService(1);
                    }
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        MyBookingObservable.getInstance().subscribe(observer);
    }

    /**
     *
     * @param view clicked for the view of the Activity
     */
    @OnClick({R.id.tvService,R.id.tvMyProject,R.id.tvProfile,R.id.tvMyChat})
    public void serviceClicked(View view) {
        switch (view.getId())
        {
            case R.id.tvService:
                mCompositeDisposable.clear();
                selectedService(0);
                mMainPresenter.onFragmentTransition("HOME",mFragmentManager,mServicesFrag,mMyBookingsFrag,mSidescreensFrag,mChatFragment,R.id.frameLayoutContainer);
                break;
            case R.id.tvMyProject:
                if(flag_guest_login)
                {
                    Intent intent = new Intent(this, LoginActivity.class);
                    //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                }else {
                    mCompositeDisposable.clear();
                    selectedService(1);
                    if(mMyBookingsFrag.isAdded())
                    {
                        if(!Constants.isLoggedIn)
                        {
                            mMyBookingsFrag.getBookingService();
                        }
                    }
                    mMainPresenter.onFragmentTransition("PROJECT",mFragmentManager,mServicesFrag,mMyBookingsFrag,mSidescreensFrag,mChatFragment,R.id.frameLayoutContainer);
                }
                break;
            case R.id.tvMyChat:
                if(flag_guest_login)
                {
                    Intent intent = new Intent(this, LoginActivity.class);
                    //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                }else {
                    selectedService(2);
                /*if(mChatFragment.isAdded())
                {
                    mChatFragment.chatHistory();
                }*/
                    mMainPresenter.onFragmentTransition("CHAT",mFragmentManager,mServicesFrag,mMyBookingsFrag,mSidescreensFrag,mChatFragment,R.id.frameLayoutContainer);
                }



                break;
            case R.id.tvProfile:
                mCompositeDisposable.clear();
                selectedService(3);
                if(mSidescreensFrag.isAdded())
                {
                    mSidescreensFrag.walletBalanceValues();
                }
                mMainPresenter.onFragmentTransition("PROFILE",mFragmentManager,mServicesFrag,mMyBookingsFrag,mSidescreensFrag,mChatFragment,R.id.frameLayoutContainer);

                break;

        }
    }

    /**
     * <p>selectedService</p>
     * @param position selected position of the activity to show the fragment
     */
    private void selectedService(int position)
    {
        switch (position)
        {
            case 0:
                tvService.setSelected(true);
                tvMyProject.setSelected(false);
                tvProfile.setSelected(false);
                tvMyChat.setSelected(false);
               /* if(mMQTTManager.isMQTTConnected())
                {
                    mMQTTManager.unSubscribeToTopic(MqttEvents.JobStatus.value + "/" + mSessionManager.getSID());
                }*/
                break;
            case 1:
                /*if(mMQTTManager.isMQTTConnected())
                {
                    mMQTTManager.subscribeToTopic(MqttEvents.JobStatus.value + "/" + mSessionManager.getSID(),1);
                }*/
                tvService.setSelected(false);
                tvMyProject.setSelected(true);
                tvProfile.setSelected(false);
                tvMyChat.setSelected(false);
                break;
            case 2:
                tvService.setSelected(false);
                tvMyProject.setSelected(false);
                tvProfile.setSelected(false);
                tvMyChat.setSelected(true);
                break;

            case 3:
                tvService.setSelected(false);
                tvMyProject.setSelected(false);
                tvProfile.setSelected(true);
                tvMyChat.setSelected(false);
                break;
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        //Utility.checkAndShowNetworkError(this);
    }

    @Override
    public void onDateSelectedApi(long fromDate, long toDate, boolean ApiCalled) {
        mMyBookingsFrag.onDateSelected(fromDate,toDate,ApiCalled);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
