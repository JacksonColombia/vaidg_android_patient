package com.vaidg.home;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.vaidg.Dagger2.ActivityScoped;

import javax.inject.Inject;

/**
 * <h2>MainActivityPresenter</h2>
 * Created by Ali on 1/24/2018.
 */

@ActivityScoped
public class MainActivityPresenter implements MainActivityContract.MainPresenter
{

    @Inject
    MainActivityPresenter() {
    }

    @Override
    public void onFragmentTransition(String TAG, FragmentManager fragmentManager, Fragment fragmentService, Fragment fragmentProject, Fragment fragmentProfile,Fragment chatFragment,int frameId)
    {
        FragmentTransaction fTransaction = fragmentManager.beginTransaction();
        switch (TAG) {
            case "HOME":

                if (fragmentProject.isAdded())
                    fTransaction.hide(fragmentProject);
                if (fragmentProfile.isAdded())
                    fTransaction.hide(fragmentProfile);
                if(chatFragment.isAdded())
                    fTransaction.hide(chatFragment);
                if (fragmentService.isAdded()) {
                    fTransaction.show(fragmentService);
                } else {
                    fTransaction.add(frameId, fragmentService, TAG);
                }
                break;

            case "PROJECT":

                if (fragmentService.isAdded())
                    fTransaction.hide(fragmentService);
                if (fragmentProfile.isAdded())
                    fTransaction.hide(fragmentProfile);
                if(chatFragment.isAdded())
                    fTransaction.hide(chatFragment);
                if (fragmentProject.isAdded()) {
                    fTransaction.show(fragmentProject);
                } else {
                    fTransaction.add(frameId, fragmentProject, TAG);
                }
                break;

            case "PROFILE":

                if (fragmentProject.isAdded())
                    fTransaction.hide(fragmentProject);
                if (fragmentService.isAdded())
                    fTransaction.hide(fragmentService);
                if(chatFragment.isAdded())
                    fTransaction.hide(chatFragment);
                if (fragmentProfile.isAdded()) {
                    fTransaction.show(fragmentProfile);

                } else {
                    fTransaction.add(frameId, fragmentProfile, TAG);
                }
                break;
            case "CHAT":
                if (fragmentProject.isAdded())
                    fTransaction.hide(fragmentProject);
                if (fragmentService.isAdded())
                    fTransaction.hide(fragmentService);
                if(fragmentProfile.isAdded())
                    fTransaction.hide(fragmentProfile);
                if (chatFragment.isAdded()) {
                    fTransaction.show(chatFragment);
                } else {
                    fTransaction.add(frameId, chatFragment, TAG);
                }
                break;
        }
        fTransaction.commit();
    }

}
