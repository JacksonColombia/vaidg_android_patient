package com.vaidg.home;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.R;
import com.vaidg.invoice.InvoiceActivity;
import com.vaidg.lspapplication.LocationDialogFragment;
import com.vaidg.model.CatDataArray;
import com.vaidg.model.Category;
import com.vaidg.model.CategoryCity;
import com.vaidg.networking.GeoNamesApi;
import com.vaidg.rateYourBooking.RateYourBooking;
import com.vaidg.searchsymptom.SearchSymptomActivity;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.AutofitTextView;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.mqtt.MQTTManager;
import com.mqtt.MqttEvents;
import com.utility.AlertProgress;
import com.utility.LocationUtil;
import com.utility.ShimmerLayout;
import com.utility.location.AcessLocation;
import com.utility.location.NetworkLocationUtil;
import com.utility.location.NetworkNotifier;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import adapters.NewHomeSubCategory;
import adapters.ServicesAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerFragment;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.Manifest.permission.*;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.app.Activity.RESULT_OK;
import static com.vaidg.utilities.Constants.*;
import static com.vaidg.utilities.Constants.H_10;
import static com.vaidg.utilities.Constants.H_480;
import static com.vaidg.utilities.Constants.H_5;
import static com.vaidg.utilities.Constants.H_8;
import static com.vaidg.utilities.Constants.W_10;
import static com.vaidg.utilities.Constants.W_15;
import static com.vaidg.utilities.Constants.W_20;
import static com.vaidg.utilities.Constants.W_30;
import static com.vaidg.utilities.Constants.W_320;
import static com.vaidg.utilities.Constants.W_5;
import static com.vaidg.utilities.Constants.W_88;
import static com.vaidg.utilities.Constants.cityId;
import static com.vaidg.utilities.Constants.cityName;
import static com.vaidg.utilities.Constants.latitude;
import static com.vaidg.utilities.Constants.paymentMode;
import static com.vaidg.utilities.Constants.proAddress;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServicesFrag extends DaggerFragment implements ServicesAdapter.OnServiceClicked, ServiceFragContract.ServiceView, LocationUtil.LocationNotifier, NetworkNotifier {

    public static final int REQUEST_PERMISSIONS_CODE = 5000;
    private final int SEARCH_CITY = 150;
    private final int SEARCH_RESULT = 101;
    @Inject
    AppTypeface mAppTypeface;
    @Inject
    ServiceFragContract.ServicePresenter servicePresenter;
    @Inject
    SessionManagerImpl sessionManager;
    @BindView(R.id.rvHomeServiceCatagories)
    RecyclerView rvHomeServiceCatagories;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvYourLocation)
    TextView tvYourLocation;
    @BindView(R.id.rlSearchLayout)
    RelativeLayout rlSearchLayout;
    @BindView(R.id.llHOmeAddress)
    LinearLayout llHOmeAddress;
    @BindView(R.id.nestedScrollViewHome)
    NestedScrollView nestedScrollViewHome;
    @BindView(R.id.shimmer)
    ShimmerLayout shimmerLayout;
    @BindView(R.id.tvNotOperational)
    TextView tvNotOperational;
    @BindView(R.id.no_of_doc_label)
    TextView no_of_doc_label;
    @BindView(R.id.tv_viewAll)
    TextView tv_viewAll;
    @BindView(R.id.rlNotOperational)
    RelativeLayout rlNotOperational;
    @BindView(R.id.llFrame)
    LinearLayout llFrame;
    @BindView(R.id.cl_incall)
    RelativeLayout cl_incall;
    @BindView(R.id.cl_outcall)
    RelativeLayout cl_outcall;
    @BindView(R.id.cl_telecall)
    RelativeLayout cl_telecall;
    @BindView(R.id.viewLine)
    View viewLine;
    @BindView(R.id.tvSearch)
    TextView tvSearch;
    @BindView(R.id.tvBookingTypeIn)
    AutofitTextView tvBookingTypeIn;
    @BindView(R.id.tvBookingTypeOut)
    AutofitTextView tvBookingTypeOut;
    @BindView(R.id.tvBookingTypeTele)
    AutofitTextView tvBookingTypeTele;
    @BindView(R.id.tvBookNowIn)
    TextView tvBookNowIn;
    @BindView(R.id.tvBookNowOut)
    TextView tvBookNowOut;
    @BindView(R.id.tvBookNowTele)
    TextView tvBookNowTele;
    @BindView(R.id.tv_what_are_you_looking)
    TextView tv_what_are_you_looking;
    @BindView(R.id.tvWhatKindOfDoctor)
    TextView tvWhatKindOfDoctor;
    @Inject
    MQTTManager mMQTTManager;
    @Inject
    AlertProgress alertProgress;
    SnappingLinearLayoutManager mLayoutManager;
    @Inject
    GeoNamesApi geoNamesApi;
    private Context mContext;
    private String TAG = ServicesFrag.class.getSimpleName();
    private ArrayList<CatDataArray> mCatDataArrayArrayList;
    private ArrayList<Category> mCategoryArrayList = new ArrayList<>();
    private ArrayList<Category> categoryList = new ArrayList<>();
    private ArrayList<Category> inComingList = new ArrayList<>();
    private ArrayList<Category> outComingList = new ArrayList<>();
    private ArrayList<Category> teleComingList = new ArrayList<>();
    private ArrayList<CategoryCity> categoryCities = new ArrayList<>();
    private NewHomeSubCategory trendingAdapter;
    private double[] currentLatLng = new double[2];
    private LocationUtil mLocationUtil;
    private boolean isReturnedFromLocation = false;

    private int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private NetworkLocationUtil networkUtilObj;

    @Inject
    public ServicesFrag() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_services, container, false);
        ButterKnife.bind(this, view);
      /*  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // request the permission
                showDialog();
            } else {
                // has the permission.
                createLocationObj();
            }
        } else {
            createLocationObj();
        }*/

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1) {
            networkUtilObj = new NetworkLocationUtil(((AppCompatActivity)getActivity()), this);

            networkUtilObj.connectGoogleApiClient();
        } else {
            if (ActivityCompat.checkSelfPermission(mContext,ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                networkUtilObj = new NetworkLocationUtil(((AppCompatActivity)getActivity()), this);

                networkUtilObj.connectGoogleApiClient();
            } else {
              showDialog();
            }
        }
        onGetSession();
        typeFace();
        calculate();
        initialize();
        return view;
    }

    private void getPendingApiCalls() {
        //onPendingCall();
        onCategoryService();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                callReviewPendingBooking();
            }
        }, 4000);

    }

    private void onGetSession() {
        if (alertProgress.isNetworkAvailable(mContext)) {
            if (sessionManager.getIpAddress().equals("0")) {
                alertProgress.IPAddress((ipAddress, lat, lng) -> {
                    sessionManager.setIpAddress(ipAddress);
                    onCategoryService();
                    servicePresenter.getSession(sessionManager, mContext);
                });
            } else {
                servicePresenter.getSession(sessionManager, mContext);
            }
        } else {
            alertProgress.showNetworkAlert(mContext);
        }
    }

    private void onPendingCall() {
        if (alertProgress.isNetworkAvailable(mContext)) {
            servicePresenter.getCalls();
        } else {
            alertProgress.showNetworkAlert(mContext);
        }
    }

    private void calculate() {
        int w10 = Utility.getScreenWidth() * W_10 / W_320;
        int w5 = Utility.getScreenWidth() * W_5 / W_320;
        int w15 = Utility.getScreenWidth() * W_15 / W_320;
        int w20 = Utility.getScreenWidth() * W_20 / W_320;
        int w88 = Utility.getScreenWidth() * W_88 / W_320;
        int w30 = Utility.getScreenWidth() * W_30 / W_320;
        int h10 = Utility.getScreenHeight() * H_10 / H_480;
        int h8 = Utility.getScreenHeight() * H_8 / H_480;
        int h5 = Utility.getScreenHeight() * H_5 / H_480;

     //   llFrame.getLayoutParams().height = w88;
        rlSearchLayout.getLayoutParams().height = w30;
        tvSearch.getLayoutParams().height = w30;
        rlSearchLayout.setPadding(w20, 0, w20, 0);
        cl_outcall.setPadding(w5, h5, w5, h5);
        cl_incall.setPadding(w5, h5, w5, h5);
        cl_telecall.setPadding(w5, h5, w5, h5);
        tv_viewAll.setPadding(w5, 0, w10, 0);
        tvWhatKindOfDoctor.setPadding(w10, h10, w10, 0);
        no_of_doc_label.setPadding(w10, 0, w10, 0);
        tvBookingTypeIn.setPadding(w5, h10, w5, 0);
        tvYourLocation.setPadding(0, h8, 0, 0);
        tvBookingTypeOut.setPadding(w5, h10, w5, 0);
        tvBookingTypeTele.setPadding(w5, h10, w5, 0);
        tvBookNowIn.setPadding(w5, h10, w5, 0);
        tvBookNowOut.setPadding(w5, h10, w5, 0);
        tvBookNowTele.setPadding(w5, h10, w5, 0);
        llFrame.setPadding(w10, 0, w10, 0);
        tvSearch.setPadding(w15, 0, w15, 0);
        tv_what_are_you_looking.setPadding(w10, h5, w10, h5);
        viewLine.setPadding(0, h5, 0, 0);
    }

    private void typeFace() {
        tvAddress.setTypeface(mAppTypeface.getHind_medium());
        tvYourLocation.setTypeface(mAppTypeface.getHind_medium());
        tvSearch.setTypeface(mAppTypeface.getHind_medium());
        tvAddress.setText(mContext.getResources().getString(R.string.fetchLocation));
        tvNotOperational.setTypeface(mAppTypeface.getHind_medium());
        tvBookingTypeTele.setTypeface(mAppTypeface.getHind_semiBold());
        tvBookingTypeOut.setTypeface(mAppTypeface.getHind_semiBold());
        tvBookingTypeIn.setTypeface(mAppTypeface.getHind_semiBold());
        tvBookNowIn.setTypeface(mAppTypeface.getHind_semiBold());
        tvBookNowOut.setTypeface(mAppTypeface.getHind_semiBold());
        tvBookNowTele.setTypeface(mAppTypeface.getHind_semiBold());
        tv_what_are_you_looking.setTypeface(mAppTypeface.getHind_semiBold());
        tvWhatKindOfDoctor.setTypeface(mAppTypeface.getHind_semiBold());
        no_of_doc_label.setTypeface(mAppTypeface.getHind_regular());
        tv_viewAll.setTypeface(mAppTypeface.getHind_semiBold());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @OnClick(R.id.llHOmeAddress)
    public void onAddressChenge() {
        if (Utility.isNetworkAvailable(mContext)) {
        Intent intent = new Intent(mContext, SearchCityActivity.class);
        intent.putExtra("categoryCities", categoryCities);
        intent.putExtra("cityId", cityId);
        intent.putExtra("cityName", cityName);
        startActivityForResult(intent, SEARCH_CITY);
      } else {
        alertProgress.tryAgain(mContext, getString(R.string.pleaseCheckInternet), getString(R.string.system_error), isClicked -> {

        });
      }
    }

    private void callReviewPendingBooking() {
        if (alertProgress.isNetworkAvailable(mContext)) {
            servicePresenter.onPendingBooking();
            //  servicePresenter.onPendingInvocieBooking();

        } else {
            alertProgress.showNetworkAlert(mContext);
        }
    }

    /**
     * initialize the view
     */
    private void initialize() {
        if (!mMQTTManager.isMQTTConnected()) {
            mMQTTManager.createMQttConnection(sessionManager.getSID(), false);
        }

        mCatDataArrayArrayList = new ArrayList<>();
        categoryList = new ArrayList<>();
        mLayoutManager = new SnappingLinearLayoutManager(mContext);
        rvHomeServiceCatagories.setLayoutManager(mLayoutManager);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
        trendingAdapter = new NewHomeSubCategory(categoryList, this);
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(mContext, R.anim.layout_animation_up_to_down);
        rvHomeServiceCatagories.setLayoutAnimation(controller);
        rvHomeServiceCatagories.setLayoutManager(gridLayoutManager);
        rvHomeServiceCatagories.setAdapter(trendingAdapter);
        rvHomeServiceCatagories.scheduleLayoutAnimation();

        setLayoutBackground();

        tvSearch.setOnClickListener(view -> {
            if (Utility.isNetworkAvailable(mContext)) {
                if (categoryList.size() > 0) {
                    Intent intent = new Intent(mContext, SearchSymptomActivity.class);
                    //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    ((Activity) mContext).overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
                }
            } else {
                alertProgress.tryAgain(mContext, getString(R.string.pleaseCheckInternet), getString(R.string.system_error), isClicked -> {

                });
            }
        });

        cl_incall.setOnClickListener(view -> {
            if (Utility.isNetworkAvailable(mContext)) {
                if (outComingList.size() > 0) {
                    Intent intent = new Intent(mContext, SearchFilterActivity.class);
                    intent.putExtra("inComingList", outComingList);
                    intent.putExtra("inCall", false);
                    intent.putExtra("outCall", true);
                    intent.putExtra("teleCall", false);
                    //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    ((Activity) mContext).overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
                }
            } else {
                alertProgress.tryAgain(mContext, getString(R.string.pleaseCheckInternet), getString(R.string.system_error), isClicked -> {

                });
            }
        });
        cl_outcall.setOnClickListener(view -> {
            if (Utility.isNetworkAvailable(mContext)) {
                if (inComingList.size() > 0) {
                    Intent intent = new Intent(mContext, SearchFilterActivity.class);
                    intent.putExtra("inComingList", inComingList);
                    intent.putExtra("inCall", true);
                    intent.putExtra("outCall", false);
                    intent.putExtra("teleCall", false);
                    //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    ((Activity) mContext).overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
                }
            } else {
                alertProgress.tryAgain(mContext, getString(R.string.pleaseCheckInternet), getString(R.string.system_error), isClicked -> {

                });
            }
        });

        cl_telecall.setOnClickListener(view -> {
            if (Utility.isNetworkAvailable(mContext)) {
                if (teleComingList.size() > 0) {
                    Intent intent = new Intent(mContext, SearchFilterActivity.class);
                    intent.putExtra("inComingList", teleComingList);
                    intent.putExtra("inCall", false);
                    intent.putExtra("outCall", false);
                    intent.putExtra("teleCall", true);
                    //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    ((Activity) mContext).overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
                }
            } else {
                alertProgress.tryAgain(mContext, getString(R.string.pleaseCheckInternet), getString(R.string.system_error), isClicked -> {

                });
            }
        });

        tv_viewAll.setOnClickListener(view -> {
            if (Utility.isNetworkAvailable(mContext)) {
                if (categoryList.size() > 0) {
                    Intent intent = new Intent(mContext, SearchFilterActivity.class);
                    intent.putExtra("inComingList", categoryList);
                    intent.putExtra("inCall", false);
                    intent.putExtra("outCall", false);
                    intent.putExtra("teleCall", false);
                    //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    ((Activity) mContext).overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
                }
            } else {
                alertProgress.tryAgain(mContext, getString(R.string.pleaseCheckInternet), getString(R.string.system_error), isClicked -> {

                });
            }
        });

        nestedScrollViewHome.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX,
                                       int oldScrollY) {
                if (scrollY > 540) {
                    servicePresenter.showStatusBar(llHOmeAddress, false);
                }
                if (scrollY == 0) {
                    servicePresenter.showStatusBar(llHOmeAddress, true);
                }
            }
        });
        rvHomeServiceCatagories.setNestedScrollingEnabled(false);
    }


    private void setLayoutBackground() {
        cl_incall.setBackground(getResources().getDrawable(R.drawable.round_background_incall));
        cl_outcall.setBackground(getResources().getDrawable(R.drawable.round_background_outcall));
        cl_telecall.setBackground(getResources().getDrawable(R.drawable.round_background_telecall));
    }


    private void onCategoryService() {
        if (alertProgress.isNetworkAvailable(mContext)) {
            onShowProgress();
            servicePresenter.onGetCategory(cityId);
        } else {
            alertProgress.showNetworkAlert(mContext);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        isJobDetailsOpen = false;
        if (alertProgress.isNetworkAvailable(mContext))
        servicePresenter.onTogetServerTime();

        if (mMQTTManager.isMQTTConnected()) {
            mMQTTManager.subscribeToTopic(MqttEvents.Provider.value + "/" + sessionManager.getSID(), 1);
        }
    }

    @Override
    public void onLocationListener(boolean isAllowed) {
        if (isAllowed) {
            //mPermissionsManager.requestLocationPermissions(getActivity());
/*
            requestPermissions(new String[]{ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_CODE);
*/
            int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 2;
            //  AcessLocation.getLocation(((AppCompatActivity)getActivity()), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            List<String> permissionsNeeded = new ArrayList<>();

            List<String> permissionsList = new ArrayList<>();

            if (!AcessLocation.addPermission(mContext,permissionsList, android.Manifest.permission.ACCESS_FINE_LOCATION))
                permissionsNeeded.add(mContext.getResources().getString(R.string.GPS));


            if (permissionsList.size() > 0)
            {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
        }
    }

    private void showDialog() {
        LocationDialogFragment locationDialogFragment = new LocationDialogFragment();
        locationDialogFragment.initializePresenter(this);
        locationDialogFragment.setCancelable(false);
        if (getFragmentManager() != null) {
            locationDialogFragment.show(getFragmentManager(), "dialog");
        }
    }


    @Override
    public void onServiceClicked(String serviceName) {

    }

    @Override
    public void onSuccess(ArrayList<CatDataArray> data) {
        no_of_doc_label.setText(homePageTagLine);
        if (data.size() > 0) {
            mCatDataArrayArrayList.clear();
            categoryList.clear();
            mCatDataArrayArrayList.addAll(data);
            for (int i = 0; i < data.size(); i++) {
                categoryList.addAll(data.get(i).getCategory());
                cityId = data.get(i).getCategory().get(0).getCityId();
                cityName = data.get(i).getCategory().get(0).getCity();
            }
            tvAddress.setText(cityName);
            if (categoryList.size() > 0) {
                nestedScrollViewHome.setVisibility(View.VISIBLE);
                rlNotOperational.setVisibility(View.GONE);
                trendingAdapter.notifyDataSetChanged();
            }

        }
    }

    @Override
    public void onItemClick(Category category, Context activityContext, Activity mActivity) {
    }


    @Override
    public void onLessData() {

    }

    @Override
    public void onPendingBooking(long bid) {
        if (Utility.isNetworkAvailable(mContext)) {
            if (bid != 0) {
                isHomeFragment = true;
                Intent intent = new Intent(mContext, RateYourBooking.class);
                intent.putExtra("BID", bid);
                //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        } else {
            alertProgress.tryAgain(mContext, getString(R.string.pleaseCheckInternet), getString(R.string.system_error), isClicked -> {

            });
        }
    }

    @Override
    public void onPendingInvocieBooking(long bookingId) {
        if (Utility.isNetworkAvailable(mContext)) {
            if (bookingId != 0) {
                isHomeFragment = true;
                Log.w("Check", "Inovice 2");
                Intent intent = new Intent(mContext, InvoiceActivity.class);
                intent.putExtra("BID", bookingId);
                //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        } else {
            alertProgress.tryAgain(mContext, getString(R.string.pleaseCheckInternet), getString(R.string.system_error), isClicked -> {

            });
        }
    }

    @Override
    public void onInCallCategory(ArrayList<Category> trendingArr) {
        inComingList.clear();
        inComingList.addAll(trendingArr);
    }

    @Override
    public void onOutCallService(ArrayList<Category> trendingArr) {
        outComingList.clear();
        outComingList.addAll(trendingArr);
    }

    @Override
    public void onTeleCallService(ArrayList<Category> trendingArr) {
        teleComingList.clear();
        teleComingList.addAll(trendingArr);
    }

    @Override
    public void onSuccessCity(ArrayList<CategoryCity> allCities) {
        categoryCities.clear();
        categoryCities.addAll(allCities);
    }


    @Override
    public void onNotOperational(String message) {
        if (paymentMode != null) {
            paymentMode.setWallet(false);
        }
        nestedScrollViewHome.setVisibility(View.GONE);
        rlNotOperational.setVisibility(View.VISIBLE);
        tvNotOperational.setText(message);
        alertProgress.alertinfo(mContext, message);
        mCatDataArrayArrayList.clear();
        categoryList.clear();
        inComingList.clear();
        outComingList.clear();
        teleComingList.clear();
        trendingAdapter.notifyDataSetChanged();
        tvAddress.setText(getResources().getString(R.string.select_location));
    }

    @Override
    public void onConnectionError(String message, boolean isPendingBooking) {
        if (isPendingBooking) {
            callReviewPendingBooking();
        } else {
            onCategoryService();
        }
    }

    @Override
    public void updateLocation(Location location) {
        currentLatLng[0] = location.getLatitude();
        currentLatLng[1] = location.getLongitude();

      //  sessionManager.setLatitude(currentLatLng[0] + "");
      //  sessionManager.setLongitude(currentLatLng[1] + "");

        latitude = currentLatLng[0];
        longitude = currentLatLng[1];
       // Constants.currentLat = currentLatLng[0];
       // Constants.currentLng = currentLatLng[1];
        String[] params = {"" + currentLatLng[0], "" + currentLatLng[1]};
        //new BackgroundGetAddress().execute(params);
        getCompleteAddressString(currentLatLng[0],currentLatLng[1]);
       // geoNamesApi(location.getLatitude(), location.getLongitude());
        if (mLocationUtil != null && mLocationUtil.isGoogleApiConnected()) {
            mLocationUtil.stoppingLocationUpdate();
        }
    }

    @Override
    public void locationMsg(String error) {

    }

    private void geoNamesApi(double latitude, double longitude) {
        geoNamesApi.getGeoNamesFromLocation(latitude, longitude, "3embed")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        String response = "";
                        try {
                            response = responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
                            if (response != null && !response.isEmpty()) {
                                Log.d(TAG, "onDataNext: "+response);
                               /* JSONObject geonamesobj = new JSONObject(response).getJSONObject("intersection");
                                String addrLine1 = geonamesobj.optString("street1", "");
                                String addrLine2 = geonamesobj.optString("street2", "");

                                Geocoder geocoder = new Geocoder(LSPApplication.getContext(), Locale.US);
                                try {
                                    List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
                                    Address obj = addresses.get(0);
                                    String addressLine1 =
                                            addrLine1 + ", " + addrLine2 + ", " + System.getProperty("line.separator") + obj
                                                    .getSubLocality() + ", " + obj.getLocality();

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }*/
                            }
                        } catch (/*JSONException |*/ IOException e) {
                            e.printStackTrace();
                            //new BackgroundGetAddress().execute(params);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void onNoInCallService() {
        inComingList.clear();
    }

    @Override
    public void onNoOutCallService() {
        outComingList.clear();
    }

    @Override
    public void onNoTeleCallService() {
        teleComingList.clear();
    }

    @Override
    public void onSessionExpired() {

    }

    @Override
    public void onLogout(String message, SessionManagerImpl sessionManager) {
        alertProgress.alertPositiveOnclick(mContext, message, getString(R.string.logout),
                getString(R.string.ok), isClicked -> Utility.setMAnagerWithBID(mContext, sessionManager));
    }

    @Override
    public void onError(String error) {
        if (paymentMode != null) {
            paymentMode.setWallet(false);
        }
        mCatDataArrayArrayList.clear();
        categoryList.clear();
        inComingList.clear();
        outComingList.clear();
        teleComingList.clear();
        trendingAdapter.notifyDataSetChanged();
        nestedScrollViewHome.setVisibility(View.GONE);
        rlNotOperational.setVisibility(View.VISIBLE);
        alertProgress.alertinfo(mContext, error);
    }

    @Override
    public void onConnectionError(String connectionError) {

    }

    @Override
    public void onShowProgress() {
        if (shimmerLayout != null) {
            shimmerLayout.setVisibility(View.VISIBLE);
            shimmerLayout.startShimmerAnimation();
        }
    }

    @Override
    public void onHideProgress() {
        if (shimmerLayout != null) {
            shimmerLayout.stopShimmerAnimation();
            shimmerLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SEARCH_CITY) {
            if (resultCode == RESULT_OK) {
                cityId = data != null ? data.getStringExtra("cityId") : "0";
                cityName = data != null ? data.getStringExtra("cityName") : "";
                onCategoryService();
            }
        }

        if (requestCode == SEARCH_RESULT) {
            if (data != null) {
                String addrssname = data.getStringExtra("placename");
                String featureAddressName = data.getStringExtra("formatedaddes");
                currentLatLng[0] = data.getDoubleExtra("LATITUDE", 0);
                currentLatLng[1] = data.getDoubleExtra("LONGITUDE", 0);

                if (shimmerLayout != null) {
                    shimmerLayout.setVisibility(View.VISIBLE);
                    shimmerLayout.startShimmerAnimation();
                }
                rlNotOperational.setVisibility(View.GONE);

                if ("NO".equals(addrssname)) {
                    isReturnedFromLocation = false;
                } else {
                    isReturnedFromLocation = true;
                    tvAddress.setText(addrssname);
                    sessionManager.setAddress(featureAddressName);
                  //  sessionManager.setLatitude(currentLatLng[0] + "");
                  //  sessionManager.setLongitude(currentLatLng[1] + "");
                    latitude = currentLatLng[0];
                    longitude = currentLatLng[1];

                    if (alertProgress.isNetworkAvailable(mContext))
                        servicePresenter.onGetCategory(cityId);
                }
            }
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(networkUtilObj != null)
        {
            networkUtilObj.disconnectGoogleApiClient();
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        servicePresenter.attachView(this);
        this.mContext = context;
    }

    @Override
    public void onDetach() {
        mContext = null;
        servicePresenter.detachView();
        super.onDetach();
    }

    private void createLocationObj() {
        mLocationUtil = new LocationUtil(getActivity());
        mLocationUtil.setListener(this);
        if (!mLocationUtil.isGoogleApiConnected()) {
            mLocationUtil.checkLocationSettings();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String permissions[], @NotNull int[] grantResults) {
/*
        switch (requestCode) {
            case REQUEST_PERMISSIONS_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted.
                    createLocationObj();
                } else {
                    // permission denied.
                    // tell the user the action is cancelled
                   // sessionManager.setLongitude("");
                   // sessionManager.setLatitude("");
                    //latitude = 0.0;
                    //longitude = 0.0;
                    proAddress = "";
                    sessionManager.setAddress("");
                    getPendingApiCalls();
                }
                return;
            }
        }
*/
        switch (requestCode)
        {
            case 2:
            {
                Map<String, Integer> perms = new HashMap<>();

                perms.put(ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);

                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                if (perms.get(ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                {
                    networkUtilObj = new NetworkLocationUtil(((AppCompatActivity)getActivity()), this);

                    networkUtilObj.connectGoogleApiClient();
                }else{
                    sessionManager.setLongitude("");
                    sessionManager.setLatitude("");
                    latitude = 0.0;
                    longitude = 0.0;
                    proAddress = "";
                    sessionManager.setAddress("");
                    getPendingApiCalls();
                }

/*
                if (grantResults[0] == PackageManager.PERMISSION_DENIED)
                {
                    boolean should = shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION);
                    if (should)
                    {
                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setTitle(R.string.location_access_denied);
                        builder.setMessage(R.string.location_denied_subtitle);
                        builder.setPositiveButton(R.string.i_am_sure, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                dialog.dismiss();
                            }
                        });
                        builder.setNegativeButton(R.string.re_try, new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                dialog.dismiss();
                                requestPermissions( new String[]{ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                            }
                        });
                        builder.show();

                    }
                }
*/

                break;
            }


            case 1: {
                Map<String, Integer> perms = new HashMap<>();

                perms.put(ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);

                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                if (perms.get(ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                {
                    networkUtilObj = new NetworkLocationUtil(((AppCompatActivity)getActivity()), this);

                    networkUtilObj.connectGoogleApiClient();
                }

                break;
            }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void locationUpdates(Location location) {
        currentLatLng[0] = location.getLatitude();
        currentLatLng[1] = location.getLongitude();

      //  sessionManager.setLatitude(currentLatLng[0] + "");
      //  sessionManager.setLongitude(currentLatLng[1] + "");

        latitude = currentLatLng[0];
        longitude = currentLatLng[1];
      //  currentLat = currentLatLng[0];
      //  currentLng = currentLatLng[1];
        String[] params = {"" + currentLatLng[0], "" + currentLatLng[1]};
        //new BackgroundGetAddress().execute(params);
        getCompleteAddressString(currentLatLng[0],currentLatLng[1]);
        if(networkUtilObj != null)
            networkUtilObj.disconnectGoogleApiClient();

    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strAdd = returnedAddress.getAddressLine(i);
                    // strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                // strAdd = strReturnedAddress.toString();
                Constants.proAddress = strAdd;
                sessionManager.setAddress(strAdd);
                getPendingApiCalls();
                Log.d("My LatLng", strAdd);
            } else {
                //  Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }

    @Override
    public void locationFailed(String message) {

    }

    @SuppressLint("StaticFieldLeak")
    private class BackgroundGetAddress extends AsyncTask<String, Void, String> {
        List<Address> address;
        String lat, lng;

        @Override
        protected String doInBackground(String... params) {
            try {
                lat = params[0];
                lng = params[1];
                if (lat != null && lng != null) {
                    if (mContext != null) {
                        Geocoder geocoder = new Geocoder(mContext);
                        address = geocoder.getFromLocation(Double.parseDouble(params[0]),
                                Double.parseDouble(params[1]), 1);

                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                ((Activity) mContext).runOnUiThread(() -> {
                  //  sessionManager.setLatitude(lat);
                   // sessionManager.setLongitude(lng);
                    latitude = Double.parseDouble(lat);
                    longitude = Double.parseDouble(lng);
                });

            }
            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (address != null && address.size() > 0) {
                Address obj = address.get(0);
                proAddress = obj.getAddressLine(0);
                sessionManager.setAddress(obj.getAddressLine(0));
                getPendingApiCalls();
            }
        }
    }
}
