package com.vaidg.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.dependent.adapter.DependentAdapter;
import com.vaidg.model.CategoryCity;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.Utility;
import com.utility.AlertProgress;

import java.util.ArrayList;

import javax.inject.Inject;

import adapters.CityAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.vaidg.utilities.Constants.cityId;
import static com.vaidg.utilities.Constants.cityName;

public class SearchCityActivity extends AppCompatActivity implements CityAdapter.OnItemClickListener {

    @BindView(R.id.rvCity)
    RecyclerView rvCity;
    @BindView(R.id.etSearch)
    EditText etSearch;
    private ArrayList<CategoryCity> categoryCities = new ArrayList<>();
    private CityAdapter cityAdapter;
    private Context mContext;
    AppTypeface appTypeface;
    AlertProgress alertProgress;
    @BindView(R.id.ivSearchClear)
    ImageView ivSearchClear;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_city);
        ButterKnife.bind(this);
        mContext = this;
        if(getIntent().getExtras()!=null)
        {
            categoryCities = (ArrayList<CategoryCity>) getIntent().getSerializableExtra("categoryCities");
            cityId = getIntent().getStringExtra("cityId");
            cityName = getIntent().getStringExtra("cityName");
        }
        intialize();
    }

    private void intialize() {
        appTypeface = AppTypeface.getInstance(mContext);
        alertProgress = new AlertProgress(mContext);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        TextView textViewCenter = findViewById(R.id.tv_center);
        textViewCenter.setText(R.string.select_city);
        textViewCenter.setTypeface(appTypeface.getHind_semiBold());
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        rvCity.setHasFixedSize(true);
        rvCity.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rvCity.setLayoutManager(new LinearLayoutManager(mContext));
        cityAdapter = new CityAdapter(categoryCities,cityId,this);
        rvCity.setAdapter(cityAdapter);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() > 0)
                    filter(s.toString());
            }
        });
    }

    private void filter(String text) {
        // creating a new array list to filter our data.
        ArrayList<CategoryCity> filteredlist = new ArrayList<>();

        // running a for loop to compare elements.
        for (CategoryCity item : categoryCities) {
            // checking if the entered string matched with any item of our recycler view.
            if (item.getCity().toLowerCase().contains(text.toLowerCase())) {
                // if the item is matched we are
                // adding it to our filtered list.
                filteredlist.add(item);
            }
        }
        if (filteredlist.isEmpty()) {
            // if no item is added in filtered list we are
            // displaying a toast message as no data found.
            rvCity.setVisibility(View.INVISIBLE);
            Toast.makeText(this, "No Data Found..", Toast.LENGTH_SHORT).show();
        } else {
            rvCity.setVisibility(View.VISIBLE);
            // at last we are passing that filtered
            // list to our adapter class.
            cityAdapter.filterList(filteredlist);
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onItemClick(CategoryCity categoryCity) {
        if(Utility.isNetworkAvailable(mContext)) {
            Intent intent = new Intent();
            cityId = categoryCity.getId();
            cityName = categoryCity.getCity();
            intent.putExtra("cityId",categoryCity.getId());
            intent.putExtra("cityName",categoryCity.getCity());
            setResult(RESULT_OK,intent);
            finish();
        }else{
            alertProgress.tryAgain(mContext,getString(R.string.pleaseCheckInternet),getString(R.string.system_error), isClicked -> {

            });
        }
    }
}
