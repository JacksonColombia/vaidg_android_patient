package com.vaidg.home;

import static com.vaidg.utilities.Constants.*;
import static com.vaidg.utilities.Constants.DATA;
import static com.vaidg.utilities.Constants.INTERNAL_SERVER_ERROR;
import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.NOT_FOUND;
import static com.vaidg.utilities.Constants.SESSION_LOGOUT;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;
import static com.vaidg.utilities.Constants.lat;
import static com.vaidg.utilities.Constants.latitude;
import static com.vaidg.utilities.Constants.longitude;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import com.vaidg.R;
import com.vaidg.model.AllCityResponse;
import com.vaidg.model.CatDataArray;
import com.vaidg.model.Category;
import com.vaidg.model.CategoryCity;
import com.vaidg.model.CategoryResponse;
import com.vaidg.model.CityData;
import com.vaidg.networking.LSPServices;
import com.vaidg.networking.NoConnectivityException;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.pojo.ErrorHandel;
import com.utility.RefreshToken;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * <h2>ServicesFragPresenter</h2>
 * Created by Ali on 1/29/2018.
 */

public class ServicesFragPresenter implements ServiceFragContract.ServicePresenter {
  @Inject
  LSPServices lspServices;
  @Inject
  SessionManagerImpl sessionManager;
  @Inject
  Gson gson;
  private static String device_id = "";
  // @Inject ServiceFragContract.ServiceView view;
  private String currentLatitude, currentLongitude;
  @Inject
  Context mContext;
  private String TAG = ServiceFragContract.class.getSimpleName();
  private ServiceFragContract.ServiceView view;

  @Inject
  ServicesFragPresenter() {

  }
  @Override
  public void onGetCategory(String cityId) {

      Log.w(TAG, "onGetCategory: "+sessionManager.getLatitude() +"  "+sessionManager.getLongitude()+"  "+sessionManager.getIpAddress() );
    Observable<Response<ResponseBody>> responseObservable = lspServices.getCategories(
        LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),
        selLang, PLATFORM_ANDROID,sessionManager.getIpAddress(), "0",latitude,longitude,cityId);
    responseObservable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> result) {
            int code = result.code();
            JSONObject jsonObject;
            try {
              String resultBody = result.body() != null ? result.body().string() : null;
              String errorBody = result.errorBody() != null ? result.errorBody().string() : null;
              switch (code)  {
                case SUCCESS_RESPONSE:
                  if (resultBody != null && !resultBody.isEmpty()) {
                    sessionManager.setHomeScreenData(resultBody);
                    categorySuccess(resultBody);
                  }
                    onHideProgress();
                  break;
                case SESSION_EXPIRED:
                  if (errorBody != null && !errorBody.isEmpty()) {

                    ErrorHandel errorHandel = gson.fromJson(errorBody, ErrorHandel.class);
                    Log.w(TAG, "TokenHandler: " + errorBody + errorHandel.getData());
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()), sessionManager.getREFRESHAUTH(),
                        lspServices,
                        new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {
                            onHideProgress();
                            LSPApplication.getInstance().setAuthToken(sessionManager.getSID(),sessionManager.getSID(),newToken);
                            onGetCategory(cityId);

                          }

                          @Override
                          public void onFailureRefreshToken() {
                            if(view != null) {
                              view.onConnectionError(mContext.getResources().getString(R.string.pleaseCheckInternet));
                              onHideProgress();

                            }
                          }

                          @Override
                          public void sessionExpired(String msg) {
                            onHideProgress();
                            if(view != null)
                            view.onLogout(msg, sessionManager);
                          }
                        });
                  }
                  break;
                case NOT_FOUND:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      if(view != null)
                      view.onNotOperational(jsonObject.getString(MESSAGE));
                        AllCityResponse response = gson.fromJson(errorBody, AllCityResponse.class);
                        citySuccess(response);
                    }
                  }

                  onHideProgress();
                  break;
                case SESSION_LOGOUT:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    Log.w(TAG, "GetCategory: " + errorBody);
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      if(view != null)
                      view.onLogout(jsonObject.getString(MESSAGE), sessionManager);
                    }
                  }
                  onHideProgress();
                  break;
                default:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      if(view != null)
                      view.onError(jsonObject.getString(MESSAGE));
                    }
                  }
                  onHideProgress();
                  break;

              }
            } catch (IOException | JSONException e) {
              if(view != null) {
                view.onConnectionError(mContext.getResources().getString(R.string.pleaseCheckInternet));
                onHideProgress();

              }
              e.printStackTrace();
            }

          }

          @Override
          public void onError(Throwable e) {
            e.printStackTrace();
            onHideProgress();
            if (e instanceof NoConnectivityException) {
              Utility.printLog(TAG, e.getMessage());
              Utility.printLog(TAG, e.getLocalizedMessage());
              if(view != null) {
                view.onConnectionError(mContext.getResources().getString(R.string.pleaseCheckInternet));
              }
            }
          }

          @Override
          public void onComplete() {
            Utility.printLog(TAG, "This is me onComplete");
          }
        });
  }

    private void citySuccess(AllCityResponse response) {
        if (response != null && response.getData() != null) {
            callOnCityDataSet(response.getData());
        }
        }

    private void callOnCityDataSet(AllCityResponse.AllCityData allCities) {
        setDataCity(allCities.getAllCities());
    }

    @Override
  public void categorySuccess(String resultBody) {
    CategoryResponse response = gson.fromJson(resultBody, CategoryResponse.class);
    if (response != null && response.getData() != null) {
      CityData cityData = response.getData().getCityData();
      try {
        if (response.getData() != null && cityData != null) {
          homePageTagLine = response.getData().getHomePageTagLine();
          currencySymbol = response.getData().getCityData().getCurrencySymbol();
            currency = response.getData().getCityData().getCurrency();
          DISTANCEMATRIXUNIT = response.getData().getCityData().getDistanceMatrix();
          if (cityData.getDistanceMatrix() == 0) {
            distanceUnit = "Kms";
          } else {
            distanceUnit = "Miles";
          }

          callOnDataSet(response.getData().getCatArr(),response.getData().getAllCities(),response.getData().getInCallCategory(),response.getData().getOutCallCategory(),response.getData().getTeleCallCategory());
          paymentAbbr = Integer.parseInt(cityData.getCurrencyAbbr());
          if (cityData.getPaymentMode() != null) {
              paymentMode = cityData.getPaymentMode();
          }
          customerHomePageInterval =
              cityData.getCustomerFrequency().getCustomerHomePageInterval();
          stripeKeys = cityData.getStripeKeys();
            razorPayKey = (cityData.getPaymentGateways() != null && cityData.getPaymentGateways().size() > 0 && cityData.getPaymentGateways().get(0).getName().equals("razorpay")) ? cityData.getRazorPayKey() : "";
            if(Utility.isFromIndia(mContext))
            {
                sessionManager.setDefaultCardId("");
                sessionManager.setDefaultCardNum("");
                sessionManager.setDefaultCardName("");
            }
          GOOGLEKEY = cityData.getCustGoogleMapKeys();
          //ConstantsInteface.SERVER_KEY = cityData.getGoogleMapKey();
          sessionManager.setFcmTopicCity(cityData.getPushTopics().getCity());
          sessionManager.setFcmTopicAllCustomer(cityData.getPushTopics().getAllCustomers());
          sessionManager.setFcmTopicAllCity(cityData.getPushTopics().getAllCitiesCustomers());
          sessionManager.setLatitude(String.valueOf(cityData.getLocation().getLatitude()));
          sessionManager.setLongitude(String.valueOf(cityData.getLocation().getLongitude()));
          lat = cityData.getLocation().getLatitude();
          lng = cityData.getLocation().getLongitude();
          if (!"".equals(cityData.getPushTopics().getCity())) {
            FirebaseMessaging.getInstance().subscribeToTopic(cityData.getPushTopics().getCity());
          }
          if (!"".equals(cityData.getPushTopics().getAllCustomers())) {
           FirebaseMessaging.getInstance().subscribeToTopic(
                cityData.getPushTopics().getAllCustomers());
          }
          if (!"".equals(cityData.getPushTopics().getAllCitiesCustomers())) {
           FirebaseMessaging.getInstance().subscribeToTopic(
                cityData.getPushTopics().getAllCitiesCustomers());
          }

        }
      } catch (Exception e) {
        e.printStackTrace();
      }

    }

  }

  @Override
  public void onHideProgress() {
    if (view != null)
      view.onHideProgress();
  }

  private void callOnDataSet(ArrayList<CatDataArray> catArr,
                             ArrayList<CategoryCity> allCities, ArrayList<Category> inCallCategory, ArrayList<Category> outCallCategory, ArrayList<Category> teleCallCategory) {
      setDataOn(catArr);
      setDataCity(allCities);
      setDataIncall(inCallCategory);
      setDataOutCall(outCallCategory);
      setDataTelCall(teleCallCategory);
  }

  private void setDataTelCall(ArrayList<Category> teleCallCategory) {
    if (teleCallCategory.size() > 0) {
      if(view != null)
      view.onTeleCallService(teleCallCategory);
    } else {
      if(view != null)
      view.onNoInCallService();
    }
  }

  private void setDataOutCall(ArrayList<Category> outCallCategory) {
    if (outCallCategory.size() > 0) {
      view.onOutCallService(outCallCategory);
    } else {
      view.onNoOutCallService();
    }
  }

  private void setDataIncall(ArrayList<Category> inCallCategory) {
    if (inCallCategory.size() > 0) {
      if(view != null)
      view.onInCallCategory(inCallCategory);
    } else {
      if(view != null)
      view.onNoTeleCallService();
    }
  }

  private void setDataCity(ArrayList<CategoryCity> allCities) {
    if (allCities.size() > 0) {
      if(view != null)
      view.onSuccessCity(allCities);
    }
  }

  private void setDataOn(ArrayList<CatDataArray> catArr) {
    if (catArr.size() > 0) {
      if(view != null)
      view.onSuccess(catArr);
      onHideProgress();
    } else {
      if(view != null)
      view.onNotOperational("");
      onHideProgress();
    }
  }
  @Override
    public void getSession(SessionManagerImpl mSessionManager, Context mContext) {
      try {
        device_id = Utility.getDeviceId(mContext);
      } catch (Exception e) {
        e.printStackTrace();
      }
      Map<String, Object> jsonParams = new HashMap<>();
      jsonParams.put("ipAddress", mSessionManager.getIpAddress());
      jsonParams.put("devMake", DEVICE_MAKER);
      jsonParams.put("devModel", DEVICE_MODEL);
      jsonParams.put("deviceOsVersion", DEVICE_OS_VERSION);
      jsonParams.put("appVersion", APP_VERSION);
      jsonParams.put("deviceId", device_id);

      Observable<Response<ResponseBody>> obsResponseBody = lspServices.onToGetSession(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),
              selLang, PLATFORM_ANDROID,jsonParams);
      obsResponseBody.subscribeOn(Schedulers.io())
              .observeOn(AndroidSchedulers.mainThread())
              .subscribe(new Observer<Response<ResponseBody>>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(Response<ResponseBody> responseBodyResponse) {
                  int code = responseBodyResponse.code();
                  JSONObject jsonObject;
                  try {
                    String response =
                            responseBodyResponse.body() != null ? responseBodyResponse.body().string()
                                    : null;
                    String errorBody =
                            responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string()
                                    : null;
                    switch (code) {
                      case SUCCESS_RESPONSE:
                        break;
                      case SESSION_EXPIRED:
                        if (errorBody != null && !errorBody.isEmpty()) {

                          ErrorHandel errorHandel = gson.fromJson(errorBody, ErrorHandel.class);
                          Log.w(TAG, "TokenHandler: " + errorBody + errorHandel.getData());
                          RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()), mSessionManager.getREFRESHAUTH(),
                                  lspServices,
                                  new RefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                      LSPApplication.getInstance().setAuthToken(sessionManager.getSID(),sessionManager.getSID(),newToken);
                                      getSession(mSessionManager, mContext);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {

                                    }

                                    @Override
                                    public void sessionExpired(String msg) {

                                    }
                                  });
                        }
                        break;
                      default:
                        if (errorBody != null && !errorBody.isEmpty()) {
                          jsonObject = new JSONObject(errorBody);
                          jsonObject.getString(MESSAGE);
                          if (!jsonObject.getString(
                                  MESSAGE).isEmpty()) {

                          }
                        }
                        break;
                    }

                  } catch (Exception e) {
                    e.printStackTrace();
                  }

                }

                @Override
                public void onError(Throwable e) {
                  Utility.printLog(TAG, e.getMessage());
                  Utility.printLog(TAG, e.getLocalizedMessage());
                }

                @Override
                public void onComplete() {

                }
              });
    }


  @Override
  public void getCalls() {
    Observable<Response<ResponseBody>> obsResponseBody = lspServices.onTOGetcalls(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),
        selLang);
    obsResponseBody.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int code = responseBodyResponse.code();
            JSONObject jsonObject;
            try {
              String response =
                  responseBodyResponse.body() != null ? responseBodyResponse.body().string()
                      : null;
              String errorBody =
                  responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string()
                      : null;
              switch (code) {
                case SUCCESS_RESPONSE:

                  break;
                case SESSION_EXPIRED:
                  if (errorBody != null && !errorBody.isEmpty()) {

                    ErrorHandel errorHandel = gson.fromJson(errorBody, ErrorHandel.class);
                    Log.w(TAG, "TokenHandler: " + errorBody + errorHandel.getData());
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()), sessionManager.getREFRESHAUTH(),
                        lspServices,
                        new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {

                            LSPApplication.getInstance().setAuthToken(sessionManager.getSID(),sessionManager.getSID(),newToken);
                            getCalls();


                          }

                          @Override
                          public void onFailureRefreshToken() {

                          }

                          @Override
                          public void sessionExpired(String msg) {

                          }
                        });
                  }
                  break;
                default:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                    }
                  }
                  break;
              }

            } catch (Exception e) {
              e.printStackTrace();
            }

          }

          @Override
          public void onError(Throwable e) {
            Utility.printLog(TAG, e.getMessage());
            Utility.printLog(TAG, e.getLocalizedMessage());
          }

          @Override
          public void onComplete() {

          }
        });
  }


  @Override
  public void onPendingBooking() {
    Observable<Response<ResponseBody>> observable = lspServices.onTOGetPendingBooking(
        LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),
        selLang, PLATFORM_ANDROID);

    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            int code = responseBodyResponse.code();
            JSONObject jsonObject;
            try {
              String response =
                  responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
              String errorBody =
                  responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null;
              switch (code) {
                case SUCCESS_RESPONSE:
                  if (response != null && !response.isEmpty()) {
                    jsonObject = new JSONObject(response);
                    JSONArray arry = jsonObject.getJSONArray(DATA);
                    if (arry.length() > 0) {
                      if(view != null)
                      view.onPendingBooking(arry.getLong(0));
                    }
                  }
                  // onHideProgress();
                  break;
                case SESSION_EXPIRED:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    ErrorHandel errorHandel = gson.fromJson(response, ErrorHandel.class);
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()), sessionManager.getREFRESHAUTH(),
                        lspServices,
                        new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {
                            LSPApplication.getInstance().setAuthToken(sessionManager.getSID(),sessionManager.getSID(),newToken);
                            onPendingBooking();
                          }

                          @Override
                          public void onFailureRefreshToken() {

                          }

                          @Override
                          public void sessionExpired(String msg) {
                            //view.onLogout(msg);
                          }
                        });
                  }
                  break;
                case SESSION_LOGOUT:
                  if (errorBody != null && !errorBody.isEmpty()) {

                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      // view.onLogout(jsonObject.getString(MESSAGE),sessionManager);
                    }
                  }
                  // onHideProgress();
                  break;
                default:
                  if (errorBody != null && !errorBody.isEmpty()) {

                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      if(view != null)
                      view.onError(jsonObject.getString(MESSAGE));

                    }
                  }
                  // onHideProgress();
                  break;

              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
            }

          }

          @Override
          public void onError(Throwable e) {
            e.printStackTrace();
            if(view != null)
            view.onConnectionError(e.getMessage(), true);
          }

          @Override
          public void onComplete() {

          }
        });
  }

  @Override
  public void onPendingInvocieBooking() {

    Observable<Response<ResponseBody>> observable = lspServices.onTOGetPendingInvoiceBooking(
            LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),
        selLang, PLATFORM_ANDROID);

    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            int code = responseBodyResponse.code();
            JSONObject jsonObject;
            try {
              String response =
                  responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
              String errorBody =
                  responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null;
              switch (code) {
                case SUCCESS_RESPONSE:
                  if (response != null && !response.isEmpty()) {

                    jsonObject = new JSONObject(response);
                    JSONArray arry = jsonObject.getJSONArray(DATA);
                    if (arry.length() > 0) {
                      for (int j = 0; j < arry.length(); j++) {
                        JSONObject jItem = arry.getJSONObject(j);
                        long bookingId = jItem.getLong("bookingId");
                        if(view != null)
                        view.onPendingInvocieBooking(bookingId);
                      }
                    }
                  }
                  //  onHideProgress();
                  break;
                case SESSION_EXPIRED:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(sessionManager.getSID())/*LSPApplication.getInstance().getAuthToken(sessionManager.getSID())*/, sessionManager.getREFRESHAUTH(),
                        lspServices,
                        new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {

                            LSPApplication.getInstance().setAuthToken(sessionManager.getSID(),sessionManager.getSID(),newToken);
                            onPendingInvocieBooking();
                          }

                          @Override
                          public void onFailureRefreshToken() {

                          }

                          @Override
                          public void sessionExpired(String msg) {
                            //  view.onLogout(msg);

                          }
                        });
                  }
                  // onHideProgress();
                  break;
                case SESSION_LOGOUT:
                  if (errorBody != null && !errorBody.isEmpty()) {

                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      //view.onLogout(jsonObject.getString(MESSAGE),sessionManager);
                    }
                  }
                  //  onHideProgress();
                  break;
                default:
                  if (errorBody != null && !errorBody.isEmpty()) {

                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      if(view != null)
                      view.onError(jsonObject.getString(MESSAGE));
                    }
                  }
                  // onHideProgress();
                  break;


              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
            }

          }

          @Override
          public void onError(Throwable e) {
            if(view != null)
            view.onConnectionError(e.getMessage(), true);

          }

          @Override
          public void onComplete() {

          }
        });
  }

  @Override
  public void onTogetServerTime() {

    Observable<Response<ResponseBody>> observable = lspServices.onTogetServerTime(
        selLang, PLATFORM_ANDROID);

    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            int code = responseBodyResponse.code();
            try {
              String response =
                  responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
              if (code == SUCCESS_RESPONSE) {
                if (response != null && !response.isEmpty()) {

                  serverTime = new JSONObject(
                      response).getLong(
                      DATA);
                }
                diffServerTime = System.currentTimeMillis() - (serverTime * 1000);
              }

            } catch (IOException | JSONException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null)
            view.onConnectionError(e.getMessage(), true);
          }

          @Override
          public void onComplete() {

          }
        });
  }

  @Override
  public void attachView(ServiceFragContract.ServiceView view) {
    this.view = view;
  }

  @Override
  public void detachView() {
    view = null;
  }

  @Override
  public void showStatusBar(LinearLayout llHOmeAddress, boolean b) {

    float alpha;
    int height;
    long duration;
    if (b) {
      alpha = 1.0f;
      height = 0;
      duration = 50;
    } else {
      height = llHOmeAddress.getHeight();
      alpha = 0.0f;
      duration = 300;
    }

    llHOmeAddress.animate()
        .translationY(height)
        .alpha(alpha)
        .setDuration(duration)
        .setListener(new AnimatorListenerAdapter() {
          @Override
          public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            if (b) {
              llHOmeAddress.setVisibility(View.VISIBLE);
            } else {
              llHOmeAddress.setVisibility(View.GONE);
            }

          }
        });
  }


}

