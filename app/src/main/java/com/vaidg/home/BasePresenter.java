package com.vaidg.home;

/**
 * <h2>BasePresenter</h2>
 * Created by Ali on 1/29/2018.
 */

public interface BasePresenter<V>
{
    /**
     * <h2>attachView</h2>
     * Binds presenter with a view when resumed. The Presenter will perform initialization here.
     * @param view the view associated with this presenter
     */
    void attachView(V view);

    /**
     * <h2>detachView</h2>
     * Drops the reference to the view when destroyed
     */
    void detachView();
}
