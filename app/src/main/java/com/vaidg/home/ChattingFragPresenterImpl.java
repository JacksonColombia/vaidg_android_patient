package com.vaidg.home;

import android.util.Log;

import com.vaidg.utilities.LSPApplication;
import com.google.gson.Gson;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;
import com.pojo.BookingChatPojo;
import com.pojo.ErrorHandel;
import com.utility.RefreshToken;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by Ali on 5/28/2018.
 */
public class ChattingFragPresenterImpl implements ChattingFragPresenter.Presenter
{

    private ChattingFragPresenter.ViewPresent view;
    @Inject
    SessionManagerImpl manager;
    @Inject
    LSPServices lspServices;
    @Inject
    Gson gson;

    @Inject
    public ChattingFragPresenterImpl() {
    }

    @Override
    public void onChattingActiveNonActive()
    {
        Observable<Response<ResponseBody>> responseObservable = lspServices.getBookingChat(LSPApplication.getInstance().getAuthToken(manager.getSID()), Constants.selLang,Constants.PLATFORM_ANDROID);
        responseObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse)
                    {
                            int code =  responseBodyResponse.code();
                            String response;
                        JSONObject jsonObject;
                            try
                            {
                                switch (code)
                                {
                                    case Constants.SUCCESS_RESPONSE:
                                        response = responseBodyResponse.body().string();
                                        Log.d("TAG", "onNextBOOKINGCHAT: "+response);
                                        BookingChatPojo resp = gson.fromJson(response,BookingChatPojo.class);
                                        if(view != null) {
                                            view.onSuccess(resp.getData());
                                            view.onHideProgress();
                                        }
                                        break;
                                    case Constants.SESSION_LOGOUT:
                                        response = responseBodyResponse.errorBody().string();
                                         jsonObject = new JSONObject(response);
                                        if(view!=null) {
                                            view.onHideProgress();
                                            view.onLogout(jsonObject.getString("message"),manager);
                                        }
                                        break;

                                    case Constants.SESSION_EXPIRED:
                                        response = responseBodyResponse.errorBody().string();
                                        ErrorHandel errorHandel = gson.fromJson(response, ErrorHandel.class);
                                        RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),manager.getREFRESHAUTH(),lspServices, new RefreshToken.RefreshTokenImple() {
                                            @Override
                                            public void onSuccessRefreshToken(String newToken) {

                                                LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                                onChattingActiveNonActive();

                                            }

                                            @Override
                                            public void onFailureRefreshToken() {

                                            }

                                            @Override
                                            public void sessionExpired(String msg)
                                            {
                                                if(view!=null) {
                                                    view.onHideProgress();
                                                    view.onLogout(msg,manager);
                                                }
                                            }
                                        });
                                        break;
                                }
                            }catch (IOException | JSONException e)
                            {
                                e.printStackTrace();
                                if(view != null) {
                                    view.onErrorNotConnected(e.getMessage());
                                    view.onHideProgress();
                                }
                            }
                    }

                    @Override
                    public void onError(Throwable e)
                    {
                        if(view != null) {
                            view.onErrorNotConnected(e.getMessage());
                            view.onHideProgress();
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    @Override
    public void attachView(ChattingFragPresenter.ViewPresent view)
    {
        this.view = view;
    }

    @Override
    public void detachView() {
        view = null;
    }
}
