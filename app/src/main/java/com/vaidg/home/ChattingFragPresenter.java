package com.vaidg.home;

import com.pojo.BookingChatPojo;

/**
 * Created by Ali on 5/28/2018.
 */
public interface ChattingFragPresenter
{
    interface Presenter extends BasePresenter<ViewPresent>
    {
        void onChattingActiveNonActive();
    }
    interface ViewPresent extends BaseView
    {
       void onSuccess(BookingChatPojo.BookingChatData data);

        void onErrorNotConnected(String message);
    }
}
