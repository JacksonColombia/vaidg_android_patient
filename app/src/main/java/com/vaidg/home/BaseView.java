package com.vaidg.home;

import com.vaidg.utilities.SessionManagerImpl;

/**
 * Created by Ali on 1/29/2018.
 */

public interface BaseView<T>
{
    void onSessionExpired();
    void onLogout(String message, SessionManagerImpl sessionManager);
    void onError(String error);
    void onConnectionError(String connectionError);
    void onShowProgress();
    void onHideProgress();

}
