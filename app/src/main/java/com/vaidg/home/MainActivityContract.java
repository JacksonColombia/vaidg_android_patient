package com.vaidg.home;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

/**
 * Created by Ali on 1/24/2018.
 */

public interface MainActivityContract
{
    interface MainPresenter
    {
       void onFragmentTransition(String TAG, FragmentManager fragmentManager, Fragment fragmentService
               , Fragment fragmentProject, Fragment fragmentProfile, Fragment chatFragment, int frameId);

    }

    interface MainServicePresenter
    {
        void onPermissionCheck();
    }
}
