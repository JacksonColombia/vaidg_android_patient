package com.vaidg.otp;

/**
 * Created by Pramod on 15/12/17.
 */

public interface OtpPresenter {

    void verifyOtp(String otp, String sid, String flag);

    void verifyPhone(String otp, String sid, String flag);

    void resendOtp(String otp, String userId, int trigger, boolean resend_flag);

    /**
     * <h2>checkForKeystore</h2>
     * This method is used to check for keystore
     */
    void checkForKeystore();
    /**
     * <h2>generateEncryptionKeyPostLollipop</h2>
     * This method is used to generate the encryption key required for encryption for post lollipop
     * @param exportedPublicKey
     */
    void generateEncryptionPublicKeyPostLollipop(String exportedPublicKey);

    /**
     * <h2>generateEncryptionKeyPostLollipop</h2>
     * This method is used to generate the encryption key required for encryption for post lollipop
     * @param exportedPrivateKey
     */
    void generateEncryptionPrivateKeyPostLollipop(String exportedPrivateKey);
    /**
     * <h2>generateEncryptionKeyPostLollipop</h2>
     * This method is used to generate the encryption key required for encryption for pre marshmallow
     * @param exportedPublicKey
     */
    void generateEncryptionPublicKeyPreMarshMallow(String exportedPublicKey);
    /**
     * <h2>generateEncryptionKeyPostLollipop</h2>
     * This method is used to generate the encryption key required for encryption for pre marshmallow
     * @param exportedPrivateKey
     */
    void generateEncryptionPrivateKeyPreMarshMallow(String exportedPrivateKey);
    /**
     * <h2>encryptPrivateKeyPostLollipop</h2>
     * This method is used to encrypt the data for post lollipop
     * @param input data to be encrypted
     */
    void encryptPrivateKeyPostLollipop(String input);
    /**
     * <h2>encryptPrivateKeyPostLollipop</h2>
     * This method is used to encrypt the data for pre marshmallow
     * @param input data to be encrypted
     */
    void encryptPrivateKeyPreMarshMallow(String input);
    /**
     * <h2>encryptPublicKeyPostLollipop</h2>
     * This method is used to encrypt the data for post lollipop
     * @param input data to be encrypted
     */
    void encryptPublicKeyPostLollipop(String input);
    /**
     * <h2>encryptPublicKeyPostLollipop</h2>
     * This method is used to encrypt the data for pre marshmallow
     * @param input data to be encrypted
     */
    void encryptPublicKeyPreMarshMallow(String input);

}
