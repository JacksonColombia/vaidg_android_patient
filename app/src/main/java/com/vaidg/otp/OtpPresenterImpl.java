package com.vaidg.otp;

import static com.vaidg.utilities.Constants.AES_MODE;
import static com.vaidg.utilities.Constants.ANDROID_KEYSTORE;
import static com.vaidg.utilities.Constants.BASE_AUTH_PASSWORD;
import static com.vaidg.utilities.Constants.BASE_AUTH_USERNAME;
import static com.vaidg.utilities.Constants.DIALOGMESSAGESHOW_YES;
import static com.vaidg.utilities.Constants.FIXED_IV;
import static com.vaidg.utilities.Constants.IV_PARAMETER_SPEC;
import static com.vaidg.utilities.Constants.KEY_PRIVATEALIAS;
import static com.vaidg.utilities.Constants.KEY_PUBLICALIAS;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.RSA_MODE;
import static com.vaidg.utilities.Constants.USERTYPE;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;
import android.util.Log;
import com.vaidg.model.OtpData;
import com.vaidg.model.ServerOtpResponse;
import com.vaidg.model.ServerResponse;
import com.vaidg.networking.BasicAuthentication;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.google.firebase.messaging.FirebaseMessaging;
import com.pojo.RequestPojo.OtpValidationRequestPojo;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;
import javax.security.auth.x500.X500Principal;

import org.json.JSONObject;
import retrofit2.Response;

/**
 * Created by ${3Embed} on 4/11/17.
 */

public class OtpPresenterImpl implements OtpPresenter {

  @Inject
  SessionManagerImpl manager;
  @Inject
  Context mContext;
  @Inject
  OtpView view;

  private KeyStore keyStore;
  private String exportedPrivateKey = "", exportedPublicKey = "";

  @Inject
  OtpPresenterImpl() {
  }

  @Override
  public void resendOtp(final String otp, final String sid, final int trigger, final boolean resend_flag) {
    //  LSPServices service = ServiceFactory.createRetrofitService(LSPServices.class);
    Log.e("resendOTP", " SID got is :: " + sid);
    if (view != null) {
      if (resend_flag) {
        view.showProgress("RESEND");
      } else {
        view.showProgress("OTP_VERIFY");
      }
    }
    LSPServices lspServices = BasicAuthentication.ServiceGenerator.createService(LSPServices.class, BASE_AUTH_USERNAME, BASE_AUTH_PASSWORD);
    Map<String, Object> jsonParams = new HashMap<>();
       /* Map<String, Object> jsonParams = new ArrayMap<>();
//put something inside the map, could be null*/
    jsonParams.put("userId", sid);
    jsonParams.put(USERTYPE, 1);
    jsonParams.put("trigger", trigger);

    Observable<Response<ServerResponse>> response = lspServices.resendOtp(Constants.selLang, PLATFORM_ANDROID, jsonParams);
    response.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ServerResponse>>() {

              @Override
              public void onSubscribe(Disposable d) {

              }

              @Override
              public void onNext(Response<ServerResponse> value) {
                Log.e("RSND", "Resend OTP ::  " + value.code() + "  msg  " + value.message());
                if (200 == value.code()) {
                  Log.e("TAG", value.body().getMessage());
                  //view.setErrorMsg(value.body().getGuestLoginMessage());
                  if (view != null) {
                    view.hideProgress();
                  }
                  if (trigger == 2) {
                    if (resend_flag) {
                      Log.e("TAG", "Resent OTP");
                    } else {
                      if ("".equals(otp)) {
                        Log.e("PRAMOD", "For resend OTP");
                      } else {
                        verifyOtp(otp, sid, DIALOGMESSAGESHOW_YES);
                      }
                      //view.navigateToChangePwd(sid);
                    }
                  } /*else {
                                view.navigateToLogin();
                            }*/
                } else {
                  Log.e("TAG", value.message());
                  if (view != null) {
                    view.hideProgress();
                  }
                  try {
                    if (value.errorBody() != null) {
                      JSONObject errJson = new JSONObject(value.errorBody().toString());
                      if (view != null) {
                        view.setErrorMsg(errJson.getString("message"));
                      }
                    }
                  } catch (Exception e) {
                    e.printStackTrace();
                  }
                }
              }

              @Override
              public void onError(Throwable e) {
                Log.e("Error", "error" + e.getMessage());
                if (view != null) {
                  view.hideProgress();
                }
                e.printStackTrace();
                //signUpView.onError(e.getGuestLoginMessage());
              }

              @Override
              public void onComplete() {

              }
            });

  }

  @Override
  public void verifyOtp(String otp, final String sid, final String flag) {
    // Retrofit instance creation
    if (view != null) {
      if (DIALOGMESSAGESHOW_YES.equals(flag)) {
        Log.e("PRAMOD", "Forgot PWD");
      } else {
        view.showProgress("OTP_VERIFY");
      }
    }
    LSPServices lspServices = BasicAuthentication.ServiceGenerator.createService(LSPServices.class, BASE_AUTH_USERNAME, BASE_AUTH_PASSWORD);

    // LSPServices service = ServiceFactory.createRetrofitService(LSPServices.class);
    Log.e("VerifyOTP", " SID got is :: " + sid);
    int trigger = 2;
    if ("change_phone".equals(flag)) {
      trigger = 3;
    }
    OtpValidationRequestPojo otpRequest = new OtpValidationRequestPojo(otp, sid, trigger, 1);

    Observable<Response<ServerResponse>> response = lspServices.verifyOtp(Constants.selLang, Constants.PLATFORM_ANDROID, otpRequest);

    response.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ServerResponse>>() {
              @Override
              public void onSubscribe(Disposable d) {

              }

              @Override
              public void onNext(Response<ServerResponse> value) {

                Log.e("code", "OTP req ::  " + value.code() + "  msg  " + value.message());
                if (200 == value.code()) {
                  Log.e("TAG", value.body().getMessage());
                  if (view != null) {
                    view.setError();
                    view.hideProgress();
                  }
                    if (flag == null || "".equals(flag)) {
                      if (view != null) {
                        view.navigateToLogin();
                      }
                  } else if ("change_phone".equals(flag)) {
                      if (view != null) {
                        view.navToProfile();
                      }
                  } else {
                      if (view != null) {
                        view.navigateToChangePwd(sid);
                      }
                    }
                } else {
                  if (view != null) {
                    view.hideProgress();
                  }
                  try {
                    if (value.errorBody() != null) {
                      JSONObject errJson = new JSONObject(value.errorBody().string());
                      if (view != null) {
                        view.setErrorMsg(errJson.getString("message"));
                      }
                    }
                  } catch (Exception e) {
                    e.printStackTrace();
                  }
                }

              }

              @Override
              public void onError(Throwable e) {
                if (view != null) {
                  view.hideProgress();
                }
                Log.e("Error", "error" + e.getMessage());
                e.printStackTrace();
              }

              @Override
              public void onComplete() {

              }
            });


  }

  @Override
  public void verifyPhone(final String otp, String sid, final String flag) {
    // Retrofit instance creation
    if (view != null) {
      view.showProgress("PHONE_VERIFY");
    }
    LSPServices lspServices = BasicAuthentication.ServiceGenerator.createService(LSPServices.class, BASE_AUTH_USERNAME, BASE_AUTH_PASSWORD);
    Log.e("VerifyPhone", " SID got is :: " + sid);
    Map<String, Object> verifyPhone = new HashMap<>();
    verifyPhone.put("code", otp);
    verifyPhone.put("userId", sid);

    Observable<Response<ServerOtpResponse>> response = lspServices.verifyPhone(Constants.selLang, Constants.PLATFORM_ANDROID, verifyPhone);

    response.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ServerOtpResponse>>() {

              @Override
              public void onSubscribe(Disposable d) {

              }

              @Override
              public void onNext(Response<ServerOtpResponse> value) {
                Log.e("code", "Verify otp req ::  " + value.code() + "  msg  " + value.message());
                if (200 == value.code()) {
                  ServerOtpResponse serverOtpResponse = value.body();
                  if (serverOtpResponse != null) {
                    Log.e("TAG", serverOtpResponse.getMessage());
                    //view.setError();
                    OtpData otpData = serverOtpResponse.getData();
                    if (otpData != null) {
                      String auth = otpData.getToken().getAccessToken();
                      String refreshtoken = otpData.getToken().getRefreshToken();

                                    /*if("profile".equals(flag))
                                        view.navToProfile();*/
                      if ("change_phone".equals(flag)) {
                        if (view != null) {
                          view.navToProfile();
                        }
                      } else {
                        if (view != null) {
                          view.navToHome(auth);
                        }
                        manager.setProfilePicUrl(otpData.getProfilePic());
                        manager.setSID(otpData.getSid());
                        manager.setREFRESHAUTH(refreshtoken);
                        manager.setReferralCode(otpData.getReferralCode());
                        manager.setGuestLogin(false);
                        manager.setRegisterId(otpData.getRequester_id());
                        manager.setFirstName(otpData.getFirstName());
                        manager.setPatientGender(otpData.getGender());
                        manager.setPatientDateOfBirth(otpData.getDateOfBirth());
                        manager.setLastName(otpData.getLastName());
                        manager.setEmail(otpData.getEmail());
                        LSPApplication.getInstance().setAuthToken(sid, sid, auth);
                        manager.setMobileNo(otpData.getPhone());
                        manager.setFcmTopic(otpData.getFcmTopic());
                        manager.setCountryCode(otpData.getCountryCode());
                        manager.setCountrySymbol(otpData.getCountrySymbol());
                        manager.setCallToken(otpData.getCall().getAuthToken());
                        manager.setVirgilPrivateKey(otpData.getPrivateKey());
                        manager.setVirgilPublicKey(otpData.getPublickKey());
                        if (!manager.getFcmTopic().equals(""))
                          FirebaseMessaging.getInstance().subscribeToTopic(manager.getFcmTopic());
                      }
                      checkForKeystore();

                    }
                    if (view != null) {
                      view.hideProgress();
                    }
                  }

                } else {
                  if (view != null) {
                    view.hideProgress();
                  }
                  try {
                    if (value.errorBody() != null) {
                      JSONObject errJson = new JSONObject(value.errorBody().string());
                      if (view != null) {
                        view.setErrorMsg(errJson.getString("message"));
                      }
                    }
                  } catch (Exception e) {
                    e.printStackTrace();
                  }
                }
              }

              @Override
              public void onError(Throwable e) {
                if (view != null) {
                  view.hideProgress();
                }
                Log.e("Error", "error" + e.getMessage());
                e.printStackTrace();
              }

              @Override
              public void onComplete() {

              }
            });

  }



  @Override
  public void checkForKeystore() {
    try {
      keyStore = KeyStore.getInstance(ANDROID_KEYSTORE);
      keyStore.load(null);
      checkForVersion();
    } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
      e.printStackTrace();
    }
    // if (!keyStore.containsAlias(KEY_PRIVATEALIAS)) {

    //}
  }


  public void checkForVersion() {

    if (manager.getVirgilPrivateKey() != null && !manager.getVirgilPrivateKey().isEmpty()) {
      // Export private key
      exportedPrivateKey = manager.getVirgilPrivateKey();
    }
    if (manager.getVirgilPublicKey() != null && !manager.getVirgilPublicKey().isEmpty()) {
      // Export public key
      exportedPublicKey = manager.getVirgilPublicKey();
    }

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      generateEncryptionPrivateKeyPostLollipop(exportedPrivateKey);
      generateEncryptionPublicKeyPostLollipop(exportedPublicKey);
    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
      generateEncryptionPrivateKeyPreMarshMallow(exportedPrivateKey);
      generateEncryptionPublicKeyPreMarshMallow(exportedPublicKey);
    }
  }


  /**
   * <hg2>generateAndStorePublicKeyAES</hg2>
   * This method is useed to generate the key for pre marsh mallow and store
   */
  private void generateAndStorePublicKeyAES() {
    String encryptedKeyB64 = manager.getEncryptionPublicKey();
    if (encryptedKeyB64 == null) {
      byte[] key = new byte[16];
      SecureRandom secureRandom = new SecureRandom();
      secureRandom.nextBytes(key);
      byte[] encryptedKey;
      try {
        encryptedKey = rsaPublicKeyEncrypt(key);
        encryptedKeyB64 = Base64.encodeToString(encryptedKey, Base64.DEFAULT);
        manager.storeEncryptionPublicKey(encryptedKeyB64);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * <hg2>generateAndStorePrivateKeyAES</hg2>
   * This method is useed to generate the key for pre marsh mallow and store
   */
  private void generateAndStorePrivateKeyAES() {
    String encryptedKeyB64 = manager.getEncryptionPrivateKey();
    if (encryptedKeyB64 == null) {
      byte[] key = new byte[16];
      SecureRandom secureRandom = new SecureRandom();
      secureRandom.nextBytes(key);
      byte[] encryptedKey;
      try {
        encryptedKey = rsaPrivateKeyEncrypt(key);
        encryptedKeyB64 = Base64.encodeToString(encryptedKey, Base64.DEFAULT);
        manager.storeEncryptionPrivateKey(encryptedKeyB64);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * <h2>rsaPublicKeyEncrypt</h2>
   * This method is used to do the rsa encrypt
   *
   * @param secret takes secret bytes as input
   * @return returns the RSA encrypt byts
   * @throws Exception throws an exception
   */
  private byte[] rsaPublicKeyEncrypt(byte[] secret) throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PUBLICALIAS, null);
    // Encrypt the text
    Cipher inputCipher = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
    inputCipher.init(Cipher.ENCRYPT_MODE, privateKeyEntry.getCertificate().getPublicKey());
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, inputCipher);
    cipherOutputStream.write(secret);
    cipherOutputStream.close();
    return outputStream.toByteArray();
  }

  /**
   * <h2>rsaPrivateKeyEncrypt</h2>
   * This method is used to do the rsa encrypt
   *
   * @param secret takes secret bytes as input
   * @return returns the RSA encrypt byts
   * @throws Exception throws an exception
   */
  private byte[] rsaPrivateKeyEncrypt(byte[] secret) throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PRIVATEALIAS, null);
    // Encrypt the text
    Cipher inputCipher = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
    inputCipher.init(Cipher.ENCRYPT_MODE, privateKeyEntry.getCertificate().getPublicKey());
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, inputCipher);
    cipherOutputStream.write(secret);
    cipherOutputStream.close();
    return outputStream.toByteArray();
  }


  /**
   * <h2>getSecretPrivateKeyPostLollipop</h2>
   * This method is used to get the secret key for post lollipop device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPrivateKeyPostLollipop() throws Exception {
    Key s = keyStore.getKey(KEY_PRIVATEALIAS, null);
    Log.i("TEST", "key " + s.toString());
    return keyStore.getKey(KEY_PRIVATEALIAS, null);
  }

  /**
   * <h2>getSecretPublicKeyPostLollipop</h2>
   * This method is used to get the secret key for post lollipop device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPublicKeyPostLollipop() throws Exception {
    Key s = keyStore.getKey(KEY_PUBLICALIAS, null);
    Log.i("TEST", "key " + s.toString());
    return keyStore.getKey(KEY_PUBLICALIAS, null);
  }

  /**
   * <h2>getSecretPrivateKeyPreMarshMallow</h2>
   * This method is used to get the secret key for pre marsh mallow device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPrivateKeyPreMarshMallow() throws Exception {
    String enryptedKeyB64 = manager.getEncryptionPrivateKey();
    Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
    // need to check null, omitted here
    byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
    byte[] key = rsaPrivateKeyDecrypt(encryptedKey);
    Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
    return new SecretKeySpec(key, "AES");
  }

  /**
   * <h2>getSecretPublicKeyPreMarshMallow</h2>
   * This method is used to get the secret key for pre marsh mallow device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPublicKeyPreMarshMallow() throws Exception {
    String enryptedKeyB64 = manager.getEncryptionPublicKey();
    Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
    // need to check null, omitted here
    byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
    byte[] key = rsaPublicKeyDecrypt(encryptedKey);
    Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
    return new SecretKeySpec(key, "AES");
  }


  private byte[] rsaPrivateKeyDecrypt(byte[] encrypted) throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PRIVATEALIAS, null);
    Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
    output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
    CipherInputStream cipherInputStream = new CipherInputStream(
            new ByteArrayInputStream(encrypted), output);
    ArrayList<Byte> values = new ArrayList<>();
    int nextByte;
    while ((nextByte = cipherInputStream.read()) != -1) {
      values.add((byte) nextByte);
    }

    byte[] bytes = new byte[values.size()];
    for (int i = 0; i < bytes.length; i++) {
      bytes[i] = values.get(i);
    }
    return bytes;
  }

  private byte[] rsaPublicKeyDecrypt(byte[] encrypted) throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PUBLICALIAS, null);
    Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
    output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
    CipherInputStream cipherInputStream = new CipherInputStream(
            new ByteArrayInputStream(encrypted), output);
    ArrayList<Byte> values = new ArrayList<>();
    int nextByte;
    while ((nextByte = cipherInputStream.read()) != -1) {
      values.add((byte) nextByte);
    }

    byte[] bytes = new byte[values.size()];
    for (int i = 0; i < bytes.length; i++) {
      bytes[i] = values.get(i);
    }
    return bytes;
  }

  @SuppressLint("NewApi")
  @Override
  public void generateEncryptionPrivateKeyPostLollipop(String exportedPrivateKey) {
    KeyGenerator keyGenerator = null;
    try {
      keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEYSTORE);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (NoSuchProviderException e) {
      e.printStackTrace();
    }
    try {
      assert keyGenerator != null;
      keyGenerator.init
              (new KeyGenParameterSpec.Builder(KEY_PRIVATEALIAS,
                      KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                      .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                      .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                      .setRandomizedEncryptionRequired(false)
                      .build());
    } catch (InvalidAlgorithmParameterException e) {
      e.printStackTrace();
    }
    keyGenerator.generateKey();
    createVirgilPrivateKey(exportedPrivateKey);
  }


  @SuppressLint("NewApi")
  @Override
  public void generateEncryptionPublicKeyPostLollipop(String exportedPublicKey) {
    KeyGenerator keyGenerator = null;
    try {
      keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEYSTORE);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (NoSuchProviderException e) {
      e.printStackTrace();
    }
    try {
      assert keyGenerator != null;
      keyGenerator.init
              (new KeyGenParameterSpec.Builder(KEY_PUBLICALIAS,
                      KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                      .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                      .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                      .setRandomizedEncryptionRequired(false)
                      .build());
    } catch (InvalidAlgorithmParameterException e) {
      e.printStackTrace();
    }
    keyGenerator.generateKey();
    createVirgilPublicKey(exportedPublicKey);
  }

  @SuppressLint("NewApi")
  @Override
  public void generateEncryptionPrivateKeyPreMarshMallow(String exportedPrivateKey) {
    // Generate a key pair for encryption
    Calendar start = Calendar.getInstance();
    Calendar end = Calendar.getInstance();
    end.add(Calendar.YEAR, 30);
    KeyPairGeneratorSpec spec;
    spec = new KeyPairGeneratorSpec.Builder(mContext)
            .setAlias(KEY_PRIVATEALIAS)
            .setSubject(new X500Principal("CN=" + KEY_PRIVATEALIAS))
            .setSerialNumber(BigInteger.TEN)
            .setStartDate(start.getTime())
            .setEndDate(end.getTime())
            .build();

    KeyPairGenerator kpg = null;
    try {
      kpg = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, ANDROID_KEYSTORE);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (NoSuchProviderException e) {
      e.printStackTrace();
    }
    try {
      assert kpg != null;
      kpg.initialize(spec);
    } catch (InvalidAlgorithmParameterException e) {
      e.printStackTrace();
    }
    kpg.generateKeyPair();

    generateAndStorePrivateKeyAES();


    createVirgilPrivateKey(exportedPrivateKey);
  }

  @SuppressLint("NewApi")
  @Override
  public void generateEncryptionPublicKeyPreMarshMallow(String exportedPublicKey) {
    // Generate a key pair for encryption
    Calendar start = Calendar.getInstance();
    Calendar end = Calendar.getInstance();
    end.add(Calendar.YEAR, 30);
    KeyPairGeneratorSpec spec;
    spec = new KeyPairGeneratorSpec.Builder(mContext)
            .setAlias(KEY_PUBLICALIAS)
            .setSubject(new X500Principal("CN=" + KEY_PUBLICALIAS))
            .setSerialNumber(BigInteger.TEN)
            .setStartDate(start.getTime())
            .setEndDate(end.getTime())
            .build();

    KeyPairGenerator kpg = null;
    try {
      kpg = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, ANDROID_KEYSTORE);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (NoSuchProviderException e) {
      e.printStackTrace();
    }
    try {
      assert kpg != null;
      kpg.initialize(spec);
    } catch (InvalidAlgorithmParameterException e) {
      e.printStackTrace();
    }
    kpg.generateKeyPair();

    generateAndStorePublicKeyAES();

    createVirgilPublicKey(exportedPublicKey);
  }

  private void createVirgilPrivateKey(String exportedPrivateKey) {
/*
    try {
      CryptographyExample cryptographyExample = new CryptographyExample();
      // Generate keys
      VirgilCrypto crypto = new VirgilCrypto();
      VirgilKeyPair keyPair = cryptographyExample.keyGeneration();
      // Export private key
      exportedPrivateKey = cryptographyExample.exportPrivateKey(keyPair, crypto);
      // Export public key
      exportedPublicKey = cryptographyExample.exportPublicKey(keyPair, crypto);
    } catch (CryptoException e) {
      e.printStackTrace();
    }
*/
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      encryptPrivateKeyPostLollipop(exportedPrivateKey);
    } else {
      encryptPrivateKeyPreMarshMallow(exportedPrivateKey);
    }
  }

  private void createVirgilPublicKey(String exportedPublicKey) {
/*
    try {
      CryptographyExample cryptographyExample = new CryptographyExample();
      // Generate keys
      VirgilCrypto crypto = new VirgilCrypto();
      VirgilKeyPair keyPair = cryptographyExample.keyGeneration();
      // Export private key
      exportedPrivateKey = cryptographyExample.exportPrivateKey(keyPair, crypto);
      // Export public key
      exportedPublicKey = cryptographyExample.exportPublicKey(keyPair, crypto);
    } catch (CryptoException e) {
      e.printStackTrace();
    }
*/
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      encryptPublicKeyPostLollipop(exportedPublicKey);
    } else {
      encryptPublicKeyPreMarshMallow(exportedPublicKey);
    }
  }

  @SuppressLint("NewApi")
  @Override
  public void encryptPrivateKeyPostLollipop(String input) {
    Cipher c;
    try {
      c = Cipher.getInstance(AES_MODE);
      c.init(Cipher.ENCRYPT_MODE, getSecretPrivateKeyPostLollipop(), new GCMParameterSpec(128,
              FIXED_IV.getBytes()));
      byte[] encodedBytes = c.doFinal(input.getBytes(StandardCharsets.US_ASCII));

      String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
      Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
      manager.setVirgilPrivateKey(encryptedBase64Encoded);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @SuppressLint("NewApi")
  @Override
  public void encryptPublicKeyPostLollipop(String input) {
    Cipher c;
    try {
      c = Cipher.getInstance(AES_MODE);
      c.init(Cipher.ENCRYPT_MODE, getSecretPublicKeyPostLollipop(), new GCMParameterSpec(128,
              FIXED_IV.getBytes()));
      byte[] encodedBytes = c.doFinal(input.getBytes(StandardCharsets.US_ASCII));

      String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
      Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
      manager.setVirgilPublicKey(encryptedBase64Encoded);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void encryptPrivateKeyPreMarshMallow(String input) {
    Cipher c;
    try {
      c = Cipher.getInstance(AES_MODE, "BC");
      c.init(Cipher.ENCRYPT_MODE, getSecretPrivateKeyPreMarshMallow(), IV_PARAMETER_SPEC);
      byte[] encodedBytes = c.doFinal(input.getBytes());
      String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
      Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
      manager.setVirgilPrivateKey(encryptedBase64Encoded);
      manager.setVirgilPrivateKeyT(encryptedBase64Encoded);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void encryptPublicKeyPreMarshMallow(String input) {
    Cipher c;
    try {
      c = Cipher.getInstance(AES_MODE, "BC");
      c.init(Cipher.ENCRYPT_MODE, getSecretPublicKeyPreMarshMallow(), IV_PARAMETER_SPEC);
      byte[] encodedBytes = c.doFinal(input.getBytes());
      String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
      Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
      manager.setVirgilPublicKey(encryptedBase64Encoded);
      manager.setVirgilPublicKeyT(encryptedBase64Encoded);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
