package com.vaidg.otp;

import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.vaidg.Login.LoginActivity;
import com.vaidg.R;
import com.vaidg.changepassword.ChangePwdActivity;
import com.vaidg.home.MainActivity;
import com.vaidg.profile.ProfileActivity;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.MySMSBroadcastReceiver;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import dagger.android.support.DaggerAppCompatActivity;

import static com.vaidg.utilities.Constants.CHANGE_PHONE;
import static com.vaidg.utilities.Constants.COMINGFROM;
import static com.vaidg.utilities.Constants.COMMING_FROM;
import static com.vaidg.utilities.Constants.COUNTDOWN_INTERVAL;
import static com.vaidg.utilities.Constants.COUNTRYCODE;
import static com.vaidg.utilities.Constants.COUNTRYSYMBOl;
import static com.vaidg.utilities.Constants.FORGOTPWD;
import static com.vaidg.utilities.Constants.MILLIS_IN_FUTURE;
import static com.vaidg.utilities.Constants.MOBILENUMBER;
import static com.vaidg.utilities.Constants.OTPVERIFY;
import static com.vaidg.utilities.Constants.PHONEVERIFY;
import static com.vaidg.utilities.Constants.RESEND;
import static com.vaidg.utilities.Constants.SID;
import static com.vaidg.utilities.Constants.SIGNUP;


public class OtpActivity extends DaggerAppCompatActivity implements OtpView {

    @BindView(R.id.btn_otp_verify)
    Button btn_otp_verify;
    @BindView(R.id.tv_heading_2)
    TextView tv_heading_2;
    @BindView(R.id.tv_heading_1)
    TextView tv_heading_1;
    @BindView(R.id.tv_timer_text)
    TextView tv_timer_text;
    @BindView(R.id.tv_otp_resend)
    TextView tv_otp_resend;
    @BindView(R.id.et_otp1)
    EditText et_otp1;
    @BindView(R.id.et_otp2)
    EditText et_otp2;
    @BindView(R.id.et_otp3)
    EditText et_otp3;
    @BindView(R.id.et_otp4)
    EditText et_otp4;
    @Inject
    OtpPresenter presenter;
    @Inject
    SessionManagerImpl sessionManager;

    private String flag = "";
    private String sid;
    private String old_phone;
    private AlertDialog alertDialog;
    private AlertDialog.Builder dialogBuilder;
    CountDownTimer mCountDownTimer;
    AccountManager accountManager;
    private IntentFilter intentFilter;
    private MySMSBroadcastReceiver otpReceiver;
    private String countryCodeName, countryCode;

    @Inject
    AppTypeface appTypeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);
        Bundle bundle = getIntent().getExtras();
        if (SIGNUP.equals(bundle.getString(COMINGFROM))) {
            flag = "";
            btn_otp_verify.setText(R.string.verifyPhone);
            old_phone = bundle.getString(MOBILENUMBER);
            countryCode = bundle.getString(COUNTRYCODE);
            countryCodeName = bundle.getString(COUNTRYSYMBOl);

        } else {
            flag = bundle.getString(FORGOTPWD);
            if (CHANGE_PHONE.equals(flag)) {
                old_phone = bundle.getString(MOBILENUMBER);
                countryCode = bundle.getString(COUNTRYCODE);
                countryCodeName = bundle.getString(COUNTRYSYMBOl);
                btn_otp_verify.setText(R.string.verifyPhone);
            } else {
                sid = bundle.getString(SID);
                old_phone = bundle.getString(MOBILENUMBER);
                countryCode = bundle.getString(COUNTRYCODE);
                countryCodeName = bundle.getString(COUNTRYSYMBOl);
                btn_otp_verify.setText(R.string.verify);
            }
        }
        if (TextUtils.isEmpty(sid)) {
            sid = sessionManager.getSID();
        }
        initialize();
        getSmsRetrival();

        /*
  Bug Name: After the phone number is updated the app gets auto logged out
  Bug id: QglaG18g
  Fix: guest login is set to false
  Dev: Sateesh
  Date:02 june 2021
  */
        sessionManager.setGuestLogin(false);
        setTypeFaceValue();
    }

    private void getSmsRetrival() {
        otpReceiver = new MySMSBroadcastReceiver();
        intentFilter = new IntentFilter("com.google.android.gms.auth.api.phone.SMS_RETRIEVED");

        // Get an instance of SmsRetrieverClient, used to start listening for a matching
// SMS message.
        SmsRetrieverClient client = SmsRetriever.getClient(this /* context */);

// Starts SmsRetriever, which waits for ONE matching SMS message until timeout
// (5 minutes). The matching SMS message will be sent via a Broadcast Intent with
// action SmsRetriever#SMS_RETRIEVED_ACTION.
        Task<Void> task = client.startSmsRetriever();

// Listen for success/failure of the start Task. If in a background thread, this
// can be made blocking using Tasks.await(task, [timeout]);
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // Successfully started retriever, expect broadcast intent
                // ...
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                // Failed to start retriever, inspect Exception for more details
                // ...
            }
        });

    }

    private void setTypeFaceValue() {
        tv_heading_2.setTypeface(appTypeface.getHind_regular());
        tv_heading_1.setTypeface(appTypeface.getHind_medium());
        tv_timer_text.setTypeface(appTypeface.getHind_semiBold());
        tv_otp_resend.setTypeface(appTypeface.getHind_medium());
        btn_otp_verify.setTypeface(appTypeface.getHind_semiBold());
        String infoString = String.format("%s %S %S %S %S", getString(R.string.we_sent_4_digit), "\n ", countryCode, "-", old_phone);
        tv_heading_2.setText(infoString);
    }

    private void initialize() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView textCenter = toolbar.findViewById(R.id.tv_center);
        textCenter.setText((CHANGE_PHONE.equals(flag) || (flag == null || "".equals(flag))) ? R.string.verifyPhone : R.string.forgotPassword);
        textCenter.setTypeface(appTypeface.getHind_semiBold());
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        accountManager = AccountManager.get(this);
        mCountDownTimer = new CountDownTimer(MILLIS_IN_FUTURE * sessionManager.getExpireOtp(), COUNTDOWN_INTERVAL) {

            public void onTick(long millisUntilFinished) {

                tv_timer_text.setText("" + String.format("%d : %d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));

            }

            public void onFinish() {
                tv_timer_text.setText("00:00");
                tv_timer_text.setVisibility(View.GONE);
                tv_otp_resend.setEnabled(true);
                tv_otp_resend.setVisibility(View.VISIBLE);

            }
        }.start();

        et_otp4.requestFocus();
        if (et_otp4.getText().toString().trim().length() > 0) {
            String otp = et_otp1.getText().toString() + et_otp2.getText().toString() + et_otp3.getText().toString() + et_otp4.getText().toString();

            if (Utility.isNetworkAvailable(OtpActivity.this) && (flag == null || "".equals(flag))) {
                presenter.verifyPhone(otp, sid, flag);
            } else if (CHANGE_PHONE.equals(flag)) {
                if (Utility.isNetworkAvailable(OtpActivity.this))
                    presenter.verifyOtp(otp, sid, flag);
                sessionManager.setMobileNo(old_phone);
                sessionManager.setCountryCode(countryCode);
                sessionManager.setCountrySymbol(countryCodeName);
            } else {
                if (Utility.isNetworkAvailable(OtpActivity.this))
                    presenter.verifyOtp(otp, sid, flag);
            }
        }
    }

    @OnClick({R.id.tv_otp_resend, R.id.btn_otp_verify})
    public void onClickEvent(View view) {
        switch (view.getId()) {
            case R.id.tv_otp_resend:
                if (flag == null || "".equals(flag)) {
                    if (Utility.isNetworkAvailable(this))
                        presenter.resendOtp("", sid, 1, true);
                } else {
                    String otp = et_otp1.getText().toString() + et_otp2.getText().toString() + et_otp3.getText().toString() + et_otp4.getText().toString();
                    if (Utility.isNetworkAvailable(this))
                        presenter.resendOtp(otp, sid, 2, true);
                }
                mCountDownTimer.start();
                tv_timer_text.setVisibility(View.VISIBLE);
                tv_otp_resend.setVisibility(View.GONE);
                break;

            case R.id.btn_otp_verify:
                String otp = et_otp1.getText().toString() + et_otp2.getText().toString() + et_otp3.getText().toString() + et_otp4.getText().toString();
                if (Utility.isNetworkAvailable(this) && (flag == null || "".equals(flag))) {
                    presenter.verifyPhone(otp, sid, flag);
                } else if (CHANGE_PHONE.equals(flag)) {
                    if (Utility.isNetworkAvailable(this))
                        presenter.verifyOtp(otp, sid, flag);
                    sessionManager.setMobileNo(old_phone);
                    sessionManager.setCountryCode(countryCode);
                    sessionManager.setCountrySymbol(countryCodeName);
                } else {
                    if (Utility.isNetworkAvailable(this))
                        presenter.verifyOtp(otp, sid, flag);
                }
                break;
        }

    }


    @OnTextChanged({R.id.et_otp1, R.id.et_otp2, R.id.et_otp3, R.id.et_otp4})
    public void afterTextChanged(Editable editable) {
        if (editable == et_otp1.getEditableText()) {
            int str_len = editable.length();
            if (str_len > 0) {
                et_otp2.requestFocus();
            }
        }
        if (editable == et_otp2.getEditableText()) {
            if (editable.length() > 0) {
                et_otp3.requestFocus();
            } else {
                et_otp1.requestFocus();
            }
        }
        if (editable == et_otp3.getEditableText()) {
            if (editable.length() > 0) {
                et_otp4.requestFocus();
            } else {
                et_otp2.requestFocus();
            }
        }
        if (editable == et_otp4.getEditableText()) {
            int str_len = editable.length();
            if (str_len > 0) {
                String otp = et_otp1.getText().toString() + et_otp2.getText().toString() + et_otp3.getText().toString() + et_otp4.getText().toString();
                if (flag == null || "".equals(flag)) {
                    presenter.verifyPhone(otp, sid, flag);
                } else if (CHANGE_PHONE.equals(flag)) {
                    if (Utility.isNetworkAvailable(this))
                        presenter.verifyOtp(otp, sid, flag);
                    sessionManager.setMobileNo(old_phone);
                    sessionManager.setCountryCode(countryCode);
                    sessionManager.setCountrySymbol(countryCodeName);
                } else {
                    if (Utility.isNetworkAvailable(this))
                        presenter.verifyOtp(otp, sid, flag);
                }
            }
            if (str_len == 0) {
                et_otp3.requestFocus();
            }
        }
    }

    @Override
    public void showProgress(String text) {
        if (!isFinishing()) {
            dialogBuilder = new AlertDialog.Builder(OtpActivity.this);
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(R.layout.progress_dialog_layout, null);
            TextView tv_progress = dialogView.findViewById(R.id.tv_progress);

            switch (text) {
                case RESEND:
                    tv_progress.setText(getString(R.string.wait_resend));
                    break;
                case OTPVERIFY:
                    tv_progress.setText(getString(R.string.wait_otp_verify));
                    break;
                case PHONEVERIFY:
                    tv_progress.setText(getString(R.string.wait_verify_signup));
                    break;
                default:
                    tv_progress.setText(getString(R.string.wait_first_signin));
                    break;
            }

            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);
            alertDialog = dialogBuilder.create();
            alertDialog.show();
        }

    }

    @Override
    public void hideProgress() {
        alertDialog.dismiss();
    }

    @Override
    public void navigateToLogin() {
        Intent intent = new Intent(OtpActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void navToProfile() {
        Intent intent = new Intent(OtpActivity.this, ProfileActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void setError() {
        Toast.makeText(this, getString(R.string.verification_success), Toast.LENGTH_LONG).show();
    }

    @Override
    public void setErrorMsg(String errorMsg) {
        Toast.makeText(this, "" + errorMsg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateToChangePwd(String sid) {
        sessionManager.setSID(sid);
        Log.e("OTP_CHG_PWD", "SID :: " + sid);
        Intent intent = new Intent(OtpActivity.this, ChangePwdActivity.class);
        intent.putExtra(COMMING_FROM, FORGOTPWD);
        startActivity(intent);

    }

    @Override
    public void navToHome(String auth) {
        sessionManager.setSID(sid);
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void onResume() {
        this.registerReceiver(otpReceiver, intentFilter);
        super.onResume();
    }

    @Override
    public void onPause() {
        this.unregisterReceiver(otpReceiver);
        super.onPause();

    }
}
