package com.vaidg.invoice;

import static com.vaidg.utilities.Constants.MESSAGE;

import android.app.Activity;
import android.app.Dialog;
import android.util.Log;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import com.vaidg.R;
import com.vaidg.encryptiondecryption.CryptographyExample;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.HandlePictureEvents;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pojo.BidQuestionAnswer;
import com.pojo.BookingDetailsPojo;
import com.pojo.ErrorHandel;
import com.utility.AlertProgress;
import com.utility.RefreshToken;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilPrivateKey;
import com.virgilsecurity.sdk.crypto.VirgilPublicKey;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * Created by Ali on 10/18/2018.
 */

public class InvoicePresenter implements InvoiceModel.InvoicePre {

  @Inject
  SessionManagerImpl manager;
  @Inject
  LSPServices lspServices;
  @Inject
  AlertProgress alertProgress;
  @Inject
  InvoiceModel.InvoiceView view;
  @Inject
  Gson gson;
  Dialog indialog;
  Activity mActivity;
  private String TAG = InvoiceActivity.class.getSimpleName();

  @Inject
  public InvoicePresenter() {
  }

  @Override
  public void attachView(Object view) {

  }

  @Override
  public void detachView() {

  }

  @Override
  public void getBookingDetailsById(long bId) {
    Observable<Response<ResponseBody>> observable = lspServices.onToGetBookingDetails(
        LSPApplication.getInstance().getAuthToken(manager.getSID())
        , Constants.selLang, Constants.PLATFORM_ANDROID, bId);
    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }
          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            int code = responseBodyResponse.code();
            Log.d(TAG, "onNextJobDetails: " + code);
            JSONObject jsonObject;
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;

              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                  if (response != null && !response.isEmpty()) {
                    Log.d(TAG, "onNextJobDetailsRes: " + response);

                    BookingDetailsPojo bData = gson.fromJson(response,
                        BookingDetailsPojo.class);

                    String messageToSign = "";
                    try {
                      CryptographyExample cryptographyExample = new CryptographyExample();

                      // Import private key
                      VirgilCrypto crypto = new VirgilCrypto();
                      VirgilPrivateKey importedPrivateKey = cryptographyExample.importPrivateKey(manager.getVirgilPrivateKey(), crypto);
                      // Import public key
                      VirgilPublicKey importedPublicKey = cryptographyExample.importPublicKey(manager.getVirgilDoctorPublicKey(), crypto);
                      // Decrypt data
                    //  byte[] encryptedData = Base64.decode(bData.getData().getQuestionAndAnswer(), Base64.NO_WRAP);/*Base64.getMimeDecoder().decode(chatData.get(position).getContent());*/
                        messageToSign = cryptographyExample.dataDecryption(bData.getData().getQuestionAndAnswer(), importedPrivateKey);
                    } catch (CryptoException e) {
                      e.printStackTrace();
                    }
                    Type collectionType = new TypeToken<ArrayList<BidQuestionAnswer>>(){}.getType();
                    ArrayList<BidQuestionAnswer> bidQuestionAnswer = gson.fromJson( messageToSign , collectionType);
                    ArrayList<BidQuestionAnswer> questionAndAnswer = new ArrayList<BidQuestionAnswer>(bidQuestionAnswer);
                    questionAndAnswer.addAll(bidQuestionAnswer);
                    if(view != null) {
                      view.onProviderDetails(
                          bData.getData().getCurrencySymbol()
                          , bData.getData().getProviderDetail(),
                          bData.getData().getCategoryName()
                          , bData.getData().getBookingRequestedFor(),
                          questionAndAnswer);
                    }
                    if (bData.getData().getAccounting() != null) {
                      if(view != null) {
                        view.onAccounting(
                            bData.getData().getAccounting());
                      }
                    }
                    if(view != null) {
                      view.onHideProgress();
                    }
                  }
                  break;
                case Constants.SESSION_LOGOUT:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                                            MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onHideProgress();
                        view.onLogout(jsonObject.getString(MESSAGE), manager);
                      }
                    }
                  }
                  break;
                case Constants.SESSION_EXPIRED:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    ErrorHandel errorHandel = gson.fromJson(errorBody,
                        ErrorHandel.class);

                    // String
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(),
                        lspServices, new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {
                              LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                            getBookingDetailsById(bId);
                          }

                          @Override
                          public void onFailureRefreshToken() {

                          }

                          @Override
                          public void sessionExpired(String msg) {
                            if(view != null) {
                              view.onHideProgress();
                              view.onLogout(msg, manager);
                            }
                          }
                        });
                  }
                  break;
                default:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                                            MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onHideProgress();
                        view.onError(jsonObject.getString(MESSAGE));
                      }
                    }
                  }
                  break;
              }

            } catch (Exception e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.onHideProgress();
              view.networkUnReachable(e.getMessage(), true);
            }
          }

          @Override
          public void onComplete() {

          }
        });
  }

  @Override
  public void timeMethod(TextView tvDate, TextView tvTime, long bookingRequestedFor) {
    try {


      Log.d("TAGTIME", " expireTime " + bookingRequestedFor);
      Date date = new Date(TimeUnit.SECONDS.toMillis(bookingRequestedFor));
      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm a", Locale.US);
      sdf.setTimeZone(Utility.getTimeZone());
      String formattedDate = sdf.format(date);

      String formattedDated = Utility.getFormattedDate(date);
      String[] dat1 = formattedDated.split(",");
      String splitDate = dat1[0] + "," + dat1[1];

      // String fullDate = splitDate[1]+" "+splitDate[2]+" "+splitDate[3];
      //   tbServiceAvailable.setText(dat1[0]);
      tvDate.setText(splitDate);
      tvTime.setText(dat1[2]);

    } catch (Exception e) {
      Log.d("TAG", "timeMethodException: " + e.toString());
    }
  }

  @Override
  public void updateStatus(JSONArray jsonArray, String signatureUrl,
      HandlePictureEvents handlePicEvent, File signatureFile, long bId, String promoCode) {


    String[] strArr = new String[jsonArray.length()];

    Map<String, Object> jsonParams = new HashMap<>();
    jsonParams.put("bookingId", bId);
    jsonParams.put("signatureUrl", signatureUrl);
    jsonParams.put("additionalService", strArr);


    Observable<Response<ResponseBody>> observable = lspServices.bookingstatus(LSPApplication.getInstance().getAuthToken(manager.getSID())
        , Constants.selLang, Constants.PLATFORM_ANDROID, jsonParams);//,promoCode
    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }
          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            Log.d(TAG, "onNext: " + signatureUrl);
            Log.d(TAG, "onNext: " + responseBodyResponse.code());

            int code = responseBodyResponse.code();
            Log.d(TAG, "onNextJobDetails: " + code);
            JSONObject jsonObject;
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                  if (response != null && !response.isEmpty()) {
                    raisedInvoice(bId);
                  }
                  break;
                case Constants.SESSION_LOGOUT:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onHideProgress();
                        view.onLogout(jsonObject.getString(MESSAGE), manager);
                      }
                    }
                  }
                  break;
                case Constants.SESSION_EXPIRED:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    ErrorHandel errorHandel = gson.fromJson(errorBody,
                        ErrorHandel.class);

                    // String
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),
                        manager.getREFRESHAUTH(),
                        lspServices, new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {
                              LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                            updateStatus(jsonArray, signatureUrl,
                                handlePicEvent, signatureFile, bId,
                                promoCode);
                          }

                          @Override
                          public void onFailureRefreshToken() {

                          }

                          @Override
                          public void sessionExpired(String msg) {
                            if(view != null) {
                              view.onHideProgress();
                              view.onLogout(msg, manager);
                            }
                          }
                        });
                  }
                  break;
                default:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onHideProgress();
                        view.onError(jsonObject.getString(MESSAGE));
                      }
                    }
                  }
                  break;
              }
            } catch (JSONException e) {
              e.printStackTrace();
            } catch (IOException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.onHideProgress();
              view.onError(e.getMessage());
            }
          }

          @Override
          public void onComplete() {

          }
        });
  }

  private void raisedInvoice(long bId) {
    Observable<Response<ResponseBody>> observable = lspServices.onToGetInvoiceDetails(
        LSPApplication.getInstance().getAuthToken(manager.getSID())
        , Constants.selLang, Constants.PLATFORM_ANDROID, bId);//,promoCode
    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }
          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            Log.d(TAG, "onNext: " + responseBodyResponse);
            Log.d(TAG, "onNext: " + responseBodyResponse.code());


            int code = responseBodyResponse.code();
            Log.d(TAG, "onNextJobDetails: " + code);
            JSONObject jsonObject;
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                  if (response != null && !response.isEmpty()) {
                    if(view != null) {
                      view.viewRatingScreen();
                    }
                  }
                  break;
                case Constants.SESSION_LOGOUT:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onHideProgress();
                        view.onLogout(jsonObject.getString(MESSAGE), manager);
                      }
                    }
                  }
                  break;
                case Constants.SESSION_EXPIRED:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    ErrorHandel errorHandel = gson.fromJson(errorBody,
                        ErrorHandel.class);

                    // String
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),
                        manager.getREFRESHAUTH(),
                        lspServices, new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {
                              LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                            raisedInvoice(bId);
                          }

                          @Override
                          public void onFailureRefreshToken() {

                          }

                          @Override
                          public void sessionExpired(String msg) {
                            if(view != null) {
                              view.onHideProgress();
                              view.onLogout(msg, manager);
                            }
                          }
                        });
                  }
                  break;
                default:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onHideProgress();
                        view.onError(jsonObject.getString(MESSAGE));
                      }
                    }
                  }
                  break;
              }
            } catch (JSONException e) {
              e.printStackTrace();
            } catch (IOException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.onHideProgress();
              view.onError(e.getMessage());
            }
          }

          @Override
          public void onComplete() {

          }
        });

  }

  @Override
  public void showPromoCodeDialog(Activity invoiceActivity, long bId) {
    mActivity = invoiceActivity;
    TextView etPromoCode, tvApply, youhaveAPRomoCode;
    ImageView crossButton;
    AppTypeface appTypeface;
    appTypeface = AppTypeface.getInstance(invoiceActivity);
    indialog = new Dialog(invoiceActivity);
    indialog.setCanceledOnTouchOutside(true);
    indialog.setCancelable(true);
    indialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    indialog.setContentView(R.layout.promo_code_dialog);
    indialog.show();
    etPromoCode = indialog.findViewById(R.id.etPromoCode);
    tvApply = indialog.findViewById(R.id.tvApply);
    youhaveAPRomoCode = indialog.findViewById(R.id.tvyouHavePromo);
    crossButton = indialog.findViewById(R.id.crossButton);
    etPromoCode.setTypeface(appTypeface.getHind_regular());
    tvApply.setTypeface(appTypeface.getHind_medium());
    youhaveAPRomoCode.setTypeface(appTypeface.getHind_semiBold());

    tvApply.setOnClickListener(v -> {
      if (!"".equals(etPromoCode.getText().toString().trim())) {
        if(view != null) {
          view.alertDialogMethod(mActivity.getResources().getString(R.string.wait));
          view.onShowProgress();
        }
        //Removed as promocode was not added
        //callPromoCode(bId,etPromoCode.getText().toString().trim());
      } else {
        alertProgress.alertinfo(mActivity, mActivity.getString(R.string.pleaseEndterPromoCode));
      }
    });
    crossButton.setOnClickListener(view -> indialog.dismiss());
  }

  private void callPromoCode(long bId, String trim) {
    Log.d(TAG, "callPromoCode: " + bId + " trim " + trim);
        /*Observable<Response<ResponseBody>>observable = lspServices.postPromoCodeValidation
        (LSPApplication.getInstance().getAuthToken(manager.getSID())
                ,ConstantsInteface.selLang,bId,trim);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int code = responseBodyResponse.code();

                        Log.d(TAG, "onNextCodePr: "+code);
                        try
                        {
                            String response;
                            switch (code)
                            {
                                case  ConstantsInteface.SUCCESS_RESPONSE:
                                    response = responseBodyResponse.body().string();
                                    Log.d(TAG, "onNextPROMOCODE: "+response);
                                    JSONObject jsonObject = new JSONObject(response);
                                    JSONObject jsonData = jsonObject.getJSONObject("data");
                                    double amount = jsonData.getDouble("discountAmount");

                                    alertProgress.alertPositiveOnclick(mActivity, jsonObject
                                    .getString("message"), mActivity.getResources().getString(R
                                    .string.promocode), new DialogInterfaceListner() {
                                        @Override
                                        public void dialogClick(boolean isClicked) {
                                            view.callOnResultActivity(amount,trim);
                                            if(indialog!=null && indialog.isShowing())
                                                indialog.dismiss();
                                        }
                                    });
                                    view.onHideProgress();

                                    break;
                                case ConstantsInteface.SESSION_LOGOUT:
                                    Utility.setMAnagerWithBID(mActivity,manager);
                                    view.onHideProgress();
                                    break;
                                case ConstantsInteface.SESSION_EXPIRED:
                                    response = responseBodyResponse.errorBody().string();
                                    RefreshToken.onRefreshToken(new JSONObject(response)
                                    .getString("data"), lspServices, new RefreshToken
                                    .RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            callPromoCode(bId,trim);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {

                                        }

                                        @Override
                                        public void sessionExpired(String msg)
                                        {
                                            Utility.setMAnagerWithBID(mActivity,manager);
                                            view.onHideProgress();
                                            if(indialog!=null && indialog.isShowing())
                                                indialog.dismiss();
                                        }
                                    });
                                    break;
                                default:
                                    response = responseBodyResponse.errorBody().string();
                                    alertProgress.alertinfo(mActivity,new JSONObject(response)
                                    .getString("message"));
                                    view.onHideProgress();
                                  *//*  if(indialog!=null && indialog.isShowing())
                                        indialog.dismiss();*//*
                                    break;
                            }

                        }catch (IOException e)
                        {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        alertProgress.alertinfo(mActivity,e.getMessage());
                        view.onHideProgress();

                        if(indialog!=null && indialog.isShowing())
                            indialog.dismiss();
                    }

                    @Override
                    public void onComplete() {

                    }
                });*/
  }
}
