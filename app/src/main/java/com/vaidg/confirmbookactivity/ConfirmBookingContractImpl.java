package com.vaidg.confirmbookactivity;


import android.annotation.SuppressLint;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;

import com.vaidg.R;
import com.vaidg.encryptiondecryption.CryptographyExample;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.google.gson.Gson;
import com.pojo.CartModifiedData;
import com.pojo.ChatEncryptDecryptPublicKey;
import com.pojo.ErrorHandel;
import com.utility.RefreshToken;

import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilPrivateKey;
import com.virgilsecurity.sdk.crypto.VirgilPublicKey;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;
import com.virgilsecurity.sdk.utils.ConvertionUtils;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.utilities.Constants.AES_MODE;
import static com.vaidg.utilities.Constants.ANDROID_KEYSTORE;
import static com.vaidg.utilities.Constants.DATA;
import static com.vaidg.utilities.Constants.FIXED_IV;
import static com.vaidg.utilities.Constants.IV_PARAMETER_SPEC;
import static com.vaidg.utilities.Constants.KEY_PRIVATEALIAS;
import static com.vaidg.utilities.Constants.KEY_PUBLICALIAS;
import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.RSA_MODE;
import static com.vaidg.utilities.Constants.SESSION_EXPIRED;
import static com.vaidg.utilities.Constants.SESSION_LOGOUT;
import static com.vaidg.utilities.Constants.jsonArray;

/**
 * <h2>ConfirmBookingContractImpl</h2>
 * Created by Ali on 2/12/2018.
 */

public class ConfirmBookingContractImpl implements ConfirmBookingContract.ContractPresenter {
  @Inject
  ConfirmBookingContract.ContractView view;
  @Inject
  SessionManagerImpl manager;
  @Inject
  LSPServices lspServices;
  @Inject
  Gson gson;
  private String TAG = ConfirmBookingContractImpl.class.getSimpleName();
  private boolean isFirstTrue = true;
  private KeyStore keyStore;

  @Inject
  public ConfirmBookingContractImpl() {

  }

  @Override
  public void attachView(Object view) {

  }

  @Override
  public void detachView() {

  }


  @Override
  public void onGetCartId() {
    Log.d(TAG, "onGetCartId: " + LSPApplication.getInstance().getAuthToken(manager.getSID()) + " " +
        Constants.selLang + " " + Constants.catId + " " + Constants.proId + " "
        + Constants.callTypeInOutTele);
    Observable<Response<ResponseBody>> observable = lspServices.getSubCart(LSPApplication.getInstance().getAuthToken(manager.getSID()),
        Constants.selLang, PLATFORM_ANDROID, Constants.catId, Constants.proId,
        Constants.callTypeInOutTele, Constants.bookingType);
    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int code = responseBodyResponse.code();
            Log.d(TAG, "onNextConfirmGetCart: " + code);
            JSONObject jsonObject;

            try {

              String response =
                  responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
                String errorBody =
                        responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null;
              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                  if(response!= null && !response.isEmpty()) {
                    CartModifiedData cartmodification = gson.fromJson(response,
                        CartModifiedData.class);
                    Constants.serviceSelected = cartmodification.getData().getServiceType();
                    if (Constants.serviceSelected == 2
                        && cartmodification.getData().getTotalQuntity() == 0) {
                      if (view != null) {
                        view.onHourly();
                      }
                    } else {
                      if (view != null) {
                        view.onCartModification(cartmodification.getData());
                      }
                    }
                    jsonObject = new JSONObject(response);
                    String cartId = jsonObject.getJSONObject(DATA).getString("_id");
                    if (view != null) {
                      view.onSuccessCartId(cartId);
                    }
                  }
                  break;

                case SESSION_LOGOUT:
                  if(errorBody!= null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    if(jsonObject.getString(MESSAGE)!= null && !jsonObject.getString(MESSAGE).isEmpty()) {
                      if (view != null) {
                        view.onLogout(jsonObject.getString(MESSAGE), manager);
                      }
                    }
                  }

                  break;
                case SESSION_EXPIRED:
                    if(errorBody!= null && !errorBody.isEmpty()) {
                        jsonObject = new JSONObject(errorBody);
                        RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(), lspServices,
                                new RefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                        onGetCartId();

                                    }

                                    @Override
                                    public void onFailureRefreshToken() {

                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                      if (view != null) {
                                        view.onLogout(msg, manager);
                                      }
                                    }
                                });
                    }
                  break;
                case 409:
                  if(response!=null && !response.isEmpty()) {
                    Log.d(TAG, "onNext: " + response);
                    JSONObject jsonObjecterrCart = new JSONObject(response);
                    if (view != null) {
                      view.onAlreadyCartPresent(jsonObjecterrCart.getString("message"), true);
                    }
                  }
                  break;
                case 416:
                  if(view != null) {
                    view.onHourly();
                  }
                  break;
                default:
                  if(view != null) {
                    view.onHidePro();
                  }

              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if (view != null) {
              view.onConnectionError(e.getMessage(), "CartId", "");
            }
          }

          @Override
          public void onComplete() {

          }
        });
  }

  @Override
  public void lastDues() {

    Observable<Response<ResponseBody>> observable = lspServices.lastDues(LSPApplication.getInstance().getAuthToken(manager.getSID()),
        Constants.selLang, PLATFORM_ANDROID);

    Log.d(TAG, "lastDues: " + LSPApplication.getInstance().getAuthToken(manager.getSID()) + " " +
        Constants.selLang);
    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            int code = responseBodyResponse.code();
            Log.d(TAG, "onNextDuesCode: " + code);
            JSONObject jsonObject;
            try {
              String response =
                  responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
              String errorBody =
                  responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null;
              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                  if(response != null  && !response.isEmpty()) {
                    Log.d(TAG, "onNextDues: " + response);
                    jsonObject = new JSONObject(response);
                    if (view != null) {
                      view.onSessionExpired();
                    }
                    String msg = jsonObject.getString("message");
                    String addLine1 = jsonObject.getJSONObject("data").getString("addLine1");
                    long timeStamp = jsonObject.getJSONObject("data").getLong("bookingRequestedAt");
                    Date date = new Date(TimeUnit.SECONDS.toMillis(timeStamp));
                    if (view != null) {
                      view.onDuesFound(msg, addLine1, Utility.getFormattedDate(date));
                    }
                  }
                  break;
                case SESSION_LOGOUT:
                    if(errorBody!= null && !errorBody.isEmpty()) {
                        jsonObject = new JSONObject(errorBody);
                      jsonObject.getString(MESSAGE);
                      if(!jsonObject.getString(MESSAGE).isEmpty()) {
                          if (view != null) {
                            view.onSessionExpired();
                            view.onLogout(jsonObject.getString(MESSAGE), manager);
                          }
                        }
                    }
                  break;
                case SESSION_EXPIRED:
                    if(errorBody!= null && !errorBody.isEmpty()) {
                        jsonObject = new JSONObject(errorBody);

                        RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(), lspServices,
                                new RefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                        lastDues();

                                    }

                                    @Override
                                    public void onFailureRefreshToken() {

                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                      if (view != null) {
                                        view.onLogout(msg, manager);
                                      }
                                    }
                                });
                    }
                  break;
                case Constants.SESSION_NoDues:
                  if(view != null) {
                    view.noDuesFoundLiveBooking();
                  }
                  break;
                default:
                    if(errorBody!= null && !errorBody.isEmpty()) {
                        jsonObject = new JSONObject(errorBody);
                      jsonObject.getString(MESSAGE);
                      if(!jsonObject.getString(MESSAGE).isEmpty()) {
                          if (view != null) {
                            view.onError(jsonObject.getString(MESSAGE));
                          }
                        }
                    }
                  break;
              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
              if(view != null) {
                view.onHideProgress();
              }
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.onConnectionError(e.getMessage(), "LastDues", "");
              view.onSessionExpired();
            }
          }

          @Override
          public void onComplete() {

          }
        });
  }

  @Override
  public void serverTime() {
    Log.d(TAG, "serverTime: ");
    Observable<Response<ResponseBody>> observable = lspServices.onTogetServerTime(Constants.selLang,
        PLATFORM_ANDROID);
    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {

          @Override
          public void onSubscribe(Disposable d) {

          }
          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            int code = responseBodyResponse.code();
            try {
                String response = responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
              if (code == Constants.SUCCESS_RESPONSE) {
                Constants.serverTime = new JSONObject(
                        response).getLong(
                    DATA);
              }
              if(view != null) {
                view.CallLiveBook();
              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {

          }

          @Override
          public void onComplete() {

          }
        });
  }

  @Override
  public void getPublicKey(String proId) {
    Observable<Response<ResponseBody>> observable = lspServices.getPublicKey(LSPApplication.getInstance().getAuthToken(manager.getSID())
        , Constants.selLang, Constants.PLATFORM_ANDROID, Constants.PROVIDER, proId);

    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int code = responseBodyResponse.code();
            String response;
            try {
              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                  response = responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
                  ChatEncryptDecryptPublicKey chatEncryptDecryptPublicKey = gson.fromJson(response, ChatEncryptDecryptPublicKey.class);
                  if (chatEncryptDecryptPublicKey.getData() != null) {
                    ChatEncryptDecryptPublicKey.ChatPublicKey chatPublicKey = chatEncryptDecryptPublicKey.getData();
                    if (chatPublicKey.getPublickKey() != null && !chatPublicKey.getPublickKey().isEmpty()) {
                      manager.setVirgilDoctorPublicKey(chatPublicKey.getPublickKey());
                    }
                  }
                  break;
                case Constants.SESSION_LOGOUT:
                  if(view != null) {
                    view.onLogout(new JSONObject(responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null).getString("data"), manager);
                  }
                  break;
                case Constants.SESSION_EXPIRED:

                  RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                        LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                      getPublicKey(proId);
                    }

                    @Override
                    public void onFailureRefreshToken() {

                    }

                    @Override
                    public void sessionExpired(String msg) {

                    }
                  });
                  break;
                default:
                  response = responseBodyResponse.errorBody().string();
                  ErrorHandel errorHandel = gson.fromJson(response, ErrorHandel.class);
                  break;
              }

            } catch (IOException | JSONException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
          }

          @Override
          public void onComplete() {
          }
        });
  }

  @Override
  public void callPromoCodeApi(double bookingLat, double bookingLng, int paymentType, String cartId,
      String proCode) {

    Map<String, Object> stringObjectMap = new HashMap<>();
    stringObjectMap.put("latitude", bookingLat);
    stringObjectMap.put("longitude", bookingLng);
    stringObjectMap.put("cartId", cartId);
    stringObjectMap.put("paymentMethod", paymentType);
    stringObjectMap.put("couponCode", proCode);


    Observable<Response<ResponseBody>> responseObservable = lspServices.postPromoCodeValidation(
        LSPApplication.getInstance().getAuthToken(manager.getSID()), Constants.selLang
        , PLATFORM_ANDROID, stringObjectMap);
    responseObservable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }
          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            int code = responseBodyResponse.code();
            JSONObject jsonObject;
            Log.d(TAG, "onNextCode: " + code);
            try {
              String response = responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null;
              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                    if(response != null && !response.isEmpty()) {
                        jsonObject = new JSONObject(response);
                        if(jsonObject.getJSONObject(DATA) != null) {
                            JSONObject jsonData = jsonObject.getJSONObject(DATA);
                            double amount = jsonData.getDouble("discountAmount");
                          if(view != null) {
                            view.promoCode(amount, proCode);
                          }
                        }
                    }

                  break;
                case SESSION_LOGOUT:
                  if(errorBody != null && !errorBody.isEmpty()){
                      jsonObject = new JSONObject(errorBody);
                      if(jsonObject.getString(MESSAGE) != null && !jsonObject.getString(MESSAGE).isEmpty()) {
                        if (view != null) {
                          view.onSessionExpired();
                          view.onLogout(jsonObject.getString(MESSAGE), manager);
                        }
                      }
                  }
                  break;
                case SESSION_EXPIRED:
                    if(errorBody != null && !errorBody.isEmpty()) {
                        RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(),
                                lspServices, new RefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                        callPromoCodeApi(bookingLat, bookingLng, paymentType, cartId, proCode);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {

                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                      if(view != null) {
                                        view.onLogout(msg, manager);
                                      }
                                    }
                                });
                    }
                  break;
                default:
                    if(errorBody != null && !errorBody.isEmpty()){
                        jsonObject = new JSONObject(errorBody);
                      jsonObject.getString(MESSAGE);
                      if(!jsonObject.getString(MESSAGE).isEmpty()) {
                          if (view != null) {
                            view.onError(jsonObject.getString(MESSAGE));
                          }
                        }
                    }

                    break;
              }

            } catch (IOException | JSONException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {


          }

          @Override
          public void onComplete() {

          }
        });

  }

  @Override
  public void setAddressWithImage(TextView tvConfirmLocation, String bookingAddress) {
    tvConfirmLocation.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        /*SpannableString ss1=  new SpannableString(bookingAddres);
        ss1.setSpan(new RelativeSizeSpan(1.1f), 0, bookingAddres.length()-bookingAddress.length()
        , Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss1.setSpan(new ForegroundColorSpan(Color.BLACK), 0, bookingAddres.length()
        -bookingAddress.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);*/
    tvConfirmLocation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow, 0);
    tvConfirmLocation.setText(bookingAddress);
    tvConfirmLocation.setPadding(120, 0, 0, 0);
       /* LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout
       .LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(30,0,0,0);
        tvConfirmLocation.setLayoutParams(params);*/

  }

  @Override
  public void onLiveBookingService(final int paymentType, boolean isWalletSelected,
      final String jobDescription, final String promoCode, final String cardId, final String cartId,
      final double bookingLat, final double bookingLng, String address) {
    int walletSelected = 0;
    if (isWalletSelected) {
      walletSelected = 1;
    }
    Log.d(TAG, "onLiveBookingServiceModel: " + Constants.callTypeInOutTele + " service "
        + Constants.serviceType + "\ntype " +
        Constants.bookingType + "\npayment " + paymentType + "\naddress " + address + "\nproId "
        + Constants.proId + "\nCatId " +
        Constants.catId + "\njobDesc " + jobDescription + "\npromoCode " + promoCode + "\n CartId "
        + cartId
        + "\ncardId " + cardId + "\nscheduledDate " + Constants.scheduledDate + "\nSceduledTime "
        + Constants.scheduledTime + "\nWallet " + walletSelected
        + "\n" + " lat " + bookingLat + " long: " + bookingLng);
    String messageFromDecrypt = "";
    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      decryptPrivateKeyPostLollipop();
      decryptPublicKeyPostLollipop();
      messageFromDecrypt =
          encryptData(decryptPrivateKeyPostLollipop(),decryptPublicKeyPostLollipop(),messageFromDecrypt,jsonArray);
    } else {
      decryptPrivateKeyPreMarshMallow();
      decryptPublicKeyPreMarshMallow();
      messageFromDecrypt =   encryptData(decryptPrivateKeyPreMarshMallow(),decryptPublicKeyPreMarshMallow(),messageFromDecrypt,jsonArray);
    }
    Map<String, Object> booking = new HashMap<>();
    booking.put("callType", Constants.callTypeInOutTele);
    booking.put("bookingModel", Constants.serviceType);
    booking.put("bookingType", Constants.bookingType);
    booking.put("paymentTypeIsApplePay", 0);
    booking.put("paymentMethod", paymentType);
    booking.put("paidByWallet", walletSelected);
    booking.put("placeName", "");
    booking.put("addLine2", "");
    booking.put("addLine1", address);
    booking.put("latitude", bookingLat);
    booking.put("longitude", bookingLng);
    booking.put("providerId", Constants.proId);
    booking.put("categoryId", Constants.catId);
    booking.put("cartId", cartId);
    booking.put("jobDescription", jobDescription);
    booking.put("promoCode", promoCode);
    booking.put("paymentCardId", cardId);
    booking.put("bookingDate", Constants.scheduledDate);
    booking.put("scheduleTime", Constants.scheduledTime);
    booking.put("endTimeStamp", Constants.onRepeatEnd);
    booking.put("days", Constants.onRepeatDays);
    booking.put("deviceTime", Utility.dateInTwentyFour(Constants.serverTime));
    booking.put("bidPrice", 0.00);
    booking.put("questionAndAnswer", messageFromDecrypt);
    booking.put("dependentId", Constants.dependentId);
    booking.put("senderPublickKey","");

    Observable<Response<ResponseBody>> observable = lspServices.onLiveBooking(LSPApplication.getInstance().getAuthToken(manager.getSID()),
        Constants.selLang, PLATFORM_ANDROID, booking);

    observable.subscribeOn(Schedulers.io())

        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
              int code = responseBodyResponse.code();
              JSONObject jsonObject;
              try {
                  String response = responseBodyResponse.body() != null
                          ? responseBodyResponse.body().string() : null;
                  String errorBody = responseBodyResponse.errorBody() != null
                          ? responseBodyResponse.errorBody().string() : null;
                  switch (code) {
                      case Constants.SUCCESS_RESPONSE:
                          if (response != null && !response.isEmpty()) {
                                jsonObject = new JSONObject(response);
                              JSONObject userJObject = jsonObject.getJSONObject("data");
                            if (view != null) {
                              view.onSuccessBooking(userJObject.getLong("bookingId"),userJObject.getInt("totalPayAmount"));
                            }
                          }
                        if(view != null) {
                            view.onHidePro();
                          view.onHideProgress();
                        }
                        break;
                      case SESSION_EXPIRED:
                          if (errorBody != null && !errorBody.isEmpty()) {
                              jsonObject = new JSONObject(errorBody);
                              RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(),
                                      lspServices, new RefreshToken.RefreshTokenImple() {
                                          @Override
                                          public void onSuccessRefreshToken(String newToken) {

                                              LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                              onLiveBookingService(paymentType, isWalletSelected, jobDescription,
                                                      promoCode, cardId, cartId,
                                                      bookingLat, bookingLng, address);
                                          }

                                          @Override
                                          public void onFailureRefreshToken() {

                                          }

                                          @Override
                                          public void sessionExpired(String msg) {
                                            if(view != null) {
                                              view.onSessionExpired();
                                              view.onLogout(msg, manager);
                                            }
                                          }
                                      });
                          }
                              break;
                              case SESSION_LOGOUT:
                                  if (errorBody != null && !errorBody.isEmpty()) {
                                      jsonObject = new JSONObject(errorBody);
                                    jsonObject.getString(MESSAGE);
                                    if (!jsonObject.getString(MESSAGE).isEmpty()) {
                                        if (view != null) {
                                          view.onLogout(jsonObject.getString(MESSAGE), manager);
                                        }
                                      }
                                  }
                                if(view != null) {
                                    view.onHidePro();
                                  view.onHideProgress();
                                }
                                  break;
                              default:
                                  if (errorBody != null && !errorBody.isEmpty()) {
                                      jsonObject = new JSONObject(errorBody);
                                    jsonObject.getString(MESSAGE);
                                    if (!jsonObject.getString(MESSAGE).isEmpty()) {
                                      if(view != null) {
                                        view.onError(jsonObject.getString(MESSAGE));
                                      }
                                      }
                                  }
                                if(view != null) {
                                    view.onHidePro();
                                  view.onHideProgress();
                                }
                                  break;
                          }



              } catch (JSONException | IOException e) {
                  e.printStackTrace();
                if(view != null) {
                  view.onHideProgress();
                }
              }
          }
              @Override
          public void onError(Throwable e) {
                if(view != null) {
                  view.onSessionExpired();
                  view.onConnectionError(e.getMessage(), "LiveBooking", "");
                }
          }

          @Override
          public void onComplete() {

          }
        });
  }

  @Override
  public void onAddSubCartModifyData(String catId, String serviceId, int action, int quantityInHr) {

    Log.d(TAG,
        "onAddSubCartModifyDataHR: " + quantityInHr + " isFIRST " + isFirstTrue + " SERVICEID "
            + serviceId
            + " SERVICESEL " + Constants.serviceSelected);

    int quantity = 0;
    if (serviceId.equals("1") && Constants.serviceSelected == 2) {
      if (!isFirstTrue) {
        quantity = 1;
      } else {
        quantity = quantityInHr;
      }
    } else {
      quantity = 1;
    }

    if(quantity == 0)
    {
        quantity =1;
    }
    Map<String, Object> stringObjectMap = new HashMap<>();
    stringObjectMap.put("serviceType", Constants.serviceSelected);
    stringObjectMap.put("bookingType", Constants.bookingType);
    stringObjectMap.put("categoryId", catId);
    stringObjectMap.put("serviceId", serviceId);
    stringObjectMap.put("quntity", quantity);
    stringObjectMap.put("action", action);
    stringObjectMap.put("providerId", Constants.proId);
    stringObjectMap.put("callType", Constants.callTypeInOutTele);


    Observable<Response<ResponseBody>> observable = lspServices.onCatModification(LSPApplication.getInstance().getAuthToken(manager.getSID()),
        Constants.selLang, PLATFORM_ANDROID, stringObjectMap);

    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int code = responseBodyResponse.code();
            JSONObject jsonObject;
            try {
              String response =
                  responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
              String errorBody =
                  responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null;
              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                  if(response!= null && !response.isEmpty()) {
                    isFirstTrue = false;
                    CartModifiedData cartModifiedData = gson.fromJson(response,
                        CartModifiedData.class);
                    if(view != null) {
                      view.onCartModification(cartModifiedData.getData());
                    }
                  }
                  break;
                case SESSION_EXPIRED:
                    if(errorBody != null && !errorBody.isEmpty()) {
                        RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(),
                                lspServices, new RefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {

                                        LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                        onAddSubCartModifyData(catId, serviceId, action, quantityInHr);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {

                                    }

                                    @Override
                                    public void sessionExpired(String msg) {

                                    }
                                });
                    }
                  break;
                case SESSION_LOGOUT:
                    if (errorBody != null && !errorBody.isEmpty()) {
                        jsonObject = new JSONObject(errorBody);
                      jsonObject.getString(MESSAGE);
                      if (!jsonObject.getString(MESSAGE).isEmpty()) {
                        if(view != null) {
                          view.onSessionExpired();
                          view.onLogout(jsonObject.getString(MESSAGE), manager);
                        }
                        }
                    }
                    break;
                default:
                    if (errorBody != null && !errorBody.isEmpty()) {
                        jsonObject = new JSONObject(errorBody);
                      jsonObject.getString(MESSAGE);
                      if (!jsonObject.getString(MESSAGE).isEmpty()) {
                        if(view != null) {
                          view.onSessionExpired();
                          view.onError(jsonObject.getString(MESSAGE));
                        }
                        }
                    }
                  break;
              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            String hourly;
            if ("1".equals(serviceId)) {
              hourly = "hourly";
            } else {
              hourly = "";
            }
            if (view != null) {
              view.onConnectionError(e.getMessage(), "AddCart", hourly);
              view.onHidePro();
            }
          }

          @Override
          public void onComplete() {

          }
        });
  }

  @Override
  public void timeMethod(TextView tvInConfirmBookingTypeDesc, long fromTime) {

    String time = "";
    try {

      Date date = new Date(TimeUnit.SECONDS.toMillis(fromTime));
      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
      // SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.US);
      sdf.setTimeZone(Utility.getTimeZone());
      tvInConfirmBookingTypeDesc.setText(sdf.format(date));

    } catch (Exception e) {
      Log.d("TAG", "timeMethodException: " + e.toString());
    }

  }


  @SuppressLint("NewApi")
  @Override
  public String decryptPrivateKeyPostLollipop()
  {
    String str ="";
    Log.i("TEST","decryptData marsh melloa ");
    Cipher c;
    try {
      c = Cipher.getInstance(AES_MODE);
      c.init(Cipher.DECRYPT_MODE, getSecretPrivateKeyPostLollipop(), new GCMParameterSpec(128, FIXED_IV.getBytes()));
      byte[] encryptedBytes = Base64.decode(manager.getVirgilPrivateKey(), Base64.DEFAULT);

      byte[] decodedBytes = c.doFinal(encryptedBytes);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        str = new String(decodedBytes, StandardCharsets.UTF_8);
      }
      else
      {
        str = new String(decodedBytes);
      }
      Log.i("TEST","decryptData srting "+str);

    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return str;
  }

  @SuppressLint("NewApi")
  @Override
  public String decryptPublicKeyPostLollipop()
  {
    String str = "";
    Log.i("TEST","decryptData marsh melloa ");
    Cipher c;
    try {
      c = Cipher.getInstance(AES_MODE);
      c.init(Cipher.DECRYPT_MODE, getSecretPublicKeyPostLollipop(), new GCMParameterSpec(128, FIXED_IV.getBytes()));
      byte[] encryptedBytes = Base64.decode(manager.getVirgilPublicKey(), Base64.DEFAULT);

      byte[] decodedBytes = c.doFinal(encryptedBytes);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        str = new String(decodedBytes, StandardCharsets.UTF_8);
      }
      else
      {
        str = new String(decodedBytes);
      }
      Log.i("TEST","decryptData srting "+str);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return str;
  }
  /**
   * <h2>getSecretPrivateKeyPreMarshMallow</h2>
   * This method is used to get the secret key for pre marsh mallow device
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPrivateKeyPreMarshMallow() throws Exception {
    String enryptedKeyB64 = manager.getEncryptionPrivateKey();
    Log.i("TEST","enryptedKeyB64 "+enryptedKeyB64);
    // need to check null, omitted here
    byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
    byte[] key = rsaPrivateKeyDecrypt(encryptedKey);
    Log.i("TEST","key "+new SecretKeySpec(key, "AES"));
    return new SecretKeySpec(key, "AES");
  }
  /**
   * <h2>getSecretPublicKeyPreMarshMallow</h2>
   * This method is used to get the secret key for pre marsh mallow device
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPublicKeyPreMarshMallow() throws Exception {
    String enryptedKeyB64 = manager.getEncryptionPublicKey();
    Log.i("TEST","enryptedKeyB64 "+enryptedKeyB64);
    // need to check null, omitted here
    byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
    byte[] key = rsaPublicKeyDecrypt(encryptedKey);
    Log.i("TEST","key "+new SecretKeySpec(key, "AES"));
    return new SecretKeySpec(key, "AES");
  }


  private byte[] rsaPrivateKeyDecrypt(byte[] encrypted) throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PRIVATEALIAS, null);
    Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
    output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
    CipherInputStream cipherInputStream = new CipherInputStream(
        new ByteArrayInputStream(encrypted), output);
    ArrayList<Byte> values = new ArrayList<>();
    int nextByte;
    while ((nextByte = cipherInputStream.read()) != -1) {
      values.add((byte) nextByte);
    }

    byte[] bytes = new byte[values.size()];
    for (int i = 0; i < bytes.length; i++) {
      bytes[i] = values.get(i);
    }
    return bytes;
  }

  private byte[] rsaPublicKeyDecrypt(byte[] encrypted) throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PUBLICALIAS, null);
    Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
    output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
    CipherInputStream cipherInputStream = new CipherInputStream(
        new ByteArrayInputStream(encrypted), output);
    ArrayList<Byte> values = new ArrayList<>();
    int nextByte;
    while ((nextByte = cipherInputStream.read()) != -1) {
      values.add((byte) nextByte);
    }

    byte[] bytes = new byte[values.size()];
    for (int i = 0; i < bytes.length; i++) {
      bytes[i] = values.get(i);
    }
    return bytes;
  }
  @Override
  public String decryptPrivateKeyPreMarshMallow() {
    Log.i("TEST","decryptData else  ");
    Cipher c;
    String str="";
    try {
      c = Cipher.getInstance(AES_MODE, "BC");
      c.init(Cipher.DECRYPT_MODE, getSecretPrivateKeyPreMarshMallow(),IV_PARAMETER_SPEC);
      byte[] encryptedBytes = Base64.decode(manager.getEncryptionPrivateKey(), Base64.DEFAULT);
      byte[] decodedBytes = c.doFinal(encryptedBytes);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        str = new String(decodedBytes, StandardCharsets.UTF_8);
      }
      else
      {
        str = new String(decodedBytes);
      }
      Log.i("TEST","decryptData srting "+str);


    }catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return str;
  }
  @Override
  public String decryptPublicKeyPreMarshMallow() {
    Log.i("TEST","decryptData else  ");
    Cipher c;
    String str="";
    try {
      c = Cipher.getInstance(AES_MODE, "BC");
      c.init(Cipher.DECRYPT_MODE, getSecretPublicKeyPreMarshMallow(),IV_PARAMETER_SPEC);
      byte[] encryptedBytes = Base64.decode(manager.getEncryptionPublicKey(), Base64.DEFAULT);
      byte[] decodedBytes = c.doFinal(encryptedBytes);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        str = new String(decodedBytes, StandardCharsets.UTF_8);
      }
      else
      {
        str = new String(decodedBytes);
      }
      Log.i("TEST","decryptData srting "+str);

    }catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return str;
  }


  /**
   * <h2>getSecretPrivateKeyPostLollipop</h2>
   * This method is used to get the secret key for post lollipop device
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPrivateKeyPostLollipop() throws Exception {
    Key s = keyStore.getKey(KEY_PRIVATEALIAS, null);
    Log.i("TEST","key "+s.toString());
    return keyStore.getKey(KEY_PRIVATEALIAS, null);
  }
  /**
   * <h2>getSecretPublicKeyPostLollipop</h2>
   * This method is used to get the secret key for post lollipop device
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPublicKeyPostLollipop() throws Exception {
    Key s = keyStore.getKey(KEY_PUBLICALIAS, null);
    Log.i("TEST","key "+s.toString());
    return keyStore.getKey(KEY_PUBLICALIAS, null);
  }

  @Override
  public void checkForKeystore()
  {
    try
    {
      keyStore = KeyStore.getInstance(ANDROID_KEYSTORE);
      keyStore.load(null);
    } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
      e.printStackTrace();
    }
    try
    {
      if (!keyStore.containsAlias(KEY_PRIVATEALIAS)){
        //mSignUpView.checkForVersion();
      }
    } catch (KeyStoreException e) {
      e.printStackTrace();
    }
  }
  private String encryptData(String privateKey, String publicKey, String messageFromDecrypt, String messageToDecrypt) {

    try {
      CryptographyExample cryptographyExample = new CryptographyExample();
      VirgilCrypto crypto = new VirgilCrypto();
      List<VirgilPublicKey> receiversPublicKeys = new ArrayList<>();
      // Import private key
      VirgilPrivateKey importedPrivateKey = cryptographyExample.importPrivateKey(privateKey, crypto);
      // Import public key
      VirgilPublicKey importedPublicKey = cryptographyExample.importPublicKey(publicKey, crypto);
      VirgilPublicKey importedDoctorPublicKey = cryptographyExample.importPublicKey(manager.getVirgilDoctorPublicKey(), crypto);
      byte[] signature = cryptographyExample.createSignature(importedPrivateKey, messageToDecrypt);
      // Verify data signature
      byte[] dataToSign = ConvertionUtils.toBytes(messageToDecrypt);
      boolean valid = cryptographyExample.verifySignature(signature, dataToSign, importedDoctorPublicKey, crypto);
      receiversPublicKeys.add(importedPublicKey);
      receiversPublicKeys.add(importedDoctorPublicKey);
      // Encrypt data
      byte[] encryptedData = cryptographyExample.dataEncryption(receiversPublicKeys, messageToDecrypt);
      messageFromDecrypt = Base64.encodeToString(encryptedData, Base64.DEFAULT);//Base64.encodeToString(encryptedData, Base64.DEFAULT);
    } catch (CryptoException e) {
      e.printStackTrace();
    }
    return messageFromDecrypt;
  }

}
