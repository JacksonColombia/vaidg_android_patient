package com.vaidg.confirmbookactivity;

import android.widget.TextView;

import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;

import com.pojo.CartModifiedData;

/**
 * <h2>ConfirmBookingContract</h2>
 * Created by Ali on 2/12/2018.
 */

public interface ConfirmBookingContract
{
    interface ContractPresenter extends BasePresenter
    {
        void onLiveBookingService(int paymentType, boolean isWalletSelected, String s,
            String toString, String cardId, String cartId, double bookingLat, double bookingLng
            , String address);
        void onAddSubCartModifyData(String catId, String serviceId, int action, int quantityInHr);
        void onGetCartId();

        void lastDues();

        void serverTime();

        void getPublicKey(String proId);

        void setAddressWithImage(TextView tvConfirmLocation, String bookingAddress);

        void callPromoCodeApi(double bookingLat, double bookingLng, int paymentType, String cartId, String trim);

        void timeMethod(TextView tvInConfirmBookingTypeDesc, long fromTime);


        /**
         * <h2>checkForKeystore</h2>
         * This method is used to check for keystore
         */
        void checkForKeystore();

        /**
         * <h2>decryptPublicKeyPostLollipop</h2>
         * This method is used to decrypt the data for post lollipop device
         * @return
         */
        String decryptPublicKeyPostLollipop();
        /**
         * <h2>decryptPrivateKeyPostLollipop</h2>
         * This method is used to decrypt the data for post lollipop device
         *
         * @return
         */
        String decryptPrivateKeyPostLollipop();
        /**
         * <h2>decryptPrivateKeyPreMarshMallow</h2>
         * This method is used to decrypt the data for pre marshmallow device
         * @return
         */
        String decryptPrivateKeyPreMarshMallow();
        /**
         * <h2>decryptPublicKeyPreMarshMallow</h2>
         * This method is used to decrypt the data for pre marshmallow device
         * @return
         */
        String decryptPublicKeyPreMarshMallow();
    }
    interface ContractView extends BaseView
    {

        void onSuccessBooking(long bookingid, int amount);

        void onSuccessCartId(String cartId);

        void onCartModification(CartModifiedData.DataSelected data);

        void onCartModified(String itemSelected, int i);

        void onHidePro();

        void onHourly();

        void onAlreadyCartPresent(String message, boolean b);

        void noDuesFoundLiveBooking();

        void onDuesFound(String msg, String addLine1, String formattedDate);

        void onConnectionError(String message, String cartId, String hourly);

        void CallLiveBook();

        void promoCode(double amount, String proCode);
    }
}
