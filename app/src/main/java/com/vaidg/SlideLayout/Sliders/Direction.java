package com.vaidg.SlideLayout.Sliders;

/**
 * Created by moda on 08/11/17.
 */


public enum Direction {
    FORWARD,
    INVERSE,
    BOTH
}