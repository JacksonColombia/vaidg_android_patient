package com.vaidg.scalablevideoview;


/*
 *
 * <h2>PivotPoint</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 *
 */
public enum PivotPoint {
    LEFT_TOP,
    LEFT_CENTER,
    LEFT_BOTTOM,
    CENTER_TOP,
    CENTER,
    CENTER_BOTTOM,
    RIGHT_TOP,
    RIGHT_CENTER,
    RIGHT_BOTTOM
}
