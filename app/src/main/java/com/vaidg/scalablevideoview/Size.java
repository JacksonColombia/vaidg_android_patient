package com.vaidg.scalablevideoview;

/*
 *
 * <h2>Size</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 *
 */
public class Size {

    private int mWidth;
    private int mHeight;

    public Size(int width, int height) {
        mWidth = width;
        mHeight = height;
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }
}
