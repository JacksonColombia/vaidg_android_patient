package com.vaidg.providerdetails;

import static com.vaidg.utilities.Constants.W_320;
import static com.vaidg.utilities.Constants.W_40;
import static com.vaidg.utilities.Constants.callTypeInOutTele;
import static com.vaidg.utilities.Constants.catName;
import static com.vaidg.utilities.Constants.proAddress;
import static com.vaidg.utilities.Constants.proLatitude;
import static com.vaidg.utilities.Constants.proLongitude;

import adapters.LangExpertiseAdapter;
import adapters.RatingReviewAdapter;
import adapters.WorkImageAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.vaidg.Login.LoginActivity;
import com.vaidg.R;
import com.vaidg.addTocart.AddToCart;
import com.vaidg.chatting.ChattingActivity;
import com.vaidg.confirmbookactivity.ConfirmBookActivity;
import com.vaidg.inCallOutCall.TimeSlots;
import com.vaidg.model.Location;
import com.vaidg.youraddress.model.YourAddrData;
import com.vaidg.providerdetails.viewschedule.ScheduleActivity;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.pojo.ProviderDetailsResponse;
import com.pojo.ReviewPojo;
import com.utility.AlertProgress;
import com.utility.DialogInterfaceListner;
import com.utility.PicassoTrustAll;

import dagger.android.support.DaggerAppCompatActivity;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.inject.Inject;

public class ProviderDetails extends DaggerAppCompatActivity implements
    ProviderDetailsContract.ProviderView
    , OnMapReadyCallback {

  @BindView(R.id.app_bar)
  AppBarLayout appBarLayout;
  @BindView(R.id.toolbar_layout)
  CollapsingToolbarLayout collapsingToolbarLayout;
  // @BindView(R.id.youtubeThumbNail)ImageView youtubeThumbNail;
  @BindView(R.id.toolbar)
  Toolbar toolbar;
  @BindView(R.id.toolBarTitle)
  TextView toolBarTitle;
  @BindView(R.id.ivDocProPic)
  ImageView ivDocProPic;
  @BindView(R.id.rlBook)
  RelativeLayout rlBook;
  @BindView(R.id.tvProBook)
  Button tvProBook;
  @BindView(R.id.progressBarPro)
  ProgressBar progressBarPro;


  /*BID*/
  @BindView(R.id.rlProDetBid)
  RelativeLayout rlProDetBid;
  @BindView(R.id.tvBidProHire)
  TextView tvBidProHire;
  @BindView(R.id.tvBidProAmount)
  TextView tvBidProAmount;
  @BindView(R.id.tvBidProAmountFee)
  TextView tvBidProAmountFee;
  @BindView(R.id.tvBidProViewMore)
  TextView tvBidProViewMore;
  @BindView(R.id.tvBidProDesc)
  TextView tvBidProDesc;


  @BindView(R.id.tvDoctorSpcl)
  TextView tvDoctorSpcl;
  @BindView(R.id.recyclerViewMetaData)
  RecyclerView recyclerViewMetaData;
  @BindView(R.id.recyclerViewDoctorimageView)
  RecyclerView recyclerViewDoctorimageView;
  @BindView(R.id.rlDoctorRateReview)
  LinearLayout rlDoctorRateReview;
  @BindView(R.id.llRatingLog)
  LinearLayout llRatingLog;
  @BindView(R.id.llReviewLog)
  LinearLayout llReviewLog;
  @BindView(R.id.tvDoctorAge)
  TextView tvDoctorAge;
  @BindView(R.id.tvLocationTitle)
  TextView tvLocationTitle;
  @BindView(R.id.tvDoctorReview)
  TextView tvDoctorReview;
  @BindView(R.id.veProDivider7)
  View veProDivider7;
  @BindView(R.id.tvRevwAvrgRtin)
  TextView tvRevwAvrgRtin;
  @BindView(R.id.tvavgrtnTotalRev)
  TextView tvavgrtnTotalRev;
  @BindView(R.id.rtReview)
  RatingBar rtReview;
  @BindView(R.id.recyclerViewRating)
  RecyclerView recyclerViewRating;
  @BindView(R.id.recyclerViewReview)
  RecyclerView recyclerViewReview;
  @BindView(R.id.nestedScrollView)
  NestedScrollView nestedScrollView;
  @BindView(R.id.cvDoctorProf)
  CardView cvDoctorProf;
  @BindView(R.id.tvDoctorExpYr)
  TextView tvDoctorExpYr;
  @BindView(R.id.tvDoctorExp)
  TextView tvDoctorExp;
  @BindView(R.id.tvDoctorDistanceAway)
  TextView tvDoctorDistanceAway;
  @BindView(R.id.tvDoctorDistance)
  TextView tvDoctorDistance;
  @BindView(R.id.tvConsultationFee)
  TextView tvConsultationFee;
  @BindView(R.id.tvConsultationFeeAmt)
  TextView tvConsultationFeeAmt;
  @BindView(R.id.llConsultationFee)
  RelativeLayout llConsultationFee;
  @BindView(R.id.tvDoctorReviewsCount)
  TextView tvDoctorReviewsCount;
  @BindView(R.id.tvDoctorReviews)
  TextView tvDoctorReviews;
  @BindView(R.id.tvDocName)
  TextView tvDocName;
  @BindView(R.id.providerProgress)
  ProgressBar providerProgress;
  @BindView(R.id.rlDoctorImage)
  RelativeLayout rlDoctorImage;
  @BindView(R.id.tvDoctorimage)
  TextView tvDoctorimage;
  @BindView(R.id.noDoctorimage)
  TextView noDoctorimage;
  @BindView(R.id.rlDistance)
  RelativeLayout rlDistance;

  @BindView(R.id.llMap)
  LinearLayout llMap;
  @BindView(R.id.tvLocationAddress1)
  TextView tvLocationAddress1;
  @BindView(R.id.tvLocationAddress2)
  TextView tvLocationAddress2;
  // @BindView(R.id.ivInCallLocation)ImageView ivInCallLocation;
  @Inject
  AppTypeface appTypeface;
  @Inject
  ProviderDetailsContract.ProviderPresenter presenter;
  @Inject
  SessionManagerImpl manager;
  @Inject
  AlertProgress alertProgress;
  SupportMapFragment mapFragment;
  @BindView(R.id.ivCal)
  ImageView ivCal;
  double latitude, longitude;
  private int pastVisiblesItems, visibleItemCount, totalItemCount;
  private int pageCount = 0;
  private double bidAmount = 0;
  private boolean loading = true;
  private LinearLayoutManager linearLayoutManagerReview;
  private LangExpertiseAdapter langAdapter, expertiseAdapter;
  private WorkImageAdapter mImageAdapter;
  private String proId;
  private Bundle intentBundle;
  private String[] lanArrayExpertise;
  private String name = "";
  private boolean isProfileView = false;
  private boolean isBidding = false;
  private long bid;
  private int bidStatus = 0;
  private GoogleMap mMap;
  private RatingReviewAdapter ratinReviewAdaptr;
  private ArrayList<ProviderDetailsResponse.ProviderResponseDetails.ReviewList> rateReview =
      new ArrayList<>();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_provider_details);
    ButterKnife.bind(this);
    mapFragment = (SupportMapFragment) getSupportFragmentManager()
        .findFragmentById(R.id.map);
    getIntentValue();
    typefaceValue();
    calculate();
    valueForAdapter();
    setToolBar();
    if (isBidding && bidStatus == 17) {
      fabIcon();
    }
    callType();
    ivCal.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(getApplicationContext(), ScheduleActivity.class);
        startActivity(intent);
      }
    });


  }

  private void calculate() {
    int w22 = Utility.getScreenWidth() * W_40 / W_320;
    tvProBook.getLayoutParams().height = w22;
  }

  private void callType() {

    if (callTypeInOutTele == 1) {
      // llConsultation.setVisibility(View.VISIBLE);
    } else if (callTypeInOutTele == 3 || Constants.calltype == 3) {
    } else {
    }

    onLoadMore();
  }

  @SuppressLint("RestrictedApi")
  private void fabIcon() {
    FloatingActionButton fab = findViewById(R.id.fab);
    fab.setVisibility(View.VISIBLE);
    fab.setOnClickListener(view -> {
      Intent intent = new Intent(this, ChattingActivity.class);
      if (bidStatus == 1 || bidStatus == 2
          || bidStatus == 3 || bidStatus == 6 || bidStatus == 7
          || bidStatus == 8 || bidStatus == 9 || bidStatus == 17) {
        intent.putExtra("isChating", true);
      }
      intent.putExtra("STATUSCODE", bidStatus);
      intent.putExtra("CurrencySymbol", Constants.currencySymbol);
      intent.putExtra("AMOUNT", bidAmount);
      intent.putExtra("CallType", callTypeInOutTele);
      startActivity(intent);
      overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
    });
  }

  private void onLoadMore() {

    recyclerViewReview.setNestedScrollingEnabled(false);

  }

  private void setToolBar() {


    collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
    collapsingToolbarLayout.setCollapsedTitleTypeface(appTypeface.getHind_semiBold());
    setSupportActionBar(toolbar);
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayShowHomeEnabled(true);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
    toolbar.setNavigationIcon(R.drawable.ic_back);
    toolbar.setNavigationOnClickListener(view -> onBackPressed());
    if(Constants.callTypeInOutTele != 3)
    {
      rlDistance.setVisibility(View.VISIBLE);
    }
  }

  private void valueForAdapter() {


    tvProBook.setOnClickListener(view -> {
      Intent intent;
      if (Constants.bookingModel == 4) {
        if (manager.getGuestLogin()) {
          intent = new Intent(this, LoginActivity.class);
          //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        } else {
          if (callTypeInOutTele != 2) //if telecall and incall
          {
            intent = new Intent(ProviderDetails.this, TimeSlots.class);

            intent.putExtras(intentBundle);

          } else {
            intent = new Intent(ProviderDetails.this, ConfirmBookActivity.class);
          }
        }
      } else {
        if (callTypeInOutTele != 2) {
          intent = new Intent(ProviderDetails.this, TimeSlots.class);
          intent.putExtras(intentBundle);
        } else {
          intent = new Intent(ProviderDetails.this, AddToCart.class);
          intent.putExtras(intentBundle);
        }

      }
      startActivity(intent);
      overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);

    });

  }


  private void appBarChangeListener() {
    appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
      boolean isShow = false;
      int scrollRange = -1;

      @Override
      public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

        if (scrollRange == -1) {
          scrollRange = appBarLayout.getTotalScrollRange();
        }
        Log.d("TAG", "onOffsetChanged: " + verticalOffset + "  scroll " + scrollRange);
        if (scrollRange + verticalOffset == 0) {
          // toolbar_layout.setTitle(name);
          toolBarTitle.setText(name);
          toolBarTitle.setVisibility(View.VISIBLE);
          //  toolBarTitle.setText("Registereres");
          // ivProDtlsPic.setVisibility(View.GONE);
          // toolbar.setNavigationIcon(R.drawable.ic_back);
          isShow = true;
        } else if (isShow) {
         // toolBarTitle.setText(getResources().getString(R.string.docProfile));
          toolBarTitle.setText(name);
          toolBarTitle.setVisibility(View.GONE);
          // toolBarTitle.setText("");
          //   ivProDtlsPic.setVisibility(View.VISIBLE);
          //  toolbar.setNavigationIcon(R.drawable.ic_back);
          isShow = false;
        }
      }
    });
  }


  private void getIntentValue() {
    intentBundle = new Bundle();

    String bidDesc = "";

    if (getIntent().getExtras() != null) {
      proId = getIntent().getStringExtra("ProviderId");
      isProfileView = getIntent().getBooleanExtra("isProFileView", false);
      isBidding = getIntent().getBooleanExtra("isBidding", false);
      bidDesc = getIntent().getStringExtra("bidDesc");
      bidAmount = getIntent().getDoubleExtra("bidAmount", 0);
      bid = getIntent().getLongExtra("BID", 0);
      bidStatus = getIntent().getIntExtra("BIDStatus", 0);
      onShowProgress();
      callProvider();

    }


    if (!isProfileView) {
      rlBook.setVisibility(View.VISIBLE);
    } else {
      rlBook.setVisibility(View.GONE);
      if (isBidding) {
        rlProDetBid.setVisibility(View.VISIBLE);
        tvBidProDesc.setText(bidDesc);

        if ("".equals(bidDesc)) {
          tvBidProViewMore.setVisibility(View.GONE);
          tvBidProDesc.setVisibility(View.GONE);
        }
      }
    }
  }

  private void callProvider() {

    if (alertProgress.isNetworkAvailable(this)) {
      presenter.onProviderDetailService(proId);
    } else {
      alertProgress.showNetworkAlert(this);
    }
  }

  @OnClick({R.id.tvBidProHire})
  public void onAboutReviewClicked(View v) {
    switch (v.getId()) {
      case R.id.tvBidProHire:
        if (alertProgress.isNetworkAvailable(this)) {
          alertProgress.alertPositiveNegativeOnclick(ProviderDetails.this,
              getString(R.string.areYouSureYouWantOTHire) + " '" + name + "' " +
                  getString(R.string.forThisJob), getString(R.string.hire),
              getString(R.string.yes),
              getString(R.string.no)
              , true, isClicked -> {
                if (isClicked) {
                  onShowProgress();
                  if(Utility.isNetworkAvailable(this))
                  presenter.hireProvider(bid, proId);
                }
              });

        } else {
          alertProgress.showNetworkAlert(this);
        }
        break;
    }
  }

  private void typefaceValue() {
    tvDocName.setTypeface(appTypeface.getHind_semiBold());
    toolBarTitle.setTypeface(appTypeface.getHind_semiBold());
    tvDoctorAge.setTypeface(appTypeface.getHind_medium());
    tvDoctorSpcl.setTypeface(appTypeface.getHind_regular());
    tvDoctorExp.setTypeface(appTypeface.getHind_semiBold());
    tvDoctorExpYr.setTypeface(appTypeface.getHind_medium());
    tvDoctorReviews.setTypeface(appTypeface.getHind_semiBold());
    tvDoctorReviewsCount.setTypeface(appTypeface.getHind_medium());
    tvDoctorDistance.setTypeface(appTypeface.getHind_semiBold());
    tvDoctorDistanceAway.setTypeface(appTypeface.getHind_medium());
    tvConsultationFee.setTypeface(appTypeface.getHind_semiBold());
    tvConsultationFeeAmt.setTypeface(appTypeface.getHind_medium());
    tvDoctorReview.setTypeface(appTypeface.getHind_medium());
    tvRevwAvrgRtin.setTypeface(appTypeface.getHind_medium());
    tvavgrtnTotalRev.setTypeface(appTypeface.getHind_regular());

    tvLocationAddress1.setTypeface(appTypeface.getHind_medium());
    tvLocationAddress2.setTypeface(appTypeface.getHind_regular());
    tvProBook.setTypeface(appTypeface.getHind_semiBold());
    toolBarTitle.setTypeface(appTypeface.getHind_semiBold());
    tvDoctorimage.setTypeface(appTypeface.getHind_medium());
    tvBidProHire.setTypeface(appTypeface.getHind_semiBold());
    tvBidProAmount.setTypeface(appTypeface.getHind_semiBold());
    tvBidProAmountFee.setTypeface(appTypeface.getHind_regular());
    tvBidProViewMore.setTypeface(appTypeface.getHind_regular());
    tvBidProDesc.setTypeface(appTypeface.getHind_regular());
    tvLocationTitle.setTypeface(appTypeface.getHind_semiBold());

    tvDocName.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_16));
    tvRevwAvrgRtin.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_42));
    tvavgrtnTotalRev.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_10));
    tvDoctorAge.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_13));
    tvDoctorSpcl.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_12));
    tvDoctorExp.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_12));
    tvDoctorReviews.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_12));
    tvDoctorDistance.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_12));
    tvConsultationFee.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_14));
    tvConsultationFeeAmt.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_15));
    tvLocationAddress1.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_15));
    tvLocationAddress2.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_15));
    tvDoctorReview.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_15));
    tvLocationTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_14));
    tvDoctorExpYr.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_14));
    tvDoctorReviewsCount.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_14));
    tvDoctorDistanceAway.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_14));
    tvProBook.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_18));
  }

  @Override
  public void onSessionExpired() {

  }

  @Override
  public void onLogout(String message, SessionManagerImpl sessionManager) {
    alertProgress.alertPositiveOnclick(this, message, getString(R.string.logout),
        getString(R.string.ok), new DialogInterfaceListner() {
          @Override
          public void dialogClick(boolean isClicked) {
            Utility.setMAnagerWithBID(ProviderDetails.this, manager);
          }
        });

  }

  @Override
  public void onError(String error) {
    alertProgress.alertinfo(this, error);

  }

  @Override
  public void onConnectionError(String connectionError) {

  }

  @Override
  public void onShowProgress() {
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    progressBarPro.setVisibility(View.GONE);
  }

  @Override
  public void onHideProgress() {
    if (progressBarPro != null) {
      progressBarPro.setVisibility(View.GONE);
    }
    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
  }

  @Override
  public void onSuccess(ProviderDetailsResponse.ProviderResponseDetails providerDataResp) {
    Log.d("Abc", "onSuccess: " + providerDataResp);


    consultFee(catName, providerDataResp.getDegree(), providerDataResp.getAmount(), providerDataResp.getCurrencySymbol());


    distance(providerDataResp.getDistance());

    if (!providerDataResp.getProfilePic().isEmpty()) {
      profilePhoto(providerDataResp.getProfilePic());
    }
    showAddress(providerDataResp.getAddress(), providerDataResp.getLocation());


    intentBundle.putString("ImagePro", providerDataResp.getProfilePic());
    intentBundle.putString("ProviderId", proId);

    String yearOfExperience = providerDataResp.getYearOfExperience() + " ";
    tvDoctorExpYr.setText(yearOfExperience + " " + getResources().getString(R.string.yrs));
    String reviews = providerDataResp.getNoOfReview() + " ";
    tvDoctorReviewsCount.setText(reviews + "" + getResources().getString(R.string.plus));
    name = providerDataResp.getTitle() + " "
        + providerDataResp.getFirstName() + " "
        + providerDataResp.getLastName();
    String bookProName = getResources().getString(R.string.bookSmall) + " " + name;
    tvProBook.setText(bookProName);
    tvDocName.setText(name);
    appBarChangeListener();
/*
    if(providerDataResp.getAge() != null && !providerDataResp.getAge().isEmpty()) {
      tvDoctorAge.setText(providerDataResp.getAge() + ""
              + getResources().getString(R.string.yr));
    }
*/


    if (providerDataResp.getRatingLog().size() > 0) {
      rlDoctorRateReview.setVisibility(View.VISIBLE);
      llRatingLog.setVisibility(View.VISIBLE);

   /*   RatingReviews ratingReviews = (RatingReviews) findViewById(R.id.rating_reviews);

      int colors[] = new int[]{
          Color.parseColor("#0e9d58"),
          Color.parseColor("#bfd047"),
          Color.parseColor("#ffc105"),
          Color.parseColor("#ef7e14"),
          Color.parseColor("#d36259")};

      int raters[] = new int[]{
          (int) providerDataResp.getRatingLog().get(0).getRating(),
          (int) providerDataResp.getRatingLog().get(1).getRating(),
          (int) providerDataResp.getRatingLog().get(2).getRating(),
          (int) providerDataResp.getRatingLog().get(3).getRating(),
          (int) providerDataResp.getRatingLog().get(4).getRating()
      };

      String strings[] = new String[]{
          getResources().getString(R.string.cleanliness),
          getResources().getString(R.string.punctuality),
          getResources().getString(R.string.value),
          getResources().getString(R.string.overallopinion),
          getResources().getString(R.string.courteousness)};
      ratingReviews.createRatingBars(5,strings, colors, raters);
*/


      LinearLayoutManager lloutMAnager = new LinearLayoutManager(this);

      RatingAdap ratingAdap = new RatingAdap(providerDataResp.getRatingLog());
      recyclerViewRating.setLayoutManager(lloutMAnager);
      recyclerViewRating.setAdapter(ratingAdap);
      recyclerViewRating.setNestedScrollingEnabled(false);


    }
    if (providerDataResp.getReview().size() > 0) {
      llReviewLog.setVisibility(View.VISIBLE);
      rateReview.clear();
      rateReview.addAll(providerDataResp.getReview());
      linearLayoutManagerReview = new LinearLayoutManager(this);
      recyclerViewReview.setLayoutManager(linearLayoutManagerReview);
      ratinReviewAdaptr = new RatingReviewAdapter(rateReview, true);
      recyclerViewReview.setAdapter(ratinReviewAdaptr);


      nestedScrollView.setOnScrollChangeListener(
          (NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            if (v.getChildAt(v.getChildCount() - 1) != null) {
              if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight()
                  - v.getMeasuredHeight())) &&
                  scrollY > oldScrollY) {

                visibleItemCount = linearLayoutManagerReview.getChildCount();
                totalItemCount = linearLayoutManagerReview.getItemCount();
                pastVisiblesItems = linearLayoutManagerReview.findFirstVisibleItemPosition();
                if (loading) {
                  if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                    loading = false;
                    providerProgress.setVisibility(View.GONE);
                    pageCount++;
                    if(Utility.isNetworkAvailable(this))
                    presenter.callReviewApi(pageCount, proId);
                  }
                }
              }
            }
          });

    }

    String nnumberRatinReview = providerDataResp.getRatingLog().size() + " " + getString(
        R.string.rating) + " | " + providerDataResp.getNoOfReview() + " " + getString(
        R.string.reviews);
    tvavgrtnTotalRev.setText(nnumberRatinReview);
    String rating = String.valueOf(providerDataResp.getRating());
    tvRevwAvrgRtin.setText(rating);
    rtReview.setIsIndicator(true);
    rtReview.setRating(Float.parseFloat(providerDataResp.getRating()));


    if (providerDataResp.getWorkImage().size() > 0) {
      rlDoctorImage.setVisibility(View.VISIBLE);
      noDoctorimage.setVisibility(View.GONE);
      recyclerViewDoctorimageView.setVisibility(View.VISIBLE);
      ArrayList<String> stringArrayList = new ArrayList<>();
      stringArrayList.clear();
      stringArrayList.addAll(providerDataResp.getWorkImage());
      recyclerViewDoctorimageView.setHasFixedSize(true);
      mImageAdapter = new WorkImageAdapter(appTypeface, stringArrayList);
      LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
      recyclerViewDoctorimageView.setLayoutManager(linearLayoutManager);
      recyclerViewDoctorimageView.setItemAnimator(new DefaultItemAnimator());
      recyclerViewDoctorimageView.setAdapter(mImageAdapter);
    } else {
      noDoctorimage.setVisibility(View.VISIBLE);
      recyclerViewDoctorimageView.setVisibility(View.GONE);
      veProDivider7.setVisibility(View.GONE);
    }


    if (providerDataResp.getMetaDataArr().size() > 0) {
      ArrayList<ProviderDetailsResponse.MetaDataArray> metaDataArrays = new ArrayList<>();
      metaDataArrays.clear();
      metaDataArrays.addAll(providerDataResp.getMetaDataArr());
      recyclerViewMetaData.setVisibility(View.VISIBLE);
      recyclerViewMetaData.setHasFixedSize(true);
      langAdapter = new LangExpertiseAdapter(appTypeface, metaDataArrays, presenter);
      LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
      recyclerViewMetaData.setLayoutManager(linearLayoutManager);
      recyclerViewMetaData.setItemAnimator(new DefaultItemAnimator());
      recyclerViewMetaData.setAdapter(langAdapter);
    }


    Constants.proId = proId;
    intentBundle.putString("NAME", name);
    intentBundle.putString("expertise", catName);

    nestedScrollView.scrollTo(0, 0);


  }

  private void distance(double distance) {
  //  Constants.DISTANCEMATRIXUNIT = (int) distance;
    String strDistance = Utility.getEtaInUnitsFromDbDistanceCalculate(distance, this);
    tvDoctorDistanceAway.setText(strDistance);
  }

  private void profilePhoto(String profilePic) {
    if(profilePic != null && !profilePic.isEmpty()) {
      PicassoTrustAll.getInstance(ProviderDetails.this)
          .load(profilePic)
          .placeholder(R.drawable.profile_price_bg)   // optional
          .error(R.drawable.profile_price_bg)// optional
          .transform(new PicassoCircleTransform())
          .into(ivDocProPic);
    }
  }

  private void consultFee(String catName, String degree, double amount, String currencySymbol) {
//    pricePerHour = amount;
    tvDoctorSpcl.setVisibility(View.VISIBLE);
    tvDoctorSpcl.setText(degree + "," + catName);
    llConsultationFee.setVisibility(View.VISIBLE);
    DecimalFormat precision = new DecimalFormat("0.00");
    String sourceString = currencySymbol + " "+ precision.format(amount);
    tvConsultationFee.setText(getResources().getString(R.string.consulationFee));
    tvConsultationFeeAmt.setText(sourceString);
    //Utility.setAmtOnRecept(amount, tvConsultationFee, currencySymbol);
   /* if (callTypeInOutTele == 2) {
        llPerHour.setVisibility(View.VISIBLE);
        Utility.setAmtOnRecept(amount, tvDoctorSpcl,currencySymbol);
    }else {
      llConsultationFee.setVisibility(View.VISIBLE);
      DecimalFormat precision = new DecimalFormat("0.00");
      String sourceString = getResources().getString(R.string.consulationFee) + " " + "<b>"
          + currencySymbol + " " + "</b> " + "<b>" + precision.format(amount) + "</b> ";
      tvConsultationFee.setText(Html.fromHtml(sourceString));
    }*/
  }

  private void showAddress(YourAddrData address, Location location) {
    if (callTypeInOutTele == 1) {
      mapFragment.getMapAsync(this);
      llMap.setVisibility(View.VISIBLE);
      proLatitude = address.getLatitude();
      proLongitude = address.getLongitude();
      if (address.getLatitude() != 0 && address.getLongitude() != 0) {
        latitude = address.getLatitude();
        longitude = address.getLongitude();
      } else {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
      }
      if (address.getAddLine1() != null && !address.getAddLine1().isEmpty()) {
        tvLocationAddress1.setVisibility(View.VISIBLE);
        proAddress = address.getAddLine1();
        tvLocationAddress1.setText(address.getAddLine1());
      } else {
        tvLocationAddress1.setVisibility(View.GONE);
      }
      if (address.getAddLine2() != null && !address.getAddLine2().isEmpty()) {
        tvLocationAddress2.setVisibility(View.VISIBLE);
        tvLocationAddress2.setText(address.getAddLine2());
      } else {
        tvLocationAddress2.setVisibility(View.GONE);
      }
    }
  }

  @Override
  public void onReviewSuccess(ReviewPojo.SignUpDataSid reviewList) {

    providerProgress.setVisibility(View.GONE);
    if (reviewList.getReviews().size() > 0) {

      loading = reviewList.getReviews().size() > 4;
      rateReview.addAll(reviewList.getReviews());
      ratinReviewAdaptr.notifyDataSetChanged();
    } else {
      loading = false;
    }
  }

  @Override
  public void onErrorNotConnected(String message) {

    alertProgress.tryAgain(this, getString(R.string.pleaseCheckInternet),
        getString(R.string.system_error), new DialogInterfaceListner() {
          @Override
          public void dialogClick(boolean isClicked) {
            if (isClicked) {
              callProvider();
            }
          }
        });
  }

  @Override
  public void onBookingHired() {

    Intent intent = new Intent();
    setResult(RESULT_OK, intent);
    finish();//finishing activityz

  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    finish();
    overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;

    // Add a marker in Sydney and move the camera
    LatLng sydney = new LatLng(latitude, longitude);
    mMap.addMarker(new MarkerOptions().position(sydney).title(name + " Location"));

    //  CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(sydney, 14.0f);
    //  mMap.animateCamera(cameraUpdate);
    //mMap.moveCamera(cameraUpdate);
    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
        new LatLng(sydney.latitude,
            sydney.longitude)
        , 15));


    googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
      @Override
      public void onMapClick(LatLng latLng) {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
            Uri.parse("http://maps.google.com/maps?daddr=" + latitude + "," + longitude));
        startActivity(intent);
      }
    });

  }

  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);

    //Utility.checkAndShowNetworkError(this);
  }

  private class RatingAdap extends RecyclerView.Adapter {
    private Context mContext;
    private ArrayList<ProviderDetailsResponse.RatingLog> ratingLog;

    public RatingAdap(ArrayList<ProviderDetailsResponse.RatingLog> ratingLog) {
      this.ratingLog = ratingLog;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rating_log, parent, false);
      mContext = parent.getContext();
      return new RecyclerHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
      RecyclerHolder hold = (RecyclerHolder) holder;
      if (ratingLog.get(position).getName().equals(
          mContext.getResources().getString(R.string.cleanliness))) {
        hold.tvRatingName.setText(ratingLog.get(position).getName());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          hold.rtRating.setCompoundDrawableTintList(ContextCompat.getColorStateList(mContext,R.color.custom_progress_bar_horizontal));
        }
        Drawable draw = getResources().getDrawable(R.drawable.custom_progress_bar_horizontal);
       // hold.rtRating.setProgressDrawable(draw);
        hold.rtRating.setText(String.valueOf(ratingLog.get(position).getRating()));
      } else if (ratingLog.get(position).getName().equals(
          mContext.getResources().getString(R.string.punctuality))) {
        hold.tvRatingName.setText(ratingLog.get(position).getName());
        hold.rtRating.setText(String.valueOf(ratingLog.get(position).getRating()));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          hold.rtRating.setCompoundDrawableTintList(ContextCompat.getColorStateList(mContext,R.color.custom_progress_bar_horizontal_blue));
        }

        Drawable draw = getResources().getDrawable(R.drawable.custom_progress_bar_horizontal_blue);
      //  hold.rtRating.setProgressDrawable(draw);
        // hold.rtRating.setText(10);
      } else if (ratingLog.get(position).getName().equals(
          mContext.getResources().getString(R.string.value))) {
        hold.tvRatingName.setText(ratingLog.get(position).getName());
        hold.rtRating.setText(String.valueOf(ratingLog.get(position).getRating()));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          hold.rtRating.setCompoundDrawableTintList(ContextCompat.getColorStateList(mContext,R.color.custom_progress_bar_horizontal_orange));
        }

        Drawable draw = getResources().getDrawable(R.drawable.custom_progress_bar_horizontal_orange);
       // hold.rtRating.setProgressDrawable(draw);
        //hold.rtRating.setText(10);
      } else if (ratingLog.get(position).getName().equals(
          mContext.getResources().getString(R.string.overallopinion))) {
        hold.tvRatingName.setText(ratingLog.get(position).getName());
        hold.rtRating.setText(String.valueOf(ratingLog.get(position).getRating()));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          hold.rtRating.setCompoundDrawableTintList(ContextCompat.getColorStateList(mContext,R.color.custom_progress_bar_horizontal_light_green));
        }

        Drawable draw = getResources().getDrawable(R.drawable.custom_progress_bar_horizontal_light_green);
      //  hold.rtRating.setProgressDrawable(draw);
        // hold.rtRating.setText(10);
      } else if (ratingLog.get(position).getName().equals(
          mContext.getResources().getString(R.string.courteousness))) {
        hold.tvRatingName.setText(ratingLog.get(position).getName());
        hold.rtRating.setText(String.valueOf(ratingLog.get(position).getRating()));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          hold.rtRating.setCompoundDrawableTintList(ContextCompat.getColorStateList(mContext,R.color.custom_progress_bar_horizontal_violet));
        }

        Drawable draw = getResources().getDrawable(R.drawable.custom_progress_bar_horizontal_violet);
      //  hold.rtRating.setProgressDrawable(draw);
        //  hold.rtRating.setText(10);
      } else {
        Drawable draw = getResources().getDrawable(R.drawable.custom_progress_bar_horizontal);
       // hold.rtRating.setProgressDrawable(draw);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          hold.rtRating.setCompoundDrawableTintList(ContextCompat.getColorStateList(mContext,R.color.custom_progress_bar_horizontal));
        }

        hold.rtRating.setText(String.valueOf(3));
      }
    }

    @Override
    public int getItemCount() {
      return ratingLog == null ? 0 : ratingLog.size();
    }

    private class RecyclerHolder extends RecyclerView.ViewHolder {
      private TextView tvRatingName;
      private TextView rtRating;

      public RecyclerHolder(View itemView) {
        super(itemView);
        tvRatingName = itemView.findViewById(R.id.tvRatingName);
        rtRating = itemView.findViewById(R.id.rtRating);
        tvRatingName.setTypeface(AppTypeface.getInstance(mContext).getHind_regular());
        rtRating.setTypeface(AppTypeface.getInstance(mContext).getHind_regular());
        tvRatingName.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
        rtRating.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
      }
    }
  }

  private class GeocoderHandler extends Handler {
    @Override
    public void handleMessage(Message message) {
      String locationAddress;
      switch (message.what) {
        case 1:
          Bundle bundle = message.getData();
          locationAddress = bundle.getString("address");
          break;
        default:
          locationAddress = null;
      }
      tvLocationAddress1.setText(locationAddress);

    }
  }
}
