package com.vaidg.providerdetails;

import com.vaidg.Dagger2.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>ProviderDetailsModules</h2>
 * Created by Ali on 2/5/2018.
 */

@Module
public interface ProviderDetailsModules
{

    @Binds
    @ActivityScoped
    ProviderDetailsContract.ProviderPresenter providePresenter(ProviderPresenterImpl providerPresenter);

    @Binds
    @ActivityScoped
    ProviderDetailsContract.ProviderView providerDetailsView(ProviderDetails providerDetails);


}
