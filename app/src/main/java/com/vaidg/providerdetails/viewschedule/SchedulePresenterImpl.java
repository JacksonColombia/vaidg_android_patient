package com.vaidg.providerdetails.viewschedule;


import android.util.Log;

import com.vaidg.utilities.LSPApplication;
import com.google.gson.Gson;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;
import com.pojo.ScheduleMonthPojo;
import com.utility.RefreshToken;
import com.vaidg.utilities.Utility;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;


/**
 * <h2>ProviderPresenterImpl</h2>
 * Created by Ali on 2/5/2018.
 */

public class SchedulePresenterImpl implements ScheduleContract.SchedulePresenter
{

    @Inject
    ScheduleContract.ScheduleView view;

   /* @Inject
    ScheduleContract.SchedulePresenter presenterImple;
*/
    @Inject
    SessionManagerImpl sessionManager;
    @Inject
    Gson gson;

    @Inject
    LSPServices lspServices;

    private SimpleDateFormat serverFormat,displayHourFormat, displayHourFormatInBooked, displayPeriodFormat;

    private boolean isFragmentAttached = false, isCurrentMonth = false;
    private String scheduleData="";

    @Inject
    SchedulePresenterImpl()
    {
       // this.view = new ScheduleFramentModel(this);
        isFragmentAttached = true;

        displayHourFormat = new SimpleDateFormat("h:mm", Locale.US);
        displayHourFormatInBooked = new SimpleDateFormat("hh:mm a", Locale.US);
        displayPeriodFormat = new SimpleDateFormat("a", Locale.US);
        serverFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
        displayHourFormat.setTimeZone(Utility.getTimeZone());
        displayHourFormatInBooked.setTimeZone(Utility.getTimeZone());
        displayPeriodFormat.setTimeZone(Utility.getTimeZone());
        serverFormat.setTimeZone(Utility.getTimeZone());
    }


    @Override
    public void getSchedule(String auth, String date, boolean isCurrentMonth,String providerId) {

        this.isCurrentMonth = isCurrentMonth;
        if(!isCurrentMonth || sessionManager.getScheduleData().equals(""))
        {
            view.showProgress();
        }
        else
        {
            onSuccessGetSchedule(sessionManager.getScheduleData());
        }
       // model.getShedule(sessionToken,date);

        Observable<Response<ResponseBody>> request = lspServices.getSchdeuleSlots(auth,Constants.selLang,Constants.PLATFORM_ANDROID,date,providerId);

        request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                       // compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Response<ResponseBody> value) {
                        JSONObject errJsonD;
                        Log.e("SIDE_PROF", "Reqq URL :: " + value.raw().request().url());
                        Log.e("SIDE_PROF", "code :: " + value.code() + " msg " + value.message());
                        try
                        {
                            switch (value.code())
                            {
                                case Constants.SUCCESS_RESPONSE:
                               /* if(){
                                        //profileView.onLogout(email);
                                    }else*/
                                    String responseBody = value.body().string();
                                      if(view != null)
                                        view.hideProgress();
                                        onSuccessGetSchedule(responseBody);
                                    break;
                                case Constants.SESSION_EXPIRED:
                                    errJsonD = new JSONObject(value.errorBody().string());

                                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),sessionManager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                          if(view != null)
                                            view.hideProgress();
                                            LSPApplication.getInstance().setAuthToken(sessionManager.getSID(),sessionManager.getSID(),newToken);
                                            getSchedule(auth,date,isCurrentMonth,providerId);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {

                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                          if(view != null) {
                                            view.onLogout(msg, sessionManager);
                                            view.hideProgress();
                                          }
                                        }
                                    });
                                    break;
                                case Constants.SESSION_LOGOUT:
                                  if(view != null)
                                    view.hideProgress();
                                    errJsonD = new JSONObject(value.errorBody().string());
                                  if(view != null)
                                    view.onError(errJsonD.getString("message"));
                                    break;
                                default:
                                    errJsonD = new JSONObject(value.errorBody().string());
                                  if(view != null) {
                                    view.hideProgress();
                                    view.onError(errJsonD.getString("message"));
                                  }
                                    break;
                            }
                        }catch (Exception e)
                        {
                          if(view != null) {
                            view.hideProgress();
                          }
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                      if(view != null) {
                        view.hideProgress();
                      }
                      e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                      if(view != null) {
                        view.hideProgress();
                      }
                    }
                });
    }

    @Override
    public void onSuccessGetSchedule(String result) {
        if(isFragmentAttached)
        {
            view.hideProgress();
        }
        if(scheduleData.equals(result))
        {
            return;
        }
        scheduleData = result;
        if(isCurrentMonth)
        {
            sessionManager.setScheduleData(result);
        }
        ScheduleMonthPojo scheduleMonthPojo = gson.fromJson(result, ScheduleMonthPojo.class);
        if(isFragmentAttached)
        {
            view.onSuccessGetSchedule(scheduleMonthPojo);
        }
    }


    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {
        isFragmentAttached = false;

    }

}
