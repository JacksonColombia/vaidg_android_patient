package com.vaidg.providerdetails.viewschedule;

import com.vaidg.Dagger2.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>ProviderDetailsModules</h2>
 * Created by Ali on 2/5/2018.
 */

@Module
public interface ScheduleModules
{

    @Binds
    @ActivityScoped
    ScheduleContract.SchedulePresenter schedulePresenter(SchedulePresenterImpl schedulePresenter);

    @Binds
    @ActivityScoped
    ScheduleContract.ScheduleView scheduleView(ScheduleActivity scheduleFragment);


}
