package com.vaidg.providerdetails.viewschedule;

import android.widget.TextView;

import com.vaidg.utilities.SessionManagerImpl;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;
import com.pojo.ProviderDetailsResponse;
import com.pojo.ReviewPojo;
import com.pojo.ScheduleMonthPojo;

import java.util.List;

/**
 * <h2>ProviderDetailsContract</h2>
 * Created by Ali on 2/5/2018.
 */

public interface ScheduleContract
{
    interface SchedulePresenter extends BasePresenter
    {
       void getSchedule(String sessionToken, String date, boolean isCurrentMonth,String providerId);
       void onSuccessGetSchedule(String result);
    }
    interface ScheduleView extends BaseView
    {
        void onLogout(String message, SessionManagerImpl sessionManager);
       void showProgress();
       void hideProgress();
        void onSuccessGetSchedule(ScheduleMonthPojo scheduleMonthPojo);

    }
}
