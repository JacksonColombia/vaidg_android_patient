package com.vaidg.providerdetails;


import static com.vaidg.utilities.Constants.DATA;
import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;

import android.util.Log;
import android.widget.TextView;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.ReadMoreSpannable;
import com.vaidg.utilities.SessionManagerImpl;
import com.google.gson.Gson;
import com.pojo.ErrorHandel;
import com.pojo.ProviderDetailsResponse;
import com.pojo.ReviewPojo;
import com.utility.RefreshToken;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;


/**
 * <h2>ProviderPresenterImpl</h2>
 * Created by Ali on 2/5/2018.
 */

public class ProviderPresenterImpl implements ProviderDetailsContract.ProviderPresenter {

  @Inject
  ProviderDetailsContract.ProviderView view;

  @Inject
  SessionManagerImpl manager;
  @Inject
  Gson gson;

  @Inject
  LSPServices lspServices;


  @Inject
  public ProviderPresenterImpl() {

  }

  @Override
  public void onProviderDetailService(final String proId) {
      double lat = 0, lng = 0;
    if (!manager.getLatitude().equals("") && !manager.getLongitude().equals("")) {
      lat = Double.parseDouble(manager.getLatitude());
      lng = Double.parseDouble(manager.getLongitude());
    }

    Log.d("TAG", "onProviderDetailService: " + Constants.catId);
    Observable<Response<ResponseBody>> observable = lspServices.getProviderDetails(LSPApplication.getInstance().getAuthToken(manager.getSID())
        , Constants.selLang, PLATFORM_ANDROID, proId, Constants.catId, lat, lng,manager.getIpAddress(),
        Constants.callTypeInOutTele);
    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
            Log.d("TAG", "onNextonSubscribe: ");
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            int responseCode = responseBodyResponse.code();
            JSONObject jsonObject;
            try {
              String responseBody =
                  responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
              String errorBody =
                  responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null;
              Log.d("TAG", "onNextProviderDetails: " + responseBodyResponse.code());
              switch (responseCode) {
                case Constants.SUCCESS_RESPONSE:
                  if (responseBody != null && !responseBody.isEmpty()) {
                    ProviderDetailsResponse providerDetailsResponse = gson.fromJson(responseBody,
                        ProviderDetailsResponse.class);
                    if(view != null) {
                      view.onSuccess(providerDetailsResponse.getData());
                    }
                  }
                  if(view != null) {
                    view.onHideProgress();
                  }
                  break;
                case Constants.SESSION_EXPIRED:
                  if(errorBody != null && !errorBody.isEmpty()) {
                    ErrorHandel errorHandel = gson.fromJson(responseBody, ErrorHandel.class);
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(),
                        lspServices,
                        new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {
                            if(view != null) {
                              view.onHideProgress();
                            }
                              LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                            onProviderDetailService(proId);

                          }

                          @Override
                          public void onFailureRefreshToken() {

                          }

                          @Override
                          public void sessionExpired(String msg) {
                            if(view != null) {
                              view.onHideProgress();
                              view.onLogout(msg, manager);
                            }
                          }
                        });
                  }
                  break;
                case Constants.SESSION_LOGOUT:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onLogout(jsonObject.getString(MESSAGE), manager);
                      }
                    }
                  }
                  if(view != null) {
                    view.onHideProgress();
                  }
                  break;
                default:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onError(jsonObject.getString(MESSAGE));
                      }
                    }
                  }
                  if(view != null) {
                    view.onHideProgress();
                  }
                  break;
              }


            } catch (IOException | JSONException e) {
              e.printStackTrace();
              if(view != null) {
                view.onHideProgress();
              }
            }

          }

          @Override
          public void onError(Throwable e) {
            Log.d("TAG", "onNextonError: " + e.getMessage());
            if(view != null) {
              view.onHideProgress();
              view.onErrorNotConnected(e.getMessage());
            }
          }

          @Override
          public void onComplete() {
            Log.d("TAG", "onNextonComplete: ");
          }
        });
  }

  @Override
  public void moreReadable(TextView tvProAbout) {
    String readMore = "read more";
    String readLess = "read less";


    new ReadMoreSpannable(readMore, readLess);
    if (tvProAbout.getText().toString().length() > 100) {
      ReadMoreSpannable.makeTextViewResizable(tvProAbout, 3, readMore, true);
    }

  }

  @Override
  public void callReviewApi(int pageCount, String proId) {

    Observable<Response<ResponseBody>> observable = lspServices.getReviews(LSPApplication.getInstance().getAuthToken(manager.getSID()),
        Constants.selLang, PLATFORM_ANDROID, proId, Constants.catId, pageCount);

    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }
          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            int code = responseBodyResponse.code();
            JSONObject jsonObject;
            try {
              String responseBody =
                  responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
              String errorBody =
                  responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null;
              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                  if (responseBody != null && !responseBody.isEmpty()) {
                    ReviewPojo reviewPojo = gson.fromJson(responseBody, ReviewPojo.class);
                    if(view != null) {
                      view.onReviewSuccess(reviewPojo.getData());
                    }
                  }
                  if(view != null) {
                    view.onHideProgress();
                  }
                  break;

                case Constants.SESSION_LOGOUT:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                      jsonObject.getString(MESSAGE);
                      if (!jsonObject.getString(MESSAGE).isEmpty()) {
                        if(view != null) {
                          view.onLogout(jsonObject.getString(MESSAGE), manager);
                        }
                    }
                  }
                  if(view != null) {
                    view.onHideProgress();
                  }
                  break;
                case Constants.SESSION_EXPIRED:
                  if(errorBody != null && !errorBody.isEmpty()) {
                    ErrorHandel errorHandel = gson.fromJson(responseBody, ErrorHandel.class);
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(),
                        lspServices,
                        new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {
                            if(view != null) {
                              view.onHideProgress();
                            }
                              LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                            callReviewApi(pageCount, proId);

                          }

                          @Override
                          public void onFailureRefreshToken() {

                          }

                          @Override
                          public void sessionExpired(String msg) {
                            if(view != null) {
                              view.onHideProgress();
                              view.onLogout(msg, manager);
                            }
                          }
                        });
                  }
                  break;
                default:
                   if (errorBody != null && !errorBody.isEmpty()) {
                  jsonObject = new JSONObject(errorBody);
                       jsonObject.getString(MESSAGE);
                       if (!jsonObject.getString(MESSAGE).isEmpty()) {
                         if(view != null) {
                           view.onError(jsonObject.getString(MESSAGE));
                         }
                  }
                }
                  if(view != null) {
                    view.onHideProgress();
                  }
                  break;
              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
              if(view != null) {
                view.onHideProgress();
              }
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.onHideProgress();
            }
          }

          @Override
          public void onComplete() {

          }
        });
  }

  @Override
  public void hireProvider(long bid, String proId) {

    Map<String, Object> jsonParams = new HashMap<>();
    jsonParams.put("bookingId", bid);
    jsonParams.put("providerId", proId);

    Observable<Response<ResponseBody>> observable = lspServices.onBookingHire(LSPApplication.getInstance().getAuthToken(manager.getSID()),
        Constants.selLang, PLATFORM_ANDROID, jsonParams);

    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            int code = responseBodyResponse.code();
            JSONObject jsonObject;
            try {
              String responseBody =
                  responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
              String errorBody =
                  responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null;
              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                  if (responseBody != null && !responseBody.isEmpty()) {
                    if(view != null) {
                      view.onBookingHired();
                    }
                  }
                  if(view != null) {
                    view.onHideProgress();
                  }
                  break;

                case Constants.SESSION_LOGOUT:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                      jsonObject.getString(MESSAGE);
                      if (!jsonObject.getString(MESSAGE).isEmpty()) {
                        if(view != null) {
                          view.onLogout(jsonObject.getString(MESSAGE), manager);
                        }
                    }
                  }
                  if(view != null) {
                    view.onHideProgress();
                  }
                  break;
                case Constants.SESSION_EXPIRED:
                  if(errorBody != null && !errorBody.isEmpty()){
                    ErrorHandel errorHandel = gson.fromJson(responseBody, ErrorHandel.class);
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),manager.getREFRESHAUTH(), lspServices,
                        new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {

                              LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                            hireProvider(bid, proId);

                          }

                          @Override
                          public void onFailureRefreshToken() {

                          }

                          @Override
                          public void sessionExpired(String msg) {
                            if(view != null) {
                              view.onLogout(msg, manager);
                            }
                          }
                        });}
                  if(view != null) {
                    view.onHideProgress();
                  }
                  break;
                default:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                      jsonObject.getString(MESSAGE);
                      if (!jsonObject.getString(MESSAGE).isEmpty()) {
                        if(view != null) {
                          view.onError(jsonObject.getString(MESSAGE));
                        }
                    }
                  }
                  break;
              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
              if(view != null) {
                view.onHideProgress();
              }
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.onHideProgress();
            }
          }

          @Override
          public void onComplete() {

          }
        });
  }

  @Override
  public void attachView(Object view) {
    //view = (ProviderDetailsContract.ProviderView) view;
  }

  @Override
  public void detachView() {
    //  view = null;
  }
}
