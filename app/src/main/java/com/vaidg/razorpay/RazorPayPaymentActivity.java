package com.vaidg.razorpay;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import com.vaidg.R;
import com.vaidg.jobDetailsStatus.JobDetailsActivity;
import com.vaidg.jobDetailsStatus.JobDetailsOnTheWayContract;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import okhttp3.logging.HttpLoggingInterceptor;



/**
 * <h>RazorPayPaymentActivity</h>
 * <p> this activity for showing the razorpay payment gateway.</p>
 *  @version : 1.0
 * @company : 3Embed Software Technologies Pvt. Ltd.
 * */
public class RazorPayPaymentActivity extends DaggerAppCompatActivity implements PaymentResultListener {
    private String id = "", userId = "", name = "", email = "", contact = "", currency = "", razorPayKey = "", image = "",recieverName = "";
    private int amount = 0;
    private long bookingId = 0;
    private ProgressBar progressBar;
    @Inject
    JobDetailsOnTheWayContract.CancelBooking cancelBooking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Checkout.preload(getApplicationContext());
        init();
    }

    /**
     * <h>init</h>
     * intialize
     */
    private void init() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        if (getIntent().hasExtra("email")) {
            email = getIntent().getStringExtra("email");
        }
        if (getIntent().hasExtra("bookingId")) {
            bookingId = getIntent().getLongExtra("bookingId",0);
        }
        if (getIntent().hasExtra("image")) {
            image = getIntent().getStringExtra("image");
        }
        if (getIntent().hasExtra("paymentAction")) {
            id = getIntent().getStringExtra("paymentAction");
        }
        if (getIntent().hasExtra("contact")) {
            contact = getIntent().getStringExtra("contact");
        }
        if (getIntent().hasExtra("razorPayKey")) {
            razorPayKey = getIntent().getStringExtra("razorPayKey");
        }
        if (getIntent().hasExtra("name")) {
            name = getIntent().getStringExtra("name");
        }
        if (getIntent().hasExtra("currency")) {
            currency = getIntent().getStringExtra("currency");
        }
        if (getIntent().hasExtra("amount")) {
            amount = getIntent().getIntExtra("amount", 0);
        }
        if (getIntent().hasExtra("userId")) {
            userId = getIntent().getStringExtra("userId");
        }
        if (getIntent().hasExtra("recieverName")) {
            recieverName = getIntent().getStringExtra("recieverName");
        }
        startPayment();

    }

    /**
     * <h>startPayment</h>
     * Pass your payment options to the Razorpay Checkout as a JSONObject
     */
    public void startPayment() {
        final Activity activity = this;
        Checkout checkout = new Checkout();
        checkout.setKeyID(razorPayKey);
        checkout.setImage(R.mipmap.ic_launcher);
        try {
            JSONObject options = new JSONObject();
            JSONObject notes = new JSONObject();
            JSONObject a = new JSONObject();
            JSONObject b = new JSONObject();
            JSONObject c = new JSONObject();
            JSONObject d = new JSONObject();

            options.put("name", name);
            options.put("description", getResources().getString(R.string.bookedFor).concat(" ").concat(recieverName));
            options.put("image",image);
            options.put("theme.color", "#1A54B0");
            notes.put("currency", currency);
            notes.put("amount", amount);//pass amount in currency subunits
            options.put("amount", amount);//pass amount in currency subunits
            options.put("prefill.email", email);
            options.put("prefill.contact", contact);
            notes.put("paymentAction", id);
            notes.put("userType","1");
            notes.put("userId",userId);
            options.put("notes",notes);
            d.put("netbanking","1");
            d.put("card","1");
            d.put("wallet","0");
            d.put("upi","0");
            c.put("method",d);
            b.put("checkout",c);
            options.put("options",b);
            checkout.open(activity, options);
            Log.d("PAYMENTS", options.toString());
        } catch (Exception e) {
            Log.e("PAYMENT", e.getLocalizedMessage());
        }
    }

    @Override
    public void onPaymentSuccess(String id) {
        setResult(RESULT_OK,new Intent().putExtra("CARDID",id));
        finish();
    }


    @Override
    public void onPaymentError(int i, String s) {
        cancelBooking.onToCancelBooking(bookingId, RazorPayPaymentActivity.this, "",true);
/*
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.UpdateAlertDialog)
                .setTitle(R.string.systemError)
                .setMessage(s)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        setResult(RESULT_CANCELED,new Intent().putExtra("CARDID",bookingId));
                        finish();
                    }
                });
        builder.show();
*/
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        cancelBooking.onToCancelBooking(bookingId, RazorPayPaymentActivity.this, "",true);
    }
}
