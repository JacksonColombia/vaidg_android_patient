package com.vaidg.razorpay;

import com.vaidg.Dagger2.ActivityScoped;
import com.vaidg.Dagger2.FragmentScoped;
import com.vaidg.jobDetailsStatus.CancelBookingImpl;
import com.vaidg.jobDetailsStatus.JobDetailsActivity;
import com.vaidg.jobDetailsStatus.JobDetailsContract;
import com.vaidg.jobDetailsStatus.JobDetailsContractImpl;
import com.vaidg.jobDetailsStatus.JobDetailsOnTheWayContract;
import com.vaidg.jobDetailsStatus.JobDetailsOnTheWayContractImpl;
import com.vaidg.jobDetailsStatus.JobPostedFrag;
import com.vaidg.jobDetailsStatus.JobStartedFrag;
import com.vaidg.jobDetailsStatus.ProviderArrivedFrag;
import com.vaidg.jobDetailsStatus.ProviderHiredFragment;
import com.vaidg.jobDetailsStatus.ProviderHiredFragmentIn;
import com.vaidg.jobDetailsStatus.ProviderOnTheWayFrag;
import com.vaidg.lspapplication.MessagesFragment;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * <h2>JobDetailsModules</h2>
 * Created by Ali on 2/15/2018.
 */
@Module
public interface RazorPaymentModules
{

    @Binds
    @ActivityScoped
    JobDetailsOnTheWayContract.CancelBooking provideCancelBooking(CancelBookingImpl cancelBooking);
}
