package com.vaidg.wallet;



import android.util.Log;

import com.vaidg.utilities.LSPApplication;
import com.google.gson.Gson;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;
import com.pojo.ErrorHandel;
import com.utility.RefreshToken;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;


public class WalletActivityPresenter implements WalletActivityContract.WalletPresenter
{
    private final String TAG = "WalletDetailsProvider";

    @Inject WalletActivityContract.WalletView view;
    @Inject
    SessionManagerImpl preferenceHelperDataSource;
    @Inject LSPServices networkService;
    @Inject
    Gson gson;

    @Inject
    public WalletActivityPresenter() {
    }


    @Override
    public void getWalletLimits()
    {if(view != null) {
        view.showProgressDialog();
    }
        Observable<Response<ResponseBody>> request = networkService.getWalletLimits(
                LSPApplication.getInstance().getAuthToken(preferenceHelperDataSource.getSID()), Constants.selLang,Constants.PLATFORM_ANDROID);

        request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                      //  compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Response<ResponseBody> value) {
                        Log.d(TAG , " getWalletLimits onNext: " + value.code());
                        String responseString;
                        try {
                            switch (value.code()) {
                                case 200:

                                     responseString = value.body().string();
                                    Log.d(TAG , " getWalletLimits onNext: "+responseString);
                                    JSONObject profileObject = new JSONObject(responseString);
                                    JSONObject dataObject = profileObject.getJSONObject("data");
                                    String balance = dataObject.getString("walletAmount");
                                    String softLimit = dataObject.getString("softLimit");
                                    String hardLimit = dataObject.getString("hardLimit");
                                    String currencySymbol=dataObject.getString("currencySymbol");
                                    Constants.walletAmount = Double.parseDouble(balance);
                                    Constants.walletCurrency = currencySymbol;
                                    if(view != null) {
                                        view.setBalanceValues(currencySymbol + " " + balance, currencySymbol + " " + hardLimit
                                            , currencySymbol + " " + softLimit);
                                    }

                                    break;
                                case 410:
                                    break;
                                case Constants.SESSION_LOGOUT:
                                    responseString =value.errorBody().string();
                                    JSONObject jsonObject = new JSONObject(responseString);
                                    if(view != null) {
                                        view.onLogout(jsonObject.getString("message"));
                                    }
                                    break;

                                case Constants.SESSION_EXPIRED:
                                    responseString = value.errorBody().string();
                                    ErrorHandel errorHandel = gson.fromJson(responseString, ErrorHandel.class);
                                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(preferenceHelperDataSource.getSID()),preferenceHelperDataSource.getREFRESHAUTH(),networkService, new RefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {

                                            LSPApplication.getInstance().setAuthToken(preferenceHelperDataSource.getSID(),preferenceHelperDataSource.getSID(),newToken);
                                            getWalletLimits();
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {

                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if(view != null) {
                                                view.onLogout(msg);
                                            }
                                        }
                                    });
                                    break;

                                default:
                                    String error = value.errorBody().string();
                                    if(view != null) {
                                        view.walletDetailsApiErrorViewNotifier(new JSONObject(error).getString("message"));
                                    }
                                    break;
                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                            if(view != null) {
                                view.hideProgressDialog();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG , "getWalletLimits error: " + e.getMessage());
                        if(view != null) {
                            view.hideProgressDialog();
                        }
                    }

                    @Override
                    public void onComplete() {
                        if(view != null) {
                            view.hideProgressDialog();
                        }
                    }
                });

    }

    @Override
    public void rechargeWallet(String amount, String cardId)
    {
        Log.d(TAG, "rechargeWallet: "+LSPApplication.getInstance().getAuthToken(preferenceHelperDataSource.getSID())
        +" cardId "+cardId +" amount  "+amount);
        if(view != null) {
            view.showProgressDialog();
        }
        Map<String, Object> jsonParams = new HashMap<>();
        jsonParams.put("cardId", cardId);
        jsonParams.put("amount", amount);

        Observable<Response<ResponseBody>> request = networkService.rechargeWallet(
                LSPApplication.getInstance().getAuthToken(preferenceHelperDataSource.getSID()),
                Constants.selLang,Constants.PLATFORM_ANDROID,jsonParams);   //card_1CAvIg2876tVKl2MlPy06DBq

        request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                      //  compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Response<ResponseBody> value) {
                        Log.d(TAG , " RechargeWallet onNext: " + value.code());
                        try {
                            switch (value.code()) {
                                case 200:

                                    String responseString = value.body().string();
                                    Log.d(TAG , " RechargeWallet onNext: " + responseString);
                                    if(view != null) {
                                        view.walletRecharged(true, new JSONObject(responseString).getString("message"));
                                    }
                                    /*JSONObject profileObject = new JSONObject(responseString);
                                    JSONObject dataObject = profileObject.getJSONObject("data");
                                    String balance=dataObject.getString("walletAmount");
                                    String softLimit=dataObject.getString("softLimit");
                                    String hardLimit=dataObject.getString("hardLimit");
                                    String currencySymbol=dataObject.getString("currencySymbol");
                                    view.setBalanceValues(currencySymbol+" "+balance, currencySymbol+" "+hardLimit
                                            , currencySymbol+" "+softLimit);*/

                                    break;
                                case Constants.SESSION_LOGOUT:
                                    responseString =value.errorBody().string();
                                    JSONObject jsonObject = new JSONObject(responseString);
                                    if(view != null) {
                                        view.onLogout(jsonObject.getString("message"));
                                    }
                                    break;

                                case Constants.SESSION_EXPIRED:
                                    responseString = value.errorBody().string();
                                    ErrorHandel errorHandel = gson.fromJson(responseString, ErrorHandel.class);
                                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(preferenceHelperDataSource.getSID()),preferenceHelperDataSource.getREFRESHAUTH(),networkService, new RefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {

                                            LSPApplication.getInstance().setAuthToken(preferenceHelperDataSource.getSID(),preferenceHelperDataSource.getSID(),newToken);
                                            rechargeWallet(amount, cardId);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {

                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if(view != null) {
                                                view.onLogout(msg);
                                            }
                                        }
                                    });
                                    break;
                                default:
                                    String responseError = value.errorBody().string();
                                    if(view != null) {
                                        view.walletDetailsApiErrorViewNotifier(new JSONObject(responseError).getString("message"));
                                    }
                                    break;
                            } } catch (IOException | JSONException e) {
                            e.printStackTrace();
                            if(view != null) {
                                view.hideProgressDialog();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG , "RechargeWallet error: " + e.getMessage());
                        if(view != null) {
                            view.hideProgressDialog();
                        }

                    }

                    @Override
                    public void onComplete() {
                        if(view != null) {
                            view.hideProgressDialog();
                        }

                    }
                });

    }

    @Override
    public void getLastCardNo()
    {
       /* if(preferenceHelperDataSource.getDefaultCardDetails()!=null) {
            Utility.printLog("MyCardNumber"+preferenceHelperDataSource.getDefaultCardDetails().getLast4());
            view.setCard(preferenceHelperDataSource.getDefaultCardDetails().getLast4(),
                    preferenceHelperDataSource.getDefaultCardDetails().getBrand());
        }
        else
            view.setNoCard();*/
    }

    @Override
    public String getCurrencySymbol() {
        return null;
    }
}
