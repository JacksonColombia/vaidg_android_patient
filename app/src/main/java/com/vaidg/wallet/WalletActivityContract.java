package com.vaidg.wallet;


import com.vaidg.home.BasePresenter;

public interface WalletActivityContract
{
     interface WalletView
    {
        /**
         * <h2>Api Error</h2>
         * <p>this method is using to display Api call Error</p>
         * @param error
         */
        void walletDetailsApiErrorViewNotifier(String error);

        /**
         * <h2>showToastNotifier</h2>
         * <p> method to trigger activity/fragment show progress dialog interfac </p>
         */
        void showProgressDialog();

        /**
         * <h2>showToastNotifier</h2>
         * <p> method to trigger activity/fragment showToast interface to show test </p>
         * @param msg: message to be shown in toast
         * @param duration: toast duration
         */
        void showToast(String msg, int duration);

        /**
         * <h2>showAlertNotifier</h2>
         * <p> method to trigger activity/fragment showAlertNotifier interface to show alert </p>
         * @param title: alert title to be setList
         * @param msg: alert message to be displayed
         */
        void showAlert(String title, String msg);


        /**
         * <h2>noInternetAlert</h2>
         * <p> method to trigger activity/fragment alert interface to show alert that there isnot internet connectivity </p>
         */
        void noInternetAlert();

        /**
         * <h2>Hide Progress bar</h2>
         * <p>This method is using to hide the progress bar</p>
         */
        void hideProgressDialog();

        /**
         * <h2>Set Wallet Balance</h2>
         * <p>Set the updated Wallet balance</p>
         * @param balance new balance
         * @param hardLimit new hard limit
         * @param softLimit new soft limit
         */
        void setBalanceValues(String balance, String hardLimit, String softLimit);

        /**
         * <h2>Show recharge Confirmation</h2>
         * <p>This method is using to  display Confirmation message to User</p>
         * @param amount amount to display
         */
        void showRechargeConfirmationAlert(String amount);

        /**
         * <h2>Set Card type View</h2>
         * <p>this method is using to set card details view</p>
         * @param cardNum card last 4 digits
         * @param cardType card brand
         */
        void setCard(String cardNum, String cardType);

        /**
         * <h2>Set no Card View</h2>
         * <p>this method is using to set no card view</p>
         */
        void setNoCard();

        void walletRecharged(boolean recharged, String message);

        void onLogout(String msg);

        void onError(String message);
    }

    interface WalletPresenter
    {
        /**
         * <h2>Recharge Wallet</h2>
         * <p>this method is uisng to recharge the Wallet</p>
         * @param amount amount to be rechrged
         * @param cardId
         */
        void rechargeWallet(String amount, String cardId);

        /**
         * <h2>get Wallet limits</h2>
         * <p>this method is using to get Wallet limit</p>
         */
        void getWalletLimits();

        /**
         * <h2>Get Crad number</h2>
         * <p>this method is using to get Card Last four digits</p>
         */
        void getLastCardNo();

        /**
         * <h2>get Currency Symbol</h2>
         * <p>this method is using to get the Currency Symbol</p>
         * @return currency  symbol
         */
        String getCurrencySymbol();

    }

    interface WalletPresenterBalance extends BasePresenter
    {
        /**
         * <h2>get Wallet limits</h2>
         * <p>this method is using to get Wallet limit</p>
         */
        void getWalletLimits();

    }
}

