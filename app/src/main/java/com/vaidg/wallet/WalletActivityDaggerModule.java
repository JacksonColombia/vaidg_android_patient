package com.vaidg.wallet;



import com.vaidg.Dagger2.ActivityScoped;

import dagger.Binds;
import dagger.Module;

@Module
public interface  WalletActivityDaggerModule
{
    @Binds
    @ActivityScoped
    WalletActivityContract.WalletView provideWalletView(WalletActivity activity);

    @Binds
    @ActivityScoped
     WalletActivityContract.WalletPresenter provideWalletPresenter(WalletActivityPresenter presenter);

}
