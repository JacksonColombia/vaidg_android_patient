package com.vaidg.profile;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.bumptech.glide.Glide;
import com.vaidg.R;
import com.vaidg.change_email.ChangeEmailActivity;
import com.vaidg.changepassword.ChangePwdActivity;
import com.vaidg.countrypic.CountryPicker;
import com.vaidg.signup.gender.GenderActivity;
import com.vaidg.signup.gender.model.GenderListPojo;
import com.vaidg.utilities.AppPermissionsRunTime;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.HandlePictureEvents;
import com.vaidg.utilities.ImageUploadedAmazon;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.google.android.material.textfield.TextInputLayout;
import com.utility.AlertProgress;
import com.utility.DialogInterfaceListner;
import com.utility.PicassoTrustAll;

import dagger.android.support.DaggerAppCompatActivity;
import eu.janmuller.android.simplecropimage.CropImage;
import eu.janmuller.android.simplecropimage.Util;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Locale;
import javax.inject.Inject;

import static com.vaidg.utilities.Utility.gender;
import static com.vaidg.utilities.Utility.setMAnagerWithBID;

public class ProfileActivity extends DaggerAppCompatActivity implements ProfileView {

  public static final int REQUEST_CAMERA_PERMISSION = 1234;
  @BindView(R.id.iv_prof_img)
  ImageView ivProfilePic;
  @BindView(R.id.etProflNme)
  EditText tieFirstName;
  @BindView(R.id.etProflLNme)
  EditText tieLastName;
  @BindView(R.id.tvProflEml)
  TextView tvProflEml;
  @BindView(R.id.tvProflMob)
  TextView tvProflMob;
  @BindView(R.id.etProfAboutMe)
  EditText tieAboutMe;
  /*
    @BindView(R.id.ivProEditForwrdEml)
    ImageView ivProEditForwrdEml;*/
  @BindView(R.id.ivProEditForwrdMob)
  ImageView ivProEditForwrdMob;
  @BindView(R.id.countryCode_divider)
  View countryCode_divider;
  @BindView(R.id.viewEml)
  View viewEml;
  @BindView(R.id.viewNm)
  View viewFnm;
  @BindView(R.id.viewLNm)
  View viewLnm;
  @BindView(R.id.viewAbtMe)
  View viewAbtMe;
  @BindView(R.id.viewMob)
  View viewMob;
  @BindView(R.id.countryFlag)
  ImageView countryFlag;
  @BindView(R.id.countryCode)
  TextView countryCode;
  //@Inject
  HandlePictureEvents handlePicEvent;
  @BindString(R.string.save)
  String save_text;
  @BindString(R.string.edit)
  String edit_text;
  @BindString(R.string.aboutMeHint)
  String about_me_hint_text;
  @BindColor(R.color.greyBackgroundVdarker)
  int greyColor;
  @BindView(R.id.btnChangepasswd)
  TextView btnChangePwd;
  @BindView(R.id.btnLogout)
  TextView btnLogout;
  @BindView(R.id.etDepDob)
  EditText et_dep_dob;
  @BindView(R.id.etRegGender)
  EditText etRegGender;
  @BindView(R.id.til_dep_dob)
  TextInputLayout til_dep_dob;
  @BindView(R.id.tilRegGender)
  TextInputLayout tilRegGender;
  @BindView(R.id.toolbar)
  Toolbar toolbarLayout;
  @BindView(R.id.tv_center)
  TextView tvYourProf;
  @BindView(R.id.tv_skip)
  TextView tv_tb_rightbtn;
  AlertDialog alertDialog;
  AlertDialog.Builder dialogBuilder;
  @Inject
  ProfilePresenter presenter;
  @Inject
  AlertProgress alertProgress;
  @Inject
  AppTypeface appTypeface;
  @Inject
  CountryPicker mCountryPicker;
  @Inject
  SessionManagerImpl manager;
  String profilePicUrl = "";
  private String auth;
  private boolean imageflag = false;
  private boolean isFromGender = false;
  private boolean isLogOut = false;
  private static final int REQUEST_GENDER = 55;
  private ArrayList<GenderListPojo> mGenderArrayList = new ArrayList<>();
  private Context mContext;
  private ProgressDialog pDialog;

  public static int getResId(String drawableName) {
    try {
      Class res = R.drawable.class;
      Field field = res.getField(drawableName);
      int drawableId = field.getInt(null);
      System.out.println("resource ids****" + drawableId);
      return drawableId;
    } catch (Exception e) {
      Log.e("MyTag", "Failure to get drawable id.", e);
      return 0;
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_profile);
    ButterKnife.bind(this);
    mContext = this;
    initialize();

  }

  private void initialize() {

    tvYourProf.setText(R.string.yourProfile);
    tvYourProf.setTypeface(appTypeface.getHind_semiBold());
    et_dep_dob.setTypeface(appTypeface.getHind_regular());
    etRegGender.setTypeface(appTypeface.getHind_regular());
    til_dep_dob.setTypeface(appTypeface.getHind_regular());
    tilRegGender.setTypeface(appTypeface.getHind_regular());

    handlePicEvent = new HandlePictureEvents(this);

    tv_tb_rightbtn.setVisibility(View.VISIBLE);
    tv_tb_rightbtn.setText(R.string.edit);
    tv_tb_rightbtn.setTypeface(appTypeface.getHind_medium());
    // editMethod();
    saveMethod();

    toolbarLayout.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
    toolbarLayout.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        String str_btn_text = tv_tb_rightbtn.getText().toString();
        if (getString(R.string.save).equals(str_btn_text)) {
          saveMethod();
          tv_tb_rightbtn.setText(R.string.edit);
        } else {
          onBackPressed();
        }
      }
    });

    auth = LSPApplication.getInstance().getAuthToken(manager.getSID());

    typeFae();
  }

  private void typeFae() {
    btnChangePwd.setTypeface(appTypeface.getHind_medium());
    btnLogout.setTypeface(appTypeface.getHind_medium());
  }

  @Override
  protected void onResume() {
    super.onResume();

    if (manager.isProfileCalled()) {
      setProfileFirstName(manager.getFirstName());
      setDateOfBirth(manager.getPatientDateOfBirth());
      setProfileLastName(manager.getLastName());
      setAbout(manager.getAbout());
      setProfileEmail(manager.getEmail());
      setProfilePic(manager.getProfilePicUrl());
      setProfileMob(manager.getMobileNo());
      setCountryCode(manager.getCountryCode());
      if(!isFromGender)
      setGender(Utility.gender(manager.getPatientGender()));
    } else {
      if (alertProgress.isNetworkAvailable(this))
        presenter.getProfile(auth);
      else
        alertProgress.showNetworkAlert(this);
    }
  }

  private void editMethod() {

    tv_tb_rightbtn.setText(getString(R.string.save));

    tieFirstName.clearFocus();
    tieLastName.clearFocus();
    tieAboutMe.clearFocus();
    tvProflMob.clearFocus();
    et_dep_dob.setClickable(true);
    tieFirstName.setFocusable(true);
    tieFirstName.setFocusableInTouchMode(true);
    tieLastName.setFocusable(true);
    tieLastName.setFocusableInTouchMode(true);
    tieLastName.setClickable(true);
    tieAboutMe.setFocusable(true);
    tieAboutMe.setFocusableInTouchMode(true);
    tieAboutMe.setClickable(true);
    ivProfilePic.setClickable(true);

    viewFnm.setVisibility(View.VISIBLE);
    viewLnm.setVisibility(View.VISIBLE);
    viewAbtMe.setVisibility(View.VISIBLE);

    viewEml.setVisibility(View.VISIBLE);
    viewMob.setVisibility(View.VISIBLE);

    countryCode_divider.setVisibility(View.VISIBLE);

    tvProflEml.setClickable(true);
    tvProflMob.setClickable(true);

    //  ivProEditForwrdEml.setVisibility(View.VISIBLE);
    ivProEditForwrdMob.setVisibility(View.VISIBLE);

    btnChangePwd.setVisibility(View.GONE);
    btnLogout.setVisibility(View.GONE);


    et_dep_dob.setOnClickListener(v -> {
      presenter.openCalendar(ProfileActivity.this);

      InputMethodManager imm = (InputMethodManager) getSystemService(
              Context.INPUT_METHOD_SERVICE);
      if (imm != null) {
        imm.hideSoftInputFromWindow(et_dep_dob.getWindowToken(), 0);
      }
    });
    etRegGender.setOnClickListener(v -> {
      Intent intent = new Intent(this, GenderActivity.class);
      intent.putExtra("mGenderArrayList",mGenderArrayList);
      startActivityForResult(intent,REQUEST_GENDER);

      InputMethodManager imm = (InputMethodManager) getSystemService(
              Context.INPUT_METHOD_SERVICE);
      if (imm != null) {
        imm.hideSoftInputFromWindow(etRegGender.getWindowToken(), 0);
      }
    });
  }

  private void saveMethod() {
    View view = this.getCurrentFocus();
    if (view != null) {
      InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
      if (imm != null) {
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
      }
    }

    tv_tb_rightbtn.setText(getString(R.string.edit));

    viewFnm.setVisibility(View.INVISIBLE);
    viewLnm.setVisibility(View.INVISIBLE);
    viewAbtMe.setVisibility(View.INVISIBLE);

    tieFirstName.setFocusable(false);
    et_dep_dob.setClickable(false);
    etRegGender.setClickable(false);
    tieFirstName.setFocusableInTouchMode(false);
    tieLastName.setFocusable(false);
    tieFirstName.setClickable(false);
    tieLastName.setFocusableInTouchMode(false);
    tieLastName.setClickable(false);
    tieAboutMe.setFocusable(false);
    tieAboutMe.setFocusableInTouchMode(false);
    et_dep_dob.setFocusable(false);
    etRegGender.setFocusable(false);
    et_dep_dob.setFocusableInTouchMode(false);
    etRegGender.setFocusableInTouchMode(false);
    tieAboutMe.setClickable(false);
    ivProfilePic.setClickable(false);

    tieFirstName.setBackgroundColor(Color.TRANSPARENT);
    tieLastName.setBackgroundColor(Color.TRANSPARENT);

    viewEml.setVisibility(View.INVISIBLE);
    viewMob.setVisibility(View.INVISIBLE);

    countryCode_divider.setVisibility(View.INVISIBLE);

    tvProflEml.setClickable(false);
    tvProflMob.setClickable(false);



    //   ivProEditForwrdEml.setVisibility(View.GONE);
    ivProEditForwrdMob.setVisibility(View.GONE);

    btnChangePwd.setVisibility(View.VISIBLE);
    btnLogout.setVisibility(View.VISIBLE);

  }

  @OnClick(R.id.tvProflEml)
  public void setTvProflEml() {
    editEmailPhone(true);
  }

  @OnClick(R.id.tvProflMob)
  public void setTvProflMob() {
    editEmailPhone(false);
  }

  @OnClick(R.id.iv_prof_img)
  public void profilePicClick() {
    checkCameraPermission();
  }

  private void checkCameraPermission() {
    if (Build.VERSION.SDK_INT >= 23) {
      // Marshmallow+
      ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList =
              new ArrayList<>();
      myPermissionConstantsArrayList.clear();
      myPermissionConstantsArrayList.add(
              AppPermissionsRunTime.MyPermissionConstants.PERMISSION_CAMERA);
      myPermissionConstantsArrayList.add(
              AppPermissionsRunTime.MyPermissionConstants.PERMISSION_WRITE_EXTERNAL_STORAGE);
      myPermissionConstantsArrayList.add(
              AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_EXTERNAL_STORAGE);
      if (AppPermissionsRunTime.checkPermission(this, myPermissionConstantsArrayList,
              REQUEST_CAMERA_PERMISSION)) {

        selectImage();
      }
    } else {
      selectImage();
    }
  }


  @OnClick(R.id.tv_skip)
  public void editProfile() {
    isLogOut = false;
    String str_btn_text = tv_tb_rightbtn.getText().toString();

    if (getString(R.string.save).equals(str_btn_text)) {

      if (!"".equals(tieFirstName.getText().toString())) {
        if (alertProgress.isNetworkAvailable(this)) {
          if (!imageflag) {
            presenter.editProfile(auth, profilePicUrl, tieFirstName.getText().toString(), tieLastName.getText().toString(), tieAboutMe.getText().toString(), etRegGender.getText().toString(),et_dep_dob.getText().toString());
          } else {
            presenter.editProfile(auth, profilePicUrl, tieFirstName.getText().toString(), tieLastName.getText().toString(), tieAboutMe.getText().toString(), etRegGender.getText().toString(), et_dep_dob.getText().toString());
          }
          saveMethod();

        } else
          alertProgress.showNetworkAlert(this);

      } else {
        alertProgress.alertinfo(this, "First Name should not be empty");
      }


    } else {
      editMethod();
    }
  }

  @OnClick(R.id.btnChangepasswd)
  void changePassword() {
    Intent intent = new Intent(this, ChangePwdActivity.class);
    intent.putExtra("auth", auth);
    intent.putExtra("coming_from", "profile");
    startActivity(intent);
    finish();
  }

  @OnClick(R.id.btnLogout)
  void logout() {
    isLogOut = true;
    if(Utility.isNetworkAvailable(this))
    presenter.doLogout(auth);
  }

  private void editEmailPhone(boolean isEmail) {
    Intent intent = new Intent(ProfileActivity.this, ChangeEmailActivity.class);
    intent.putExtra("IS_EMAIL", isEmail);
    startActivity(intent);
  }

  @Override
  public void onSessionExpired() {
  }

  @Override
  public void onLogout(String msg, SessionManagerImpl sessionManager) {
    if (alertProgress != null && !TextUtils.isEmpty(msg)) {
      alertProgress.alertPositiveOnclick(this, msg, mContext.getResources().getString(R.string.logout), mContext.getResources().getString(R.string.ok),
              isClicked -> setMAnagerWithBID(mContext, sessionManager));
    }else{
      setMAnagerWithBID(mContext, sessionManager);
    }
  }

  @Override
  public void onError(String msg) {
    if (alertProgress != null) {
      alertProgress.alertinfo(mContext, msg);
    }
  }

  @Override
  public void onConnectionError(String connectionError) {
  }

  @Override
    public void setDateOfBirth(String dateOfBirth) {
        et_dep_dob.setText(dateOfBirth);
    }

    @Override
  public void setProfileEmail(String email) {
    tvProflEml.setText(email);
  }

  @Override
  public void setProfileFirstName(String name) {
    tieFirstName.setText(name);
  }

  @Override
  public void setProfileLastName(String lastName) {
    tieLastName.setText(lastName);
  }

  @Override
  public void setProfileMob(String mobile_no) {
    tvProflMob.setText(mobile_no);
  }

  @Override
  public void setAbout(String about) {
    tieAboutMe.setText(about);
  }

  @Override
  public void setGender(String gender) {
    etRegGender.setText(gender(gender));
  }

  @Override
  public void onShowProgress() {
    //progressBar.setVisibility(View.VISIBLE);
    pDialog = alertProgress.getProcessDialog(this);
    if(isLogOut)
    pDialog.setMessage(mContext.getResources().getString(R.string.wait_logout));
    else
      pDialog.setMessage(mContext.getResources().getString(R.string.wait_profile));
    pDialog.setCancelable(false);
    try {
      if (pDialog != null && !pDialog.isShowing() && !isFinishing()) {
        pDialog.show();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void onHideProgress() {
    if (pDialog != null && !isFinishing() && pDialog.isShowing()) {
      pDialog.dismiss();
    }
  }

  @Override
  public void onSuccess(String message) {
    Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void setProfilePic(String profilePicUrl) {
    if (!TextUtils.isEmpty(profilePicUrl)) {
      manager.setProfilePicUrl(profilePicUrl);
      PicassoTrustAll.getInstance(this)
              .load(profilePicUrl)
              .placeholder(R.drawable.ic_fever_placeholder)   // optional
              .error(R.drawable.ic_fever_placeholder)// optional
              .transform(new PicassoCircleTransform())
              .into(ivProfilePic);
    }
  }

  @Override
  public void setCountryCode(String country_code) {
   // countrycodeFlag(countryCode.toString());
    countryCode.setText(country_code);
    String drawableName = "flag_"
            + manager.getCountrySymbol().trim().toLowerCase(Locale.ENGLISH);
    countryFlag.setImageResource(getResId(drawableName));
  }

  private void countrycodeFlag(String country_code) {
    String[] arrContryCode = this.getResources().getStringArray(R.array.DialingCountryCode);
    for (String anArrContryCode : arrContryCode) {
      String[] arrDial = anArrContryCode.split(",");
      if (arrDial[0].equals(country_code)) {

        countryCode.setText(country_code);
        String drawableName = "flag_"
            + arrDial[1].trim().toLowerCase(Locale.ENGLISH);
        countryFlag.setImageResource(getResId(drawableName));
        break;
      }
    }
  }

  /**
   * <h2>selectImage</h2>
   *
   * @see HandlePictureEvents
   * This mehtod is used to show the popup where we can select our images.
   */
  private void selectImage() {
    handlePicEvent.openDialog();
  }

  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    boolean isDenine = false;
    switch (requestCode) {
      case REQUEST_CAMERA_PERMISSION:
        for (int grantResult : grantResults) {
          if (grantResult != PackageManager.PERMISSION_GRANTED) {
            isDenine = true;
          }
        }
        if (isDenine) {
          Toast.makeText(this, "Permission denied by the user", Toast.LENGTH_SHORT).show();
        } else {
          selectImage();
        }
        break;
    }
  }

  /**
   * This is an overrided method, got a call, when an activity opens by StartActivityForResult(), and return something back to its calling activity.
   *
   * @param requestCode returning the request code.
   * @param resultCode  returning the result code.
   * @param data        contains the actual data.
   */
  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode != RESULT_OK) {
      //result code to check is the result is ok or not
      return;
    }

    switch (requestCode) {
      case Constants.CAMERA_PIC:
        handlePicEvent.startCropImage(handlePicEvent.newFile);
        break;
      case Constants.GALLERY_PIC:
        if (data != null)
          handlePicEvent.gallery(data.getData());
        break;
      case Constants.CROP_IMAGE:
        String path = data.getStringExtra(CropImage.IMAGE_PATH);
        if (path != null) {
          try {
            File fileExist = new File(path);

            imageflag = true;
            handlePicEvent.uploadToAmazon(this, Constants.AmazonProfileFolderName, fileExist, new ImageUploadedAmazon() {
              @Override
              public void onSuccessAdded(String image) {
                Log.d("TAG", "onSuccessAdded: " + image);
                profilePicUrl = image;
                if(profilePicUrl != null && !profilePicUrl.isEmpty()) {
                  PicassoTrustAll.getInstance(ProfileActivity.this)
                      .load(profilePicUrl)
                      .placeholder(R.drawable.ic_fever_placeholder)   // optional
                      .error(R.drawable.ic_fever_placeholder)// optional
                      .transform(new PicassoCircleTransform())
                      .into(ivProfilePic);
                }
              }

              @Override
              public void onerror(String errormsg) {
                Log.d("TAG", "onerror: " + errormsg);
              }

            });

          } catch (Exception e) {
            e.printStackTrace();
          }

        }
        break;

      case REQUEST_GENDER:
        if (data != null) {
          isFromGender = true;
          mGenderArrayList = (ArrayList<GenderListPojo>) data.getSerializableExtra("mGenderArrayList");
          setGender(data.getStringExtra("GenderName"));
        }
    }
  }

}
