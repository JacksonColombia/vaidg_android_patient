package com.vaidg.profile;

import android.content.Context;
import com.vaidg.home.BasePresenter;

/**
 * @author Pramod
 * @since  12-01-2018.
 */

public interface ProfilePresenter extends BasePresenter {

    void getProfile(String auth);

    void editProfile(String auth, String profile_pic, String firstName, String lastName, String aboutMe, String gender_val, String dateOfBirth);

    void doLogout(String auth);

    /**
     * <h2>openDate_Picker</h2>
     * Method is used to open a date picker and Pick a date.
     * <p>
     * This method open a Date Picker dialog by DatePickerDialog object.
     * Hen set the Max time as Current Date by the help of the {@code fromDatePickerDialog
     * .getDatePicker().setMaxDate(new
     * Date().getTime())}.
     * Also listening the response From the DatePicker and setting the Date to the Given Edit text.
     * </p>
     * @param context
     */
    void openCalendar(
        Context context);

    /**
     * <h2>checkDobValidity</h2>
     * This method is used to check the validation of the date of birth field.
     */
    boolean checkDobValidity();

    /**
     * <h2>onHideProgress</h2>
     * <p>This method is used to hide the loading view</p>
     */
    void onHideProgress();

    /**
     * <h2>onShowProgress</h2>
     * <p>This method is used to show the loading view</p>
     */
    void onShowProgress();

    /**
     * <h2>onLogout</h2>
     * <p>This method is used to show msg if session is expired</p>
     */
    void onLogout(String errorBody);

    /**
     * <h2>onErrorMsg</h2>
     * <p>This method is used to show error msg from api calls</p>
     */
    void onErrorMsg(String errorBody);
}
