package com.vaidg.profile;

import static com.vaidg.utilities.Constants.INTERNAL_SERVER_ERROR;
import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.SESSION_EXPIRED;
import static com.vaidg.utilities.Constants.SESSION_LOGOUT;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;
import static com.vaidg.utilities.Constants.dateOB;
import static com.vaidg.utilities.Constants.selLang;
import static com.vaidg.utilities.LSPApplication.getInstance;

import android.app.DatePickerDialog;
import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.DatePicker;

import com.vaidg.R;
import com.vaidg.faq.model.FaqResponse;
import com.vaidg.model.ProfileData;
import com.vaidg.model.ProfileResponse;
import com.vaidg.model.ServerResponse;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.google.gson.Gson;
import com.pojo.EditProfileBody;
import com.pojo.ErrorHandel;
import com.utility.RefreshToken;
import com.vaidg.utilities.Utility;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import javax.inject.Inject;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * @author Pramod
 * @since 4/12/2017.
 */

public class ProfilePresenterImpl implements ProfilePresenter {

  @Inject
  ProfileView view;

  @Inject
  LSPServices lspServices;

  @Inject
  SessionManagerImpl sessionManager;

  @Inject
  Gson gson;

  @Inject
  Context context;

  private CompositeDisposable compositeDisposable;

  private CompositeDisposable verDisposable;

  @Inject
  ProfilePresenterImpl(ProfileView view) {
    this.view = view;
    this.compositeDisposable = new CompositeDisposable();
    this.verDisposable = new CompositeDisposable();
  }

  @Override
  public void getProfile(String auth) {
    onShowProgress();
    Observable<Response<ResponseBody>> request = lspServices.getProfile(getInstance().getAuthToken(sessionManager.getSID()), selLang, PLATFORM_ANDROID);
    request.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {

          @Override
          public void onSubscribe(Disposable d) {
            compositeDisposable.add(d);
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int code = responseBodyResponse.code();
            try {
              JSONObject jsonObject;
              String response = responseBodyResponse.body() != null ?
                      responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                      ? responseBodyResponse.errorBody().string() : null;
              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                  if (!TextUtils.isEmpty(response)) {
                    onHideProgress();
                    ProfileResponse profileResponse = gson.fromJson(response,
                            ProfileResponse.class);
                    if (profileResponse.getData() != null) {
                      ProfileData profileData = profileResponse.getData();

                      String firstName = profileData.getFirstName();
                      String lastName = profileData.getLastName();
                      String email = profileData.getEmail();
                      String phoneNo = profileData.getPhone();
                      String about = profileData.getAbout();
                      String gender = profileData.getGender();
                      String dateOfBirth = profileData.getDateOfBirth();
                      String profilePic = profileData.getProfilePic();
                      sessionManager.setCountrySymbol(profileData.getCountrySymbol());
                      if(view != null) view.setProfileFirstName(firstName);
                      if(view != null) view.setProfileLastName(lastName);
                      if(view != null) view.setProfileEmail(email);
                      if(view != null) view.setProfileMob(phoneNo);
                      if(view != null) view.setAbout(about);
                      if(view != null) view.setGender(gender);
                      if(view != null) view.setDateOfBirth(dateOfBirth);
                      if(view != null) view.setProfilePic(profilePic);
                      if(view != null) view.setCountryCode(profileData.getCountryCode());

                      sessionManager.setFirstName(firstName);
                      sessionManager.setPatientDateOfBirth(dateOfBirth);
                      sessionManager.setPatientGender(gender);
                      sessionManager.setLastName(lastName);
                      sessionManager.setEmail(email);
                      sessionManager.setMobileNo(profileData.getPhone());
                      sessionManager.setProfilePicUrl(profilePic);
                      sessionManager.setAbout(about);
                      sessionManager.setProfileCalled(true);
                      sessionManager.setCountryCode(profileData.getCountryCode());
                    }
                  }
                  break;
                case SESSION_EXPIRED:
                  RefreshToken.onRefreshToken(getInstance().getAuthToken(sessionManager.getSID()), sessionManager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      onHideProgress();
                      getInstance().setAuthToken(sessionManager.getSID(), sessionManager.getSID(), newToken);
                      getProfile(newToken);
                    }

                    @Override
                    public void onFailureRefreshToken() {
                      onHideProgress();
                    }

                    @Override
                    public void sessionExpired(String msg) {
                      onLogout(msg);
                    }
                  });
                  break;
                case SESSION_LOGOUT:
                  onLogout(errorBody);
                  break;
                default:
                  onErrorMsg(errorBody);
                  break;
              }
            } catch (Exception e) {
              onErrorMsg(e.getMessage());
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            onErrorMsg(e.getMessage());
            e.printStackTrace();
          }

          @Override
          public void onComplete() {
            onHideProgress();
          }
        });

  }

  @Override
  public void editProfile(String auth, String profile_pic, String firstName, String lastName, String aboutMe, String gender_val, String dateOfBirth) {
    //LSPServices service = ServiceFactory.createRetrofitService(LSPServices.class);
    EditProfileBody body = new EditProfileBody();
    if (!"".equals(profile_pic)) {
      body.setProfilePic(profile_pic);
      sessionManager.setProfilePicUrl(profile_pic);
    }
    if (!"".equals(firstName)) {
      body.setFirstName(firstName);
      sessionManager.setFirstName(firstName);
    }
    if (!"".equals(lastName)) {
      body.setLastName(lastName);
      sessionManager.setLastName(lastName);
    }
    if (!"".equals(aboutMe)) {
      body.setAbout(aboutMe);
      sessionManager.setAbout(aboutMe);
    }
    if (!"".equals(gender_val)) {
      body.setGender(gender_val);
      sessionManager.setPatientGender(gender_val);
    }
    if (!"".equals(dateOfBirth)) {
      body.setDateOfBirth(dateOB);
      sessionManager.setPatientDateOfBirth(dateOB);
    }
    Observable<Response<ResponseBody>> request = lspServices.editProfile(getInstance().getAuthToken(sessionManager.getSID()), selLang, PLATFORM_ANDROID, body);
    request.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {

          @Override
          public void onSubscribe(Disposable d) {
            verDisposable.add(d);
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int code = responseBodyResponse.code();
            try {
              JSONObject jsonObject;
              String response = responseBodyResponse.body() != null ?
                      responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                      ? responseBodyResponse.errorBody().string() : null;
              switch (code)
              {
                case SUCCESS_RESPONSE:
                  if (!TextUtils.isEmpty(response)) {
                    ServerResponse serverResponse = gson.fromJson(response, ServerResponse.class);
                    if(view != null)view.onSuccess(serverResponse.getMessage());
                  }
                  break;
                case SESSION_EXPIRED:
                  //  ErrorHandel errorHandel = gson.fromJson(errorBody, ErrorHandel.class);
                    RefreshToken.onRefreshToken(getInstance().getAuthToken(sessionManager.getSID()), sessionManager.getREFRESHAUTH(),
                            lspServices,
                            new RefreshToken.RefreshTokenImple() {
                              @Override
                              public void onSuccessRefreshToken(String newToken) {
                                getInstance().setAuthToken(sessionManager.getSID(), sessionManager.getSID(), newToken);
                                editProfile(getInstance().getAuthToken(sessionManager.getSID()), profile_pic, firstName, lastName, aboutMe, gender_val, dateOfBirth);
                               // onHideProgress();
                              }

                              @Override
                              public void onFailureRefreshToken() {
                                //  onHideProgress();
                              }

                              @Override
                              public void sessionExpired(String msg) {
                                //  onHideProgress();
                                  onLogout(msg);
                              }
                            });
                  break;
                  case SESSION_LOGOUT:
                      onLogout(errorBody);
                      break;
                  default:
                  onErrorMsg(errorBody);
                  break;

              }
            } catch (Exception e) {
             // onHideProgress();
              onErrorMsg(e.getMessage());
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            //onHideProgress();
            onErrorMsg(e.getMessage());
            e.printStackTrace();
          }

          @Override
          public void onComplete() {
            // onHideProgress();
          }
        });

  }

  @Override
  public void doLogout(String auth) {
    onShowProgress();
    Observable<Response<ServerResponse>> request = lspServices.logout(getInstance().getAuthToken(sessionManager.getSID()), selLang, PLATFORM_ANDROID);
    request.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ServerResponse>>() {
          @Override
          public void onSubscribe(Disposable d) {
            verDisposable.add(d);
          }

          @Override
          public void onNext(Response<ServerResponse> responseBodyResponse) {
            int code = responseBodyResponse.code();
            try {
              String errorBody = responseBodyResponse.errorBody() != null
                      ? responseBodyResponse.errorBody().string() : null;
              switch (code)
              {
                case SUCCESS_RESPONSE:
                  if (responseBodyResponse.body() != null) {
                    onHideProgress();
                      if (view != null) view.onLogout("", sessionManager);
                  }
                  break;
                case SESSION_EXPIRED:
                  //  ErrorHandel errorHandel = gson.fromJson(errorBody, ErrorHandel.class);
                  RefreshToken.onRefreshToken(getInstance().getAuthToken(sessionManager.getSID()), sessionManager.getREFRESHAUTH(),
                          lspServices,
                          new RefreshToken.RefreshTokenImple() {
                            @Override
                            public void onSuccessRefreshToken(String newToken) {
                              onHideProgress();
                              getInstance().setAuthToken(sessionManager.getSID(), sessionManager.getSID(), newToken);
                              doLogout(auth);
                            }

                            @Override
                            public void onFailureRefreshToken() {
                                onHideProgress();
                            }

                            @Override
                            public void sessionExpired(String msg) {
                              onLogout(msg);
                            }
                          });
                  break;
                  case SESSION_LOGOUT:
                      onLogout(errorBody);
                      break;
                default:
                  onErrorMsg(errorBody);
                  break;

              }
            } catch (Exception e) {
              onErrorMsg(e.getMessage());
              e.printStackTrace();
            }
          }
          @Override
          public void onError(Throwable e) {
           onErrorMsg(e.getMessage());
            e.printStackTrace();
          }

          @Override
          public void onComplete() {
              onHideProgress();
          }
        });

  }


  @Override
  public void openCalendar(
      Context mContext) {
    final Calendar newCalendar = Calendar.getInstance();
    newCalendar.setTimeZone(Utility.getTimeZone());
    DatePickerDialog fromDatePickerDialog = new DatePickerDialog(mContext,
            (v, year, monthOfYear, dayOfMonth) -> {
              newCalendar.set(year, monthOfYear, dayOfMonth);
              SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy",
                  Locale.US);
              dateFormatter.setTimeZone(Utility.getTimeZone());
              dateOB = dateFormatter.format(newCalendar.getTime());
              String title = DateUtils.formatDateTime(mContext,
                  newCalendar.getTimeInMillis(),
                  DateUtils.FORMAT_SHOW_DATE
                      | DateUtils.FORMAT_SHOW_YEAR
                      | DateUtils.FORMAT_ABBREV_MONTH);
              view.setDateOfBirth(dateFormatter.format(newCalendar.getTime()));
              checkDobValidity();
            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH),
        newCalendar.get(Calendar.DAY_OF_MONTH));

    newCalendar.add(Calendar.YEAR, -18);
    fromDatePickerDialog.getDatePicker().setMaxDate(newCalendar.getTimeInMillis());
    fromDatePickerDialog.show();
  }

  @Override
  public boolean checkDobValidity() {
    return true;
  }

  @Override
  public void attachView(Object view) {
  }

  @Override
  public void detachView() {
  }

  @Override
  public void onHideProgress() {
    if (view != null) view.onHideProgress();
  }

  @Override
  public void onShowProgress() {
    if (view != null) view.onShowProgress();
  }

  @Override
  public void onLogout(String errorBody) {
      onHideProgress();
    try {
      if (!TextUtils.isEmpty(errorBody)) {
        JSONObject jsonObject = new JSONObject(errorBody);
        if (!jsonObject.getString(MESSAGE).isEmpty()) {
          if (view != null) view.onLogout(jsonObject.getString(MESSAGE), sessionManager);
        }
      }
    } catch (JSONException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void onErrorMsg(String errorBody) {
      onHideProgress();
    try {
      if (!TextUtils.isEmpty(errorBody)) {
        JSONObject jsonObject = new JSONObject(errorBody);
        if (!jsonObject.getString(MESSAGE).isEmpty()) {
          if (view != null) view.onError(jsonObject.getString(MESSAGE));
        }
      }
    } catch (JSONException e) {
      e.printStackTrace();
    }
  }
}
