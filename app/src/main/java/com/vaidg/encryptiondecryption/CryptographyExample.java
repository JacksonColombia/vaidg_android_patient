/*
 * Copyright (c) 2015-2020, Virgil Security, Inc.
 *
 * Lead Maintainer: Virgil Security Inc. <support@virgilsecurity.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     (1) Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *     (2) Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *     (3) Neither the name of virgil nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.vaidg.encryptiondecryption;

import android.util.Base64;
import android.util.Log;

import com.virgilsecurity.sdk.crypto.KeyPairType;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilKeyPair;
import com.virgilsecurity.sdk.crypto.VirgilPrivateKey;
import com.virgilsecurity.sdk.crypto.VirgilPublicKey;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;
import com.virgilsecurity.sdk.crypto.exceptions.DecryptionException;
import com.virgilsecurity.sdk.crypto.exceptions.EncryptionException;
import com.virgilsecurity.sdk.crypto.exceptions.SigningException;
import com.virgilsecurity.sdk.crypto.exceptions.VerificationException;
import com.virgilsecurity.sdk.utils.ConvertionUtils;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class CryptographyExample.
 */
public class CryptographyExample {

  public static void main(String[] args) {
   // new CryptographyExample().run();
    System.out.println("Done!");
  }

  public byte[] createSignature(VirgilPrivateKey senderPrivateKey, String messageToSign) throws SigningException {
    VirgilCrypto crypto = new VirgilCrypto();

    // prepare a message
  //  String messageToSign = "Hello, Bob!";
    byte[] dataToSign = ConvertionUtils.toBytes(messageToSign);

    // generate a signature
    byte[] signature = crypto.generateSignature(dataToSign, senderPrivateKey);

    return signature;
  }

  public String dataDecryption(String encryptedData, VirgilPrivateKey receiverPrivateKey)
      throws DecryptionException {
    VirgilCrypto crypto = new VirgilCrypto();

    byte[] dataToDecrypt = Base64.decode(encryptedData.getBytes(StandardCharsets.UTF_8), Base64.DEFAULT);

    // prepare data to be decrypted5
    byte[] decryptedData = crypto.decrypt(dataToDecrypt, receiverPrivateKey);

    // decrypt the encrypted data using a public key
    String decryptedMessage =  new String(decryptedData, StandardCharsets.UTF_8);
      ;
    /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
      decryptedMessage = new String(decryptedData, StandardCharsets.UTF_8);
    }
    else
    {
      decryptedMessage = new String(decryptedData);
    }*/
    return decryptedMessage;
  }

  public byte[] dataEncryption(List<VirgilPublicKey> receiverPublicKeys, String messageToEncrypt) throws EncryptionException {
    VirgilCrypto crypto = new VirgilCrypto();

    // prepare a message
    //String messageToEncrypt = "Hello, Bob!";

    byte[] dataToEncrypt = messageToEncrypt.getBytes(StandardCharsets.UTF_8);//messageToEncrypt.getBytes(StandardCharsets.UTF_8);

    // encrypt the message
    byte[] encryptedData = crypto.encrypt(dataToEncrypt, receiverPublicKeys);

    return encryptedData;
  }

  public String decryptThenVerify(byte[] encryptedData, VirgilPrivateKey receiverPrivateKey,
                                  VirgilPublicKey senderPublicKey, VirgilCrypto crypto) throws CryptoException {

    // data to be decrypted and verified
    byte[] decryptedData = crypto.decryptThenVerify(encryptedData, receiverPrivateKey,
        Arrays.asList(senderPublicKey));

    // a decrypted message
    String decryptedMessage = ConvertionUtils.toString(decryptedData);

    return decryptedMessage;
  }

  public String exportPrivateKey(VirgilKeyPair keyPair, VirgilCrypto crypto) throws CryptoException {

    // export a public key
    byte[] privateKeyData = crypto.exportPrivateKey(keyPair.getPrivateKey());
    String privateKeyStr = ConvertionUtils.toBase64String(privateKeyData);
    Log.w("Abc", "exportPrivateKey: "+privateKeyStr );
    return privateKeyStr;
  }

  public String exportPublicKey(VirgilKeyPair keyPair, VirgilCrypto crypto) throws CryptoException {

    // export a Public key
    byte[] publicKeyData = crypto.exportPublicKey(keyPair.getPublicKey());
    String publicKeyStr = ConvertionUtils.toBase64String(publicKeyData);

    Log.w("Abc", "exportPrivateKey: "+publicKeyStr );
    return publicKeyStr;
  }

  public VirgilPrivateKey importPrivateKey(String privateKeyStr, VirgilCrypto crypto) throws CryptoException {


   // String privateKeyStr = "MIGhMF0GCSqGSIb3DQEFDTBQMC8GCSqGSIb3DQEFDDAiBBBtfBoM7VfmWPlvyHuGWvMSAgIZ6zAKBggqhkiG9w0CCjAdBglghkgBZQMEASoEECwaKJKWFNn3OMVoUXEcmqcEQMZ+WWkmPqzwzJXGFrgS/+bEbr2DvreVgEUiLKrggmXL9ZKugPKG0VhNY0omnCNXDzkXi5dCFp25RLqbbSYsCyw=";

    byte[] privateKeyData = ConvertionUtils.base64ToBytes(privateKeyStr);

    // import a public key
    VirgilPrivateKey privateKey = crypto.importPrivateKey(privateKeyData).getPrivateKey();

    Log.w("importPrivateKey", "importPrivateKey: "+privateKey.getPrivateKey());

    return privateKey;
  }

  public VirgilPublicKey importPublicKey(String publicKeyStr, VirgilCrypto crypto) throws CryptoException {

   // String publicKeyStr = "MCowBQYDK2VwAyEA9IVUzsQENtRVzhzraTiEZZy7YLq5LDQOXGQG/q0t0kE=";

    byte[] publicKeyData = ConvertionUtils.base64ToBytes(publicKeyStr);

    // import a Public key
    VirgilPublicKey publicKey = crypto.importPublicKey(publicKeyData);

    return publicKey;
  }

  public VirgilKeyPair keyGeneration() throws CryptoException {
    VirgilCrypto crypto = new VirgilCrypto();
    VirgilKeyPair keyPair = crypto.generateKeyPair();

    return keyPair;
  }

  @SuppressWarnings("unused")
  public byte[] multipleEncryption() throws CryptoException {
    // specify participants public keys
    VirgilCrypto crypto = new VirgilCrypto();
    List<VirgilPublicKey> receiversPublicKeys = new ArrayList<>();
    for (int i = 0; i < 3; i++) {
      VirgilKeyPair keyPair = crypto.generateKeyPair();
      receiversPublicKeys.add(keyPair.getPublicKey());
    }

    // prepare a message to be encrypted for participants
    String messageToEncrypt = "Hello, Bob!";
    byte[] dataToEncrypt = ConvertionUtils.toBytes(messageToEncrypt);

    // encrypt the message
    byte[] encryptedData = crypto.encrypt(dataToEncrypt, receiversPublicKeys);

    return encryptedData;
  }

  @SuppressWarnings("unused")
  public void run() {
   /* try {
      // Generate keys
      VirgilKeyPair keyPair = keyGeneration();
      VirgilCrypto crypto = new VirgilCrypto();
      // Export public key
      String exportedPrivateKey = exportPrivateKey(keyPair,crypto);
      System.out.println(String.format("Exported public key: %s", exportedPrivateKey));

      // Export public key
      String exportedPublicKey = exportPublicKey(keyPair,crypto);
      System.out.println(String.format("Exported public key: %s", exportedPublicKey));

      // Import public key
      VirgilPrivateKey importedPrivateKey = importPrivateKey(exportedPrivateKey, crypto);

      // Import public key
      VirgilPublicKey importedPublicKey = importPublicKey(exportedPublicKey, crypto);

      String messageToSign = "Hello, Bob!,How are you";

      // Sign data wit public key
      byte[] signature = createSignature(keyPair.getPrivateKey(),messageToSign);

      // Verify data signature
      byte[] dataToSign = ConvertionUtils.toBytes(messageToSign);
      boolean valid = verifySignature(signature, dataToSign, keyPair.getPublicKey());
      System.out.println(valid ? "Signature is valid" : "Signature is not valid");

      // Encrypt data
      byte[] encryptedData = dataEncryption(keyPair.getPublicKey(),messageToSign);

      // Decrypt data
      String decrypted = dataDecryption(encryptedData, keyPair.getPrivateKey());
      System.out.println(String.format("Data decrypted: %s", decrypted));

      // Sign and encrypt data
      VirgilKeyPair receiverKeyPair = keyGeneration();
      byte[] signedAndEncryptedData = signThenEncrypt(keyPair.getPrivateKey(),
          receiverKeyPair.getPublicKey(), crypto, dataToDecrypt);

      // Decrypt and verify data
      String decryptedAndVerified = decryptThenVerify(signedAndEncryptedData,
          receiverKeyPair.getPrivateKey(), keyPair.getPublicKey());
      System.out.println(String.format("Data decrypted and verified: %s", decryptedAndVerified));

    } catch (CryptoException e) {
      e.printStackTrace();
    }*/
  }

  public byte[] signThenEncrypt(VirgilPrivateKey senderPrivateKey,
                                VirgilPublicKey receiverPublicKey, VirgilCrypto crypto, byte[] dataToDecrypt) throws CryptoException {
    // use a public key to sign the message and a public key to decrypt it
    byte[] encryptedData = crypto.signThenEncrypt(dataToDecrypt, senderPrivateKey,
        receiverPublicKey);

    return encryptedData;
  }

  @SuppressWarnings("unused")
  public VirgilKeyPair specificGeneration() throws CryptoException {
    VirgilCrypto crypto = new VirgilCrypto(KeyPairType.RSA_4096);
    VirgilKeyPair keyPair = crypto.generateKeyPair();

    return null;
  }

  public boolean verifySignature(byte[] signature, byte[] dataToSign,
                                 VirgilPublicKey senderPublicKey, VirgilCrypto crypto) throws VerificationException {

    // verify a signature
    boolean verified = crypto.verifySignature(signature, dataToSign, senderPublicKey);

    return verified;
  }
}
