package com.vaidg.jobDetailsStatus;

import static com.vaidg.utilities.Constants.AES_MODE;
import static com.vaidg.utilities.Constants.ANDROID_KEYSTORE;
import static com.vaidg.utilities.Constants.FIXED_IV;
import static com.vaidg.utilities.Constants.IV_PARAMETER_SPEC;
import static com.vaidg.utilities.Constants.KEY_PRIVATEALIAS;
import static com.vaidg.utilities.Constants.KEY_PUBLICALIAS;
import static com.vaidg.utilities.Constants.RSA_MODE;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.vaidg.R;
import com.vaidg.encryptiondecryption.CryptographyExample;
import com.vaidg.networking.ChatApiService;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pojo.BookingDetailsPojo;
import com.pojo.ErrorHandel;
import com.pojo.PostCallResponse;
import com.pojo.SymptomQuestionAnswer;
import com.utility.RefreshToken;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilPrivateKey;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * <h2>JobDetailsContractImpl</h2>
 * Created by Ali on 2/15/2018.
 */

public class JobDetailsContractImpl implements JobDetailsContract.Presenter {
  @Inject
  SessionManagerImpl manager;
  @Inject
  LSPServices lspServices;
  @Inject
  JobDetailsContract.JobView view;
  @Inject
  Gson gson;
  private String TAG = JobDetailsContractImpl.class.getSimpleName();
  private String callId;
  private KeyStore keyStore;

  @Inject
  public JobDetailsContractImpl() {

  }

  @Override
  public void attachView(Object view) {

  }

  @Override
  public void detachView() {

  }

  @Override
  public void onGetBookingDetails(final long bid) {
    Log.d(TAG, "onGetBookingDetails: " + bid);
    Observable<Response<ResponseBody>> observable = lspServices.onToGetBookingDetails(LSPApplication.getInstance().getAuthToken(manager.getSID())
        , Constants.selLang, Constants.PLATFORM_ANDROID, bid);

    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            int code = responseBodyResponse.code();
            Log.d(TAG, "onNextJobDetails: " + code);
            String response;
            JSONObject jsonObject;
            try {
              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                  response = responseBodyResponse.body().string();
                  Log.d(TAG, "onNextJobDetailsRes: " + response);
                  try {
                    BookingDetailsPojo bookingDetailsPojo = gson.fromJson(response, BookingDetailsPojo.class);
                    BookingDetailsPojo.BookingDetailsData bookDD = bookingDetailsPojo.getData();
                    Constants.bookingModelJobDetails = bookDD.getBookingModel();
                    Constants.bookingModel = bookDD.getAccounting().getServiceType();
                    Constants.catName = bookDD.getCategoryName();
                    Constants.callTypeInOutTele = bookDD.getCallType();
                    Constants.isCallActivated = bookDD.isCallActive();

                    Constants.isNeedApproveBycustomer = bookDD.isNeedApproveBycustomer();

                    Constants.bookingTypeNowSchedule = bookDD.getBookingModel();

                    LatLng customerLatLng = new LatLng(bookDD.getLatitude(), bookDD.getLongitude());
                    Constants.custLatLng = customerLatLng;
                    LatLng proLatLng = new LatLng(bookDD.getProviderDetail().getProLocation().getLatitude()
                        , bookDD.getProviderDetail().getProLocation().getLongitude());
                    // view.onJobStatusTime(liveBookingStatusPojo.getData().getJobStatusLogs());
                    Constants.catId = bookDD.getCart().getCategoryId();
                    String currencySymbol = bookDD.getCurrencySymbol();
                    double amount = bookDD.getAccounting().getAmount();
                    if(view != null) {
                      view.setCatDesc(bookDD.getCategoryName(),
                          bookDD.getCategoryId(), bookDD.getCallType());
                      view.onProviderDtls(bookDD.getProviderDetail(), amount, currencySymbol
                          , bookDD.getStatus(), bookDD.getReminderId(), bookDD.getCart().getCategoryName(), bookDD.getJobDescription());
                      String name = bookDD.getProviderDetail().getTitle() + " " + bookDD.getProviderDetail().getFirstName() + " " + bookDD.getProviderDetail().getLastName();
                      manager.setProName(name);
                      manager.setChatProId(bookDD.getProviderDetail().getProviderId());
                      manager.setChatBookingID(bookDD.getBookingId());
                      manager.setChatProPic(bookDD.getProviderDetail().getProfilePic());
                      view.onBookingSuccessInfo(bookDD.getBookingExpireTime(),
                          bookDD.getServerTime(), customerLatLng, proLatLng, bookDD.getStatus(), bookDD.getCart().getCategoryName());
                      view.onBookingTimer(bookDD.getBookingTimer(), bookDD.getStatus()
                          , bookDD.getServerTime(), bookDD.getStatusMsg()
                          , bookDD.getBookingRequestedAt(), bookDD.getAccounting().getTotalJobTime());
                      view.JobDetailsAccounting(bookDD.getJobDescription(),
                          bookDD.getBookingType(),
                          bookDD.getBookingRequestedFor()
                          , bookDD.getAddLine1(), bookDD.getAccounting()
                          , bookDD.getCart());
                      view.onJobInfo(bookDD.getBookingModel(), bookDD.getAccounting(), bookDD.getCart()
                          , bookDD.getCategoryName());
                      view.onBidDispatchLog(bookDD.getBidDispatchLog(), bookDD.getStatus());
                      String messageFromDecrypt = "";
                      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        decryptPrivateKeyPostLollipop();
                        decryptPublicKeyPostLollipop();
                        messageFromDecrypt =
                            encryptData(decryptPrivateKeyPostLollipop(), decryptPublicKeyPostLollipop(), messageFromDecrypt, bookDD.getQuestionAndAnswer());
                      } else {
                        decryptPrivateKeyPreMarshMallow();
                        decryptPublicKeyPreMarshMallow();
                        messageFromDecrypt = encryptData(decryptPrivateKeyPreMarshMallow(), decryptPublicKeyPreMarshMallow(), messageFromDecrypt, bookDD.getQuestionAndAnswer());
                      }
                      if (messageFromDecrypt != null && !messageFromDecrypt.isEmpty()) {
                        Type collectionType = new TypeToken<ArrayList<SymptomQuestionAnswer>>() {
                        }.getType();
                        ArrayList<SymptomQuestionAnswer> questionAndAnswer = gson.fromJson(messageFromDecrypt, collectionType);
                        view.onBidQuestionAnswer(questionAndAnswer, bookDD.getBookingModel());
                      } else {
                        view.noSymptom();
                      }
                      if (bookDD.getCallType() == 1)
                        view.loadFragment();
                      view.onHideProgress();
                    }
                  } catch (Exception e) {
                    e.printStackTrace();
                    if(view != null)
                    view.onHideProgress();
                  }
                  // gson
                  break;
                case Constants.SESSION_LOGOUT:
                  response = responseBodyResponse.errorBody().string();
                  jsonObject = new JSONObject(response);
                  if(view != null) {
                    view.onHideProgress();
                    view.onLogout(jsonObject.getString("message"), manager);
                  }
                  break;
                case Constants.SESSION_EXPIRED:
                  response = responseBodyResponse.errorBody().string();
                  ErrorHandel errorHandel = gson.fromJson(response, ErrorHandel.class);

                  // String
                  RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      if(view != null)
                        view.onHideProgress();
                        LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                      onGetBookingDetails(bid);
                    }

                    @Override
                    public void onFailureRefreshToken() {

                    }

                    @Override
                    public void sessionExpired(String msg) {
                      if(view != null) {
                        view.onHideProgress();
                        view.onLogout(msg, manager);
                      }
                    }
                  });
                  break;
              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
              if(view != null)
                view.onHideProgress();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null)
              view.onHideProgress();
          }

          @Override
          public void onComplete() {

          }
        });
  }
  private String encryptData(String privateKey, String publicKey, String messageFromDecrypt, String messageToDecrypt) {

    try {
      CryptographyExample cryptographyExample = new CryptographyExample();
      VirgilCrypto crypto = new VirgilCrypto();
      // Import private key
      VirgilPrivateKey importedPrivateKey = cryptographyExample.importPrivateKey(privateKey, crypto);
       // Decrypt data
    //  byte[] encryptedData = Base64.decode(messageToDecrypt, Base64.DEFAULT);/*Base64.getMimeDecoder().decode(chatData.get(position).getContent());*/
      messageFromDecrypt = cryptographyExample.dataDecryption(messageToDecrypt, importedPrivateKey);
    } catch (CryptoException e) {
      e.printStackTrace();
    }
    return messageFromDecrypt;
  }

  @Override
  public void onFragmentTransition(int status, FragmentManager supportFragmentManger, Fragment... mFragment) {

    FragmentTransaction fragmentTransaction = supportFragmentManger.beginTransaction();


    switch (status) {
      case 1:
      case 2:
      case 17:
      case 130:
        fragmentTransaction.replace(R.id.flBookingScreen, mFragment[4], "MessageFragment");
        break;
      case 3:
        fragmentTransaction.replace(R.id.flBookingScreen, mFragment[0], "ProHiredFrag");
        break;
      case 6:
        fragmentTransaction.replace(R.id.flBookingScreen, mFragment[1], "ProOnTHeWayFrag");
        break;
      case 7:
        fragmentTransaction.replace(R.id.flBookingScreen, mFragment[2], "ProArrivedFrag");
        break;
      case 8:
      case 9:
        fragmentTransaction.replace(R.id.flBookingScreen, mFragment[3], "JobStartedFrag");
        break;
    }

    fragmentTransaction.commit();

  }

  @Override
  public void onFragmentTransitionIn(int status, FragmentManager supportFragmentManger, Fragment... mFragment) {

    FragmentTransaction fragmentTransaction = supportFragmentManger.beginTransaction();
    Log.d(TAG, "onFragmentTransitionIn: bidding test status " + status);
    switch (status) {
      case 1:
      case 2:
        fragmentTransaction.replace(R.id.flBookingScreen, mFragment[0], "MessageFragment");
        break;
      case 3:
      case 6:
      case 7:
      case 8:
      case 9:
      case 17:
      case 130:
        fragmentTransaction.replace(R.id.flBookingScreen, mFragment[1], "ProHiredIn");
        break;

    }

    fragmentTransaction.commit();

  }

  @Override
  public void onUiStatusChange(Context mContext, int status, View... views) {

    Log.d(TAG, "onUiStatusChange: " + status);
    switch (status) {
      case 1:
      case 2:
      case 17:
      case 130:
        break;
                   /* views[0].setBackgroundResource(R.drawable.ic_check);
                    views[1].setBackgroundResource(R.color.parrotGreen);*/
      // break;
      case 3:
        views[0].setBackgroundResource(R.color.parrotGreen);
        views[1].setBackgroundResource(R.drawable.ic_check);
        break;
      case 6:
        views[0].setBackgroundResource(R.color.parrotGreen);
        views[1].setBackgroundResource(R.drawable.ic_check);
        views[2].setBackgroundResource(R.color.parrotGreen);
        views[3].setBackgroundResource(R.color.parrotGreen);
        views[4].setBackgroundResource(R.drawable.ic_check);
        views[5].setBackgroundResource(R.color.parrotGreen);
        views[6].setBackgroundResource(R.color.parrotGreen);
        break;
      case 7:
        views[0].setBackgroundResource(R.color.parrotGreen);
        views[1].setBackgroundResource(R.drawable.ic_check);
        views[2].setBackgroundResource(R.color.parrotGreen);
        views[3].setBackgroundResource(R.color.parrotGreen);
        views[4].setBackgroundResource(R.drawable.ic_check);
        views[5].setBackgroundResource(R.color.parrotGreen);
        views[6].setBackgroundResource(R.color.parrotGreen);
        views[7].setBackgroundResource(R.drawable.ic_check);
        views[8].setBackgroundResource(R.color.parrotGreen);

        break;
      case 8:
        views[0].setBackgroundResource(R.color.parrotGreen);
        views[1].setBackgroundResource(R.drawable.ic_check);
        views[2].setBackgroundResource(R.color.parrotGreen);
        views[3].setBackgroundResource(R.color.parrotGreen);
        views[4].setBackgroundResource(R.drawable.ic_check);
        views[5].setBackgroundResource(R.color.parrotGreen);
        views[6].setBackgroundResource(R.color.parrotGreen);
        views[7].setBackgroundResource(R.drawable.ic_check);
        views[8].setBackgroundResource(R.color.parrotGreen);
        views[9].setBackgroundResource(R.color.parrotGreen);
        views[10].setBackgroundResource(R.drawable.ic_check);
        break;
      case 9:
        views[0].setBackgroundResource(R.color.parrotGreen);
        views[1].setBackgroundResource(R.drawable.ic_check);
        views[2].setBackgroundResource(R.color.parrotGreen);
        views[3].setBackgroundResource(R.color.parrotGreen);
        views[4].setBackgroundResource(R.drawable.ic_check);
        views[5].setBackgroundResource(R.color.parrotGreen);
        views[6].setBackgroundResource(R.color.parrotGreen);
        views[7].setBackgroundResource(R.drawable.ic_check);
        views[8].setBackgroundResource(R.color.parrotGreen);
        views[9].setBackgroundResource(R.color.parrotGreen);
        views[10].setBackgroundResource(R.drawable.ic_check);
        break;
    }
  }

  @Override
  public void onUiInCallCahnge(Context mContext, int status, View... views) {


    Log.d(TAG, "onUiStatusChange: " + status);
    switch (status) {
      case 1:
      case 2:
      case 17:
      case 130:
        break;

      case 3:
        views[0].setBackgroundResource(R.drawable.ic_check);
        break;
      case 6:
        views[0].setBackgroundResource(R.drawable.ic_check);
        views[1].setBackgroundResource(R.color.parrotGreen);
        views[2].setBackgroundResource(R.color.parrotGreen);

        break;
      case 7:
        views[0].setBackgroundResource(R.drawable.ic_check);
        views[1].setBackgroundResource(R.color.parrotGreen);
        views[2].setBackgroundResource(R.color.parrotGreen);


        break;
      case 8:
        views[0].setBackgroundResource(R.drawable.ic_check);
        views[1].setBackgroundResource(R.color.parrotGreen);
        views[2].setBackgroundResource(R.color.parrotGreen);

        break;
      case 9:
        views[0].setBackgroundResource(R.drawable.ic_check);
        views[1].setBackgroundResource(R.color.parrotGreen);
        views[2].setBackgroundResource(R.color.parrotGreen);
        views[3].setBackgroundResource(R.drawable.ic_check);
        break;
    }
  }

  @Override
  public void changeWaitSum(Context mContext, int status, LinearLayout llJobContactInfo, TextView... textViews) {
if(Constants.isCallActivated) {
  textViews[0].setVisibility(View.VISIBLE); // call
}else{
  textViews[0].setVisibility(View.VISIBLE); // call

}
    textViews[1].setVisibility(View.VISIBLE); // message
    textViews[2].setVisibility(View.VISIBLE); // cancel
    textViews[3].setVisibility(View.VISIBLE); // details
    switch (status) {
      case 1:
      case 2:
      case 17:
      case 130:
        //llJobContactInfo.setWeightSum(2);
        if(Constants.isCallActivated) {
          textViews[0].setVisibility(View.VISIBLE); // call
        }else{
          textViews[0].setVisibility(View.VISIBLE); // call

        }        textViews[1].setVisibility(View.GONE);
        break;
      case 3:
      case 6:
        //llJobContactInfo.setWeightSum(4);
        break;
      case 7:
      case 8:
      case 9:
        //llJobContactInfo.setWeightSum(3);
        textViews[2].setVisibility(View.GONE);
        break;

    }
  }

  @Override
  public void changeViews(Context mContext, int status, View... views) {
    if(Constants.isCallActivated) {
      views[0].setVisibility(View.VISIBLE); // call
    }else{
      views[0].setVisibility(View.GONE); // call

    }
    views[1].setVisibility(View.VISIBLE); // message
    views[2].setVisibility(View.VISIBLE); // cancel
    // textViews[3].setVisibility(View.VISIBLE); // details
    switch (status) {
      case 1:
      case 2:
      case 17:
      case 130:
        // llJobContactInfo.setWeightSum(2);
        if(Constants.isCallActivated) {
          views[0].setVisibility(View.VISIBLE); // call
        }else{
          views[0].setVisibility(View.VISIBLE); // call

        }
        views[1].setVisibility(View.GONE);
        break;
      case 3:
      case 6:
        // llJobContactInfo.setWeightSum(4);
        break;
      case 7:
      case 8:
      case 9:
        //   llJobContactInfo.setWeightSum(3);
        views[2].setVisibility(View.GONE);
        //  views[2].setVisibility(View.VISIBLE);
        break;

    }
  }

  @Override
  public void changeWaitSumInCall(JobDetailsActivity jobDetailsActivity, int statusCode, boolean isVisible, LinearLayout llJobContactInfo, TextView... textViews) {

    if(Constants.isCallActivated) {
      textViews[0].setVisibility(View.VISIBLE); // call
    }else{
      textViews[0].setVisibility(View.VISIBLE); // call

    }    textViews[1].setVisibility(View.VISIBLE); // message
    textViews[2].setVisibility(View.VISIBLE); // cancel
    textViews[3].setVisibility(View.VISIBLE); // details
    switch (statusCode) {
      case 1:
      case 2:
      case 17:
      case 130:
        if (isVisible) {
          //llJobContactInfo.setWeightSum(4);
          if(Constants.isCallActivated) {
            textViews[0].setVisibility(View.VISIBLE); // call
          }else{
            textViews[0].setVisibility(View.VISIBLE); // call

          }        } else {
          //llJobContactInfo.setWeightSum(3);
          textViews[0].setVisibility(View.GONE);
        }


        break;
      case 3:
      case 6:
        if (isVisible) {
          //llJobContactInfo.setWeightSum(4);
          if(Constants.isCallActivated) {
            textViews[0].setVisibility(View.VISIBLE); // call
          }else{
            textViews[0].setVisibility(View.VISIBLE); // call

          }
        } else {
          //llJobContactInfo.setWeightSum(3);
          textViews[0].setVisibility(View.GONE);
        }

        break;
      case 7:
      case 8:
      case 9:
        if (isVisible) {
          //llJobContactInfo.setWeightSum(4);
          if(Constants.isCallActivated) {
            textViews[0].setVisibility(View.VISIBLE); // call
          }else{
            textViews[0].setVisibility(View.VISIBLE); // call

          }        } else {
          //llJobContactInfo.setWeightSum(3);
          textViews[0].setVisibility(View.GONE);
        }

        break;

    }
  }

  @Override
  public void changeViewsInCall(JobDetailsActivity jobDetailsActivity, int statusCode, boolean isVisible, View... views) {
    if(Constants.isCallActivated) {
      views[0].setVisibility(View.VISIBLE); // call
    }else{
      views[0].setVisibility(View.VISIBLE); // call

    }
    views[1].setVisibility(View.VISIBLE); // message
    views[2].setVisibility(View.VISIBLE); // cancel
    // textViews[3].setVisibility(View.VISIBLE); // details
    switch (statusCode) {
      case 1:
      case 2:
      case 17:
      case 130:
        if (isVisible) {
          views[2].setVisibility(View.VISIBLE);
        } else {
          views[2].setVisibility(View.GONE);
        }


        break;
      case 3:
      case 6:
        if (isVisible) {
          views[2].setVisibility(View.VISIBLE);
        } else {
          views[2].setVisibility(View.GONE);
        }
        break;
      case 7:
      case 8:
      case 9:
        if (isVisible) {
          views[2].setVisibility(View.VISIBLE);
        } else {
          views[2].setVisibility(View.GONE);
        }
        break;

    }
  }

  @Override
  public void onPendingBiddingBooking(long bId, int status) {


    Map<String, Object> jsonParams = new HashMap<>();
    jsonParams.put("bookingId", bId);
    jsonParams.put("status", status);

    Observable<Response<ResponseBody>> observable = lspServices.acceptBidBooking(LSPApplication.getInstance().getAuthToken(manager.getSID()),
        Constants.selLang, Constants.PLATFORM_ANDROID, jsonParams);

    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            int code = responseBodyResponse.code();
            String response;
            try {

              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                  response = responseBodyResponse.body().string();
                  Log.d(TAG, "onNextPendingBooking: " + response);
                  JSONObject jsonObject = new JSONObject(response);
                  JSONArray arry = jsonObject.getJSONArray("data");
                  if (view != null && arry.length() > 0)
                    view.onPendingBiddingBooking(arry.getLong(0));

                  break;
                case Constants.SESSION_EXPIRED:
                  response = responseBodyResponse.errorBody().string();
                  ErrorHandel errorHandel = gson.fromJson(response, ErrorHandel.class);
                  RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {

                        LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                      onPendingBiddingBooking(bId, status);
                    }

                    @Override
                    public void onFailureRefreshToken() {

                    }

                    @Override
                    public void sessionExpired(String msg) {
                      if (view != null)
                        view.onLogout(msg, manager);
                    }
                  });
                  break;
                case Constants.SESSION_LOGOUT:
                  response = responseBodyResponse.errorBody().string();
                  jsonObject = new JSONObject(response);
                  if (view != null)
                    view.onLogout(jsonObject.getString("message"), manager);
                  break;

              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {

            if (view != null) {
              view.onConnectionError(e.getMessage(), true);
            }
          }

          @Override
          public void onComplete() {

          }
        });
  }

  @Override
  public void showAnimationCardVied(boolean isVisible, CardView cardJobViewContactInfo) {
    float alpha;
    int height;
    if (isVisible) {
      alpha = 1.0f;
      height = 0;
    } else {
      height = cardJobViewContactInfo.getHeight();
      alpha = 0.0f;
    }

    cardJobViewContactInfo.animate()
        .translationY(height)
        .alpha(alpha)
        .setDuration(300)
        .setListener(new AnimatorListenerAdapter() {
          @Override
          public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            if (isVisible)
              cardJobViewContactInfo.setVisibility(View.VISIBLE);
            else
              cardJobViewContactInfo.setVisibility(View.GONE);

          }
        });

  }

  @Override
  public void callInitCallApi(String callProId, String randomString, ChatApiService chatApiService, String bookingId, String callTypeValue) {

    callId = null;

    Map<String, Object> objectMap = new HashMap<>();
    objectMap.put("type", callTypeValue);
    objectMap.put("room", randomString);
    objectMap.put("to", callProId);
    objectMap.put("bookingId", bookingId);


    chatApiService.initCall(manager.getCallToken(), Constants.selLang, objectMap)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {

          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> value) {
            Log.d(TAG, "onNext: " + value.code());


            if (value.code() == 200) {

              try {

                String response = value.body().string();
                //  Log.d(TAG, "onNext: initCall Response" + response);
                PostCallResponse postCallResponse = gson.fromJson(response, PostCallResponse.class);
                callId = postCallResponse.getData().getCallId();
              } catch (Exception e) {
                e.printStackTrace();
              }
              if (!TextUtils.isEmpty(callId)) {

                if (view != null)
                  view.launchCallsScreen(callId, randomString);
              }


            } else {


              //No 200 code
              try {

                String response = value.errorBody().string();
                Log.d(TAG, "onNext: initCall Response" + response);
              } catch (Exception e) {
                e.printStackTrace();
              }
            }


          }

          @Override
          public void onError(Throwable e) {

          }

          @Override
          public void onComplete() {

          }
        });
  }


  @SuppressLint("NewApi")
  @Override
  public String decryptPrivateKeyPostLollipop() {
    String str = "";
    Log.i("TEST", "decryptData marsh melloa ");
    Cipher c;
    try {
      c = Cipher.getInstance(AES_MODE);
      c.init(Cipher.DECRYPT_MODE, getSecretPrivateKeyPostLollipop(), new GCMParameterSpec(128, FIXED_IV.getBytes()));
      byte[] encryptedBytes = Base64.decode(manager.getVirgilPrivateKey(), Base64.DEFAULT);

      byte[] decodedBytes = c.doFinal(encryptedBytes);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        str = new String(decodedBytes, StandardCharsets.UTF_8);
      } else {
        str = new String(decodedBytes);
      }
      Log.i("TEST", "decryptData srting " + str);

    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return str;
  }

  @SuppressLint("NewApi")
  @Override
  public String decryptPublicKeyPostLollipop() {
    String str = "";
    Log.i("TEST", "decryptData marsh melloa ");
    Cipher c;
    try {
      c = Cipher.getInstance(AES_MODE);
      c.init(Cipher.DECRYPT_MODE, getSecretPublicKeyPostLollipop(), new GCMParameterSpec(128, FIXED_IV.getBytes()));
      byte[] encryptedBytes = Base64.decode(manager.getVirgilPublicKey(), Base64.DEFAULT);

      byte[] decodedBytes = c.doFinal(encryptedBytes);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        str = new String(decodedBytes, StandardCharsets.UTF_8);
      } else {
        str = new String(decodedBytes);
      }
      Log.i("TEST", "decryptData srting " + str);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return str;
  }

  /**
   * <h2>getSecretPrivateKeyPreMarshMallow</h2>
   * This method is used to get the secret key for pre marsh mallow device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPrivateKeyPreMarshMallow() throws Exception {
    String enryptedKeyB64 = manager.getEncryptionPrivateKey();
    Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
    // need to check null, omitted here
    byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
    byte[] key = rsaPrivateKeyDecrypt(encryptedKey);
    Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
    return new SecretKeySpec(key, "AES");
  }

  /**
   * <h2>getSecretPublicKeyPreMarshMallow</h2>
   * This method is used to get the secret key for pre marsh mallow device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPublicKeyPreMarshMallow() throws Exception {
    String enryptedKeyB64 = manager.getEncryptionPublicKey();
    Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
    // need to check null, omitted here
    byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
    byte[] key = rsaPublicKeyDecrypt(encryptedKey);
    Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
    return new SecretKeySpec(key, "AES");
  }


  private byte[] rsaPrivateKeyDecrypt(byte[] encrypted) throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PRIVATEALIAS, null);
    Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
    output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
    CipherInputStream cipherInputStream = new CipherInputStream(
        new ByteArrayInputStream(encrypted), output);
    ArrayList<Byte> values = new ArrayList<>();
    int nextByte;
    while ((nextByte = cipherInputStream.read()) != -1) {
      values.add((byte) nextByte);
    }

    byte[] bytes = new byte[values.size()];
    for (int i = 0; i < bytes.length; i++) {
      bytes[i] = values.get(i);
    }
    return bytes;
  }

  private byte[] rsaPublicKeyDecrypt(byte[] encrypted) throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PUBLICALIAS, null);
    Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
    output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
    CipherInputStream cipherInputStream = new CipherInputStream(
        new ByteArrayInputStream(encrypted), output);
    ArrayList<Byte> values = new ArrayList<>();
    int nextByte;
    while ((nextByte = cipherInputStream.read()) != -1) {
      values.add((byte) nextByte);
    }

    byte[] bytes = new byte[values.size()];
    for (int i = 0; i < bytes.length; i++) {
      bytes[i] = values.get(i);
    }
    return bytes;
  }


  @Override
  public String decryptPrivateKeyPreMarshMallow() {
    Log.i("TEST", "decryptData else  ");
    Cipher c;
    String str = "";
    try {
      c = Cipher.getInstance(AES_MODE, "BC");
      c.init(Cipher.DECRYPT_MODE, getSecretPrivateKeyPreMarshMallow(), IV_PARAMETER_SPEC);
      byte[] encryptedBytes = Base64.decode(manager.getEncryptionPrivateKey(), Base64.DEFAULT);
      byte[] decodedBytes = c.doFinal(encryptedBytes);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        str = new String(decodedBytes, StandardCharsets.UTF_8);
      } else {
        str = new String(decodedBytes);
      }
      Log.i("TEST", "decryptData srting " + str);


    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return str;
  }

  @Override
  public String decryptPublicKeyPreMarshMallow() {
    Log.i("TEST", "decryptData else  ");
    Cipher c;
    String str = "";
    try {
      c = Cipher.getInstance(AES_MODE, "BC");
      c.init(Cipher.DECRYPT_MODE, getSecretPublicKeyPreMarshMallow(), IV_PARAMETER_SPEC);
      byte[] encryptedBytes = Base64.decode(manager.getEncryptionPublicKey(), Base64.DEFAULT);
      byte[] decodedBytes = c.doFinal(encryptedBytes);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        str = new String(decodedBytes, StandardCharsets.UTF_8);
      } else {
        str = new String(decodedBytes);
      }
      Log.i("TEST", "decryptData srting " + str);

    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return str;
  }


  /**
   * <h2>getSecretPrivateKeyPostLollipop</h2>
   * This method is used to get the secret key for post lollipop device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPrivateKeyPostLollipop() throws Exception {
    Key s = keyStore.getKey(KEY_PRIVATEALIAS, null);
    Log.i("TEST", "key " + s.toString());
    return keyStore.getKey(KEY_PRIVATEALIAS, null);
  }

  /**
   * <h2>getSecretPublicKeyPostLollipop</h2>
   * This method is used to get the secret key for post lollipop device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPublicKeyPostLollipop() throws Exception {
    Key s = keyStore.getKey(KEY_PUBLICALIAS, null);
    Log.i("TEST", "key " + s.toString());
    return keyStore.getKey(KEY_PUBLICALIAS, null);
  }

  @Override
  public void checkForKeystore() {
    try {
      keyStore = KeyStore.getInstance(ANDROID_KEYSTORE);
      keyStore.load(null);
    } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
      e.printStackTrace();
    }
    try {
      if (!keyStore.containsAlias(KEY_PRIVATEALIAS)) {
        //mSignUpView.checkForVersion();
      }
    } catch (KeyStoreException e) {
      e.printStackTrace();
    }
  }

}
