package com.vaidg.jobDetailsStatus;

/**
 * <h2>TimeDuration</h2>
 * Created by Ali on 1/12/2018.
 */

public interface TimeDuration
{
    void onDistanceTime(String timeEta, String distance);
}