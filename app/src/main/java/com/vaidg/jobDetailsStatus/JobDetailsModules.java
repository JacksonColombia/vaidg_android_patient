package com.vaidg.jobDetailsStatus;

import com.vaidg.Dagger2.ActivityScoped;
import com.vaidg.Dagger2.FragmentScoped;
import com.vaidg.lspapplication.MessagesFragment;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * <h2>JobDetailsModules</h2>
 * Created by Ali on 2/15/2018.
 */
@Module
public interface JobDetailsModules
{

    @FragmentScoped
    @ContributesAndroidInjector
    JobPostedFrag provideJobPosted();

    @FragmentScoped
    @ContributesAndroidInjector
    ProviderHiredFragment provideHiredFragment();

    @FragmentScoped
    @ContributesAndroidInjector
    ProviderHiredFragmentIn provideHiredFragmentIn();


    @FragmentScoped
    @ContributesAndroidInjector
    MessagesFragment provideMessageFragment();

    @FragmentScoped
    @ContributesAndroidInjector
    ProviderOnTheWayFrag provideOnTheWayFragment();

    @FragmentScoped
    @ContributesAndroidInjector
    ProviderArrivedFrag provideArrivedFragment();

    @FragmentScoped
    @ContributesAndroidInjector
    JobStartedFrag provideJobStartedFragment();

    @Binds
    @ActivityScoped
    JobDetailsContract.Presenter providePresenter(JobDetailsContractImpl jobDetailsContract);

    @Binds
    @ActivityScoped
    JobDetailsContract.JobView provideView(JobDetailsActivity jobDetailsActivity);

    @Binds
    @ActivityScoped
    JobDetailsOnTheWayContract.JobDetailsOnTheWayPresenter provideJobDetailsOnTheWayPresenter(JobDetailsOnTheWayContractImpl presenter);

    @Binds
    @ActivityScoped
    JobDetailsOnTheWayContract.CancelBooking provideCancelBooking(CancelBookingImpl cancelBooking);
}
