package com.vaidg.jobDetailsStatus;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.pojo.CancelReasonPojo;
import com.pojo.ErrorHandel;
import com.utility.AlertProgress;
import com.utility.CalendarEventHelper;
import com.vaidg.R;
import com.vaidg.home.MainActivity;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import adapters.CancelAdapter;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.utilities.Constants.MESSAGE;

/**
 * <h2>CancelBookingImpl</h2>
 * Created by Ali on 3/14/2018.
 */

public class CancelBookingImpl implements JobDetailsOnTheWayContract.CancelBooking {

    @Inject
    SessionManagerImpl manager;
    @Inject
    LSPServices lspServices;
    @Inject
    Gson gson;
    @Inject
    AppTypeface appTypeface;
    String reminderId = "";
    @Inject
    AlertProgress alertProgress;
    private long bid;
    private boolean isFromPayment = false;
    private Context mContext;
    private ProgressDialog pDialog;
    private Dialog indialog;
    private int resId = -1;

    @Inject
    public CancelBookingImpl() {

    }

    @Override
    public void onToCancelBooking(long bid, Context mContext, String reminderId) {
        this.bid = bid;
        this.mContext = mContext;
        this.reminderId = reminderId;
        pDialog = new ProgressDialog(mContext);
        pDialog.setCancelable(false);
        pDialog.setMessage(mContext.getString(R.string.wait));
        if (alertProgress.isNetworkAvailable(mContext)) {
            pDialog.show();
            callCancelApi();
        } else {
            alertProgress.showNetworkAlert(mContext);
        }
    }

    @Override
    public void onToCancelBooking(long bid, Context mContext, String reminderId, boolean isFromPayment) {
        this.bid = bid;
        this.mContext = mContext;
        this.isFromPayment = isFromPayment;
        this.reminderId = reminderId;
        pDialog = new ProgressDialog(mContext);
        pDialog.setCancelable(false);
        pDialog.setMessage(mContext.getString(R.string.wait));
        if (alertProgress.isNetworkAvailable(mContext)) {
            pDialog.show();
            callCancelApi();
        } else {
            alertProgress.showNetworkAlert(mContext);
        }
    }

    @Override
    public void onSelectedReason(int res_id) {
        resId = res_id;
    }

    private void callCancelApi() {

        Log.d("TAG", "callCancelApi: " + bid);
        Observable<Response<ResponseBody>> observable = lspServices.onCancelReasons(LSPApplication.getInstance().getAuthToken(manager.getSID()),
                Constants.selLang, Constants.PLATFORM_ANDROID, bid + "");

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {

                        int code = responseBodyResponse.code();
                        Log.d("TAG", "onNext: " + code);

                        try {
                            String response =
                                    responseBodyResponse.body() != null ? responseBodyResponse.body().string()
                                            : null;
                            switch (code) {
                                case Constants.SUCCESS_RESPONSE:
                                    if (response != null && !response.isEmpty()) {
                                        Log.d("TAG", "onNextCancelReason: " + response);
                                        CancelReasonPojo cancel = gson.fromJson(response, CancelReasonPojo.class);
                                        cancelReasonDialog(cancel.getData().getReason(),
                                                cancel.getData().isCancellationFeeApplied()
                                                , cancel.getData().getCancellationFee(), cancel.getMessage());//, isPending
                                    }
                                    break;
                                case Constants.SESSION_LOGOUT:
                                    break;
                                case Constants.SESSION_EXPIRED:
                                    break;
                                default:

                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        if (pDialog != null && pDialog.isShowing()) {
                            pDialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (pDialog != null && pDialog.isShowing()) {
                            pDialog.dismiss();
                        }
                        alertProgress.alertinfo(mContext, e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void cancelReasonDialog(ArrayList<CancelReasonPojo.CancelReasonData> reason,
                                    boolean cancellationFeeApplied, double cancellationFee, String message) {


        if (cancellationFeeApplied) {
            alertProgress.alertPositiveNegativeOnclick(mContext, message,
                    mContext.getResources().getString(R.string.cancel),
                    mContext.getResources().getString(R.string.ok),
                    mContext.getResources().getString(R.string.cancel), false, isClicked -> {
                        if (isClicked) {
                            showDialog(reason);
                        }
                    });
        } else {
            showDialog(reason);
        }

    }

    private void showDialog(ArrayList<CancelReasonPojo.CancelReasonData> reason) {

        indialog = new Dialog(mContext);
        indialog.setCanceledOnTouchOutside(!isFromPayment);
        indialog.setCancelable(!isFromPayment);
        indialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        indialog.setContentView(R.layout.cancel_dialog);
        indialog.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        indialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        RecyclerView listcancel = indialog.findViewById(R.id.listcancel);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        TextView cancelDialogReson = indialog.findViewById(R.id.cancelDialogReson);
        ImageView cancelDialog = indialog.findViewById(R.id.cancelDialog);
        TextView submitcancel = indialog.findViewById(R.id.submitcancel);
        cancelDialogReson.setTypeface(appTypeface.getHind_medium());
        submitcancel.setTypeface(appTypeface.getHind_semiBold());
        cancelDialog.setVisibility(isFromPayment ? View.GONE : View.VISIBLE);
        cancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indialog.dismiss();
            }
        });
        final CancelAdapter cancelAdapter = new CancelAdapter(reason, CancelBookingImpl.this);
        listcancel.setLayoutManager(linearLayoutManager);
        listcancel.setAdapter(cancelAdapter);

        submitcancel.setOnClickListener(v -> {
            if (resId != -1) {
                if (alertProgress.isNetworkAvailable(mContext)) {

                    cancelAppService(resId);
                    indialog.dismiss();
                } else {
                    alertProgress.showNetworkAlert(mContext);
                }

            } else {
                Toast.makeText(mContext, "Please provide valid reason", Toast.LENGTH_SHORT).show();
            }
        });

        indialog.show();
    }

    private void cancelAppService(int resId) {

        pDialog.show();
        Map<String, Object> jsonParams = new HashMap<>();
        jsonParams.put("bookingId", bid);
        jsonParams.put("resonId", resId);

        Observable<Response<ResponseBody>> observable = lspServices.cancelBooking(LSPApplication.getInstance().getAuthToken(manager.getSID())
                , Constants.selLang, Constants.PLATFORM_ANDROID, jsonParams);
        Log.d("SHIJEN",
                "cancelAppService: cancelApiService auth:" + LSPApplication.getInstance().getAuthToken(manager.getSID()) + " bid: " + bid + "resId:"
                        + resId);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int code = responseBodyResponse.code();
                        JSONObject jsonObject;
                        Log.d("TAG", "onNextintCode: " + code);

                        try {
                            String response = responseBodyResponse.body() != null
                                    ? responseBodyResponse.body().string() : null;
                            switch (code) {
                                case Constants.SUCCESS_RESPONSE:
                                    if (response != null && !response.isEmpty()) {
                                        Log.d("TAG", "onNextintResponse: "
                                                + responseBodyResponse.body().string());
                                        jsonObject = new JSONObject(response);
                                        if (jsonObject.getString(MESSAGE) != null && !jsonObject.getString(
                                                MESSAGE).isEmpty()) {
                                            cancelResponse(jsonObject.getString(MESSAGE));
                                        }

                                        if (!"".equals(reminderId) && reminderId != null) {
                                            removeReminder(bid, reminderId);
                                        }
                                    }
                                    break;
                                case Constants.SESSION_LOGOUT:
                                    break;
                                case Constants.SESSION_EXPIRED:
                                    break;
                                default:
                                    if (response != null && !response.isEmpty()) {
                                        ErrorHandel errorHandel = gson.fromJson(response,
                                                ErrorHandel.class);
                                        alertProgress.alertinfo(mContext, errorHandel.getMessage());
                                    }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (pDialog != null && pDialog.isShowing()) {
                            pDialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (pDialog != null && pDialog.isShowing()) {
                            pDialog.dismiss();
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void removeReminder(long bid, String reminderId) {

        CalendarEventHelper calendarEventHelper = new CalendarEventHelper(mContext);
        calendarEventHelper.deleteEvent(Long.parseLong(reminderId));
       /* Observable<Response<ResponseBody>> observable = lspServices.reminderEvent(manager
       .getAUTH(),
                ConstantsInteface.selLang,bid,0);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        Log.d("TAG", "onNext: "+responseBodyResponse.code());
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });*/
    }

    private void cancelResponse(String message) {


        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getString(R.string.cancel));
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton(mContext.getString(R.string.ok), (dialog, which) -> {
            Constants.isJobDetailsOpen = false;

            Intent intent = new Intent(mContext, MainActivity.class);
           // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            mContext.startActivity(intent);
            dialog.dismiss();
        });
        builder.show();
    }
}
