package com.vaidg.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;

import com.vaidg.BuildConfig;
import com.vaidg.R;
import com.vaidg.videocalling.UtilityVideoCall;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import eu.janmuller.android.simplecropimage.CropImage;

import static android.os.Build.VERSION_CODES.N;
import static com.vaidg.BuildConfig.PARENT_FOLDER;


//import com.notary90210.interfaceMgr.ImageUploadedAmazon;

/**
 * <h2>HandlePictureEvents</h2>
 * this class open the popup for the option to take the image
 * after it takes the the, it crops the image
 * and then upload it to amazon
 * Created by ${Ali} on 8/17/2017.
 */

public class HandlePictureEvents {
    public File newFile;
    UploadAmazonS3 upload;
    private Activity mcontext = null;
    private String takenNewImage;
    private Fragment fragment = null;
    private String amazonFileName = "";

    public HandlePictureEvents(Activity mcontext, Fragment fragment) {
        this.fragment = fragment;
        this.mcontext = mcontext;
        initializeAmazon();
    }

    public HandlePictureEvents(Activity mcontext) {
        this.mcontext = mcontext;
        initializeAmazon();
    }

    private void initializeAmazon() {
        upload = UploadAmazonS3.getInstance(mcontext);

    }

    /**
     * <h2>openDialog</h2>
     * this dialog have the option to choose whether to take picture
     * or open gallery or cancel the dialog
     */
    public void openDialog() {
        takenNewImage = "DayRunner" + System.nanoTime() + ".png";
        CreateOrClearDirectory directory = CreateOrClearDirectory.getInstance();
        newFile = directory.getAlbumStorageDir(mcontext, PARENT_FOLDER + "/Profile_Pictures", false);
        final Resources resources = mcontext.getResources();
        final CharSequence[] options = {resources.getString(R.string.takephoto), resources.getString(R.string.choose_from_gallery), resources.getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(mcontext);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(resources.getString(R.string.takephoto))) {
                        takePicFromCamera();
                } else if (options[item].equals(resources.getString(R.string.choose_from_gallery))) {
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    //photoPickerIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    if (fragment != null)
                        fragment.startActivityForResult(photoPickerIntent, Constants.GALLERY_PIC);
                    else
                        mcontext.startActivityForResult(photoPickerIntent, Constants.GALLERY_PIC);
                } else if (options[item].equals(resources.getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void openDialogPicAncDoc(boolean isFromCall) {
        takenNewImage = "DayRunner" + System.nanoTime() + ".png";
        CreateOrClearDirectory directory = CreateOrClearDirectory.getInstance();
        final Resources resources = mcontext.getResources();
        final CharSequence[] options = {resources.getString(R.string.takephoto), resources.getString(R.string.choose_from_gallery), resources.getString(R.string.choose_from_document), resources.getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(mcontext);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(resources.getString(R.string.takephoto))) {
                    if(isFromCall) {
                        Toast.makeText(mcontext, "can't use camera while using VaidG video call", Toast.LENGTH_SHORT).show();
                    }else{
                        newFile = directory.getAlbumStorageDir(mcontext, PARENT_FOLDER + "/Profile_Pictures", false);
                        takePicFromCamera();
                    }
                } else if (options[item].equals(resources.getString(R.string.choose_from_gallery))) {
                    newFile = directory.getAlbumStorageDir(mcontext, PARENT_FOLDER + "/Profile_Pictures", false);
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    //photoPickerIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    if (fragment != null)
                        fragment.startActivityForResult(photoPickerIntent, Constants.GALLERY_PIC);
                    else
                        mcontext.startActivityForResult(photoPickerIntent, Constants.GALLERY_PIC);
                } else if (options[item].equals(resources.getString(R.string.choose_from_document))) {
                    newFile = directory.getAlbumStorageDirDoc(mcontext, PARENT_FOLDER + "/Profile_Pictures", false);
                    //   Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                    //   chooseFile.setType("*/*");
                    String[] mimeTypes =
                            {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                                    "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                                    "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                                    "text/plain",
                                    "application/pdf"};

                    Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                    chooseFile.addCategory(Intent.CATEGORY_OPENABLE);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        chooseFile.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
                        if (mimeTypes.length > 0) {
                            chooseFile.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                        }
                    } else {
                        String mimeTypesStr = "";
                        for (String mimeType : mimeTypes) {
                            mimeTypesStr += mimeType + "|";
                        }
                        chooseFile.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
                    }
                    //photoPickerIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    if (fragment != null)
                        fragment.startActivityForResult(chooseFile, Constants.PICKFILE_RESULT_CODE);
                    else
                        mcontext.startActivityForResult(chooseFile, Constants.PICKFILE_RESULT_CODE);
                } else if (options[item].equals(resources.getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    /**
     * <h2>takePicFromCamera</h2>
     * This method is got called, when user chooses to take photos from camera.
     */
    public void takePicFromCamera() {
        String state;
        Log.i("1", "takePicFromCamera: ");
        try {
            Log.i("2", "takePicFromCamera: ");

            Uri newProfileImageUri;
            takenNewImage = "";
            state = Environment.getExternalStorageState();
            takenNewImage = "takenNewImage" + System.nanoTime() + ".png";
            if (Environment.MEDIA_MOUNTED.equals(state))
                newFile = new File(mcontext.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + PARENT_FOLDER + "/Profile_Pictures/", takenNewImage);
            else
                newFile = new File(mcontext.getFilesDir() + "/" + PARENT_FOLDER + "/Profile_Pictures/", takenNewImage);
            if (Build.VERSION.SDK_INT >= N)
                newProfileImageUri = FileProvider.getUriForFile(mcontext, BuildConfig.APPLICATION_ID + ".provider", newFile);
            else
                newProfileImageUri = Uri.fromFile(newFile);

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, newProfileImageUri);
            intent.putExtra("return-data", true);
            //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            if (fragment != null)
                fragment.startActivityForResult(intent, Constants.CAMERA_PIC);
            else
                mcontext.startActivityForResult(intent, Constants.CAMERA_PIC);
        } catch (ActivityNotFoundException e) {
            Log.d("TAG", "takePicFromCamera: " + e);
        }
    }

    /**
     * This method got called when cropping starts done.
     *
     * @param newFile image file to be cropped
     */
    public File startCropImage(File newFile) {

        Log.d("TAG", "startCropImage: " + newFile + " CROP " + this.newFile.getPath());
        Intent intent = new Intent(mcontext, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, this.newFile.getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 4);
        intent.putExtra(CropImage.ASPECT_Y, 4);
        //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        if (fragment != null)
            fragment.startActivityForResult(intent, Constants.CROP_IMAGE);
        else
            mcontext.startActivityForResult(intent, Constants.CROP_IMAGE);
        return newFile;
    }

    /**
     * <h2>gallery</h2>
     * This method is got called, when user chooses to take photos from camera.
     *
     * @param data uri data given by gallery
     */
    public File gallery(Uri data) {

        try {

            String state = Environment.getExternalStorageState();
            takenNewImage = "takenNewImage" + System.nanoTime() + ".png";
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                newFile = new File(mcontext.getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/" + PARENT_FOLDER + "/Profile_Pictures/", takenNewImage);
            } else {
                newFile = new File(mcontext.getFilesDir() + "/" + PARENT_FOLDER + "/Profile_Pictures/", takenNewImage);
            }
            InputStream inputStream = mcontext.getContentResolver().openInputStream(data);
            FileOutputStream fileOutputStream = new FileOutputStream(newFile);
            Utility.copyStream(inputStream, fileOutputStream);
            fileOutputStream.close();
            inputStream.close();
            // newProfileImageUri = Uri.fromFile(newFile);
            startCropImage(newFile);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return newFile;
    }

    public File doc(Uri data, String mimeType) {

        try {

            String state = Environment.getExternalStorageState();
            String fileName = data.getPath().substring( data.getPath().lastIndexOf('/')+1, data.getPath().length() );
            String fileNameWithoutExtn ="";
            if(fileName.contains("."))
             fileNameWithoutExtn = fileName.substring(0, fileName.lastIndexOf('.'));
            else
                fileNameWithoutExtn = fileName;
            switch (mimeType) {
                case "pdf":
                    takenNewImage = fileNameWithoutExtn + ".pdf";
                    break;
                case "doc":
                    takenNewImage = fileNameWithoutExtn + ".doc";
                    break;
                case "docx":
                    takenNewImage = fileNameWithoutExtn + ".docx";
                    break;
                case "dot":
                    takenNewImage = fileNameWithoutExtn + ".dot";
                    break;
                case "dotx":
                    takenNewImage = fileNameWithoutExtn + ".dotx";
                    break;
                case "ppt":
                    takenNewImage = fileNameWithoutExtn + ".ppt";
                    break;
                case "pptx":
                    takenNewImage = fileNameWithoutExtn + ".pptx";
                    break;
                case "xls":
                    takenNewImage = fileNameWithoutExtn + ".xls";
                    break;
                case "xlsx":
                    takenNewImage = fileNameWithoutExtn + ".xlsx";
                    break;
                case "txt":
                    takenNewImage = fileNameWithoutExtn + ".txt";
                    break;
                default:
                    takenNewImage = fileNameWithoutExtn + ".pdf";
                    break;
            }
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                newFile = new File(mcontext.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) + "/" + PARENT_FOLDER + "/Profile_Pictures/", takenNewImage);
            } else {
                newFile = new File(mcontext.getFilesDir() + "/" + PARENT_FOLDER + "/Profile_Pictures/", takenNewImage);
            }
            InputStream inputStream = mcontext.getContentResolver().openInputStream(data);
            FileOutputStream fileOutputStream = new FileOutputStream(newFile);
            Utility.copyStream(inputStream, fileOutputStream);
            fileOutputStream.close();
            inputStream.close();
            // newProfileImageUri = Uri.fromFile(newFile);
            // startCropImage(newFile);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return newFile;
    }

    /**
     * <h2>uploadToAmazon</h2>
     * This method is used to upload the image on AMAZON bucket.
     *
     * @param image       image file to be uploaded
     * @param imageupload interface call back for the update of profile on the server
     */
    public void uploadToAmazon(Context context, String bucketName, File image, final ImageUploadedAmazon imageupload) {
        upload.Upload_data(context, bucketName, image, new UploadAmazonS3.Upload_CallBack() {
            @Override
            public void sucess(String sucess) {
                imageupload.onSuccessAdded(sucess);
            }

            @Override
            public void error(String errormsg) {
                imageupload.onerror(errormsg);
            }
        });
    }


}
