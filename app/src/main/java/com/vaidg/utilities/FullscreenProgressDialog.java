package com.vaidg.utilities;

import static android.content.Context.DOWNLOAD_SERVICE;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.DownloadListener;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.vaidg.R;
import com.vaidg.rateYourBooking.RateYourBooking;
import com.vaidg.rateYourBooking.adapter.PrescriptionAdapter;
import com.google.android.material.snackbar.Snackbar;
import com.pojo.InvoiceDetails;
import com.utility.PicassoTrustAll;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class FullscreenProgressDialog extends BaseFullscreenDialog {

  private final static String TAG = "ProgressDialog";
  private static final int MY_PERMISSION_REQUEST_CODE = 123;
  private final float DEFAULT_DIM_AMOUNT = 0.7f;
  private final int DISMISS_DELAY = 100; // ms
  final private Handler mHandler = new Handler();
  private String mMessage;
  private TextView tvMessage;
  private Runnable mRunnable;
  private float mDimAmount = DEFAULT_DIM_AMOUNT;
  private int mTintColor = R.color.colorAccent;
  private String pdfFile = "";
  private String categoryName = "";
  private String docDegree = "";
  private String name = "";
  private String profilePic = "";
  private long mDateAndTime = 0;
  private ArrayList<InvoiceDetails.Medication> medications = new ArrayList<>();
  private Context mContext;
  private boolean loadingFinished = true;
  private boolean redirect = false;
  private TextView tv_skip;

  public static FullscreenProgressDialog dialogWithCustomMessage(String message) {
    FullscreenProgressDialog progressDialog = new FullscreenProgressDialog();
    progressDialog.mMessage = message;
    return progressDialog;
  }

  public Context getmContext() {
    return mContext;
  }

  public void setmContext(Context mContext) {
    this.mContext = mContext;
  }

  public String getProfilePic() {
    return profilePic;
  }

  public void setProfilePic(String profilePic) {
    this.profilePic = profilePic;
  }

  public String getDocDegree() {
    return docDegree;
  }

  public void setDocDegree(String docDegree) {
    this.docDegree = docDegree;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public ArrayList<InvoiceDetails.Medication> getMedications() {
    return medications;
  }

  public void setMedications(ArrayList<InvoiceDetails.Medication> medications) {
    this.medications = medications;
  }

  public long getmDateAndTime() {
    return mDateAndTime;
  }

  public void setmDateAndTime(long mDateAndTime) {
    this.mDateAndTime = mDateAndTime;
  }

  public float getDimAmount() {
    return mDimAmount;
  }

  public void setDimAmount(float dimAmount) {
    mDimAmount = dimAmount;
  }

  public int getTintColor() {
    return mTintColor;
  }

  public void setTintColor(int tintColor) {
    mTintColor = tintColor;
  }

  public String getPdfFile() {
    return pdfFile;
  }

  public void setPdfFile(String pdfFile) {
    this.pdfFile = pdfFile;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fullscreen_progress_dialog, container, false);

    mContext = getmContext();
    setCancelable(true);
    getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
    getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    getDialog().getWindow().setDimAmount(DEFAULT_DIM_AMOUNT);
    getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    // Get the widget reference from xml layout
    RelativeLayout mRootLayout = view.findViewById(R.id.root_layout);
    WebView mWebViewDownload = view.findViewById(R.id.web_view);
    ImageView ivMyEventProPic = view.findViewById(R.id.ivMyEventProPic);
    TextView tvMyEventProName = view.findViewById(R.id.tvMyEventProName);
    TextView tvMyEvnetProStatus = view.findViewById(R.id.tvMyEvnetProStatus);
    TextView tvMytimedate = view.findViewById(R.id.tvMytimedate);
    TextView tvNoPrescription = view.findViewById(R.id.tvNoPrescription);
    RecyclerView rvPrecription = view.findViewById(R.id.rvPrecription);
    Toolbar toolPrecription = view.findViewById(R.id.toolbar);
    TextView tv_center = view.findViewById(R.id.tv_center);
    tv_skip = view.findViewById(R.id.tv_skip);
    AppTypeface appTypeface = AppTypeface.getInstance(mContext);
    tvMyEventProName.setTypeface(appTypeface.getHind_semiBold());
    tv_center.setTypeface(appTypeface.getHind_semiBold());
    tvMyEvnetProStatus.setTypeface(appTypeface.getHind_regular());
    tv_skip.setTypeface(appTypeface.getHind_semiBold());
    tvMytimedate.setTypeface(appTypeface.getHind_regular());

    tvMyEventProName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_18));
    tvMyEvnetProStatus.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_16));
    tvMytimedate.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_14));
    // tvMedication.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_14));

    ((AppCompatActivity) mContext).setSupportActionBar(toolPrecription);
    if (((AppCompatActivity) mContext).getSupportActionBar() != null) {
      ((AppCompatActivity) mContext).getSupportActionBar().setDisplayShowHomeEnabled(false);
      ((AppCompatActivity) mContext).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
      ((AppCompatActivity) mContext).getSupportActionBar().setDisplayShowTitleEnabled(false);
      toolPrecription.setNavigationIcon(R.drawable.ic_back);
      toolPrecription.setNavigationOnClickListener(v -> dismiss());
    }

    tv_center.setText(getString(R.string.prescription));
    tv_skip.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_printer, 0, 0, 0);
    tv_skip.setText(getResources().getString(R.string.print));
    tv_skip.setGravity(Gravity.CENTER_VERTICAL);
    tv_skip.setCompoundDrawablePadding(15);
    tv_skip.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
    tv_skip.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
    try {

      Date date = new Date(TimeUnit.SECONDS.toMillis(getmDateAndTime()));
      tvMytimedate.setText(Utility.getFormattedDate(date));
    } catch (Exception e) {
      Log.d("TAG", "timeMethodException: " + e.toString());
    }
    if (getProfilePic() != null && !getProfilePic().isEmpty()) {
      PicassoTrustAll.getInstance(mContext)
              .load(getProfilePic())
              .placeholder(R.drawable.register_profile_default_image)   // optional
              .error(R.drawable.register_profile_default_image)// optional
              .transform(new PicassoCircleTransform())
              .into(ivMyEventProPic);

    }
    tvMyEventProName.setText(getName());
    String docDesc = "";
    if (getDocDegree() != null &&getDocDegree().isEmpty()) {
      docDesc = getCategoryName();
    } else if (getCategoryName() != null && getCategoryName().isEmpty()) {
      docDesc = getDocDegree();
    } else {
      docDesc = getDocDegree() + "," + getCategoryName();
    }
    tvMyEvnetProStatus.setText(docDesc);

    // Check permission for write external storage
    //checkPermission();
    if(medications != null && medications.size() > 0)
      tvNoPrescription.setVisibility(View.GONE);
    else
      tvNoPrescription.setVisibility(View.VISIBLE);
    LinearLayoutManager llManager = new LinearLayoutManager(mContext);
    rvPrecription.setLayoutManager(llManager);
    PrescriptionAdapter prescriptionAdapter = new PrescriptionAdapter(medications);
    rvPrecription.setAdapter(prescriptionAdapter);
    // The target url to surf using web view
    String url = getPdfFile();
    Log.w(TAG, "onCreateView: " + url);
    tv_skip.setOnClickListener(v -> {
      if(url != null && !url.isEmpty()) {
        if(Utility.isNetworkAvailable(mContext))
          mContext.startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse(url)));
        else
          Toast.makeText(mContext, "Please check your internetConnection", Toast.LENGTH_SHORT).show();

       /* FullscreenWebViewDialog fullscreenWebViewDialog = new FullscreenWebViewDialog();
        fullscreenWebViewDialog.setDimAmount(0);
        fullscreenWebViewDialog.setPdfFile(pdfFile);
        fullscreenWebViewDialog.setmContext(mContext);
        fullscreenWebViewDialog.setName(name);
        fullscreenWebViewDialog.setProfilePic(profilePic);
        fullscreenWebViewDialog.setMedications(medications);
        fullscreenWebViewDialog.setCategoryName(categoryName);
        fullscreenWebViewDialog.setmDateAndTime(mDateAndTime);
        fullscreenWebViewDialog.setDocDegree(docDegree);
        fullscreenWebViewDialog.show((AppCompatActivity) mContext);*/
      }else{
        Toast.makeText(mContext, "There is no pdf file for printing", Toast.LENGTH_SHORT).show();
      }
    });

    return view;
  }

  public static String getMimeType(Context context, Uri uri) {
    String extension;

    //Check uri format to avoid null
    if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
      //If scheme is a content
      final MimeTypeMap mime = MimeTypeMap.getSingleton();
      extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
    } else {
      //If scheme is a File
      //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
      extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());

    }

    return extension;
  }


  public void setMessage(String message) {
    if (message != null && tvMessage != null) {
      tvMessage.setText(message);
    }
  }

  public void show(int delay, final AppCompatActivity activity) {
    mRunnable = new Runnable() {
      @Override
      public void run() {
        show(activity);
      }
    };
    mHandler.postDelayed(mRunnable, delay);
  }

  public void show(AppCompatActivity activity) {
    if (activity != null && !activity.isFinishing() && !isShowing() && !isAdded()) {
      super.show(activity.getSupportFragmentManager(), TAG);
    }
  }

  public void show(Fragment fragment) {
    if (fragment != null) {
      FragmentManager fragmentManager = fragment.getFragmentManager();
      if (fragmentManager != null && !fragmentManager.isDestroyed() && !isShowing() || !isAdded()) {
        super.show(fragmentManager, TAG);
      }
    }
  }

  public boolean isShowing() {
    Dialog dialog = getDialog();
    return dialog != null && dialog.isShowing();
  }

  @Override
  public void dismiss() {
    if (mRunnable != null) {
      mHandler.removeCallbacks(mRunnable);
      mRunnable = null;
    }

    mHandler.postDelayed(new Runnable() {
      @Override
      public void run() {
        if (isShowing()) {
          try {
            FullscreenProgressDialog.super.dismissAllowingStateLoss();
          } catch (IllegalStateException ignored) {

          }
        }
      }
    }, DISMISS_DELAY);
  }


  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    switch (requestCode) {
      case 2:
        Log.d(TAG, "External storage2");
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
          //resume tasks needing this permission
          tv_skip.performClick();
        } else {
          //progress.dismiss();
          ActivityCompat.requestPermissions(((Activity) mContext), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        }
        break;

      case 3:
        Log.d(TAG, "External storage1");
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
          //resume tasks needing this permission
          tv_skip.performClick();
        } else {
          // progress.dismiss();
          ActivityCompat.requestPermissions(((Activity) mContext), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);

        }
        break;
    }
  }

  public boolean isReadStoragePermissionGranted() {
    if (Build.VERSION.SDK_INT >= 23) {
      if (mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
          == PackageManager.PERMISSION_GRANTED) {
        Log.v(TAG, "Permission is granted1");
        return true;
      } else {

        Log.v(TAG, "Permission is revoked1");
        ActivityCompat.requestPermissions(((Activity) mContext), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
        return false;
      }
    } else { //permission is automatically granted on sdk<23 upon installation
      Log.v(TAG, "Permission is granted1");
      return true;
    }
  }

  public boolean isWriteStoragePermissionGranted() {
    if (Build.VERSION.SDK_INT >= 23) {
      if (mContext.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
          == PackageManager.PERMISSION_GRANTED) {
        Log.v(TAG, "Permission is granted2");
        return true;
      } else {

        Log.v(TAG, "Permission is revoked2");
        ActivityCompat.requestPermissions(((Activity) mContext), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        return false;
      }
    } else { //permission is automatically granted on sdk<23 upon installation
      Log.v(TAG, "Permission is granted2");
      return true;
    }
  }

}
