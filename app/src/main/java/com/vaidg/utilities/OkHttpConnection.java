package com.vaidg.utilities;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class OkHttpConnection
{
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public static void requestOkHttpConnection(String requestUrl, Request_type requestType, JSONObject jsonRequest, JsonResponceCallback jsonCallback)
    {
        OkHttpRequestData data = new OkHttpRequestData();
        data.requestUrl = requestUrl;
        data.requestType = requestType;
        data.jsonRequest = jsonRequest;
        data.jsonCallback = jsonCallback;
      //  data.token = token;
      //  data.lan = lan;
        Log.d("SHIJENTEST", "requestOkHttpConnection: "+data);
      //  Log.d("SHIJENTEST", "requestOkHttpConnection: "+jsonRequest+ " "+token);
        new OKHttpJsonRequest().execute(data);
    }

    public static void printLog(String message) {
        Log.d("OKHTTPCONNECTION", message);
    }

    /**
     * <h2></h2>
     */
    private static String getUrl(String url, JSONObject jsonObject) {
        String service_url = url + "?";
        String query = "";
        Iterator<String> object_keys = jsonObject.keys();
        try {
            while (object_keys.hasNext()) {
                String keys_value = object_keys.next();
                query = query + keys_value + "=" + jsonObject.getString(keys_value) + "&";
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("Vallue", service_url + query);

        return service_url + query;
    }


    /**
     * <H3>Request_type</H3>
     * <p>
     * this Enum tell which is the request type we have
     * </p>
     */
    public enum Request_type {
        GET("getRequest"),
        URl("urlRequest"),
        POST("postRequest"),
        DELETE("deleteRequest"),
        PUT("putRequest"),
        PATCH("patchRequest");
        public String value;

        Request_type(String value) {
            this.value = value;
        }
    }

    /**************************Callback Interface***************************/

    public interface JsonResponceCallback {
        /**
         * Called When Success result of JSON request
         *
         * @param result
         */
        // void onSuccess(String result);
        void onSuccess(String headerData, String result);


        /**
         * Called When Error result of JSON request
         *
         * @param error error on the responce
         */
        void onError(String headerError, String error);

    }

    private static class OkHttpRequestData
    {
        String requestUrl,token;
        Request_type requestType;
        JSONObject jsonRequest;
        JsonResponceCallback jsonCallback;
        String lan;

    }

    @SuppressLint("NewApi")
    private static class OKHttpJsonRequest extends AsyncTask<OkHttpRequestData, Void, String[]>
            // private static class OKHttpJsonRequest extends AsyncTask<OkHttpRequestData,Void,String>
    {

        JsonResponceCallback jsonCallback;
        boolean error =false;

        @Override
        protected void onPreExecute()
        {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected String[] doInBackground(OkHttpRequestData... params)
        //  protected String doInBackground(OkHttpRequestData... params)
        {
            jsonCallback = params[0].jsonCallback;
            String header = null;
            String result;
            try
            {
                Request request = null;
                OkHttpClient client = OkHttpInstance.getInstance();         //This is the OkHttp3Client connection.
                client.hostnameVerifier();

                if(params[0].requestType.equals(Request_type.URl))
                {
                    Log.d("TAG","RequestUrl "+params[0].requestType);
                    String url = getUrl(params[0].requestUrl,params[0].jsonRequest);
                    request = new Request.Builder()
                            .url(url)
                            .addHeader("authorization", params[0].token)
                            .addHeader("lan",params[0].lan+"")
                            .build();
                }
                else if(params[0].requestType.equals(Request_type.GET))
                {
                    RequestBody body = RequestBody.create(JSON, params[0].jsonRequest.toString());
                    request = new Request.Builder()
                            .url(params[0].requestUrl)
                            /*.addHeader("authorization", params[0].token)
                            .addHeader("lan",params[0].lan+"")*/
                            .put(body)
                            .get()
                            .build();
                }
                else if(params[0].requestType.equals(Request_type.POST))
                {

                    RequestBody body = RequestBody.create(JSON, params[0].jsonRequest.toString());
                    request = new Request.Builder()
                            .url(params[0].requestUrl)
                            .addHeader("authorization", params[0].token)
                            .addHeader("lan",params[0].lan+"")
                            .post(body)
                            .build();
                }
                else if(params[0].requestType.equals(Request_type.DELETE))
                {
                    RequestBody body = RequestBody.create(JSON, params[0].jsonRequest.toString());
                    request = new Request.Builder()
                            .url(params[0].requestUrl)
                            .addHeader("authorization", params[0].token)
                            .addHeader("lan",params[0].lan+"")
                            .delete(body)
                            .build();
                }
                else if(params[0].requestType.equals(Request_type.PUT))
                {
                    RequestBody body = RequestBody.create(JSON, params[0].jsonRequest.toString());
                    request = new Request.Builder()
                            .url(params[0].requestUrl)
                            .addHeader("authorization", params[0].token)
                            .addHeader("lan",params[0].lan+"")
                            .put(body)
                            .build();
                }
                else if(params[0].requestType.equals(Request_type.PATCH))
                {
                    RequestBody body = RequestBody.create(JSON, params[0].jsonRequest.toString());
                    request = new Request.Builder()
                            .url(params[0].requestUrl)
                            .addHeader("authorization", params[0].token)
                            .addHeader("lan", params[0].lan + "")
                            .patch(body)
                            .build();
                }
                Response response = client.newCall(request).execute();

               /* Headers responseHeaders = response.headers();
                JSONObject headerJson=new JSONObject();
                for (int i = 0; i < responseHeaders.size(); i++)
                {
                    headerJson.put(responseHeaders.name(i),responseHeaders.value(i));
                }
                header=headerJson.toString();*/
                header = response.code() + "";
                result = response.body().string();
                Log.d("OKHTTP ", "doInBackground: " + header + " result " + result);
                if (response.code() != 200)
                {
                    error = true;
                }

                response.body().close();
               /* Log.d(TAG, "doInBackground: "+response.isSuccessful());
                if (!response.isSuccessful())
                {
                    try
                    {
                        result =  response.body().string();
                        header = "549";
                    }catch (Exception e)
                    {
                        result =  e.getMessage();
                        header = "549";
                        e.printStackTrace();
                    }
                }*/
                // if (! response.isSuccessful()) throw new IOException("unexpected code "+ response);
            }catch (Exception e)
            {
                // TODO: handle exception
                //  error= true;
                Log.d("EXCEPTION", "doInBackground: "+ e.getMessage());
                header = "549";
                error=true;
                result = e.getMessage();
                e.printStackTrace();
            }
            return new String[]{header, result};
            // return result;
        }

        @Override
        protected void onPostExecute(String[] result) {
            //  protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (!error)
                jsonCallback.onSuccess(result[0], result[1]);
            else
                jsonCallback.onError(result[0], result[1]);
        }
    }

    //Singleton Class.
    private static class OkHttpInstance
    {
        private static OkHttpClient okHttpClient = null;
        private OkHttpInstance()
        {
            //to avoid instantiation.
        }

        public static OkHttpClient getInstance()
        {
            HttpLoggingInterceptor httpLoggingInterceptor= new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);

            if (okHttpClient == null) {
                okHttpClient = new OkHttpClient()
                        .newBuilder()
                        .readTimeout(50, TimeUnit.SECONDS)
                        .writeTimeout(50, TimeUnit.SECONDS)
                        .connectTimeout(30,TimeUnit.SECONDS)
                        .addInterceptor(httpLoggingInterceptor)
                        .addNetworkInterceptor(new StethoInterceptor())
                        .hostnameVerifier(new HostnameVerifier() {
                            @Override
                            public boolean verify(String hostname, SSLSession session) {
                                // return false;
                                HostnameVerifier hv =
                                        HttpsURLConnection.getDefaultHostnameVerifier();
                                return hv.verify("api.redapron.com.na", session);
                            }
                        })
                        .build();
            }
            return okHttpClient;
        }
    }



}
