package com.vaidg.utilities;

/**
 * <h2>ImageUploadedAmazon</h2>
 * this is ti check if image is uploaded on the amazon or not
 * Created by ${Ali} on 8/18/2017.
 */

public interface ImageUploadedAmazon
{
    void onSuccessAdded(String image);
    void onerror(String errormsg);
}
