package com.vaidg.utilities;

import android.os.Build;

import com.vaidg.BuildConfig;
import com.vaidg.incalloutcalltelecall.symptom.model.Symptom;
import com.vaidg.incalloutcalltelecall.symptom.model.SymptomCategory;
import com.vaidg.model.CallType;
import com.vaidg.model.Category;
import com.vaidg.model.CityData;
import com.vaidg.model.Offers;
import com.vaidg.incalloutcalltelecall.questionselection.model.SymptomResponsePojo;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;
import javax.crypto.spec.IvParameterSpec;

/**
 * <h2>ConstantsInteface</h2>
 * Created by ${3Embed} on 6/10/17.
 */

public class Constants {
    public static final int WALLETCALL = 15;
    public static String Amazonbucket = BuildConfig.AMAZON_BUCKET_NAME;
    public static String Amazoncognitoid = BuildConfig.AMAZON_POOL_ID;
    public static String AmazonRegion = BuildConfig.AMAZON_REGION;
    public static String AmazoncognitoToken = "";
    public static final String AmazonProfileFolderName = "ProfileImages";
    public static final String AmazonUploadJobImage = "UploadJobImage";
    public static final String AmazonUploadChatImage = "UploadChatImage";
    public static final String AmazonDependentFolderName = "DependentProfileImages";
    public static final String SIGNATURE_PIC_DIR = "Signature";
    public static final String SIGNATURE_UPLOAD = "SignatureUpload";
    public static final String SIGNUPPROFILEIMAGESFOLDER = "SignupProfileImages";
    public static final String JOB_COMPLETED_RAISE_INVOICE = "10";
    public static final String PLAY_STORE_LINK = BuildConfig.ANDROID_PLAYSTORE_LINK  + BuildConfig.APPLICATION_ID;
    public static final String WEB_SITE_LINK = "https://www.appscrip.com/";
    public static final String FACEBOOK_LINK = "https://www.facebook.com/appscrip/";
    public static final String FACEBOOK_PAGE_ID = "appscrip";
    public static final int GALLERY_PIC = 10;
    public static final int PICKFILE_RESULT_CODE = 100;
    public static final int CAMERA_PIC = 11;
    public static final int CROP_IMAGE = 12;
    public static final int GALLERY = 13;
    public static final int CAMERA = 14;
    //Password must contain at least one digit [0-9].
    //Password must contain at least one lowercase Latin character [a-z].
    //Password must contain at least one uppercase Latin character [A-Z].
    //Password must contain at least one special character like ! @ # & ( ).
    //Password must contain a length of at least 7 characters and a maximum of 20 characters.
    //"^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{7,20}$"
    //"(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{7,20}"
    public static final Pattern PASSWORD_PATTERN = Pattern
            .compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{7,20}$");
    public static final String APP_VERSION = BuildConfig.VERSION_NAME;
    public static final String DEVICE_MODEL = Build.MODEL;
    public static final String DEVICE_MAKER = Build.MANUFACTURER;
    public static final int DEVICE_TYPE = 2; //1-iOS , 2-android, 3-web
    public static final String DEVICE_OS_VERSION = Build.VERSION.RELEASE;
    public static final int FILTER_RESULT_CODE = 6;
    public static final int ADDRESS_RESULT_CODE = 14;
    public static final int DEPENDENT_RESULT_CODE = 15;
    public static final int PAYMENT_RESULT_CODE = 151;

    public static final int REPEAT_RESULT_CODE = 152;
    public static final int AMOUNT_RESULT_CODE = 153;
    public static final int TIME_RESULT_CODE = 154;
    public static boolean isFrist = true;
    public static final int SUCCESS_RESPONSE = 200;
    public final static int BAD_REQUEST = 400;
    public final static int NOT_FOUND = 404;
    public static final int SESSION_EXPIRED = 406;
    public static final int SESSION_LOGOUT = 401;
    public final static int INTERNAL_SERVER_ERROR = 500;
    public final static int UNAUTHORIZATION = 498;
    public final static int FORBIDDEN = 403;
    public final static int LENGTH_REQUIRED = 411;
    public final static int PHONENUMBER_NOTREGISTER = 406;
    public final static int EMAIL_NOTREGISTER = 409;
    public final static int EXCEED_PASSWORD_LIMIT = 429;
    // public static final int NOT_ACCEPTABLE = 406;//440
    public static final int SESSION_NoDues = 410;
    public static final int CONFLICT = 409;
    public static final int PRECONDITION_FAILED = 412;
    public static final int REQUEST_CODE_CONTACTS = 65;
    public static final int PICK_CONTACT = 67;
    public static final int ADD_DEPENDENT_CODE = 303;
    public final static int REQUEST_CODE_DEPENDENT_IMAGE = 43;
    public static final String KILOMETER = "Kilometers";
    public static final String METER = "meters";
    public static final String NAUTICAL_MILES = "nauticalMiles";
    public static final boolean isInComingCalls = false;
    public static final String JOB_TIMER_INCOMPLETE = "15";
    public final static int REQ_CODE_FOR_CHCKBOX = 1;
    public final static int REQ_CODE_FOR_IMAGEUPLOAD = 2;
    public final static int REQ_CODE_FOR_DATE_CURRENT_TO_FUTURE = 3;
    public final static int REQ_CODE_FOR_DATE_PAST_TO_CURRENT = 4;
    public final static int REQ_CODE_FOR_TIME = 5;
    public final static int REQ_CODE_FOR_TEXT_AREA_COMMASEPARATED = 6;
    public final static int REQ_CODE_FOR_DROPDOWN = 7;
    public final static int REQ_CODE_FOR_VIDEOUPLOAD = 8;
    public final static int REQ_CODE_FOR_NUMBERSLIDER = 9;
    public static final int REQUEST_ID_READ_STORAGE_PERMISSION = 116;
    public static final int REQUEST_ID_GOOGLE_SIGNIN = 66;

    /*Facebook Login*/
    public static final String PUBLIC_PROFILE = "public_profile";
    public static final String ID = "id";
    public static final String FIRSTNAME = "first_name";
    public static final String LASTNAME = "last_name";
    public static final String PICTURE = "picture";
    public static final String NAME = "name";

    /*Permissions */
    public static final int PERMISSION_REQUEST = 1;
    public static final int LOCATION_PERMISSION_CODE = 4;
    public static final int WRITE_STORAGE_PERMISSION_CODE = 8;
    public static final int SMS_PERMISSION_CODE = 6;
    public static final int PERMISSION_GRANTED = 40;
    public static final int PERMISSION_DENIED = 41;
    public static final int PERMISSION_BLOCKED = 42;
    public static final int OREO_PERMISSION = 78;
    /****************************************************
     * doGuestLogin**************************************/

    public static final String REQUEST_ID_TOKEN = "2957797312-7ip9kle57g6du4bedu63u26opt9hanns.apps.googleusercontent.com";
    public static String RELATION_TYPE = "";
    public static final String LAN = "lan";
    public static final String AUTHORIZATION = "authorization";
    public static final String BASE_AUTH_USERNAME = "druidx";
    public static final String BASE_AUTH_PASSWORD = "druidx";
    public static final String PLATFORM = "platform";
    public static final String PLATFORM_ANDROID = "ANDROID";
    public static final String PROVIDER = "PROVIDER";
    public static final int ANDROID_CUSTOMER_USERTYPE = 12;
    public static final String USERTYPE = "userType";
    public static final String REFRESHTOKEN = "refreshToken";
    public static final String DEVICEID = "deviceId";
    public static final String APPVERSION = "appVersion";
    public static final String DEVMAKE = "devMake";
    public static final String DEVICEMODEL = "devModel";
    public static final String DEVTYPE = "devType";
    public static final String DEVICETIME = "deviceTime";
    public static final String DEVICEOSVERSION = "deviceOsVersion";
    public static final String IPADDRESS = "ipAddress";
    /**************************************************** SignUpActivity showProgress
     *  **************************************/
    public static final String DATA = "data";
    public static final String MESSAGE = "message";
    public static final String MANDATORY = "mandatory";
    public static int WAITFORRESPONSEFROMAPIToMQTT = 1;
    /**************************************************** SignUpActivity showProgress
     *  **************************************/
    public static final String EMAIL = "EMAIL";
    public static final String PHONE = "PHONE";
    /**************************************************** SignUpActivity navigateToOtp
     *  **************************************/
    public static final String COMINGFROM = "ComingFrom";
    public static final String MOBILENUMBER = "MobileNumber";
    public static final String COUNTRYCODE = "CountryCode";
    public static final String COUNTRYSYMBOl = "CountrySymbol";
    public static final String SIGNUP = "SignUp";
    public static final String RESEND = "RESEND";
    public static final String OTPVERIFY = "OTP_VERIFY";
    public static final String PHONEVERIFY = "PHONE_VERIFY";
    public static final String EXPIREOTP = "expireOtp";
    public static final String FORGOTPWD = "forgot_pwd";
    public static final String COMMING_FROM = "coming_from";
    /**************************************************** TokenPojo.class
     *  **************************************/
    public static final String ACCESS_EXPIRE_AT = "accessExpireAt";
    public static final String ACCESS_TOKEN = "accessToken";
    public static final String REFRESH_TOKEN = "refreshToken";
    /**************************************************** TokenPojo.class
     *  **************************************/
    public static final int NORMAL_LOGIN = 1;
    public static final int FACEBOOK_LOGIN = 2;
    public static final int GOOGLE_LOGIN = 3;
    /**************************************************** Call.class
     *  **************************************/
    public static final String AUTH_TOKEN = "authToken";
    public static final String WILL_TOPIC = "willTopic";
    /**************************************************** Data.class
     *  **************************************/
    public static final String TOKEN = "token";
    public static final String SID = "sid";
    public static final String LOGIN_EMAIL = "email";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String AVG_RATING = "averageRating";
    public static final String COUNTRY_CODE = "countryCode";
    public static final String COUNTRY_SYMBOL = "countrySymbol";
    public static final String LOGIN_PHONE = "phone";
    public static final String REFERRAL_CODE = "referralCode";
    public static final String PROFILE_PIC = "profilePic";
    public static final String PAYMENT_ID = "paymentId";
    public static final String FCMTOPIC = "fcmTopic";
    public static final String CURRENCYCODE = "currencyCode";
    public static final String PUBLISHABLE_KEY = "PublishableKey";
    public static final String CARD_DETAIL = "cardDetail";
    public static final String REQUESTER_ID = "requester_id";
    public static final String CALL = "call";
    /**************************************************** LoginPresnsterImpl.class  jsonParams
     *  **************************************/
    public static final String EMAIL_OR_PHONE = "emailOrPhone";
    public static final String PASSWORD = "password";
    public static final String PUSH_TOKEN = "pushToken";
    public static final String LOGINTYPE = "loginType";
    public static final String FACEBOOKID = "facebookId";
    public static final String GOOGLEID = "googleId";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String PUSHKITTOKEN = "pushKitToken";
    /**************************************************** UserDetailsDataModel class bundle key
     * Login.class  **************************************/
    public static final String USER_DETAILS = "user_details";
    /**************************************************** SingupPresenterImpl.class
     *  **************************************/
    public static final int CHECKFIRSTNAME = 1;
    public static final int CHECKEMAIL = 2;
    public static final int CHECKPASSWORD = 3;
    public static final int CHECKBUSSINESS = 4;
    public static final int REFERAL_LENGTH_FOUR = 4;
    public static final int REFERAL_LENGTH_FIVE = 5;
    public static final int REFERAL_LENGTH_SIX = 6;
    /**************************************************** Singup.class
     *  **************************************/
    public static final String CAMERAIMAGE = "cameraImage";
    public static final String DIALOGMESSAGESHOW_NO = "n";
    public static final String DIALOGMESSAGESHOW_YES = "y";
    public static boolean IS_CHATTING_OPENED = false;
    public static boolean IS_CHATTING_RESUMED = false;
    public static String currencySymbol = "$";
    public static String currency = "INR";
    public static String bookingcurrencySymbol = "$";
    public static boolean isDependentCalled = false;
    public static String isFromChckBox = "isFromChckBox";
    public static String isFromImageUpload = "isFromImageUpload";
    public static String isFromVideoUpload = "isFromVideoUpload";
    public static String isFromDatePicker = "isFromDatePicker";
    public static String isFromTime = "isFromTime";
    public static String isFromNumberPicker = "isFromNumberPicker";
    public static String isFromTextAreaCommaSeparated = "isFromTextAreaCommaSeparated";
    public static String isFromDropDown = "isFromDropDown";
    public static String isQuestionTitle = "isQuestionTitle";
    public static String isQuestionHeader = "isQuestionHeader";
    public static String isMin = "isMin";
    public static String isMax = "isMax";
    public static String symptom = "";
    public static String symptomId = "";
    public static String ENGLISH = "English";
    public static String PORTUGUESE = "Portuguese";
    public static String ENGLISH_CODE = "en";
    public static String PORTUGUESE_CODE = "pt";

    public static CityData.PaymentMode paymentMode;
    public static ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr> questionArrs =
            new ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr>();
    public static ArrayList<SymptomResponsePojo.SymptomCategory> responseData =
            new ArrayList<SymptomResponsePojo.SymptomCategory>();
    public static ArrayList<Symptom> categories =
            new ArrayList<Symptom>();
    public static ArrayList<SymptomCategory> categoriesTitle =
            new ArrayList<SymptomCategory>();
    public static HashMap<String, String> stringHashMap = new HashMap<String, String>();
    public static int POS = -1;
    public static int COUNT = -1;
    public static String customerPushTopic;
    public static String stripeKeys;
    public static String razorPayKey="";
    public static double latitude = 0.0;
    public static double longitude = 0.0;
    public static boolean isAddressSelect = false;
    public static String proAddress="";
    public static double proLatitude = 0.0;
    public static double proLongitude = 0.0;
    public static String searchType = "";
    public static String dependentId = "1";
    public static String dependentName = "";
    public static String dependentRelation = "Myself";
    public static String dependentImageUrl = "";
    public static String searchSymptomSelectedID = "";

    /*Call Module */
    public static double currentLat = 0;
    public static double currentLng = 0;
    public static boolean isFromNotification = false;
    public static String dateOB = "";
    public static LatLng custLatLng;
    public static int customerHomePageInterval = 5;
    public static int customerHandler = 1000;
    public static String selLang = "en";
    public static String mqttErrorMsg = "";
    public static String providerId = "";
    public static double lat = 0.0;
    public static double lng = 0.0;
    public static double minPrice = 0.0;
    public static double maxPrice = 0.0;
    public static int distance = 0;
    public static String ADD_DEPENDENTS = "ADD_DEPENDENT";
    public static String EDIT_DEPENDENTS = "EDIT_DEPENDENT";
    public static String CHANGE_PHONE = "change_phone";
    public static long COUNTDOWN_INTERVAL = 1000;
    public static long MILLIS_IN_FUTURE = 1000;


    /**
     * <h2>bookingType</h2>
     * 1 now booking
     * 2 late booking
     * 3 repeat
     */
    public static int bookingType = 1;
    /**
     * <h2>serviceType</h2>
     * service_type = 1 onDemand (ex Taxi booking) not price
     * service_type = 2 MarketPlace(ex Music) edit price
     * service_type = 3 Bidding
     */
    public static int serviceType = 2;
    /**
     * <h2>bookingModel</h2>
     * billing_model
     * 1 - fixed not edit
     * 2 - hourly + fixed not edit
     * 3 - hourly not edit
     * <p>
     * 4 - hourly edit
     * 5 - fixed edit
     * 6 - hourly + fixed edit
     * 7 - bidding
     */
    public static int bookingModel = 0;
    public static boolean isNeedApproveBycustomer;
    public static String catId = "";
    public static String prefKey = "SymptomQuestionFragment";
    public static boolean isFirstTime = false;
    /******************************************************************************************/
    public static String catName = "";
    public static String subCatId = "";
    public static int bookingTypeNowSchedule = 0;
    /******************************************************************************************/
    public static int bookingOffer = 0;
    public static int offerType = 0;
    /******************************************************************************************/
    public static String offerName = "";
    public static double minFee = 0;
    public static double maxFee = 0;
    public static ArrayList<Offers> offers = null;
    public static String proId = "";
    public static int scheduledTime = 0;
    /******************************************************************************************/
    public static long onRepeatEnd = 0;
    public static String scheduledDate = "";
    public static String cityId = "0";
    public static String cityName = "";
    public static int calltype = 0; //1 in call 2 out call
    /******************************************************************************************/
    public static int bookingModelJobDetails; // 1 on-Demand 2 market place, 3 bidding
    public static ArrayList<String> onRepeatDays = new ArrayList<>();
    public static CallType callType;
    /******************************************************************************************/
    public static Category.BookingTypeAction bookingTypeAction;
    public static String distanceUnit = "";
    public static int DISTANCEMATRIXUNIT = 0;
    public static String distanceSpeed = "";
    /******************************************************************************************/
    public static boolean isConfirmBook = false;
    public static String LiveTrackBookingPid = "";
    public static boolean IS_NETWORK_ERROR_SHOWED = false;
    public static boolean isHomeFragment = false;
    public static boolean isJobDetailsOpen = false;
    public static boolean isCallActivated = false;
    public static boolean isJobBidDetailsOpen = false;
    public static int callTypeInOutTele = 1;
    public static int POSCOUNT = 4;
    public static long fromTime = 0;
    public static long toTime = 0;
    public static boolean isMenuActivityCalled = false;
    public static String filteredAddress = "";
    public static double filteredLat = 0;
    public static double filteredLng = 0;
    public static double visitFee = 0;
    public static double pricePerHour = 0;
    public static int serviceSelected = 2;
    /******************************************************************************************/
    public static int paymentAbbr = 1;
    public static String homePageTagLine ="";
    public static boolean isHelpIndexOpen = false;
    public static long serverTime = 0;
    public static long diffServerTime = 0;
    public static String walletCurrency = "$";
    public static double walletAmount = 0;
    public static boolean isLoggedIn = true;
    public static String selectedDate = "";
    public static String selectedDuration = "";
    /******************************************************************************************/
    /*******************************RepeatText Area **************************************/

    public static String repeatStartTime = "";
    /******************************************************************************************/
    public static String repeatStartDate = "";
    public static String repeatEndTime = "";
    public static String repeatEndDate = "";
    public static int repeatNumOfShift = 0;
    public static ArrayList<String> repeatDays = new ArrayList<>();
    public static ArrayList<String> GOOGLEKEY = new ArrayList<>();
    public static String jsonArray = "";
    /******************************************************************************************/
    public static String userUids = "";
    public static String userNames = "";
    public static String userIdentifiers = "";
    public static String userImageUrls = "";
    public static boolean isRelationCalled;

    public static void updateUserDetails(String userUid, String userName, String userIdentifier,
                                         String userImageUrl) {
        userUids = userUid;
        userNames = userName;
        userIdentifiers = userIdentifier;
        userImageUrls = userImageUrl;
    }


    public static final String  CERTIFICATE_TYPE = "X.509";
    public static final String ALIAS_NAME = "ca";
    public static final String REQUESTED_PROTOCOL = "SSL";
    public static final int ARRAY_SIZE = 1;
    public static final String SEARCH_ITEM = "searchItem";
    public static final String CERTIFICATION_ERROR = "Unexpected default trust managers:";
    public static final int LOWER_BOUND = 0;
    /******************************************************************************************/

    public static int BITRATE_128Kbps = 128000;
    public static int BITRATE_300Kbps = 200000;
    public static int BITRATE_3000Kbps = 2000000;

    public static final int W_320 = 320;
    public static final int H_480 = 480;
    public static final int W_1 = 1;
    public static final int W_2 = 2;
    public static final int W_3 = 3;
    public static final int W_4 = 4;
    public static final int W_5 = 5;
    public static final int W_6 = 6;
    public static final int W_7 = 7;
    public static final int W_8 = 8;
    public static final int W_9 = 9;
    public static final int W_10 = 10;
    public static final int W_11 = 11;
    public static final int W_12 = 12;
    public static final int W_13 = 13;
    public static final int W_14 = 14;
    public static final int W_15 = 15;
    public static final int W_16 = 16;
    public static final int W_17 = 17;
    public static final int W_18 = 18;
    public static final int W_19 = 19;
    public static final int W_20 = 20;
    public static final int W_21 = 21;
    public static final int W_22 = 22;
    public static final int W_23 = 23;
    public static final int W_24 = 24;
    public static final int W_25 = 25;
    public static final int W_26 = 26;
    public static final int W_27 = 27;
    public static final int W_28 = 28;
    public static final int W_29 = 29;
    public static final int W_30 = 30;
    public static final int W_31 = 31;
    public static final int W_32 = 32;
    public static final int W_33 = 33;
    public static final int W_34 = 34;
    public static final int W_35 = 35;
    public static final int W_36 = 36;
    public static final int W_37 = 37;
    public static final int W_38 = 38;
    public static final int W_39 = 39;
    public static final int W_40 = 40;
    public static final int W_41 = 41;
    public static final int W_42 = 42;
    public static final int W_43 = 43;
    public static final int W_44 = 44;
    public static final int W_45 = 45;
    public static final int W_46 = 46;
    public static final int W_47 = 47;
    public static final int W_48 = 48;
    public static final int W_49 = 49;
    public static final int W_50 = 50;
    public static final int W_52 = 52;
    public static final int W_57 = 57;
    public static final int W_54 = 54;
    public static final int W_59 = 59;
    public static final int W_80 = 80;
    public static final int W_88 = 88;
    public static final int W_168 = 168;
    public static final int W_155 = 155;
    public static final int W_144 = 144;
    public static final int W_110 = 110;
    public static final int W_100 = 100;


    public static final int H_110 = 110;
    public static final int H_155 = 155;
    public static final int H_144 = 144;
    public static final int H_168 = 168;
    public static final int H_88 = 88;
    public static final int H_1 = 1;
    public static final int H_2 = 2;
    public static final int H_3 = 3;
    public static final int H_4 = 4;
    public static final int H_5 = 5;
    public static final int H_6 = 6;
    public static final int h_7 = 7;
    public static final int H_8 = 8;
    public static final int H_9 = 9;
    public static final int H_10 = 10;
    public static final int H_11 = 11;
    public static final int H_12 = 12;
    public static final int H_13 = 13;
    public static final int H_14 = 14;
    public static final int H_15 = 15;
    public static final int H_16 = 16;
    public static final int h_17 = 17;
    public static final int H_18 = 18;
    public static final int H_19 = 19;
    public static final int H_20 = 20;
    public static final int H_21 = 21;
    public static final int H_22 = 22;
    public static final int H_23 = 23;
    public static final int H_24 = 24;
    public static final int H_25 = 25;
    public static final int H_26 = 26;
    public static final int h_27 = 27;
    public static final int H_28 = 28;
    public static final int H_29 = 29;
    public static final int H_30 = 30;
    public static final int H_31 = 31;
    public static final int H_32 = 32;
    public static final int H_33 = 33;
    public static final int H_34 = 34;
    public static final int H_35 = 35;
    public static final int H_36 = 36;
    public static final int h_37 = 37;
    public static final int H_38 = 38;
    public static final int H_39 = 39;
    public static final int H_40 = 40;
    public static final int H_41 = 41;
    public static final int H_42 = 42;
    public static final int H_43 = 43;
    public static final int H_44 = 44;
    public static final int H_45 = 45;
    public static final int H_46 = 46;
    public static final int h_47 = 47;
    public static final int H_48 = 48;
    public static final int H_49 = 49;
    public static final int H_50 = 50;
    public static final String FULLADDRESS = "FULLADDRESS";
    public static final String ADDRESSPOJO = "ADDRESSPOJO";


    public static final String KEY_PRIVATEALIAS = "PRIVATEALIAS";
    public static final String KEY_PUBLICALIAS = "PUBLICALIAS";
    public static final String ANDROID_KEYSTORE = "AndroidKeyStore";
    public static final String RSA_MODE =  "RSA/ECB/PKCS1Padding";
    public static final String AES_MODE = "AES/GCM/NoPadding";
    public static final String FIXED_IV = "appscripName";
    private final static byte[] IV = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
    public static final IvParameterSpec IV_PARAMETER_SPEC = new IvParameterSpec(IV);

    public interface SHARESCREEN {
        String DESCRIPTION = "description";
        String REFERRALCODE = "referralCode";
        String EXTRA_REFERRAL_CODE = "ReferralCode";
 }
    public interface YOURADDRESSSCREEN {
        String EXTRA_IS_NOT_FROM_ADDRESS = "isNotFromAddress";
        String EXTRA_ADDRESSLINEONE = "AddressLine1";
        String EXTRA_ADDRESSLINETWO = "AddressLine2";
        String EXTRA_LAT = "lat";
        String EXTRA_LNG = "lng";
        String EXTRA_TAGAS = "TAGAS";
        String BUNDLE_EDIT_ADD = "edit_addr";
        public static final int EDT_ADD_REQ_CODE = 101;
    }
    public interface FAQSCREEN {
        String EXTRA_LINK = "Link";
        String EXTRA_TITLE = "Title";
        String EXTRA_COMINFROM = "COMINFROM";
        String BUNDLE_FAQ_LIST = "faqList";
    }
    public interface PRESCRIPTIONCREEN {
        String EXTRA_FILTER_DATE_NAME = "FilterDateName";
        String EXTRA_DEPENDENT_ID = "mDependentId";
        String EXTRA_FILTER_DATA_POJO = "mFilterDateListPojos";
        String ONE = "1";
        String TWO = "2";
        String THREE = "3";
        String FOUR = "4";
        String LAST_MONTH = "Last month";
        String LAST_THREE_MONTH = "Last 3 months";
        String LAST_SIX_MONTH = "Last 6 months";
        String LAST_ONE_YEAR = "Last 1 year";
        public static final int FILTERDEP_RESULT_CODE = 108;
    }

}
