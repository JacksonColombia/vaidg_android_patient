package com.vaidg.utilities;

import static com.vaidg.utilities.Constants.*;
import static com.vaidg.utilities.Constants.BITRATE_128Kbps;
import static com.vaidg.utilities.Constants.BITRATE_3000Kbps;
import static com.vaidg.utilities.Constants.BITRATE_300Kbps;
import static com.vaidg.utilities.Constants.KILOMETER;
import static com.vaidg.utilities.Constants.METER;
import static com.vaidg.utilities.Constants.NAUTICAL_MILES;
import static com.vaidg.utilities.Constants.cityId;
import static com.vaidg.utilities.Constants.cityName;
import static com.vaidg.utilities.Constants.lat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.AppCompatDrawableManager;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.os.ConfigurationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.vaidg.R;
import com.vaidg.countrypic.Country;
import com.vaidg.countrypic.CountryPicker;
import com.vaidg.intro.IntroActivity;
import com.vaidg.youraddress.model.YourAddrData;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.messaging.FirebaseMessaging;
import com.pojo.LanguagesList;
import com.utility.MathUtil;
import com.utility.NetworkErrorActivity;
import com.utility.TimezoneMapper;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;
import java.util.TimeZone;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import eu.janmuller.android.simplecropimage.Util;
import io.card.payment.CardType;


/**
 * <h2>Utility</h2>
 * Created by ${3Embed} on 6/10/17.
 */

public class Utility {

  static int w20, w50, w40, w10, w15, w8, w5, w250;
  static int h20, h2, h10, h15, h8, h7, h5, h290;
  /**
   * <h2>Get Device Id</h2>
   * <p>
   * method to retrieve the device id
   * Please call this in try catch block to handle gracefully
   * </p>
   *
   * @param context of the application such as activity/fragment
   */
  public static String getDeviceId(Context context) {
    String deviceId;
    @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(
        context.getContentResolver(), Settings.Secure.ANDROID_ID);
    if (!android_id.equals("")) {
      deviceId = android_id;
    } else {
      deviceId = UUID.randomUUID().toString();
    }

    return deviceId;
  }

  public static String dateInTwentyFour(long addedTime) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeZone(Utility.getTimeZone());
    if (addedTime > 0) {
      calendar.setTimeInMillis(TimeUnit.SECONDS.toMillis(addedTime));
    }
    calendar.setTimeZone(getTimeZone());
    Date date = calendar.getTime();
    SimpleDateFormat formated = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
    formated.setTimeZone(getTimeZone());
    return formated.format(date);
  }

  public static TimeZone getTimeZone() {
    String timeZoneString ="";
    if(lat != 0.0) {
      timeZoneString = TimezoneMapper.latLngToTimezoneString(lat,
              lng);
    }else{
      timeZoneString = TimeZone.getDefault().getID();
    }
   // String timeZoneString = (String) TimeZone.getDefault().getID();
    return TimeZone.getTimeZone(timeZoneString);
  }

  public static void copyStream(InputStream input, OutputStream output)
      throws IOException {

    byte[] buffer = new byte[2084];
    int bytesRead;
    while ((bytesRead = input.read(buffer)) != -1) {
      output.write(buffer, 0, bytesRead);
    }
  }

  /*get the color*/

  public static int getColor(Context mContext, int id) {


    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      return mContext.getColor(id);
    } else {
      return ContextCompat.getColor(mContext, id);
    }
  }

  public static void showKeyboard(final Context context, final View view) {
    view.requestFocus();
    view.postDelayed(() -> {
      InputMethodManager keyboard = (InputMethodManager) context.getSystemService(
          Context.INPUT_METHOD_SERVICE);
      keyboard.showSoftInput(view, 0);
    }, 200);
  }

  public static int getCountryMax(CountryPicker mCountryPicker, Context context) {

    Country country = mCountryPicker.getUserCountryInfo(context);
    String max_dig = country.getMax_digits();
    int max = 0;
    if (max_dig != null || !("".equals(max_dig.trim()))) {
      max = Integer.parseInt(max_dig);
    }
    return max;
  }

  public static boolean isNetworkAvailable(Context context) {
    ConnectivityManager cm =
        (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    return activeNetwork != null &&
        activeNetwork.isConnectedOrConnecting();
  }

  public static boolean isLocationEnabled(Context context) {
    int locationMode = 0;
    String locationProviders;

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
      try {
        locationMode = Settings.Secure.getInt(context.getContentResolver(),
            Settings.Secure.LOCATION_MODE);

      } catch (Settings.SettingNotFoundException e) {
        e.printStackTrace();
        return false;
      }

      return locationMode != Settings.Secure.LOCATION_MODE_OFF;

    } else {
      locationProviders = Settings.Secure.getString(context.getContentResolver(),
          Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
      return !TextUtils.isEmpty(locationProviders);
    }


  }

  /**
   * This method is used when our current Session got expired, so we instructs user to again do
   * Login.
   *
   * @param mcontext Context of the Activity or the Fragment
   * @param manager  sharedPreference
   */
  public static void setMAnagerWithBID(Context mcontext, SessionManagerImpl manager) {


    Intent intent = new Intent(mcontext, IntroActivity.class);

    Log.d("TAGUTILITY", "setMAnagerWithBID: " + manager.getFcmTopic());
    if (!"".equals(manager.getFcmTopic()) && manager.getFcmTopic() != null) {
      FirebaseMessaging.getInstance().unsubscribeFromTopic(manager.getFcmTopic());
      // manager.setFcmTopic("");

    }
    if (!"".equals(manager.getFcmTopicCity()) && manager.getFcmTopicCity() != null) {
      FirebaseMessaging.getInstance().unsubscribeFromTopic(manager.getFcmTopicCity());
      //  manager.setFcmTopicCity("");
    }

    if (!"".equals(manager.getFcmTopicAllCustomer()) && manager.getFcmTopicAllCustomer() != null) {
      FirebaseMessaging.getInstance().unsubscribeFromTopic(manager.getFcmTopicAllCustomer());
      //  manager.setFcmTopicAllCustomer("");
    }

    if (!"".equals(manager.getFcmTopicAllCity()) && manager.getFcmTopicAllCity() != null) {
      FirebaseMessaging.getInstance().unsubscribeFromTopic(manager.getFcmTopicAllCity());
      //  manager.setFcmTopicAllCity("");
    }
    LSPApplication.getInstance().removeAccount(manager.getSID());
    cityId ="0";
    cityName="";
    manager.clearSession();
    if (manager.getLanguageSettings() == null) {
      manager.setLanguageSettings(new LanguagesList("en", "English", 0));
    } else {
      Utility.changeLanguageConfig(manager.getLanguageSettings().getCode(), mcontext);
    }

    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    mcontext.startActivity(intent);
    // VariableConstant.CALLMQTTDATA = 0;
    ((Activity) mcontext).finish();

  }

  public static void setAmtOnRecept(double amt, TextView textView, String currencySybmol) {
    if (amt <= 0) {
      if (paymentAbbr == 1) {
        String timefee = currencySybmol + " 0.00";
        textView.setText(timefee);
      } else {
        String timefee = "0.00 " + currencySybmol;
        textView.setText(timefee);
      }

    } else {
      if (paymentAbbr == 1) {
        String timefee = currencySybmol + " " + doubleformate(amt);
        textView.setText(timefee);
      } else {
        String timefee = doubleformate(amt) + " " + currencySybmol;
        textView.setText(timefee);
      }

    }
  }

  public static void setAmtOnRecept(double amt, TextView textView, String currencySybmol,
                                    String text) {
    if (amt <= 0) {
      if (paymentAbbr == 1) {
        String timefee = text + "   (" + currencySybmol + " 0.00)";
        textView.setText(timefee);
      } else {
        String timefee = text + "   (0.00 " + currencySybmol + ")";
        textView.setText(timefee);
      }

    } else {
      if (paymentAbbr == 1) {
        String timefee = text + "   (" + currencySybmol + " " + doubleformate(amt) + ")";
        textView.setText(timefee);
      } else {
        String timefee = text + "   (" + doubleformate(amt) + " " + currencySybmol + ")";
        textView.setText(timefee);
      }

    }
  }

  public static String doubleformate(double amountvalue) {
    NumberFormat formatter = new DecimalFormat("#0.00");
    String datavalueIs;
    String datavalue = String.valueOf(amountvalue);
    if (datavalue.contains(",")) {
      String value = datavalue.replace(",", ".");
      datavalueIs = formatter.format(Double.parseDouble(value));
    } else {
      datavalueIs = formatter.format(amountvalue);
    }

    return datavalueIs;
  }

  public static int[] calculateTimeDifference(long reviewRate, long serverTime) {
    String currentDate = Utility.dateInTwentyFour(0);
    int[] duration = {0, 0, 0};
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
    simpleDateFormat.setTimeZone(Utility.getTimeZone());
    // Date rAppDate=null,
    Date curDate;

    try {
      //  rAppDate = simpleDateFormat.parse(reviewRate*1000L);
      curDate = simpleDateFormat.parse(currentDate);
      long timeDiffInMilli = curDate.getTime() - (TimeUnit.SECONDS.toMillis(reviewRate));
      int seconds = (int) (timeDiffInMilli / 1000);
      duration[0] = Utility.getDurationString(seconds)[0];
      duration[1] = Utility.getDurationString(seconds)[1];
      duration[2] = Utility.getDurationString(seconds)[2];
      Log.d("TAG", "InvoiceAct calculateTimeDifference duration " + duration[0] + " min "
          + duration[1] + " sec " + duration[2]);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return duration;
  }

  /**
   * Custom method to convert consumed seconds into hh:mm:sec
   *
   * @param seconds: is the total amount of time consumed to reach to pickup adrs
   */
  private static int[] getDurationString(int seconds) {
    int[] duration = {0, 0, 0};
    int hours = seconds / 3600;
    int minutes = (seconds % 3600) / 60;
    seconds = seconds % 60;

    duration[0] = hours;

    duration[1] = minutes;

    duration[2] = seconds;

    return duration;
  }

  public static String getFormattedDate(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTimeZone(Utility.getTimeZone());
    cal.setTime(date);
    //2nd of march 2015
    int day = cal.get(Calendar.DATE);

    if (!((day > 10) && (day < 19))) {
      switch (day % 10) {
        case 1:
          // return simpleDateFormater("EEE d'st' MMM yyyy',' hh:mm a", date);
          return simpleDateFormater("d'st' MMM yyyy '|' hh:mm a", date);
        case 2:
          //return simpleDateFormater("EEE d'nd' MMM yyyy',' hh:mm a", date);
          return simpleDateFormater("d'nd' MMM yyyy '|' hh:mm a", date);
        case 3:
          // return simpleDateFormater("EEE d'rd' MMM yyyy',' hh:mm a", date);
          return simpleDateFormater("d'rd' MMM yyyy '|' hh:mm a", date);
        default:
          //  return simpleDateFormater("EEE d'th' MMM yyyy',' hh:mm a", date);
          return simpleDateFormater("d'th' MMM yyyy '|' hh:mm a", date);
      }
    }
    //  return simpleDateFormater("EEE d'th' MMM yyyy',' hh:mm a", date);
    return simpleDateFormater("d'th' MMM yyyy '|' hh:mm a", date);
  }
  public static String getFormattedDateLong(long date) {
    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
    cal.setTimeZone(Utility.getTimeZone());
    cal.setTimeInMillis(date);
   // cal.setTimeZone(getTimeZone());
    //2nd of march 2015
    int day = cal.get(Calendar.DATE);

    if (!((day > 10) && (day < 19))) {
      switch (day % 10) {
        case 1:
          // return simpleDateFormater("EEE d'st' MMM yyyy',' hh:mm a", date);
          return simpleDateFormaterLong("d'st' MMM yyyy '|' hh:mm a", cal);
        case 2:
          //return simpleDateFormater("EEE d'nd' MMM yyyy',' hh:mm a", date);
          return simpleDateFormaterLong("d'nd' MMM yyyy '|' hh:mm a", cal);
        case 3:
          // return simpleDateFormater("EEE d'rd' MMM yyyy',' hh:mm a", date);
          return simpleDateFormaterLong("d'rd' MMM yyyy '|' hh:mm a", cal);
        default:
          //  return simpleDateFormater("EEE d'th' MMM yyyy',' hh:mm a", date);
          return simpleDateFormaterLong("d'th' MMM yyyy '|' hh:mm a", cal);
      }
    }
    //  return simpleDateFormater("EEE d'th' MMM yyyy',' hh:mm a", date);
    return simpleDateFormaterLong("d'th' MMM yyyy '|' hh:mm a", cal);
  }

  private static String simpleDateFormater(String format, Date date) {
    SimpleDateFormat sfd = new SimpleDateFormat(format, Locale.US);
    sfd.setTimeZone(getTimeZone());
    return sfd.format(date);
  }
  private static String simpleDateFormaterLong(String format, Calendar cal) {
   // SimpleDateFormat sfd = new SimpleDateFormat(format, Locale.US);
 //   sfd.setTimeZone(getTimeZone());
    return DateFormat.format(format, cal).toString();
  }


  @SuppressLint("PrivateResource")
  public static Bitmap setCreditCardLogo(String cardMethod, Context context) {
    Bitmap anImage;
    switch (cardMethod) {
      case "Visa":
      case "visa":
        anImage = CardType.VISA.imageBitmap(context);//getBitmapFromVectorDrawable(context, CardBrand.Visa.getIcon());
        break;
      case "MasterCard":
      case "mastercard":
        anImage = CardType.MASTERCARD.imageBitmap(context);//getBitmapFromVectorDrawable(context, CardBrand.MasterCard.getIcon());
        break;
      case "American Express":
      case "amex":
        anImage = CardType.AMEX.imageBitmap(context);//getBitmapFromVectorDrawable(context, CardBrand.AmericanExpress.getIcon());
        break;
      case "Discover":
      case "discover":
        anImage =CardType.DISCOVER.imageBitmap(context); //getBitmapFromVectorDrawable(context, CardBrand.Discover.getIcon());
        break;
      case "Diners Club":
      case "diners":
        anImage = CardType.DINERSCLUB.imageBitmap(context);//getBitmapFromVectorDrawable(context, CardBrand.DinersClub.getIcon());
        break;

      case "JCB":
      case "jcb":
        anImage = CardType.JCB.imageBitmap(context);//getBitmapFromVectorDrawable(context, CardBrand.JCB.getIcon());
        break;

      case "unionpay":
        anImage = CardType.MAESTRO.imageBitmap(context);//getBitmapFromVectorDrawable(context, CardBrand.UnionPay.getIcon());
        break;

      case "Cash":
        anImage = getBitmapFromVectorDrawable(context, R.drawable.ic_cash_icon);
        break;
      case "Wallet":
        anImage = getBitmapFromVectorDrawable(context,
            R.drawable.ic_account_balance_wallet_black_24dp);
        break;

      default:
        anImage = CardType.UNKNOWN.imageBitmap(context);//getBitmapFromVectorDrawable(context, R.drawable.visa_card);
        break;
    }
    return anImage;
  }


  @SuppressLint("RestrictedApi")
  public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
    Drawable drawable = AppCompatDrawableManager.get().getDrawable(context, drawableId);
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
      drawable = (DrawableCompat.wrap(drawable)).mutate();
    }

    Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
        drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
    drawable.draw(canvas);

    return bitmap;
  }

  public static long timeStamp(long expireTime, long serverTime) {

    Log.d("TAGTIME", " expireTime " + expireTime + " server " + serverTime);
    Date date = new Date(TimeUnit.SECONDS.toMillis(expireTime));
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.US);
    sdf.setTimeZone(getTimeZone());
    String formattedDate = sdf.format(date);
    Log.d("TIME", "timeStamp: " + formattedDate + " TIMER " + (date.getTime()));
    Date date2 = new Date(TimeUnit.SECONDS.toMillis(serverTime));
    String formattedDate2 = sdf.format(date2);
    Log.d("TIME", "timeStamp2: " + formattedDate2 + " TIMER2 " + (date2.getTime()));
    long diff = date.getTime() - date2.getTime();
    if (diff > 0) {
      int seconds = (int) (diff / 1000);
      int hours = seconds / 3600;
      int minutes = (seconds % 3600) / 60;
      seconds = seconds % 60;
      long totaltime = (minutes * 60 * 1000) + (seconds * 1000);
      Log.d("TAGTIME", "timeStamp: " + hours + " min " + minutes + " sec " + seconds);

      return diff;
    } else {
      return 0;
    }
  }


  public static boolean isAppIsInBackgroundOne(Context context) {
    boolean isInBackground = true;
    ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
      List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
      for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
        if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
          for (String activeProcess : processInfo.pkgList) {
            if (activeProcess.equals(context.getPackageName())) {
              isInBackground = false;
            }
          }
        }
      }
    } else {
      List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
      ComponentName componentInfo = taskInfo.get(0).topActivity;
      if (componentInfo.getPackageName().equals(context.getPackageName())) {
        isInBackground = false;
      }
    }

    return isInBackground;
  }


  /**
   * Method checks if the app is in background or not
   */
  public static boolean isAppIsInBackground(Context context) {
    boolean isInBackground = true;
    ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
      List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
      for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
        if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
          for (String activeProcess : processInfo.pkgList) {
            if (activeProcess.equals(context.getPackageName())) {
              isInBackground = false;
            }
          }
        }
      }
    } else {

      if (isTopActivity(context, context.getPackageName())) {
        isInBackground = false;
      }
    }

    return isInBackground;
  }

  public static boolean appInForeground(@NonNull Context context) {
    ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager != null ? activityManager.getRunningAppProcesses() : null;
    if (runningAppProcesses == null) {
      return false;
    }

    for (ActivityManager.RunningAppProcessInfo runningAppProcess : runningAppProcesses) {
      if (runningAppProcess.processName.equals(context.getPackageName()) &&
          runningAppProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
        return true;
      }
    }
    return false;
  }

  /**
   * TODO 根据包名判断是否在最前面显示
   *
   * @param context {@link Context}
   * @return boolean
   * @author Melvin
   * @date 2013-4-23
   */
  private static boolean isTopActivity(Context context, String packageName) {
    if (context == null) {
      return false;
    }//|| isNull(packageName)
    int id = context.checkCallingOrSelfPermission(Manifest.permission.GET_TASKS);
    if (PackageManager.PERMISSION_GRANTED != id) {
      return false;
    }

    ActivityManager activityManager = (ActivityManager) context
        .getSystemService(Context.ACTIVITY_SERVICE);
    List<ActivityManager.RunningTaskInfo> tasksInfo = activityManager.getRunningTasks(1);
    if (tasksInfo.size() > 0) {
      return packageName.equals(tasksInfo.get(0).topActivity.getPackageName());
    }
    return false;
  }


  public static void hideKeyboard(Activity mcontext) {
    try {
      InputMethodManager inputManager = (InputMethodManager) mcontext.getSystemService(
          Context.INPUT_METHOD_SERVICE);
      if (inputManager != null) {
        inputManager.hideSoftInputFromWindow(mcontext.getCurrentFocus().getWindowToken(),
            InputMethodManager.RESULT_UNCHANGED_SHOWN);
      }
    } catch (NullPointerException e) {

    }

  }

  public static void showKeyBoard(Activity mContext) {
    try {
      InputMethodManager imm = (InputMethodManager) mContext.getSystemService(
          Context.INPUT_METHOD_SERVICE);
      imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    } catch (Exception e) {

    }
  }

  public static boolean containsLocation(double latitude, double longitude, List<LatLng> polygon,
                                         boolean geodesic) {
    int size = polygon.size();
    if (size == 0) {
      return false;
    } else {
      double lat3 = Math.toRadians(latitude);
      double lng3 = Math.toRadians(longitude);
      LatLng prev = polygon.get(size - 1);
      double lat1 = Math.toRadians(prev.latitude);
      double lng1 = Math.toRadians(prev.longitude);
      int nIntersect = 0;

      double lng2;
      for (Iterator var17 = polygon.iterator(); var17.hasNext(); lng1 = lng2) {
        LatLng point2 = (LatLng) var17.next();
        double dLng3 = MathUtil.wrap(lng3 - lng1, -3.141592653589793D, 3.141592653589793D);
        if (lat3 == lat1 && dLng3 == 0.0D) {
          return true;
        }

        double lat2 = Math.toRadians(point2.latitude);
        lng2 = Math.toRadians(point2.longitude);
        if (intersects(lat1, lat2,
            MathUtil.wrap(lng2 - lng1, -3.141592653589793D, 3.141592653589793D), lat3, dLng3,
            geodesic)) {
          ++nIntersect;
        }

        lat1 = lat2;
      }

      return (nIntersect & 1) != 0;
    }
  }

  private static boolean intersects(double lat1, double lat2, double lng2, double lat3, double lng3,
                                    boolean geodesic) {
    if ((lng3 < 0.0D || lng3 < lng2) && (lng3 >= 0.0D || lng3 >= lng2)) {
      if (lat3 <= -1.5707963267948966D) {
        return false;
      } else if (lat1 > -1.5707963267948966D && lat2 > -1.5707963267948966D
          && lat1 < 1.5707963267948966D && lat2 < 1.5707963267948966D) {
        if (lng2 <= -3.141592653589793D) {
          return false;
        } else {
          double linearLat = (lat1 * (lng2 - lng3) + lat2 * lng3) / lng2;
          return !(lat1 >= 0.0D && lat2 >= 0.0D && lat3 < linearLat) && (
              lat1 <= 0.0D && lat2 <= 0.0D && lat3 >= linearLat || (lat3 >= 1.5707963267948966D || (
                  geodesic ? Math.tan(lat3) >= tanLatGC(lat1, lat2, lng2, lng3) : MathUtil.mercator(
                      lat3) >= mercatorLatRhumb(lat1, lat2, lng2, lng3))));
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  private static double tanLatGC(double lat1, double lat2, double lng2, double lng3) {
    return (Math.tan(lat1) * Math.sin(lng2 - lng3) + Math.tan(lat2) * Math.sin(lng3)) / Math.sin(
        lng2);
  }

  private static double mercatorLatRhumb(double lat1, double lat2, double lng2, double lng3) {
    return (MathUtil.mercator(lat1) * (lng2 - lng3) + MathUtil.mercator(lat2) * lng3) / lng2;
  }

  public static RequestOptions createGlideOption(Context mContext) {
    return new RequestOptions()
        .centerCrop()
        .placeholder(R.drawable.register_profile_default_image)
        .error(R.drawable.register_profile_default_image)
        .transform(new CircleTransform(mContext))
        .priority(Priority.HIGH);

  }

  public static RequestOptions createGlideOptionCall(Context mContext) {
    return new RequestOptions()
        .centerCrop()
        .placeholder(R.drawable.ic_fever_placeholder)
        .error(R.drawable.ic_fever_placeholder)
        .transform(new CircleTransform(mContext))
        .priority(Priority.HIGH);

  }

  public static RequestOptions createGlideOptionMetadData(Context mContext) {
    return new RequestOptions()
        .placeholder(R.drawable.logo)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .priority(Priority.HIGH)
        .fitCenter()
        .override(Target.SIZE_ORIGINAL)
        .format(DecodeFormat.PREFER_ARGB_8888)
        .skipMemoryCache(false)
        .error(R.drawable.logo)
        .transform(new CircleTransform(mContext));

  }

  public static void checkAndShowNetworkError(Context context) {
    if (!isNetworkAvailable(context) && !IS_NETWORK_ERROR_SHOWED) {
      Intent intent = new Intent(context, NetworkErrorActivity.class);
      context.startActivity(intent);
    }
  }

  public static double round(double value, int places) {
    if (places < 0) throw new IllegalArgumentException();

    long factor = (long) Math.pow(10, places);
    value = value * factor;
    long tmp = Math.round(value);
    return (double) tmp / factor;
  }

  public static void statusbar(Activity activity) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      Window window = activity.getWindow();
      window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
      int blueColorValue = Color.parseColor("#FFFFFFFF");//#2598ED
      window.setStatusBarColor(blueColorValue);
    }
  }

  /**
   * <h2>Get status bar color</h2>
   * <p>
   * method to apply the color in the status bar
   * </p>
   *
   * @param activity of the application such as activity/fragment
   */
  public static void statusbarColor(Activity activity) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
      Window window = activity.getWindow();
      window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
      int blueColorValue = Color.parseColor("#1A54B0");//#2598ED
      window.setStatusBarColor(blueColorValue);
    }
  }

  /**
   * <h2>changeLanguageConfig</h2>
   * used to set the language configuration
   *
   * @param code language code
   */
  public static int changeLanguageConfig(String code, Context context) {
    Configuration configuration = context.getResources().getConfiguration();
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
      configuration.setLayoutDirection(new Locale(code));
      Log.d("TAG", " language direction " + configuration.getLayoutDirection());
    }
    configuration.locale = new Locale(code);
    context.getResources().updateConfiguration(configuration,
        context.getResources().getDisplayMetrics());
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
      return configuration.getLayoutDirection();
    }
    return 0;
  }

  public static List<Date> getDatesBetweenUsingJava7(
      Date startDate, Date endDate) {
    List<Date> datesInRange = new ArrayList<>();
    Calendar calendar = new GregorianCalendar();
    calendar.setTime(startDate);

    Calendar endCalendar = new GregorianCalendar();
    endCalendar.setTime(endDate);

    while (calendar.before(endCalendar)) {
      Date result = calendar.getTime();
      datesInRange.add(result);
      calendar.add(Calendar.DATE, 1);
    }
    return datesInRange;
  }


  public static String dayOfTheWeek(int day) {
    String days;
    switch (day) {
      case 1:
        days = "Sunday";
        break;
      case 2:
        days = "Monday";
        break;
      case 3:
        days = "Tuesday";
        break;
      case 4:
        days = "Wednesday";
        break;
      case 5:
        days = "Thursday";
        break;
      case 6:
        days = "Friday";
        break;
      default:
        days = "Saturday";
        break;


    }
    return days;
  }

  public static double distance(double lat1, double lng1, double lat2, double lng2, String unit) {
    double earthRadius = 3958.75; // miles (or 6371.0 kilometers)
    double dLat = Math.toRadians(lat2 - lat1);
    double dLng = Math.toRadians(lng2 - lng1);
    double sindLat = Math.sin(dLat / 2);
    double sindLng = Math.sin(dLng / 2);
    double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1))
        * Math.cos(Math.toRadians(lat2));
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    double dist = earthRadius * c;
    if (KILOMETER.equals(unit))
// Kilometer
    {
      dist = dist * 1.609344;
    } else if (NAUTICAL_MILES.equals(unit))
// Nautical Miles
    {
      dist = dist * 0.8684;
    } else if (METER.equals(unit))
// meter
    {
      dist = dist * 1609.344;
    }
    return dist;
  }

  public static View getCustomeBackgroundTextProfilePic(LayoutInflater inflater, String firstLetter,
                                                        String secondLetter) {
    View view = inflater.inflate(R.layout.custom_background_text_profile_pic_100, null);
    TextView tvImageName = view.findViewById(R.id.tvImageName);
    String name = "";
    try {
      if (firstLetter != null) {
        name = firstLetter.substring(0, 1);
        if (secondLetter != null && !secondLetter.equals("")) {
          name = name + secondLetter.substring(0, 1);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }


    tvImageName.setText(name);
    return view;
  }

  public static Bitmap createDrawableFromView(Context context, View view) {

    DisplayMetrics displayMetrics = new DisplayMetrics();
    if (context != null) {
      if (context.getApplicationContext() != null) {
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
      }
    }
    view.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,
        ActionBar.LayoutParams.WRAP_CONTENT));
    view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
    view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
    view.buildDrawingCache();
    Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
        Bitmap.Config.ARGB_8888);

    Canvas canvas = new Canvas(bitmap);
    view.draw(canvas);

    return bitmap;
  }


  public static void saveImage(Bitmap finalBitmap, File file) {
    Random generator = new Random();
    int n = 10000;
    n = generator.nextInt(n)
    ;
    if (file.exists()) {
      file.delete();
    }
    try {
      FileOutputStream out = new FileOutputStream(file);
      finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
      out.flush();
      out.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  /**
   * method for conveting utctime to custom time format
   *
   * @param time utc time
   * @return custom time format
   */
  public static String convertUTCToServerFormat(String time) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.US);
    sdf.setTimeZone(getTimeZone());
    Calendar cal = Calendar.getInstance();
    cal.setTimeZone(Utility.getTimeZone());
    cal.setTimeInMillis(TimeUnit.SECONDS.toMillis(Long.parseLong(time)));
    //cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
    Date currenTimeZone = cal.getTime();
    return sdf.format(currenTimeZone);
  }

  /**
   * method for conveting utctime to custom time format
   *
   * @param time utc time
   * @return custom time format
   */
  public static String convertUTCToServerFormat(String time, String format) {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
    sdf.setTimeZone(getTimeZone());
    Calendar cal = Calendar.getInstance();
    cal.setTimeZone(Utility.getTimeZone());
    cal.setTimeInMillis(TimeUnit.SECONDS.toMillis(Long.parseLong(time)));
    //cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
    Date currenTimeZone = cal.getTime();
    return sdf.format(currenTimeZone);
  }


  /**
   * method for conveting utctime to custom time format
   *
   * @param time utc time
   * @return custom time format
   */
  public static String convertUTCToDateFormat(String time) {
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
    sdf.setTimeZone(getTimeZone());
    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
    cal.setTimeZone(Utility.getTimeZone());
    cal.setTimeInMillis(TimeUnit.SECONDS.toMillis(Long.parseLong(time)));
    //cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
    Date currenTimeZone = cal.getTime();
    return sdf.format(currenTimeZone);
  }

  public static String convertUTCToTodayDateFormat(String time) {
    long timestamp = Long.parseLong(time);
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
    sdf.setTimeZone(getTimeZone());
    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
    cal.setTimeZone(Utility.getTimeZone());
    cal.setTimeInMillis(timestamp);
    //cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
    Date currenTimeZone = cal.getTime();
    return sdf.format(currenTimeZone);
  }


  /**
   * method for conveting utctime to custom time format
   *
   * @param time utc time
   * @return custom time format
   */
  public static String convertUTCToYesterdayDateFormat(String time) {
    long timestamp = Long.parseLong(time);
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
    sdf.setTimeZone(getTimeZone());
    Calendar cal = Calendar.getInstance(Locale.ENGLISH);
    cal.setTimeZone(Utility.getTimeZone());
    cal.setTimeInMillis(timestamp);
    //cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
    cal.add(Calendar.DATE, -1);
    Date currenTimeZone = cal.getTime();
    return sdf.format(currenTimeZone);
  }


  /**
   * method for converting utc time to timeStamp
   *
   * @param time utc time
   * @return timestamp
   */
  public static long convertUTCToTimeStamp(String time) {

    try {
      Calendar cal = Calendar.getInstance(Locale.ENGLISH);
      cal.setTimeZone(getTimeZone());
      cal.setTimeInMillis(TimeUnit.SECONDS.toMillis(Long.parseLong(time)));
      //cal.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));

      return cal.getTimeInMillis();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return 0;
  }

  public static RequestOptions createGlideDependent(Context mContext) {


    double[] size = Scaler.getScalingFactor(mContext);
    double height = (140) * size[1];
    double width = (140) * size[0];
    return new RequestOptions()
        .placeholder(R.drawable.default_photo)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .priority(Priority.HIGH)
        .fitCenter()
        .override(Target.SIZE_ORIGINAL)
        .format(DecodeFormat.PREFER_ARGB_8888)
        .skipMemoryCache(false)
        .error(R.drawable.default_photo)
        .transform(new CircleTransform(mContext));
  }

  public static RequestOptions createGlideSymptom(Context mContext) {

    return new RequestOptions()
        .diskCacheStrategy(DiskCacheStrategy.NONE)
        .skipMemoryCache(true)
        .dontAnimate()
        .placeholder(R.drawable.ic_fever_placeholder)
        .error(R.drawable.ic_fever_placeholder)
        .transform(new CircleTransform(mContext));
  }


  /*mSetDurationPresetRadioGroup = (PresetRadioGroup) findViewById(R.id.preset_time_radio_group);
  mSetDurationEditText = (AppCompatEditText) findViewById(R.id.edit_text_set_duration);
      mSetDurationPresetRadioGroup.setOnCheckedChangeListener(new PresetRadioGroup
      .OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(View radioGroup, View radioButton, boolean isChecked, int
      checkedId) {
          mSetDurationEditText.setText(((PresetValueButton) radioButton).getValue());
          mSetDurationEditText.setSelection(mSetDurationEditText.getText().length());
      }
  });*/
  public static void showFragment(Fragment fragment, String tag, FragmentManager fragmentManager) {
    String fragmentTag = "";
    if (tag.equals(fragment.getClass().getName())) {
      final int min = 0;
      final int max = 1000;
      final int random = new Random().nextInt((max - min) + 1) + min;
      fragmentTag = fragment.getClass().getName() + random;
      Log.d("fragmentTag", "showFragment: " + fragmentTag + " " + random);
    } else {
      fragmentTag = fragment.getClass().getName();
      Log.d("fragmentTag", "showFragment: " + fragmentTag);
    }
    //prefKey = fragmentTag;
    boolean fragmentPopped = fragmentManager.popBackStackImmediate(fragmentTag, 0);

    if (!fragmentPopped && fragmentManager.findFragmentByTag(fragmentTag)
        == null) { //fragment not in back stack, create it.
      FragmentTransaction ft = fragmentManager.beginTransaction();
      ft.setCustomAnimations(R.anim.in_from_right, R.anim.out_to_left,
          R.anim.in_from_left, R.anim.out_to_right);
      ft.add(R.id.content_frame, fragment, fragmentTag);
      ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
      ft.addToBackStack(fragmentTag);
      ft.commit();
    }
  }

  /**
   * method for printing the log
   *
   * @param TAG tag
   * @param log log message
   */
  public static void printLog(String TAG, String log) {
    Log.d(TAG, log);
  }


  /**
   * method for returing current time
   *
   * @return current time
   */
  public static String getCurrentTime() {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeZone(Utility.getTimeZone());
    Date currentTime = calendar.getTime();
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
    df.setTimeZone(Utility.getTimeZone());
    String mDeviceTime = df.format(currentTime);
    mDeviceTime = mDeviceTime.replace('T', ' ');
    return mDeviceTime;
  }


  public static String getAssetJsonData(Context context) {
    String json;
    try {
      InputStream is = context.getAssets().open("properties.json");
      int size = is.available();
      byte[] buffer = new byte[size];
      is.read(buffer);
      is.close();
      json = new String(buffer, StandardCharsets.UTF_8);
    } catch (IOException ex) {
      ex.printStackTrace();
      return null;
    }
    Log.e("data", json);
    return json;
  }


  public static String getEtaInUnitsFromDbDistanceCalculate(double distance,
                                                            Context context) {

    if (DISTANCEMATRIXUNIT == 0/*DistanceMatrix.UNIT_KM.ordinal()*/) {
      distanceSpeed = context.getResources().getString(R.string.km_away);
      return distance +" "+ distanceSpeed;//String.format("%.2f ", (float) distance);
    } else if (DISTANCEMATRIXUNIT == 1/*DistanceMatrix.UNIT_MILES.ordinal()*/) {
      distanceSpeed = context.getResources().getString(R.string.miles_away);
      return distance +" "+ distanceSpeed;//String.format("%.2f ", (float) distance);
    } else {
      distanceSpeed = context.getResources().getString(R.string.yards_away);
      return distance +" "+ distanceSpeed;//String.format("%.2f ", (float) distance);
    }
  }

  /**
   * <h2>animateFtomLeft</h2>
   * <p>
   * method to show animation effect from left
   * </p>
   *
   * @param context of the application such as activity/fragment
   */
  public static Animation animateFtomLeft(Context context) {
    return AnimationUtils.loadAnimation(context, R.anim.lfade);
  }

  /**
   * <h2>animateFtomRight</h2>
   * <p>
   * method to show animation effect from right
   * </p>
   *
   * @param context of the application such as activity/fragment
   */
  public static Animation animateFtomRight(Context context) {
    return AnimationUtils.loadAnimation(context, R.anim.rfade);
  }

  public static float convertSpToPixels(float sp, Context context) {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
  }

  public static void calculate(int width, int height) {
    w8 = (int) width * 8 / 320;
    w10 = (int) width * 10 / 320;
    w5 = (int) width * 5 / 320;
    w15 = (int) width * 15 / 320;
    w50 = (int) width * 50 / 320;
    w40 = (int) width * 40 / 320;
    w20 = (int) width * 20 / 320;
    w250 = (int) width * 250 / 320;

    h10 = (int) height * 10 / 480;
    h2 = (int) height * 2 / 480;
    h8 = (int) height * 8 / 480;
    h5 = (int) height * 5 / 480;
    h7 = (int) height * 7 / 480;
    h15 = (int) height * 15 / 480;
    h20 = (int) height * 20 / 480;
    h290 = (int) height * 290 / 480;
  }

  public static int getScreenWidth() {
    return Resources.getSystem().getDisplayMetrics().widthPixels;
  }

  public static int getScreenHeight() {
    return Resources.getSystem().getDisplayMetrics().heightPixels;
  }
  public static int bitrateValue() {
    int bitrate = 0;
    NetworkInfo info = getNetworkInfo(LSPApplication.getContext());
    if(info.getType()== ConnectivityManager.TYPE_MOBILE){
      switch(info.getSubtype()){

        case TelephonyManager.NETWORK_TYPE_1xRTT:
        case TelephonyManager.NETWORK_TYPE_EDGE:
          bitrate= BITRATE_128Kbps;
          break;   // ~ 50-100     -Kbps   slow-----
        case TelephonyManager.NETWORK_TYPE_CDMA:
          bitrate= BITRATE_128Kbps;
          break;   // ~ 14-64      -Kbps   slow-----
        case TelephonyManager.NETWORK_TYPE_EVDO_0:
          bitrate= BITRATE_128Kbps;
          break;    // ~ 400-1000   -Kbps
        case TelephonyManager.NETWORK_TYPE_EVDO_A:
          bitrate= BITRATE_128Kbps;
          break;    // ~ 600-1400   -Kbps
        case TelephonyManager.NETWORK_TYPE_GPRS:
          bitrate= BITRATE_128Kbps;
          break;   // ~ 100        -Kbps   slow-----
        case TelephonyManager.NETWORK_TYPE_HSDPA:
          bitrate= BITRATE_300Kbps;
          break;    // ~ 2000-1400  -Kbps
        case TelephonyManager.NETWORK_TYPE_HSPA:
          bitrate= BITRATE_128Kbps;
          break;    // ~ 700-1700   -Kbps
        case TelephonyManager.NETWORK_TYPE_HSUPA:
          bitrate= BITRATE_300Kbps;
          break;    // ~ 1000-2300  -Kbps
        case TelephonyManager.NETWORK_TYPE_UMTS:
          bitrate= BITRATE_300Kbps;
          break;    // ~ 400-7000   -Kbps
        case TelephonyManager.NETWORK_TYPE_EHRPD:
          bitrate= BITRATE_128Kbps;
          break;    // ~ 1000-2000  -Kbps   // API level 11
        case TelephonyManager.NETWORK_TYPE_EVDO_B:
          bitrate= BITRATE_3000Kbps;
          break;    // ~ 5000       -Kbps   // API level 9
        case TelephonyManager.NETWORK_TYPE_HSPAP:
          bitrate= BITRATE_3000Kbps;
          break;    // ~ 10000-20000-Kbps   // API level 13
        case TelephonyManager.NETWORK_TYPE_IDEN:
          bitrate= BITRATE_128Kbps;
          break;   // ~ 25         -Kbps   // API level 8
        case TelephonyManager.NETWORK_TYPE_LTE:
          bitrate= BITRATE_3000Kbps;
          break;    // ~ 10000+     -Kbps   // API level 11
        case TelephonyManager.NETWORK_TYPE_UNKNOWN:                 // ~ Unknown
          bitrate= BITRATE_128Kbps;
          break;
        default:
          break;
      }
    } else {
      bitrate=BITRATE_3000Kbps;
    }

    return bitrate;
  }

  /**
   * Get network info
   * @param context - For getting connectivity service
   * @return NetworkInfo
   */
  public static NetworkInfo getNetworkInfo(Context context){
    ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    return Objects.requireNonNull(connectivityManager).getActiveNetworkInfo();
  }


  public static String getFormattedAddress(YourAddrData address, Context context){
    String addressDetails = "";
    if (!TextUtils.isEmpty(address.getAddLine1())) {
      addressDetails = addressDetails + context.getResources().getString(R.string.street_1)+":" + address.getAddLine1() + "\n";
    }
    if (!TextUtils.isEmpty(address.getAddLine2())) {
      addressDetails = addressDetails + context.getResources().getString(R.string.street_2)+":" + address.getAddLine2() + "\n";
    }
    if (!TextUtils.isEmpty(address.getNeighbourHood())) {
      addressDetails = addressDetails + context.getResources().getString(R.string.neighbourhood)+":" + address.getNeighbourHood() + "\n";
    }
    if (!TextUtils.isEmpty(address.getCity())) {
      addressDetails = addressDetails + context.getResources().getString(R.string.city)+":" + address.getCity() + "\n";
    }
    if (!TextUtils.isEmpty(address.getCountry())) {
      addressDetails = addressDetails + context.getResources().getString(R.string.country)+":" + address.getCountry() + "\n";
    }
    if (!TextUtils.isEmpty(address.getPincode())) {
      addressDetails = addressDetails + context.getResources().getString(R.string.zip_code)+":" + address.getPincode()+ "\n";
    }
    if (!TextUtils.isEmpty(address.getReference())) {
      addressDetails = addressDetails + context.getResources().getString(R.string.notes)+":" + address.getReference();
    }
    return addressDetails;
  }

  private static final float BITMAP_SCALE = 0.4f;
  private static final int BLUR_RADIUS = 8;

  public static Bitmap fastblur(Bitmap sentBitmap) {
    float scale = BITMAP_SCALE;
    int radius = BLUR_RADIUS;
    int width = Math.round(sentBitmap.getWidth() * scale);
    int height = Math.round(sentBitmap.getHeight() * scale);
    sentBitmap = Bitmap.createScaledBitmap(sentBitmap, width, height, false);

    Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

    if (radius < 1) {
      return (null);
    }

    int w = bitmap.getWidth();
    int h = bitmap.getHeight();

    int[] pix = new int[w * h];
    Log.e("pix", w + " " + h + " " + pix.length);
    bitmap.getPixels(pix, 0, w, 0, 0, w, h);

    int wm = w - 1;
    int hm = h - 1;
    int wh = w * h;
    int div = radius + radius + 1;

    int r[] = new int[wh];
    int g[] = new int[wh];
    int b[] = new int[wh];
    int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
    int vmin[] = new int[Math.max(w, h)];

    int divsum = (div + 1) >> 1;
    divsum *= divsum;
    int dv[] = new int[256 * divsum];
    for (i = 0; i < 256 * divsum; i++) {
      dv[i] = (i / divsum);
    }

    yw = yi = 0;

    int[][] stack = new int[div][3];
    int stackpointer;
    int stackstart;
    int[] sir;
    int rbs;
    int r1 = radius + 1;
    int routsum, goutsum, boutsum;
    int rinsum, ginsum, binsum;

    for (y = 0; y < h; y++) {
      rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
      for (i = -radius; i <= radius; i++) {
        p = pix[yi + Math.min(wm, Math.max(i, 0))];
        sir = stack[i + radius];
        sir[0] = (p & 0xff0000) >> 16;
        sir[1] = (p & 0x00ff00) >> 8;
        sir[2] = (p & 0x0000ff);
        rbs = r1 - Math.abs(i);
        rsum += sir[0] * rbs;
        gsum += sir[1] * rbs;
        bsum += sir[2] * rbs;
        if (i > 0) {
          rinsum += sir[0];
          ginsum += sir[1];
          binsum += sir[2];
        } else {
          routsum += sir[0];
          goutsum += sir[1];
          boutsum += sir[2];
        }
      }
      stackpointer = radius;

      for (x = 0; x < w; x++) {

        r[yi] = dv[rsum];
        g[yi] = dv[gsum];
        b[yi] = dv[bsum];

        rsum -= routsum;
        gsum -= goutsum;
        bsum -= boutsum;

        stackstart = stackpointer - radius + div;
        sir = stack[stackstart % div];

        routsum -= sir[0];
        goutsum -= sir[1];
        boutsum -= sir[2];

        if (y == 0) {
          vmin[x] = Math.min(x + radius + 1, wm);
        }
        p = pix[yw + vmin[x]];

        sir[0] = (p & 0xff0000) >> 16;
        sir[1] = (p & 0x00ff00) >> 8;
        sir[2] = (p & 0x0000ff);

        rinsum += sir[0];
        ginsum += sir[1];
        binsum += sir[2];

        rsum += rinsum;
        gsum += ginsum;
        bsum += binsum;

        stackpointer = (stackpointer + 1) % div;
        sir = stack[(stackpointer) % div];

        routsum += sir[0];
        goutsum += sir[1];
        boutsum += sir[2];

        rinsum -= sir[0];
        ginsum -= sir[1];
        binsum -= sir[2];

        yi++;
      }
      yw += w;
    }
    for (x = 0; x < w; x++) {
      rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
      yp = -radius * w;
      for (i = -radius; i <= radius; i++) {
        yi = Math.max(0, yp) + x;

        sir = stack[i + radius];

        sir[0] = r[yi];
        sir[1] = g[yi];
        sir[2] = b[yi];

        rbs = r1 - Math.abs(i);

        rsum += r[yi] * rbs;
        gsum += g[yi] * rbs;
        bsum += b[yi] * rbs;

        if (i > 0) {
          rinsum += sir[0];
          ginsum += sir[1];
          binsum += sir[2];
        } else {
          routsum += sir[0];
          goutsum += sir[1];
          boutsum += sir[2];
        }

        if (i < hm) {
          yp += w;
        }
      }
      yi = x;
      stackpointer = radius;
      for (y = 0; y < h; y++) {
        // Preserve alpha channel: ( 0xff000000 & pix[yi] )
        pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

        rsum -= routsum;
        gsum -= goutsum;
        bsum -= boutsum;

        stackstart = stackpointer - radius + div;
        sir = stack[stackstart % div];

        routsum -= sir[0];
        goutsum -= sir[1];
        boutsum -= sir[2];

        if (x == 0) {
          vmin[y] = Math.min(y + r1, hm) * w;
        }
        p = x + vmin[y];

        sir[0] = r[p];
        sir[1] = g[p];
        sir[2] = b[p];

        rinsum += sir[0];
        ginsum += sir[1];
        binsum += sir[2];

        rsum += rinsum;
        gsum += ginsum;
        bsum += binsum;

        stackpointer = (stackpointer + 1) % div;
        sir = stack[stackpointer];

        routsum += sir[0];
        goutsum += sir[1];
        boutsum += sir[2];

        rinsum -= sir[0];
        ginsum -= sir[1];
        binsum -= sir[2];

        yi += w;
      }
    }

    Log.e("pix", w + " " + h + " " + pix.length);
    bitmap.setPixels(pix, 0, w, 0, 0, w, h);

    return (bitmap);

  }

  public static String gender(String value){
    String genderStr="";
    switch (value)
    {
      case "1":
      case "Male":
        genderStr = "Male";
        break;
      case "2":
      case "Female":
        genderStr = "Female";
        break;
      case "3":
      case "Other":
        genderStr = "Other";
        break;
      default:
        genderStr = "Male";
        break;
    }
    return genderStr;
  }
  public static void  setTextSize14Sp(Context mContext,TextView textView){
    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
  }
  public static void  setTextSize16Sp(Context mContext,TextView textView){
    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
  }
  public static void  setTextSize18Sp(Context mContext,TextView textView){
    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_18));
  }
  public static void  setTextSize12Sp(Context mContext,TextView textView){
    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_12));
  }
  public static void  setTextSize10Sp(Context mContext,TextView textView){
    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_10));
  }
  public static void  setTextSize20Sp(Context mContext,TextView textView){
    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_20));
  }
  public static void  setTextSize22Sp(Context mContext,TextView textView){
    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_22));
  }
  public static void  setTextSize24Sp(Context mContext,TextView textView){
    textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_24));
  }

  /**
   * Returns true if the string is null or 0-length.
   *
   * @param str the string to be examined
   * @return true if str is null or zero length
   */
  public static boolean isTextEmpty(CharSequence str) {
    return str == null || str.toString().trim().length() == 0;
  }
  public static boolean isFromIndia(Context context) {
    return /*getDeviceCountryCode(context).equals("in")*/razorPayKey.length() > 0;
  }

  private static String getDeviceCountryCode(Context context) {
    String countryCode;

    // Try to get country code from TelephonyManager service
    TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    if(tm != null) {
      // Query first getSimCountryIso()
      countryCode = tm.getSimCountryIso();
      if (countryCode != null && countryCode.length() == 2)
        return countryCode.toLowerCase();

      if (tm.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
        // Special case for CDMA Devices
        countryCode = getCDMACountryIso();
      }
      else {
        // For 3G devices (with SIM) query getNetworkCountryIso()
        countryCode = tm.getNetworkCountryIso();
      }

      if (countryCode != null && countryCode.length() == 2)
        return countryCode.toLowerCase();
    }

    // If network country not available (tablets maybe), get country code from Locale class
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      countryCode = context.getResources().getConfiguration().getLocales().get(0).getCountry();
    }
    else {
      countryCode = context.getResources().getConfiguration().locale.getCountry();
    }

    if (countryCode != null && countryCode.length() == 2)
      return  countryCode.toLowerCase();

    // General fallback to "us"
    return "us";
  }

  @SuppressLint("PrivateApi")
  private static String getCDMACountryIso() {
    try {
      // Try to get country code from SystemProperties private class
      Class<?> systemProperties = Class.forName("android.os.SystemProperties");
      Method get = systemProperties.getMethod("get", String.class);

      // Get homeOperator that contain MCC + MNC
      String homeOperator = ((String) get.invoke(systemProperties,
              "ro.cdma.home.operator.numeric"));

      // First three characters (MCC) from homeOperator represents the country code
      int mcc = Integer.parseInt(homeOperator.substring(0, 3));

      // Mapping just countries that actually use CDMA networks
      switch (mcc) {
        case 330: return "PR";
        case 310: return "US";
        case 311: return "US";
        case 312: return "US";
        case 316: return "US";
        case 283: return "AM";
        case 460: return "CN";
        case 455: return "MO";
        case 414: return "MM";
        case 619: return "SL";
        case 450: return "KR";
        case 634: return "SD";
        case 434: return "UZ";
        case 232: return "AT";
        case 204: return "NL";
        case 262: return "DE";
        case 247: return "LV";
        case 255: return "UA";
      }
    }
    catch (ClassNotFoundException ignored) {
    }
    catch (NoSuchMethodException ignored) {
    }
    catch (IllegalAccessException ignored) {
    }
    catch (InvocationTargetException ignored) {
    }
    catch (NullPointerException ignored) {
    }

    return null;
  }
}
