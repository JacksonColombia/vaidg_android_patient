package com.vaidg.utilities;

/**
 * Created by ${3Embed} on 22/6/18.
 */
public enum DistanceMatrix {
    UNIT_KM,UNIT_MILES,UNIT_YARDS;
}
