package com.vaidg.utilities;

public interface RemoveFavCallBack{
    void onRemoveFav(String catId, String providerId);
}
