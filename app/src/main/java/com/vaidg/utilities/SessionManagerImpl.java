package com.vaidg.utilities;

import com.pojo.LanguagesList;

/**
 * <h2>SessionManagerImpl</h2>
 * Created by Ali on 1/30/2018.
 */

public interface SessionManagerImpl {

  void clearSession();

  String getSID();

  void setSID(String sid);

  String getRegistrationId();

  void setRegistrationId(String registrationId);

  String getAUTH();

  void setAUTH(String auth);

  String getREFRESHAUTH();

  void setREFRESHAUTH(String auth);

  void setHashMap(String key, String value);

  String getHashMap(String mapKey, String key);

  String getScheduleData();

  void setScheduleData(String data);

  String getLatitude();

  void setLatitude(String latitude);

  String getLongitude();

  void setLongitude(String longitude);

  String getAddress();

  void setAddress(String address);

  String getHomeScreenData();

  void setHomeScreenData(String Data);

  String getVirgilPrivateKey();

  void setVirgilPrivateKey(String virgilPrivateKey);

  String getVirgilPrivateKeyT();

  void setVirgilPrivateKeyT(String virgilPrivateKeyT);

  /**
   * <h2>storeEncryptionPrivateKey</h2>
   * This method is used to store the encryption key
   *
   * @param encryptedPrivateKey encryption key to be stored
   */
  void storeEncryptionPrivateKey(String encryptedPrivateKey);

  /**
   * <h2>getEncryptionPrivateKey</h2>
   * This method is used to get the encryption key
   *
   * @return encryption key
   */
  String getEncryptionPrivateKey();

  /**
   * <h2>storeEncryptionPublicKey</h2>
   * This method is used to store the encryption key
   *
   * @param encryptedPublicKey encryption key to be stored
   */
  void storeEncryptionPublicKey(String encryptedPublicKey);

  /**
   * <h2>getEncryptionKey</h2>
   * This method is used to get the encryption key
   *
   * @return encryption key
   */
  String getEncryptionPublicKey();

  String getVirgilPublicKey();

  void setVirgilPublicKey(String virgilPublicKey);

  String getVirgilPublicKeyT();

  void setVirgilPublicKeyT(String virgilPublicKeyT);

  String getVirgilDoctorPublicKey();

  void setVirgilDoctorPublicKey(String publicKey);

  String getFirstName();

  void setFirstName(String firstName);

  String getPatientGender();

  void setPatientGender(String patientGender);

  String getPatientDateOfBirth();

  void setPatientDateOfBirth(String patientDateOfBirth);

  String getLastName();

  void setLastName(String lastName);

  String getEmail();

  void setEmail(String email);

  String getMobileNo();

  void setMobileNo(String mobileNo);

  String getAbout();

  void setAbout(String about);

  String getRegisterId();

  void setRegisterId(String registerId);

  String getReferralCode();

  void setReferralCode(String refCode);

  boolean getGuestLogin();

  void setGuestLogin(boolean guestLogin);

  boolean isProfileCalled();

  void setProfileCalled(boolean isProfileCalled);

  String getProfilePicUrl();

  void setProfilePicUrl(String profilePicUrl);

  String getCountryCode();

  void setCountryCode(String countryCode);

  String getCountrySymbol();

  void setCountrySymbol(String countrySymbol);

  long getChatBookingID();

  void setChatBookingID(long bookingID);

  String getChatProId();

  void setChatProId(String chatProId);

  String getChatProPic();

  void setChatProPic(String chatProPic);

  String getProName();

  void setProName(String proName);

  int getBookingStatus(long bookingId);

  void setBookingStatus(long bookingId, int status);

  int getChatCount(long bookingId);

  void setChatCount(long bookingId, int chatCount);

  void clearChatCountPreference(long bookingId);

  void clearVirgilPrivatePublicKeyT();

  long getLastTimeStampMsg();

  void setLastTimeStampMsg(long lastTimeStampMsg);

  String getDefaultCardName();

  void setDefaultCardName(String defaultCardName);

  String getIpAddress();

  void setIpAddress(String ipAddress);

  String getDefaultCardNum();

  void setDefaultCardNum(String defaultCardNum);

  String getDefaultCardId();

  void setDefaultCardId(String defaultCardId);

  String getFcmTopic();

  void setFcmTopic(String fcmTopic);

  String getFcmTopicCity();

  void setFcmTopicCity(String fcmTopicCity);

  String getFcmTopicAllCustomer();

  void setFcmTopicAllCustomer(String fcmTopicAllCustomer);

  String getFcmTopicAllCity();

  void setFcmTopicAllCity(String fcmTopicAllCity);


  String getContactName();

  void setContactName(String contactName);

  String getContactPicUrl();

  void setContactPicUrl(String contactPicUrl);

  LanguagesList getLanguageSettings();

  void setLanguageSettings(LanguagesList languageSettings);

  int getAppOpenTime();

  void setAppOpenTime(int AppOpenTime);

  boolean getDontShowRate();

  void setDontShowRate(boolean dontOpenRate);

  boolean isAppOpen();

  void setAppOpen(boolean isAppOpen);

  String getCallToken();

  void setCallToken(String authToken);

  long getExpireOtp();

  void setExpireOtp(long expireOtp);


   String getDependentFname();

   void setDependentFname(String dependentFname);

   String getDependentLname();

   void setDependentLname(String dependentLname);

   String getGender();

   void setGender(String gender);

   String getDependentImage();

   void setDependentImage(String dependentImage);

   String getDependentMobileNo();

   void setDependentMobileNo(String dependentMobileNo);

   String getDependentCountryCodeSymbol();

   void setDependentCountryCodeSymbol(String dependentCountryCodeSymbol);

   String getDateOBirth();

   void setDateOBirth(String dateOBirth);
}

