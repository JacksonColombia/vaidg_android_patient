package com.vaidg.utilities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.DownloadListener;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.R;
import com.vaidg.rateYourBooking.adapter.PrescriptionAdapter;
import com.pojo.InvoiceDetails;
import com.utility.PicassoTrustAll;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static android.content.Context.DOWNLOAD_SERVICE;

public class FullscreenWebViewDialog extends BaseFullscreenDialog {

  private final static String TAG = "ProgressDialog";
  private static final int MY_PERMISSION_REQUEST_CODE = 123;
  private final float DEFAULT_DIM_AMOUNT = 0.7f;
  private final int DISMISS_DELAY = 100; // ms
  final private Handler mHandler = new Handler();
  private String mMessage;
  private TextView tvMessage;
  private Runnable mRunnable;
  private float mDimAmount = DEFAULT_DIM_AMOUNT;
  private int mTintColor = R.color.colorAccent;
  private String pdfFile = "";
  private String categoryName = "";
  private String docDegree = "";
  private String name = "";
  private String profilePic = "";
  private long mDateAndTime = 0;
  private ArrayList<InvoiceDetails.Medication> medications = new ArrayList<>();
  private Context mContext;
  private boolean loadingFinished = true;
  private boolean redirect = false;
  private TextView tv_skip;

  public static FullscreenWebViewDialog dialogWithCustomMessage(String message) {
    FullscreenWebViewDialog progressDialog = new FullscreenWebViewDialog();
    progressDialog.mMessage = message;
    return progressDialog;
  }

  public Context getmContext() {
    return mContext;
  }

  public void setmContext(Context mContext) {
    this.mContext = mContext;
  }

  public String getProfilePic() {
    return profilePic;
  }

  public void setProfilePic(String profilePic) {
    this.profilePic = profilePic;
  }

  public String getDocDegree() {
    return docDegree;
  }

  public void setDocDegree(String docDegree) {
    this.docDegree = docDegree;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public ArrayList<InvoiceDetails.Medication> getMedications() {
    return medications;
  }

  public void setMedications(ArrayList<InvoiceDetails.Medication> medications) {
    this.medications = medications;
  }

  public long getmDateAndTime() {
    return mDateAndTime;
  }

  public void setmDateAndTime(long mDateAndTime) {
    this.mDateAndTime = mDateAndTime;
  }

  public float getDimAmount() {
    return mDimAmount;
  }

  public void setDimAmount(float dimAmount) {
    mDimAmount = dimAmount;
  }

  public int getTintColor() {
    return mTintColor;
  }

  public void setTintColor(int tintColor) {
    mTintColor = tintColor;
  }

  public String getPdfFile() {
    return pdfFile;
  }

  public void setPdfFile(String pdfFile) {
    this.pdfFile = pdfFile;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fullscreen_progress_dialog, container, false);
    mContext = getmContext();
    setCancelable(true);
    getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
    getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    getDialog().getWindow().setDimAmount(DEFAULT_DIM_AMOUNT);
    getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    // Get the widget reference from xml layout
    RelativeLayout mRootLayout = view.findViewById(R.id.root_layout);
    LinearLayout lvDocDetail = view.findViewById(R.id.lvDocDetail);
    WebView mWebViewDownload = view.findViewById(R.id.web_view);
    View viewLine = view.findViewById(R.id.viewLine);
    ImageView ivMyEventProPic = view.findViewById(R.id.ivMyEventProPic);
    TextView tvMyEventProName = view.findViewById(R.id.tvMyEventProName);
    TextView tvMyEvnetProStatus = view.findViewById(R.id.tvMyEvnetProStatus);
    TextView tvMytimedate = view.findViewById(R.id.tvMytimedate);
    RecyclerView rvPrecription = view.findViewById(R.id.rvPrecription);
    Toolbar toolPrecription = view.findViewById(R.id.toolbar);
    TextView tv_center = view.findViewById(R.id.tv_center);
    tv_skip = view.findViewById(R.id.tv_skip);
    AppTypeface appTypeface = AppTypeface.getInstance(mContext);
    tvMyEventProName.setTypeface(appTypeface.getHind_semiBold());
    tv_center.setTypeface(appTypeface.getHind_semiBold());
    tvMyEvnetProStatus.setTypeface(appTypeface.getHind_regular());
    tv_skip.setTypeface(appTypeface.getHind_semiBold());
    tvMytimedate.setTypeface(appTypeface.getHind_regular());
    lvDocDetail.setVisibility(View.GONE);
    viewLine.setVisibility(View.GONE);
    rvPrecription.setVisibility(View.GONE);
    mWebViewDownload.setVisibility(View.VISIBLE);

    tvMyEventProName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_18));
    tvMyEvnetProStatus.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_16));
    tvMytimedate.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_14));
    // tvMedication.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_14));

    ((AppCompatActivity) mContext).setSupportActionBar(toolPrecription);
    if (((AppCompatActivity) mContext).getSupportActionBar() != null) {
      ((AppCompatActivity) mContext).getSupportActionBar().setDisplayShowHomeEnabled(false);
      ((AppCompatActivity) mContext).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
      ((AppCompatActivity) mContext).getSupportActionBar().setDisplayShowTitleEnabled(false);
      toolPrecription.setNavigationIcon(R.drawable.ic_back);
      toolPrecription.setNavigationOnClickListener(v -> dismiss());
    }
    tv_center.setText(getString(R.string.prescription));
    tv_skip.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_printer, 0, 0, 0);
    tv_skip.setText(getResources().getString(R.string.print));
    tv_skip.setGravity(Gravity.CENTER_VERTICAL);
    tv_skip.setCompoundDrawablePadding(15);
    tv_skip.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
    tv_skip.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
    try {

      Date date = new Date(TimeUnit.SECONDS.toMillis(getmDateAndTime()));
      tvMytimedate.setText(Utility.getFormattedDate(date));
    } catch (Exception e) {
      Log.d("TAG", "timeMethodException: " + e.toString());
    }
    if (getProfilePic() != null && !getProfilePic().isEmpty()) {
      PicassoTrustAll.getInstance(mContext)
              .load(getProfilePic())
              .placeholder(R.drawable.register_profile_default_image)   // optional
              .error(R.drawable.register_profile_default_image)// optional
              .transform(new PicassoCircleTransform())
              .into(ivMyEventProPic);

    }
    tvMyEventProName.setText(getName());
    String docDesc = "";
    if (getDocDegree().isEmpty()) {
      docDesc = getCategoryName();
    } else if (getCategoryName().isEmpty()) {
      docDesc = getDocDegree();
    } else {
      docDesc = getDocDegree() + "," + getCategoryName();
    }
    tvMyEvnetProStatus.setText(docDesc);

    // Check permission for write external storagehttps://drive.google.com/viewerng/viewer?embedded=true&url="
    //checkPermission();
    LinearLayoutManager llManager = new LinearLayoutManager(mContext);
    rvPrecription.setLayoutManager(llManager);
    PrescriptionAdapter prescriptionAdapter = new PrescriptionAdapter(medications);
    rvPrecription.setAdapter(prescriptionAdapter);
    // The target url to surf using web view
    String url = getPdfFile();
  //  String myPdfUrl = "http://docs.google.com/gview?embedded=true&url=" + url;
    String myPdfUrl = "https://drive.google.com/viewerng/viewer?embedded=true&url="+ url;
  if(!url.isEmpty()) {
    Log.w(TAG, "onCreateView: " + myPdfUrl);
    mWebViewDownload.setVisibility(View.VISIBLE);
   /* mWebViewDownload.getSettings().setJavaScriptEnabled(true);
    mWebViewDownload.getSettings().setLoadWithOverviewMode(true);
    mWebViewDownload.getSettings().setUseWideViewPort(true);
    mWebViewDownload.getSettings().setDomStorageEnabled(true); // Add this
    mWebViewDownload.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);*/
    WebSettings settings = mWebViewDownload.getSettings();
    settings.setJavaScriptEnabled(true);
   // settings.setAllowFileAccess(true);
  //  settings.setPluginState(WebSettings.PluginState.ON);
    settings.setDomStorageEnabled(true);
    mWebViewDownload.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
   // mWebViewDownload.loadData(myPdfUrl, "text/html", "UTF-8");
    PdfWebViewClient pdfWebViewClient = new PdfWebViewClient(mContext, mWebViewDownload);
    pdfWebViewClient.loadPdfUrl(myPdfUrl);
    // mWebViewDownload.loadUrl(myPdfUrl);

    tv_skip.setOnClickListener(v -> {
      if (isWriteStoragePermissionGranted()) {
        mWebViewDownload.getSettings().setJavaScriptEnabled(true);
        mWebViewDownload.getSettings().setLoadWithOverviewMode(true);
        mWebViewDownload.getSettings().setUseWideViewPort(true);
        mWebViewDownload.getSettings().setDomStorageEnabled(true); // Add this
        mWebViewDownload.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        mWebViewDownload.setDownloadListener(new DownloadListener() {
          @Override
          public void onDownloadStart(String url1, String userAgent, String contentDescription,
                                      String mimetype, long contentLength) {
              /*
                  DownloadManager.Request
                      This class contains all the information necessary to request a new download.
                      The URI is the only required parameter. Note that the default download
                      destination is a shared volume where the system might delete your file
                      if it needs to reclaim space for system use. If this is a problem,
                      use a location on external storage (see setDestinationUri(Uri).
              */

            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url1));
            Log.w(TAG, "onCreateView:Three " + url1);
             /*
                  void allowScanningByMediaScanner ()
                      If the file to be downloaded is to be scanned by MediaScanner, this method
                      should be called before enqueue(Request) is called.
              */
            request.allowScanningByMediaScanner();

            /*
                  DownloadManager.Request setNotificationVisibility (int visibility)
                      Control whether a system notification is posted by the download manager
                      while this download is running or when it is completed. If enabled, the
                      download manager posts notifications about downloads through the system
                      NotificationManager. By default, a notification is shown only
                      when the download is in progress.

                      It can take the following values: VISIBILITY_HIDDEN, VISIBILITY_VISIBLE,
                      VISIBILITY_VISIBLE_NOTIFY_COMPLETED.

                      If set to VISIBILITY_HIDDEN, this requires the permission
                      android.permission.DOWNLOAD_WITHOUT_NOTIFICATION.

                  Parameters
                      visibility int : the visibility setting value
                  Returns
                      DownloadManager.Request this object
              */
            request.setNotificationVisibility(
                    DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

              /*
                  DownloadManager
                      The download manager is a system service that handles long-running HTTP
                      downloads. Clients may request that a URI be downloaded to a particular
                      destination file. The download manager will conduct the download in the
                      background, taking care of HTTP interactions and retrying downloads
                      after failures or across connectivity changes and system reboots.
              */

   /*
                  String guessFileName (String url, String contentDisposition, String mimeType)
                      Guesses canonical filename that a download would have, using the URL
                      and contentDisposition. File extension, if not defined,
                      is added based on the mimetype

                  Parameters
                      url String : Url to the content
                      contentDisposition String : Content-Disposition HTTP header or null
                      mimeType String : Mime-type of the content or null

                  Returns
                      String : suggested filename
              */
            String fileName = URLUtil.guessFileName(url1, contentDescription, mimetype);
            Log.w(TAG, "onCreateView:Three " + fileName);
              /*
                  DownloadManager.Request setDestinationInExternalPublicDir
                  (String dirType, String subPath)

                      Set the local destination for the downloaded file to a path within
                      the public external storage directory (as returned by
                      getExternalStoragePublicDirectory(String)).

                      The downloaded file is not scanned by MediaScanner. But it can be made
                      scannable by calling allowScanningByMediaScanner().

                  Parameters
                      dirType String : the directory type to pass to
                                       getExternalStoragePublicDirectory(String)
                      subPath String : the path within the external directory, including
                                       the destination filename

                  Returns
                      DownloadManager.Request this object

                  Throws
                      IllegalStateException : If the external storage directory cannot be
                                              found or created.
              */

            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);

            DownloadManager dManager = (DownloadManager) LSPApplication.getInstance().getSystemService(DOWNLOAD_SERVICE);

              /*
                  long enqueue (DownloadManager.Request request)
                      Enqueue a new download. The download will start automatically once the
                      download manager is ready to execute it and connectivity is available.

                  Parameters
                      request DownloadManager.Request : the parameters specifying this download

                  Returns
                      long : an ID for the download, unique across the system. This ID is used
                             to make future calls related to this download.
              */
            if (dManager != null) {
              dManager.enqueue(request);
            }
          }
        });
        mWebViewDownload.loadUrl(url);
      } else {
        ActivityCompat.requestPermissions(((Activity) mContext), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
      }
    });
  }else{
    mWebViewDownload.setVisibility(View.GONE);
  }
    return view;
  }

  public void setMessage(String message) {
    if (message != null && tvMessage != null) {
      tvMessage.setText(message);
    }
  }

  public void show(int delay, final AppCompatActivity activity) {
    mRunnable = new Runnable() {
      @Override
      public void run() {
        show(activity);
      }
    };
    mHandler.postDelayed(mRunnable, delay);
  }

  public void show(AppCompatActivity activity) {
    if (activity != null && !activity.isFinishing() && !isShowing() && !isAdded()) {
      super.show(activity.getSupportFragmentManager(), TAG);
    }
  }

  public void show(Fragment fragment) {
    if (fragment != null) {
      FragmentManager fragmentManager = fragment.getFragmentManager();
      if (fragmentManager != null && !fragmentManager.isDestroyed() && !isShowing() || !isAdded()) {
        super.show(fragmentManager, TAG);
      }
    }
  }

  public boolean isShowing() {
    Dialog dialog = getDialog();
    return dialog != null && dialog.isShowing();
  }

  @Override
  public void dismiss() {
    if (mRunnable != null) {
      mHandler.removeCallbacks(mRunnable);
      mRunnable = null;
    }

    mHandler.postDelayed(new Runnable() {
      @Override
      public void run() {
        if (isShowing()) {
          try {
            FullscreenWebViewDialog.super.dismissAllowingStateLoss();
          } catch (IllegalStateException ignored) {

          }
        }
      }
    }, DISMISS_DELAY);
  }


  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    switch (requestCode) {
      case 2:
        Log.d(TAG, "External storage2");
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
          //resume tasks needing this permission
          tv_skip.performClick();
        } else {
          //progress.dismiss();
          ActivityCompat.requestPermissions(((Activity) mContext), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        }
        break;

      case 3:
        Log.d(TAG, "External storage1");
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
          //resume tasks needing this permission
          tv_skip.performClick();
        } else {
          // progress.dismiss();
          ActivityCompat.requestPermissions(((Activity) mContext), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);

        }
        break;
    }
  }

  public boolean isReadStoragePermissionGranted() {
    if (Build.VERSION.SDK_INT >= 23) {
      if (mContext.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
          == PackageManager.PERMISSION_GRANTED) {
        Log.v(TAG, "Permission is granted1");
        return true;
      } else {

        Log.v(TAG, "Permission is revoked1");
        ActivityCompat.requestPermissions(((Activity) mContext), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
        return false;
      }
    } else { //permission is automatically granted on sdk<23 upon installation
      Log.v(TAG, "Permission is granted1");
      return true;
    }
  }

  public boolean isWriteStoragePermissionGranted() {
    if (Build.VERSION.SDK_INT >= 23) {
      if (mContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
          == PackageManager.PERMISSION_GRANTED) {
        Log.v(TAG, "Permission is granted2");
        return true;
      } else {

        Log.v(TAG, "Permission is revoked2");
        ActivityCompat.requestPermissions(((Activity) mContext), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        return false;
      }
    } else { //permission is automatically granted on sdk<23 upon installation
      Log.v(TAG, "Permission is granted2");
      return true;
    }
  }

}
