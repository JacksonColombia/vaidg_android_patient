package com.vaidg.utilities;

import android.content.Context;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.vaidg.R;
import com.vaidg.model.CognitoIdRespomse;
import com.vaidg.networking.LSPServices;
import com.vaidg.networking.ServiceFactory;
import com.google.gson.Gson;
import com.pojo.ErrorHandel;
import com.utility.AlertProgress;
import com.utility.RefreshToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.BuildConfig.AMAZON_BASE_URL;
import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;

/**
 * Created by Ali on 28/1/16.
 */
public class UploadAmazonS3 {

    private static UploadAmazonS3 uploadAmazonS3;
    private static SessionManager sessionManager;
    private CognitoCachingCredentialsProvider credentialsProvider = null;
    private AmazonS3Client s3Client = null;
    private TransferUtility transferUtility = null;
    private DeveloperAuthenticationProvider developerProvider;

    /**
     * Creating single tone object by defining private.
     * <p>
     * At the time of creating
     * </P>
     */
    private UploadAmazonS3(Context context) {
         /* credentialsProvider=getCredentialProvider(context,canito_pool_id);
         *//**
         * Creating the object  of the s3Client *//*
        s3Client=getS3Client(context,credentialsProvider);
        s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
        s3Client.setEndpoint("https://s3-ap-southeast-1.amazonaws.com/");

        *//**
         * Creating the object of the TransferUtility of the Amazone.*//*
        transferUtility=getTransferUtility(context,s3Client);*/

    }

    public static UploadAmazonS3 getInstance(Context context) {
        sessionManager = new SessionManager(context);
        if (uploadAmazonS3 == null) {
            uploadAmazonS3 = new UploadAmazonS3(context);
            return uploadAmazonS3;
        } else {
            return uploadAmazonS3;
        }

    }

    /**
     * <h3>Upload_data</h3>
     * <p>
     * Method is use to upload data in the amazone server.
     *
     * </P>
     */

    public void Upload_data(Context context, final String bukkate_name, final File file, final Upload_CallBack callBack) {

        getCognitoToken(context, bukkate_name, file, callBack);
    }


    public void getCognitoToken(Context context, String bukkate_name, File file,
                                Upload_CallBack callBack) {

        LSPServices lspServices = ServiceFactory.createRetrofitService(LSPServices.class,context);

        Observable<Response<ResponseBody>> obsResponseBody = lspServices.onTOGetCognitoToken(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()), Constants.selLang, Constants.PLATFORM_ANDROID);
        obsResponseBody.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> value) {
                        //  Log.d(TAG, "onNext: " + value.code());
                        int code = value.code();
                        JSONObject jsonObject;
                        try {
                            String response = value.body() != null ? value.body().string() : null;
                            String errorBody = value.errorBody() != null ? value.errorBody().string() : null;
                            switch (code) {
                                case SUCCESS_RESPONSE:
                                    if (response != null && !response.isEmpty()) {
                                        CognitoIdRespomse cognitoIdRespomse = new Gson().fromJson(response,
                                                CognitoIdRespomse.class);
                                        Constants.Amazonbucket = cognitoIdRespomse.getData().getBucket();
                                        Constants.Amazoncognitoid = cognitoIdRespomse.getData().getIdentityId();
                                        Constants.AmazoncognitoToken = cognitoIdRespomse.getData().getToken();
                                        Constants.AmazonRegion = cognitoIdRespomse.getData().getRegion();
                                        /**
                                         * Creating the object of the getCredentialProvider object. */
                                        developerProvider = new DeveloperAuthenticationProvider(null,
                                                cognitoIdRespomse.getData().getIdentityId(), Regions.fromName(cognitoIdRespomse.getData().getRegion()));

                                        onSuccessCognitoTo(context, bukkate_name, file, callBack,cognitoIdRespomse.getData().getRegion());
                                    }
                                    break;
                                case Constants.SESSION_EXPIRED:
                                    if (errorBody != null && !errorBody.isEmpty()) {

                                        ErrorHandel errorHandel = new Gson().fromJson(response, ErrorHandel.class);
                                        RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()), sessionManager.getREFRESHAUTH(),
                                                lspServices,
                                                new RefreshToken.RefreshTokenImple() {
                                                    @Override
                                                    public void onSuccessRefreshToken(String newToken) {
                                                        LSPApplication.getInstance().setAuthToken(sessionManager.getSID(),sessionManager.getSID(),newToken);
                                                        getCognitoToken(context, bukkate_name, file,
                                                                callBack);
                                                    }

                                                    @Override
                                                    public void onFailureRefreshToken() {

                                                    }

                                                    @Override
                                                    public void sessionExpired(String msg) {
                                                        AlertProgress mAlertProgress = new AlertProgress(context);
                                                        mAlertProgress.alertPositiveOnclick(context, msg, context.getResources().getString(R.string.logout),
                                                                context.getResources().getString(R.string.ok), isClicked -> Utility.setMAnagerWithBID(context, sessionManager));

                                                    }
                                                });
                                        break;
                                    }
                                default:
                                    if (errorBody != null && !errorBody.isEmpty()) {
                                        jsonObject = new JSONObject(errorBody);
                                        if (jsonObject.getString(MESSAGE) != null && !jsonObject.getString(
                                                MESSAGE).isEmpty()) {
                                            Utility.printLog("Response", " ------>>> " + jsonObject);
                                        }

                                    }
                                    break;
                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }

    private void onSuccessCognitoTo(Context context, String bukkate_name, File file,
                                    Upload_CallBack callBack,String region) {
        getCredentialProvider(context,region);

        if (transferUtility != null && file != null) {

            TransferObserver observer = transferUtility.upload(Constants.Amazonbucket,
                    bukkate_name + "/" + file.getName(), file, CannedAccessControlList.PublicRead);

            observer.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (state.equals(TransferState.COMPLETED)) {
                        callBack.sucess("https://" + Constants.Amazonbucket + "." + AMAZON_BASE_URL
                                + bukkate_name + "/"
                                + file.getName());

                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

                }

                @Override
                public void onError(int id, Exception ex) {
                    callBack.error(id + ":" + ex.toString());

                }
            });


        } else {
            callBack.error("Amamzon s3 is not intialize or File is empty !");
        }

    }


    /**
     * This is the method to upload data in amazon s3. Here file name is passed separately
     *
     * @param bukkate_name Bucket name
     * @param fileName     The name which the file is to be uploaded
     * @param file         The file to be uploaded
     * @param callBack     Overidden method
     */
    public void UploadDataWithName(final String bukkate_name, final String fileName, final File file, final Upload_CallBack callBack) {

        if (transferUtility != null && file != null) {

            TransferObserver observer = transferUtility.upload(bukkate_name, fileName, file);

            observer.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (state.equals(TransferState.COMPLETED)) {
                        callBack.sucess("https://" + Constants.Amazonbucket + "." + AMAZON_BASE_URL
                                + bukkate_name + "/"
                                + file.getName());

                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

                }

                @Override
                public void onError(int id, Exception ex) {
                    callBack.error(id + ":" + ex.toString());

                }
            });

        } else {
            callBack.error("Amamzon s3 is not intialize or File is empty !");
        }

    }

    /**
     * <h2>delete_Item</h2>
     * <P>Deleting the item from aws.</P>
     *
     * @param bucketName Aws bucket name
     * @param keyName    file name in the aws folder.
     */
    public void delete_Item(String bucketName, String keyName) {
        try {
            if (s3Client != null)
                // s3Client.deleteObject(bucketName,keyName);
                s3Client.deleteObject(new DeleteObjectRequest(bucketName, keyName));
        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException.");
            System.out.println("Error Message: " + ace.getMessage());
        }
    }


    /**
     * This method is used to get the CredentialProvider and we provide only context as a parameter.
     *
     * @param context Here, we are getting the context from calling Activity.
     */
    private CognitoCachingCredentialsProvider getCredentialProvider(Context context,String region) {
        //Call this method from success response of fetch token
        if (!Constants.AmazoncognitoToken.isEmpty()
                && !Constants.Amazoncognitoid.isEmpty()) {

            developerProvider.updateCredentials();

            credentialsProvider = new CognitoCachingCredentialsProvider(context, developerProvider, Regions.fromName(region));
            /* *
             * Creating the object  of the s3Client */
            s3Client = getS3Client();

            /* *
             * Creating the object of the TransferUtility of the Amazone.*/
            transferUtility = getTransferUtility(context, s3Client);
        }

       /* if (credentialsProvider == null)
        {
            credentialsProvider = new CognitoCachingCredentialsProvider(
                    context.getApplicationContext(),
                    pool_id, // Identity Pool ID
                    Regions.US_EAST_1 // Region//us-east-1
                    //Regions.AP_NORTHEAST_1 // Region
            );
        }*/
        return credentialsProvider;
    }

    /**
     * This method is used to get the AmazonS3 Client
     * and we provide only context as a parameter.
     * and from here we are calling getCredentialProvider() function.
     * Here, we are getting the context from calling Activity.
     */
    private AmazonS3Client getS3Client() {
        s3Client = new AmazonS3Client(credentialsProvider);
      /*  if (s3Client == null)
        {
            s3Client = new AmazonS3Client(cognitoCachingCredentialsProvider);
        }*/
        return s3Client;
    }

    /**
     * This method is used to get the Transfer Utility
     * and we provide only context as a parameter.
     * and from here we are, calling getS3Client() function.
     *
     * @param context Here, we are getting the context from calling Activity.
     */
    private TransferUtility getTransferUtility(Context context, AmazonS3Client amazonS3Client) {
        if (transferUtility == null) {
            transferUtility = new TransferUtility(amazonS3Client, context.getApplicationContext());
        }
        return transferUtility;
    }


    /**
     * Interface for the sucess callback fro the Amazon uploading .
     */
    public interface Upload_CallBack {
        /**
         * Method for sucess .
         *
         * @param sucess it is true on sucess and false for falure.
         */
        void sucess(String sucess);

        /**
         * Method for falure.
         *
         * @param errormsg contains the error message.
         */
        void error(String errormsg);

    }
}
