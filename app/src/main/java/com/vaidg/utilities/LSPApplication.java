package com.vaidg.utilities;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.mqtt.MQTTManager;
import com.pojo.LanguagesList;
import com.vaidg.Dagger2.AppComponent;
import com.vaidg.Dagger2.DaggerAppComponent;
import com.vaidg.R;
import com.vaidg.account.AccountGeneral;
import com.vaidg.networkmanager.DroidNet;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import io.fabric.sdk.android.Fabric;
import io.reactivex.disposables.CompositeDisposable;

import static com.vaidg.utilities.Constants.ENGLISH;
import static com.vaidg.utilities.Constants.ENGLISH_CODE;
import static com.vaidg.utilities.Constants.PORTUGUESE;
import static com.vaidg.utilities.Constants.PORTUGUESE_CODE;
import static com.vaidg.utilities.Constants.selLang;

/**
 * <h2>IsForeground</h2>
 *
 * @author ${3Embed}
 * @since on 4/11/2017.
 * <p>
 * Usage:
 * <p>
 * 1. Get the Foreground Singleton, passing a Context or Application object unless you
 * are sure that the Singleton has definitely already been initialised elsewhere.
 * <p>
 * 2.a) Perform a direct, synchronous check: Foreground.isForeground() / .isBackground()
 * <p>
 * or
 * <p>
 * 2.b) Register to be notified (useful in Service or other non-UI components):
 * <p>
 * Foreground.Listener myListener = new Foreground.Listener(){
 * public void onBecameForeground(){
 * // ... whatever you want to do
 * }
 * public void onBecameBackground(){
 * // ... whatever you want to do
 * }
 * }
 * <p>
 * public void on_Create(){
 * super.on_Create();
 * Foreground.get(this).addListener(listener);
 * }
 * <p>
 * public void onDestroy(){
 * super.on_Create();
 * Foreground.get(this).removeListener(listener);
 * }
 */
public class LSPApplication extends DaggerApplication implements
        Application.ActivityLifecycleCallbacks {
    public static final String TAG = LSPApplication.class.getName();
    private static final long CHECK_DELAY = 500;
    private static LSPApplication instance;
    private static Context mContext;
    private static AccountManager mAccountManager;

    @Inject
    Gson gson;
    @Inject
    MQTTManager mqttManager;
    @Inject
    SessionManagerImpl manager;
    @Inject
    CompositeDisposable compositeDisposable;
    private boolean foreground = false, paused = true;
    private final List<Listener> listeners = new CopyOnWriteArrayList<Listener>();
    private Runnable check;
    private final Handler handler = new Handler();

    /**
     * Its not strictly necessary to use this method - _usually_ invoking
     * get with a Context gives us a path to retrieve the Application and
     * initialise, but sometimes (e.g. in test harness) the ApplicationContext
     * is != the Application, and the docs make no guarantees.
     *
     * @param application Application
     * @return an initialised Foreground instance
     */
    public static LSPApplication init(Application application) {
        if (instance == null) {
            instance = new LSPApplication();
            application.registerActivityLifecycleCallbacks(
                    instance);   //Registering life cycle for application class.
        }
        return instance;
    }


    /**
     * <h2>get</h2>
     * <p>
     * to create the single instance of this class
     * </p>
     *
     * @param application: application reference
     * @return : return the singleton instance of this class
     */
    public static LSPApplication get(Application application) {
        if (instance == null) {
            init(application);
        }
        return instance;
    }

    /**
     * <h2>get</h2>
     * <p>
     * to create the single instance of this class
     * </p>
     *
     * @param context: calling activity reference
     * @return : return the singleton instance of this class
     */
    public static LSPApplication get(Context context) {
        if (instance == null) {
            Context appContext = context.getApplicationContext();
            if (appContext instanceof Application) {
                init((Application) appContext);
            }
            throw new IllegalStateException(
                    "Foreground is not initialised and " +
                            "cannot obtain the Application object");
        }
        return instance;
    }

    /**
     * <h2>get</h2>
     * <p>
     * to create the single instance of this class
     * </p>
     *
     * @return: return the singleton instance of this class
     */
    public static LSPApplication get() {
        if (instance == null) {
            throw new IllegalStateException(
                    "Foreground is not initialised - invoke " +
                            "at least once with parameterised init/get");
        }
        return instance;
    }

    public static synchronized LSPApplication getInstance() {
        return instance;
    }

    public static synchronized Context getContext() {
        return mContext;
    }

    /**
     * <h2>addAccount</h2>
     * This method is used to set the auth token by creating the account manager with the account
     *
     * @param emailID email ID to be added
     */
    public void setAuthToken(String emailID, String password, String authToken) {
        Account account = new Account(emailID, AccountGeneral.ACCOUNT_TYPE);
        mAccountManager.addAccountExplicitly(account, password, null);
        mAccountManager.setAuthToken(account, AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS, authToken);
    }

    /**
     * <h2>getAuthToken</h2>
     * This method is used to get the auth token from the created account
     *
     * @return auth token stored
     */
    public String getAuthToken(String emailID) {
        Account[] account = mAccountManager.getAccountsByType(AccountGeneral.ACCOUNT_TYPE);
        List<Account> accounts = Arrays.asList(account);
        Utility.printLog(TAG, "auth token from size " + accounts.size() + " " + emailID);
        if (accounts.size() > 0) {
            for (int i = 0; i < accounts.size(); i++) {
                Utility.printLog(TAG, "auth token from size " + accounts.get(i).name);
                if (accounts.get(i).name.equals(emailID))
                    return mAccountManager.peekAuthToken(accounts.get(i), AccountGeneral.AUTHTOKEN_TYPE_FULL_ACCESS);
                else
                    removeAccount(accounts.get(i).name);
            }
        }
        return null;
    }

    /**
     * <h2>removeAccount</h2>
     * This method is used to remove the account stored
     *
     * @param emailID email ID of the account
     */
    public void removeAccount(String emailID) {
        Account[] account = mAccountManager.getAccountsByType(AccountGeneral.ACCOUNT_TYPE);
        List<Account> accounts = Arrays.asList(account);
        Utility.printLog(TAG, "auth token from size " + accounts.size() + " " + emailID);
        if (accounts.size() > 0) {
            for (int i = 0; i < accounts.size(); i++) {
                if (accounts.get(i).name.equals(emailID)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                        Utility.printLog(TAG, "account removed " + mAccountManager.removeAccountExplicitly(accounts.get(i)));
                    }
                }
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        instance = this;
        mContext = this;
        mAccountManager = AccountManager.get(getApplicationContext());
        DroidNet.init(this);
        ListenListener();
        setLanguageValueTosession();
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        DroidNet.getInstance().removeAllInternetConnectivityChangeListeners();
    }

    /**
     * <h2>LSPApplication</h2>
     * <p>
     * this method will return the state of the app
     * whether the app is in  foreground or not
     * </p>
     *
     * @return boolean:  whether the app is in  foreground or not
     */
    public boolean LSPApplication() {
        return foreground;
    }

    /**
     * <h2>isBackground</h2>
     * <p>
     * this method will return the state of the app
     * whether the app is in background or not
     * </p>
     *
     * @return boolean:  whether the app is in  background or not
     */
    public boolean isBackground() {
        return !foreground;
    }

    /**
     * <h2>addListener</h2>
     * <p>
     * method to add listener to get callback
     * </p>
     */
    public void addListener(Listener listener) {
        listeners.add(listener);
    }


    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
    }

    @Override
    public void onActivityResumed(Activity activity) {
        paused = false;
        boolean wasBackground = !foreground;
        foreground = true;
        if (check != null) {
            handler.removeCallbacks(check);
        }
        if (wasBackground) {
            Log.i(TAG, "went foreground");
            for (Listener l : listeners) {
                try {
                    l.onBecameForeground();
                } catch (Exception e) {
                    Log.e(TAG, "Listener threw exception!", e);
                }
            }
        } else {
            Log.i(TAG, "still foreground");
        }
    }

    @Override
    public void onActivityPaused(Activity activity) {
        paused = true;
        if (check != null) {
            handler.removeCallbacks(check);
        }
        handler.postDelayed(check = new Runnable() {
            @Override
            public void run() {
                if (foreground && paused) {
                    foreground = false;
                    Log.i(TAG, "went background");
                    for (Listener l : listeners) {
                        try {
                            l.onBecameBackground();
                        } catch (Exception e) {
                            Log.e(TAG, "Listener threw exception!", e);
                        }
                    }
                } else {
                    Log.i(TAG, "still foreground");
                }
            }
        }, CHECK_DELAY);
    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        if (activity.isDestroyed()) {
            Log.w(TAG, "onActivityDestroyed: ");
        }
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder().application(this).build();
        appComponent.inject(this);
        return appComponent;
    }

    /*
    Bug Name: The patient app keeps on crashing in app launch state
    Bug id: q1OINGb3
    Fix: handled NullPointerException when app is launched for the first time after installing
    Dev: Sateesh
    Date:02 june 2021
    */
    private void setLanguageValueTosession() {
        if (manager.getLanguageSettings() != null && TextUtils.isEmpty(manager.getLanguageSettings().getCode())) {
            selLang = ENGLISH_CODE;
            manager.setLanguageSettings(new LanguagesList(ENGLISH_CODE, ENGLISH, 0));
        } else {
            if (manager.getLanguageSettings() != null && manager.getLanguageSettings().getCode().equals(PORTUGUESE_CODE)) {
                selLang = PORTUGUESE_CODE;
                manager.setLanguageSettings(new LanguagesList(PORTUGUESE_CODE, PORTUGUESE, 0));
            } else {
                selLang = ENGLISH_CODE;
                manager.setLanguageSettings(new LanguagesList(ENGLISH_CODE, ENGLISH, 0));
            }
        }
        Utility.changeLanguageConfig(manager.getLanguageSettings().getCode(), this);
        if (getAuthToken(manager.getSID()) != null && !getAuthToken(manager.getSID()).isEmpty()) {
            if (Utility.isNetworkAvailable(mContext)) {

            }
        }
    }

    public void ListenListener() {
        addListener(new Listener() {
            @Override
            public void onBecameForeground() {
                if (!mqttManager.isMQTTConnected()) {
                    if (!getAuthToken(manager.getSID()).equals("") || getAuthToken(manager.getSID()) != null
                            && !manager.getSID().equals("") || manager.getSID() != null) {
                        mqttManager.createMQttConnection(manager.getSID(), false);
                    }
                }
            }

            @Override
            public void onBecameBackground() {
                compositeDisposable.clear();
            }
        });
    }

    public interface Listener {
        void onBecameForeground();

        void onBecameBackground();
    }
}
