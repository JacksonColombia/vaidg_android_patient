package com.vaidg.changepassword;

import static com.vaidg.utilities.Constants.BASE_AUTH_PASSWORD;
import static com.vaidg.utilities.Constants.BASE_AUTH_USERNAME;
import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.SESSION_EXPIRED;
import static com.vaidg.utilities.Constants.SESSION_LOGOUT;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;
import static com.vaidg.utilities.Constants.USERTYPE;
import static com.vaidg.utilities.LSPApplication.getInstance;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.utility.RefreshToken;
import com.vaidg.R;
import com.vaidg.model.ServerResponse;
import com.vaidg.networking.BasicAuthentication;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * <h2>ChangePwdPresenterImpl</h2>
 * Created by Pramod on 19/12/17.
 */

public class ChangePwdPresenterImpl implements ChangePwdPresenter {

   @Inject
   ChangePwdView view;
   @Inject
   Context context;
   @Inject
    SessionManagerImpl sessionManager;
    @Inject
    LSPServices lspServices;

    @Inject
    CompositeDisposable compositeDisposable;

    CompositeDisposable verDisposable;
    //private ChangePwdModel changePwdModel;


    @Inject
    ChangePwdPresenterImpl() {
        this.verDisposable = new CompositeDisposable();
        //this.changePwdModel = new ChangePwdModel();
    }


    @Override
    public void profChangePassword(String auth, String old_password, String new_password) {
    onShowProgress();
      Map<String,Object> stringObjectMap =new HashMap<>();
      stringObjectMap.put("oldPassword",old_password);
      stringObjectMap.put("newPassword",new_password);

        Observable<Response<ServerResponse>> response = lspServices.profChangePwd(getInstance().getAuthToken(sessionManager.getSID()),Constants.selLang,PLATFORM_ANDROID,stringObjectMap);
        response.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ServerResponse>>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        verDisposable.add(d);
                    }

                    @Override
                    public void onNext(Response<ServerResponse> responseBodyResponse) {
                        int code = responseBodyResponse.code();
                        try {
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (code) {
                                case SUCCESS_RESPONSE:
                                    if (responseBodyResponse.body() != null) {
                                        onHideProgress();
                                        onErrorMsg(responseBodyResponse.body().getMessage());
                                        if (view != null)
                                            view.navToProfile();
                                    }
                                    break;
                                case SESSION_EXPIRED:
                                    //  ErrorHandel errorHandel = gson.fromJson(errorBody, ErrorHandel.class);
                                    RefreshToken.onRefreshToken(getInstance().getAuthToken(sessionManager.getSID()), sessionManager.getREFRESHAUTH(),
                                            lspServices,
                                            new RefreshToken.RefreshTokenImple() {
                                                @Override
                                                public void onSuccessRefreshToken(String newToken) {
                                                    onHideProgress();
                                                    getInstance().setAuthToken(sessionManager.getSID(), sessionManager.getSID(), newToken);
                                                    profChangePassword(auth,old_password,new_password);
                                                }

                                                @Override
                                                public void onFailureRefreshToken() {
                                                    onHideProgress();
                                                }

                                                @Override
                                                public void sessionExpired(String msg) {
                                                    onLogout(msg);
                                                }
                                            });
                                    break;
                                case SESSION_LOGOUT:
                                    onLogout(errorBody);
                                    break;
                                default:
                                    onErrorMsg(errorBody);
                                    break;

                            }
                        } catch (Exception e) {
                            onErrorMsg(e.getMessage());
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(Throwable e)
                    {
                        onErrorMsg(e.getMessage());
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                            onHideProgress();
                    }
                });
    }

    @Override
    public void changePassword(String sid,String new_password) {
        onShowProgress();
        //LSPServices service = ServiceFactory.createRetrofitService(LSPServices.class);
      lspServices = BasicAuthentication.ServiceGenerator.createService(LSPServices.class, BASE_AUTH_USERNAME, BASE_AUTH_PASSWORD);

      Map<String, Object> jsonParams = new HashMap<>();
       /* Map<String, Object> jsonParams = new ArrayMap<>();
//put something inside the map, could be null*/
      jsonParams.put("password", new_password);
      jsonParams.put("userId", sid);
      jsonParams.put(USERTYPE, 1);
      Observable<Response<ServerResponse>> response = lspServices.changePassword(Constants.selLang,
          PLATFORM_ANDROID,jsonParams);

        response.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ServerResponse>>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Response<ServerResponse> responseBodyResponse)
                    {
                        int code = responseBodyResponse.code();
                        try {
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (code) {
                                case SUCCESS_RESPONSE:
                                    if (responseBodyResponse.body() != null) {
                                        onHideProgress();
                                        onErrorMsg(responseBodyResponse.body().getMessage());
                                        if (view != null)
                                            view.navtoLogin();
                                    }
                                    break;
                                default:
                                    onErrorMsg(errorBody);
                                    break;

                            }
                        } catch (Exception e) {
                            onErrorMsg(e.getMessage());
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e)
                    {
                        onErrorMsg(e.getMessage());
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                            onHideProgress();
                    }
                });


    }

  @Override
  public void attachView(Object view) {
  }

  @Override
  public void detachView() {
  }

    @Override
    public void onHideProgress() {
        if (view != null) view.onHideProgress();
    }

    @Override
    public void onShowProgress() {
        if (view != null) view.onShowProgress();
    }

    @Override
    public void onLogout(String errorBody) {
        onHideProgress();
        try {
            if (!TextUtils.isEmpty(errorBody)) {
                JSONObject jsonObject = new JSONObject(errorBody);
                if (!jsonObject.getString(MESSAGE).isEmpty()) {
                    if (view != null) view.onLogout(jsonObject.getString(MESSAGE), sessionManager);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorMsg(String errorBody) {
        onHideProgress();
        try {
            if (!TextUtils.isEmpty(errorBody)) {
                JSONObject jsonObject = new JSONObject(errorBody);
                if (!jsonObject.getString(MESSAGE).isEmpty()) {
                    if (view != null) view.onError(jsonObject.getString(MESSAGE));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
