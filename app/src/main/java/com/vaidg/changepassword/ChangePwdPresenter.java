package com.vaidg.changepassword;

import com.vaidg.home.BasePresenter;

/**
 * <h2>ChangePwdPresenter</h2>
 * Created by Pramod on 19/12/17.
 */

public interface ChangePwdPresenter extends BasePresenter {

    void changePassword(String sid, String new_password);

    void profChangePassword(String auth, String old_password, String new_password);

    /**
     * <h2>onHideProgress</h2>
     * <p>This method is used to hide the loading view</p>
     */
    void onHideProgress();

    /**
     * <h2>onShowProgress</h2>
     * <p>This method is used to show the loading view</p>
     */
    void onShowProgress();

    /**
     * <h2>onLogout</h2>
     * <p>This method is used to show msg if session is expired</p>
     */
    void onLogout(String errorBody);

    /**
     * <h2>onErrorMsg</h2>
     * <p>This method is used to show error msg from api calls</p>
     */
    void onErrorMsg(String errorBody);
}
