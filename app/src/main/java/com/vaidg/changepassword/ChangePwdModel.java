package com.vaidg.changepassword;

import com.vaidg.utilities.Constants;

/**
 * Created by ${3Embed} on 4/11/17.
 */

public class ChangePwdModel {

    public boolean emptyPwd(String pwd) {
        return pwd == null || pwd.length() == 0 || "".equals(pwd.trim());
    }

    public boolean validatePwd(String pwd) {
        return !Constants.PASSWORD_PATTERN.matcher(pwd).matches();
    }

    public boolean validatePassword(final String password) {
        return password == null || "".equals(password) || !Constants.PASSWORD_PATTERN.matcher(password).matches();
    }

}
