package com.vaidg.changepassword;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.vaidg.Login.LoginActivity;
import com.vaidg.R;
import com.vaidg.profile.ProfileActivity;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.SessionManager;
import com.utility.AlertProgress;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;

import static com.vaidg.utilities.Utility.setMAnagerWithBID;

public class ChangePwdActivity extends DaggerAppCompatActivity implements ChangePwdView {

    public final String TAG = "ChangePwdActivity";

    @BindView(R.id.btn_continue)
    Button btn_continue;

    @BindView(R.id.change_pwd_text)
    TextView change_pwd_text;

    @BindView(R.id.et_new_pwd)
    EditText et_new_pwd;

    @BindView(R.id.et_old_pwd)
    EditText et_old_pwd;

    @BindView(R.id.til_old_pwd)
    TextInputLayout til_old_pwd;

    @BindView(R.id.til_new_pwd)
    TextInputLayout til_new_pwd;

    @BindView(R.id.et_reenter_pwd)
    EditText et_reenter_pwd;

    @BindView(R.id.til_reenter_pwd)
    TextInputLayout til_reenter_pwd;

    AlertDialog alertDialog;
    AlertDialog.Builder dialogBuilder;

    @Inject
    AlertProgress alertProgress;

    private String coming_from;
    private String auth;

    @Inject
    ChangePwdPresenter presenter;

    @Inject
    SessionManager sessionManager;

    @Inject
    AppTypeface appTypeface;

    private Context mContext;
    private ProgressDialog pDialog;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");

                //TextView tv = (TextView) findViewById(R.id.txtview);
                //Log.e("OTP"," message is ::  "+message);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pwd);
        ButterKnife.bind(this);
        mContext = this;
        Bundle bundle = getIntent().getExtras();
        if(bundle == null) {
            coming_from="";
            btn_continue.setText(getResources().getString(R.string.continue_txt));
        } else {
            auth = bundle.getString("auth");
            coming_from = bundle.getString("coming_from");
            btn_continue.setText(getResources().getString(R.string.changePassword));
        }
        initialize();
    }

    private void initialize() {
        Toolbar toolbar= findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView textCenter = toolbar.findViewById(R.id.tv_center);
        if ("profile".equals(coming_from))
            textCenter.setText(R.string.changePassword);
        else
            textCenter.setText(R.string.your_new_password);

        textCenter.setTypeface(appTypeface.getHind_semiBold());
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(view -> {
            if ("profile".equals(coming_from)) {
                Intent intent = new Intent(ChangePwdActivity.this, ProfileActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                finish();
            } else
                onBackPressed();
        });

        if ("profile".equals(coming_from)) {
            change_pwd_text.setVisibility(View.GONE);
            til_new_pwd.setVisibility(View.VISIBLE);
        } else {
            change_pwd_text.setVisibility(View.VISIBLE);
            til_old_pwd.setVisibility(View.GONE);
            //til_new_pwd.setVisibility(View.GONE);

            et_new_pwd.requestFocus();

            et_reenter_pwd.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String passwrd = et_new_pwd.getText().toString();
                    String confirm_pwd = et_reenter_pwd.getText().toString();
                    if (editable.length() > 0 && passwrd.length() > 0) {
                        if(!confirm_pwd.equals(passwrd )){
                            // give an error that password and confirm password not match
                          //  Log.e(TAG,"Error confirm_pwd"+confirm_pwd+ "   pwd ::  "+passwrd);
                        } else {
                           // Log.e(TAG,"confirm_pwd"+confirm_pwd+ "   pwd ::  "+passwrd);
                        }

                    }
                }
            });
        }
    }

    @OnClick(R.id.btn_continue)
    void setBtn_continue() {
        if ("profile".equals(coming_from)) {
           // Log.e(TAG, "setBtn_continue: AUTH"+auth);
            if(et_new_pwd.getText().toString().trim().equals(et_reenter_pwd.getText().toString().trim())) {
                if(Utility.isNetworkAvailable(this))
                    presenter.profChangePassword(auth,et_old_pwd.getText().toString(),et_new_pwd.getText().toString());
            } else
                alertProgress.alertinfo(this,getString(R.string.RePasswordShouldBeSame));
        } else {
            String sid = sessionManager.getSID();

           // Log.e(TAG, "setBtn_continue: SID =>  " + sid);

            if(et_new_pwd.getText().toString().trim().equals(et_reenter_pwd.getText().toString().trim())) {
                if (Utility.isNetworkAvailable(this))
                    presenter.changePassword(sid, et_new_pwd.getText().toString());
            } else
                alertProgress.alertinfo(this,getString(R.string.PasswordShouldBeSame));
        }

    }

    @Override
    public void onShowProgress() {
        pDialog = alertProgress.getProcessDialog(this);
        if ("profile".equals(coming_from))
            pDialog.setMessage(mContext.getResources().getString(R.string.changePassword));
        else
            pDialog.setMessage(mContext.getResources().getString(R.string.waitSignin));
        pDialog.setCancelable(false);
        try {
            if (pDialog != null && !pDialog.isShowing() && !isFinishing()) {
                pDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onHideProgress() {
        if (pDialog != null && !isFinishing() && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    @Override
    public void setPasswordInvalid() {
        til_new_pwd.setError(getString(R.string.invalidPassword));
        et_new_pwd.setText("");
    }

    @Override
    public void setPasswordError() {
        til_new_pwd.setError(getString(R.string.errPassword));
    }

    @Override
    public void setConfirmPwdInvalid() {
        til_reenter_pwd.setError(getString(R.string.invalidPassword));
        et_reenter_pwd.setText("");
    }

    @Override
    public void setConfirmPwdError() {
        til_reenter_pwd.setError(getString(R.string.errPassword));
    }

    @Override
    public void navtoLogin() {
        Intent intent = new Intent(ChangePwdActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void navToProfile() {
        Intent intent = new Intent(ChangePwdActivity.this, ProfileActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).
                registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    @Override
    public void onSessionExpired() {
    }

    @Override
    public void onLogout(String msg, SessionManagerImpl sessionManager) {
        if (alertProgress != null) {
            alertProgress.alertPositiveOnclick(this, msg, mContext.getResources().getString(R.string.logout), mContext.getResources().getString(R.string.ok),
                    isClicked -> setMAnagerWithBID(mContext, sessionManager));
        }
    }

    @Override
    public void onError(String msg) {
        Toast.makeText(this,""+msg,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionError(String connectionError) {
    }
}
