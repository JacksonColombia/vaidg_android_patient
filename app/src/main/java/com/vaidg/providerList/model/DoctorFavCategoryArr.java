package com.vaidg.providerList.model;

import com.vaidg.model.CallType;
import com.vaidg.model.Offers;
import com.vaidg.model.QuestionList;
import com.vaidg.model.SubCategory;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

public class DoctorFavCategoryArr implements Serializable {

  @SerializedName("_id")
  @Expose
  private String id;
  @SerializedName("city_id")
  @Expose
  private String cityId;
  @SerializedName("jobType")
  @Expose
  private int jobType;
  @SerializedName("prepTime")
  @Expose
  private String prepTime;
  @SerializedName("packupTime")
  @Expose
  private String packupTime;
  @SerializedName("bussinessGroup")
  @Expose
  private String bussinessGroup;
  @SerializedName("city")
  @Expose
  private String city;
  @SerializedName("service_type")
  @Expose
  private int serviceType;
  @SerializedName("billing_model")
  @Expose
  private int billingModel;
  @SerializedName("cat_name")
  @Expose
  private String catName;
  @SerializedName("cat_desc")
  @Expose
  private String catDesc;
  @SerializedName("can_fees")
  @Expose
  private Integer canFees;
  @SerializedName("bannerImageWeb")
  @Expose
  private String bannerImageWeb;
  @SerializedName("bannerImageApp")
  @Expose
  private String bannerImageApp;
  @SerializedName("sel_img")
  @Expose
  private String selImg;
  @SerializedName("unsel_img")
  @Expose
  private String unselImg;
  /* @SerializedName("metadataIds")
   @Expose
   private  ArrayList<MetaData> metadataIds = null;*/
   /* @SerializedName("ratingParameterArr")
    @Expose
    private  ArrayList<RatingParameter> ratingParameterArr = null;*/
  @SerializedName("symptomGroup")
  @Expose
  private String symptomGroup;
  /* @SerializedName("symptomCategory")
   @Expose
   private String symptomCategory;*/
   /* @SerializedName("symptomArray")
    @Expose
    private  ArrayList<Symptom> symptomArray = null;*/
  @SerializedName("deactive")
  @Expose
  private boolean deactive;
  @SerializedName("mileage")
  @Expose
  private int mileage;
  @SerializedName("consultancyFee")
  @Expose
  private int consultancyFee;
  @SerializedName("minimumFeesForConsultancy")
  @Expose
  private double minimumFeesForConsultancy;
  @SerializedName("maximumFeesForConsultancy")
  @Expose
  private double maximumFeesForConsultancy;
  @SerializedName("status")
  @Expose
  private int status;
  @SerializedName("categoryName")
  @Expose
  private CategoryName categoryName;
  @SerializedName("categoryDescription")
  @Expose
  private CategoryDescription categoryDescription;
  @SerializedName("bidCreditArr")
  @Expose
  private ArrayList<BidCred> bidCreditArr = null;
  @SerializedName("visit_fees")
  @Expose
  private double visitFee;
  @SerializedName("bookingTypeAction")
  @Expose
  private BookingTypeAction bookingTypeAction;
  @SerializedName("callType")
  @Expose
  private CallType callType;
  @SerializedName("bidCredit")
  @Expose
  private String bidCredit;
  @SerializedName("offers")
  @Expose
  private ArrayList<Offers> offers = null;
  @SerializedName("subCategory")
  @Expose
  private ArrayList<SubCategory> subCategory = null;
  @SerializedName("questionArr")
  @Expose
  private ArrayList<QuestionList> questionArr = null;
  @SerializedName("bookingType")
  @Expose
  private String bookingType;
  @SerializedName("bussinessgp")
  @Expose
  private String bussinessgp;
  @SerializedName("minimum_fees")
  @Expose
  private double minimumFees;
  @SerializedName("miximum_fees")
  @Expose
  private double maximumFees;
  @SerializedName("price_per_fees")
  @Expose
  private double pricePerHour;
  @SerializedName("recomdedBannerImageApp")
  @Expose
  private String recommendedBannerImageApp;
  @SerializedName("iconApp")
  @Expose
  private String iconApp;
  @SerializedName("categoryId")
  @Expose
  private String categoryId;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCityId() {
    return cityId;
  }

  public void setCityId(String cityId) {
    this.cityId = cityId;
  }

  public int getJobType() {
    return jobType;
  }

  public void setJobType(int jobType) {
    this.jobType = jobType;
  }

  public String getPrepTime() {
    return prepTime;
  }

  public void setPrepTime(String prepTime) {
    this.prepTime = prepTime;
  }

  public String getPackupTime() {
    return packupTime;
  }

  public void setPackupTime(String packupTime) {
    this.packupTime = packupTime;
  }

  public String getBussinessGroup() {
    return bussinessGroup;
  }

  public void setBussinessGroup(String bussinessGroup) {
    this.bussinessGroup = bussinessGroup;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public int getServiceType() {
    return serviceType;
  }

  public void setServiceType(int serviceType) {
    this.serviceType = serviceType;
  }

/*
    public ArrayList<MetaData> getMetadataIds() {
        return metadataIds;
    }
*/

/*
    public ArrayList<RatingParameter> getRatingParameterArr() {
        return ratingParameterArr;
    }
*/

  public int getBillingModel() {
    return billingModel;
  }

/*
    public String getSymptomCategory() {
        return symptomCategory;
    }
*/

/*
    public ArrayList<Symptom> getSymptomArray() {
        return symptomArray;
    }
*/

  public void setBillingModel(int billingModel) {
    this.billingModel = billingModel;
  }

  public String getCatName() {
    return catName;
  }

  public void setCatName(String catName) {
    this.catName = catName;
  }

  public String getCatDesc() {
    return catDesc;
  }

  public void setCatDesc(String catDesc) {
    this.catDesc = catDesc;
  }

  public Integer getCanFees() {
    return canFees;
  }

  public void setCanFees(Integer canFees) {
    this.canFees = canFees;
  }

  public String getBannerImageWeb() {
    return bannerImageWeb;
  }

  public void setBannerImageWeb(String bannerImageWeb) {
    this.bannerImageWeb = bannerImageWeb;
  }

  public String getBannerImageApp() {
    return bannerImageApp;
  }

  public void setBannerImageApp(String bannerImageApp) {
    this.bannerImageApp = bannerImageApp;
  }

  public String getSelImg() {
    return selImg;
  }

  public void setSelImg(String selImg) {
    this.selImg = selImg;
  }

  public String getUnselImg() {
    return unselImg;
  }

  public void setUnselImg(String unselImg) {
    this.unselImg = unselImg;
  }

  public String getSymptomGroup() {
    return symptomGroup;
  }

  public void setSymptomGroup(String symptomGroup) {
    this.symptomGroup = symptomGroup;
  }

  public boolean isDeactive() {
    return deactive;
  }

  public void setDeactive(boolean deactive) {
    this.deactive = deactive;
  }

  public int getMileage() {
    return mileage;
  }

  public void setMileage(int mileage) {
    this.mileage = mileage;
  }

  public int getConsultancyFee() {
    return consultancyFee;
  }

  public void setConsultancyFee(int consultancyFee) {
    this.consultancyFee = consultancyFee;
  }

  public double getMinimumFeesForConsultancy() {
    return minimumFeesForConsultancy;
  }

  public void setMinimumFeesForConsultancy(int minimumFeesForConsultancy) {
    this.minimumFeesForConsultancy = minimumFeesForConsultancy;
  }

  public double getMaximumFeesForConsultancy() {
    return maximumFeesForConsultancy;
  }

  public void setMaximumFeesForConsultancy(int maximumFeesForConsultancy) {
    this.maximumFeesForConsultancy = maximumFeesForConsultancy;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public CategoryName getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(CategoryName categoryName) {
    this.categoryName = categoryName;
  }

  public CategoryDescription getCategoryDescription() {
    return categoryDescription;
  }

  public void setCategoryDescription(CategoryDescription categoryDescription) {
    this.categoryDescription = categoryDescription;
  }

  public ArrayList<BidCred> getBidCreditArr() {
    return bidCreditArr;
  }

  public void setBidCreditArr(ArrayList<BidCred> bidCreditArr) {
    this.bidCreditArr = bidCreditArr;
  }

  public double getVisitFee() {
    return visitFee;
  }

  public void setVisitFee(double visitFee) {
    this.visitFee = visitFee;
  }

  public BookingTypeAction getBookingTypeAction() {
    return bookingTypeAction;
  }

  public void setBookingTypeAction(BookingTypeAction bookingTypeAction) {
    this.bookingTypeAction = bookingTypeAction;
  }

  public CallType getCallType() {
    return callType;
  }

  public void setCallType(CallType callType) {
    this.callType = callType;
  }

  public String getBidCredit() {
    return bidCredit;
  }

  public void setBidCredit(String bidCredit) {
    this.bidCredit = bidCredit;
  }

  public ArrayList<Offers> getOffers() {
    return offers;
  }

  public void setOffers(ArrayList<Offers> offers) {
    this.offers = offers;
  }

  public ArrayList<SubCategory> getSubCategory() {
    return subCategory;
  }

  public void setSubCategory(ArrayList<SubCategory> subCategory) {
    this.subCategory = subCategory;
  }

/*
    public void setMetadataIds(ArrayList<MetaData> metadataIds) {
        this.metadataIds = metadataIds;
    }
*/

/*
    public void setRatingParameterArr(ArrayList<RatingParameter> ratingParameterArr) {
        this.ratingParameterArr = ratingParameterArr;
    }
*/

  public ArrayList<QuestionList> getQuestionArr() {
    return questionArr;
  }

   /* public void setSymptomCategory(String symptomCategory) {
        this.symptomCategory = symptomCategory;
    }
*/
/*
    public void setSymptomArray(ArrayList<Symptom> symptomArray) {
        this.symptomArray = symptomArray;
    }
*/

  public void setQuestionArr(ArrayList<QuestionList> questionArr) {
    this.questionArr = questionArr;
  }

  public String getBookingType() {
    return bookingType;
  }

  public void setBookingType(String bookingType) {
    this.bookingType = bookingType;
  }

  public String getBussinessgp() {
    return bussinessgp;
  }

  public void setBussinessgp(String bussinessgp) {
    this.bussinessgp = bussinessgp;
  }

  public double getMinimumFees() {
    return minimumFees;
  }

  public void setMinimumFees(double minimumFees) {
    this.minimumFees = minimumFees;
  }

  public double getMaximumFees() {
    return maximumFees;
  }

  public void setMaximumFees(double maximumFees) {
    this.maximumFees = maximumFees;
  }

  public double getPricePerHour() {
    return pricePerHour;
  }

  public void setPricePerHour(double pricePerHour) {
    this.pricePerHour = pricePerHour;
  }

  public String getRecommendedBannerImageApp() {
    return recommendedBannerImageApp;
  }

  public void setRecommendedBannerImageApp(String recommendedBannerImageApp) {
    this.recommendedBannerImageApp = recommendedBannerImageApp;
  }

  public String getIconApp() {
    return iconApp;
  }

  public void setIconApp(String iconApp) {
    this.iconApp = iconApp;
  }

  public String getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public class BookingTypeAction implements Serializable {
        /*"now":true,
"schedule":false,
"repeate":false*/

    @SerializedName("now")
    @Expose
    private boolean now;

    @SerializedName("schedule")
    @Expose
    private boolean schedule;

    @SerializedName("repeat")
    @Expose
    private boolean repeat;

    public boolean isNow() {
      return now;
    }

    public boolean isSchedule() {
      return schedule;
    }

    public boolean isRepeat() {
      return repeat;
    }
  }

  public class CategoryName implements Serializable {
    @SerializedName("ar")
    @Expose
    private String ar;
    @SerializedName("en")
    @Expose
    private String en;

    public String getAr() {
      return ar;
    }

    public String getEn() {
      return en;
    }

  }

  public class CategoryDescription implements Serializable {
    @SerializedName("ar")
    @Expose
    private String ar;
    @SerializedName("en")
    @Expose
    private String en;

    public String getAr() {
      return ar;
    }

    public String getEn() {
      return en;
    }

  }

  public class BidCred implements Serializable {

  }

  public class MetaData implements Serializable {

  }

  public class RatingParameter implements Serializable {

  }

  public class Symptom implements Serializable {

  }
}
