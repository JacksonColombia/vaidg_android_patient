package com.vaidg.providerList.model;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoctorAddress implements Serializable
{

  @SerializedName("_id")
  @Expose
  private String id;
  @SerializedName("taggedAs")
  @Expose
  private String taggedAs;
  @SerializedName("user_Id")
  @Expose
  private String userId;
  @SerializedName("addLine1")
  @Expose
  private String addLine1;
  @SerializedName("addLine2")
  @Expose
  private String addLine2;
  @SerializedName("city")
  @Expose
  private String city;
  @SerializedName("state")
  @Expose
  private String state;
  @SerializedName("pincode")
  @Expose
  private String pincode;
  @SerializedName("userType")
  @Expose
  private int userType;
  @SerializedName("defaultAddress")
  @Expose
  private int defaultAddress;
  @SerializedName("latitude")
  @Expose
  private float latitude;
  @SerializedName("longitude")
  @Expose
  private float longitude;
  @SerializedName("country")
  @Expose
  private String country;
  @SerializedName("placeId")
  @Expose
  private String placeId;


  /**
   * No args constructor for use in serialization
   *
   */
  public DoctorAddress() {
  }

  /**
   *
   * @param pincode
   * @param country
   * @param city
   * @param latitude
   * @param placeId
   * @param userId
   * @param addLine1
   * @param id
   * @param addLine2
   * @param state
   * @param userType
   * @param taggedAs
   * @param defaultAddress
   * @param longitude
   */
  public DoctorAddress(String id, String taggedAs, String userId, String addLine1, String addLine2, String city, String state, String pincode, int userType, int defaultAddress, float latitude, float longitude, String country, String placeId) {
    super();
    this.id = id;
    this.taggedAs = taggedAs;
    this.userId = userId;
    this.addLine1 = addLine1;
    this.addLine2 = addLine2;
    this.city = city;
    this.state = state;
    this.pincode = pincode;
    this.userType = userType;
    this.defaultAddress = defaultAddress;
    this.latitude = latitude;
    this.longitude = longitude;
    this.country = country;
    this.placeId = placeId;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTaggedAs() {
    return taggedAs;
  }

  public void setTaggedAs(String taggedAs) {
    this.taggedAs = taggedAs;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getAddLine1() {
    return addLine1;
  }

  public void setAddLine1(String addLine1) {
    this.addLine1 = addLine1;
  }

  public String getAddLine2() {
    return addLine2;
  }

  public void setAddLine2(String addLine2) {
    this.addLine2 = addLine2;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getPincode() {
    return pincode;
  }

  public void setPincode(String pincode) {
    this.pincode = pincode;
  }

  public int getUserType() {
    return userType;
  }

  public void setUserType(int userType) {
    this.userType = userType;
  }

  public int getDefaultAddress() {
    return defaultAddress;
  }

  public void setDefaultAddress(int defaultAddress) {
    this.defaultAddress = defaultAddress;
  }

  public float getLatitude() {
    return latitude;
  }

  public void setLatitude(float latitude) {
    this.latitude = latitude;
  }

  public float getLongitude() {
    return longitude;
  }

  public void setLongitude(float longitude) {
    this.longitude = longitude;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getPlaceId() {
    return placeId;
  }

  public void setPlaceId(String placeId) {
    this.placeId = placeId;
  }

}