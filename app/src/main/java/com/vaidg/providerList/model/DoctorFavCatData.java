package com.vaidg.providerList.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DoctorFavCatData implements Serializable
{
  /*"providerId":"5b34b17361596a30f76aa946",
"categoryName":"Tutor",
"categoryId":"5aaa93be61596a5557059168",
"timestamp":1530520915*/
  @SerializedName("providerId")
  @Expose
  private String providerId;
  @SerializedName("categoryName")
  @Expose
  private String categoryName;
  @SerializedName("categoryId")
  @Expose
  private String categoryId;
  @SerializedName("timestamp")
  @Expose
  private long timestamp;

  public String getProviderId() {
    return providerId;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public String getCategoryId() {
    return categoryId;
  }

  public long getTimestamp() {
    return timestamp;
  }
}
