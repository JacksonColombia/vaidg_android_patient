package com.vaidg.providerList.model;

import com.vaidg.model.Category;
import com.vaidg.model.Location;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;


public class DoctorData implements Serializable {

  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("firstName")
  @Expose
  private String firstName;
  @SerializedName("title")
  @Expose
  private String title;
  @SerializedName("lastName")
  @Expose
  private String lastName;
  @SerializedName("image")
  @Expose
  private String image;
  @SerializedName("location")
  @Expose
  private Location location;
  @SerializedName("status")
  @Expose
  private int status;
  @SerializedName("distance")
  @Expose
  private double distance;
  @SerializedName("averageRating")
  @Expose
  private double averageRating;
  @SerializedName("amount")
  @Expose
  private double amount;
  @SerializedName("currencySymbol")
  @Expose
  private String currencySymbol;
  @SerializedName("currency")
  @Expose
  private String currency;
  @SerializedName("distanceMatrix")
  @Expose
  private int distanceMatrix;
  @SerializedName("totalBooking")
  @Expose
  private int totalBooking;
  @SerializedName("favouriteProvider")
  @Expose
  private boolean favouriteProvider;
  @SerializedName("yearOfExperience")
  @Expose
  private String yearOfExperience;
  @SerializedName("address")
  @Expose
  private DoctorAddress address;
  @SerializedName("categoryData")
  @Expose
  private ArrayList<DoctorFavCatData> categoryData;
  @SerializedName("categoryArr")
  @Expose
  private Category categoryArr;


  /**
   * No args constructor for use in serialization
   */
  public DoctorData() {
  }


  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public double getDistance() {
    return distance;
  }

  public void setDistance(float distance) {
    this.distance = distance;
  }

  public double getAverageRating() {
    return averageRating;
  }

  public void setAverageRating(float averageRating) {
    this.averageRating = averageRating;
  }

  public double getAmount() {
    return amount;
  }

  public void setAmount(double amount) {
    this.amount = amount;
  }

  public String getCurrencySymbol() {
    return currencySymbol;
  }

  public void setCurrencySymbol(String currencySymbol) {
    this.currencySymbol = currencySymbol;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public int getDistanceMatrix() {
    return distanceMatrix;
  }

  public void setDistanceMatrix(int distanceMatrix) {
    this.distanceMatrix = distanceMatrix;
  }

  public int getTotalBooking() {
    return totalBooking;
  }

  public void setTotalBooking(int totalBooking) {
    this.totalBooking = totalBooking;
  }

  public boolean isFavouriteProvider() {
    return favouriteProvider;
  }

  public void setFavouriteProvider(boolean favouriteProvider) {
    this.favouriteProvider = favouriteProvider;
  }

  public String getYearOfExperience() {
    return yearOfExperience;
  }

  public void setYearOfExperience(String yearOfExperience) {
    this.yearOfExperience = yearOfExperience;
  }

  public DoctorAddress getAddress() {
    return address;
  }

  public void setAddress(DoctorAddress address) {
    this.address = address;
  }

  public ArrayList<DoctorFavCatData> getCategoryData() {
    return categoryData;
  }

  public Category getCategoryArr() {
    return categoryArr;
  }
}