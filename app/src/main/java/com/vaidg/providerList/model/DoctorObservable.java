package com.vaidg.providerList.model;

import com.vaidg.providerList.model.DoctorData;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;

/**
 * <h2>DoctorObservable</h2>
 * Created by Ali on 1/30/2018.
 */

public class DoctorObservable extends Observable<ArrayList<DoctorData>>
{
   // private static DoctorObservable providerObservable;
    private Observer<?super ArrayList<DoctorData>>observer;
    @Inject
    public DoctorObservable() {
    }

   /* public static DoctorObservable getInstance()
    {
        if (providerObservable == null) {
            providerObservable = new DoctorObservable();

            return providerObservable;
        } else {
            return providerObservable;
        }
    }*/

    @Override
    protected void subscribeActual(Observer<? super ArrayList<DoctorData>> observer) {

        this.observer = observer;
    }
    public void emitData(ArrayList<DoctorData> data)
    {
        observer.onNext(data);
        observer.onComplete();
    }
}
