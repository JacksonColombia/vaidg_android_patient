package com.vaidg.providerList.model;

import static com.vaidg.utilities.Constants.DATA;
import static com.vaidg.utilities.Constants.MESSAGE;

import android.util.Log;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class DoctorResponse implements Serializable
{

  @SerializedName(MESSAGE)
  @Expose
  private String message;
  @SerializedName(DATA)
  @Expose
  private ArrayList<DoctorData> data = null;
  private int errNum,errFlag;
  private String errMsg;

  /**
   * No args constructor for use in serialization
   *
   */
  public DoctorResponse() {
  }

  /**
   *
   * @param data
   * @param message
   */
  public DoctorResponse(String message, ArrayList<DoctorData> data) {
    super();
    this.message = message;
    this.data = data;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public ArrayList<DoctorData> getData() {
    return data;
  }

  public void setData(ArrayList<DoctorData> data) {
    this.data = data;
  }

  public int getErrNum() {
    return errNum;
  }

  public void setErrNum(int errNum) {
    this.errNum = errNum;
  }

  public int getErrFlag() {
    return errFlag;
  }

  public void setErrFlag(int errFlag) {
    this.errFlag = errFlag;
  }

  public String getErrMsg() {
    return errMsg;
  }

  public void setErrMsg(String errMsg) {
    this.errMsg = errMsg;
  }
}