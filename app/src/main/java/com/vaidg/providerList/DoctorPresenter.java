package com.vaidg.providerList;

import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.SESSION_LOGOUT;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;
import static com.vaidg.utilities.Constants.WAITFORRESPONSEFROMAPIToMQTT;
import static com.vaidg.utilities.Constants.mqttErrorMsg;
import static com.vaidg.utilities.Constants.selLang;

import android.content.Context;
import android.util.Log;
import com.vaidg.networking.LSPServices;
import com.vaidg.providerList.model.DoctorResponse;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.google.gson.Gson;
import com.mqtt.MQTTManager;
import com.pojo.ErrorHandel;
import com.utility.AlertProgress;
import com.utility.RefreshToken;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * <h2>DoctorPresenter</h2>
 * Created by Ali on 1/29/2018.
 */

public class DoctorPresenter implements DoctorListContract.doctorPresenter {
  @Inject
  LSPServices mLSPServices;
  @Inject
  DoctorListContract.doctorView view;
  @Inject
  Gson mGson;
  @Inject
  MQTTManager mMQTTManager;
  private String TAG = DoctorPresenter.class.getSimpleName();

  @Inject
  DoctorPresenter() {
  }

  @Override
  public void onGetDoctorService(Context context, AlertProgress alertProgress,
                                 SessionManagerImpl sessionmSessionManager,
                                 String auth, String lang, String latitude,
                                 String longitude,
                                 String catId,
                                 int callTypeInOutTele, int bookingType, int distance, String searchType,
                                 String metadata, String scheduledDate, int scheduledTime, long onRepeatEnd,
                                 ArrayList<String> onRepeatDays, String serverTime, String ipAddress,
                                 int waitforresponse, String gender, String availability, String minConsultancy, String maxConsultancy, String inHospital, String sortBy, String cityId) {
    if (waitforresponse > 0) {
      if(view != null) {
        view.onShowProgress();
      }
    }
    Log.d(TAG, "onGetDoctorService: " + gender);
    Map<String, Object> jsonParams = new HashMap<>();
    jsonParams.put("lat", latitude);
    jsonParams.put("long", longitude);
    jsonParams.put("categoryId", catId);
    jsonParams.put("callType", callTypeInOutTele);
    jsonParams.put("bookingType", bookingType);
    jsonParams.put("distance", distance);
    jsonParams.put("search", searchType);
    jsonParams.put("metaData", metadata);
    jsonParams.put("scheduleDate", scheduledDate);
    jsonParams.put("scheduleTime", scheduledTime);
    jsonParams.put("endTimeStamp", onRepeatEnd);
    jsonParams.put("days", onRepeatDays);
    jsonParams.put("deviceTime", serverTime);
    jsonParams.put("ipAddress", ipAddress);
    jsonParams.put("waitForResponse", waitforresponse);
    jsonParams.put("gender", gender);
    jsonParams.put("availability", availability);
    jsonParams.put("minConsultancy", minConsultancy);
    jsonParams.put("maxConsultancy", maxConsultancy);
    jsonParams.put("inHospital", inHospital);
    jsonParams.put("sortByConsultancy", sortBy);
    jsonParams.put("cityId", cityId);
    Log.d(TAG, "onGetDoctorService: " + jsonParams.toString());

    mLSPServices.getProviders(auth,
        lang, PLATFORM_ANDROID, jsonParams)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            String responseBody;
            String errorBody;
            JSONObject jsonObject;
            int responseCode = +responseBodyResponse.code();
            try {
              Log.d(TAG, "onNextProviderCode: " + responseCode);
              responseBody =
                  responseBodyResponse.body() != null ? responseBodyResponse.body().string()
                      : null;
              errorBody =
                  responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string()
                      : null;
              Constants.catId = catId;
              Constants.scheduledDate = scheduledDate;
              Constants.scheduledTime = scheduledTime;
              Log.d(TAG, "onNextProviderResponse: " + responseBody);

              switch (responseCode) {
                case SUCCESS_RESPONSE:
                  if (responseBody != null && !responseBody.isEmpty()) {
                    if (waitforresponse > 0) {
                      DoctorResponse response = mGson.fromJson(responseBody, DoctorResponse.class);

                      WAITFORRESPONSEFROMAPIToMQTT = 0;
                      response.setErrNum(WAITFORRESPONSEFROMAPIToMQTT);
                      response.setErrFlag(SUCCESS_RESPONSE);
                      response.setErrMsg(response.getMessage());
                      if (response.getData() != null && response.getData().size() > 0) {
                        if(view != null) {
                          view.onSuccessDoctorDataList(response.getData());
                        }
                      } else {
                        if (view != null) {
                          view.noProviderAvailable(mqttErrorMsg, true);
                        }
                      }
                    }
                  }
                  if(view != null) {
                    view.onHideProgress();
                  }
                  break;
                case SESSION_LOGOUT:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      if(view != null) {
                      view.onLogout(jsonObject.getString(MESSAGE), sessionmSessionManager);
                    }
                      }
                  }
                  if(view != null) {
                    view.onHideProgress();
                  }
                  break;
                case Constants.SESSION_EXPIRED:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    ErrorHandel errorHandel = mGson.fromJson(errorBody, ErrorHandel.class);
                    RefreshToken.onRefreshToken(auth, sessionmSessionManager.getREFRESHAUTH(), mLSPServices,
                        new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {
                            if(view != null) {
                              view.onHideProgress();
                            }
                            LSPApplication.getInstance().setAuthToken(sessionmSessionManager.getSID(),sessionmSessionManager.getSID(),newToken);
                            onGetDoctorService(context, alertProgress,
                                sessionmSessionManager,
                                newToken, lang, latitude,
                                longitude,
                                catId,
                                callTypeInOutTele, bookingType, distance, searchType,
                                metadata, scheduledDate, scheduledTime, onRepeatEnd,
                                onRepeatDays, serverTime, ipAddress,
                                waitforresponse, gender, availability, minConsultancy, maxConsultancy, inHospital, sortBy, Constants.cityId);
                          }

                          @Override
                          public void onFailureRefreshToken() {

                          }

                          @Override
                          public void sessionExpired(String msg) {
                            if(view != null) {
                              view.onHideProgress();
                              view.onLogout(msg, sessionmSessionManager);
                            }
                          }
                        });
                  }
                  break;

                default:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onError(jsonObject.getString(MESSAGE));
                      }
                    }
                  }
                  if(view != null) {
                    view.onHideProgress();
                  }
                  break;

              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
              if(view != null) {
                view.onHideProgress();
              }
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.onHideProgress();
            }

          }

          @Override
          public void onComplete() {

          }
        });
  }

  @Override
  public void attachView(Object view) {
    //   view = (DoctorListContract.view) view;
  }

  @Override
  public void detachView() {
    //   view = null;
  }


  @Override
  public void onToGetFavouriteProvider(Context mContext, SessionManagerImpl mSessionManager, String selLang, Boolean isFavDoc) {
    if (view != null) {
      view.onShowProgress();
    }
    // providerView.onShowProgress();
    Observable<Response<ResponseBody>> observable = mLSPServices.onToGetFavProvider(LSPApplication.getInstance().getAuthToken(mSessionManager.getSID()), Constants.selLang, Constants.PLATFORM_ANDROID);

    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            String responseBody;
            String errorBody;
            JSONObject jsonObject;
            int responseCode = +responseBodyResponse.code();
            try {
              Log.d(TAG, "onNextProviderCode: " + responseCode);
              responseBody =
                  responseBodyResponse.body() != null ? responseBodyResponse.body().string()
                      : null;
              errorBody =
                  responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string()
                      : null;

              Log.d(TAG, "onNextProviderResponse: " + responseBody);

              switch (responseCode) {
                case SUCCESS_RESPONSE:
                  if (responseBody != null && !responseBody.isEmpty()) {
                    DoctorResponse response = mGson.fromJson(responseBody, DoctorResponse.class);
                    if (response.getData() != null && response.getData().size() > 0) {
                      if (view != null) {
                        view.onSuccessDoctorDataList(response.getData());
                      }
                    } else {
                      if (view != null) {
                        view.noProviderAvailable(mqttErrorMsg, true);
                      }
                    }
                  }
                  if (view != null) {
                    view.onHideProgress();
                  }
                  break;
                case SESSION_LOGOUT:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      if (view != null) {
                        view.onLogout(jsonObject.getString(MESSAGE), mSessionManager);
                      }
                    }
                  }
                  if (view != null) {
                    view.onHideProgress();
                  }
                  break;
                case Constants.SESSION_EXPIRED:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    ErrorHandel errorHandel = mGson.fromJson(errorBody, ErrorHandel.class);
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(mSessionManager.getSID()), mSessionManager.getREFRESHAUTH(), mLSPServices,
                        new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {
                            if (view != null) {
                              view.onHideProgress();
                            }
                            LSPApplication.getInstance().setAuthToken(mSessionManager.getSID(),mSessionManager.getSID(),newToken);
                            onToGetFavouriteProvider(mContext, mSessionManager, selLang, isFavDoc);
                          }

                          @Override
                          public void onFailureRefreshToken() {

                          }

                          @Override
                          public void sessionExpired(String msg) {
                            if (view != null) {
                              view.onHideProgress();
                              view.onLogout(msg, mSessionManager);
                            }
                          }
                        });
                  }
                  break;

                default:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      if (view != null) {
                        view.onError(jsonObject.getString(MESSAGE));
                      }
                    }
                  }
                  if (view != null) {
                    view.onHideProgress();
                  }
                  break;

              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
              if (view != null) {
                view.onHideProgress();
              }
            }
          }

          @Override
          public void onError(Throwable e) {
            if (view != null) {
              view.onHideProgress();
            }
         /*   view.onHideProgress();
            view.onRetry(e.getMessage());*/
          }

          @Override
          public void onComplete() {

          }
        });
  }


  @Override
  public void removeFavDoctor(Context mContext, String providerId, String categoryId, SessionManagerImpl mSessionManager) {
    Observable<Response<ResponseBody>> responseObservable = mLSPServices.removeFromFav(LSPApplication.getInstance().getAuthToken(mSessionManager.getSID()), Constants.selLang, Constants.PLATFORM_ANDROID, categoryId, providerId);
    responseObservable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Response<ResponseBody>>() {
      @Override
      public void onSubscribe(Disposable d) {
      }

      @Override
      public void onNext(Response<ResponseBody> responseBodyResponse) {
        Log.d(TAG, "onNext: " + responseBodyResponse.toString());
        onToGetFavouriteProvider(mContext, mSessionManager, selLang, true);
      }

      @Override
      public void onError(Throwable e) {
        e.printStackTrace();
      }

      @Override
      public void onComplete() {

      }
    });
  }


}
