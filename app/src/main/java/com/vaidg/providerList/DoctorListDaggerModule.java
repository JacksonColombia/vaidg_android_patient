package com.vaidg.providerList;

import com.vaidg.Dagger2.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>DoctorListDaggerModule</h2>
 * Created by Ali on 1/29/2018.
 */

@Module
public interface DoctorListDaggerModule
{
    @Binds
    @ActivityScoped
    DoctorListContract.doctorPresenter providerPresenter(DoctorPresenter providerController);

    @Binds
    @ActivityScoped
    DoctorListContract.doctorView providerViewPresent(DoctorList providerList);

}
