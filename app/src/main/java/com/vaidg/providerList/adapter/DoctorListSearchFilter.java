package com.vaidg.providerList.adapter;


import static com.vaidg.utilities.Constants.WAITFORRESPONSEFROMAPIToMQTT;
import static com.vaidg.utilities.Constants.callTypeInOutTele;
import static com.vaidg.utilities.Constants.latitude;
import static com.vaidg.utilities.Constants.longitude;
import static com.vaidg.utilities.Constants.selLang;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.utility.AlertProgress;
import com.vaidg.R;
import com.vaidg.filter.model.FilterData;
import com.vaidg.home.AutoFitGridRecyclerView;

import com.vaidg.providerList.DoctorListContract;
import com.vaidg.providerList.model.DoctorData;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import dagger.android.support.DaggerAppCompatActivity;
import java.util.ArrayList;
import javax.inject.Inject;

/**
 * Created by Ali on 4/4/2018.
 */
public class DoctorListSearchFilter extends DaggerAppCompatActivity implements DoctorListContract.doctorView{
  @BindView(R.id.recyclerviewSearch)
  RecyclerView recyclerviewSearch;
  @BindView(R.id.etSearch)
  EditText etSearch;
  @BindView(R.id.tvTitle)
  TextView tvTitle;
  @BindView(R.id.ivSearchClear)
  ImageView ivSearchClear;
  @BindView(R.id.progressBarShow)
  ProgressBar progressBarShow;
  AppTypeface appTypeface;
  private ArrayList<DoctorData> mDoctorData;
  private ArrayList<DoctorData>mDoctorDataFilter = new ArrayList<>();
  private DoctorListAdapter mDoctorListAdapter;
  private boolean isEditStop;
  private Boolean isFavDoc = false;
  @Inject
  DoctorListContract.doctorPresenter mDoctorPresenter;
  @Inject
  SessionManagerImpl mSessionManager;
  private Context mContext;
  private String searchType = "";
  private String metadata = "";
  private String gender = "0";
  private String sortBy = "0";
  private String availability = "0";
  private String minConsultancy = "0";
  private String maxConsultancy = "0";
  private String inHospital = "0";
  @Inject
  AlertProgress mAlertProgress;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.layout_doctor_search);
    ButterKnife.bind(this);
    mContext = this;
    appTypeface = AppTypeface.getInstance(this);
    getIntentValue();
    setTypeFaceValue();
  }

  private void getIntentValue() {

    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
   if(getSupportActionBar() != null) {
     getSupportActionBar().setDisplayShowHomeEnabled(true);
     getSupportActionBar().setDisplayHomeAsUpEnabled(true);
     getSupportActionBar().setDisplayShowTitleEnabled(false);
   }
    toolbar.setNavigationIcon(R.drawable.ic_back);
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });


    TextView tvTbTitle = toolbar.findViewById(R.id.tv_center);
    tvTbTitle.setText(R.string.findnbook);
    tvTbTitle.setTypeface(appTypeface.getHind_semiBold());
    tvTitle.setTypeface(appTypeface.getHind_medium());
    tvTitle.setVisibility(View.GONE);
    if (getIntent().getExtras() != null) {

      isFavDoc = getIntent().getBooleanExtra("isFavDoc", false);
      mDoctorData = (ArrayList<DoctorData>) getIntent().getSerializableExtra(
          "mDoctorData");

      // isImageDrawn = true;
    }
    recyclerviewSearch.setHasFixedSize(false);
    // LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
    recyclerviewSearch.setHasFixedSize(true);
 /*   GridLayoutManager linearLayoutManager = new GridLayoutManager(this, 1,
        GridLayoutManager.VERTICAL, false);*/
    recyclerviewSearch.setLayoutManager(new LinearLayoutManager(this));
    mDoctorListAdapter = new DoctorListAdapter(mDoctorDataFilter, appTypeface,isFavDoc);
    recyclerviewSearch.setAdapter(mDoctorListAdapter);
    mDoctorListAdapter.setClickListener((id, categoryId) -> {
      if(Utility.isNetworkAvailable(mContext))
      mDoctorPresenter.removeFavDoctor(mContext,id,categoryId,mSessionManager);
    });
  }

  private void setTypeFaceValue() {
    etSearch.setTypeface(appTypeface.getHind_regular());
    etSearch.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelOffset(R.dimen.sp_12));
    etSearch.setHint(getResources().getString(R.string.search_for_providers));
    etSearch.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }


      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        isEditStop = false;
      }

      @Override
      public void afterTextChanged(final Editable editable) {
        Log.d("TAG", "afterTextChanged: " + editable.toString() + " isEdit " + isEditStop);

        new Handler().postDelayed(new Runnable() {
          @Override
          public void run() {
            WAITFORRESPONSEFROMAPIToMQTT = 1;
            searchType = editable.toString();
            isEditStop = true;
            Log.d("TAG", "afterTextChanged: " + editable.toString()
                + " isEditable " + isEditStop);
            filterMethod(editable.toString());
            if(Utility.isNetworkAvailable(mContext)) {
              Log.d("TAG", "afterTextChanged: " + editable.toString());
              mDoctorPresenter.onGetDoctorService(mContext, mAlertProgress, mSessionManager, LSPApplication.getInstance().getAuthToken(mSessionManager.getSID()), selLang,
                  String.valueOf(latitude), String.valueOf(longitude), Constants.catId,
                  callTypeInOutTele, Constants.bookingType, Constants.distance, searchType, metadata,
                  Constants.scheduledDate, Constants.scheduledTime, Constants.onRepeatEnd,
                  Constants.onRepeatDays,
                  Utility.dateInTwentyFour(Constants.serverTime), mSessionManager.getIpAddress(),
                  WAITFORRESPONSEFROMAPIToMQTT, gender, availability, minConsultancy, maxConsultancy, inHospital, sortBy, Constants.cityId);
            }
          }
        }, 1000);

      }
    });

  }

  private void filterMethod(String s) {
    if (isEditStop) {
      showProgress();
      filter(s);
    }

  }

  private void showProgress() {
    progressBarShow.setVisibility(View.VISIBLE);
  }


  private void filter(String s) {
    // ArrayList<String> catName = new ArrayList<>();

    mDoctorDataFilter.clear();
    mDoctorListAdapter.notifyDataSetChanged();

    for (int i = 0; i < mDoctorData.size(); i++) {
          /* String catNam = inComingList.get(i).getCatName().toLowerCase();
           catName.add(catNam);*/
      // str1.toLowerCase().contains(str2.toLowerCase())
      if (mDoctorData.get(i).getFirstName().toLowerCase().contains(s.toLowerCase()) || mDoctorData.get(i).getLastName().toLowerCase().contains(s.toLowerCase())) {
        mDoctorDataFilter.add(mDoctorData.get(i));
        mDoctorListAdapter.notifyDataSetChanged();
      }

    }
    progressBarShow.setVisibility(View.GONE);
  }

  @OnClick({R.id.ivSearchClear})
  public void clicked(View v) {
    if (v.getId() == R.id.ivSearchClear) {
      mDoctorDataFilter.clear();
      etSearch.setText("");
      mDoctorListAdapter.notifyDataSetChanged();
    } else {
      onBackPressed();
    }
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    finish();

  }

  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);

    //Utility.checkAndShowNetworkError(this);
  }

  @Override
  public void onSuccessDoctorDataList(ArrayList<DoctorData> data) {

  }

  @Override
  public void onFilterRemoveArray(ArrayList<FilterData> filteredResponses) {

  }

  @Override
  public void setLatLng() {

  }

  @Override
  public void onNoConnectionAvailable(String message, boolean isLocation) {

  }

  @Override
  public void setOnItemSelected(String subCatId, String subCatName) {

  }

  @Override
  public void noProviderAvailable(String providerNotAvailable, Boolean isFavDoc) {

  }

  @Override
  public void onSessionExpired() {

  }

  @Override
  public void onLogout(String message, SessionManagerImpl sessionManager) {

  }

  @Override
  public void onError(String error) {

  }

  @Override
  public void onConnectionError(String connectionError) {

  }

  @Override
  public void onShowProgress() {

  }

  @Override
  public void onHideProgress() {

  }
}
