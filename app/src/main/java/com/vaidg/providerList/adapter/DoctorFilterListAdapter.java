package com.vaidg.providerList.adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vaidg.R;
import com.vaidg.utilities.AppTypeface;
import java.util.ArrayList;

/**
 * <h2>DoctorListAdapter</h2>
 * Created by Ali on 1/31/2018.
 */
public class DoctorFilterListAdapter extends RecyclerView.Adapter {
  private static final String TAG = DoctorFilterListAdapter.class.getSimpleName();
  private Context mContext;
  private ArrayList<String> mStringArrayList;
  private AppTypeface mAppTypeface;
  private OnItemClickListener mItemClickListener;
  private String minConsultancy="0",maxConsultancy="0";

  public DoctorFilterListAdapter(ArrayList<String> stringArrayList,
                                 AppTypeface appTypeface, String minConsultancy, String maxConsultancy) {
    this.mStringArrayList = stringArrayList;
    this.mAppTypeface = appTypeface;
    this.minConsultancy = minConsultancy;
    this.maxConsultancy = maxConsultancy;
  }

  public interface OnItemClickListener {

    void onItemClick(View view, int position,
                     ArrayList<String> stringArrayList, String minConsultancy, String maxConsultancy);
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_filterprovider_view, parent, false);
    mContext = parent.getContext();
    return new Viewholdr(view);
  }

  public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
    this.mItemClickListener = mItemClickListener;
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    final Viewholdr holdr = (Viewholdr) holder;
    onDataSet(mStringArrayList, position, holdr);
  }


  private void onDataSet(ArrayList<String> stringArrayList, int position, Viewholdr holdr) {
      holdr.tvEmailId.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_12));
      holdr.tvEmailId.setText(stringArrayList.get(position).toString());
  }


  @Override
  public int getItemCount() {
    return mStringArrayList == null ? 0 : mStringArrayList.size();
  }


  class Viewholdr extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.chkSelected)
    TextView chkSelected;
    @BindView(R.id.tvEmailId)
    TextView tvEmailId;


    public Viewholdr(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      mAppTypeface = AppTypeface.getInstance(mContext);
      tvEmailId.setTypeface(mAppTypeface.getHind_medium());
      itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

      if (mItemClickListener != null) {
        mItemClickListener.onItemClick(v, getAdapterPosition(), mStringArrayList,minConsultancy,maxConsultancy);
      }
    }
  }

}
