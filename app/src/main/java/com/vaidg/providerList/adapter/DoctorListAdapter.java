package com.vaidg.providerList.adapter;

import static com.vaidg.utilities.Constants.W_15;
import static com.vaidg.utilities.Constants.W_22;
import static com.vaidg.utilities.Constants.W_320;
import static com.vaidg.utilities.Constants.W_36;
import static com.vaidg.utilities.Constants.W_40;
import static com.vaidg.utilities.Constants.catId;
import static com.vaidg.utilities.Constants.catName;
import static com.vaidg.utilities.Constants.latitude;
import static com.vaidg.utilities.Constants.longitude;
import static com.vaidg.utilities.Constants.proAddress;
import static com.vaidg.utilities.Constants.proId;
import static com.vaidg.utilities.Constants.proLatitude;
import static com.vaidg.utilities.Constants.proLongitude;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.vaidg.R;
import com.vaidg.confirmbookactivity.ConfirmBookActivity;
import com.vaidg.inCallOutCall.TimeSlots;
import com.vaidg.incalloutcalltelecall.InCallOutCallTeleCallActivity;
import com.vaidg.providerList.model.DoctorData;
import com.vaidg.providerList.model.DoctorFavCatData;
import com.vaidg.providerdetails.ProviderDetails;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.Utility;
import com.utility.PicassoTrustAll;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;

/*Get Colored String:

    String name = getColoredSpanned("Hiren", "#800000");
    String surName = getColoredSpanned("Patel","#000080");
    Set Text on TextView of two strings with different colors:

    txtView.setText(Html.fromHtml(name+" "+surName));*/

/**
 * <h2>DoctorListAdapter</h2>
 * Created by Ali on 1/31/2018.
 */
public class DoctorListAdapter extends RecyclerView.Adapter {
  private static final String TAG = DoctorListAdapter.class.getSimpleName();
  private OnItemClickListener listener;
  private Context mContext;
  private ArrayList<DoctorData> mDoctorData;
  private AppTypeface mAppTypeface;
  private Boolean mIsFavDoc = false;
  private boolean inCall = false;
  private boolean outCall = false;
  private boolean teleCall = false;
  private int w22;

  public DoctorListAdapter(ArrayList<DoctorData> doctorData,
                           AppTypeface appTypeface, Boolean isFavDoc) {
    this.mDoctorData = doctorData;
    this.mAppTypeface = appTypeface;
    this.mIsFavDoc = isFavDoc;
    calculate();
  }

  private void calculate() {
    w22 = Utility.getScreenWidth() * W_36 / W_320;
  }


  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_provider_view, parent, false);
    mContext = parent.getContext();
    return new Viewholdr(view);
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    final Viewholdr holdr = (Viewholdr) holder;
    onDataSet(mDoctorData, position, holdr);
  }


  private void onDataSet(ArrayList<DoctorData> doctorData, int position, Viewholdr holdr) {
    holdr.tvMyEventDocName.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
    holdr.tvMyEvnetDocCat.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_13));
    holdr.tvMyEvnetDocExp.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_13));
    holdr.tvMyEvnetDocLike.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_12));
    holdr.tvMyEvnetDocPatientExp.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_12));
    holdr.tvMyEventDocLoc.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_13));
    holdr.tvMyEventDocConsultFee.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_12));
    holdr.tvMyEventDocAvail.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_10));
    holdr.btnViewDocf.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
    holdr.btnBookApp.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
    holdr.btnViewDocf.getLayoutParams().height = w22;
    holdr.btnBookApp.getLayoutParams().height = w22;
    if (mIsFavDoc) {
      holdr.ivArrow.setVisibility(View.GONE);
      holdr.lvConsultFee.setVisibility(View.GONE);
      holdr.btnBookApp.setText(mContext.getResources().getString(R.string.remove_Cart));
      holdr.btnViewDocf.setText(mContext.getResources().getString(R.string.book_appointment));
        holdr.tvMyEvnetDocCat.setText(doctorData.get(position).getCategoryArr().getCatName());
    } else {
      holdr.ivArrow.setVisibility(View.VISIBLE);
      holdr.lvConsultFee.setVisibility(View.VISIBLE);
      holdr.btnViewDocf.setText(mContext.getResources().getString(R.string.view_docfile));
      holdr.btnBookApp.setText(mContext.getResources().getString(R.string.book_appointment));
      holdr.tvMyEvnetDocCat.setText(catName);
    }
    if (doctorData.get(position).getStatus() == 1) {
      holdr.ivOnline.setBackground(mContext.getDrawable(R.drawable.circle_green));
    } else {
      holdr.ivOnline.setBackground(mContext.getDrawable(R.drawable.circle_grey));
    }

    String doctorName = doctorData.get(position).getTitle() + " " + doctorData.get(
        position).getFirstName() + " " + doctorData.get(position).getLastName();
    String amountString = doctorData.get(position).getCurrencySymbol() + " " + doctorData.get(
        position).getAmount();
    String consultationFee = mContext.getResources().getString(R.string.consulationFee) + " : ";
    String yearsOfExperience = doctorData.get(position).getYearOfExperience() + " "
        + mContext.getResources().getString(R.string.yrsOfExp);
    String patientExperience = doctorData.get(position).getTotalBooking() + " "
        + mContext.getResources().getString(R.string.patientExp);
    String doctorLike = round(doctorData.get(position).getAverageRating(),2)+" ";
        //+ " " + mContext.getResources().getString(R.string.percentage);
    holdr.tvMyEventDocName.setText(doctorName);
    holdr.tvMyEvnetDocPatientExp.setText(patientExperience);
    holdr.tvMyEvnetDocExp.setText(yearsOfExperience);
    holdr.tvMyEventDocLoc.setText(doctorData.get(position).getAddress().getAddLine1());
    holdr.tvMyEventDocConsultFee.setText(consultationFee);
    holdr.tvMyEventDocAmount.setText(amountString);
    holdr.tvMyEvnetDocLike.setText(doctorLike);

    if(doctorData.get(position).getImage() != null && !doctorData.get(position).getImage().isEmpty()) {
      PicassoTrustAll.getInstance(mContext)
              .load(doctorData.get(position).getImage())
              .placeholder(R.drawable.ic_fever_placeholder)   // optional
              .error(R.drawable.ic_fever_placeholder)// optional
              .transform(new PicassoCircleTransform())
              .into(holdr.ivMyEventDocPic);
    }


    holdr.btnBookApp.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (mIsFavDoc) {
          if (listener != null) {
            for (DoctorFavCatData doctorFavCatData : doctorData.get(position).getCategoryData())
              listener.onItemClick(doctorData.get(position).getId(), doctorFavCatData.getCategoryId());
          }
        } else {
          proLatitude = doctorData.get(position).getLocation().getLatitude();
          proLongitude = doctorData.get(position).getLocation().getLongitude();
          proId = doctorData.get(position).getId();
          Intent intent;
          if (Constants.callTypeInOutTele != 2) //if telecall and incall
          {
            proAddress = doctorData.get(position).getAddress().getAddLine1();
            intent = new Intent(mContext, TimeSlots.class);
            intent.putExtra("ProviderId", proId);
            intent.putExtra("ImagePro", doctorData.get(position).getImage());
            intent.putExtra("expertise",catName);
            intent.putExtra("NAME",
                doctorData.get(position).getTitle() + " " + doctorData.get(
                    position).getFirstName() + " " + doctorData.get(position).getLastName());
            intent.putExtra("isProFileView", false);

          } else {
         /*   proLatitude = doctorData.get(position).getLocation().getLatitude();
            proLongitude = doctorData.get(position).getLocation().getLongitude();
            proAddress = doctorData.get(position).getAddress().getAddLine1();
          */  intent = new Intent(mContext, ConfirmBookActivity.class);
          }
          //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
          mContext.startActivity(intent);
          ((Activity) mContext).overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
        }
      }
    });

    holdr.itemView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (!mIsFavDoc) {
          holdr.btnViewDocf.performClick();
        }
      }
    });

    holdr.btnViewDocf.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (mIsFavDoc) {
          Bundle bundle = new Bundle();
          latitude = doctorData.get(position).getAddress().getLatitude();
          longitude = doctorData.get(position).getAddress().getLongitude();

          HashMap<Integer, Boolean> hashMap = new HashMap<>();
          hashMap.put(1, doctorData.get(position).getCategoryArr().getCallType().isIncall());
          hashMap.put(2, doctorData.get(position).getCategoryArr().getCallType().isOutcall());
          hashMap.put(3, doctorData.get(position).getCategoryArr().getCallType().isTelecall());
          // ConstantsInteface.callTypeInOutTele = doctorData.get(position).getCategoryArr().getJobType();
          catId = doctorData.get(position).getCategoryArr().getId();
          catName = doctorData.get(position).getCategoryArr().getCatName();
          Constants.bookingModel = doctorData.get(position).getCategoryArr().getBillingModel();
          Constants.serviceType = doctorData.get(position).getCategoryArr().getServiceType();
        //  Constants.offers = doctorData.get(position).getCategoryArr().getOffers();
          Constants.callType = doctorData.get(position).getCategoryArr().getCallType();
          Constants.minFee = doctorData.get(position).getCategoryArr().getMinimumFeesForConsultancy();
          Constants.maxFee = doctorData.get(position).getCategoryArr().getMaximumFeesForConsultancy();
          proId = doctorData.get(position).getId();
          if (Constants.callTypeInOutTele != 2) //if telecall and incall
          {
            proLatitude = doctorData.get(position).getLocation().getLatitude();
            proLongitude = doctorData.get(position).getLocation().getLongitude();
            proAddress = doctorData.get(position).getAddress().getAddLine1();
          }
         Constants.visitFee = 0;
          Intent intent = new Intent(mContext, InCallOutCallTeleCallActivity.class);//BookingType
          //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
          callIntent(intent, doctorData.get(position), bundle,mIsFavDoc);
        } else {
          proId = doctorData.get(position).getId();
          if (Constants.callTypeInOutTele != 2) //if telecall and incall
          {
            proLatitude = doctorData.get(position).getLocation().getLatitude();
            proLongitude = doctorData.get(position).getLocation().getLongitude();
            proAddress = doctorData.get(position).getAddress().getAddLine1();
          }
          Intent intent = new Intent(mContext, ProviderDetails.class);
          intent.putExtra("ProviderId", proId);
          intent.putExtra("ImagePro", doctorData.get(position).getImage());
          intent.putExtra("expertise", catName);
          intent.putExtra("NAME",
              doctorData.get(position).getTitle() + " " + doctorData.get(
                  position).getFirstName() + " " + doctorData.get(position).getLastName());
          intent.putExtra("isProFileView", false);
          //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
          mContext.startActivity(intent);
          ((Activity) mContext).overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
        }
      }
    });
  }

  public static double round(double value, int places) {
    if (places < 0) throw new IllegalArgumentException();

    BigDecimal bd = new BigDecimal(value);
    bd = bd.setScale(places, RoundingMode.HALF_UP);
    return bd.doubleValue();
  }

  private void callIntent(Intent intent, DoctorData doctorData, Bundle bundle, Boolean mIsFavDoc) {
    bundle.putString("CatId", doctorData.getCategoryArr().getId());
   // bundle.putSerializable("SubCat", doctorData.getCategoryArr().getSubCategory());
    bundle.putDouble("MinAmount", doctorData.getCategoryArr().getMinimumFees());
    bundle.putDouble("MaxAmount", doctorData.getCategoryArr().getMaximumFees());
    bundle.putSerializable("BookingTypeAction", doctorData.getCategoryArr().getBookingTypeAction());
    bundle.putSerializable("CallType", doctorData.getCategoryArr().getCallType());
   // bundle.putSerializable("OFFERS", doctorData.getCategoryArr().getOffers());
    bundle.putBoolean("isFavDoc", mIsFavDoc);
    intent.putExtra("ProviderId", proId);
    intent.putExtra("ImagePro", doctorData.getImage());
    intent.putExtra("expertise", catName);
    intent.putExtra("NAME",
        doctorData.getTitle() + " " + doctorData.getFirstName() + " " + doctorData.getLastName());
    intent.putExtra("isProFileView", false);
    intent.putExtras(bundle);
    //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    mContext.startActivity(intent);
    ((Activity) mContext).overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
  }


  @Override
  public int getItemCount() {
    return mDoctorData == null ? 0 : mDoctorData.size();
  }

  public void setClickListener(OnItemClickListener itemClickListener) {
    this.listener = itemClickListener;
  }

  public interface OnItemClickListener {

    void onItemClick(String id, String categoryId);
  }

  class Viewholdr extends RecyclerView.ViewHolder {
    @BindView(R.id.ivMyEventDocPic)
    ImageView ivMyEventDocPic;
    @BindView(R.id.tvMyEventDocName)
    TextView tvMyEventDocName;
    @BindView(R.id.ivOnline)
    ImageView ivOnline;
    @BindView(R.id.ivArrow)
    ImageView ivArrow;
    @BindView(R.id.lvConsultFee)
    LinearLayout lvConsultFee;
    @BindView(R.id.tvMyEvnetDocCat)
    TextView tvMyEvnetDocCat;
    @BindView(R.id.tvMyEvnetDocExp)
    TextView tvMyEvnetDocExp;
    @BindView(R.id.tvMyEvnetDocPatientExp)
    TextView tvMyEvnetDocPatientExp;
    @BindView(R.id.tvMyEvnetDocLike)
    TextView tvMyEvnetDocLike;
    @BindView(R.id.tvMyEventDocLoc)
    TextView tvMyEventDocLoc;
    @BindView(R.id.tvMyEventDocConsultFee)
    TextView tvMyEventDocConsultFee;
    @BindView(R.id.tvMyEventDocAmount)
    TextView tvMyEventDocAmount;
    @BindView(R.id.tvMyEventDocAvail)
    TextView tvMyEventDocAvail;
    @BindView(R.id.btnViewDocf)
    TextView btnViewDocf;
    @BindView(R.id.btnBookApp)
    TextView btnBookApp;


    public Viewholdr(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      mAppTypeface = AppTypeface.getInstance(mContext);
      tvMyEventDocName.setTypeface(mAppTypeface.getHind_semiBold());
      tvMyEvnetDocCat.setTypeface(mAppTypeface.getHind_regular());
      tvMyEvnetDocExp.setTypeface(mAppTypeface.getHind_regular());
      tvMyEvnetDocPatientExp.setTypeface(mAppTypeface.getHind_regular());
      tvMyEvnetDocLike.setTypeface(mAppTypeface.getHind_regular());
      tvMyEventDocLoc.setTypeface(mAppTypeface.getHind_medium());
      tvMyEventDocAmount.setTypeface(mAppTypeface.getHind_semiBold());
      tvMyEventDocConsultFee.setTypeface(mAppTypeface.getHind_regular());
      tvMyEventDocAvail.setTypeface(mAppTypeface.getHind_regular());
      btnBookApp.setTypeface(mAppTypeface.getHind_medium());
      btnViewDocf.setTypeface(mAppTypeface.getHind_medium());

    }

  }
}
