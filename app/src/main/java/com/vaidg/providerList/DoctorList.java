package com.vaidg.providerList;

import static com.vaidg.utilities.Constants.WAITFORRESPONSEFROMAPIToMQTT;
import static com.vaidg.utilities.Constants.callTypeInOutTele;
import static com.vaidg.utilities.Constants.customerHandler;
import static com.vaidg.utilities.Constants.customerHomePageInterval;
import static com.vaidg.utilities.Constants.latitude;
import static com.vaidg.utilities.Constants.longitude;
import static com.vaidg.utilities.Constants.mqttErrorMsg;
import static com.vaidg.utilities.Constants.selLang;

import adapters.FilterAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.vaidg.R;
import com.vaidg.filter.FilterActivity;
import com.vaidg.filter.model.FilterData;
import com.vaidg.providerList.adapter.DoctorFilterListAdapter;
import com.vaidg.providerList.adapter.DoctorListAdapter;
import com.vaidg.providerList.adapter.DoctorListSearchFilter;
import com.vaidg.providerList.model.DoctorData;
import com.vaidg.providerList.model.DoctorObservable;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.google.gson.Gson;
import com.mqtt.MQTTManager;
import com.mqtt.MqttEvents;
import com.utility.AlertProgress;
import com.utility.ShimmerLayout;
import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.inject.Inject;

/**
 * <h2>DoctorList</h2>
 * Created by Ali on 1/29/2018.
 */

public class DoctorList extends DaggerAppCompatActivity implements DoctorListContract.doctorView,
    Runnable, DoctorListAdapter.OnItemClickListener {
  public static final String PROVIDERLIST = "DoctorList";
  @BindView(R.id.tv_center)
  TextView tv_center;
  @BindView(R.id.tv_skip)
  TextView tv_skip;
  @BindView(R.id.progressBar)
  ProgressBar mProgressBar;
  @BindView(R.id.shimmerProvider)
  ShimmerLayout shimmerLayout;
  @BindView(R.id.rlToolImage)
  LinearLayout rlToolImage;
  @BindView(R.id.nestedScrollViewProList)
  NestedScrollView nestedScrollViewProList;
  @BindView(R.id.rlSearchLayout)
  RelativeLayout rlSearchLayout;
  @BindView(R.id.tvSearch)
  TextView tvSearch;
  @BindView(R.id.tvCancelFilter)
  TextView tvCancelFilter;
  @BindView(R.id.mRecyclerViewDoctorList)
  RecyclerView mRecyclerViewDoctorList;
  @BindView(R.id.mRecyclerViewFilter)
  RecyclerView mRecyclerViewFilter;
  @BindView(R.id.llNoProviderAvailable)
  LinearLayout llNoProviderAvailable;
  @BindView(R.id.tvProviderNotAvailable)
  TextView tvProviderNotAvailable;
  @Inject
  AppTypeface mAppTypeface;
  @Inject
  DoctorListContract.doctorPresenter mDoctorPresenter;
  @Inject
  SessionManagerImpl mSessionManager;
  @Inject
  CompositeDisposable mCompositeDisposable;
  @Inject
  DoctorObservable mDoctorObservable;
  @Inject
  MQTTManager mqttManager;
  @Inject
  AlertProgress mAlertProgress;
  Observer<ArrayList<DoctorData>> mArrayListObserver;
  Runnable runnableCode;
  private Context mContext;
  private String TAG = DoctorList.class.getSimpleName();
  private String searchType = "";
  private String metadata = "";
  private String catId;
  private double minAmount, maxAmount;
  private DoctorListAdapter mDoctorListAdapter;
  private DoctorFilterListAdapter mDoctorFliterListAdapter;
  private ArrayList<DoctorData> mDoctorData;
  private ArrayList<String> mStringArrayList;
  private ArrayList<FilterData> filteredResponseArray;
  private FilterAdapter filterAdapter;
  private Handler handler = new Handler();
  private String gender = "0";
  private String sortBy = "0";
  private String availability = "0";
  private String minConsultancy = "0";
  private String maxConsultancy = "0";
  private String inHospital = "0";
  private String key = "";
  private String value = "";
  private Boolean isFavDoc = false;
  private HashMap<String, String> hashMap = new HashMap<String, String>();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_provider_list);
    Constants.mqttErrorMsg = "";
    ButterKnife.bind(this);
    mContext = this;
    if (getIntent().getExtras() != null) {

      isFavDoc = getIntent().getBooleanExtra("isFavDoc", false);
    }

    initializeToolBar();

  }


  @Override
  protected void onResume() {
    super.onResume();
    Log.d(TAG, "onResume: "+isFavDoc);
    if (isFavDoc) {
      if(Utility.isNetworkAvailable(mContext))
      mDoctorPresenter.onToGetFavouriteProvider(mContext, mSessionManager, selLang,isFavDoc);
    } else {
      WAITFORRESPONSEFROMAPIToMQTT = 1;
      if(Utility.isNetworkAvailable(mContext)) {
        mDoctorPresenter.onGetDoctorService(mContext, mAlertProgress, mSessionManager, LSPApplication.getInstance().getAuthToken(mSessionManager.getSID()), selLang,
            String.valueOf(latitude), String.valueOf(longitude), Constants.catId,
            callTypeInOutTele, Constants.bookingType, Constants.distance, searchType, metadata,
            Constants.scheduledDate, Constants.scheduledTime, Constants.onRepeatEnd,
            Constants.onRepeatDays,
            Utility.dateInTwentyFour(Constants.serverTime), mSessionManager.getIpAddress(),
            WAITFORRESPONSEFROMAPIToMQTT, gender, availability, minConsultancy, maxConsultancy, inHospital, sortBy, Constants.cityId);
      }
      initializeRxJava();
      if (mqttManager.isMQTTConnected()) {
        mqttManager.subscribeToTopic(MqttEvents.Provider.value + "/" + mSessionManager.getSID(), 1);
      }
      Log.d(TAG, "onCreateIsMqttConnected: " + mqttManager.isMQTTConnected());
    }
  }


  private void initializeRxJava() {

    mArrayListObserver = new Observer<ArrayList<DoctorData>>() {
      @Override
      public void onSubscribe(Disposable d) {

      }

      @Override
      public void onNext(ArrayList<DoctorData> doctorData) {
        if (doctorData.size() > 0) {
          mqttErrorMsg = "";
          mDoctorData.clear();
          mDoctorData.addAll(doctorData);
          Log.w(TAG, "onNextLocation: " + doctorData.size());
          mDoctorListAdapter.notifyDataSetChanged();
        } else {
          noProviderAvailable(mqttErrorMsg, isFavDoc);
        }

      }

      @Override
      public void onError(Throwable e) {
        e.printStackTrace();
      }

      @Override
      public void onComplete() {

      }
    };
    mDoctorObservable.subscribe(mArrayListObserver);

  }


  @Override
  public void run() {
    runnableCode = new Runnable() {
      public void run() {
        Log.d(TAG, "running runnableCodeInside: ");
        if(Utility.isNetworkAvailable(mContext)) {
          mDoctorPresenter.onGetDoctorService(mContext, mAlertProgress, mSessionManager, LSPApplication.getInstance().getAuthToken(mSessionManager.getSID()), selLang,
              String.valueOf(latitude), String.valueOf(longitude), Constants.catId,
              callTypeInOutTele, Constants.bookingType, Constants.distance, searchType, metadata,
              Constants.scheduledDate, Constants.scheduledTime, Constants.onRepeatEnd,
              Constants.onRepeatDays,
              Utility.dateInTwentyFour(Constants.serverTime), mSessionManager.getIpAddress(),
              WAITFORRESPONSEFROMAPIToMQTT, gender, availability, minConsultancy, maxConsultancy, inHospital, sortBy, Constants.cityId);
        }
       /* Calendar c = Calendar.getInstance();

        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        Log.d(TAG, "run: "+timeOfDay);
        if(timeOfDay >= 0 && timeOfDay < 12){
          Toast.makeText(DoctorList.this, "Good Morning", Toast.LENGTH_SHORT).show();
          Log.d(TAG, "Good Morning: "+timeOfDay);
        }else if(timeOfDay >= 12 && timeOfDay < 16){
          Toast.makeText(DoctorList.this, "Good Afternoon", Toast.LENGTH_SHORT).show();
          Log.d(TAG, "Good Afternoon: "+timeOfDay);
        }else if(timeOfDay >= 16 && timeOfDay < 21){
          Toast.makeText(DoctorList.this, "Good Evening", Toast.LENGTH_SHORT).show();
          Log.d(TAG, "Good Evening: "+timeOfDay);
        }else if(timeOfDay >= 21 && timeOfDay < 24){
          Toast.makeText(DoctorList.this, "Good Night", Toast.LENGTH_SHORT).show();
          Log.d(TAG, "Good Night: "+timeOfDay);
        }
*/
        handler.postDelayed(this, customerHandler * customerHomePageInterval);
      }
    };

    handler.postDelayed(runnableCode, customerHandler * customerHomePageInterval);
    Log.d(TAG, "runningOutside: ");

  }

  private void initializeToolBar() {
    Toolbar providerToolBar = findViewById(R.id.toolbar);
    setSupportActionBar(providerToolBar);
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayShowHomeEnabled(true);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
    tv_center.setTypeface(mAppTypeface.getHind_semiBold());
    tvSearch.setTypeface(mAppTypeface.getHind_regular());
    tvProviderNotAvailable.setTypeface(mAppTypeface.getHind_medium());
    tvSearch.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelOffset(R.dimen.sp_14));
    tvSearch.setCursorVisible(false);

    if (isFavDoc) {
      tv_skip.setVisibility(View.GONE);
      tv_center.setText(getResources().getString(R.string.favouriteProvider));//getString(R.string.bookingType)
      tvProviderNotAvailable.setText(getResources().getString(R.string.yourFavouriteProvider));
    } else {
      tv_center.setText(getResources().getString(R.string.findnbook));//getString(R.string.bookingType)
      tv_skip.setVisibility(View.VISIBLE);
      tv_skip.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_filter, 0);
      tvProviderNotAvailable.setText(getResources().getString(R.string.providerNotAvailable));
    }
    // providerToolBar.setNavigationIcon(R.drawable.ic_back);
    providerToolBar.setNavigationOnClickListener(view -> onBackPressed());
    mDoctorData = new ArrayList<>();
    mStringArrayList = new ArrayList<>();
    mRecyclerViewDoctorList.setNestedScrollingEnabled(false);
    mRecyclerViewDoctorList.setHasFixedSize(true);
    mDoctorListAdapter = new DoctorListAdapter(mDoctorData, mAppTypeface, isFavDoc);
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
    mRecyclerViewDoctorList.setLayoutManager(linearLayoutManager);
    mRecyclerViewDoctorList.setItemAnimator(new DefaultItemAnimator());
    mRecyclerViewDoctorList.setAdapter(mDoctorListAdapter);
    mDoctorListAdapter.setClickListener(this);

    tvSearch.setOnClickListener(v -> {
      if (mDoctorData.size() > 0) {
        Intent intent = new Intent(this, DoctorListSearchFilter.class);
        intent.putExtra("mDoctorData", mDoctorData);
        intent.putExtra("isFavDoc", isFavDoc);
        //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
      }
    });

    // addTextListener();//it is made below to add searchType operation
  }

  @Override
  public boolean dispatchTouchEvent(MotionEvent event) {
    /*if (event.getAction() == MotionEvent.ACTION_DOWN) {
      View v = getCurrentFocus();
      if ( v instanceof EditText) {
        Rect outRect = new Rect();
        v.getGlobalVisibleRect(outRect);
        if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
          v.clearFocus();
          tvSearch.setCursorVisible(false);
          InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
          if (imm != null) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
          }
        }
      }
    }*/
    return super.dispatchTouchEvent(event);
  }

  private void addTextListener() {
    tvSearch.addTextChangedListener(new TextWatcher() {

      public void afterTextChanged(Editable s) {
        /*Utility.hideKeyboard(DoctorList.this);*/
      }

      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      public void onTextChanged(CharSequence query, int start, int before, int count) {

        searchType = query.toString().toLowerCase();

        final ArrayList<DoctorData> filteredList = new ArrayList<>();

        for (int i = 0; i < mDoctorData.size(); i++) {

          final String text = mDoctorData.get(i).getFirstName().toLowerCase().trim() + " " + mDoctorData.get(i).getLastName().toLowerCase().trim();
          if (text.contains(searchType)) {
            filteredList.add(mDoctorData.get(i));
          }
        }

        mRecyclerViewDoctorList.setHasFixedSize(true);
        mDoctorListAdapter = new DoctorListAdapter(filteredList, mAppTypeface, isFavDoc);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(DoctorList.this);
        mRecyclerViewDoctorList.setLayoutManager(linearLayoutManager);
        mRecyclerViewDoctorList.setItemAnimator(new DefaultItemAnimator());
        mRecyclerViewDoctorList.setAdapter(mDoctorListAdapter);
        mDoctorListAdapter.setClickListener(DoctorList.this);
        mDoctorListAdapter.notifyDataSetChanged();// data set changed

      }
    });
  }

  @OnClick(R.id.ivFilter)
  public void filterClicked() {
    if (mDoctorData.size() > 0) {
      Intent intent = new Intent(this, FilterActivity.class);
      intent.putExtra("map", hashMap);
      //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
      startActivityForResult(intent, Constants.FILTER_RESULT_CODE);
      overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
    }
  }

  @Override
  public void onSessionExpired() {

  }

  @Override
  public void onLogout(String message, SessionManagerImpl sessionManager) {
    if(mAlertProgress != null) {
      mAlertProgress.alertPositiveOnclick(mContext, message, getString(R.string.logout),
              getString(R.string.ok), isClicked -> Utility.setMAnagerWithBID(mContext, mSessionManager));
    }
  }

  @Override
  public void onError(String error) {

  }

  @Override
  public void onConnectionError(String connectionError) {

  }

  @Override
  public void onShowProgress() {
    if (shimmerLayout != null) {
      shimmerLayout.setVisibility(View.VISIBLE);
      shimmerLayout.startShimmerAnimation();
    }
    mProgressBar.setVisibility(View.GONE);
  }

  @Override
  public void onHideProgress() {
    if (shimmerLayout != null) {
      shimmerLayout.stopShimmerAnimation();
      shimmerLayout.setVisibility(View.GONE);
    }
    mProgressBar.setVisibility(View.GONE);
  }

  @Override
  public void onSuccessDoctorDataList(ArrayList<DoctorData> doctorData) {
    if (isFavDoc) {
      if (doctorData.size() > 0) {
        nestedScrollViewProList.setVisibility(View.VISIBLE);
        llNoProviderAvailable.setVisibility(View.GONE);
        mDoctorData.clear();
        mDoctorData.addAll(doctorData);
        mDoctorListAdapter.notifyDataSetChanged();
      } else {
        noProviderAvailable(mqttErrorMsg, isFavDoc);
      }
    } else {
      if (doctorData.size() > 0) {
        mqttErrorMsg = "";
        noProviderAvailable(mqttErrorMsg, isFavDoc);
        mDoctorData.clear();
        mDoctorData.addAll(doctorData);
        mDoctorListAdapter.notifyDataSetChanged();
        run();
      } else {
        noProviderAvailable(mqttErrorMsg, isFavDoc);
        run();
      }
    }
  }

  @Override
  public void onFilterRemoveArray(ArrayList<FilterData> filteredResponsed) {
  }

  @Override
  public void setLatLng() {

  }

  @Override
  public void onNoConnectionAvailable(String message, boolean isLocation) {

  }

  @Override
  public void setOnItemSelected(String subCatId, String subCatName) {
  }


  @OnClick({R.id.tvSearch, R.id.tv_skip, R.id.tvCancelFilter})
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.tvSearch:
        tvSearch.setCursorVisible(true);
        break;
      case R.id.tvCancelFilter:
        if (mStringArrayList.size() > 0) {
          mStringArrayList.clear();
          gender = "0";
          sortBy = "0";
          availability = "0";
          minConsultancy = "0";
          maxConsultancy = "0";
          inHospital = "0";
          mDoctorFliterListAdapter.notifyDataSetChanged();
          if (mDoctorFliterListAdapter.getItemCount() == 0) {
            tvCancelFilter.setVisibility(View.GONE);
            mRecyclerViewFilter.setVisibility(View.GONE);
          }
        }
        break;
      case R.id.tv_skip:
        if (mDoctorData.size() > 0) {
          Intent intent = new Intent(this, FilterActivity.class);
          intent.putExtra("map", hashMap);
          //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
          startActivityForResult(intent, Constants.FILTER_RESULT_CODE);
          overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
        }
        break;
    }
  }


  @Override
  protected void onDestroy() {
    super.onDestroy();
    Log.d(TAG, "onDestroy: ");
    if (!isFavDoc) {
      mqttErrorMsg = "";
      searchType = "";
      WAITFORRESPONSEFROMAPIToMQTT = 1;
      mqttManager.unSubscribeToTopic(MqttEvents.Provider.value + "/" + mSessionManager.getSID());
      mqttManager.onModelResponseSet();
      //  handler.removeCallbacks(runnableCode);
      handler.removeCallbacksAndMessages(null);
    }
  }


  @Override
  protected void onStop() {
    super.onStop();
    Log.d(TAG, "onStop: ");
    if (!isFavDoc) {
      mqttErrorMsg = "";
      searchType = "";
      WAITFORRESPONSEFROMAPIToMQTT = 1;
      mqttManager.unSubscribeToTopic(MqttEvents.Provider.value + "/" + mSessionManager.getSID());
      mqttManager.onModelResponseSet();
      // handler.removeCallbacksAndMessages(runnableCode);
      handler.removeCallbacksAndMessages(null);
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    if(isFavDoc){
      isFavDoc = true;
    }else {
      Log.d(TAG, "onPause: ");
      isFavDoc = false;
      mqttErrorMsg = "";
      searchType = "";
      WAITFORRESPONSEFROMAPIToMQTT = 1;
      mqttManager.unSubscribeToTopic(MqttEvents.Provider.value + "/" + mSessionManager.getSID());
      mqttManager.onModelResponseSet();
      // handler.removeCallbacksAndMessages(runnableCode);
      handler.removeCallbacksAndMessages(null);
    }
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    if(!isFavDoc) {
      mqttErrorMsg = "";
      searchType = "";
      WAITFORRESPONSEFROMAPIToMQTT = 1;
      mqttManager.unSubscribeToTopic(MqttEvents.Provider.value + "/" + mSessionManager.getSID());
      mqttManager.onModelResponseSet();
      handler.removeCallbacksAndMessages(null);
      hashMap.clear();
    }
    finish();
    overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode == Constants.FILTER_RESULT_CODE) {
      if (resultCode == RESULT_OK) {
        handler.removeCallbacksAndMessages(null);
        mStringArrayList.clear();
        hashMap = (HashMap<String, String>) data.getSerializableExtra("map");
        gender = data.getStringExtra("gender");
        sortBy = data.getStringExtra("sortBy");
        availability = data.getStringExtra("availability");
        minConsultancy = data.getStringExtra("minConsultancy");
        maxConsultancy = data.getStringExtra("maxConsultancy");
        inHospital = data.getStringExtra("inHospital");
        Constants.filteredAddress = data.getStringExtra("address");
        Constants.distance = data.getIntExtra("distance", 0);

        metadata = new Gson().toJson(hashMap.values());
        Iterator myVeryOwnIterator = hashMap != null ? hashMap.keySet().iterator() : null;
        while (myVeryOwnIterator != null && myVeryOwnIterator.hasNext()) {
          key = (String) myVeryOwnIterator.next();
          value = hashMap.get(key);
          if (key.equals("6") || key.equals("7")) {

          } else {
            mStringArrayList.add(value);
          }
        }
        // metadata = new Gson().toJson(hashMap.values());
      /*  Iterator myVeryOwnIterator = hashMap.keySet().iterator();
        while(myVeryOwnIterator.hasNext()) {
          String key=(String)myVeryOwnIterator.next();
          String value=(String)hashMap.get(key);
          Log.v("HashMapTest", value);
          mStringArrayList.add(value);
        }*/
        tvCancelFilter.setVisibility(View.GONE);
        mRecyclerViewFilter.setVisibility(View.VISIBLE);
        mRecyclerViewFilter.setNestedScrollingEnabled(false);
        mRecyclerViewFilter.setHasFixedSize(true);
        mDoctorFliterListAdapter = new DoctorFilterListAdapter(mStringArrayList, mAppTypeface, minConsultancy, maxConsultancy);
        mRecyclerViewFilter.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mRecyclerViewFilter.setAdapter(mDoctorFliterListAdapter);
        mDoctorFliterListAdapter.setOnItemClickListener(new DoctorFilterListAdapter.OnItemClickListener() {
          @Override
          public void onItemClick(View view, int position, ArrayList<String> stringArrayList, String minconsultancy, String maxconsultancy) {
            if (stringArrayList.size() > 0) {
              Log.w(TAG, "onItemClick: " + stringArrayList.get(position));
              String price = minconsultancy + " " + Constants.currencySymbol + "-" + maxconsultancy + " " + Constants.currencySymbol;
              Iterator myVeryOwnIterator = hashMap != null ? hashMap.keySet().iterator() : null;
              while (myVeryOwnIterator != null && myVeryOwnIterator.hasNext()) {
                key = (String) myVeryOwnIterator.next();
                value = hashMap.get(key);
              }
              switch (stringArrayList.get(position)) {
                case "Male Doctor":
                case "Médico masculino":
                case "Female Doctor":
                case "Médica":
                case "Others":
                  gender = "0";
                  if (hashMap != null) {
                    hashMap.entrySet()
                        .removeIf(
                            entry -> (stringArrayList.get(position).equals(entry.getValue())));
                  }
                  stringArrayList.remove(position);

                  break;
                case "In Hospital":
                case "No Hospital":
                  inHospital = "0";
                  if (hashMap != null) {
                    hashMap.entrySet()
                        .removeIf(
                            entry -> (stringArrayList.get(position).equals(entry.getValue())));
                  }
                  stringArrayList.remove(position);

                  break;
                case "Available any day":
                case "Disponível qualquer dia":
                case "Available today":
                case "Disponível hoje":
                case "Available in next 3 days":
                case "Disponível nos próximos 3 dias":
                  availability = "0";
                  if (hashMap != null) {
                    hashMap.entrySet()
                        .removeIf(
                            entry -> (stringArrayList.get(position).equals(entry.getValue())));
                  }
                  stringArrayList.remove(position);

                  break;
                case "Consultation Fee":
                case "Preço da consulta":
                  sortBy = "0";
                  if (hashMap != null) {
                    hashMap.entrySet()
                        .removeIf(
                            entry -> (stringArrayList.get(position).equals(entry.getValue())));
                  }
                  stringArrayList.remove(position);
                  break;
                default:
                  if (stringArrayList.get(position).equals(price)) {
                    minConsultancy = "0";
                    maxConsultancy = "0";
                    if (hashMap != null) {
                      hashMap.entrySet()
                          .removeIf(
                              entry -> (stringArrayList.get(position).equals(entry.getValue())));
                      stringArrayList.remove(position);
                      hashMap.remove("6");
                      hashMap.remove("7");
                    }
                    break;
                  }
                  throw new IllegalStateException("Unexpected value: " + stringArrayList.get(position));
              }
              filterData();
            }
          }
        });
      } else if (resultCode == RESULT_CANCELED) {

        Log.d("RESULT_OK", "onActivityResult: " + " RESULT_CANCELED ");

      }
    }
  }

  private void filterData() {

    mDoctorFliterListAdapter.notifyDataSetChanged();
    if (mDoctorFliterListAdapter.getItemCount() == 0) {
      mRecyclerViewFilter.setVisibility(View.GONE);
      tvCancelFilter.setVisibility(View.GONE);
    }
    handler.removeCallbacksAndMessages(null);
    WAITFORRESPONSEFROMAPIToMQTT = 1;
    if(Utility.isNetworkAvailable(mContext)) {
      mDoctorPresenter.onGetDoctorService(mContext, mAlertProgress, mSessionManager, LSPApplication.getInstance().getAuthToken(mSessionManager.getSID()), selLang,
          String.valueOf(latitude), String.valueOf(longitude), Constants.catId,
          callTypeInOutTele, Constants.bookingType, Constants.distance, searchType, metadata,
          Constants.scheduledDate, Constants.scheduledTime, Constants.onRepeatEnd,
          Constants.onRepeatDays,
          Utility.dateInTwentyFour(Constants.serverTime), mSessionManager.getIpAddress(),
          WAITFORRESPONSEFROMAPIToMQTT, gender, availability, minConsultancy, maxConsultancy, inHospital, sortBy, Constants.cityId);
    }
    initializeRxJava();
    if (mqttManager.isMQTTConnected()) {
      mqttManager.subscribeToTopic(MqttEvents.Provider.value + "/" + mSessionManager.getSID(), 1);
    }
  }

  private void filteredAdapterSet(ArrayList<FilterData> filteredResponseArray) {
  }

  @Override
  public void noProviderAvailable(String providerNotAvailable, Boolean isFavDoc) {
    if (isFavDoc) {
      nestedScrollViewProList.setVisibility(View.GONE);
      llNoProviderAvailable.setVisibility(View.VISIBLE);
    } else {
      if (Constants.mqttErrorMsg.isEmpty()) {
        nestedScrollViewProList.setVisibility(View.VISIBLE);
        llNoProviderAvailable.setVisibility(View.GONE);
      } else {
        nestedScrollViewProList.setVisibility(View.GONE);
        llNoProviderAvailable.setVisibility(View.VISIBLE);
      }
    }
  }

  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);

    //Utility.checkAndShowNetworkError(this);
  }

  @Override
  public void onItemClick(String id, String categoryId) {
    if(Utility.isNetworkAvailable(mContext))
    mDoctorPresenter.removeFavDoctor(mContext,id,categoryId,mSessionManager);
  }
}
