package com.vaidg.providerList;

import android.content.Context;
import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;

import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.filter.model.FilterData;
import com.utility.AlertProgress;
import java.util.ArrayList;

import com.vaidg.providerList.model.DoctorData;

/**
 * <h2>DoctorListContract</h2>
 * Created by Ali on 1/29/2018.
 */

public interface DoctorListContract
{
    interface doctorPresenter extends BasePresenter
    {
        void onGetDoctorService(Context context, AlertProgress alertProgress, SessionManagerImpl sessionManager,
                                String auth, String lang, String latitude,
                                String longitude,
                                String catId,
                                int callTypeInOutTele, int bookingType, int distance, String searchType,
                                String metadata, String scheduledDate, int scheduledTime, long onRepeatEnd,
                                ArrayList<String> onRepeatDays, String serverTime, String ipAddress,
                                int waitforresponse, String gender, String availability, String minConsultancy, String maxConsultancy, String inHospital, String sortBy, String cityId);
        void onToGetFavouriteProvider(Context mContext, SessionManagerImpl mSessionManager, String selLang, Boolean isFavDoc);

        void removeFavDoctor(Context mContext, String id, String categoryId, SessionManagerImpl mSessionManager);
    }

    interface doctorView extends BaseView
    {
        void onSuccessDoctorDataList(ArrayList<DoctorData> data);
        void onFilterRemoveArray(ArrayList<FilterData> filteredResponses);

        void setLatLng();

        void onNoConnectionAvailable(String message, boolean isLocation);

        void setOnItemSelected(String subCatId, String subCatName);

        void noProviderAvailable(String providerNotAvailable, Boolean isFavDoc);

    }
}
