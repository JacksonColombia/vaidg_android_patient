package com.vaidg.sidescreens;

import android.util.Log;

import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.wallet.WalletActivityContract;
import com.utility.RefreshToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by ${3Embed} on 4/10/17.
 */

public class SidescreensPresenter implements WalletActivityContract.WalletPresenterBalance {


    private String TAG = SidescreensPresenter.class.getSimpleName();
    @Inject
    LSPServices lspServices;
    @Inject
    SessionManagerImpl manager;
    WalletActivityContract.WalletView view;

    @Inject
    public SidescreensPresenter() {
    }

    @Override
    public void getWalletLimits()
    {

        Observable<Response<ResponseBody>> request = lspServices.getWalletLimits(
                LSPApplication.getInstance().getAuthToken(manager.getSID()), Constants.selLang,Constants.PLATFORM_ANDROID);

        request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        //  compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Response<ResponseBody> value) {
                        Log.d(TAG , " getWalletLimits onNext: " + value.code());
                        String responseString;
                        JSONObject jsonObject;
                        try {
                            switch (value.code()) {
                                case 200:

                                    responseString = value.body().string();
                                    Log.d(TAG , " getWalletLimits onNext: "+responseString);
                                    JSONObject profileObject = new JSONObject(responseString);
                                    JSONObject dataObject = profileObject.getJSONObject("data");
                                    String balance = dataObject.getString("walletAmount");
                                    String softLimit = dataObject.getString("softLimit");
                                    String hardLimit = dataObject.getString("hardLimit");
                                    String currencySymbol=dataObject.optString("currencySymbol");
                                    Constants.walletCurrency = currencySymbol;
                                    Constants.walletAmount = Double.parseDouble(balance);
                                    if(view!=null){
                                        view.setBalanceValues(currencySymbol+" "+balance, currencySymbol+" "+hardLimit
                                                , currencySymbol+" "+softLimit);
                                    }

                                    break;
                                case 410:
                                    break;

                                case Constants.SESSION_LOGOUT:
                                    responseString =value.errorBody().string();
                                    jsonObject = new JSONObject(responseString);
                                    //  view.onHideProgress();
                                    if(view != null) {
                                        view.onLogout(jsonObject.getString("message"));
                                    }
                                    break;
                                case 404:
                                    responseString =value.errorBody().string();
                                    jsonObject = new JSONObject(responseString);
                                    if(view != null) {
                                        view.onError(jsonObject.getString("message"));
                                    }
                                    // providerView.onLogout(jsonObject.getString("message"),manager);
                                    break;
                                case Constants.SESSION_EXPIRED:
                                    responseString = value.errorBody().string();
                                    jsonObject = new JSONObject(responseString);
                                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),manager.getREFRESHAUTH(),lspServices, new RefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {

                                            LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                            getWalletLimits();

                                        }

                                        @Override
                                        public void onFailureRefreshToken() {

                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if(view != null) {
                                                //  providerView.onHideProgress();
                                                view.onLogout(msg);
                                            }
                                        }
                                    });
                                    break;

                                default:
                                    String error = value.errorBody().string();
                                    if(view != null) {
                                        view.walletDetailsApiErrorViewNotifier(new JSONObject(error).getString("message"));
                                    }
                                    break;
                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG , "getWalletLimits error: " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        if(view!=null)
                            view.hideProgressDialog();
                    }
                });
    }

    @Override
    public void attachView(Object view)
    {
        this.view = (WalletActivityContract.WalletView) view;
    }

    @Override
    public void detachView()
    {
        view = null;
    }
}
