package com.vaidg.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class AllCityResponse implements Serializable{

  @SerializedName("message")
  @Expose
  private String message;
  @SerializedName("data")
  @Expose
  private AllCityData data;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public AllCityData getData() {
    return data;
  }

  public void setData(AllCityData data) {
    this.data = data;
  }


  public class AllCityData implements Serializable
  {

    @SerializedName("allCities")
    @Expose
    private ArrayList<CategoryCity> allCities;
    
    public ArrayList<CategoryCity> getAllCities() {
      return allCities;
    }
  }
}