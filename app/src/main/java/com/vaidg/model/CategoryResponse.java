package com.vaidg.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

public class CategoryResponse implements Serializable{

  @SerializedName("message")
  @Expose
  private String message;
  @SerializedName("data")
  @Expose
  private CategoryData data;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public CategoryData getData() {
    return data;
  }

  public void setData(CategoryData data) {
    this.data = data;
  }


  public class CategoryData implements Serializable
  {

    @SerializedName("cityData")
    @Expose
    private CityData cityData;
    @SerializedName("catArr")
    @Expose
    private ArrayList<CatDataArray> catArr;
    @SerializedName("teleCallCategory")
    @Expose
    private ArrayList<Category> teleCallCategory;
    @SerializedName("inCallCategory")
    @Expose
    private ArrayList<Category> inCallCategory;
    @SerializedName("outCallCategory")
    @Expose
    private ArrayList<Category> outCallCategory;
  @SerializedName("allCities")
    @Expose
    private ArrayList<CategoryCity> allCities;

    @SerializedName("homePageTagLine")
    @Expose
    private String homePageTagLine;

    public CityData getCityData() {
      return cityData;
    }

    public ArrayList<CatDataArray> getCatArr() {
      return catArr;
    }

    public ArrayList<Category> getTeleCallCategory() {
      return teleCallCategory;
    }

    public ArrayList<Category> getInCallCategory() {
      return inCallCategory;
    }

    public ArrayList<Category> getOutCallCategory() {
      return outCallCategory;
    }

    public ArrayList<CategoryCity> getAllCities() {
      return allCities;
    }

    public String getHomePageTagLine() {
      return homePageTagLine;
    }
  }
}