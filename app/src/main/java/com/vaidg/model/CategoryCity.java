package com.vaidg.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CategoryCity implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("city")
    @Expose
    private String city;

    public String getId() {
        return id;
    }

    public String getCity() {
        return city;
    }
}
