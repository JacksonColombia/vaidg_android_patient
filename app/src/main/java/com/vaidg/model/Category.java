package com.vaidg.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class Category implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("jobType")
    @Expose
    private int jobType;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("service_type")
    @Expose
    private int serviceType;
    @SerializedName("billing_model")
    @Expose
    private int billingModel;
    @SerializedName("cat_name")
    @Expose
    private String catName;
    @SerializedName("searchSymptomName")
    @Expose
    private String searchSymptomName;
   @SerializedName("searchSymptomId")
    @Expose
    private String searchSymptomId;
    @SerializedName("bannerImageApp")
    @Expose
    private String bannerImageApp;
    @SerializedName("minimumFeesForConsultancy")
    @Expose
    private double minimumFeesForConsultancy;
    @SerializedName("maximumFeesForConsultancy")
    @Expose
    private double maximumFeesForConsultancy;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("visit_fees")
    @Expose
    private double visitFee;
    @SerializedName("bookingTypeAction")
    @Expose
    private BookingTypeAction bookingTypeAction;
    @SerializedName("callType")
    @Expose
    private CallType callType;
    @SerializedName("minimum_fees")
    @Expose
    private double minimumFees;
    @SerializedName("miximum_fees")
    @Expose
    private double maximumFees;
    @SerializedName("price_per_fees")
    @Expose
    private double pricePerHour;
    @SerializedName("iconApp")
    @Expose
    private String iconApp;
    @SerializedName("categoryId")
    @Expose
    private String categoryId;


    public String getId() {
        return id;
    }

    public String getCityId() {
        return cityId;
    }

    public String getCity() {
        return city;
    }

    public int getServiceType() {
        return serviceType;
    }

    public int getBillingModel() {
        return billingModel;
    }

    public String getCatName() {
        return catName;
    }

    public String getSearchSymptomName() {
        return searchSymptomName;
    }

    public String getSearchSymptomId() {
        return searchSymptomId;
    }

    public String getBannerImageApp() {
        return bannerImageApp;
    }

    public double getMinimumFeesForConsultancy() {
        return minimumFeesForConsultancy;
    }

    public double getMaximumFeesForConsultancy() {
        return maximumFeesForConsultancy;
    }

    public int getStatus() {
        return status;
    }
    public double getVisitFee() {
        return visitFee;
    }

    public BookingTypeAction getBookingTypeAction() {
        return bookingTypeAction;
    }

    public CallType getCallType() {
        return callType;
    }

    public double getMinimumFees() {
        return minimumFees;
    }

    public double getMaximumFees() {
        return maximumFees;
    }

    public double getPricePerHour() {
        return pricePerHour;
    }

    public String getIconApp() {
        return iconApp;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public class BookingTypeAction implements Serializable {

        @SerializedName("now")
        @Expose
        private boolean now;

        @SerializedName("schedule")
        @Expose
        private boolean schedule;

        @SerializedName("repeat")
        @Expose
        private boolean repeat;

        public boolean isNow() {
            return now;
        }

        public boolean isSchedule() {
            return schedule;
        }

        public boolean isRepeat() {
            return repeat;
        }
    }

}