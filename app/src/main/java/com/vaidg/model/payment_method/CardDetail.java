package com.vaidg.model.payment_method;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CardDetail implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("customer")
    @Expose
    private String customer;
    @SerializedName("exp_month")
    @Expose
    private Integer expMonth;
    @SerializedName("exp_year")
    @Expose
    private Integer expYear;
    @SerializedName("last4")
    @Expose
    private String last4;
    @SerializedName("brand")
    @Expose
    private String brand;
    @SerializedName("isDefault")
    @Expose
    private boolean isDefault;


    /**
     * No args constructor for use in serialization
     *
     */
    public CardDetail() {
    }

    /**
     *
     * @param expMonth
     * @param last4
     * @param isDefault
     * @param expYear
     * @param id
     * @param brand
     * @param customer
     */
    public CardDetail(String id, String customer, Integer expMonth, Integer expYear, String last4, String brand, boolean isDefault) {
        super();
        this.id = id;
        this.customer = customer;
        this.expMonth = expMonth;
        this.expYear = expYear;
        this.last4 = last4;
        this.brand = brand;
        this.isDefault = isDefault;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public Integer getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(Integer expMonth) {
        this.expMonth = expMonth;
    }

    public Integer getExpYear() {
        return expYear;
    }

    public void setExpYear(Integer expYear) {
        this.expYear = expYear;
    }

    public String getLast4() {
        return last4;
    }

    public void setLast4(String last4) {
        this.last4 = last4;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public boolean isIsDefault() {
        return isDefault;
    }

    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

}

