package com.vaidg.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h2>SignUpData</h2>
 * Created by Pramod on 14/12/17.
 */

public class SignUpData implements Serializable {

    @SerializedName("sid")
    @Expose
    private String sid;

    @SerializedName("expireOtp")
    @Expose
    private int expireOtp;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public void setExpireOtp(int expireOtp) {
        this.expireOtp = expireOtp;
    }

    public long getExpireOtp() {
        return expireOtp;
    }
}
