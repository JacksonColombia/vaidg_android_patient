package com.vaidg.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ali on 11/23/2018.
 */
public class CallType implements Serializable
{
    /*"incall":true,
"outcall":true,
"telecall":false*/

    @SerializedName("incall")
    @Expose
    private boolean incall;

    @SerializedName("outcall")
    @Expose
    private boolean outcall;

    @SerializedName("telecall")
    @Expose
    private boolean telecall;
  // private boolean incall,outcall,telecall;

    public boolean isIncall() {
        return incall;
    }

    public boolean isOutcall() {
        return outcall;
    }

    public boolean isTelecall() {
        return telecall;
    }
}
