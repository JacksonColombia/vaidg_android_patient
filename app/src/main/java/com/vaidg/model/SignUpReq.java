package com.vaidg.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author  Pramod
 * @since  14/12/17.
 */

public class SignUpReq implements Serializable {
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("countrySymbol")
    @Expose
    private String countrySymbol;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("dateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("loginType")
    @Expose
    private Integer loginType;
    @SerializedName("facebookId")
    @Expose
    private String facebookId;
    @SerializedName("googleId")
    @Expose
    private String googleId;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("preferredGenres")
    @Expose
    private String preferredGenres;
    @SerializedName("termsAndCond")
    @Expose
    private Integer termsAndCond;
    @SerializedName("devType")
    @Expose
    private Integer devType;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("pushToken")
    @Expose
    private String pushToken;
    @SerializedName("appVersion")
    @Expose
    private String appVersion;
    @SerializedName("deviceOsVersion")
    @Expose
    private String deviceOsVersion;
    @SerializedName("devMake")
    @Expose
    private String devMake;
    @SerializedName("devModel")
    @Expose
    private String devModel;
    @SerializedName("deviceTime")
    @Expose
    private String deviceTime;
    @SerializedName("referralCode")
    @Expose
    private String referralCode;
    @SerializedName("privateKey")
    @Expose
    private String privateKey ;
    @SerializedName("publickKey")
    @Expose
    private String publickKey;
    @SerializedName("gender")
    @Expose
    private String gender;

    public SignUpReq(String firstName, String lastName, String email, String password,String countryCodeName, String countryCode, String phone, String profilePic, Integer loginType, String facebookId, String googleId, Double latitude, Double longitude, String preferredGenres, Integer termsAndCond, Integer devType, String deviceId, String pushToken, String appVersion, String deviceOsVersion, String devMake, String devModel, String deviceTime, String referralCode, String ipAddress, String privateKey, String publickKey, String gender, String dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.countrySymbol = countryCodeName;
        this.countryCode = countryCode;
        this.phone = phone;
        this.profilePic = profilePic;
        this.loginType = loginType;
        this.facebookId = facebookId;
        this.googleId = googleId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.preferredGenres = preferredGenres;
        this.termsAndCond = termsAndCond;
        this.devType = devType;
        this.deviceId = deviceId;
        this.pushToken = pushToken;
        this.appVersion = appVersion;
        this.deviceOsVersion = deviceOsVersion;
        this.devMake = devMake;
        this.devModel = devModel;
        this.deviceTime = deviceTime;
        this.referralCode = referralCode;
        this.ipAddress = ipAddress;
        this.privateKey = privateKey;
        this.publickKey = publickKey;
        this.gender = gender;
        this.dateOfBirth = dateOfBirth;
    }


    public String getGender() {
        return gender;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPublickKey() {
        return publickKey;
    }

    public void setPublickKey(String publickKey) {
        this.publickKey = publickKey;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getPreferredGenres() {
        return preferredGenres;
    }

    public void setPreferredGenres(String preferredGenres) {
        this.preferredGenres = preferredGenres;
    }

    public String getCountrySymbol() {
        return countrySymbol;
    }

    public void setCountrySymbol(String countrySymbol) {
        this.countrySymbol = countrySymbol;
    }
    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    private String ipAddress;

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getLoginType() {
        return loginType;
    }

    public void setLoginType(Integer loginType) {
        this.loginType = loginType;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Integer getTermsAndCond() {
        return termsAndCond;
    }

    public void setTermsAndCond(Integer termsAndCond) {
        this.termsAndCond = termsAndCond;
    }

    public Integer getDevType() {
        return devType;
    }

    public void setDevType(Integer devType) {
        this.devType = devType;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getDeviceOsVersion() {
        return deviceOsVersion;
    }

    public void setDeviceOsVersion(String deviceOsVersion) {
        this.deviceOsVersion = deviceOsVersion;
    }

    public String getDevMake() {
        return devMake;
    }

    public void setDevMake(String devMake) {
        this.devMake = devMake;
    }

    public String getDevModel() {
        return devModel;
    }

    public void setDevModel(String devModel) {
        this.devModel = devModel;
    }

    public String getDeviceTime() {
        return deviceTime;
    }

    public void setDeviceTime(String deviceTime) {
        this.deviceTime = deviceTime;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
