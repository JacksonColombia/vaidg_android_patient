package com.vaidg.model.card;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DeleteCard implements Serializable
{
    @SerializedName("cardId")
    @Expose
    private String cardId;

    public DeleteCard(String cardId) {
        super();
        this.cardId = cardId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }
}
