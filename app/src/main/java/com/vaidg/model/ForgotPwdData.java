package com.vaidg.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Pramod on 14/12/17.
 */

public class ForgotPwdData implements Serializable {

    @SerializedName("sid")
    @Expose
    private String sid;

    @SerializedName("expireOtp")
    @Expose
    private long expireOtp;

    public String getSid() {
        return sid;
    }

    public long getExpireOtp() {
        return expireOtp;
    }
}
