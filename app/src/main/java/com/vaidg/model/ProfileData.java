package com.vaidg.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Pramod on 14/12/17.
 */

public class ProfileData implements Serializable {

  @SerializedName("firstName")
  @Expose
  private String firstName;
  @SerializedName("lastName")
  @Expose
  private String lastName;
  @SerializedName("dateOfBirth")
  @Expose
  private String dateOfBirth;
   /*
    @SerializedName("preferredGenres")
    @Expose
    private List<Object> preferredGenres = null;*/

  @SerializedName("email")
  @Expose
  private String email;

  @SerializedName("profilePic")
  @Expose
  private String profilePic;

  @SerializedName("about")
  @Expose
  private String about;

  @SerializedName("gender")
  @Expose
  private String gender;
  @SerializedName("countrySymbol")
  @Expose
  private String countrySymbol;

  @SerializedName("countryCode")
  @Expose
  private String countryCode;

  @SerializedName("phone")
  @Expose
  private String phone;

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

   /*

    public List<Object> getPreferredGenres() {
        return preferredGenres;
    }

    public void setPreferredGenres(List<Object> preferredGenres) {
        this.preferredGenres = preferredGenres;
    }
*/

  public String getGender() {
    return gender;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getProfilePic() {
    return profilePic;
  }

  public void setProfilePic(String profilePic) {
    this.profilePic = profilePic;
  }

  public String getAbout() {
    return about;
  }

  public void setAbout(String about) {
    this.about = about;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getCountrySymbol() {
    return countrySymbol;
  }

  public void setCountrySymbol(String countrySymbol) {
    this.countrySymbol = countrySymbol;
  }
}
