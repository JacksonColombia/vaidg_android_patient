package com.vaidg.model;

public class ModelType {

    public static final int COLD_AND_FLUE=0;
    public static final int SKIN_HAIR_ISSUES=1;
    public static final int WOMENS_HEALTH=2;
    public static final int MENS_HEALTH=3;
    public static final int ALLERGIES=4;
    public static final int HEADACHES=5;

    public int type;
    public int data;
    public String text;

    public ModelType(int type, String text, int data)
    {
        this.type=type;
        this.data=data;
        this.text=text;
    }
}
