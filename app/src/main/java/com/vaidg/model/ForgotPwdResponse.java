package com.vaidg.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Pramod on 14/12/17.
 */

public class ForgotPwdResponse implements Serializable {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ForgotPwdData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ForgotPwdData getData() {
        return data;
    }

    public void setData(ForgotPwdData data) {
        this.data = data;
    }
}
