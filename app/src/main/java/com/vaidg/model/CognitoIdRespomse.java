package com.vaidg.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CognitoIdRespomse implements Serializable {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public class Data implements Serializable{
        @SerializedName("IdentityId")
        @Expose
        private String IdentityId;
        @SerializedName("bucket")
        @Expose
        private String bucket;
        @SerializedName("Token")
        @Expose
        private String Token;
        @SerializedName("region")
        @Expose
        private String region;

        public String getRegion() {
            return region;
        }

        public String getToken() {
            return Token;
        }

        public String getIdentityId() {
            return IdentityId;
        }

        public String getBucket() {
            return bucket;
        }
    }
}

