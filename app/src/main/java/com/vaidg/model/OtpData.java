package com.vaidg.model;

import com.vaidg.Login.pojo.Call;
import com.vaidg.Login.pojo.TokenPojo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


/**
 * Created by Pramod on 14/12/17.
 */

public class OtpData implements Serializable {

  @SerializedName("token")
  @Expose
  private TokenPojo token;
  @SerializedName("sid")
  @Expose
  private String sid;
  @SerializedName("email")
  @Expose
  private String email;
  @SerializedName("firstName")
  @Expose
  private String firstName;
  @SerializedName("lastName")
  @Expose
  private String lastName;
  @SerializedName("phone")
  @Expose
  private String phone;
  @SerializedName("countryCode")
  @Expose
  private String countryCode;
  @SerializedName("countrySymbol")
  @Expose
  private String countrySymbol;
  @SerializedName("referralCode")
  @Expose
  private String referralCode;
  @SerializedName("profilePic")
  @Expose
  private String profilePic;
  @SerializedName("fcmTopic")
  @Expose
  private String fcmTopic;
  @SerializedName("currencyCode")
  @Expose
  private String currencyCode;
  @SerializedName("PublishableKey")
  @Expose
  private String publishableKey;
  @SerializedName("privateKey")
  @Expose
  private String privateKey;
  @SerializedName("publickKey")
  @Expose
  private String publickKey;

  @SerializedName("gender")
  @Expose
  private String gender;
  @SerializedName("dateOfBirth")
  @Expose
  private String dateOfBirth;

  @SerializedName("requester_id")
  @Expose
  private String requesterId;

  @SerializedName("call")
  @Expose
  private Call call;


  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPublickKey() {
        return publickKey;
    }

    public void setPublickKey(String publickKey) {
        this.publickKey = publickKey;
    }

    public Call getCall() {
    return call;
  }

  public String getRequester_id() {
    return requesterId;
  }

  public String getGender() {
    return gender;
  }

  public TokenPojo getToken() {
    return token;
  }

  public void setToken(TokenPojo token) {
    this.token = token;
  }

  public String getSid() {
    return sid;
  }

  public void setSid(String sid) {
    this.sid = sid;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getCountrySymbol() {
    return countrySymbol;
  }

  public void setCountrySymbol(String countrySymbol) {
    this.countrySymbol = countrySymbol;
  }

  public String getReferralCode() {
    return referralCode;
  }

/*
    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }
*/

  public String getProfilePic() {
    return profilePic;
  }

  public void setProfilePic(String profilePic) {
    this.profilePic = profilePic;
  }

  public String getFcmTopic() {
    return fcmTopic;
  }


}
