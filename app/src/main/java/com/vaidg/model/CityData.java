package com.vaidg.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <h2>CityData</h2>
 * Created by Ali on 3/15/2018.
 */
import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CityData implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("currencySymbol")
    @Expose
    private String currencySymbol;
    @SerializedName("currencyAbbr")
    @Expose
    private String currencyAbbr;
   @SerializedName("distanceMatrix")
    @Expose
    private int distanceMatrix;
   @SerializedName("paymentMode")
    @Expose
    private PaymentMode paymentMode;
   @SerializedName("paymentGateways")
    @Expose
    private ArrayList<PaymentGateways> paymentGateways;
    @SerializedName("stripeKeys")
    @Expose
    private String stripeKeys;
    @SerializedName("RAZORPAY_ID")
    @Expose
    private String razorPayKey;
    @SerializedName("custGoogleMapKeys")
    @Expose
    private ArrayList<String> custGoogleMapKeys;
    @SerializedName("customerFrequency")
    @Expose
    private CustomerFrequency customerFrequency;
    @SerializedName("pushTopics")
    @Expose
    private PushTopics pushTopics;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("googleMapKey")
    @Expose
    private String googleMapKey;

    public String getId() {
        return id;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public String getCurrencyAbbr() {
        return currencyAbbr;
    }

    public int getDistanceMatrix() {
        return distanceMatrix;
    }

    public PaymentMode getPaymentMode() {
        return paymentMode;
    }

    public ArrayList<PaymentGateways> getPaymentGateways() {
        return paymentGateways;
    }

    public String getStripeKeys() {
        return stripeKeys;
    }

    public String getRazorPayKey() {
        return razorPayKey;
    }

    public ArrayList<String> getCustGoogleMapKeys() {
        return custGoogleMapKeys;
    }

    public CustomerFrequency getCustomerFrequency() {
        return customerFrequency;
    }

    public PushTopics getPushTopics() {
        return pushTopics;
    }

    public String getGoogleMapKey() {
        return googleMapKey;
    }

    public Location getLocation() {
        return location;
    }

    public class CustomerFrequency implements Serializable {

        @SerializedName("customerHomePageInterval")
        @Expose
        private int customerHomePageInterval;

        public int getCustomerHomePageInterval() {
            return customerHomePageInterval;
        }

    }

    public class PaymentMode implements Serializable
    {

        @SerializedName("cash")
        @Expose
        private boolean cash;
        @SerializedName("card")
        @Expose
        private boolean card;
        @SerializedName("wallet")
        @Expose
        private boolean wallet;

        public boolean isCash() {
            return cash;
        }

        public boolean isCard() {
            return card;
        }

        public boolean isWallet() {
            return wallet;
        }

        public void setWallet(boolean wallet) {
            this.wallet = wallet;
        }

    }

    public class PaymentGateways implements Serializable
    {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("fixedCommission")
        @Expose
        private String fixedCommission;
        @SerializedName("percentageCommission")
        @Expose
        private String percentageCommission;
        @SerializedName("id")
        @Expose
        private String id;

        public String getName() {
            return name;
        }
    }

    public class PushTopics implements Serializable
    {

        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("allCustomers")
        @Expose
        private String allCustomers;
        @SerializedName("allCitiesCustomers")
        @Expose
        private String allCitiesCustomers;

        public String getCity() {
            return city;
        }

        public String getAllCustomers() {
            return allCustomers;
        }

        public String getAllCitiesCustomers() {
            return allCitiesCustomers;
        }

    }

}

