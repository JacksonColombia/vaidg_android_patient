package com.vaidg.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ali on 9/26/2018.
 */
public class Offers implements Serializable
{
    /*TITLE:"test",
"discountType":2,
"value":10,
"shiftBooking":3*/
    @SerializedName("discountType")
    @Expose
    private int discountType;
    @SerializedName("value")
    @Expose
    private int value;
    @SerializedName("minShiftBooking")
    @Expose
    private int minShiftBooking;
    @SerializedName("maxShiftBooking")
    @Expose
    private int maxShiftBooking;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("offerDescription")
    @Expose
    private String offerDescription;


    public String getOfferDescription() {
        return offerDescription;
    }

    public int getDiscountType() {
        return discountType;
    }

    public int getValue() {
        return value;
    }

    public int getMinShiftBooking() {
        return minShiftBooking;
    }

    public int getMaxShiftBooking() {
        return maxShiftBooking;
    }

    public String getTitle() {
        return title;
    }
}
