package com.vaidg.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Pramod on 14/12/17.
 */

public class ForgotPwdReq implements Serializable {
    @SerializedName("emailOrPhone")
    @Expose
    private String emailOrPhone;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("userType")
    @Expose
    private Integer userType;
    @SerializedName("type")
    @Expose
    private Integer type;

    public ForgotPwdReq(String emailOrPhone, String countryCode, Integer userType, Integer type) {
        this.emailOrPhone = emailOrPhone;
        this.countryCode = countryCode;
        this.userType = userType;
        this.type = type;
    }

    public String getEmailOrPhone() {
        return emailOrPhone;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public Integer getType() {
        return type;
    }

}