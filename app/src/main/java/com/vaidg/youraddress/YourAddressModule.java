package com.vaidg.youraddress;

import android.app.Activity;

import com.vaidg.Dagger2.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>YourAddressModule</h2>
 * <p>
 * This is a Dagger module. We use this to pass in the View dependency to the
 * {@link YourAddressPresenterImpl}.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
@Module
public abstract class YourAddressModule {

    @ActivityScoped
    @Binds
    abstract Activity activity(YourAddressActivity activity);

    @ActivityScoped
    @Binds
    abstract YourAddressContract.YourAddressPresenter presenter(YourAddressPresenterImpl presenter);

    @ActivityScoped
    @Binds
    abstract YourAddressContract.YourAddressView view(YourAddressActivity activity);
}
