package com.vaidg.youraddress.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.R;
import com.vaidg.add_address.AddAddressActivity;
import com.vaidg.databinding.ItemYourAddressListBinding;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.youraddress.YourAddressActivity;
import com.vaidg.youraddress.model.YourAddrData;

import java.util.List;

import static com.vaidg.utilities.Constants.DATA;
import static com.vaidg.utilities.Constants.DIALOGMESSAGESHOW_YES;
import static com.vaidg.utilities.Constants.YOURADDRESSSCREEN.BUNDLE_EDIT_ADD;
import static com.vaidg.utilities.Constants.YOURADDRESSSCREEN.EDT_ADD_REQ_CODE;
import static com.vaidg.utilities.Utility.isTextEmpty;
import static com.vaidg.utilities.Utility.setTextSize12Sp;
import static com.vaidg.utilities.Utility.setTextSize14Sp;

/**
 * <h2>AddressListAdapter</h2>
 * <p>
 * Adapter for YourAddressListActivity.
 * </p>
 *
 * @author 3Embed.
 * @version 1.0
 * @since 29-11-2019.
 */
public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.ItemViewHolder> {
    public int selectedItem = -1;
    private Context mContext;
    private AppTypeface appTypeface;
    private final boolean isBidding;
    private final OnItemClickListener listener;
    private final List<YourAddrData> yourAddrDataList;
    //to set Address
    String addressLine1 = "";

    public AddressListAdapter(List<YourAddrData> yourAddrDataList, boolean isBidding,
                              OnItemClickListener listener) {
        this.isBidding = isBidding;
        this.yourAddrDataList = yourAddrDataList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemYourAddressListBinding binding = ItemYourAddressListBinding.
                inflate(LayoutInflater.from(parent.getContext()), parent, false);
        mContext = parent.getContext();
        return new ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.bind(yourAddrDataList.get(position));
    }

    @Override
    public int getItemCount() {
        return yourAddrDataList == null ? 0 : yourAddrDataList.size();
    }

    /**
     * <h2>deleteItem</h2>
     * This method is used to delete the address.
     */
    public void deleteItem(int index) {
        yourAddrDataList.remove(index);
        notifyItemRemoved(index);
    }


    public interface OnItemClickListener {
        /**
         * <h2>onItemClick</h2>
         * This method is used to delete the selected address.
         */
        void onItemClick(int position, List<YourAddrData> yourAddrDataList);
    }

    /**
     * <h2>ItemViewHolder</h2>
     * <p>
     * This itemViewHolder class hold the item state for
     *
     * @author 3Embed.
     * @version 1.0
     * @see YourAddressActivity , and handle the view click event.
     * </p>
     * @since 29-11-2019.
     */
    class ItemViewHolder extends RecyclerView.ViewHolder {
        private final ItemYourAddressListBinding binding;

        public ItemViewHolder(ItemYourAddressListBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;
            appTypeface = AppTypeface.getInstance(mContext);
        }

        public void bind(YourAddrData yourAddrData) {
            binding.tvAddType.setTypeface(appTypeface.getHind_regular());
            binding.tvAddLineTwo.setTypeface(appTypeface.getHind_regular());
            binding.tvAddLineOne.setTypeface(appTypeface.getHind_regular());
            setTextSize14Sp(mContext, binding.tvAddType);
            setTextSize12Sp(mContext, binding.tvAddLineOne);
            setTextSize12Sp(mContext, binding.tvAddLineTwo);
            itemView.setOnClickListener(view -> {
                if (isBidding) {
                    notifyItemChanged(selectedItem);
                    selectedItem = getAdapterPosition();
                    notifyItemChanged(selectedItem);
                }
            });
            binding.ivEdtAdd.setOnClickListener(view -> {
                Intent intent = new Intent(mContext, AddAddressActivity.class);
                if (yourAddrData != null) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(DATA, yourAddrData);
                    bundle.putString(BUNDLE_EDIT_ADD, DIALOGMESSAGESHOW_YES);
                    intent.putExtras(bundle);
                    Activity activity = (Activity) mContext;
                    activity.startActivityForResult(intent, EDT_ADD_REQ_CODE);
                }
            });
            binding.ivDelAdd.setOnClickListener(view -> {
                if (yourAddrData != null) {
                    if (listener != null) listener.onItemClick(getAdapterPosition(),
                            yourAddrDataList);
                }
            });
            if (isBidding) {
                if (getAdapterPosition() == selectedItem) {
                    binding.clContainer.setBackground(ContextCompat.getDrawable(mContext,
                            R.drawable.dependents_selection));
                    binding.tvAddType.setBackground(ContextCompat.getDrawable(mContext,
                            R.drawable.rounded_selection));
                } else {
                    binding.clContainer.setBackground(ContextCompat.getDrawable(mContext,
                            R.drawable.dependents_unselection));
                    binding.tvAddType.setBackground(ContextCompat.getDrawable(mContext,
                            R.drawable.rounded_unselection));
                }
                if (mContext.getResources().getString(R.string.home).equals(yourAddrData.getTaggedAs()))
                    binding.tvAddType.setText(mContext.getResources().getString(R.string.home));
                else if (mContext.getResources().getString(R.string.office).equals(yourAddrData.getTaggedAs()))
                    binding.tvAddType.setText(mContext.getResources().getString(R.string.work));
                else binding.tvAddType.setText(yourAddrData.getTaggedAs());
            } else {
                binding.ivAddType.setVisibility(View.VISIBLE);
                binding.tvAddType.setBackground(ContextCompat.getDrawable(mContext,
                        R.drawable.rounded_unselection));
                if (mContext.getResources().getString(R.string.home).equals(yourAddrData.getTaggedAs())) {
                    binding.ivAddType.setImageResource(R.drawable.ic_home);
                    binding.tvAddType.setText(mContext.getResources().getString(R.string.home));
                } else if (mContext.getResources().getString(R.string.office).equals(yourAddrData.getTaggedAs())) {
                    binding.ivAddType.setImageResource(R.drawable.ic_work);
                    binding.tvAddType.setText(mContext.getResources().getString(R.string.work));
                } else {
                    binding.ivAddType.setImageResource(R.drawable.ic_other_addr);
                    binding.tvAddType.setText(yourAddrData.getTaggedAs());
                }
            }
            if (isTextEmpty(yourAddrData.getHouseNo()))
                addressLine1 = yourAddrData.getAddLine1();
            else addressLine1 =
                    yourAddrData.getHouseNo() + ", " + yourAddrData.getAddLine1();

            binding.tvAddLineOne.setText(addressLine1);
            if (isTextEmpty(yourAddrData.getAddLine2()))
                binding.tvAddLineTwo.setVisibility(View.GONE);
            else binding.tvAddLineTwo.setText(yourAddrData.getAddLine2());
        }
    }
}
