package com.vaidg.youraddress.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h2>YourAddressResponse</h2>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class YourAddressResponse implements Serializable {

  /*  {     "message":"Got The Details.",
            "data":[{}]
      }  */

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<YourAddrData> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<YourAddrData> getData() {
        return data;
    }

    public void setData(ArrayList<YourAddrData> data) {
        this.data = data;
    }

}