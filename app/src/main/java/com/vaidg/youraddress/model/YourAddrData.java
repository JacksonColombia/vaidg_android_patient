package com.vaidg.youraddress.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * <h2>YourAddrData</h2>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class YourAddrData implements Serializable {

  /* {
           "_id":"5fc28b5cb29035aa695b308a",
          "taggedAs":"Home",
          "user_Id":"5e9ed6f050b8dbd3888f9271",
          "addLine1":"Unnamed Road, Birubari, Guwahati, Assam 781032, India",
          "addLine2":"",
          "city":"Guwahati",
          "country":"India",
          "houseNo":"",
          "latitude":26.1532777,
          "longitude":91.7489217,
          "name":"",
          "neighbourHood":"",
          "pincode":"781032",
          "placeId":"",
          "reference":"",
          "state":"Assam",
          "userType":1
  } */
  @SerializedName("_id")
  @Expose
  private String id;
  @SerializedName("taggedAs")
  @Expose
  private String taggedAs;
  @SerializedName("user_Id")
  @Expose
  private String userId;
  @SerializedName("addLine1")
  @Expose
  private String addLine1;
  @SerializedName("addLine2")
  @Expose
  private String addLine2;
  @SerializedName("city")
  @Expose
  private String city;
  @SerializedName("state")
  @Expose
  private String state;
  @SerializedName("country")
  @Expose
  private String country;
  @SerializedName("placeId")
  @Expose
  private String placeId;
  @SerializedName("pincode")
  @Expose
  private String pincode;
  @SerializedName("latitude")
  @Expose
  private Double latitude;
  @SerializedName("longitude")
  @Expose
  private Double longitude;
  @SerializedName("userType")
  @Expose
  private long userType;
  @SerializedName("defaultAddress")
  @Expose
  private long defaultAddress;
  @SerializedName("houseNo")
  @Expose
  private String houseNo;
  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("neighbourHood")
  @Expose
  private String neighbourHood;
  @SerializedName("reference")
  @Expose
  private String reference;

  public String getId() {
    return id;
  }

  public String getTaggedAs() {
    return taggedAs;
  }

  public String getUserId() {
    return userId;
  }

  public String getAddLine1() {
    return addLine1;
  }

  public String getAddLine2() {
    return addLine2;
  }

  public String getCity() {
    return city;
  }

  public String getState() {
    return state;
  }

  public String getCountry() {
    return country;
  }

  public String getPlaceId() {
    return placeId;
  }

  public String getPincode() {
    return pincode;
  }

  public Double getLatitude() {
    return latitude;
  }

  public Double getLongitude() {
    return longitude;
  }

  public long getUserType() {
    return userType;
  }

  public long getDefaultAddress() {
    return defaultAddress;
  }

  public String getHouseNo() {
    return houseNo;
  }

  public String getName() {
    return name;
  }

  public String getNeighbourHood() {
    return neighbourHood;
  }

  public String getReference() {
    return reference;
  }
}