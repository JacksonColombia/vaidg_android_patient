package com.vaidg.youraddress;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.utility.AlertProgress;
import com.vaidg.R;
import com.vaidg.add_address.AddAddressActivity;
import com.vaidg.databinding.ActivityYouraddressBinding;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.RecyclerTouchListener;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.youraddress.adapter.AddressListAdapter;
import com.vaidg.youraddress.model.YourAddrData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.vaidg.utilities.Constants.YOURADDRESSSCREEN.EXTRA_ADDRESSLINEONE;
import static com.vaidg.utilities.Constants.YOURADDRESSSCREEN.EXTRA_ADDRESSLINETWO;
import static com.vaidg.utilities.Constants.YOURADDRESSSCREEN.EXTRA_IS_NOT_FROM_ADDRESS;
import static com.vaidg.utilities.Constants.YOURADDRESSSCREEN.EXTRA_LAT;
import static com.vaidg.utilities.Constants.YOURADDRESSSCREEN.EXTRA_LNG;
import static com.vaidg.utilities.Constants.YOURADDRESSSCREEN.EXTRA_TAGAS;
import static com.vaidg.utilities.Utility.isNetworkAvailable;
import static com.vaidg.utilities.Utility.setMAnagerWithBID;
import static com.vaidg.utilities.Utility.setTextSize16Sp;
import static com.vaidg.utilities.Utility.setTextSize18Sp;

/**
 * <h2>YourAddressActivity</h2>
 * <p>
 * Address page where it will show the list of saved address.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class YourAddressActivity extends DaggerAppCompatActivity implements
        YourAddressContract.YourAddressView, View.OnClickListener,
        AddressListAdapter.OnItemClickListener, RecyclerTouchListener.ClickListener {

    public final String TAG = YourAddressActivity.class.getSimpleName();
    @Inject
    AppTypeface appTypeface;
    @Inject
    YourAddressContract.YourAddressPresenter presenter;
    private ProgressDialog pDialog;
    @Inject
    AlertProgress alertProgress;
    private AddressListAdapter mAddressListAdapter;
    private boolean isNotFromAddress = true;
    private ArrayList<YourAddrData> mYourAddrDataArrayList;
    private ActivityYouraddressBinding binding;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityYouraddressBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        mContext = this;
        if (getIntent().getExtras() != null)
            isNotFromAddress = getIntent().getBooleanExtra(EXTRA_IS_NOT_FROM_ADDRESS, true);
        initialize();
    }

    /**
     * <h2>initialize</h2>
     * <p> method to initialize the views</p>
     */
    private void initialize() {
        mYourAddrDataArrayList = new ArrayList<>();
        setSupportActionBar(binding.toolbarLayout.toolbar);
        binding.toolbarLayout.toolbar.setElevation(0f);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0f);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.toolbarLayout.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
        binding.toolbarLayout.tvCenter.setTypeface(appTypeface.getHind_semiBold());
        setTextSize16Sp(mContext, binding.toolbarLayout.tvCenter);
        binding.toolbarLayout.tvCenter.setText(mContext.getResources()
                .getString(R.string.your_addresses));
        binding.tvSavedAdd.setTypeface(appTypeface.getHind_medium());
        binding.btnAddAddress.setTypeface(appTypeface.getHind_semiBold());
        setTextSize16Sp(mContext, binding.tvSavedAdd);
        setTextSize16Sp(mContext, binding.tvEmptyAdd);
        setTextSize18Sp(mContext, binding.btnAddAddress);
        binding.btnAddAddress.setOnClickListener(this);
        mAddressListAdapter = new AddressListAdapter(mYourAddrDataArrayList,
                false, this);
        binding.rvAddress.setHasFixedSize(true);
        binding.rvAddress.setLayoutManager(new LinearLayoutManager(this));
        binding.rvAddress.setAdapter(mAddressListAdapter);
        binding.rvAddress.addOnItemTouchListener(new RecyclerTouchListener(mContext,
                binding.rvAddress, this));
    }


    @Override
    public void onAddressSelected(int position) {
        if (!isNotFromAddress) {
            Intent intent = new Intent();
            intent.putExtra(EXTRA_ADDRESSLINEONE, mYourAddrDataArrayList.get(position).getAddLine1());
            intent.putExtra(EXTRA_LAT, mYourAddrDataArrayList.get(position).getLatitude());
            intent.putExtra(EXTRA_LNG, mYourAddrDataArrayList.get(position).getLongitude());
            intent.putExtra(EXTRA_ADDRESSLINETWO, mYourAddrDataArrayList.get(position).getAddLine2());
            intent.putExtra(EXTRA_TAGAS, mYourAddrDataArrayList.get(position).getTaggedAs());
            setResult(RESULT_OK, intent);
            closeActivity();
        }
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    /**
     * <h2>closeActivity</h2>
     * <p> method for closing current page</p>
     */
    private void closeActivity() {
        finish();
        overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
    }

    /**
     * <h2>addAddress</h2>
     * <p> method for redirecting to add address page</p>
     */
    private void addAddress() {
        Intent intent = new Intent(mContext, AddAddressActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
    }

    @Override
    public void addItems(ArrayList<YourAddrData> list) {
        if (list != null && list.size() > 0) {
            mYourAddrDataArrayList.clear();
            mYourAddrDataArrayList.addAll(list);
            binding.tvSavedAdd.setVisibility(VISIBLE);
            binding.rvAddress.setVisibility(VISIBLE);
            binding.ivEmptyAdd.setVisibility(GONE);
            binding.tvEmptyAdd.setVisibility(GONE);
        } else {
            binding.tvSavedAdd.setVisibility(GONE);
            binding.rvAddress.setVisibility(GONE);
            binding.ivEmptyAdd.setVisibility(VISIBLE);
            binding.tvEmptyAdd.setVisibility(VISIBLE);
        }
        mAddressListAdapter.notifyDataSetChanged();
    }

    @Override
    public void refreshItems(YourAddrData rowItem, int adapterPosition) {
        if (mAddressListAdapter != null) {
            mAddressListAdapter.deleteItem(adapterPosition);
            if (mAddressListAdapter.getItemCount() == 0) setNoAddressAvailable();
        }
    }

    @Override
    public void onShowProgress() {
        pDialog = alertProgress.getProcessDialog(this);
        pDialog.setMessage(mContext.getResources().getString(R.string.waitFetchAddr));
        pDialog.setCancelable(false);
        try {
            if (pDialog != null && !pDialog.isShowing() && !isFinishing()) pDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onHideProgress() {
        if (pDialog != null && !isFinishing() && pDialog.isShowing()) pDialog.dismiss();
    }

    @Override
    public void setNoAddressAvailable() {
        binding.tvSavedAdd.setVisibility(GONE);
        binding.rvAddress.setVisibility(GONE);
        binding.ivEmptyAdd.setVisibility(VISIBLE);
        binding.tvEmptyAdd.setVisibility(VISIBLE);
    }

    @Override
    public void onError(String msg) {
        alertProgress.alertinfo(mContext, msg);
    }

    @Override
    public void onConnectionError(String connectionError) {

    }

    @Override
    public void onSessionExpired() {

    }

    @Override
    public void onLogout(String msg, SessionManagerImpl sessionManager) {
        if (alertProgress != null) alertProgress.alertPositiveOnclick(this, msg,
                mContext.getResources().getString(R.string.logout), mContext.getResources().getString(R.string.ok),
                isClicked -> setMAnagerWithBID(mContext, sessionManager));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isNetworkAvailable(mContext)) presenter.getAddress();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btnAddAddress) addAddress();
    }

    @Override
    public void onClick(View view, int position) {
        presenter.onItemClicked(position);
    }

    @Override
    public void onLongClick(View view, int position) {

    }

    @Override
    public void onItemClick(int position, List<YourAddrData> yourAddrDataList) {
        if(alertProgress != null) alertProgress.alertPositiveNegativeOnclick(mContext,
                    mContext.getResources().getString(R.string.areYouSureYouWantOTDelete),
                    mContext.getResources().getString(R.string.system_error),
                    mContext.getResources().getString(R.string.ok),
                    mContext.getResources().getString(R.string.cancel), false, isClicked -> {
                        if (isClicked) {
                            if (isNetworkAvailable(mContext)) presenter.deleteAddress(position,
                                    yourAddrDataList);
                        }
                    });
    }
}

