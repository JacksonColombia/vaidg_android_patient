package com.vaidg.youraddress;

import com.google.gson.Gson;
import com.utility.AlertProgress;
import com.utility.RefreshToken;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.youraddress.model.YourAddrData;
import com.vaidg.youraddress.model.YourAddressResponse;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.SESSION_EXPIRED;
import static com.vaidg.utilities.Constants.SESSION_LOGOUT;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;
import static com.vaidg.utilities.Constants.selLang;
import static com.vaidg.utilities.LSPApplication.getInstance;

/**
 * <h2>YourAddressPresenterImpl</h2>
 * <p>
 * This class is used to call the API.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class YourAddressPresenterImpl implements YourAddressContract.YourAddressPresenter {
    public final String TAG = YourAddressPresenterImpl.class.getSimpleName();
    @Inject
    LSPServices lspServices;
    @Inject
    AlertProgress alertProgress;
    @Inject
    Gson gson;
    @Inject
    SessionManagerImpl sessionManager;
    @Inject
    YourAddressContract.YourAddressView view;

    @Inject
    YourAddressPresenterImpl() {
    }

    @Override
    public void getAddress() {
        onShowProgress();
        Observable<Response<ResponseBody>> response =
                lspServices.getAddress(getInstance().getAuthToken(sessionManager.getSID()),
                        selLang, PLATFORM_ANDROID);
        response.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NotNull Response<ResponseBody> responseBodyResponse) {
                        int code = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null ?
                                    responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (code) {
                                case SUCCESS_RESPONSE:
                                    if (response != null && !response.isEmpty()) {
                                        YourAddressResponse yourAddrResponse =
                                                gson.fromJson(response,
                                                        YourAddressResponse.class);
                                        if (yourAddrResponse.getData() != null
                                                && yourAddrResponse.getData().size() > 0) {
                                            if (view != null)
                                                view.addItems(yourAddrResponse.getData());
                                        } else {
                                            if (view != null) view.setNoAddressAvailable();
                                        }
                                    }
                                    onHideProgress();
                                    break;
                                case SESSION_LOGOUT:
                                    onLogout(errorBody);
                                    break;
                                case SESSION_EXPIRED:
                                    //ErrorHandel errorHandel = gson.fromJson(response, ErrorHandel.class);
                                    RefreshToken.onRefreshToken(getInstance()
                                                    .getAuthToken(sessionManager.getSID()),
                                            sessionManager.getREFRESHAUTH(),
                                            lspServices,
                                            new RefreshToken.RefreshTokenImple() {
                                                @Override
                                                public void onSuccessRefreshToken(String newToken) {
                                                    onHideProgress();
                                                    getInstance()
                                                            .setAuthToken(sessionManager.getSID(),
                                                                    sessionManager.getSID(),
                                                                    newToken);
                                                    getAddress();
                                                }

                                                @Override
                                                public void onFailureRefreshToken() {

                                                }

                                                @Override
                                                public void sessionExpired(String msg) {
                                                    onLogout(msg);
                                                }
                                            });
                                    break;
                                default:
                                    onErrorMsg(errorBody);
                                    break;
                            }
                        } catch (IOException | NumberFormatException e) {
                            e.printStackTrace();
                            onHideProgress();
                        }
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        e.printStackTrace();
                        onHideProgress();
                    }

                    @Override
                    public void onComplete() {
                        onHideProgress();
                    }
                });

    }

    @Override
    public void deleteAddress(int position, List<YourAddrData> yourAddrDataList) {
        onShowProgress();
        Observable<Response<ResponseBody>> response = lspServices.deleteAddress(getInstance()
                        .getAuthToken(sessionManager.getSID()),
                selLang, PLATFORM_ANDROID, yourAddrDataList.get(position).getId());
        response.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {


                    @Override
                    public void onSubscribe(@NotNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NotNull Response<ResponseBody> responseBodyResponse) {
                        int code = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null ?
                                    responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (code) {
                                case SUCCESS_RESPONSE:
                                    if (response != null && !response.isEmpty()) {
                                        if (view != null) view.refreshItems(yourAddrDataList
                                                .get(position), position);
                                    }
                                    onHideProgress();
                                    break;
                                case SESSION_LOGOUT:
                                    onLogout(errorBody);
                                    break;
                                case SESSION_EXPIRED:
                                    // ErrorHandel errorHandel = gson.fromJson(response,
                                    // ErrorHandel.class);
                                    RefreshToken.onRefreshToken(getInstance()
                                                    .getAuthToken(sessionManager.getSID()),
                                            sessionManager.getREFRESHAUTH(),
                                            lspServices,
                                            new RefreshToken.RefreshTokenImple() {
                                                @Override
                                                public void onSuccessRefreshToken(String newToken) {
                                                    onHideProgress();
                                                    getInstance().setAuthToken(sessionManager
                                                                    .getSID(),
                                                            sessionManager.getSID(), newToken);
                                                    deleteAddress(position, yourAddrDataList);
                                                }

                                                @Override
                                                public void onFailureRefreshToken() {
                                                    onHideProgress();
                                                }

                                                @Override
                                                public void sessionExpired(String msg) {
                                                    onLogout(msg);
                                                }
                                            });
                                    break;
                                default:
                                    onErrorMsg(errorBody);
                                    break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            onHideProgress();
                        }
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        e.printStackTrace();
                        onHideProgress();
                    }

                    @Override
                    public void onComplete() {
                        onHideProgress();
                    }
                });
    }

    @Override
    public void onItemClicked(int adapterPosition) {
        view.onAddressSelected(adapterPosition);
    }

    @Override
    public void attachView(Object view) {
    }

    @Override
    public void detachView() {
    }

    @Override
    public void onHideProgress() {
        if (view != null) view.onHideProgress();
    }

    @Override
    public void onShowProgress() {
        if (view != null) view.onShowProgress();
    }

    @Override
    public void onLogout(String errorBody) {
        try {
            if (errorBody != null && !errorBody.isEmpty()) {
                JSONObject jsonObject = new JSONObject(errorBody);
                if (!jsonObject.getString(MESSAGE).isEmpty()) {
                    if (view != null) view.onLogout(jsonObject.getString(MESSAGE), sessionManager);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        onHideProgress();
    }

    @Override
    public void onErrorMsg(String errorBody) {
        try {
            if (errorBody != null && !errorBody.isEmpty()) {
                JSONObject jsonObject = new JSONObject(errorBody);
                if (!jsonObject.getString(MESSAGE).isEmpty()) {
                    if (view != null) view.onError(jsonObject.getString(MESSAGE));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        onHideProgress();
    }
}
