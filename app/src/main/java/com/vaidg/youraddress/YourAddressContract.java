package com.vaidg.youraddress;

import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;
import com.vaidg.youraddress.model.YourAddrData;

import java.util.ArrayList;
import java.util.List;

/**
 * <h2>YourAddressContract</h2>
 * <p>
 * This class is used to act as a link between view and presenter.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public interface YourAddressContract
{
    interface YourAddressPresenter extends  BasePresenter{
        /**
         * <h2>getAddress</h2>
         * <p>This method is used to get the address</p>
         */
        void getAddress();

        /**
         * <h2>deleteAddress</h2>
         * <p>This method is used to delete the address which you want to delete</p>
         */
        void deleteAddress(int position, List<YourAddrData> yourAddrDataList);

        /**
         * <h2>onItemClicked</h2>
         * <p>This method is used get the selected position of the address</p>
         */
        void onItemClicked(int adapterPosition);

        /**
         * <h2>onHideProgress</h2>
         * <p>This method is used to hide the loading view</p>
         */
        void onHideProgress();

        /**
         * <h2>onShowProgress</h2>
         * <p>This method is used to show the loading view</p>
         */
        void onShowProgress();

        /**
         * <h2>onLogout</h2>
         * <p>This method is used to show msg if session is expired</p>
         */
        void onLogout(String errorBody);

        /**
         * <h2>onErrorMsg</h2>
         * <p>This method is used to show error msg from api calls</p>
         */
        void onErrorMsg(String errorBody);
    }

    interface YourAddressView extends BaseView{

        /**
         * <h2>setNoAddressAvailable</h2>
         * <p>This method is used to when there is no address available</p>
         *
         */
        void setNoAddressAvailable();

        /**
         * <h2>addItems</h2>
         * <p>This method is used to Add the address items to the adapter</p>
         *
         * @param list List of items to add
         */
        void addItems(ArrayList<YourAddrData> list);

        /**
         * <h2>refereshtems</h2>
         * <p>This method is used to refresh the address items from the adapter</p>
         * @param data Address Item for deleting from the adapter
         * @param adapterPosition
         */
        void refreshItems(YourAddrData data, int adapterPosition);

        /**
         * <h2>onAddressSelected</h2>
         * <p>This method is used to select the address</p>
         *
         * @param adapterPosition
         */
        void onAddressSelected(int adapterPosition);
    }

}
