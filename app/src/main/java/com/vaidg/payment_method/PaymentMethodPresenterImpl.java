package com.vaidg.payment_method;

import android.content.Context;
import android.util.Log;

import com.vaidg.model.payment_method.CardDetail;
import com.vaidg.model.payment_method.CardGetResponse;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.utility.RefreshToken;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import org.json.JSONObject;

import java.util.ArrayList;
import javax.inject.Inject;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * @author Pramod
 * @since 31-01-2018.
 */
public class PaymentMethodPresenterImpl implements PaymentMethodPresenter{

    @Inject
    PaymentMethodView view;

    @Inject
    Context context;

    @Inject
    LSPServices lspServices;


    @Inject
    SessionManagerImpl sessionManager;

    @Inject
    PaymentMethodPresenterImpl() {
    }

    @Override
    public void getCard(String auth) {
        if (view!=null) {
            view.onShowProgress();
        }
        Observable<Response<CardGetResponse>> bad = lspServices.getCard(auth, Constants.selLang,Constants.PLATFORM_ANDROID);
        bad.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<CardGetResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                    @Override
                    public void onNext(Response<CardGetResponse> value)
                    {
                        try {
                            if (200 == value.code()) {
                                CardGetResponse response = value.body();

                                if (response != null) {
                                    ArrayList<CardDetail> cardsList = response.getData();
                                    if (cardsList.size() > 0) {
                                        Log.d("TAG", "onNextCARDBODY: " + cardsList.get(0).getBrand()
                                            + " ");
                                        sessionManager.setDefaultCardId(cardsList.get(0).getId());
                                        sessionManager.setDefaultCardNum(cardsList.get(0).getLast4());
                                        sessionManager.setDefaultCardName(cardsList.get(0).getBrand());
                                        if (view != null) {
                                            view.addItems(cardsList);
                                        }
                                    }else {
                                        if (view != null) {
                                            view.cardNotFound();
                                        }
                                    }
                                    if (view != null) {
                                        view.onHideProgress();
                                    }
                                } else {
                                    if (view != null) {
                                        view.onHideProgress();
                                    }
                                }
                            } else if (498 == value.code()) {
                                JSONObject errJson = new JSONObject(value.errorBody().string());
                                if (view != null) {
                                    view.onLogout(errJson.getString("message"),sessionManager);
                                    view.onHideProgress();
                                }
                            } else if(440 == value.code())
                            {
                                JSONObject errJsonD = new JSONObject(value.errorBody().string());
                                RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),sessionManager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                                    @Override
                                    public void onSuccessRefreshToken(String newToken) {
                                        if (view != null) {
                                            view.onHideProgress();
                                        }
                                        LSPApplication.getInstance().setAuthToken(sessionManager.getSID(),sessionManager.getSID(),newToken);
                                        getCard(newToken);
                                    }

                                    @Override
                                    public void onFailureRefreshToken() {

                                    }

                                    @Override
                                    public void sessionExpired(String msg) {
                                        if (view != null) {
                                            view.onLogout(msg,sessionManager);
                                            view.onHideProgress();
                                        }
                                    }
                                });
                            }
                            else{

                                if (value.errorBody() != null) {
                                    JSONObject errJson = new JSONObject(value.errorBody().string());
                                    if (view != null) {
                                        view.setErrorMsg(errJson.getString("message"));
                                        view.onHideProgress();
                                    }
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            if (view != null) {
                                view.onHideProgress();
                            }
                        }
                    }
                    @Override
                    public void onError(Throwable e)
                    {
                        Log.e("Error","error"+e.getMessage());
                        e.printStackTrace();
                        if (view != null) {
                            view.setErrorMsg(e.getMessage());
                            view.onHideProgress();
                        }
                    }
                    @Override
                    public void onComplete() {
                        if (view != null) {
                            view.onHideProgress();
                        }
                    }
                });


    }

   /* @Override
    public void editCard(String auth) {

    }*/
}
