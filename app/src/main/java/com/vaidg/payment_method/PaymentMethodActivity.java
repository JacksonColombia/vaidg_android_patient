package com.vaidg.payment_method;

import static com.appscrip.stripe.Constants.LANGUAGE;
import static com.appscrip.stripe.Constants.USER_ID;
import static com.vaidg.utilities.Constants.AUTHORIZATION;
import static com.vaidg.utilities.Constants.selLang;

import adapters.CardsListAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.appscrip.stripe.AccountsDelegate;
import com.appscrip.stripe.StripePaymentIntentActivity;
import com.appscrip.stripe.UserAccounts;
import com.vaidg.R;
import com.vaidg.model.card.DeleteCard;
import com.vaidg.model.payment_method.CardDetail;
import com.vaidg.model.payment_method.CardGetResponse;
import com.vaidg.payment_details.PaymentDetailActivity;
import com.vaidg.payment_edit_card.CardEditActivity;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManager;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.google.gson.Gson;
import com.utility.AlertProgress;
import com.utility.DialogInterfaceListner;
import dagger.android.support.DaggerAppCompatActivity;
import java.util.ArrayList;
import javax.inject.Inject;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

/**
 * @author Pramod
 * @since 16-01-2018.
 */

public class PaymentMethodActivity extends DaggerAppCompatActivity implements PaymentMethodView {

  @Inject
  SessionManager sessionManager;

  @Inject
  PaymentMethodPresenter paymentMethodPresenter;

  @Inject
  AppTypeface appTypeface;

  @BindView(R.id.rvPaymentList)
  ListView rvPaymentList;

  @BindView(R.id.rlMainCardList)
  RelativeLayout rlMainCardList;

  @BindView(R.id.rlListCardEmpty)
  RelativeLayout rlListCardEmpty;
  @BindView(R.id.tvCardNew)
  TextView tvCardNew;

  @BindView(R.id.tvAddCard)
  Button tvAddCard;
 @BindView(R.id.progress)
 ProgressBar progress;

  @BindString(R.string.wait_card)
  String wait_card;

  CardsListAdapter cardsListAdapter;

  AlertDialog alertDialog;
  AlertDialog.Builder dialogBuilder;
  String auth;
  private boolean isNotFromPayment = true;
    @Inject
    AlertProgress alertProgress;
    private ProgressDialog pDialog;
    private Context mContext;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_payment);
    ButterKnife.bind(this);
      mContext = this;
    if (getIntent().getExtras() != null) {
      isNotFromPayment = getIntent().getBooleanExtra("isNotFormPayment", true);
    }
    initialize();
  }

  private void initialize() {
    //Setting toolbar
    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    if(getSupportActionBar() != null) {
      getSupportActionBar().setDisplayShowHomeEnabled(true);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
    toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
    toolbar.setNavigationOnClickListener(view -> onBackPressed());

    TextView tvTbTitle = toolbar.findViewById(R.id.tv_center);
    tvTbTitle.setText(R.string.cards);
    tvTbTitle.setTypeface(appTypeface.getHind_semiBold());
    tvCardNew.setTypeface(appTypeface.getHind_medium());

    auth = LSPApplication.getInstance().getAuthToken(sessionManager.getSID());
    //   paymentMethodPresenter.getCard(auth);

    rvPaymentList.setOnItemClickListener((adapterView, view, position, id) -> {
      if (isNotFromPayment) {
      /*  CardDetail data = (CardDetail) adapterView.getItemAtPosition(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", data);
        Intent intent = new Intent(PaymentMethodActivity.this, CardEditActivity.class);
        intent.putExtras(bundle);
        //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);*/
      } else {
        CardDetail data = (CardDetail) adapterView.getItemAtPosition(position);
        Intent intent = new Intent();
        intent.putExtra("CARDID", data.getId());
        intent.putExtra("CARDTYPE", data.getBrand());
        intent.putExtra("LAST4", data.getLast4());
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
      }

      //paymentMethodPresenter.editCard();
    });

  }

  @OnClick({R.id.tvAddCard, R.id.tvCardNew})
  void setTvAddCard() {
    Intent intent = new Intent(PaymentMethodActivity.this, StripePaymentIntentActivity.class);
    intent.putExtra(USER_ID, sessionManager.getSID());
      intent.putExtra(AUTHORIZATION, LSPApplication.getInstance().getAuthToken(sessionManager.getSID()));
      intent.putExtra(LANGUAGE, selLang);
    //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    startActivity(intent);
   /* Intent intent = new Intent(PaymentMethodActivity.this, PaymentDetailActivity.class);
    startActivity(intent);*/
  }

  @Override
  public void addItems(ArrayList<CardDetail> list) {
    rlMainCardList.setVisibility(View.VISIBLE);
    rlListCardEmpty.setVisibility(View.GONE);
    cardsListAdapter = new CardsListAdapter(list);
    rvPaymentList.setAdapter(cardsListAdapter);
  }

  @Override
  public void setErrorMsg(String errorMsg) {
    Toast.makeText(this, "" + errorMsg, Toast.LENGTH_LONG).show();
  }

  @Override
  public void cardNotFound() {

    rlMainCardList.setVisibility(View.GONE);
    rlListCardEmpty.setVisibility(View.VISIBLE);
    sessionManager.setDefaultCardId("");
    sessionManager.setDefaultCardNum("");
    sessionManager.setDefaultCardName("");

  }

    @Override
    public void onShowProgress() {
        pDialog = alertProgress.getProcessDialog(this);
      runOnUiThread(new Runnable() {
          @Override
          public void run() {
              //    pDialog.setMessage(mContext.getResources().getString(R.string.wait_profile));
              pDialog.setCancelable(false);
              try {
                  if (pDialog != null && !pDialog.isShowing() && !isFinishing()) {
                      pDialog.show();
                  }
              } catch (Exception e) {
                  e.printStackTrace();
              }
          }
      });
    }

    @Override
    public void onHideProgress() {
      runOnUiThread(new Runnable() {
          @Override
          public void run() {
              if (pDialog != null && !isFinishing() && pDialog.isShowing()) {
                  pDialog.dismiss();
              }
          }
      });
    }

  @Override
  protected void onResume() {
    super.onResume();

    if (Utility.isNetworkAvailable(this)) {
      //  onShowProgress();
      // paymentMethodPresenter.getCard(auth);
        onShowProgress();
        UserAccounts.INSTANCE.getCards(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),selLang, sessionManager.getSID(), new AccountsDelegate() {
        @Override
        public void onSuccess(@NotNull Object successData) {

          runOnUiThread(new Runnable() {
            @Override
            public void run() {
              CardGetResponse response = new Gson().fromJson(successData.toString(),CardGetResponse.class);

              if (response != null) {

               // Log.w("Success", "onSuccess: "+response);
                ArrayList<CardDetail> cardsList = response.getData();
                if (cardsList!=null && cardsList.size() > 0) {
                //  Log.d("TAG", "onNextCARDBODY: " + cardsList.get(0).getBrand() + " ");

               /*   sessionManager.setDefaultCardId(cardsList.get(0).getId());
                  sessionManager.setDefaultCardNum(cardsList.get(0).getLast4());
                  sessionManager.setDefaultCardName(cardsList.get(0).getBrand());
*/
                  addItems(cardsList);
                } else
                  cardNotFound();
              }
                onHideProgress();
            }
          });
        }

        @Override
        public void onFailure(@NotNull String failure) {
            onHideProgress();
        }
      });

    } else {
      if(alertProgress != null)
          alertProgress.showNetworkAlert(this);
    }

  }


  @Override
  public void onLogout(String msg, SessionManagerImpl manager) {
    alertProgress.alertPositiveOnclick(PaymentMethodActivity.this, msg, getString(R.string.logout),
        getString(R.string.ok), new DialogInterfaceListner() {
          @Override
          public void dialogClick(boolean isClicked) {
            Utility.setMAnagerWithBID(PaymentMethodActivity.this, manager);
          }
        });
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    finish();
    overridePendingTransition(R.anim.mainfadein,R.anim.slide_down_acvtivity);
  }

  public void deleteCard(String cardId) {
    alertProgress.alertPositiveNegativeOnclick(this, getString(R.string.want_to_delete), getString(R.string.system_error),getResources().getString(R.string.ok), getResources().getString(R.string.cancel),false , isClicked -> {
      if(isClicked)
      {
          alertDialog = alertProgress.getProgressDialog(this,getString(R.string.deleting));
        if (alertDialog != null && alertDialog.isShowing()) {
          alertDialog.show();
        }
        DeleteCard deleteCard = new DeleteCard(cardId);
        UserAccounts.INSTANCE.deleteCard(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),Constants.selLang, deleteCard.getCardId(),
                new AccountsDelegate() {
                  @Override
                  public void onSuccess(@NotNull Object successData) {
                   runOnUiThread(() -> {
                      if (alertDialog!= null && alertDialog.isShowing())
                        alertDialog.dismiss();

                      onShowProgress();
                       UserAccounts.INSTANCE.getCards(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),selLang, sessionManager.getSID(), new AccountsDelegate() {
                       @Override
                       public void onSuccess(@NotNull Object successData) {
                         runOnUiThread(new Runnable() {
                           @Override
                           public void run() {
                             CardGetResponse response = new Gson().fromJson(successData.toString(),CardGetResponse.class);

                             if (response != null) {
                               Log.w("Success", "onSuccess: "+response);
                               ArrayList<CardDetail> cardsList = response.getData();
                               if (cardsList!=null && cardsList.size() > 0) {
                                 Log.d("TAG", "onNextCARDBODY: " + cardsList.get(0).getBrand() + " ");
                                 addItems(cardsList);
                               } else
                                 cardNotFound();

                             }
                             onHideProgress();
                           }
                         });
                       }

                       @Override
                       public void onFailure(@NotNull String failure) {
                           runOnUiThread(new Runnable() {
                               @Override
                               public void run() {
                                   if (alertDialog!= null && alertDialog.isShowing())
                                       alertDialog.dismiss();
                               }
                           });
                                onHideProgress();
                       }
                     });
                    });

                  }

                  @Override
                  public void onFailure(@NotNull String failure) {
                    onHideProgress();
                  }
                });
      }
    });

  }

    @Override
    public void onConnectionError(String connectionError) {
    }
 @Override
    public void onError(String msg) {
    }

    @Override
    public void onSessionExpired() {
    }
}
