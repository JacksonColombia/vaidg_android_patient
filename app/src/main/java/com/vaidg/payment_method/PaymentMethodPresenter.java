package com.vaidg.payment_method;

/**
 * @author Pramod
 * @since 31-01-2018.
 */

public interface PaymentMethodPresenter {

    void getCard(String auth);
}
