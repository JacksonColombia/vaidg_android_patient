package com.vaidg;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.utility.AlertProgress;
import com.vaidg.databinding.ActivityAboutLspBinding;
import com.vaidg.utilities.AppTypeface;

import static com.vaidg.BuildConfig.VERSION_NAME;
import static com.vaidg.utilities.Constants.FACEBOOK_LINK;
import static com.vaidg.utilities.Constants.PLAY_STORE_LINK;
import static com.vaidg.utilities.Constants.WEB_SITE_LINK;
import static com.vaidg.utilities.Utility.setTextSize14Sp;
import static com.vaidg.utilities.Utility.setTextSize16Sp;

/**
 * <h2>AboutLSPActivity</h2>
 * <p>
 * About page where it will show the info about the application.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class AboutLSPActivity extends AppCompatActivity implements View.OnClickListener {
    public final String TAG = AboutLSPActivity.class.getSimpleName();
    private ActivityAboutLspBinding binding;
    private AppTypeface mAppTypeface;
    private AlertProgress mAlertProgress;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAboutLspBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        mContext = this;
        mAppTypeface = AppTypeface.getInstance(mContext);
        mAlertProgress = new AlertProgress(mContext);
        initialize();
    }

    /**
     * <h2>initialize</h2>
     * <p> initialize the view</p>
     */
    private void initialize() {
        setSupportActionBar(binding.toolbarLayout.toolbar);
        binding.toolbarLayout.toolbar.setElevation(0f);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0f);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.toolbarLayout.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
        binding.toolbarLayout.tvCenter.setTypeface(mAppTypeface.getHind_semiBold());
        setTextSize16Sp(mContext, binding.toolbarLayout.tvCenter);
        binding.toolbarLayout.tvCenter.setText(mContext.getResources().getString(R.string.about));
        binding.tvAppVersion.setTypeface(mAppTypeface.getHind_medium());
        binding.tvWebsite.setTypeface(mAppTypeface.getHind_medium());
        binding.tvRateStore.setTypeface(mAppTypeface.getHind_medium());
        binding.tvLikeFb.setTypeface(mAppTypeface.getHind_medium());
        setTextSize16Sp(mContext, binding.tvRateStore);
        setTextSize16Sp(mContext, binding.tvLikeFb);
        setTextSize16Sp(mContext, binding.tvWebsite);
        setTextSize14Sp(mContext, binding.tvAppVersion);
        binding.tvAppVersion.setText(new StringBuilder()
                .append(getString(R.string.appVersion))
                .append(" ")
                .append(VERSION_NAME));
        SpannableString content =
                new SpannableString(mContext.getResources().getString(R.string.lsp_website));
        content.setSpan(new UnderlineSpan(), 0,
                new SpannableString(mContext.getResources()
                        .getString(R.string.lsp_website)).length(), 0);
        binding.tvWebsite.setText(content);
        binding.tvWebsite.setOnClickListener(this);
        binding.tvRateStore.setOnClickListener(this);
        binding.tvLikeFb.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    /**
     * <h2>closeActivity</h2>
     * <p> method for closing current page</p>
     */
    private void closeActivity() {
        finish();
        overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.tvWebsite) {
            if (mAlertProgress.isNetworkAvailable(mContext)) openWebSite();
            else mAlertProgress.showNetworkAlert(mContext);
        } else if (id == R.id.tvRateStore) {
            if (mAlertProgress.isNetworkAvailable(mContext)) openGooglePlay();
            else mAlertProgress.showNetworkAlert(mContext);
        } else {
            if (mAlertProgress.isNetworkAvailable(mContext)) openFaceBook();
            else mAlertProgress.showNetworkAlert(mContext);
        }
    }

    /**
     * <h2>openWebSite</h2>
     * <p>method for opening website of the app</p>
     */
    private void openWebSite() {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(WEB_SITE_LINK));
            startActivity(i);
        } catch (Exception e) {
            Toast.makeText(mContext, getString(R.string.unableToOpenBrowser), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }


    }

    /**
     * <h2>openGooglePlay</h2>
     * <p>method for opening current app in google play store</p>
     */
    private void openGooglePlay() {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(PLAY_STORE_LINK));
            startActivity(i);
        } catch (Exception e) {
            Toast.makeText(mContext, getString(R.string.unableToOpenPlayStore), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    /**
     * <h2>openFaceBook</h2>
     * <p>method for opening facebook page of the app</p>
     */
    private void openFaceBook() {
        Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
        String facebookUrl = getFacebookPageURL();
        facebookIntent.setData(Uri.parse(facebookUrl));
        startActivity(facebookIntent);
    }

    /**
     * <h2>getFacebookPageURL</h2>
     * <p> mehtod for checking the facebook app installed or not ,
     * if installed then return valid page url accoring to the facebook app</p>
     *
     * @return url of the Facebook page of the app
     */
    public String getFacebookPageURL() {
      /*  PackageManager packageManager = getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + FACEBOOK_LINK;
            } else { //older versions of fb app
                return "fb://page/" + FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return FACEBOOK_LINK; //normal web url
        }*/
        return FACEBOOK_LINK;
    }
}