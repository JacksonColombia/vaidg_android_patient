package com.vaidg.chatting;

import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;
//import com.vaidg.networking.ChatApiService;
import com.pojo.ChatData;

import java.util.ArrayList;

/**
 * Created by Ali on 4/19/2018.
 */
public interface ChattingPresenter
{
    interface Presenter extends BasePresenter
    {
        void onHistoryApi(long bid, String proId, int pageIndex);

        void onPostMsg(int msgType, long msgId, String msg, String cId, long bid, String proId);

        void getIntentValue();

        void loadImage(String path, long bid);

         void getPublicKey(String proId);


        /**
         * <h2>checkForKeystore</h2>
         * This method is used to check for keystore
         */
        void checkForKeystore();

         /**
         * <h2>decryptPublicKeyPostLollipop</h2>
         * This method is used to decrypt the data for post lollipop device
          * @return
          */
        String decryptPublicKeyPostLollipop();
        /**
         * <h2>decryptPrivateKeyPostLollipop</h2>
         * This method is used to decrypt the data for post lollipop device
         *
         * @return
         */
        String decryptPrivateKeyPostLollipop();
        /**
         * <h2>decryptPrivateKeyPreMarshMallow</h2>
         * This method is used to decrypt the data for pre marshmallow device
         * @return
         */
        String decryptPrivateKeyPreMarshMallow();
        /**
         * <h2>decryptPublicKeyPreMarshMallow</h2>
         * This method is used to decrypt the data for pre marshmallow device
         * @return
         */
        String decryptPublicKeyPreMarshMallow();
    }
    interface ViewChatting extends BaseView
    {

        void onMoreAvailable(boolean isMoreAvailable);

        void onChatHistoryResponse(ArrayList<ChatData> data);

        void onRefreshing(boolean b);

        void onDataRefresh(int msgType, long msgId, String msg, String cId, long bid, String proId);

        void setIntentValue(long chatBookingID, String chatProId, String proName, String sid);

        void sendImageMessage(String image, int typeMsg, long msgid);

    }
}
