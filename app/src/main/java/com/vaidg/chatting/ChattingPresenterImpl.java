package com.vaidg.chatting;


import static com.vaidg.utilities.Constants.AES_MODE;
import static com.vaidg.utilities.Constants.ANDROID_KEYSTORE;
import static com.vaidg.utilities.Constants.FIXED_IV;
import static com.vaidg.utilities.Constants.IV_PARAMETER_SPEC;
import static com.vaidg.utilities.Constants.KEY_PRIVATEALIAS;
import static com.vaidg.utilities.Constants.KEY_PUBLICALIAS;
import static com.vaidg.utilities.Constants.RSA_MODE;

import android.annotation.SuppressLint;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import com.vaidg.encryptiondecryption.CryptographyExample;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.google.gson.Gson;
import com.pojo.ChatEncryptDecryptPublicKey;
import com.pojo.ChatResponseHistory;
import com.pojo.ErrorHandel;
import com.utility.RefreshToken;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilPrivateKey;
import com.virgilsecurity.sdk.crypto.VirgilPublicKey;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;
import com.virgilsecurity.sdk.utils.ConvertionUtils;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.vaidg.networking.ChatApiService;


/**
 * Created by Ali on 4/19/2018.
 */
public class ChattingPresenterImpl implements ChattingPresenter.Presenter {


  @Inject
  ChattingPresenter.ViewChatting view;

  @Inject
  SessionManagerImpl manager;
  @Inject
  Gson gson;

  @Inject
  LSPServices lspServices;

  private KeyStore keyStore;

  private CompositeDisposable disposable;

  @Inject
  public ChattingPresenterImpl() {
    disposable = new CompositeDisposable();
  }


  @Override
  public void attachView(Object view) {

  }

  @Override
  public void detachView() {

  }

  @Override
  public void onHistoryApi(final long bid, String proId, final int pageIndex) {
    Observable<Response<ResponseBody>> observable = lspServices.getChatHistory(LSPApplication.getInstance().getAuthToken(manager.getSID())
        , Constants.selLang, Constants.PLATFORM_ANDROID, bid, proId, pageIndex);

    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            disposable.clear();
            disposable.dispose();
            int code = responseBodyResponse.code();
            String response;
            try {
              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                  response = responseBodyResponse.body().string();
                  Log.d("TAG", "onNextOnSuccess: " + response);
                  ChatResponseHistory chatReponce = gson.fromJson(response, ChatResponseHistory.class);
                  if (chatReponce.getData() != null) {
                    if (chatReponce.getData().size() == 0) {
                      if (view != null) {
                        view.onMoreAvailable(false);
                      }
                    }
                    if(view != null) {
                      view.onChatHistoryResponse(chatReponce.getData());
                    }
                  }
                  if(view != null) {
                    view.onRefreshing(false);
                  }
                  break;
                case Constants.SESSION_LOGOUT:
                  if(view != null) {
                    view.onLogout(new JSONObject(responseBodyResponse.errorBody().string()).getString("data"), manager);
                  }
                  break;
                case Constants.SESSION_EXPIRED:

                  RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                      onHistoryApi(bid, proId, pageIndex);
                    }

                    @Override
                    public void onFailureRefreshToken() {

                    }

                    @Override
                    public void sessionExpired(String msg) {

                    }
                  });
                  break;
                default:
                  response = responseBodyResponse.errorBody().string();
                  ErrorHandel errorHandel = gson.fromJson(response, ErrorHandel.class);
                  if(view != null) {
                    view.onError(errorHandel.getMessage());
                    view.onMoreAvailable(false);
                    view.onRefreshing(false);
                  }
                  break;
              }

            } catch (IOException e) {
              e.printStackTrace();
              if(view != null) {
                view.onMoreAvailable(false);
                view.onRefreshing(false);
              }
            } catch (JSONException e) {
              e.printStackTrace();
            }


          }

          @Override
          public void onError(Throwable e) {

            disposable.clear();
            disposable.dispose();
          }

          @Override
          public void onComplete() {

            disposable.clear();
            disposable.dispose();
          }
        });
  }

  @Override
  public void onPostMsg(final int msgType, final long msgId, final String messageToDecrypt, final String cId, final long bid, final String proId) {
    String messageFromDecrypt = "";
    if (msgType == 1) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        decryptPrivateKeyPostLollipop();
        decryptPublicKeyPostLollipop();
        messageFromDecrypt = encryptData(decryptPrivateKeyPostLollipop(), decryptPublicKeyPostLollipop(), messageFromDecrypt, messageToDecrypt);
      } else {
        decryptPrivateKeyPreMarshMallow();
        decryptPublicKeyPreMarshMallow();
        messageFromDecrypt = encryptData(decryptPrivateKeyPreMarshMallow(), decryptPublicKeyPreMarshMallow(), messageFromDecrypt, messageToDecrypt);
      }
    } else {
      messageFromDecrypt = messageToDecrypt;
    }


    Map<String, Object> jsonParams = new HashMap<>();
    jsonParams.put("type", msgType);
    jsonParams.put("timestamp", msgId);
    jsonParams.put("content", messageFromDecrypt);
    jsonParams.put("fromID", cId);
    jsonParams.put("bid", bid + "");
    jsonParams.put("targetId", proId);

    final String messageToDecode = messageFromDecrypt;
    Observable<Response<ResponseBody>> observable = lspServices.postMessage(LSPApplication.getInstance().getAuthToken(manager.getSID()),
        Constants.selLang, Constants.PLATFORM_ANDROID, jsonParams);

    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            int code = responseBodyResponse.code();
            Log.w("TAG", "onNextSendMsg: " + code);
            String response;
            try {
              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                  if(view != null) {
                    view.onDataRefresh(msgType, msgId, messageToDecode, cId, bid, proId);
                  }
                  break;
                case Constants.SESSION_LOGOUT:
                  if(view != null) {
                    view.onLogout(new JSONObject(responseBodyResponse.errorBody().string()).getString("data"), manager);
                  }
                  break;
                case Constants.SESSION_EXPIRED:
                  RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                      onPostMsg(msgType, msgId, messageToDecrypt, cId, bid, proId);
                    }

                    @Override
                    public void onFailureRefreshToken() {

                    }

                    @Override
                    public void sessionExpired(String msg) {

                    }
                  });
                  break;
                default:

                  response = responseBodyResponse.errorBody().string();
                  try {
                    ErrorHandel errorHandel = gson.fromJson(response, ErrorHandel.class);
                    if(view != null) {
                      view.onError(errorHandel.getMessage());
                    }
                  } catch (Exception e) {
                    e.printStackTrace();
                  }

                  break;
              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {

          }

          @Override
          public void onComplete() {

          }
        });
  }

  private String encryptData(String privateKey, String publicKey, String messageFromDecrypt, String messageToDecrypt) {

    try {
      CryptographyExample cryptographyExample = new CryptographyExample();
      VirgilCrypto crypto = new VirgilCrypto();
      List<VirgilPublicKey> receiversPublicKeys = new ArrayList<>();
      // Import private key
      VirgilPrivateKey importedPrivateKey = cryptographyExample.importPrivateKey(privateKey, crypto);
      // Import public key
      VirgilPublicKey importedPublicKey = cryptographyExample.importPublicKey(publicKey, crypto);
      VirgilPublicKey importedDoctorPublicKey = cryptographyExample.importPublicKey(manager.getVirgilDoctorPublicKey(), crypto);
      byte[] signature = cryptographyExample.createSignature(importedPrivateKey, messageToDecrypt);
      // Verify data signature
      byte[] dataToSign = ConvertionUtils.toBytes(messageToDecrypt);
      boolean valid = cryptographyExample.verifySignature(signature, dataToSign, importedDoctorPublicKey, crypto);
      receiversPublicKeys.add(importedPublicKey);
      receiversPublicKeys.add(importedDoctorPublicKey);
      // Encrypt data
      byte[] encryptedData = cryptographyExample.dataEncryption(receiversPublicKeys, messageToDecrypt);
      messageFromDecrypt = Base64.encodeToString(encryptedData, Base64.DEFAULT);//Base64.encodeToString(encryptedData, Base64.DEFAULT);
    } catch (CryptoException e) {
      e.printStackTrace();
    }
    return messageFromDecrypt;
  }

  @Override
  public void loadImage(String path, long bid) {

    File file = new File(path);
    RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
    MultipartBody.Part body = MultipartBody.Part.createFormData("photo", file.getName(), reqFile);
    // RequestBody description = RequestBody.create(MediaType.parse("text/plain"), bid + "");

    // Log.d("TAG", "launchUploadActivity: " + file.getName() + " reqfile " + reqFile.toString() + " body " + body.toString() + " descrip " + description.toString());

    // finally, execute the request
    Call<ResponseBody> call = lspServices.upload(body);//description
    call.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call,
                             Response<ResponseBody> response) {

        int code = response.code();
        Log.v("Upload", "success " + code);
        long msgid = (System.currentTimeMillis());///1000;
        int typeMsg = 2;
        try {

          if (response.isSuccessful()) {

            String responseBody = response.body().string();
            Log.d("TAG", "onResponse: " + responseBody);
            JSONObject jsonObject = new JSONObject(responseBody);
            String image = jsonObject.getString("data");

            view.sendImageMessage(image, typeMsg, msgid);
          } else {
            String errorBody = response.errorBody().string();
            Log.d("TAG", "onResponse: " + errorBody);
          }

        } catch (JSONException e) {
          e.printStackTrace();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.e("Upload error:", t.getMessage());
      }
    });
  }

  @Override
  public void getPublicKey(String proId) {
    Observable<Response<ResponseBody>> observable = lspServices.getPublicKey(LSPApplication.getInstance().getAuthToken(manager.getSID())
        , Constants.selLang, Constants.PLATFORM_ANDROID, Constants.PROVIDER, proId);

    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int code = responseBodyResponse.code();
            String response;
            try {
              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                  response = responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
                  ChatEncryptDecryptPublicKey chatEncryptDecryptPublicKey = gson.fromJson(response, ChatEncryptDecryptPublicKey.class);
                  if (chatEncryptDecryptPublicKey.getData() != null) {
                    ChatEncryptDecryptPublicKey.ChatPublicKey chatPublicKey = chatEncryptDecryptPublicKey.getData();
                    if (chatPublicKey.getPublickKey() != null && !chatPublicKey.getPublickKey().isEmpty()) {
                      manager.setVirgilDoctorPublicKey(chatPublicKey.getPublickKey());
                    }
                  }
                  break;
                case Constants.SESSION_LOGOUT:
                  if(view != null) {
                    view.onLogout(new JSONObject(responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null).getString("data"), manager);
                  }
                  break;
                case Constants.SESSION_EXPIRED:

                  RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                    @Override
                    public void onSuccessRefreshToken(String newToken) {
                      LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                      getPublicKey(proId);
                    }

                    @Override
                    public void onFailureRefreshToken() {

                    }

                    @Override
                    public void sessionExpired(String msg) {

                    }
                  });
                  break;
                default:
                  response = responseBodyResponse.errorBody().string();
                  ErrorHandel errorHandel = gson.fromJson(response, ErrorHandel.class);
                  break;
              }

            } catch (IOException | JSONException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
          }

          @Override
          public void onComplete() {
          }
        });
  }

  @Override
  public void getIntentValue() {

    view.setIntentValue(manager.getChatBookingID(), manager.getChatProId(), manager.getProName(), manager.getSID());
   /* long chatBookID =  Long.parseLong("1590583264158");
    view.setIntentValue(chatBookID, "5eb90c1621a0daddd194529c","Dr SS Kk", "5ebd5dd821a0daddd1564c02");
  */}


  @SuppressLint("NewApi")
  @Override
  public String decryptPrivateKeyPostLollipop() {
    String str = "";
    Log.i("TEST", "decryptData marsh melloa ");
    Cipher c;
    try {
      c = Cipher.getInstance(AES_MODE);
      c.init(Cipher.DECRYPT_MODE, getSecretPrivateKeyPostLollipop(), new GCMParameterSpec(128, FIXED_IV.getBytes()));
      byte[] encryptedBytes = Base64.decode(manager.getVirgilPrivateKey(), Base64.DEFAULT);

      byte[] decodedBytes = c.doFinal(encryptedBytes);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        str = new String(decodedBytes, StandardCharsets.UTF_8);
      } else {
        str = new String(decodedBytes);
      }
      Log.i("TEST", "decryptData srting " + str);

    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return str;
  }

  @SuppressLint("NewApi")
  @Override
  public String decryptPublicKeyPostLollipop() {
    String str = "";
    Log.i("TEST", "decryptData marsh melloa ");
    Cipher c;
    try {
      c = Cipher.getInstance(AES_MODE);
      c.init(Cipher.DECRYPT_MODE, getSecretPublicKeyPostLollipop(), new GCMParameterSpec(128, FIXED_IV.getBytes()));
      byte[] encryptedBytes = Base64.decode(manager.getVirgilPublicKey(), Base64.DEFAULT);

      byte[] decodedBytes = c.doFinal(encryptedBytes);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        str = new String(decodedBytes, StandardCharsets.UTF_8);
      } else {
        str = new String(decodedBytes);
      }
      Log.i("TEST", "decryptData srting " + str);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return str;
  }

  /**
   * <h2>getSecretPrivateKeyPreMarshMallow</h2>
   * This method is used to get the secret key for pre marsh mallow device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPrivateKeyPreMarshMallow() throws Exception {
    String enryptedKeyB64 = manager.getEncryptionPrivateKey();
    Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
    // need to check null, omitted here
    byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
    byte[] key = rsaPrivateKeyDecrypt(encryptedKey);
    Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
    return new SecretKeySpec(key, "AES");
  }

  /**
   * <h2>getSecretPublicKeyPreMarshMallow</h2>
   * This method is used to get the secret key for pre marsh mallow device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPublicKeyPreMarshMallow() throws Exception {
    String enryptedKeyB64 = manager.getEncryptionPublicKey();
    Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
    // need to check null, omitted here
    byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
    byte[] key = rsaPublicKeyDecrypt(encryptedKey);
    Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
    return new SecretKeySpec(key, "AES");
  }


  private byte[] rsaPrivateKeyDecrypt(byte[] encrypted) throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PRIVATEALIAS, null);
    Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
    output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
    CipherInputStream cipherInputStream = new CipherInputStream(
        new ByteArrayInputStream(encrypted), output);
    ArrayList<Byte> values = new ArrayList<>();
    int nextByte;
    while ((nextByte = cipherInputStream.read()) != -1) {
      values.add((byte) nextByte);
    }

    byte[] bytes = new byte[values.size()];
    for (int i = 0; i < bytes.length; i++) {
      bytes[i] = values.get(i);
    }
    return bytes;
  }

  private byte[] rsaPublicKeyDecrypt(byte[] encrypted) throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PUBLICALIAS, null);
    Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
    output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
    CipherInputStream cipherInputStream = new CipherInputStream(
        new ByteArrayInputStream(encrypted), output);
    ArrayList<Byte> values = new ArrayList<>();
    int nextByte;
    while ((nextByte = cipherInputStream.read()) != -1) {
      values.add((byte) nextByte);
    }

    byte[] bytes = new byte[values.size()];
    for (int i = 0; i < bytes.length; i++) {
      bytes[i] = values.get(i);
    }
    return bytes;
  }


  @Override
  public String decryptPrivateKeyPreMarshMallow() {
    Log.i("TEST", "decryptData else  ");
    Cipher c;
    String str = "";
    try {
      c = Cipher.getInstance(AES_MODE, "BC");
      c.init(Cipher.DECRYPT_MODE, getSecretPrivateKeyPreMarshMallow(), IV_PARAMETER_SPEC);
      byte[] encryptedBytes = Base64.decode(manager.getEncryptionPrivateKey(), Base64.DEFAULT);
      byte[] decodedBytes = c.doFinal(encryptedBytes);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        str = new String(decodedBytes, StandardCharsets.UTF_8);
      } else {
        str = new String(decodedBytes);
      }
      Log.i("TEST", "decryptData srting " + str);


    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return str;
  }

  @Override
  public String decryptPublicKeyPreMarshMallow() {
    Log.i("TEST", "decryptData else  ");
    Cipher c;
    String str = "";
    try {
      c = Cipher.getInstance(AES_MODE, "BC");
      c.init(Cipher.DECRYPT_MODE, getSecretPublicKeyPreMarshMallow(), IV_PARAMETER_SPEC);
      byte[] encryptedBytes = Base64.decode(manager.getEncryptionPublicKey(), Base64.DEFAULT);
      byte[] decodedBytes = c.doFinal(encryptedBytes);
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        str = new String(decodedBytes, StandardCharsets.UTF_8);
      } else {
        str = new String(decodedBytes);
      }
      Log.i("TEST", "decryptData srting " + str);

    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return str;
  }


  /**
   * <h2>getSecretPrivateKeyPostLollipop</h2>
   * This method is used to get the secret key for post lollipop device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPrivateKeyPostLollipop() throws Exception {
    Key s = keyStore.getKey(KEY_PRIVATEALIAS, null);
    Log.i("TEST", "key " + s.toString());
    return keyStore.getKey(KEY_PRIVATEALIAS, null);
  }

  /**
   * <h2>getSecretPublicKeyPostLollipop</h2>
   * This method is used to get the secret key for post lollipop device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPublicKeyPostLollipop() throws Exception {
    Key s = keyStore.getKey(KEY_PUBLICALIAS, null);
    Log.i("TEST", "key " + s.toString());
    return keyStore.getKey(KEY_PUBLICALIAS, null);
  }

  @Override
  public void checkForKeystore() {
    try {
      keyStore = KeyStore.getInstance(ANDROID_KEYSTORE);
      keyStore.load(null);
    } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
      e.printStackTrace();
    }
    try {
      if (!keyStore.containsAlias(KEY_PRIVATEALIAS)) {
        //mSignUpView.checkForVersion();
      }
    } catch (KeyStoreException e) {
      e.printStackTrace();
    }
  }

}
