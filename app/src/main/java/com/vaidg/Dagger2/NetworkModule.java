package com.vaidg.Dagger2;

import static com.vaidg.SlideLayout.SlideLayout.TAG;
import static com.vaidg.networking.ServiceFactory.getLoggingInterceptor;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.vaidg.BuildConfig;
import com.vaidg.networking.GeoNamesApi;
import com.vaidg.networking.LSPServices;
import com.vaidg.networking.NetworkConnectionInterceptor;
import com.vaidg.sslcertificate.SSLCertificate;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import dagger.Module;
import dagger.Provides;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;
import javax.inject.Named;
import javax.inject.Singleton;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Pramod
 * @since 15-12-2017
 */
@Module
public class NetworkModule {
  public static final String NAME_BASE_URL = "NAME_BASE_URL";
  public static final String GEONAMES_BASE_URL="GEONAMES_BASE_URL";
  public static final String CONNECT_TIMEOUT = "60000";
  public static final String READ_TIMEOUT = "30000";
  public static final String WRITE_TIMEOUT = "15000";
  private static final long CACHE_SIZE = 10 * 1024 * 1024; //10 MB

  @Provides
  @Named(NAME_BASE_URL)
  String provideBaseUrl() {
    return BuildConfig.BASE_URL;
  }


  @Provides
  @Named(GEONAMES_BASE_URL)
  String provideGeonamesBaseUrl(){
    return BuildConfig.GEONAMES_BASE_URL;
  }

  @Provides
  @Singleton
  Converter.Factory provideGsonConverterFactory() {
    return GsonConverterFactory.create();
  }


  @Provides
  @Singleton
  Cache provideOkHttpCache(Application application) {
    return new Cache(application.getCacheDir(), CACHE_SIZE);
  }

  @Provides
  @Singleton
  OkHttpClient provideOkHttpClient(Cache cache, Context context) {
    OkHttpClient.Builder builder = //SSLCertificate.getSSLCertificate(context).newBuilder()
    new OkHttpClient().newBuilder()
        .cache(cache)
        .connectTimeout(60, TimeUnit.SECONDS)
        .readTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(60, TimeUnit.SECONDS)
        .retryOnConnectionFailure(true)
        .addInterceptor(new NetworkConnectionInterceptor(context))
        .addInterceptor(getLoggingInterceptor());
    return builder.build();
  }

  @Provides
  @Named(NAME_BASE_URL)
  @Singleton
  Retrofit provideRetrofit(Converter.Factory converter, @Named(NAME_BASE_URL) String baseUrl, OkHttpClient okHttpClient) {

    return new Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(converter)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(okHttpClient)
        .build();
  }

  @Provides
  @Named(GEONAMES_BASE_URL)
  @Singleton
  Retrofit provideRetrofitGeonames(Converter.Factory converter,@Named(GEONAMES_BASE_URL) String baseUrl, OkHttpClient okHttpClient){
    return new Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(converter)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build();
  }

  @Provides
  @Singleton
  LSPServices provideLSPService(@Named(NAME_BASE_URL)Retrofit retrofit) {
    return retrofit.create(LSPServices.class);
  }

  @Provides
  @Singleton
  GeoNamesApi provideGeoNamesService(@Named(GEONAMES_BASE_URL)Retrofit retrofit){
    return retrofit.create(GeoNamesApi.class);
  }
}
