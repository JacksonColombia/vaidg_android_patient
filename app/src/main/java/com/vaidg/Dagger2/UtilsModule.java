package com.vaidg.Dagger2;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.vaidg.incalloutcalltelecall.dependent.model.AddDependentModel;
import com.vaidg.providerList.model.DoctorObservable;
import com.google.gson.Gson;
import com.vaidg.Login.LoginModel;
import com.vaidg.countrypic.CountryPicker;
import com.vaidg.signup.SignUpModel;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.SessionManagerImpl;
import com.mqtt.MQTTManager;
import com.pojo.CheckFlagForValidity;
import com.utility.AlertProgress;
import com.utility.PermissionsManager;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import com.pojo.MyBookingObservable;

import java.util.HashMap;

/**
 * @author Pramod
 * @since 05-01-2018
 */
@Module
public class UtilsModule {

    @Provides
    @Singleton
    CompositeDisposable provideDisposable(){
        return new CompositeDisposable();
    }

    @Provides
    @Singleton
    LoginModel loginModel() { return new LoginModel();}

    @Provides
    @Singleton
    SignUpModel provideSingUpModel()
    {
        return new SignUpModel();
    }

    @Provides
    @Singleton
    CountryPicker countryPicker() { return new CountryPicker();}

    @Provides
    @Singleton
    DoctorObservable provideObservable(){return new DoctorObservable();}

    @Named("Booking")
    @Provides
    @Singleton
    MyBookingObservable provideMyBookngObservable(){return new MyBookingObservable();}

   //, DoctorObservable rxProviderObserver,
    @Provides
    @Singleton
    MQTTManager  mqttManager(Context context,
                             SessionManagerImpl sessionManagerImpl, Gson gson,MyBookingObservable rxMyBookingObservable
    , DoctorObservable providerObservable)//,MyBookingObservable rxMyBookingObservable
    {
        return new MQTTManager(context,sessionManagerImpl,gson,rxMyBookingObservable,providerObservable);//rxProviderObserver,
    }


    @Provides
    @Singleton
    Gson provideGSON(){return new Gson();}

    @Provides
    @Singleton
    AppTypeface provideAppTypeFace(Context mContext){return AppTypeface.getInstance(mContext);}

    @Provides
    @Singleton
    PermissionsManager providePermissionMgr(Context mContext) {return new PermissionsManager();}

    @Provides
    @Singleton
    AlertProgress provideAlertProgress(Context mContext) {return new AlertProgress(mContext);}

    @Provides
    @Singleton
    LinearLayoutManager provideLinearLayoutManager(Context mContext)
    {
        return new LinearLayoutManager(mContext);

    }

    @Provides
    @Singleton
    AddDependentModel provideAddDependentModel() {
        return new AddDependentModel();
    }

  @Provides
  @Singleton
  CheckFlagForValidity checkFlagForValidity() {return new CheckFlagForValidity();}

    /*@Provides
    @Singleton
    SessionManager provideSessionManager(Context mContext)
    {
        return new SessionManager(mContext);
    }*/

   /* @Provides
    @Singleton
    LocationUtil provideLocationUtil(Activity mContext){return new LocationUtil(mContext);}*/

}
