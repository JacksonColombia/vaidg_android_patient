package com.vaidg.Dagger2;

import com.vaidg.Login.LoginActivity;
import com.vaidg.Login.LoginModule;
import com.vaidg.prescription.filterdate.FilterDateActivity;
import com.vaidg.razorpay.RazorPayPaymentActivity;
import com.vaidg.razorpay.RazorPaymentModules;
import com.vaidg.share.ShareDaggerModule;
import com.vaidg.splashscreen.SplashActivity;
import com.vaidg.splashscreen.SplashModule;
import com.vaidg.addTocart.AddToCart;
import com.vaidg.addTocart.AddToCartModule;
import com.vaidg.add_address.AddAddressActivity;
import com.vaidg.add_address.AddAddressModule;
import com.vaidg.cancelledBooking.CancelledBooking;
import com.vaidg.cancelledBooking.CancelledBookingModule;
import com.vaidg.change_email.ChangeEmailActivity;
import com.vaidg.change_email.ChangeEmailModule;
import com.vaidg.changepassword.ChangePwdActivity;
import com.vaidg.changepassword.ChangePwdModule;
import com.vaidg.chatting.ChattingActivity;
import com.vaidg.chatting.ChattingModule;
import com.vaidg.confirmbookactivity.ConfirmBookActivity;
import com.vaidg.confirmbookactivity.ConfirmBookingModule;
import com.vaidg.faq.FaqActivity;
import com.vaidg.faq.FaqModule;
import com.vaidg.favouriteProvider.FavouriteProvider;
import com.vaidg.favouriteProvider.FavouriteProviderModule;
import com.vaidg.filter.FilterActivity;
import com.vaidg.filter.FilterDaggerModule;
import com.vaidg.filter.FilterDaggerResponseModule;
import com.vaidg.forgotpassword.ForgotPwdActivity;
import com.vaidg.forgotpassword.ForgotPwdModule;
import com.vaidg.home.MainActivity;
import com.vaidg.home.MainActivityDaggerModule;
import com.vaidg.inCallOutCall.TimeSlots;
import com.vaidg.inCallOutCall.TimeSlotsModule;
import com.vaidg.incalloutcalltelecall.InCallOutCallTeleCallActivity;
import com.vaidg.incalloutcalltelecall.InCallOutCallTeleCallModule;
import com.vaidg.incalloutcalltelecall.dependent.DependentActivity;
import com.vaidg.incalloutcalltelecall.dependent.DependentModule;
import com.vaidg.incalloutcalltelecall.dependent.add_dependent.AddDependent;
import com.vaidg.incalloutcalltelecall.dependent.add_dependent.AddDependentModule;
import com.vaidg.incalloutcalltelecall.dependent.editrelationship.EditRelationshipActivity;
import com.vaidg.incalloutcalltelecall.dependent.editrelationship.EditRelationshipModule;
import com.vaidg.incalloutcalltelecall.questionselection.QuestionSelectionActivity;
import com.vaidg.incalloutcalltelecall.questionselection.QuestionSelectionModule;
import com.vaidg.incalloutcalltelecall.symptom.SearchFilterModule;
import com.vaidg.incalloutcalltelecall.symptom.SymptomSearchFilter;
import com.vaidg.intro.IntroActivity;
import com.vaidg.intro.IntroModule;
import com.vaidg.invoice.InvoiceActivity;
import com.vaidg.invoice.InvoiceDaggerModule;
import com.vaidg.jobDetailsStatus.JobDetailsActivity;
import com.vaidg.jobDetailsStatus.JobDetailsModules;
import com.vaidg.otp.OtpActivity;
import com.vaidg.otp.OtpModule;
import com.vaidg.payment_details.PaymentDetailActivity;
import com.vaidg.payment_details.PaymentDetailsModule;
import com.vaidg.payment_edit_card.CardEditActivity;
import com.vaidg.payment_edit_card.CardEditModule;
import com.vaidg.payment_method.PaymentMethodActivity;
import com.vaidg.payment_method.PaymentMethodModule;
import com.vaidg.prescription.FilterDependentModule;
import com.vaidg.prescription.PrescriptionActivity;
import com.vaidg.prescription.PrescriptionDaggerModule;
import com.vaidg.profile.ProfileActivity;
import com.vaidg.profile.ProfileModule;
import com.vaidg.promocode.PromoCodeActivity;
import com.vaidg.promocode.PromoCodeDaggerModule;
import com.vaidg.providerList.DoctorList;
import com.vaidg.providerList.DoctorListDaggerModule;
import com.vaidg.providerList.DoctorSearchListDaggerModule;
import com.vaidg.providerList.adapter.DoctorListSearchFilter;
import com.vaidg.providerdetails.ProviderDetails;
import com.vaidg.providerdetails.ProviderDetailsModules;
import com.vaidg.providerdetails.viewschedule.ScheduleActivity;
import com.vaidg.providerdetails.viewschedule.ScheduleModules;
import com.vaidg.rateYourBooking.RateYourBooking;
import com.vaidg.rateYourBooking.RateYourBookingModule;
import com.vaidg.searchsymptom.SearchSymptomActivity;
import com.vaidg.searchsymptom.SearchSymptomActivityDaggerModule;
import com.vaidg.selectPaymentMethod.SelectDaggerModule;
import com.vaidg.selectPaymentMethod.SelectPayment;
import com.vaidg.share.ShareActivity;
import com.vaidg.signup.SignUpActivity;
import com.vaidg.signup.SignUpModule;
import com.vaidg.wallet.WalletActivity;
import com.vaidg.wallet.WalletActivityDaggerModule;
import com.vaidg.walletTransaction.WalletTransActivity;
import com.vaidg.walletTransaction.WalletTransactionActivityDaggerModule;
import com.vaidg.youraddress.YourAddressActivity;
import com.vaidg.youraddress.YourAddressModule;
import com.vaidg.zendesk.zendeskHelpIndex.ZendeskAdapterModule;
import com.vaidg.zendesk.zendeskHelpIndex.ZendeskHelpIndex;
import com.vaidg.zendesk.zendeskHelpIndex.ZendeskModule;
import com.vaidg.zendesk.zendeskTicketDetails.HelpIndexAdapterModule;
import com.vaidg.zendesk.zendeskTicketDetails.HelpIndexTicketDetails;
import com.vaidg.zendesk.zendeskTicketDetails.HelpTicketDetailsModule;
import com.utility.OnMyService;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * @author Pramod
 * @since 11/12/17.
 */

@Module
public interface ActivityBindingModule {
  @ContributesAndroidInjector
  OnMyService service();

  @ActivityScoped
  @ContributesAndroidInjector(modules = IntroModule.class)
  IntroActivity provideIntroActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = LoginModule.class)
  LoginActivity loginActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = FaqModule.class)
  FaqActivity provideFaqActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = SplashModule.class)
  SplashActivity splashScreen();

  @ActivityScoped
  @ContributesAndroidInjector(modules = SignUpModule.class)
  SignUpActivity signUpActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = ForgotPwdModule.class)
  ForgotPwdActivity forgotPwdActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = OtpModule.class)
  OtpActivity otpActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = ChangePwdModule.class)
  ChangePwdActivity changePwdActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = MainActivityDaggerModule.class)
  MainActivity provideMainActivity();


  @ActivityScoped
  @ContributesAndroidInjector(modules = DoctorListDaggerModule.class)
  DoctorList provideProviderList();

  @ActivityScoped
  @ContributesAndroidInjector(modules = DoctorSearchListDaggerModule.class)
  DoctorListSearchFilter provideDoctorListSearchFilter();

  @ActivityScoped
  @ContributesAndroidInjector(modules = {FilterDaggerModule.class,
      FilterDaggerResponseModule.class})
  FilterActivity provideFilterActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = ProviderDetailsModules.class)
  ProviderDetails provideProviderDetails();

  @ActivityScoped
  @ContributesAndroidInjector(modules = ScheduleModules.class)
  ScheduleActivity scheduleFragment();

  @ActivityScoped
  @ContributesAndroidInjector(modules = ConfirmBookingModule.class)
  ConfirmBookActivity provideConfirmBookingActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = SearchSymptomActivityDaggerModule.class)
  SearchSymptomActivity provideSearchSymptomActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = AddToCartModule.class)
  AddToCart provideAddToCart();

  @ActivityScoped
  @ContributesAndroidInjector(modules = ProfileModule.class)
  ProfileActivity profileActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = YourAddressModule.class)
  YourAddressActivity yourAddressActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = AddAddressModule.class)
  AddAddressActivity addAddressActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = CardEditModule.class)
  CardEditActivity provideCardActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = PaymentDetailsModule.class)
  PaymentDetailActivity providePaymentDetailsActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = PaymentMethodModule.class)
  PaymentMethodActivity providePaymentMethodActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = RateYourBookingModule.class)
  RateYourBooking provideRateYourBooking();

  @ActivityScoped
  @ContributesAndroidInjector(modules = JobDetailsModules.class)
  JobDetailsActivity provideJobDetails();


  @ActivityScoped
  @ContributesAndroidInjector(modules = {ZendeskModule.class, ZendeskAdapterModule.class})
  ZendeskHelpIndex provideZendeskHelp();

  @ActivityScoped
  @ContributesAndroidInjector(modules = {HelpTicketDetailsModule.class,
      HelpIndexAdapterModule.class})
  HelpIndexTicketDetails provideHelpIndexDetails();

  @ActivityScoped
  @ContributesAndroidInjector(modules = SelectDaggerModule.class)
  SelectPayment provideSelectPayment();

  @ActivityScoped
  @ContributesAndroidInjector(modules = CancelledBookingModule.class)
  CancelledBooking provideCancelledBooking();

  @ActivityScoped
  @ContributesAndroidInjector(modules = RazorPaymentModules.class)
  RazorPayPaymentActivity provideRazorPayPayment();

  @ActivityScoped
  @ContributesAndroidInjector(modules = ChangeEmailModule.class)
  ChangeEmailActivity provideChangeEmail();

  @ActivityScoped
  @ContributesAndroidInjector(modules = ChattingModule.class)
  ChattingActivity provideChatting();

  @ActivityScoped
  @ContributesAndroidInjector(modules = WalletActivityDaggerModule.class)
  WalletActivity provideWalletActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = PrescriptionDaggerModule.class)
  PrescriptionActivity providePrescriptionActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = WalletTransactionActivityDaggerModule.class)
  WalletTransActivity provideWalletTransaction();

  @ActivityScoped
  @ContributesAndroidInjector(modules = PromoCodeDaggerModule.class)
  PromoCodeActivity providePromoCode();

  @ActivityScoped
  @ContributesAndroidInjector(modules = ShareDaggerModule.class)
  ShareActivity provideShareScree();

  @ActivityScoped
  @ContributesAndroidInjector(modules = FavouriteProviderModule.class)
  FavouriteProvider provideFavouritePro();

  @ActivityScoped
  @ContributesAndroidInjector(modules = TimeSlotsModule.class)
  TimeSlots provideTimeSlots();

  @ActivityScoped
  @ContributesAndroidInjector(modules = InvoiceDaggerModule.class)
  InvoiceActivity provideInvoiceActivity();


  @ActivityScoped
  @ContributesAndroidInjector(modules = InCallOutCallTeleCallModule.class)
  InCallOutCallTeleCallActivity provideInCallOutCallTeleCall();


  @ActivityScoped
  @ContributesAndroidInjector(modules = SearchFilterModule.class)
  SymptomSearchFilter provideSymptomSearchFilter();

  @ActivityScoped
  @ContributesAndroidInjector(modules = QuestionSelectionModule.class)
  QuestionSelectionActivity provideQuestionSelectionActivity();

   /* @ActivityScoped
    @ContributesAndroidInjector(modules = ActivityAudioCallModule.class)
    AudioCallActivity provideAudioCall();*/


  @ActivityScoped
  @ContributesAndroidInjector(modules = DependentModule.class)
  DependentActivity provideDependentActivity();

@ActivityScoped
  @ContributesAndroidInjector(modules = FilterDependentModule.class)
FilterDateActivity provideFilterDateActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = AddDependentModule.class)
  AddDependent provideAddDependentActivity();

  @ActivityScoped
  @ContributesAndroidInjector(modules = EditRelationshipModule.class)
  EditRelationshipActivity provideEditRelationshipActivity();

}
