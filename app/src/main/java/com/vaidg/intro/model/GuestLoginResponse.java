package com.vaidg.intro.model;


import static com.vaidg.utilities.Constants.DATA;
import static com.vaidg.utilities.Constants.MESSAGE;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GuestLoginResponse implements Parcelable {

  public final static Parcelable.Creator<GuestLoginResponse> CREATOR = new Creator<GuestLoginResponse>() {


    @SuppressWarnings({
        "unchecked"
    })
    public GuestLoginResponse createFromParcel(Parcel in) {
      return new GuestLoginResponse(in);
    }

    public GuestLoginResponse[] newArray(int size) {
      return (new GuestLoginResponse[size]);
    }

  };
  @SerializedName(MESSAGE)
  @Expose
  private String message;
  @SerializedName(DATA)
  @Expose
  private GuestLoginData data;

  protected GuestLoginResponse(Parcel in) {
    this.message = ((String) in.readValue((String.class.getClassLoader())));
    this.data = ((GuestLoginData) in.readValue((GuestLoginData.class.getClassLoader())));
  }

  /**
   * No args constructor for use in serialization
   */
  public GuestLoginResponse() {
  }

  /**
   * @param data
   * @param message
   */
  public GuestLoginResponse(String message, GuestLoginData data) {
    super();
    this.message = message;
    this.data = data;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public GuestLoginData getData() {
    return data;
  }

  public void setData(GuestLoginData data) {
    this.data = data;
  }

  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(message);
    dest.writeValue(data);
  }

  public int describeContents() {
    return 0;
  }

}