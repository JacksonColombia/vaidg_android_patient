package com.vaidg.intro.model;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.databinding.ItemIntroSliderBinding;

import java.util.ArrayList;


/**
 * <h1>IntroSliderAdapter</h1>
 * <p>This adapter is used to load list of images in slidingViewPager at IntroSliderNewActivity</p>
 *
 * @author 3embed
 * @version 1.0.20
 * @see
 * @since 06/06/2020
 **/
public class IntroSliderAdapter extends RecyclerView.Adapter<IntroSliderAdapter.IntroSliderViewHolder> {

    private final ArrayList<SliderData> arrayList;
    private ItemIntroSliderBinding binding;

    public IntroSliderAdapter(ArrayList<SliderData> arrayList) {
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public IntroSliderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = ItemIntroSliderBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new IntroSliderViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull IntroSliderViewHolder holder, int position) {
        holder.ivSliderImage.setImageResource(arrayList.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return arrayList != null ? arrayList.size() : 0;
    }

    class IntroSliderViewHolder extends RecyclerView.ViewHolder {

        AppCompatImageView ivSliderImage;

        IntroSliderViewHolder(@NonNull ItemIntroSliderBinding itemView) {
            super(itemView.getRoot());
            ivSliderImage = binding.ivSliderImage;
        }
    }
}
