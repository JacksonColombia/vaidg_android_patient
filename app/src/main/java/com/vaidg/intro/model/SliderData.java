package com.vaidg.intro.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h1>SliderData</h1>
 * <p>It's used to hold drawable id of image ans load intro image into intro slider activity</p>
 *
 * @author 3Embed
 * @version 1.0.20
 * @since 02/06/2020
 */
public class SliderData implements Serializable {
    @SerializedName("image")
    @Expose
    private int image;
    @SerializedName("title")
    @Expose
    private int title;
    @SerializedName("description")
    @Expose
    private int description;

    public SliderData(int image, int title, int description) {
        this.image = image;
        this.title = title;
        this.description = description;
    }

    /**
     * <p>Return id of drawable resource for slider image</p>
     *
     * @return int id of drawable resource
     */
    public int getImage() {
        return image;
    }

    /**
     * <p>Return of string resource for slider title</p>
     *
     * @return int id of string resource
     */
    public int getTitle() {
        return title;
    }

    /**
     * <p>Return id of string resource for slider description</p>
     *
     * @return int id of string resource
     */
    public int getDescription() {
        return description;
    }
}
