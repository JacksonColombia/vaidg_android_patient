package com.vaidg.intro.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.vaidg.Login.pojo.TokenPojo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GuestLoginData implements Parcelable {

  public final static Parcelable.Creator<GuestLoginData> CREATOR = new Creator<GuestLoginData>() {

    @SuppressWarnings({"unchecked"})
    public GuestLoginData createFromParcel(Parcel in) {
      return new GuestLoginData(in);
    }

    public GuestLoginData[] newArray(int size) {
      return (new GuestLoginData[size]);
    }

  };
  @SerializedName("token")
  @Expose
  private TokenPojo token;
  @SerializedName("sid")
  @Expose
  private String sid;

  protected GuestLoginData(Parcel in) {
    this.token = ((TokenPojo) in.readValue((TokenPojo.class.getClassLoader())));
    this.sid = ((String) in.readValue((String.class.getClassLoader())));
  }

  /**
   * No args constructor for use in serialization
   */
  public GuestLoginData() {
  }

  /**
   * @param token
   * @param sid
   */
  public GuestLoginData(TokenPojo token, String sid) {
    super();
    this.token = token;
    this.sid = sid;
  }

  public TokenPojo getToken() {
    return token;
  }

  public void setToken(TokenPojo token) {
    this.token = token;
  }

  public String getSid() {
    return sid;
  }

  public void setSid(String sid) {
    this.sid = sid;
  }

  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(token);
    dest.writeValue(sid);
  }

  public int describeContents() {
    return 0;
  }

}