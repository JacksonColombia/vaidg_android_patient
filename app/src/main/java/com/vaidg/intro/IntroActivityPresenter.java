package com.vaidg.intro;

import static com.vaidg.utilities.Constants.APPVERSION;
import static com.vaidg.utilities.Constants.APP_VERSION;
import static com.vaidg.utilities.Constants.BASE_AUTH_PASSWORD;
import static com.vaidg.utilities.Constants.BASE_AUTH_USERNAME;
import static com.vaidg.utilities.Constants.DEVICEID;
import static com.vaidg.utilities.Constants.DEVICEMODEL;
import static com.vaidg.utilities.Constants.DEVICEOSVERSION;
import static com.vaidg.utilities.Constants.DEVICETIME;
import static com.vaidg.utilities.Constants.DEVICE_MAKER;
import static com.vaidg.utilities.Constants.DEVICE_MODEL;
import static com.vaidg.utilities.Constants.DEVICE_OS_VERSION;
import static com.vaidg.utilities.Constants.DEVICE_TYPE;
import static com.vaidg.utilities.Constants.DEVMAKE;
import static com.vaidg.utilities.Constants.DEVTYPE;
import static com.vaidg.utilities.Constants.IPADDRESS;
import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;
import static com.vaidg.utilities.Constants.selLang;
import static com.vaidg.utilities.Utility.getCurrentTime;
import static com.vaidg.utilities.Utility.isNetworkAvailable;

import com.vaidg.R;
import com.vaidg.intro.model.GuestLoginData;
import com.vaidg.intro.model.GuestLoginResponse;
import com.vaidg.intro.model.SliderData;
import com.vaidg.networking.BasicAuthentication;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.google.gson.Gson;
import com.pojo.LanguageResponse;
import com.pojo.LanguagesList;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * <h2>IntroActivityPresenter</h2>
 * <p>This class is used to communicate between intro activity and model</p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 12-07-2019.
 */
public class IntroActivityPresenter implements IntroActivityContract.IntroPresenter {
  private static String mDeviceId = "";
  @Inject
  IntroActivityContract.IntroView view;
  @Inject
  LSPServices lspServices;
  @Inject
  Gson gson;
  @Inject
  IntroActivity mContext;
  @Inject
  SessionManagerImpl sessionManager;
  @Inject
  IntroActivityPresenter(IntroActivity context) {
    this.mContext = context;
  }

  @Override
  public void doGuestLogin() {
    if (isNetworkAvailable(mContext)) {
      if (view != null) {
        view.onShowProgress();
      }
      try {
        mDeviceId = Utility.getDeviceId(mContext);
      } catch (Exception e) {
        e.printStackTrace();
        if (view != null) {
          view.onHideProgress();
        }
      }
      lspServices = BasicAuthentication.ServiceGenerator.createService(LSPServices.class,
          BASE_AUTH_USERNAME, BASE_AUTH_PASSWORD);
      Map<String, Object> jsonParams = new HashMap<>();
      jsonParams.put(DEVICEID, mDeviceId);
      jsonParams.put(APPVERSION, APP_VERSION);
      jsonParams.put(DEVMAKE, DEVICE_MAKER);
      jsonParams.put(DEVICEMODEL, DEVICE_MODEL);
      jsonParams.put(DEVTYPE, DEVICE_TYPE);
      jsonParams.put(DEVICETIME, getCurrentTime());
      jsonParams.put(DEVICEOSVERSION, DEVICE_OS_VERSION);
      jsonParams.put(IPADDRESS, sessionManager.getIpAddress());
      Observable<Response<ResponseBody>> responseObservable = lspServices.doGuestLogin(selLang,
          PLATFORM_ANDROID, jsonParams);
      responseObservable.subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(new Observer<Response<ResponseBody>>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(Response<ResponseBody> responseBodyResponse) {
              int responseCode = responseBodyResponse.code();
              JSONObject errJson;
              try {
                String responseBody = responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
                String errorBody = responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string()
                    : null;
                switch (responseCode) {
                  case SUCCESS_RESPONSE:
                    if (view != null) {
                      view.onHideProgress();
                    }
                      GuestLoginResponse loginResponse = gson.fromJson(responseBody,
                        GuestLoginResponse.class);
                    GuestLoginData loginData = loginResponse.getData();
                    String auth = loginData.getToken().getAccessToken();
                    String refreshtoken = loginData.getToken().getRefreshToken();
                    String sid = loginData.getSid();
                    LSPApplication.getInstance().setAuthToken(sid, sid, auth);
                    sessionManager.setREFRESHAUTH(refreshtoken);
                    sessionManager.setSID(sid);
                    sessionManager.setEmail(mDeviceId);
                    sessionManager.setGuestLogin(true);
                    if (view != null) {
                      view.loginSuccess(auth, mDeviceId);
                    }
                    break;
                  default:
                    if (errorBody != null && !errorBody.isEmpty()) {
                      errJson = new JSONObject(errorBody);
                      errJson.getString(MESSAGE);
                      if (!errJson.getString(MESSAGE).isEmpty()) {
                        if(view != null) {
                          view.onError(errJson.getString(MESSAGE));
                        }
                      }
                    }
                    break;
                }
              } catch (Exception e) {
                if(view != null) {
                  view.onHideProgress();
                  view.showToast(mContext.getString(R.string.networkProblem));
                }
                e.printStackTrace();
              }
            }

            @Override
            public void onError(Throwable e) {
              if(view != null) {
                view.onHideProgress();
                view.showToast(mContext.getString(R.string.networkProblem));
              }
            }

            @Override
            public void onComplete() {
            }
          });
    } else {
      if (view != null) {
        view.onHideProgress();
        view.showToast(mContext.getString(R.string.networkProblem));
      }
    }
  }

  @Override
  public void onLanguageCalled() {
    if(Constants.selLang.equals("en")) {
      sessionManager.setLanguageSettings(new LanguagesList("en", "English", 0));
    } else if(Constants.selLang.equals("pt")) {
      sessionManager.setLanguageSettings(new LanguagesList("pt", "Portuguese", 0));
    }
    lspServices = BasicAuthentication.ServiceGenerator.createService(LSPServices.class,
        BASE_AUTH_USERNAME, BASE_AUTH_PASSWORD);
    Observable<Response<ResponseBody>> languageResponse = lspServices.onLanguageBasicCalled(selLang, PLATFORM_ANDROID);
    languageResponse.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int responseCode = responseBodyResponse.code();
            JSONObject errJson;
            try {
              String responseBody =
                  responseBodyResponse.body() != null ? responseBodyResponse.body().string()
                      : null;
              String errorBody =
                  responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string()
                      : null;
              switch (responseCode) {
                case SUCCESS_RESPONSE:
                  LanguageResponse languageResponses = gson.fromJson(responseBody,
                      LanguageResponse.class);
                  boolean isLanguage = false;
                  for (int i = 0; i < languageResponses.getLanguagesLists().size(); i++) {
                    if (sessionManager.getLanguageSettings().getCode().equals(
                        languageResponses.getLanguagesLists().get(i).getCode())) {
                      isLanguage = true;
                      view.showLanguagesDialog(languageResponses.getLanguagesLists().indexOf(
                          languageResponses.getLanguagesLists().get(i)),
                          languageResponses.getLanguagesLists());
                      break;
                    }
                  }
                  if (!isLanguage) {
                    view.showLanguagesDialog(-1, languageResponses.getLanguagesLists());
                  }
                  break;
                default:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    errJson = new JSONObject(errorBody);
                    if (errJson.getString(MESSAGE) != null && !errJson.getString(MESSAGE).isEmpty()) {
                      view.onError(errJson.getString(MESSAGE));
                    }
                  }
                  break;
              }
            } catch (JSONException | IOException ex) {
              ex.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {

          }

          @Override
          public void onComplete() {
          }
        });
  }

  @Override
  public void changeLanguage(String langCode, String langName, int direction) {
    selLang = langCode;
    sessionManager.setLanguageSettings(new LanguagesList(langCode, langName, direction));
    view.setLanguage(langName, true);
  }




  /**
   * This method is used to change screen content on view pager page changed through it's updated position
   *
   * @param sliderList
   * @param position int position of view pager item
   */
  @Override
  public void changeSliderData(ArrayList<SliderData> sliderList, int position) {
    view.setSliderContent(getSliderData(sliderList,position));
  }


  /**
   * It's used to get SliderData object from the sliderList at particular position
   *
   *
   * @param sliderList
   * @param position int position of list
   * @return SliderData object
   */
  public SliderData getSliderData(ArrayList<SliderData> sliderList, int position) {
    return (sliderList.size() >= position) ? sliderList.get(position) : null;
  }
  @Override
  public void attachView(Object view) {
  }

  @Override
  public void detachView() {
  }
}
