package com.vaidg.intro;

import static com.vaidg.utilities.Constants.H_480;
import static com.vaidg.utilities.Constants.H_5;
import static com.vaidg.utilities.Constants.H_50;
import static com.vaidg.utilities.Constants.W_10;
import static com.vaidg.utilities.Constants.W_15;
import static com.vaidg.utilities.Constants.W_155;
import static com.vaidg.utilities.Constants.W_22;
import static com.vaidg.utilities.Constants.W_320;
import static com.vaidg.utilities.Constants.W_40;
import static com.vaidg.utilities.Utility.isNetworkAvailable;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.viewpager2.widget.ViewPager2;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.vaidg.Login.LoginActivity;
import com.vaidg.R;
import com.vaidg.home.MainActivity;
import com.vaidg.intro.model.IntroSliderAdapter;
import com.vaidg.intro.model.SliderData;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.pojo.LanguageResponse;
import com.utility.AlertProgress;
import dagger.android.support.DaggerAppCompatActivity;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

/**
 * <h2>IntroActivity</h2>
 * <p>
 * This class is used to provide a login or guest mode to customer.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class IntroActivity extends DaggerAppCompatActivity implements IntroActivityContract.IntroView {
  @BindView(R.id.btnLogin)
  Button btnLogin;
  @BindView(R.id.btnGuestLogin)
  Button btnGuestLogin;
  @BindView(R.id.tvDescription)
  TextView tvDescription;
  @BindView(R.id.ll_btn)
  LinearLayout ll_btn;
  @BindView(R.id.tvTitle)
  TextView tvTitle;
  @BindView(R.id.ivImage)
  AppCompatImageView ivImage;
  @BindString(R.string.waitGuestLogin)
  String waitGuestLogin;
  @BindView(R.id.tvLandingLanguages)
  AppCompatTextView tvLandingLanguages;
  @BindView(R.id.viewpager_intro_slider)
  ViewPager2 viewpagerIntroSlider;
  @Inject
  IntroActivityContract.IntroPresenter introPresenter;
  @Inject
  AlertProgress alertProgress;
  @Inject
  AppTypeface appTypeface;
  private Context mContext;
  private ProgressDialog pDialog;
  IntroSliderAdapter introSliderAdapter;
  private int mSliderCurrentPage = 0;
  private Timer sliderTimer;
  ArrayList<SliderData> sliderList = new ArrayList<>();
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_intro);
    ButterKnife.bind(this);
    Utility.statusbarColor(this);
    mContext = this;
    initialize();
    calculate();
  }

  private void calculate() {
    int w10 = Utility.getScreenWidth() * W_10 / W_320;
    int h50 = Utility.getScreenHeight() * H_50 / H_480;
    int w15 = Utility.getScreenWidth() * W_15 / W_320;
    int h5 = Utility.getScreenHeight() * H_5 / H_480;
    int w22 = Utility.getScreenWidth() * W_40 / W_320;
    int w155 = Utility.getScreenWidth() * W_155 / W_320;
    tvTitle.setPadding(w10, h50, w10, h5);
    tvDescription.setPadding(w15, h5, w15, h5);
    ll_btn.setPadding(0, h5, 0, h5);
    btnLogin.getLayoutParams().width = w155;
    btnLogin.getLayoutParams().height = w22;
    btnGuestLogin.getLayoutParams().width = w155;
    btnGuestLogin.getLayoutParams().height = w22;
  }

  /**
   * <h2>initialize View</h2>
   * <p>this is the method, for initialize the views</p>
   */
  private void initialize() {
    btnLogin.startAnimation(Utility.animateFtomLeft(mContext));
    btnGuestLogin.startAnimation(Utility.animateFtomRight(mContext));
    tvDescription.setTypeface(appTypeface.getHind_regular());
    tvTitle.setTypeface(appTypeface.getHind_semiBold());
    btnLogin.setTypeface(appTypeface.getHind_semiBold());
    btnGuestLogin.setTypeface(appTypeface.getHind_semiBold());
    setupSlider();
  }

  @OnClick({R.id.btnLogin, R.id.btnGuestLogin, R.id.tvLandingLanguages})
  public void onClickEvent(View view) {
    Intent intent;
    switch (view.getId()) {
      case R.id.btnLogin:
        intent = new Intent(mContext, LoginActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        break;
      case R.id.btnGuestLogin:
        pDialog = alertProgress.getProcessDialog(this);
        pDialog.setMessage(waitGuestLogin);
        pDialog.setCancelable(false);
        introPresenter.doGuestLogin();
        break;
      case R.id.tvLandingLanguages:
        introPresenter.onLanguageCalled();
        break;
    }
  }

  @Override
  public void showToast(String errorMsg) {
    Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void loginSuccess(String auth, String deviceId) {
    Intent intent = new Intent(mContext, MainActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
    finish();
  }

  @Override
  public void setLanguage(String language, boolean restart) {
    tvLandingLanguages.setText(language);
    recreate();
/*
    if (restart) {
      Intent intent = new Intent(mContext, IntroActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
      startActivity(intent);
      Runtime.getRuntime().exit(0);
    }
*/
  }

  @Override
  public void showLanguagesDialog(int index, ArrayList<LanguageResponse.LanguagesLists> languagesLists) {
    alertProgress.showLanguagesAlert((Activity) mContext, languagesLists, introPresenter, index);
  }

  @Override
  public void onSessionExpired() {
  }

  @Override
  public void onLogout(String message, SessionManagerImpl sessionManager) {
  }

  @Override
  public void onError(String msg) {
    alertProgress.alertinfo(mContext, msg);
  }

  @Override
  public void onConnectionError(String connectionError) {
  }

  @Override
  public void onShowProgress() {
    try {
      if (pDialog != null && !pDialog.isShowing() && !isFinishing()) {
        pDialog.show();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void onHideProgress() {
    if (pDialog != null && !isFinishing() && pDialog.isShowing()) {
      pDialog.dismiss();
    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    sliderTimer.cancel();
  }

    @Override
  protected void onResume() {
    super.onResume();
    if(Constants.selLang.equals("en")) {
      tvLandingLanguages.setText("English");
    } else if(Constants.selLang.equals("pt"))
      tvLandingLanguages.setText("Portuguese");
  }

  @Override
  public void setupSlider() {
    addSliderData();
    introSliderAdapter = new IntroSliderAdapter(sliderList);
    viewpagerIntroSlider.setAdapter(introSliderAdapter);
    viewpagerIntroSlider.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
      @Override
      public void onPageSelected(int position) {
        super.onPageSelected(position);
        introPresenter.changeSliderData(sliderList,position);
      }
    });

    Handler sliderHandler = new Handler();

    Runnable sliderRunnable = () -> {
      if (mSliderCurrentPage == introSliderAdapter.getItemCount()) {
        mSliderCurrentPage = 0;
      }

      viewpagerIntroSlider.setCurrentItem(mSliderCurrentPage++, true);
    };

    sliderTimer = new Timer();
    sliderTimer.schedule(new TimerTask() {
      @Override
      public void run() {
        sliderHandler.post(sliderRunnable);
      }
    }, 3000, 3000);
  }

  @Override
  public void setSliderContent(SliderData sliderData) {
    if (sliderData != null) {
      tvTitle.setText(sliderData.getTitle());
      tvDescription.setText(sliderData.getDescription());
    }
  }

  @Override
  public void addSliderData() {
    addSliderToAdapter(new SliderData(R.drawable.intro_slider_img1, R.string.intro_slider_title_1, R.string.intro_slider_description_1));
    addSliderToAdapter(new SliderData(R.drawable.intro_slider_img2, R.string.intro_slider_title_2, R.string.intro_slider_description_2));
    addSliderToAdapter(new SliderData(R.drawable.intro_slider_img3, R.string.intro_slider_title_3, R.string.intro_slider_description_3));
    addSliderToAdapter(new SliderData(R.drawable.intro_slider_img4, R.string.intro_slider_title_4, R.string.intro_slider_description_4));
  }

  /**
   * It's used to add SliderData object into the sliderList
   *
   * @param sliderData object of SliderData
   */
  public void addSliderToAdapter(SliderData sliderData) {
    sliderList.add(sliderData);
  }
}