package com.vaidg.intro;

import com.vaidg.Dagger2.ActivityScoped;
import dagger.Binds;
import dagger.Module;

/**
 * <h2>IntroModule</h2>
 * <p>This class is used to provide the IntroActivityPresenter object</p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 12-07-2019.
 */
@Module
public abstract class IntroModule {
  @ActivityScoped
  @Binds
  abstract IntroActivityContract.IntroPresenter introPresenter(IntroActivityPresenter introActivityPresenter);

  @ActivityScoped
  @Binds
  abstract IntroActivityContract.IntroView introView(IntroActivity introActivity);

}
