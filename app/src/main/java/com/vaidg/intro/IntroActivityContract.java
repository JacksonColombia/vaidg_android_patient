package com.vaidg.intro;

import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;
import com.pojo.LanguageResponse;
import com.vaidg.intro.model.SliderData;

import java.util.ArrayList;

/**
 * <h2>IntroActivityContract</h2>
 * <p>This interface is used to communicate between View and Model</p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 12-07-2019.
 */
public interface IntroActivityContract {
  interface IntroView extends BaseView {
    /**
     * <h2>setupSlider</h2>
     */
    void setupSlider();

    /**
     * <h2>setSliderContent</h2>
     */
    void setSliderContent(SliderData sliderData);
    /**
     * <h2>showToast</h2>
     * <p>This method is used to show the toast with message</p>
     *
     * @param errorMsg message to be shown on toast
     */
    void showToast(String errorMsg);

    /**
     * <h2>loginSuccess</h2>
     * <p>This method is called on success of the guest login API and used to Navigate to HomeScreen</p>
     *
     * @param auth     Auth token
     * @param deviceId device id of a phone
     */
    void loginSuccess(String auth, String deviceId);

    /**
     * <h2>onError</h2>
     * <p>This method is used to show the error message if the Login API call fails and gives error from server</p>
     *
     * @param msg Message to show via TOAST
     */
    void onError(String msg);

    /**
     * <h2>onError</h2>
     * <p>This method is used to set the language</p>
     *
     * @param langName text for language
     */
    void setLanguage(String langName, boolean b);

    /**
     * <h2>onError</h2>
     * <p>This method is used show list of language avialable</p>
     *
     * @param i              position of langugage in list
     * @param languagesLists list for language
     */
    void showLanguagesDialog(int i, ArrayList<LanguageResponse.LanguagesLists> languagesLists);

    /**
     * <h2>addSliderData</h2>
     * <p>This method is add objects of SliderData into adapter</p>
     *
     */
    void addSliderData();
  }

  interface IntroPresenter extends BasePresenter {
    /**
     * <h2>loginSuccess</h2>
     * <p>This method is called on success of the guest login API and used to Navigate to HomeScreen</p>
     */
    void doGuestLogin();

    /**
     * <h2>onLanguageCalled</h2>
     * <p>This method is called on to cancel the language selection</p>
     */
    void onLanguageCalled();

    /**
     * <h2>changeLanguage</h2>
     * <p>This method is called on to change the language from list</p>
     *
     * @param langCode  language code
     * @param langName  name of language
     * @param direction rtl or ltr direction
     */
    void changeLanguage(String langCode, String langName, int direction);

    /**
     * <h2>changeSliderData</h2>
     * <p>This method is used to change screen content on view pager page changed through it's updated position</p>
     *
     * @param sliderList
     * @param position  int position of view pager item
     */
    void changeSliderData(ArrayList<SliderData> sliderList, int position);

  }
}
