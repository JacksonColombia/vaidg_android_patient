package com.vaidg.prescription;

import com.vaidg.Dagger2.ActivityScoped;
import com.vaidg.incalloutcalltelecall.dependent.DependentActivity;
import com.vaidg.incalloutcalltelecall.dependent.DependentContract;
import com.vaidg.incalloutcalltelecall.dependent.DependentPresenterImpl;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>FilterDependentModule</h2>
 * <p>
 * This is a Dagger module. We use this to pass in the View dependency to the
 * {@link PrescriptionPresenterImpl}.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
@Module
public interface FilterDependentModule {
}