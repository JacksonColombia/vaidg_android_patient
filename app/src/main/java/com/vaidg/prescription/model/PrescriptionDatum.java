package com.vaidg.prescription.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pojo.InvoiceDetails;

import java.util.ArrayList;

/**
 * <h2>PrescriptionDatum</h2>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class PrescriptionDatum implements Parcelable
{

    @SerializedName("bookingRequestedFor")
    @Expose
    private long bookingRequestedFor;
    @SerializedName("providerTitle")
    @Expose
    private String providerTitle;
    @SerializedName("providerFirstName")
    @Expose
    private String providerFirstName;
    @SerializedName("providerLastName")
    @Expose
    private String providerLastName;
    @SerializedName("providerProfilePic")
    @Expose
    private String providerProfilePic;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("dependentId")
    @Expose
    private String dependentId;
    @SerializedName("pdfFile")
    @Expose
    private String pdfFile;
    @SerializedName("degree")
    @Expose
    private String degree;
    @SerializedName("dependentData")
    @Expose
    private DependentData dependentData;
    @SerializedName("medication")
    @Expose
    private ArrayList<InvoiceDetails.Medication> medication = new ArrayList<>();
    public final static Parcelable.Creator<PrescriptionDatum> CREATOR = new Creator<PrescriptionDatum>() {


        @SuppressWarnings({"unchecked"})
        public PrescriptionDatum createFromParcel(Parcel in) {
            return new PrescriptionDatum(in);
        }

        public PrescriptionDatum[] newArray(int size) {
            return (new PrescriptionDatum[size]);
        }

    }
            ;

    protected PrescriptionDatum(Parcel in) {
        this.bookingRequestedFor = ((int) in.readValue((long.class.getClassLoader())));
        this.providerTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.providerFirstName = ((String) in.readValue((String.class.getClassLoader())));
        this.providerLastName = ((String) in.readValue((String.class.getClassLoader())));
        this.providerProfilePic = ((String) in.readValue((String.class.getClassLoader())));
        this.categoryName = ((String) in.readValue((String.class.getClassLoader())));
        this.dependentId = ((String) in.readValue((String.class.getClassLoader())));
        this.pdfFile = ((String) in.readValue((String.class.getClassLoader())));
        this.degree = ((String) in.readValue((String.class.getClassLoader())));
        this.dependentData = ((DependentData) in.readValue((DependentData.class.getClassLoader())));
        in.readList(this.medication, (InvoiceDetails.Medication.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public PrescriptionDatum() {
    }

    /**
     *
     * @param dependentId
     * @param pdfFile
     * @param degree
     * @param providerLastName
     * @param providerProfilePic
     * @param bookingRequestedFor
     * @param providerFirstName
     * @param medication
     * @param categoryName
     * @param dependentData
     * @param providerTitle
     */
    public PrescriptionDatum(int bookingRequestedFor, String providerTitle, String providerFirstName, String providerLastName, String providerProfilePic, String categoryName, String dependentId, String pdfFile, String degree, DependentData dependentData, ArrayList<InvoiceDetails.Medication> medication) {
        super();
        this.bookingRequestedFor = bookingRequestedFor;
        this.providerTitle = providerTitle;
        this.providerFirstName = providerFirstName;
        this.providerLastName = providerLastName;
        this.providerProfilePic = providerProfilePic;
        this.categoryName = categoryName;
        this.dependentId = dependentId;
        this.pdfFile = pdfFile;
        this.degree = degree;
        this.dependentData = dependentData;
        this.medication = medication;
    }

    public long getBookingRequestedFor() {
        return bookingRequestedFor;
    }

    public void setBookingRequestedFor(int bookingRequestedFor) {
        this.bookingRequestedFor = bookingRequestedFor;
    }

    public String getProviderTitle() {
        return providerTitle;
    }

    public void setProviderTitle(String providerTitle) {
        this.providerTitle = providerTitle;
    }

    public String getProviderFirstName() {
        return providerFirstName;
    }

    public void setProviderFirstName(String providerFirstName) {
        this.providerFirstName = providerFirstName;
    }

    public String getProviderLastName() {
        return providerLastName;
    }

    public void setProviderLastName(String providerLastName) {
        this.providerLastName = providerLastName;
    }

    public String getProviderProfilePic() {
        return providerProfilePic;
    }

    public void setProviderProfilePic(String providerProfilePic) {
        this.providerProfilePic = providerProfilePic;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDependentId() {
        return dependentId;
    }

    public void setDependentId(String dependentId) {
        this.dependentId = dependentId;
    }

    public DependentData getDependentData() {
        return dependentData;
    }

    public void setDependentData(DependentData dependentData) {
        this.dependentData = dependentData;
    }

    public ArrayList<InvoiceDetails.Medication> getMedication() {
        return medication;
    }

    public void setMedication(ArrayList<InvoiceDetails.Medication> medication) {
        this.medication = medication;
    }

    public String getPdfFile() {
        return pdfFile;
    }

    public void setPdfFile(String pdfFile) {
        this.pdfFile = pdfFile;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(bookingRequestedFor);
        dest.writeValue(providerTitle);
        dest.writeValue(providerFirstName);
        dest.writeValue(providerLastName);
        dest.writeValue(providerProfilePic);
        dest.writeValue(categoryName);
        dest.writeValue(dependentId);
        dest.writeValue(pdfFile);
        dest.writeValue(degree);
        dest.writeValue(dependentData);
        dest.writeList(medication);
    }

    public int describeContents() {
        return 0;
    }

}