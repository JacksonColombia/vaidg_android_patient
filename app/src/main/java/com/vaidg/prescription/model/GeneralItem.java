package com.vaidg.prescription.model;

public class GeneralItem extends ListItem {
    private PrescriptionDatum pojoOfJsonArray;

    public PrescriptionDatum getPojoOfJsonArray() {
        return pojoOfJsonArray;
    }

    public void setPojoOfJsonArray(PrescriptionDatum pojoOfJsonArray) {
        this.pojoOfJsonArray = pojoOfJsonArray;
    }

    @Override
    public int getType() {
        return TYPE_GENERAL;
    }


}
