package com.vaidg.prescription.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h2>Medication</h2>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class Medication implements Parcelable
{

    @SerializedName("compoundName")
    @Expose
    private String compoundName;
    @SerializedName("drugStrength")
    @Expose
    private String drugStrength;
    @SerializedName("drugUnit")
    @Expose
    private String drugUnit;
    @SerializedName("frequency")
    @Expose
    private String frequency;
    @SerializedName("drugRoute")
    @Expose
    private String drugRoute;
    @SerializedName("direction")
    @Expose
    private String direction;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("instruction")
    @Expose
    private String instruction;
    public final static Parcelable.Creator<Medication> CREATOR = new Creator<Medication>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Medication createFromParcel(Parcel in) {
            return new Medication(in);
        }

        public Medication[] newArray(int size) {
            return (new Medication[size]);
        }

    };

    protected Medication(Parcel in) {
        this.compoundName = ((String) in.readValue((String.class.getClassLoader())));
        this.drugStrength = ((String) in.readValue((String.class.getClassLoader())));
        this.drugUnit = ((String) in.readValue((String.class.getClassLoader())));
        this.frequency = ((String) in.readValue((String.class.getClassLoader())));
        this.drugRoute = ((String) in.readValue((String.class.getClassLoader())));
        this.direction = ((String) in.readValue((String.class.getClassLoader())));
        this.duration = ((String) in.readValue((String.class.getClassLoader())));
        this.instruction = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public Medication() {
    }

    /**
     *
     * @param duration
     * @param drugUnit
     * @param drugStrength
     * @param instruction
     * @param drugRoute
     * @param compoundName
     * @param frequency
     * @param direction
     */
    public Medication(String compoundName, String drugStrength, String drugUnit, String frequency, String drugRoute, String direction, String duration, String instruction) {
        super();
        this.compoundName = compoundName;
        this.drugStrength = drugStrength;
        this.drugUnit = drugUnit;
        this.frequency = frequency;
        this.drugRoute = drugRoute;
        this.direction = direction;
        this.duration = duration;
        this.instruction = instruction;
    }

    public String getCompoundName() {
        return compoundName;
    }

    public void setCompoundName(String compoundName) {
        this.compoundName = compoundName;
    }

    public String getDrugStrength() {
        return drugStrength;
    }

    public void setDrugStrength(String drugStrength) {
        this.drugStrength = drugStrength;
    }

    public String getDrugUnit() {
        return drugUnit;
    }

    public void setDrugUnit(String drugUnit) {
        this.drugUnit = drugUnit;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getDrugRoute() {
        return drugRoute;
    }

    public void setDrugRoute(String drugRoute) {
        this.drugRoute = drugRoute;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(compoundName);
        dest.writeValue(drugStrength);
        dest.writeValue(drugUnit);
        dest.writeValue(frequency);
        dest.writeValue(drugRoute);
        dest.writeValue(direction);
        dest.writeValue(duration);
        dest.writeValue(instruction);
    }

    public int describeContents() {
        return 0;
    }

}