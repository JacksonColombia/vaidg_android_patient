package com.vaidg.prescription.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * <h2>PrescriptionData</h2>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class PrescriptionData implements Parcelable
{

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<PrescriptionDatum> data = new ArrayList<>();
    public final static Parcelable.Creator<PrescriptionData> CREATOR =
            new Creator<PrescriptionData>() {


        @SuppressWarnings({"unchecked"})
        public PrescriptionData createFromParcel(Parcel in) {
            return new PrescriptionData(in);
        }

        public PrescriptionData[] newArray(int size) {
            return (new PrescriptionData[size]);
        }

    };

    protected PrescriptionData(Parcel in) {
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.data, (PrescriptionDatum.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public PrescriptionData() {
    }

    /**
     *
     * @param data
     * @param message
     */
    public PrescriptionData(String message, ArrayList<PrescriptionDatum> data) {
        super();
        this.message = message;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<PrescriptionDatum> getData() {
        return data;
    }

    public void setData(ArrayList<PrescriptionDatum> data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeList(data);
    }

    public int describeContents() {
        return 0;
    }

}