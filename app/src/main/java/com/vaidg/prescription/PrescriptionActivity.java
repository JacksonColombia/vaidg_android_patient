package com.vaidg.prescription;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.utility.AlertProgress;
import com.utility.PicassoTrustAll;
import com.vaidg.R;
import com.vaidg.databinding.ActivityPrescriptionBinding;
import com.vaidg.databinding.LayoutFilterdependentBinding;
import com.vaidg.incalloutcalltelecall.dependent.model.DependentsData;
import com.vaidg.prescription.adapter.FilterDependentAdapter;
import com.vaidg.prescription.adapter.PopupWindowHelper;
import com.vaidg.prescription.adapter.PrescriptionListAdapter;
import com.vaidg.prescription.filterdate.FilterDateActivity;
import com.vaidg.prescription.filterdate.model.FilterDateListPojo;
import com.vaidg.prescription.model.DateItem;
import com.vaidg.prescription.model.GeneralItem;
import com.vaidg.prescription.model.ListItem;
import com.vaidg.prescription.model.PrescriptionDatum;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

import static com.vaidg.utilities.Constants.PRESCRIPTIONCREEN.EXTRA_DEPENDENT_ID;
import static com.vaidg.utilities.Constants.PRESCRIPTIONCREEN.EXTRA_FILTER_DATA_POJO;
import static com.vaidg.utilities.Constants.PRESCRIPTIONCREEN.EXTRA_FILTER_DATE_NAME;
import static com.vaidg.utilities.Constants.PRESCRIPTIONCREEN.FILTERDEP_RESULT_CODE;
import static com.vaidg.utilities.Utility.isNetworkAvailable;
import static com.vaidg.utilities.Utility.isTextEmpty;
import static com.vaidg.utilities.Utility.setMAnagerWithBID;
import static com.vaidg.utilities.Utility.setTextSize12Sp;
import static com.vaidg.utilities.Utility.setTextSize14Sp;
import static com.vaidg.utilities.Utility.setTextSize16Sp;

/**
 * <h2>PrescriptionActivity</h2>
 * <p>
 * Prescription page where it will show the list of prescription.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class PrescriptionActivity extends DaggerAppCompatActivity implements PrescriptionContract
        .PrescriptionView, AdapterView.OnItemSelectedListener {

    public final String TAG = PrescriptionActivity.class.getSimpleName();
    private final String mFrom = "", mTo = "";
    private final double mLimit = 50, mSkip = 0;
    @Inject
    PrescriptionContract.PrescriptionPresent presenter;
    private String mDependentId = "", mFilterBaseOnDate = "", mDependentName = "";
    private ArrayList<FilterDateListPojo> mFilterDateListPojoArrayList;
    private ArrayList<ListItem> mPrescriptionDatumArrayList;
    private Context mContext;
    @Inject
    AlertProgress alertProgress;
    @Inject
    AppTypeface appTypeface;
    private PrescriptionListAdapter mPrescriptionListAdapter;
    private ArrayList<DependentsData> mDependentsDataArrayList;
    private PopupWindowHelper mPopupWindowHelper;
    private LayoutFilterdependentBinding popupview;
    private FilterDependentAdapter mDependentAdapter;
    private ActivityPrescriptionBinding binding;
    private ProgressDialog pDialog;
    private ArrayList<ListItem> consolidatedList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPrescriptionBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        mContext = this;
        popupview = LayoutFilterdependentBinding.inflate(LayoutInflater.from(mContext),
                (ViewGroup) null, false);
        toolBar();
        initialize();
    }

    /**
     * <h2>initialize</h2>
     * <p> method to initialize the views</p>
     */
    private void initialize() {
        mPrescriptionDatumArrayList = new ArrayList<>();
        mFilterDateListPojoArrayList = new ArrayList<>();
        mDependentsDataArrayList = new ArrayList<>();

        if (isNetworkAvailable(mContext)) {
            presenter.getPrescriptionList(mDependentId, mFrom, mTo, mFilterBaseOnDate, mLimit,
                    mSkip);
            presenter.getDependents(mContext);
        }

        binding.rvPrescription.setHasFixedSize(true);
        binding.rvPrescription.setLayoutManager(new LinearLayoutManager(mContext));
        mPrescriptionListAdapter = new PrescriptionListAdapter(mPrescriptionDatumArrayList);
        binding.rvPrescription.setAdapter(mPrescriptionListAdapter);

        popupview.clSelectAll.setOnClickListener(v -> {
            if (popupview.tvSelectAll.isSelected()) {
                popupview.tvSelectAll.setSelected(false);
                popupview.tvSelectAll.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        R.drawable.ic_unselected, 0);
                mDependentName = mContext.getResources().getString(R.string.selectDependent);
                mDependentId = "";
            } else {
                popupview.tvSelectAll.setSelected(true);
                popupview.tvSelectAll.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        R.drawable.ic_check, 0);
                mDependentName = mContext.getResources().getString(R.string.all);
                mDependentId = "";
                popupview.tvDependentName.setSelected(false);
                if (mDependentsDataArrayList != null && mDependentsDataArrayList.size() > 0) {
                    for (int i = 0; i < mDependentsDataArrayList.size(); i++) {
                        mDependentsDataArrayList.get(i).setChecked(false);
                    }
                    mDependentAdapter.notifyDataSetChanged();
                }
            }
            if (popupview.tvDependentName.isSelected()) {
                popupview.tvDependentName.setSelected(false);
                popupview.tvDependentName.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        R.drawable.ic_unselected, 0);
            } else {
                popupview.tvDependentName.setSelected(true);
                popupview.tvDependentName.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        R.drawable.ic_check, 0);
            }
            if (mDependentsDataArrayList != null && mDependentsDataArrayList.size() > 0) {
                for (int i = 0; i < mDependentsDataArrayList.size(); i++) {
                    mDependentsDataArrayList.get(i)
                            .setChecked(!mDependentsDataArrayList.get(i).isChecked());
                }
                mDependentAdapter.notifyDataSetChanged();
            }
            selectDependent(mDependentId, mDependentName);
        });
        popupview.clSelectSelf.setOnClickListener(v -> {
            if (popupview.tvSelectAll.isSelected()) {
                popupview.tvSelectAll.setSelected(false);
                popupview.tvDependentName.setSelected(false);
                popupview.tvSelectAll.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        R.drawable.ic_unselected, 0);
            }
            if (popupview.tvDependentName.isSelected()) {
                popupview.tvDependentName.setSelected(false);
                popupview.tvDependentName.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        R.drawable.ic_unselected, 0);
                mDependentName = mContext.getResources().getString(R.string.selectDependent);
                mDependentId = "";
            } else {
                popupview.tvDependentName.setSelected(true);
                popupview.tvDependentName.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        R.drawable.ic_check, 0);
                mDependentId = "1";
                mDependentName = presenter.getFirstName() + " " + presenter.getLastName();
            }
            if (mDependentsDataArrayList != null && mDependentsDataArrayList.size() > 0) {
                for (int i = 0; i < mDependentsDataArrayList.size(); i++) {
                    mDependentsDataArrayList.get(i).setChecked(false);
                }
                mDependentAdapter.notifyDataSetChanged();
            }
            selectDependent(mDependentId, mDependentName);
        });
        mDependentAdapter = new FilterDependentAdapter(mDependentsDataArrayList,
                (position, dependentsDataArrayList, dependentsData, tvdependentName,
                 tvDependentRelation, ivDependentProfilePic) -> {
                    if (popupview.tvSelectAll.isSelected()) {
                        popupview.tvSelectAll.setSelected(false);
                        popupview.tvSelectAll.setCompoundDrawablesWithIntrinsicBounds(0,
                                0, R.drawable.ic_unselected, 0);

                    }
                    if (popupview.tvDependentName.isSelected()) {
                        popupview.tvDependentName.setSelected(false);
                        popupview.tvDependentName.setCompoundDrawablesWithIntrinsicBounds(0,
                                0, R.drawable.ic_unselected, 0);
                    }
                   // dependentsData.setChecked(!dependentsData.isChecked());
                    if (dependentsDataArrayList != null && dependentsDataArrayList.size() > 0) {
                        for (int i = 0; i < dependentsDataArrayList.size(); i++) {
                            if(i != position) {
                                dependentsDataArrayList.get(i).setChecked(false);
                            }else{
                                dependentsDataArrayList.get(i).setChecked(true);
                                dependentsData.setChecked(true);
                            }
                        }
                    }
                    if (dependentsData.isChecked()) {
                        mDependentId = dependentsDataArrayList.get(position).getDependentId();
                        mDependentName = dependentsDataArrayList.get(position).getFirstName() + " "
                                + dependentsDataArrayList.get(position).getLastName();
                        dependentsData.setChecked(true);
                        tvdependentName.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                                R.drawable.ic_check, 0);
                    } else {
                        mDependentId = "";
                        mDependentName = mContext.getResources().getString(R.string.selectDependent);
                        dependentsData.setChecked(false);
                        tvdependentName.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                                R.drawable.ic_unselected, 0);
                    }
                    mDependentsDataArrayList = dependentsDataArrayList;
                    mDependentAdapter.notifyDataSetChanged();
                    selectDependent(mDependentId, mDependentName);
                });
        popupview.rvDependents.setHasFixedSize(true);
        popupview.rvDependents.setLayoutManager(new LinearLayoutManager(mContext));
        popupview.rvDependents.setAdapter(mDependentAdapter);
        binding.tvPopUp.setOnClickListener(v -> {
            if (mDependentsDataArrayList.size() > 0) {
                binding.tvPopUp.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                        R.drawable.ic_keyboard_arrow_up_black_24dp, 0);
                mPopupWindowHelper = new PopupWindowHelper(popupview.getRoot());
                mPopupWindowHelper.showAsDropDown(v, 0, 0);
            }
        });
    }

    /**
     * <h2>initialize</h2>
     * <p> method to initialize the toolbar</p>
     */
    private void toolBar() {
        setSupportActionBar(binding.toolbarLayout.toolbar);
        binding.toolbarLayout.toolbar.setElevation(0f);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0f);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.toolbarLayout.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
        binding.toolbarLayout.tvCenter.setTypeface(appTypeface.getHind_semiBold());
        popupview.tvDependentRelation.setTypeface(appTypeface.getHind_regular());
        popupview.tvDependentName.setTypeface(appTypeface.getHind_medium());
        binding.tvPopUp.setTypeface(appTypeface.getHind_medium());
        popupview.tvSelectAll.setTypeface(appTypeface.getHind_medium());
        setTextSize14Sp(mContext, popupview.tvDependentName);
        setTextSize14Sp(mContext, popupview.tvSelectAll);
        setTextSize14Sp(mContext, binding.tvPopUp);
        setTextSize12Sp(mContext, popupview.tvDependentRelation);
        setTextSize16Sp(mContext, binding.toolbarLayout.tvCenter);

        binding.toolbarLayout.tvCenter.setText(new StringBuilder().append(mContext.getResources().getString(R.string.my)).append(" ")
                .append(mContext.getResources().getString(R.string.prescriptions)));
        binding.tvFilter.setOnClickListener(v -> {
            Intent intent = new Intent(this, FilterDateActivity.class);
            intent.putExtra(EXTRA_FILTER_DATA_POJO, mFilterDateListPojoArrayList);
            intent.putExtra(EXTRA_DEPENDENT_ID, mDependentId);
            startActivityForResult(intent, FILTERDEP_RESULT_CODE);
            overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
        });
    }

    @Override
    public void onSessionExpired() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeActivity();
    }

    /**
     * <h2>closeActivity</h2>
     * <p> method for closing current page</p>
     */
    private void closeActivity() {
        finish();
        overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
    }

    @Override
    public void onLogout(String msg, SessionManagerImpl sessionManager) {
        if (alertProgress != null)
            alertProgress.alertPositiveOnclick(mContext, msg, mContext.getResources().getString(R.string.logout),
                    mContext.getResources().getString(R.string.ok), isClicked -> setMAnagerWithBID(mContext, sessionManager));
    }

    @Override
    public void onError(String msg) {
        if (alertProgress != null) alertProgress.alertinfo(mContext, msg);
    }

    @Override
    public void onConnectionError(String connectionError) {

    }

    @Override
    public void onShowProgress() {
        pDialog = alertProgress.getProcessDialog(this);
        pDialog.setMessage(mContext.getResources().getString(R.string.waitFetchPrescription));
        pDialog.setCancelable(false);
        try {
            if (pDialog != null && !pDialog.isShowing() && !isFinishing()) pDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onHideProgress() {
        if (pDialog != null && !isFinishing() && pDialog.isShowing()) pDialog.dismiss();
    }

    @Override
    public void onSuccess(ArrayList<PrescriptionDatum> prescriptionDatum) {
        binding.clNoPrescriptionAvailable.setVisibility(View.GONE);
        binding.nlPrecription.setVisibility(View.VISIBLE);
        mPrescriptionDatumArrayList.clear();
      /*  mPrescriptionDatumArrayList.addAll(prescriptionDatum);*/

        HashMap<String, ArrayList<PrescriptionDatum>> groupedHashMap = groupDataIntoHashMap(prescriptionDatum);


        for (String date : groupedHashMap.keySet()) {
            DateItem dateItem = new DateItem();
            dateItem.setDate(date);
            mPrescriptionDatumArrayList.add(dateItem);


            for (PrescriptionDatum pojoOfJsonArray : groupedHashMap.get(date)) {
                GeneralItem generalItem = new GeneralItem();
                generalItem.setPojoOfJsonArray(pojoOfJsonArray);//setBookingDataTabs(bookingDataTabs);
                mPrescriptionDatumArrayList.add(generalItem);
            }
        }
        mPrescriptionListAdapter.notifyDataSetChanged();
    }

    @Override
    public void noPrescriptionAvailable() {
        binding.clNoPrescriptionAvailable.setVisibility(View.VISIBLE);
        binding.nlPrecription.setVisibility(View.GONE);
    }

    @Override
    public void getDependentDataList(ArrayList<DependentsData> dependentsData) {
        mDependentsDataArrayList.clear();
        mDependentsDataArrayList.addAll(dependentsData);
        mDependentAdapter.notifyDataSetChanged();
        popupview.tvDependentRelation.setText(mContext.getResources().getString(R.string.myself));
        popupview.tvDependentName.setText(new StringBuilder().append(presenter.getFirstName())
                .append(" ").append(presenter.getLastName()));
        if (isTextEmpty(presenter.getProfilePicUrl())) popupview.ivDependentProfilePic
                .setImageResource(R.drawable.default_photo);
        else PicassoTrustAll.getInstance(mContext)
                .load(presenter.getProfilePicUrl())
                .placeholder(R.drawable.default_photo)   // optional
                .error(R.drawable.default_photo)      // optional
                .transform(new PicassoCircleTransform())
                .into(popupview.ivDependentProfilePic);
    }

    @Override
    public void noDependentDataList() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == FILTERDEP_RESULT_CODE) {
                mFilterBaseOnDate = data != null ? data.getStringExtra(EXTRA_FILTER_DATE_NAME) : "";
                mFilterDateListPojoArrayList =
                        (ArrayList<FilterDateListPojo>) (data != null
                                ? data.getSerializableExtra(EXTRA_FILTER_DATA_POJO) : null);
                mDependentId = data != null ? data.getStringExtra(EXTRA_DEPENDENT_ID) : "";
                if (isNetworkAvailable(mContext)) presenter.getPrescriptionList(mDependentId,
                        mFrom, mTo, mFilterBaseOnDate, mLimit, mSkip);
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * <h2>selectDependent</h2>
     * <p> method to select the dependent based on tht it will display list of prescription</p>
     */
    private void selectDependent(String dependentId, String dependentName) {
        if (isNetworkAvailable(mContext)) presenter.getPrescriptionList(dependentId,
                mFrom, mTo, mFilterBaseOnDate, mLimit, mSkip);
        binding.tvPopUp.setText(dependentName);
        binding.tvPopUp.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                R.drawable.ic_keyboard_arrow_down_black_24dp, 0);
        mPopupWindowHelper.dismiss();
    }

    public String timeMethodYear(long bookingRequestedFor) {
        try {
            Date date = new Date(TimeUnit.SECONDS.toMillis(bookingRequestedFor));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy", Locale.US);
            sdf.setTimeZone(Utility.getTimeZone());
            String formattedDate = sdf.format(date);
            return formattedDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private HashMap<String, ArrayList<PrescriptionDatum>> groupDataIntoHashMap(ArrayList<PrescriptionDatum> listOfPojosOfJsonArray) {

        HashMap<String, ArrayList<PrescriptionDatum>> groupedHashMap = new HashMap<>();

        for (PrescriptionDatum pojoOfJsonArray : listOfPojosOfJsonArray) {

            String hashMapKey = timeMethodYear(pojoOfJsonArray.getBookingRequestedFor());

            if (groupedHashMap.containsKey(hashMapKey)) {
                // The key is already in the HashMap; add the pojo object
                // against the existing key.
                groupedHashMap.get(hashMapKey).add(pojoOfJsonArray);
            } else {
                // The key is not there in the HashMap; create a new key-value pair
                ArrayList<PrescriptionDatum> list = new ArrayList<>();
                list.add(pojoOfJsonArray);
                groupedHashMap.put(hashMapKey, list);
            }
        }
        return groupedHashMap;
    }
}
