package com.vaidg.prescription;

import android.content.Context;

import com.google.gson.Gson;
import com.utility.RefreshToken;
import com.vaidg.incalloutcalltelecall.dependent.model.DependentsGet;
import com.vaidg.networking.LSPServices;
import com.vaidg.prescription.model.PrescriptionData;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.SESSION_EXPIRED;
import static com.vaidg.utilities.Constants.SESSION_LOGOUT;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;
import static com.vaidg.utilities.LSPApplication.getInstance;

/**
 * <h2>PrescriptionPresenterImpl</h2>
 * <p>
 * This class is used to call the API.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class PrescriptionPresenterImpl  implements PrescriptionContract.PrescriptionPresent{

    @Inject
    PrescriptionContract.PrescriptionView view;
    @Inject
    LSPServices lspServices;
    @Inject
    SessionManagerImpl sessionManager;
    @Inject
    Gson gson;
    @Inject
    Context context;
    public final String TAG = PrescriptionPresenterImpl.class.getSimpleName();

    @Inject
    public PrescriptionPresenterImpl() {
    }

    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }

    @Override
    public void getPrescriptionList(String mDependentId, String mFrom, String mTo,
                                    String mFilterBaseOnDate, double mLimit, double mSkip) {
        onShowProgress();
        Observable<Response<ResponseBody>> responseObservable = lspServices.
                getPrescription(getInstance().getAuthToken(sessionManager.getSID()),
                        Constants.selLang, Constants.PLATFORM_ANDROID, mDependentId, mFrom, mTo,
                        mFilterBaseOnDate, mLimit, mSkip);
        responseObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {
                    }

                    @Override
                    public void onNext(@NotNull Response<ResponseBody> responseBodyResponse) {
                        int code = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null ?
                                    responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (code) {
                                case Constants.SUCCESS_RESPONSE:
                                    if (response != null && !response.isEmpty()) {
                                        PrescriptionData prescriptionData = gson.fromJson(response,
                                                PrescriptionData.class);
                                        if (prescriptionData.getData() != null
                                                && prescriptionData.getData().size() > 0) {
                                            if (view != null) view.onSuccess(prescriptionData.getData());
                                        } else {
                                            if (view != null) view.noPrescriptionAvailable();
                                        }
                                    }
                                    onHideProgress();
                                    break;
                                case Constants.SESSION_EXPIRED:
                                    // ErrorHandel errorHandel = gson.fromJson(errorBody,
                                    // ErrorHandel.class);
                                    RefreshToken.onRefreshToken(getInstance().
                                                    getAuthToken(sessionManager.getSID()),
                                            sessionManager.getREFRESHAUTH(),
                                            lspServices,
                                            new RefreshToken.RefreshTokenImple() {
                                                @Override
                                                public void onSuccessRefreshToken(String newToken) {
                                                    onHideProgress();
                                                    getInstance()
                                                            .setAuthToken(sessionManager.getSID(),
                                                                    sessionManager.getSID(),
                                                                    newToken);
                                                    getPrescriptionList(mDependentId, mFrom, mTo,
                                                            mFilterBaseOnDate, mLimit, mSkip);
                                                }

                                                @Override
                                                public void onFailureRefreshToken() {
                                                    onHideProgress();
                                                }

                                                @Override
                                                public void sessionExpired(String msg) {
                                                    onLogout(msg);
                                                }
                                            });
                                    break;
                                case SESSION_LOGOUT:
                                    onLogout(errorBody);
                                    break;
                                default:
                                    onErrorMsg(errorBody);
                                    break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            onHideProgress();
                        }

                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        e.printStackTrace();
                        onHideProgress();
                    }

                    @Override
                    public void onComplete() {
                        onHideProgress();
                    }
                });

    }

    @Override
    public void onHideProgress() {
        if (view != null) view.onHideProgress();
    }

    @Override
    public void onShowProgress() {
        if (view != null) view.onShowProgress();
    }

    @Override
    public void onLogout(String errorBody) {
        try {
            if (errorBody != null && !errorBody.isEmpty()) {
                JSONObject jsonObject = new JSONObject(errorBody);
                if (!jsonObject.getString(MESSAGE).isEmpty()) {
                    if (view != null) view.onLogout(jsonObject.getString(MESSAGE), sessionManager);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        onHideProgress();
    }

    @Override
    public void onErrorMsg(String errorBody) {
        try {
            if (errorBody != null && !errorBody.isEmpty()) {
                JSONObject jsonObject = new JSONObject(errorBody);
                if (!jsonObject.getString(MESSAGE).isEmpty()) {
                    if (view != null) view.onError(jsonObject.getString(MESSAGE));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        onHideProgress();
    }

    @Override
    public String getFirstName() {
        return sessionManager.getFirstName();
    }

    @Override
    public String getLastName() {
        return sessionManager.getLastName();
    }

    @Override
    public String getProfilePicUrl() {
        return sessionManager.getProfilePicUrl();
    }

    @Override
    public void getDependents(Context mcontext) {
        Observable<Response<ResponseBody>> responseObservable = lspServices.getDependents(
                getInstance().getAuthToken(sessionManager.getSID()),
                Constants.selLang, PLATFORM_ANDROID);
        responseObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NotNull Response<ResponseBody> responseBodyResponse) {
                        int code = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null ?
                                    responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (code) {
                                case SUCCESS_RESPONSE:
                                    if (response != null && !response.isEmpty()) {
                                        Constants.isDependentCalled = false;
                                        DependentsGet dependentsGet = gson.fromJson(response,
                                                DependentsGet.class);
                                        if (dependentsGet.getData() != null
                                                && dependentsGet.getData().size() > 0) {
                                            if (view != null) view
                                                    .getDependentDataList(dependentsGet.getData());
                                        } else {
                                            if (view != null) view.noDependentDataList();
                                        }
                                    }
                                    break;

                                case SESSION_EXPIRED:
                                    // ErrorHandel errorHandel = gson.fromJson(errorBody, ErrorHandel.class);
                                    RefreshToken.onRefreshToken(getInstance()
                                                    .getAuthToken(sessionManager.getSID()),
                                            sessionManager.getREFRESHAUTH(), lspServices,
                                            new RefreshToken.RefreshTokenImple() {
                                                @Override
                                                public void onSuccessRefreshToken(String newToken) {
                                                    getInstance()
                                                            .setAuthToken(sessionManager.getSID(),
                                                                    sessionManager.getSID(),
                                                                    newToken);
                                                    getDependents(mcontext);
                                                }

                                                @Override
                                                public void onFailureRefreshToken() {
                                                }

                                                @Override
                                                public void sessionExpired(String msg) {
                                                    onLogout(msg);
                                                }
                                            });
                                    break;
                                case SESSION_LOGOUT:
                                    onLogout(errorBody);
                                    break;
                                default:
                                    onErrorMsg(errorBody);
                                    break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
