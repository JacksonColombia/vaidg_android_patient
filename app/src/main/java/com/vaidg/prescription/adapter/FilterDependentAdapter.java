package com.vaidg.prescription.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.utility.PicassoTrustAll;
import com.vaidg.R;
import com.vaidg.databinding.ItemFilterdependentListBinding;
import com.vaidg.incalloutcalltelecall.dependent.model.DependentsData;
import com.vaidg.prescription.PrescriptionActivity;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.PicassoCircleTransform;

import java.util.ArrayList;

import static com.vaidg.utilities.Utility.isTextEmpty;
import static com.vaidg.utilities.Utility.setTextSize14Sp;
import static com.vaidg.utilities.Utility.setTextSize16Sp;

/**
 * <h2>FilterDependentAdapter</h2>
 * <p>
 * Adapter for PrescriptionActivity.
 * </p>
 *
 * @author 3Embed.
 * @version 1.0
 * @since 29-11-2019.
 */
public class FilterDependentAdapter extends RecyclerView.Adapter<FilterDependentAdapter
        .ItemViewHolder> {

    private Context mContext;
    private final ArrayList<DependentsData> mDependentsDataArrayList;
    private final OnItemClickListener listener;
    private AppTypeface appTypeface;

    public FilterDependentAdapter(ArrayList<DependentsData> dependentsDataList,
                                  OnItemClickListener listener) {
        this.mDependentsDataArrayList = dependentsDataList;
        this.listener = listener;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        ItemFilterdependentListBinding binding =
                ItemFilterdependentListBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        mContext = parent.getContext();
        return new ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.bind(mDependentsDataArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return mDependentsDataArrayList == null ? 0 : mDependentsDataArrayList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int position, ArrayList<DependentsData> mDependentsDataArrayList,
                         DependentsData dependentsData, TextView tvDependentName,
                         TextView tvDependentRelation, ImageView ivDependentProfilePic);
    }

    /**
     * <h2>ItemViewHolder</h2>
     * <p>
     * This itemViewHolder class hold the item state for
     *
     * @author 3Embed.
     * @version 1.0
     * @see PrescriptionActivity , and handle the view click event.
     * </p>
     * @since 29-11-2019.
     */
    class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ItemFilterdependentListBinding binding;

        ItemViewHolder(@NonNull ItemFilterdependentListBinding convertView) {
            super(convertView.getRoot());
            this.binding = convertView;
            appTypeface = AppTypeface.getInstance(mContext);
            binding.clContainer.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (listener != null) listener.onItemClick(getAdapterPosition(),
                    mDependentsDataArrayList, mDependentsDataArrayList.get(getAdapterPosition()),
                    binding.tvDependentName, binding.tvDependentRelation,
                    binding.ivDependentProfilePic);
        }

        public void bind(DependentsData dependentsData) {
            binding.tvDependentName.setTypeface(appTypeface.getHind_semiBold());
            binding.tvDependentRelation.setTypeface(appTypeface.getHind_regular());
            setTextSize16Sp(mContext, binding.tvDependentName);
            setTextSize14Sp(mContext, binding.tvDependentRelation);

            binding.tvDependentName.setText(new StringBuilder().append(dependentsData
                    .getFirstName()).append(" ").append(dependentsData.getLastName()));
            binding.tvDependentName.setTag(getAdapterPosition());
            binding.tvDependentRelation.setText(dependentsData.getRelationship());
            if (isTextEmpty(dependentsData.getProfilePic())) binding.ivDependentProfilePic
                    .setImageResource(R.drawable.default_photo);
                else PicassoTrustAll.getInstance(mContext)
                        .load(dependentsData.getProfilePic())
                        .placeholder(R.drawable.default_photo)   // optional
                        .error(R.drawable.default_photo)      // optional
                        .transform(new PicassoCircleTransform())
                        .into(binding.ivDependentProfilePic);

            if (dependentsData.isChecked()) {
                dependentsData.setChecked(true);
                binding.tvDependentName.setCompoundDrawablesWithIntrinsicBounds(0,
                        0, R.drawable.ic_check, 0);
            } else {
                dependentsData.setChecked(false);
                binding.tvDependentName.setCompoundDrawablesWithIntrinsicBounds(0,
                        0, R.drawable.ic_unselected, 0);
            }
        }
    }
}
