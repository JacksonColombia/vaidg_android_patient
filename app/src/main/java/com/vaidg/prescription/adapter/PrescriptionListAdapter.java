package com.vaidg.prescription.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.utility.PicassoTrustAll;
import com.vaidg.R;
import com.vaidg.databinding.ItemPrescriptionListBinding;
import com.vaidg.databinding.ItemPrescriptionListDateBinding;
import com.vaidg.prescription.PrescriptionActivity;
import com.vaidg.prescription.model.DateItem;
import com.vaidg.prescription.model.GeneralItem;
import com.vaidg.prescription.model.ListItem;
import com.vaidg.prescription.model.PrescriptionDatum;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.FullscreenProgressDialog;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.SessionManager;
import com.vaidg.utilities.Utility;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.vaidg.utilities.Utility.isTextEmpty;
import static com.vaidg.utilities.Utility.setTextSize14Sp;
import static com.vaidg.utilities.Utility.setTextSize16Sp;
import static com.vaidg.utilities.Utility.setTextSize22Sp;

/**
 * <h2>PrescriptionListAdapter</h2>
 * <p>
 * Adapter for PrescriptionActivity.
 * </p>
 *
 * @author 3Embed.
 * @version 1.0
 * @since 29-11-2019.
 */
public class PrescriptionListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final ArrayList<ListItem> mPrescriptionData;
    private Context mContext;
    private AppTypeface appTypeface;
    private SessionManager sessionManager;

    public PrescriptionListAdapter(ArrayList<ListItem> prescriptionData) {
        this.mPrescriptionData = prescriptionData;
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case ListItem.TYPE_GENERAL:
                ItemPrescriptionListBinding binding =
                        ItemPrescriptionListBinding.inflate(LayoutInflater.from(parent.getContext()),
                                parent, false);
                mContext = parent.getContext();
                viewHolder = new GeneralViewHolder(binding);
                break;

            case ListItem.TYPE_DATE:
                ItemPrescriptionListDateBinding bindingDate =
                        ItemPrescriptionListDateBinding.inflate(LayoutInflater.from(parent.getContext()),
                                parent, false);
                mContext = parent.getContext();
                viewHolder = new DateViewHolder(bindingDate);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        switch (viewHolder.getItemViewType()) {
            case ListItem.TYPE_GENERAL:
                GeneralItem generalItem   = (GeneralItem) mPrescriptionData.get(position);
                GeneralViewHolder generalViewHolder= (GeneralViewHolder) viewHolder;
                generalViewHolder.bind(generalItem.getPojoOfJsonArray());
                break;

            case ListItem.TYPE_DATE:
                DateItem dateItem = (DateItem) mPrescriptionData.get(position);
                DateViewHolder dateViewHolder = (DateViewHolder) viewHolder;
                dateViewHolder.binding.tvPrescriptionYear.setTypeface(appTypeface.getHind_semiBold());
                setTextSize16Sp(mContext, dateViewHolder.binding.tvPrescriptionYear);
                dateViewHolder.binding.tvPrescriptionYear.setText(dateItem.getDate());
                // Populate date item data here
                break;
        }
    }

    /**
     * <h2>ItemViewHolder</h2>
     * <p>
     * This itemViewHolder class hold the item state for
     *
     * @author 3Embed.
     * @version 1.0
     * @see PrescriptionActivity , and handle the view click event.
     * </p>
     * @since 29-11-2019.
     */
    class GeneralViewHolder extends RecyclerView.ViewHolder {
        private final ItemPrescriptionListBinding binding;

        GeneralViewHolder(ItemPrescriptionListBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;
            appTypeface = AppTypeface.getInstance(mContext);
            sessionManager = new SessionManager(mContext);
        }

        public void bind(PrescriptionDatum prescriptionDatum) {
            binding.tvDocName.setTypeface(appTypeface.getHind_medium());
            binding.tvDocSpec.setTypeface(appTypeface.getHind_medium());
            binding.tvMonth.setTypeface(appTypeface.getHind_medium());
            binding.tvDate.setTypeface(appTypeface.getHind_medium());
            binding.tvDependentRelation.setTypeface(appTypeface.getHind_regular());
            binding.tvDependentName.setTypeface(appTypeface.getHind_medium());
            setTextSize14Sp(mContext, binding.tvDocSpec);
            setTextSize14Sp(mContext, binding.tvDependentRelation);
            setTextSize16Sp(mContext, binding.tvMonth);
            setTextSize16Sp(mContext, binding.tvDocName);
            setTextSize16Sp(mContext, binding.tvDependentName);
            setTextSize22Sp(mContext, binding.tvDate);

            binding.tvDocName.setText(new StringBuilder()
                    .append(prescriptionDatum.getProviderTitle()).append(" ")
                    .append(prescriptionDatum.getProviderFirstName()).append(" ")
                    .append(prescriptionDatum.getProviderLastName()));
            binding.tvDocSpec.setText(prescriptionDatum.getCategoryName());

            if (prescriptionDatum.getDependentId().equals("1")) {
                binding.tvDependentName.setText(new StringBuilder()
                        .append(sessionManager.getFirstName()).append(" ")
                        .append(sessionManager.getLastName()).append(","));
                binding.tvDependentRelation.setText(new StringBuilder().append(" ")
                        .append(mContext.getResources().getText(R.string.myself)));
                if (!isTextEmpty(prescriptionDatum.getProviderProfilePic())) PicassoTrustAll
                        .getInstance(mContext)
                        .load(prescriptionDatum.getProviderProfilePic())
                        .transform(new PicassoCircleTransform())
                        .into(binding.ivPrescription);
            } else {
                binding.tvDependentName.setText(new StringBuilder()
                        .append(prescriptionDatum.getDependentData().getFirstName())
                        .append(" ").append(prescriptionDatum.getDependentData().getLastName())
                        .append(","));
                binding.tvDependentRelation.setText(new StringBuilder().append(" ")
                        .append(prescriptionDatum.getDependentData().getRelationship()));
                if (!isTextEmpty(prescriptionDatum.getProviderProfilePic())) PicassoTrustAll
                        .getInstance(mContext)
                        .load(prescriptionDatum.getDependentData().getProfilePic())
                        .transform(new PicassoCircleTransform())
                        .into(binding.ivPrescription);
            }
            timeMethodDate(binding.tvDate, prescriptionDatum.getBookingRequestedFor());
            timeMethodMonth(binding.tvMonth, prescriptionDatum.getBookingRequestedFor());

            itemView.setOnClickListener(v -> {
                FullscreenProgressDialog mFullscreenProgressDialog = new FullscreenProgressDialog();
                mFullscreenProgressDialog.setDimAmount(0);
                if(!TextUtils.isEmpty(prescriptionDatum.getPdfFile()))
                mFullscreenProgressDialog.setPdfFile(prescriptionDatum.getPdfFile());
                mFullscreenProgressDialog.setmContext(mContext);
                mFullscreenProgressDialog.setName(prescriptionDatum.getProviderTitle()
                        + " " + prescriptionDatum.getProviderFirstName() + " "
                        + prescriptionDatum.getProviderLastName());
                if(!TextUtils.isEmpty(prescriptionDatum.getProviderProfilePic()))
                    mFullscreenProgressDialog.setProfilePic(prescriptionDatum
                            .getProviderProfilePic());
                if(prescriptionDatum.getMedication() != null && prescriptionDatum
                        .getMedication().size() > 0)
                    mFullscreenProgressDialog.setMedications(prescriptionDatum.getMedication());
                mFullscreenProgressDialog.setCategoryName(prescriptionDatum.getCategoryName());
                mFullscreenProgressDialog.setmDateAndTime(prescriptionDatum
                        .getBookingRequestedFor());
                mFullscreenProgressDialog.setDocDegree(prescriptionDatum.getDegree());
                mFullscreenProgressDialog.show(((AppCompatActivity) mContext));
            });

        }


        public void timeMethodMonth(TextView tbServiceAvailable, long bookingRequestedFor) {
            try {
                Date date = new Date(TimeUnit.SECONDS.toMillis(bookingRequestedFor));
                SimpleDateFormat sdf = new SimpleDateFormat("MMM", Locale.US);
                sdf.setTimeZone(Utility.getTimeZone());
                String formattedDate = sdf.format(date);
                tbServiceAvailable.setText(formattedDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        public void timeMethodDate(TextView tbServiceAvailable, long bookingRequestedFor) {
            try {
                Date date = new Date(TimeUnit.SECONDS.toMillis(bookingRequestedFor));
                SimpleDateFormat sdf = new SimpleDateFormat("d", Locale.US);
                sdf.setTimeZone(Utility.getTimeZone());
                String formattedDate = sdf.format(date);
                tbServiceAvailable.setText(formattedDate);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    // ViewHolder for date row item
    class DateViewHolder extends RecyclerView.ViewHolder {
        private final ItemPrescriptionListDateBinding binding;

        public DateViewHolder(ItemPrescriptionListDateBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;
            appTypeface = AppTypeface.getInstance(mContext);
            sessionManager = new SessionManager(mContext);
        }
    }

    @Override
    public int getItemCount() {
        return mPrescriptionData == null ? 0 : mPrescriptionData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mPrescriptionData.get(position).getType();
    }
}

  
