package com.vaidg.prescription;

import com.vaidg.Dagger2.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>PrescriptionDaggerModule</h2>
 * <p>
 * This is a Dagger module. We use this to pass in the View dependency to the
 * {@link PrescriptionPresenterImpl}.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
@Module
public interface PrescriptionDaggerModule
{
    @Binds
    @ActivityScoped
    PrescriptionContract
            .PrescriptionPresent providePrescriptionPresenter(PrescriptionPresenterImpl presenter);

    @Binds
    @ActivityScoped
    PrescriptionContract.PrescriptionView providerPrescriptionView(PrescriptionActivity activity);
}
