package com.vaidg.prescription;

import android.content.Context;

import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;
import com.vaidg.incalloutcalltelecall.dependent.model.DependentsData;
import com.vaidg.prescription.model.PrescriptionDatum;

import java.util.ArrayList;

/**
 * <h2>YourAddressContract</h2>
 * <p>
 * This class is used to act as a link between view and presenter.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public interface PrescriptionContract {
    interface PrescriptionPresent extends BasePresenter {

        /**
         * <h2>getPrescriptionList</h2>
         * <p>This method is used to get the list of prescription</p>
         */
        void getPrescriptionList(String mDependentId, String mFrom, String mTo,
                                 String mFilterBaseOnDate, double mLimit, double mSkip);

        /**
         * <h2>onHideProgress</h2>
         * <p>This method is used to hide the loading view</p>
         */
        void onHideProgress();

        /**
         * <h2>onShowProgress</h2>
         * <p>This method is used to show the loading view</p>
         */
        void onShowProgress();

        /**
         * <h2>onLogout</h2>
         * <p>This method is used to show msg if session is expired</p>
         */
        void onLogout(String errorBody);

        /**
         * <h2>onErrorMsg</h2>
         * <p>This method is used to show error msg from api calls</p>
         */
        void onErrorMsg(String errorBody);

        /**
         * <h2>getFirstName</h2>
         * <p>This method is used to get the login user first name</p>
         */
        String getFirstName();

        /**
         * <h2>getLastName</h2>
         * <p>This method is used to get the login user last name</p>
         */
        String getLastName();

        /**
         * <h2>getProfilePicUrl</h2>
         * <p>This method is used to get the login user profile pic</p>
         */
        String getProfilePicUrl();

        /**
         * <h2>getDependents</h2>
         * This method is used to get the data of the dependent
         *
         * @param mcontext instance of class
         */
        void getDependents(Context mcontext);
    }

    interface PrescriptionView extends BaseView {

        /**
         * <h2>onSuccess</h2>
         * <p>This method is used to get the list of prescription</p>
         *
         * @param prescriptionDatum list of prescription
         */
        void onSuccess(ArrayList<PrescriptionDatum> prescriptionDatum);

        /**
         * <h2>onSuccess</h2>
         * <p>This method is used to show no list of prescription</p>
         *
         */
        void noPrescriptionAvailable();
        /**
         * <h2>getDependentDataList</h2>
         * This method is used to get the list of the dependent
         *
         * @param dependentsData list of dependent
         */
        void getDependentDataList(ArrayList<DependentsData> dependentsData);

        /**
         * <h2>getDependentDataList</h2>
         * This method is used to show no list of the dependent
         *
         */
        void noDependentDataList();
    }
}
