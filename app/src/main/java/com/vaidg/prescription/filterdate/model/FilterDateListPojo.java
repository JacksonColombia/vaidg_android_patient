package com.vaidg.prescription.filterdate.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h2>FilterDateListPojo</h2>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class FilterDateListPojo implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("genderName")
    @Expose
    private String genderName;
    @SerializedName("checked")
    @Expose
    private boolean checked;

    public FilterDateListPojo(String id, String genderName) {
        this.id = id;
        this.genderName = genderName;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getId() {
        return id;
    }

    public String getGenderName() {
        return genderName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }

    public boolean isChecked() {
        return checked;
    }
}
