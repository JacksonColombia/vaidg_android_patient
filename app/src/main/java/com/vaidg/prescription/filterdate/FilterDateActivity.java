package com.vaidg.prescription.filterdate;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.vaidg.R;
import com.vaidg.databinding.ActivityFilterDateBinding;
import com.vaidg.prescription.filterdate.adapter.FilterDateListAdapter;
import com.vaidg.prescription.filterdate.model.FilterDateListPojo;
import com.vaidg.utilities.AppTypeface;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

import static com.vaidg.utilities.Constants.PRESCRIPTIONCREEN.EXTRA_DEPENDENT_ID;
import static com.vaidg.utilities.Constants.PRESCRIPTIONCREEN.EXTRA_FILTER_DATA_POJO;
import static com.vaidg.utilities.Constants.PRESCRIPTIONCREEN.EXTRA_FILTER_DATE_NAME;
import static com.vaidg.utilities.Constants.PRESCRIPTIONCREEN.FOUR;
import static com.vaidg.utilities.Constants.PRESCRIPTIONCREEN.LAST_MONTH;
import static com.vaidg.utilities.Constants.PRESCRIPTIONCREEN.LAST_ONE_YEAR;
import static com.vaidg.utilities.Constants.PRESCRIPTIONCREEN.LAST_SIX_MONTH;
import static com.vaidg.utilities.Constants.PRESCRIPTIONCREEN.LAST_THREE_MONTH;
import static com.vaidg.utilities.Constants.PRESCRIPTIONCREEN.ONE;
import static com.vaidg.utilities.Constants.PRESCRIPTIONCREEN.THREE;
import static com.vaidg.utilities.Constants.PRESCRIPTIONCREEN.TWO;
import static com.vaidg.utilities.Utility.setTextSize14Sp;
import static com.vaidg.utilities.Utility.setTextSize16Sp;

/**
 * <h2>FilterDateActivity</h2>
 * <p>
 * FilterDate page where it will allow user to select date wise filter for prescription.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class FilterDateActivity extends DaggerAppCompatActivity {
    private FilterDateListAdapter mFilterDateListAdapter;
    @Inject
    AppTypeface appTypeface;
    private Context mContext;
    private ArrayList<FilterDateListPojo> mFilterDateListPojos;
    private String mDependentId = "", mID = "";
    private ActivityFilterDateBinding binding;
    private LayoutAnimationController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFilterDateBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        mContext = this;
        poupulateDataList();
        initialize();
    }

    /**
     * <h2>initialize</h2>
     * <p> method to initialize the views</p>
     */
    private void initialize() {
        setSupportActionBar(binding.toolbarLayout.toolbar);
        binding.toolbarLayout.toolbar.setElevation(0f);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0f);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.toolbarLayout.toolbar.setNavigationOnClickListener(v -> closeActivity());
            binding.toolbarLayout.tvSkip.setOnClickListener(v -> {
                mID ="";
                if(mFilterDateListAdapter != null) mFilterDateListAdapter.setSelectedDate(mID);
            });
        }
        binding.toolbarLayout.tvCenter.setTypeface(appTypeface.getHind_semiBold());
        binding.toolbarLayout.tvSkip.setTypeface(appTypeface.getHind_medium());
        binding.tvFilterSave.setTypeface(appTypeface.getHind_semiBold());
        setTextSize14Sp(mContext, binding.toolbarLayout.tvSkip);
        setTextSize16Sp(mContext, binding.toolbarLayout.tvCenter);
        setTextSize14Sp(mContext, binding.tvFilterSave);
        binding.toolbarLayout.tvCenter.setText(mContext.getResources().getString(R.string.filter));
        binding.toolbarLayout.tvSkip.setText(mContext.getResources().getString(R.string.clear));
        binding.tvFilterSave.setOnClickListener(v -> {
            closeActivity();
        });
    }

    /**
     * <h2>poupulateDataList</h2>
     * <p> method to populate the list of data</p>
     */
    private void poupulateDataList() {
        mFilterDateListPojos = new ArrayList<>();
        if (getIntent().getExtras() != null) {
            mFilterDateListPojos = (ArrayList<FilterDateListPojo>) getIntent()
                    .getSerializableExtra(EXTRA_FILTER_DATA_POJO);
            mDependentId = getIntent().getStringExtra(EXTRA_DEPENDENT_ID);
        }

        if (mFilterDateListPojos != null && mFilterDateListPojos.size() == 0) {
            mFilterDateListPojos.add(new FilterDateListPojo(ONE, LAST_MONTH));
            mFilterDateListPojos.add(new FilterDateListPojo(TWO, LAST_THREE_MONTH));
            mFilterDateListPojos.add(new FilterDateListPojo(THREE, LAST_SIX_MONTH));
            mFilterDateListPojos.add(new FilterDateListPojo(FOUR, LAST_ONE_YEAR));
        }

        mFilterDateListAdapter = new FilterDateListAdapter(mFilterDateListPojos,
                (position, genderArrayList, genderName, tvGenderName) -> {
                    mID = genderName.getId();
                    mFilterDateListPojos = genderArrayList;
                    mFilterDateListAdapter.setSelectedDate(mID);
                });
        controller = AnimationUtils.loadLayoutAnimation(mContext,
                R.anim.layout_animation_right_to_left);
        binding.rvFilterDate.setHasFixedSize(true);
        binding.rvFilterDate.setLayoutManager(new LinearLayoutManager(mContext));
        binding.rvFilterDate.addItemDecoration(new DividerItemDecoration(mContext,
                LinearLayoutManager.VERTICAL));
        binding.rvFilterDate.setLayoutAnimation(controller);
        binding.rvFilterDate.setAdapter(mFilterDateListAdapter);
    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        closeActivity();
    }

    /**
     * <h2>closeActivity</h2>
     * <p> method for closing current page</p>
     */
    private void closeActivity() {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_FILTER_DATE_NAME, mID);
        intent.putExtra(EXTRA_DEPENDENT_ID, mDependentId);
        intent.putExtra(EXTRA_FILTER_DATA_POJO, mFilterDateListPojos);
        setResult(RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
    }
}

