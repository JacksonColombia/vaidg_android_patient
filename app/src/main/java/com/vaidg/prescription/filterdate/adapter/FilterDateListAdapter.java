package com.vaidg.prescription.filterdate.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.R;
import com.vaidg.databinding.ItemFilterDateListBinding;
import com.vaidg.prescription.filterdate.FilterDateActivity;
import com.vaidg.prescription.filterdate.model.FilterDateListPojo;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Utility;

import java.util.ArrayList;

/**
 * <h2>FilterDateListAdapter</h2>
 * <p>
 * Adapter for FilterDateListActivity.
 * </p>
 *
 * @author 3Embed.
 * @version 1.0
 * @since 29-11-2019.
 */
public class FilterDateListAdapter extends RecyclerView.Adapter<FilterDateListAdapter.ViewHolder> {

    private Context mContext;
    private final ArrayList<FilterDateListPojo> mFilterDateListPojos;
    private final OnItemClickListener listener;
    private AppTypeface appTypeface;

    public FilterDateListAdapter(ArrayList<FilterDateListPojo> filterDateListPojos,
                                 OnItemClickListener listener) {
        this.mFilterDateListPojos = filterDateListPojos;
        this.listener = listener;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        ItemFilterDateListBinding binding = ItemFilterDateListBinding
                .inflate(LayoutInflater.from(parent.getContext()), parent, false);
        mContext = parent.getContext();
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mFilterDateListPojos.get(position));
    }

    @Override
    public int getItemCount() {
        return mFilterDateListPojos == null ? 0 : mFilterDateListPojos.size();
    }

    /**
     * <h2>setSelectedDate</h2>
     * This method is used to select the date from filter.
     */
    public void setSelectedDate(String mID) {
        for (int i = 0; i < mFilterDateListPojos.size(); i++) {
            mFilterDateListPojos.get(i).setChecked(mFilterDateListPojos.get(i).getId().equals(mID));
        }
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        /**
         * <h2>onItemClick</h2>
         * This method is used to delete the selected filter.
         */
        void onItemClick(int position, ArrayList<FilterDateListPojo> mFilterDateListPojos,
                         FilterDateListPojo genderName, TextView tvGenderName);
    }

    /**
     * <h2>ItemViewHolder</h2>
     * <p>
     * This itemViewHolder class hold the item state for
     *
     * @author 3Embed.
     * @version 1.0
     * @see FilterDateActivity , and handle the view click event.
     * </p>
     * @since 29-11-2019.
     */
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ItemFilterDateListBinding binding;

        ViewHolder(@NonNull ItemFilterDateListBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;
            appTypeface = AppTypeface.getInstance(mContext);
            binding.clContainer.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (listener != null) {
/*
                for (int i = 0; i < mFilterDateListPojos.size(); i++) {
                    mFilterDateListPojos.get(i).setChecked(i == getAdapterPosition());
                }
*/
                listener.onItemClick(getAdapterPosition(), mFilterDateListPojos,
                        mFilterDateListPojos.get(getAdapterPosition()), binding.tvGenderName);
            }
        }

        public void bind(FilterDateListPojo filterDateListPojo) {
            binding.tvGenderName.setTypeface(appTypeface.getHind_medium());
            Utility.setTextSize16Sp(mContext, binding.tvGenderName);
            filterDateListPojo.setGenderName(filterDateListPojo.getGenderName());
            filterDateListPojo.setId(filterDateListPojo.getId());
            binding.tvGenderName.setText(filterDateListPojo.getGenderName());
            if (filterDateListPojo.isChecked()) {
                filterDateListPojo.setChecked(true);
                binding.clContainer.setSelected(true);
                binding.ivGender.setImageResource(R.drawable.ic_check);
            } else {
                filterDateListPojo.setChecked(false);
                binding.clContainer.setSelected(false);
                binding.ivGender.setImageResource(R.drawable.ic_unselected);
            }
        }
    }
}
