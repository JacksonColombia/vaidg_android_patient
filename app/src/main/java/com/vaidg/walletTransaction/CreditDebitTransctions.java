package com.vaidg.walletTransaction;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreditDebitTransctions
{

    /*"txnId":"WAL-1525329330-882.4883915169897",
"trigger":"WALLET RECHARGE",
"txnType":"CREDIT",
"comment":"Wallet recharge from provider app",
"currency":"USD",
"openingBal":0,
"amount":1000,
"closingBal":1000,
"paymentType":"WALLET",
"timestamp":1525329330,
"paymentTxnId":"ch_1CNb2MLsYTSIIkpUU5DZ6lQ8",
"intiatedBy":"Provider",
"tripId":"N/A"*/
    @SerializedName("txnType")
    @Expose
    private String txnType;
    @SerializedName("trigger")
    @Expose
    private String trigger;
    @SerializedName("openingBal")
    @Expose
    private String openingBal;
    @SerializedName("tripId")
    @Expose
    private String tripId;
    @SerializedName("paymentType")
    @Expose
    private String paymentType;
    @SerializedName("intiatedBy")
    @Expose
    private String intiatedBy;
    @SerializedName("txnId")
    @Expose
    private String txnId;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("amount")
    @Expose
    private double amount;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("paymentTxnId")
    @Expose
    private String paymentTxnId;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("closingBal")
    @Expose
    private String closingBal;

    public String getTxnType ()
    {
        return txnType;
    }

    public void setTxnType (String txnType)
    {
        this.txnType = txnType;
    }

    public String getTrigger ()
    {
        return trigger;
    }

    public void setTrigger (String trigger)
    {
        this.trigger = trigger;
    }

    public String getOpeningBal ()
    {
        return openingBal;
    }

    public void setOpeningBal (String openingBal)
    {
        this.openingBal = openingBal;
    }

    public String getTripId ()
    {
        return tripId;
    }

    public void setTripId (String tripId)
    {
        this.tripId = tripId;
    }

    public String getPaymentType ()
    {
        return paymentType;
    }

    public void setPaymentType (String paymentType)
    {
        this.paymentType = paymentType;
    }

    public String getIntiatedBy ()
    {
        return intiatedBy;
    }

    public void setIntiatedBy (String intiatedBy)
    {
        this.intiatedBy = intiatedBy;
    }

    public String getTxnId ()
    {
        return txnId;
    }

    public void setTxnId (String txnId)
    {
        this.txnId = txnId;
    }

    public String getCurrency ()
    {
        return currency;
    }

    public void setCurrency (String currency)
    {
        this.currency = currency;
    }

    public double getAmount ()
    {
        return amount;
    }


    public String getTimestamp ()
    {
        return timestamp;
    }

    public void setTimestamp (String timestamp)
    {
        this.timestamp = timestamp;
    }

    public String getPaymentTxnId ()
    {
        return paymentTxnId;
    }

    public void setPaymentTxnId (String paymentTxnId)
    {
        this.paymentTxnId = paymentTxnId;
    }

    public String getComment ()
    {
        return comment;
    }

    public void setComment (String comment)
    {
        this.comment = comment;
    }

    public String getClosingBal ()
    {
        return closingBal;
    }

    public void setClosingBal (String closingBal)
    {
        this.closingBal = closingBal;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [txnType = "+txnType+", trigger = "+trigger+", openingBal = "+openingBal+", tripId = "+tripId+", paymentType = "+paymentType+", intiatedBy = "+intiatedBy+", txnId = "+txnId+", currency = "+currency+", amount = "+amount+", timestamp = "+timestamp+", paymentTxnId = "+paymentTxnId+", comment = "+comment+", closingBal = "+closingBal+"]";
    }
}
