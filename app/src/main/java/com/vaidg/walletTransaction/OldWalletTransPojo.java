package com.vaidg.walletTransaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @since 19/09/17.
 */

public class OldWalletTransPojo
{
        /*"errNum":200,
    "errMsg":"Got The Details",
    "errFlag":0,
    "data":{
    "debitArr":[],
    "creditArr":[],
    "creditDebitArr":[*/
        @SerializedName("errNum")
        @Expose
    private int errNum;
    @SerializedName("errFlag")
    @Expose
    private int  errFlag;
    @SerializedName("errMsg")
    @Expose
    private String errMsg;
    @SerializedName("data")
    @Expose
    private WalletTransDataPojo data;

    public int getErrNum() {
        return errNum;
    }



    public int getErrFlag() {
        return errFlag;
    }


    public String getErrMsg() {
        return errMsg;
    }


    public WalletTransDataPojo getData() {
        return data;
    }



    @Override
    public String toString() {
        return "OldWalletTransPojo{" +
                "errNum=" + errNum +
                ", errFlag=" + errFlag +
                ", errMsg='" + errMsg + '\'' +
                ", data=" + data +
                '}';
    }
}
