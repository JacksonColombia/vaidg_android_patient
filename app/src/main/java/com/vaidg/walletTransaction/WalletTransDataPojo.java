package com.vaidg.walletTransaction;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * @since 19/09/17.
 */

public class WalletTransDataPojo
{
   /* "data":{
    "debitArr":[],
    "creditArr":[],
    "creditDebitArr":[]}*/
   @SerializedName("debitArr")
   @Expose
   private ArrayList<CreditDebitTransctions> debitArr;
    @SerializedName("creditArr")
    @Expose
   private ArrayList<CreditDebitTransctions> creditArr;
    @SerializedName("creditDebitArr")
    @Expose
   private ArrayList<CreditDebitTransctions> creditDebitArr;
    @SerializedName("paymentArr")
    @Expose
   private ArrayList<CreditDebitTransctions> paymentArr;


    public ArrayList<CreditDebitTransctions> getPaymentArr() {
        return paymentArr;
    }

    public ArrayList<CreditDebitTransctions> getDebitArr() {
        return debitArr;
    }

    public ArrayList<CreditDebitTransctions> getCreditArr() {
        return creditArr;
    }

    public ArrayList<CreditDebitTransctions> getCreditDebitArr() {
        return creditDebitArr;
    }

    @Override
    public String toString() {
        return "WalletTransDataPojo{" +
                "debitArr=" + debitArr +
                ", creditArr=" + creditArr +
                ",paymentArr=" + paymentArr +
                ", creditDebitArr=" + creditDebitArr +
                '}';
    }
}
