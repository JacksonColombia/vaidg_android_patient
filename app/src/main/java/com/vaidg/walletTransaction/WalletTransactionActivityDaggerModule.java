package com.vaidg.walletTransaction;



import com.vaidg.Dagger2.ActivityScoped;
import com.vaidg.Dagger2.FragmentScoped;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public interface WalletTransactionActivityDaggerModule
{
    @Binds
    @ActivityScoped
    WalletTransactionContract.WalletTrasactionView provideWalletTransactionView(WalletTransActivity transActivity);

    @Binds
    @ActivityScoped
    WalletTransactionContract.WalletTransactionPresenter provideWalletTransPresnter(WalletTransactionActivityPresenter transactionActivityPresenter);

    @ContributesAndroidInjector
    @FragmentScoped
    WalletTransactionsFragment provideWalletListFragmentAll();

    /*@ContributesAndroidInjector
    @FragmentScoped
    WalletTransactionsFragment provideWalletListFragmentDebit();

    @ContributesAndroidInjector
    @FragmentScoped
    WalletTransactionsFragment provideWalletListFragmentCredit();*/

}