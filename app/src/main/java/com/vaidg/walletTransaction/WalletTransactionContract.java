package com.vaidg.walletTransaction;


import java.util.ArrayList;

public interface WalletTransactionContract
{

    interface WalletTrasactionView
    {
        void walletTransactionsApiSuccessViewNotifier();

        /**
         * <h2>showToastNotifier</h2>
         * <p> method to trigger activity/fragment show progress dialog interface </p>
         * @param msg: message to be shown along with the progress dialog
         */
        void showProgressDialog(String msg);

        /**
         * <h2>showToastNotifier</h2>
         * <p> method to trigger activity/fragment showToast interface to show test </p>
         * @param msg: message to be shown in toast
         * @param duration: toast duration
         */
        void showToast(String msg, int duration);

        /**
         * <h2>showAlertNotifier</h2>
         * <p> method to trigger activity/fragment showAlertNotifier interface to show alert </p>
         * @param title: alert title to be setList
         * @param msg: alert message to be displayed
         */
        void showAlert(String title, String msg);


        /**
         * <h2>noInternetAlert</h2>
         * <p> method to trigger activity/fragment alert interface to show alert that there isnot internet connectivity </p>
         */
        void noInternetAlert();

        /**
         * <h2>Hide Progress bar</h2>
         * <p>This method is using to hide the progress bar</p>
         */
        void hideProgressDialog();

        /**
         * <h2>Set All transaction data to display</h2>
         * <p>this method is using to set the all transaction data</p>
         * @param allTransactionsAL all transaction data
         */
        void setAllTransactionsAL(ArrayList<CreditDebitTransctions> allTransactionsAL);

        /**
         * <h2>Set debit Transactions data to display</h2>
         * <p>this method is using to set the debit  transaction data</p>
         * @param debitTransactionsAL debit transaction data
         */
        void setDebitTransactionsAL(ArrayList<CreditDebitTransctions> debitTransactionsAL);

        /**
         * <h2>Set credit Transactions data to display</h2>
         * <p>this method is using to set the credit  transaction data</p>
         * @param creditTransactionsAL credit transaction data
         */
        void setCreditTransactionsAL(ArrayList<CreditDebitTransctions> creditTransactionsAL);


        /**
         * <h2>Set payment Transactions data to display</h2>
         * <p>this method is using to set the payment  transaction data</p>
         * @param paymentTransactionsAL payment transaction data
         */
        void setPaymentTransactionsAL(ArrayList<CreditDebitTransctions> paymentTransactionsAL);


    }

    interface WalletTransactionPresenter
    {
        /**
         * <h2>Show notification</h2>
         * <p>this method is using to  Show notification to user</p>
         * @param msg notification message
         * @param duration duration
         */
        void showToastNotifier(String msg, int duration);

        /**
         *<h2>Initialize transaction Api call</h2>
         * <P>this method is using to initialize the Api call</P>
         * @param isToLoadMore is load more
         * @param isFromOnRefresh it from refresh
         */
        void initLoadTransactions(boolean isToLoadMore, boolean isFromOnRefresh);

    }
}
