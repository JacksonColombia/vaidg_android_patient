package com.vaidg.walletTransaction;


import android.content.Context;
import android.util.Log;

import com.vaidg.utilities.LSPApplication;
import com.google.gson.Gson;

import com.vaidg.R;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class WalletTransactionActivityPresenter implements WalletTransactionContract.WalletTransactionPresenter
{

    private final String TAG = "WalletTransProvider";

    @Inject
    SessionManagerImpl preferenceHelperDataSource;
    @Inject Context mContext;
    @Inject WalletTransactionContract.WalletTrasactionView view;
    @Inject
    LSPServices networkService;
   // @Inject NetworkStateHolder networkStateHolder;
    @Inject Gson gson;


    @Inject
    WalletTransactionActivityPresenter()
    {
    }

    /**
     * <h2>initLoadTransactions</h2>
     * <p> method to init the getTransactionsHistory() api call if network connectivity is there </p>
     * @param isToLoadMore: true if is from load more option
     * @param isFromOnRefresh: true if it is to refresh
     */
    public void initLoadTransactions(boolean isToLoadMore, boolean isFromOnRefresh)
    {
//        if( networkStateHolder.isConnected())
//        {
            if(!isFromOnRefresh)
            {
//                view.showProgressDialog(mContext.getString(R.string.pleaseWait));
                getTransactionHistory();
            }else
                getTransactionHistory();

//        }
//        else
//        {
//            view.noInternetAlert();
//        }
    }


    @Override
    public void showToastNotifier(String msg, int duration)
    {
      if(view != null) {
        view.showToast(msg, duration);
      }
    }


    /**
     * <h2>get Wallet History</h2>
     * <p>this method is using to get the Wallet history data</p>
     */
    private void getTransactionHistory()
    {

        view.showProgressDialog(mContext.getString(R.string.pleaseWait));
        Observable<Response<ResponseBody>> request = networkService.getWalletTransaction(
                LSPApplication.getInstance().getAuthToken(preferenceHelperDataSource.getSID()),
                Constants.selLang,Constants.PLATFORM_ANDROID,0);

        request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>()
                {
                  @Override
                  public void onSubscribe(Disposable d) {

                  }

                    @Override
                    public void onNext(Response<ResponseBody> value)
                    {
                        Log.d(TAG, " getWalletTrans onNext: "+value.code());
                        try
                        {
                            switch (value.code())
                            {
                                case 200:
                                    //   String response= DataParser.fetchSuccessResponse(value);
                                    String response =value.body().string();
                                    Log.d(TAG, " getWalletTrans onNext: "+response);
                                    handleResponse(response);
                                    break;
                                default:
                                    break;
                            }
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }

                    }
                    @Override
                    public void onError(Throwable e) {
                       Log.d(TAG, "getWalletTrans error: "+e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                      if(view != null) {
                        view.hideProgressDialog();
                      }

                    }
                });
    }


    /**
     * <h2>Response Handler</h2>
     * <p>this method is using to  handle the Server Response</p>
     * @param response server response
     */
    private void handleResponse(String response)
    {
        try
        {
            OldWalletTransPojo oldWalletTransPojo = gson.fromJson(response, OldWalletTransPojo.class);
          if(view != null) {
            view.setAllTransactionsAL(oldWalletTransPojo.getData().getCreditDebitArr());
            view.setCreditTransactionsAL(oldWalletTransPojo.getData().getCreditArr());
            view.setDebitTransactionsAL(oldWalletTransPojo.getData().getDebitArr());
            view.setPaymentTransactionsAL(oldWalletTransPojo.getData().getPaymentArr());
            view.walletTransactionsApiSuccessViewNotifier();
          }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}