package com.vaidg.zendesk.zendeskpojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h2>SpinnerRowItem</h2>
 * Created by Ali on 12/29/2017.
 */

public class SpinnerRowItem implements Serializable
{
    @SerializedName("colorId")
    @Expose
    private int colorId;
    @SerializedName("priority")
    @Expose
    private String priority;

    public SpinnerRowItem(int colorId, String priority) {
        this.colorId = colorId;
        this.priority = priority;
    }

    public int getColorId() {
        return colorId;
    }

    public String getPriority() {
        return priority;
    }
}
