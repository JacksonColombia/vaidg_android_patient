package com.vaidg.zendesk.zendeskTicketDetails;

import com.vaidg.Dagger2.ActivityScoped;
import com.vaidg.zendesk.zendeskadapter.HelpIndexRecyclerAdapter;

import dagger.Module;
import dagger.Provides;

/**
 * <h2>HelpIndexAdapterModule</h2>
 * Created by Ali on 2/26/2018.
 */
@Module
public class HelpIndexAdapterModule
{
    @ActivityScoped
    @Provides
    HelpIndexRecyclerAdapter provideHelpAdapter()
    {
     return new  HelpIndexRecyclerAdapter();
    }
}
