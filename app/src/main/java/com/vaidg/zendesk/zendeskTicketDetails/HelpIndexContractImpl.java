package com.vaidg.zendesk.zendeskTicketDetails;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;

import com.vaidg.utilities.LSPApplication;
import com.google.gson.Gson;
import com.vaidg.R;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.vaidg.zendesk.zendeskpojo.ZendeskHistory;
import com.utility.RefreshToken;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import com.pojo.ErrorHandel;
import retrofit2.Response;

/**
 * <h2>HelpIndexContractImpl</h2>
 * Created by Ali on 2/26/2018.
 */

public class HelpIndexContractImpl implements HelpIndexContract.presenter
{
    @Inject
    LSPServices lspServices;
    @Inject HelpIndexContract.HelpView helpView;
    @Inject Gson gson;
    @Inject
    SessionManagerImpl manager;
    @Inject
    public HelpIndexContractImpl() {
    }

    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }

    @Override
    public void onPriorityImage(Context mContext, String priority, ImageView ivHelpCenterPriorityPre) {

        if(priority.equalsIgnoreCase(mContext.getString(R.string.priorityUrgent)))
            ivHelpCenterPriorityPre.setBackgroundColor(Utility.getColor(mContext,R.color.greenContinue));
        else if(priority.equalsIgnoreCase(mContext.getString(R.string.priorityHigh)))
            ivHelpCenterPriorityPre.setBackgroundColor(Utility.getColor(mContext,R.color.livemblue3498));
        else if(priority.equalsIgnoreCase(mContext.getString(R.string.priorityNormal)))
            ivHelpCenterPriorityPre.setBackgroundColor(Utility.getColor(mContext,R.color.redGoogle));
        else if(priority.equalsIgnoreCase(mContext.getString(R.string.priorityLow)))
            ivHelpCenterPriorityPre.setBackgroundColor(Utility.getColor(mContext,R.color.saffron));

    }

    @Override
    public void callApiToCommentOnTicket(String trim, int zenId)
    {
      Map<String, Object> jsonParams = new HashMap<>();
      jsonParams.put("id", zenId);
      jsonParams.put("body", trim);
      jsonParams.put("author_id", manager.getRegisterId());

        Observable<Response<ResponseBody>>observable = lspServices.commentOnTicket(LSPApplication.getInstance().getAuthToken(manager.getSID()),
                Constants.selLang,Constants.PLATFORM_ANDROID,jsonParams);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d)
                    {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {

                        int code = responseBodyResponse.code();

                        switch (code)
                        {
                            case Constants.SUCCESS_RESPONSE:

                                break;
                            case Constants.SESSION_LOGOUT:

                                break;
                            case Constants.SESSION_EXPIRED:

                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void callApiToCreateTicket(final String trim, final String subject, final String priority)
    {

      Map<String, Object> jsonParams = new HashMap<>();
      jsonParams.put("subject", subject);
      jsonParams.put("body", trim);
      jsonParams.put("status", "open");
      jsonParams.put("priority", priority);
      jsonParams.put("type", "problem");
      jsonParams.put("requester_id", manager.getRegisterId());

        Observable<Response<ResponseBody>>observable = lspServices.createTicket(LSPApplication.getInstance().getAuthToken(manager.getSID()),
                Constants.selLang,Constants.PLATFORM_ANDROID,jsonParams);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse)
                    {
                        int code = responseBodyResponse.code();
                        String response;
                        try {


                            switch (code)
                            {
                                case Constants.SUCCESS_RESPONSE:
                                    response = responseBodyResponse.body().string();
                                    Log.d("TAG", "RESULTonSuccess: "+response);
                                    helpView.onZendeskTicketAdded(response);
                                    break;
                                case Constants.SESSION_LOGOUT:
                                    break;
                                case Constants.SESSION_EXPIRED:
                                    response = responseBodyResponse.errorBody().string();
                                    ErrorHandel errorHandel = gson.fromJson(response,ErrorHandel.class);

                                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),manager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {

                                            LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                            callApiToCreateTicket(trim, subject, priority);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {

                                        }

                                        @Override
                                        public void sessionExpired(String msg) {

                                            helpView.onLogout(msg,manager);
                                        }
                                    });
                                    break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void callApiToGetTicketInfo(final int zenId)
    {
        Observable<Response<ResponseBody>> observable = lspServices.onToGetZendeskHistory(LSPApplication.getInstance().getAuthToken(manager.getSID()),
                Constants.selLang,Constants.PLATFORM_ANDROID,zenId);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {

                        int code = responseBodyResponse.code();
                        String response;
                        JSONObject jsonObject;
                        try
                        {


                            switch (code)
                            {
                                case Constants.SUCCESS_RESPONSE:
                                    response = responseBodyResponse.body().string();
                                    Log.d("HELPINDEX", "onNextResponseINFOTICKET: "+response);
                                    ZendeskHistory zendeskHistory = gson.fromJson(response,ZendeskHistory.class);

                                    Date date = new Date(zendeskHistory.getData().getTimeStamp() * 1000L);
                                    String[] dateTime = Utility.getFormattedDate(date).split("|");
                                    String timeToSet =  dateTime[0]+" | "+dateTime[1];
                                    helpView.onTicketInfoSuccess(zendeskHistory.getData().getEvents(),timeToSet,
                                            zendeskHistory.getData().getSubject(),zendeskHistory.getData().getPriority(),zendeskHistory.getData().getType());
                                    helpView.onHideProgress();
                                    break;
                                case Constants.SESSION_LOGOUT:
                                    response = responseBodyResponse.errorBody().string();
                                    jsonObject = new JSONObject(response);
                                    helpView.onLogout(jsonObject.getString("message"),manager);
                                    helpView.onHideProgress();
                                    break;
                                case Constants.SESSION_EXPIRED:
                                    response = responseBodyResponse.errorBody().string();
                                    ErrorHandel errorHandel = gson.fromJson(response,ErrorHandel.class);

                                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),manager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {

                                            LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                            callApiToGetTicketInfo(zenId);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {

                                        }

                                        @Override
                                        public void sessionExpired(String msg) {

                                            helpView.onLogout(msg,manager);
                                        }
                                    });

                                    break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
