package com.vaidg.zendesk.zendeskTicketDetails;

import com.vaidg.Dagger2.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>HelpTicketDetailsModule</h2>
 * Created by Ali on 2/26/2018.
 */
@Module
public interface HelpTicketDetailsModule
{
    @ActivityScoped
    @Binds
    HelpIndexContract.presenter providePresenter(HelpIndexContractImpl helpIndexContract);

    @ActivityScoped
    @Binds
    HelpIndexContract.HelpView provideView(HelpIndexTicketDetails helpIndexTicketDetails);
}
