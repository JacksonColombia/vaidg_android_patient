package com.vaidg.zendesk.zendeskHelpIndex;

import android.content.Intent;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vaidg.R;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.vaidg.zendesk.zendeskTicketDetails.HelpIndexTicketDetails;
import com.vaidg.zendesk.zendeskpojo.OpenClose;
import com.utility.AlertProgress;
import java.util.ArrayList;
import javax.inject.Inject;
import com.vaidg.zendesk.zendeskadapter.HelpIndexAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

public class ZendeskHelpIndex extends DaggerAppCompatActivity implements ZendeskHelpIndexContract.ZendeskView,SwipeRefreshLayout.OnRefreshListener {

    @Inject AppTypeface appTypeface;
    @Inject SessionManagerImpl manager;
    @Inject AlertProgress alertProgress;
    @Inject HelpIndexAdapter helpIndexAdapter;
    @Inject ZendeskHelpIndexContract.Presenter presenter;

    @BindView(R.id.toolbar)Toolbar toolHelpIndex;
    @BindView(R.id.rlHelpIndex)RelativeLayout rlHelpIndex;
    @BindView(R.id.recyclerHelpIndex)RecyclerView recyclerHelpIndex;
    @BindView(R.id.progressbarHelpIndex)ProgressBar progressbarHelpIndex;
    @BindView(R.id.srlZenDesk)SwipeRefreshLayout srlZenDesk;

    private ArrayList<OpenClose> openCloses = new ArrayList<>();
    private int openSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_index);
        ButterKnife.bind(this);
        initializeToolBar();
        initializeView();
    }

    private void initializeView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerHelpIndex.setLayoutManager(layoutManager);
        helpIndexAdapter.onCreateIndex(this,openCloses);
        recyclerHelpIndex.setAdapter(helpIndexAdapter);
        onShowProgress();
        presenter.onToGetZendeskTicket();
        onRefreshing(false);
        srlZenDesk.setOnRefreshListener(this);
    }

    /*
    initialize toolBar
     */
    private void initializeToolBar()
    {

        setSupportActionBar(toolHelpIndex);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView tvHelpCenter = findViewById(R.id.tv_center);
        TextView tvAddNewTicket = findViewById(R.id.tv_skip);

        getSupportActionBar().setTitle("");
        tvHelpCenter.setText(R.string.helpcenter);
        tvAddNewTicket.setVisibility(View.VISIBLE);
        tvAddNewTicket.setText("+");
        tvAddNewTicket.setTextSize(20);
        tvAddNewTicket.setTextColor(Utility.getColor(this,R.color.parrotGreen));
        tvHelpCenter.setTypeface(appTypeface.getHind_semiBold());
        tvAddNewTicket.setTypeface(appTypeface.getHind_semiBold());
        toolHelpIndex.setNavigationIcon(R.drawable.ic_back);
        toolHelpIndex.setNavigationOnClickListener(v -> onBackPressed());

        tvAddNewTicket.setOnClickListener(view -> {
            Intent intent = new Intent(ZendeskHelpIndex.this,HelpIndexTicketDetails.class);
            intent.putExtra("ISTOAddTICKET",true);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_up,R.anim.stay_still);
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        toolHelpIndex.setVisibility(View.INVISIBLE);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onSessionExpired() {

    }

    @Override
    public void onLogout(String message, SessionManagerImpl sessionManager) {

    }

    @Override
    public void onError(String error)
    {
        alertProgress.alertPositiveOnclick(this,error, getString(R.string.system_error),getString(R.string.ok),
                isClicked -> Utility.setMAnagerWithBID(ZendeskHelpIndex.this,manager));
    }

    @Override
    public void onConnectionError(String connectionError) {

    }

    @Override
    public void onShowProgress()
    {
        progressbarHelpIndex.setVisibility(View.VISIBLE);

    }

    @Override
    public void onHideProgress() {
        progressbarHelpIndex.setVisibility(View.GONE);

    }

    @Override
    public void onGetTicketSuccess() {

    }

    @Override
    public void onEmptyTicket()
    {
        rlHelpIndex.setVisibility(View.VISIBLE);
        recyclerHelpIndex.setVisibility(View.GONE);
    }

    @Override
    public void onTicketStatus(OpenClose openClose, int openCloseSize, boolean isOpenClose)
    {
        openCloses.clear();

        if(isOpenClose)
        {
            openSize = 0;
            openSize = openCloseSize;
        }

       /* else
            closeSize = openCloseSize;*/

        helpIndexAdapter.openCloseSize(openSize);
        openCloses.add(openClose);
    }

    @Override
    public void onNotifyData(ArrayList<OpenClose> alOpenClose)
    {
        openCloses = alOpenClose;
        rlHelpIndex.setVisibility(View.GONE);
        recyclerHelpIndex.setVisibility(View.VISIBLE);
        helpIndexAdapter.onCreateIndex(this,openCloses);
        recyclerHelpIndex.setAdapter(helpIndexAdapter);
        helpIndexAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRefreshing(boolean isRefreshing) {
        srlZenDesk.setRefreshing(isRefreshing);
    }

    @Override
    public void onRefresh()
    {
        Log.d("TAG", "onRefresh: "+srlZenDesk.isRefreshing());
       /* if(!srlZenDesk.isRefreshing())
        {*/
            onRefreshing(true);
            presenter.onToGetZendeskTicket();
       // }

    }
}
