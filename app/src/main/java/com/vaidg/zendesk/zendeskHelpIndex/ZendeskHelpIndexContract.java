package com.vaidg.zendesk.zendeskHelpIndex;

import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;
import com.vaidg.zendesk.zendeskpojo.OpenClose;

import java.util.ArrayList;

/**
 * <h2>ZendeskHelpIndexContract</h2>
 * Created by Ali on 2/26/2018.
 */

public interface ZendeskHelpIndexContract
{
    interface Presenter extends BasePresenter
    {
        void onToGetZendeskTicket();
    }
    interface  ZendeskView extends BaseView
    {
        void onGetTicketSuccess();

        void onEmptyTicket();

        void onTicketStatus(OpenClose openClose, int openCloseSize, boolean isOpenClose);

        void onNotifyData(ArrayList<OpenClose> alOpenClose);
        void onRefreshing(boolean isRefreshing);
    }
}
