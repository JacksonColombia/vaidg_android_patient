package com.vaidg.zendesk.zendeskHelpIndex;

import com.vaidg.Dagger2.ActivityScoped;

import com.vaidg.zendesk.zendeskadapter.HelpIndexAdapter;

import dagger.Module;
import dagger.Provides;

/**
 * <h2>ZendeskAdapterModule</h2>
 * Created by Ali on 2/26/2018.
 */

@Module
public class ZendeskAdapterModule
{
    @ActivityScoped
    @Provides
    HelpIndexAdapter provideHelpIndexAdapter()
    {
        return  new HelpIndexAdapter();
    }
}
