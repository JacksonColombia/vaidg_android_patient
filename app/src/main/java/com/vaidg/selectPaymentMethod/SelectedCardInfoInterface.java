package com.vaidg.selectPaymentMethod;

import android.content.Context;

import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;
import com.vaidg.model.payment_method.CardDetail;
import com.utility.AlertProgress;

import java.util.ArrayList;

/**
 * <h2>SelectedCardInfoInterface</h2>
 * Created by Ali on 3/12/2018.
 */

public interface SelectedCardInfoInterface
{
    interface SelectedPresenter extends BasePresenter
    {
        void onGetCards();

        void getWalletAmount();

        void setCashCardBookingView(int select, double balance, double softLimit, double hardLimit, Context mContext, AlertProgress alertProgress);
    }
    interface SelectedView extends BaseView
    {
        void onToCallIntent();

        void onToBackIntent(int adapterPosition);

        void addItems(ArrayList<CardDetail> cardsList);

        void onVisibilitySet();

        void showWalletAmount(String currencySymbol, double v, double softLimit, double hardLimit);

        void startActivity();

        void paymentSelection(int selectedCell);
    }
}
