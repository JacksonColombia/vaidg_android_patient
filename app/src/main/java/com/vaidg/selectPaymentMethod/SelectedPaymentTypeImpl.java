package com.vaidg.selectPaymentMethod;

import android.content.Context;
import android.util.Log;

import com.vaidg.R;
import com.vaidg.model.payment_method.CardDetail;
import com.vaidg.model.payment_method.CardGetResponse;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.utility.AlertProgress;
import com.utility.RefreshToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h2>SelectedPaymentTypeImpl</h2>
 * Created by Ali on 3/12/2018.
 */

public class SelectedPaymentTypeImpl implements SelectedCardInfoInterface.SelectedPresenter
{
    @Inject
    SessionManagerImpl manager;
    @Inject
    LSPServices lspServices;

    @Inject
    SelectedCardInfoInterface.SelectedView view;

    @Inject
    public SelectedPaymentTypeImpl() {
    }




    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }

    @Override
    public void onGetCards()
    {

        Observable<Response<CardGetResponse>> observable = lspServices.getCard(LSPApplication.getInstance().getAuthToken(manager.getSID()),Constants.selLang,Constants.PLATFORM_ANDROID);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<CardGetResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<CardGetResponse> cardGetResponseResponse)
                    {
                        if (200 == cardGetResponseResponse.code()) {
                            CardGetResponse response = cardGetResponseResponse.body();

                            if (response!=null) {
                                ArrayList<CardDetail> cardsList = response.getData();
                                if (cardsList.size()>0) {
                                    Log.d("TAG", "onNextCARDBODY: "+cardsList.get(0).getBrand()
                                            +" ");
                                  if(view != null) {
                                    view.addItems(cardsList);
                                  }
                                }
                              if(view != null) {
                                view.onHideProgress();
                              }
                            } else {
                              if(view != null) {
                                view.onHideProgress();
                              }
                            }
                        }else if(440 == cardGetResponseResponse.code())
                        {
                            if (cardGetResponseResponse.errorBody()!=null) {
                                JSONObject errJson = null;
                                try {
                                    errJson = new JSONObject(cardGetResponseResponse.errorBody().string());

                                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),manager.getREFRESHAUTH(), lspServices
                                            , new RefreshToken.RefreshTokenImple() {
                                                @Override
                                                public void onSuccessRefreshToken(String newToken) {
                                                    LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                                    //  view.onShowProgress();
                                                    getWalletAmount();
                                                }

                                                @Override
                                                public void onFailureRefreshToken() {

                                                }

                                                @Override
                                                public void sessionExpired(String msg) {
                                                  if(view != null) {
                                                    view.onLogout(msg, manager);
                                                    view.onHideProgress();
                                                  }
                                                }
                                            });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        } else if(498 == cardGetResponseResponse.code())
                        {
                            try {
                            JSONObject errJson = new JSONObject(cardGetResponseResponse.errorBody().string());
                              if(view != null) {
                                view.onLogout(errJson.getString("message"), manager);
                                view.onHideProgress();
                              }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                        else{
                            try {
                                if (cardGetResponseResponse.errorBody()!=null) {
                                    assert cardGetResponseResponse.errorBody() != null;
                                    JSONObject errJson = new JSONObject(cardGetResponseResponse.errorBody().string());
                                    if(view != null) {
                                      view.onError(errJson.getString("message"));
                                      view.onHideProgress();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void getWalletAmount()
    {
        Observable<Response<ResponseBody>> observable = lspServices.getWalletLimits(LSPApplication.getInstance().getAuthToken(manager.getSID())
        , Constants.selLang,Constants.PLATFORM_ANDROID);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {

                  @Override
                  public void onSubscribe(Disposable d) {

                  }
                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse)
                    {


                        Log.d("TAG" , " getWalletLimits onNext: " + responseBodyResponse.code());
                        String responseString;
                        try {
                            switch (responseBodyResponse.code()) {
                                case 200:

                                     responseString = responseBodyResponse.body().string();
                                    Log.d("responseBodyResponse" , " getWalletLimits onNext: "+responseString);
                                    JSONObject profileObject = new JSONObject(responseString);
                                    JSONObject dataObject = profileObject.getJSONObject("data");
                                    String balance = dataObject.getString("walletAmount");
                                    double softLimit = dataObject.getDouble("softLimit");
                                    double hardLimit = dataObject.getDouble("hardLimit");
                                    String currencySymbol=dataObject.getString("currencySymbol");
                                    Constants.walletCurrency = currencySymbol;
                                    Constants.walletAmount = Double.parseDouble(balance);
                                  if(view != null) {
                                    view.showWalletAmount(currencySymbol, Double.parseDouble(balance), softLimit, hardLimit);
                                  }
                                   /* walletView.setBalanceValues(currencySymbol+" "+balance, currencySymbol+" "+hardLimit
                                            , currencySymbol+" "+softLimit);*/


                                    break;
                                case 410:
                                    break;
                                case Constants.SESSION_EXPIRED:
                                    assert responseBodyResponse.errorBody() != null;
                                    responseString = responseBodyResponse.errorBody().string();
                                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),manager.getREFRESHAUTH(), lspServices
                                            , new RefreshToken.RefreshTokenImple() {
                                                @Override
                                                public void onSuccessRefreshToken(String newToken) {
                                                    LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                                  //  view.onShowProgress();
                                                    getWalletAmount();
                                                }

                                                @Override
                                                public void onFailureRefreshToken() {

                                                }

                                                @Override
                                                public void sessionExpired(String msg) {
                                                  if(view != null) {
                                                    view.onLogout(msg, manager);
                                                    view.onHideProgress();
                                                  }
                                                }
                                            });
                                    break;
                                case Constants.SESSION_LOGOUT:
                                    assert responseBodyResponse.errorBody() != null;
                                    responseString = responseBodyResponse.errorBody().string();
                                  if(view != null) {
                                    view.onLogout(new JSONObject(responseString).getString("message"), manager);
                                  }
                                    break;

                                default:
                                    String error = responseBodyResponse.errorBody().string();
                                  if(view != null) {
                                    view.onError(new JSONObject(error).getString("message"));
                                  }
                                    break;
                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private Context mContext;
    private AlertProgress alertProgress;
    @Override
    public void setCashCardBookingView(int select, double balance, double softLimit, double hardLimit, Context mContext, AlertProgress alertProgress)
    {
        this.alertProgress = alertProgress;
        this.mContext = mContext;
        if(balance<softLimit)
        {
            String message = mContext.getResources().getString(R.string.reachedSoftLimit);
            String title = mContext.getResources().getString(R.string.warning);
            openDialog(select,message,title,true);
        }else if(balance<hardLimit)
        {
            String message = mContext.getResources().getString(R.string.reachedHardLimit) + " "+mContext.getResources().getString(R.string.cashBooking);
            String title = mContext.getResources().getString(R.string.error);
            openDialog(0,message,title,false);
        }else {
          if(view != null) { view.paymentSelection(select);}
        }
    }

    private void openDialog(int i, String message, String title, boolean b) {
        alertProgress.alertPositiveNegativeOnclick(mContext, message, title,
                mContext.getResources().getString(R.string.ok), mContext.getResources().getString(R.string.later), false, isClicked -> {

                    if(isClicked) {
                      if (view != null) {
                        view.startActivity();
                      }
                    }else {
                      if (b) {
                        if (view != null) {
                          view.paymentSelection(i);
                        }
                      }
                    }
                });
    }
}
