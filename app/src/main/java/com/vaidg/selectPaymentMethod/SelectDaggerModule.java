package com.vaidg.selectPaymentMethod;

import com.vaidg.Dagger2.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>DaggerModule</h2>
 * Created by Ali on 3/9/2018.
 */

@Module
public interface SelectDaggerModule
{

    @Binds
    @ActivityScoped
    SelectedCardInfoInterface.SelectedView providerView(SelectPayment selectPayment);
    @Binds
    @ActivityScoped
    SelectedCardInfoInterface.SelectedPresenter providePresenter(SelectedPaymentTypeImpl selectedPaymentType);
}
