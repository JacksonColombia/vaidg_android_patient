package com.vaidg.incalloutcalltelecall.questionselection.adapter;


import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.questionselection.model.SymptomResponsePojo;
import com.vaidg.utilities.AppTypeface;
import java.util.ArrayList;


public class CheckBoxTypeQuestionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
  private Context mContext;
  private AppTypeface mAppTypeface;
  private ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> mPreDefineds = new ArrayList<>();

  public CheckBoxTypeQuestionAdapter(
      ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds,
      AppTypeface appTypeface) {
    this.mPreDefineds = preDefineds;
    this.mAppTypeface = appTypeface;
  }

  public void setEmployees(
      ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds) {
    this.mPreDefineds = new ArrayList<>();
    this.mPreDefineds = preDefineds;
    notifyDataSetChanged();
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
    View view = LayoutInflater.from(viewGroup.getContext()).inflate(
        R.layout.item_predefined_chckbox, viewGroup, false);
    mContext = viewGroup.getContext();
    return new CheckBoxTypeViewHolder(view);
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder viewHolder,int position) {

    ((CheckBoxTypeViewHolder) viewHolder).radioButtonBidding.setText(mPreDefineds.get(position).getName());
    ((CheckBoxTypeViewHolder) viewHolder).radioButtonBidding.setTag(position);

    ((CheckBoxTypeViewHolder) viewHolder).radioButtonBidding.setTextColor(
        mPreDefineds.get(position).isChecked() ? mContext.getResources().getColor(
            R.color.colorAccent) : mContext.getResources().getColor(R.color.black));

    if (mPreDefineds.get(position).isChecked()) {
      ((CheckBoxTypeViewHolder) viewHolder).radioButtonBidding.setTypeface(mAppTypeface.getHind_medium());
      ((CheckBoxTypeViewHolder) viewHolder).radioButtonBidding.setCompoundDrawablesWithIntrinsicBounds(
          R.drawable.ic_check, 0, 0, 0);
    } else {
      ((CheckBoxTypeViewHolder) viewHolder).radioButtonBidding.setTypeface(mAppTypeface.getHind_regular());
      ((CheckBoxTypeViewHolder) viewHolder).radioButtonBidding.setCompoundDrawablesWithIntrinsicBounds(
          R.drawable.ic_unselected, 0, 0, 0);
    }
    ((CheckBoxTypeViewHolder) viewHolder).itemView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        mPreDefineds.get(position).setChecked(!mPreDefineds.get(position).isChecked());
        ((CheckBoxTypeViewHolder) viewHolder).radioButtonBidding.setTextColor(
            mPreDefineds.get(position).isChecked() ? mContext.getResources().getColor(
                R.color.colorAccent) : mContext.getResources().getColor(R.color.black));

        if (mPreDefineds.get(position).isChecked()) {
          ((CheckBoxTypeViewHolder) viewHolder).radioButtonBidding.setTypeface(mAppTypeface.getHind_medium());
          ((CheckBoxTypeViewHolder) viewHolder).radioButtonBidding.setCompoundDrawablesWithIntrinsicBounds(
              R.drawable.ic_check, 0, 0, 0);
        } else {
          ((CheckBoxTypeViewHolder) viewHolder).radioButtonBidding.setTypeface(mAppTypeface.getHind_regular());
          ((CheckBoxTypeViewHolder) viewHolder).radioButtonBidding.setCompoundDrawablesWithIntrinsicBounds(
              R.drawable.ic_unselected, 0, 0, 0);
        }
      }
    });

  }

  public ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> getSelected() {
    ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> selected =
        new ArrayList<>();
    for (int i = 0; i < mPreDefineds.size(); i++) {
      if (mPreDefineds.get(i).isChecked()) {
        selected.add(mPreDefineds.get(i));
      }
    }
    return selected;
  }

  @Override
  public int getItemCount() {
    return mPreDefineds == null ? 0 : mPreDefineds.size();
  }

  private class CheckBoxTypeViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {
    private TextView radioButtonBidding;

    public CheckBoxTypeViewHolder(View itemView) {
      super(itemView);
      radioButtonBidding = itemView.findViewById(R.id.radioButtonBidding);
      itemView.setOnLongClickListener(this);
    }

    @Override
    public boolean onLongClick(View view) {

      return false;
    }
  }

}

