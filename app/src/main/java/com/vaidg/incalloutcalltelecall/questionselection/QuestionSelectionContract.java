package com.vaidg.incalloutcalltelecall.questionselection;

import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;

/**
 * <h2>QuestionSelectionContract</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */

public interface QuestionSelectionContract {
  interface QuestionSelectionView extends BaseView {

    /**
     * <h2>deleteVideoPhoto</h2>
     * This method is used to delete paticular image from list
     *
     * @param imagePostion  key of the value  of image added in hashmap
     * @param adapterPosition  position of the list of image added in list
     */
    void deleteImagePhoto(int adapterPosition, int imagePostion);

    /**
     * <h2>deleteVideoPhoto</h2>
     * This method is used to delete paticular video from list
     *
     * @param imagePostion  key of the value  of video added in hashmap
     * @param adapterPosition  position of the list of video added in list
     */
    void deleteVideoPhoto(int adapterPosition, int imagePostion);

  }

  interface QuestionSelectionPresenter extends BasePresenter {


  }
}
