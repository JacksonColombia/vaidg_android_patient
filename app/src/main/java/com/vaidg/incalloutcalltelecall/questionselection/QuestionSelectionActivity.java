package com.vaidg.incalloutcalltelecall.questionselection;

import static com.vaidg.utilities.Constants.isFromDatePicker;
import static com.vaidg.utilities.Constants.isFromNumberPicker;
import static com.vaidg.utilities.Constants.isQuestionHeader;

import adapters.ImageSelctionAdapterGrid;
import adapters.VideoSelctionAdapterGrid;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ClipData;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vaidg.R;
import com.vaidg.bookingtype.DialogTimeFragment;
import com.vaidg.home.AutoFitGridRecyclerView;
import com.vaidg.incalloutcalltelecall.questionselection.adapter.CheckBoxTypeQuestionAdapter;
import com.vaidg.incalloutcalltelecall.questionselection.adapter.DropDownTypeQuestionAdapter;
import com.vaidg.incalloutcalltelecall.questionselection.model.SymptomResponsePojo;
import com.vaidg.utilities.AppPermissionsRunTime;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.HandlePictureEvents;
import com.vaidg.utilities.SessionManager;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.pojo.QuestionImage;
import com.pojo.QuestionVideo;
import com.seekbar.IndicatorSeekBar;
import com.seekbar.OnSeekChangeListener;
import com.seekbar.SeekParams;
import com.utility.AlertProgress;
import com.utility.PermissionsListener;
import com.utility.PermissionsManager;
import dagger.android.support.DaggerAppCompatActivity;
import eu.janmuller.android.simplecropimage.CropImage;
import eu.janmuller.android.simplecropimage.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import javax.inject.Inject;

/**
 * /**
 * <h2>QuestionSelectionActivity</h2>
 * <p>
 * This class is used to select the Question flow, where a customer
 * can book an appointment for doctor.
 * </p>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class QuestionSelectionActivity extends DaggerAppCompatActivity implements
    QuestionSelectionContract.QuestionSelectionView, PermissionsListener,
    DialogTimeFragment.OnFragmentInteractionListener {

  private static final String VIDEO_DIRECTORY = "/VideoSelect";
  private final int REQUEST_CODE_PERMISSION_VIDEO = 1;
  private final int REQUEST_CODE_PERMISSION_IMAGE = 0;
  public ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> mPreDefineds =
      new ArrayList<>();
  @Inject
  AppTypeface mAppTypeface;
  @BindView(R.id.recyclerView)
  RecyclerView mRecyclerView;
  @BindView(R.id.imgrecyclerView)
  RecyclerView imgrecyclerView;
  @Inject
  AppTypeface appTypeface;
  ImageSelctionAdapterGrid questionAdapterGrid;
  VideoSelctionAdapterGrid videoSelctionAdapterGrid;
  @Inject
  QuestionSelectionContract.QuestionSelectionPresenter presenter;
  @Inject
  PermissionsManager permissionsManager;
  @Inject
  AlertProgress mAlertProgress;
  @Inject
  SessionManager mSessionManager;
  @BindView(R.id.tvImgUpload)
  TextView tvImgUpload;
  @BindView(R.id.tvStartDate)
  TextView tvStartDate;
  @BindView(R.id.tvStartDateSelect)
  TextView tvStartDateSelect;
  @BindView(R.id.tvEndDate)
  TextView tvEndDate;
  @BindView(R.id.tvEndDateSelect)
  TextView tvEndDateSelect;
  @BindView(R.id.tvTitle)
  TextView tvTitle;
  @BindView(R.id.tvTitleFee)
  TextView tvTitleFee;
  @BindView(R.id.tvPriceSymbol)
  TextView tvPriceSymbol;
  @BindView(R.id.tvPriceText)
  TextView tvPriceText;
  @BindView(R.id.tvTitlePrice)
  TextView tvTitlePrice;
  @BindView(R.id.tvTime)
  TextView tvTime;
  @BindView(R.id.tvTimeSelect)
  TextView tvTimeSelect;
  @BindView(R.id.tvMinPick)
  TextView tvMinPick;
  @BindView(R.id.tvMaxPick)
  TextView tvMaxPick;
  @BindView(R.id.imgll)
  LinearLayout imgll;
  @BindView(R.id.timell)
  LinearLayout timell;
  @BindView(R.id.datell)
  LinearLayout datell;
  @BindView(R.id.chckbxll)
  LinearLayout chckbxll;
  @BindView(R.id.numberpickerll)
  LinearLayout numberpickerll;
  @BindView(R.id.txtCommaSeparatedll)
  LinearLayout txtCommaSeparatedll;
  @BindView(R.id.llMyListEdit)
  LinearLayout llMyListEdit;
  @BindView(R.id.videoll)
  LinearLayout videoll;
  @BindView(R.id.tvAddDynamic)
  TextView tvAddDynamic;
  @BindView(R.id.indicatorSeekbar)
  IndicatorSeekBar indicatorSeekbar;
  @BindView(R.id.tvVideoUpload)
  TextView tvVideoUpload;
  @BindView(R.id.tvVideoTitle)
  TextView tvVideoTitle;
  @BindView(R.id.videorecyclerView)
  AutoFitGridRecyclerView videorecyclerView;
  @BindView(R.id.toolbar)
  Toolbar toolBookingType;
  @BindView(R.id.tv_skip)
  TextView btnSave;
  @BindView(R.id.tv_center)
  TextView tv_center;
  DropDownTypeQuestionAdapter singleSelectionRecyclerAdapter;
  HashMap<Integer, String> imageArray;
  HashMap<Integer, String> videoArray;
  private CheckBoxTypeQuestionAdapter mCheckBoxTypeQuestionAdapter;
  private LinearLayoutManager layoutManager;
  private HandlePictureEvents handlePicEvent;
  private int imagePositionTaken = 0;
  private ArrayList<QuestionImage> questionImages;
  private ArrayList<QuestionVideo> questionVideo;
  private int mYearStart, mMonthStart, mDayStart, mYearEnd, mMonthEnd, mDayEnd, mHour, mMinute;
  private long mMinDate = 0;
  private int seekbarPos = 0;
  private LayoutInflater inflater;
  private ArrayList<EditText> etMyListEditDynamics = new ArrayList<>();


  /**
   * <h2>getPath</h2>
   * <p> method to get video path</p>
   *
   * @param context Context of the activity
   * @param uri     return path for video which has been selected from gallery
   */
  public static String getPath(final Context context, final Uri uri) {

    final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

    // DocumentProvider
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
      if (DocumentsContract.isDocumentUri(context, uri)) {
        // ExternalStorageProvider
        if (isExternalStorageDocument(uri)) {
          final String docId = DocumentsContract.getDocumentId(uri);
          final String[] split = docId.split(":");
          final String type = split[0];

          if ("primary".equalsIgnoreCase(type)) {
            return context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)+ "/" + split[1];
          }

          // TODO handle non-primary volumes
        }
        // DownloadsProvider
        else if (isDownloadsDocument(uri)) {

          final String id = DocumentsContract.getDocumentId(uri);
          final Uri contentUri = ContentUris.withAppendedId(
              Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

          return getDataColumn(context, contentUri, null, null);
        }
        // MediaProvider
        else if (isMediaDocument(uri)) {
          final String docId = DocumentsContract.getDocumentId(uri);
          final String[] split = docId.split(":");
          final String type = split[0];

          Uri contentUri = null;
          if ("image".equals(type)) {
            contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
          } else if ("video".equals(type)) {
            contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
          } else if ("audio".equals(type)) {
            contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
          }

          final String selection = "_id=?";
          final String[] selectionArgs = new String[]{
              split[1]
          };

          return getDataColumn(context, contentUri, selection, selectionArgs);
        }
      }
      // MediaStore (and general)
      else if ("content".equalsIgnoreCase(uri.getScheme())) {

        // Return the remote address
        if (isGooglePhotosUri(uri)) {
          return uri.getLastPathSegment();
        }

        return getDataColumn(context, uri, null, null);
      }
      // File
      else if ("file".equalsIgnoreCase(uri.getScheme())) {
        return uri.getPath();
      }
    }

    return null;
  }

  /**
   * <h2>getDataColumn</h2>
   * <p> method to get video path</p>
   *
   * @param context Context of the activity
   * @param uri     return path for video which has been selected from gallery
   */
  public static String getDataColumn(Context context, Uri uri, String selection,
      String[] selectionArgs) {

    final String column = "_data";
    final String[] projection = {
        column
    };
    try (Cursor cursor = context.getContentResolver().query(uri, projection, selection,
        selectionArgs,
        null)) {
      if (cursor != null && cursor.moveToFirst()) {
        final int index = cursor.getColumnIndexOrThrow(column);
        return cursor.getString(index);
      }
    }
    return null;
  }

  /**
   * @param uri The Uri to check.
   * @return Whether the Uri authority is ExternalStorageProvider.
   */
  public static boolean isExternalStorageDocument(Uri uri) {
    return "com.android.externalstorage.documents".equals(uri.getAuthority());
  }

  /**
   * @param uri The Uri to check.
   * @return Whether the Uri authority is DownloadsProvider.
   */
  public static boolean isDownloadsDocument(Uri uri) {
    return "com.android.providers.downloads.documents".equals(uri.getAuthority());
  }

  /**
   * @param uri The Uri to check.
   * @return Whether the Uri authority is MediaProvider.
   */
  public static boolean isMediaDocument(Uri uri) {
    return "com.android.providers.media.documents".equals(uri.getAuthority());
  }

  /**
   * @param uri The Uri to check.
   * @return Whether the Uri authority is Google Photos.
   */
  public static boolean isGooglePhotosUri(Uri uri) {
    return "com.google.android.apps.photos.content".equals(uri.getAuthority());
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_question_selection);
    ButterKnife.bind(this);
    comingFromBundle();
    toolBarSetting();

  }

  private void toolBarSetting() {
    //getString(R.string.bookingType)

    setSupportActionBar(toolBookingType);
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayShowHomeEnabled(true);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
    toolBookingType.setNavigationIcon(R.drawable.ic_back);
    toolBookingType.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });
    btnSave.setTypeface(mAppTypeface.getHind_medium());
    btnSave.setTextSize(TypedValue.COMPLEX_UNIT_PX,
        getResources().getDimension(R.dimen.dp_14));
    btnSave.setVisibility(View.VISIBLE);
    btnSave.setText(getResources().getString(R.string.save));
  }

  private void comingFromBundle() {

      /*  Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
*/
    Intent intent = getIntent();
    Bundle args = intent.getBundleExtra("BUNDLE");
    if (args.getBoolean(Constants.isFromChckBox)) {
      mPreDefineds =
          (ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined>) args.getSerializable(
              "ARRAYLIST");
      setDataForCheckBox(args.getString(isQuestionHeader));
    } else if (args.getBoolean(isFromDatePicker)) {
      setDataForDatePicker(args.getString(isQuestionHeader));
    } else if (args.getBoolean(isFromNumberPicker)) {
      //Log.d("isFromNumberPicker", "comingFromBundle: "+args.getString(ConstantsInteface
      // .isQuestionTitle)+"   "+args.getString(ConstantsInteface.isMax)+"  "+args.getString(ConstantsInteface
      // .isMin));
      setDataForRangePicker(args.getString(isQuestionHeader),
          args.getString(Constants.isQuestionTitle),
          args.getString(Constants.isMin), args.getString(Constants.isMax));
    } else if (args.getBoolean(Constants.isFromTime)) {
      setDataForTime(args.getString(isQuestionHeader));
    } else if (args.getBoolean(Constants.isFromTextAreaCommaSeparated)) {
      setDatatxtCommaSeparated(args.getString(isQuestionHeader),
          args.getString(Constants.isQuestionTitle));
    } else if (args.getBoolean(Constants.isFromDropDown)) {
      mPreDefineds =
          (ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined>) args.getSerializable(
              "ARRAYLIST");
      setDataForDropDown(args.getString(isQuestionHeader));
    } else if (args.getBoolean(Constants.isFromVideoUpload)) {
      setDataForVideoUpload(args.getString(isQuestionHeader),
          args.getString(Constants.isQuestionTitle),
          args.getString(Constants.isMin), args.getString(Constants.isMax));
    } else {
      setDataForImageUpload(args.getString(isQuestionHeader),
          args.getString(Constants.isQuestionTitle),
          args.getString(Constants.isMin), args.getString(Constants.isMax));
    }


    //


  }

  private void setDataForVideoUpload(String header, String name, String min, String max) {

    videoll.setVisibility(View.VISIBLE);
    setHeader(header);
    questionVideo = new ArrayList<>();
    tvVideoTitle.setTypeface(mAppTypeface.getHind_regular());
    tvVideoUpload.setTypeface(mAppTypeface.getHind_regular());
    tvVideoTitle.setText(name);

    // videoArray = new HashMap<>();
    imagePositionTaken = 0;
       /* if (imagePositionTaken == -1){
            posimage = 0;
        }
        if(questionImages.size() -1 == -1)
            pos = 0;
        questionImages.add(pos,new QuestionImage("",false,posimage));*/
    Log.d("Index", "onActivityResult: " + questionVideo.size() + "   " + imagePositionTaken);
    videorecyclerView.setHasFixedSize(true);
    videoSelctionAdapterGrid = new VideoSelctionAdapterGrid( questionVideo);
    GridLayoutManager linearLayoutManager = new GridLayoutManager(this, 2,
        GridLayoutManager.VERTICAL, false);
    videorecyclerView.setLayoutManager(linearLayoutManager);
    videorecyclerView.setItemAnimator(new DefaultItemAnimator());
    videorecyclerView.setAdapter(videoSelctionAdapterGrid);
    // this is needed if you are working with CollapsingToolbarLayout, I am adding this here just
    // in case I forget.
    // mRecyclerView.setNestedScrollingEnabled(false);
    // ViewCompat.setNestedScrollingEnabled(videorecyclerView, false);

    tvVideoUpload.setOnClickListener(view -> {
      if (questionVideo.size() < Integer.parseInt(max)) {
        checkPermissionVideo();
      } else {
        mAlertProgress.alertinfo(this, "Max Limit Video");
      }
    });

    btnSave.setOnClickListener(v -> {
      StringBuilder stringBuilder = new StringBuilder();
      if (questionVideo.size() > 0) {
        for (QuestionVideo questionImage : questionVideo) {
          stringBuilder.append(questionImage.getImage());
          stringBuilder.append(",");
        }
        Intent i = new Intent();
        Bundle args = new Bundle();
        args.putSerializable("QuestionVideos", questionVideo);
        i.putExtra("BUNDLE", args);
        i.putExtra("Path", stringBuilder.toString().trim());
        setResult(RESULT_OK, i);
        finish();
      } else {
        mAlertProgress.alertinfo(this, "Please select atleast one video");
      }

    });

  }

  private void setDataForDropDown(String header) {
    chckbxll.setVisibility(View.VISIBLE);
    setHeader(header);
    mRecyclerView.setHasFixedSize(true);
    layoutManager = new LinearLayoutManager(this);
    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
    mRecyclerView.setLayoutManager(layoutManager);
    mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
    final LayoutAnimationController controller =
        AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_right_to_left);

    mRecyclerView.setLayoutAnimation(controller);
    /*
     * RecyclerView: Implementing single item click and long press (Part-II)
     * */
        /*
                    @Override
                    public void onLongClick(View view, int position) {
                        Toast.makeText(MainActivity.this, "Long press on position :"+position,
                                Toast.LENGTH_LONG).show();
                    }
        */
    mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
        (view, position) -> {
          //Values are passing to activity & to fragment as well
          btnSave.setOnClickListener(v -> {

            Intent i = new Intent();
            i.putExtra("DROPDOWN", mPreDefineds.get(position).getName());
            i.putExtra("ID", mPreDefineds.get(position).get_id());
            setResult(RESULT_OK, i);
            finish();
    /*                if (adapter.getSelected().size() > 0) {
                    StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 0; i < adapter.getSelected().size(); i++) {
                        stringBuilder.append(adapter.getSelected().get(i).getName());
                        stringBuilder.append(",");


    *//*
                        stringBuilder.append("\n");
    *//*
                    }

                    Intent i = new Intent();
                    i.putExtra("CARDID", stringBuilder.toString().trim());
                    setResult(RESULT_OK, i);
                    finish();
                    Log.d("RESULT_OK", "setDataForCheckBox: " + stringBuilder.toString().trim());
                } else {
                    Log.d("RESULT_OK", "setDataForCheckBox: " + "No Selection");

                }*/
          });

        }));
    singleSelectionRecyclerAdapter = new DropDownTypeQuestionAdapter( mPreDefineds,
        mAppTypeface);
    // adapter.SetOnItemClickListener(mItemClickListener);
    mRecyclerView.setAdapter(singleSelectionRecyclerAdapter);
    //  adapter.setEmployees(mPreDefineds);

    // this is needed if you are working with CollapsingToolbarLayout, I am adding this here just
    // in case I forget.
    // mRecyclerView.setNestedScrollingEnabled(false);
    ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);

  }

  private void addRemoveDynamicView(String value) {
    final View singleRowMyListEdit = inflater.inflate(R.layout.single_row_my_list_edit, (ViewGroup)null);
    final EditText etMyListEditDynamic = singleRowMyListEdit.findViewById(R.id.etMyListEditDynamic);

    ImageView ivDelete = singleRowMyListEdit.findViewById(R.id.ivDelete);
    llMyListEdit.addView(singleRowMyListEdit);
    etMyListEditDynamic.setHint(value.trim());
    etMyListEditDynamics.add(etMyListEditDynamic);
    ivDelete.setOnClickListener(view -> {
      llMyListEdit.removeView(singleRowMyListEdit);
      etMyListEditDynamics.remove(etMyListEditDynamic);
    });
  }

  private void setDatatxtCommaSeparated(String header, String descripton) {
    txtCommaSeparatedll.setVisibility(View.VISIBLE);
    setHeader(header);
    inflater = getLayoutInflater();
       /* String[] etFields = data.split(",");
        for (String field : etFields) {
            addRemoveDynamicView(field);
        }*/

    tvAddDynamic.setVisibility(View.VISIBLE);
    tvAddDynamic.setTypeface(appTypeface.getHind_regular());
    tvAddDynamic.setText(getString(R.string.add) /*+" "+tvTitle.getText().toString()*/);
    tvAddDynamic.setOnClickListener(view -> addRemoveDynamicView(descripton));

    btnSave.setOnClickListener(view -> {
      getValuesFromDynamicFields(etMyListEditDynamics);

      Intent i = new Intent();
      i.putExtra("TEXTAREACOMMASEPARATED", getValuesFromDynamicFields(etMyListEditDynamics));
      setResult(RESULT_OK, i);
      finish();
    });
  }

  private void setDataForTime(String header) {
    timell.setVisibility(View.VISIBLE);
    setHeader(header);
    tvTime.setTypeface(mAppTypeface.getHind_regular());
    tvTimeSelect.setTypeface(mAppTypeface.getHind_regular());
    tvTimeSelect.setOnClickListener(v -> {
      if (mHour == 0 || mMinute == 0) {        // Get Current Time
        final Calendar c = Calendar.getInstance();
        c.setTimeZone(Utility.getTimeZone());
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
      }
      // Launch Time Picker Dialog
      TimePickerDialog timePickerDialog = new TimePickerDialog(QuestionSelectionActivity.this,
          (view, hourOfDay, minute) -> {
            mHour = hourOfDay;
            mMinute = minute;

            String amPm;
            if (hourOfDay >= 12) {
              amPm = "PM";
            } else {
              amPm = "AM";
            }
            tvTimeSelect.setText(
                String.format("%s %s", String.format(Locale.ENGLISH, "%02d:%02d", mHour, mMinute),
                    amPm));
          }, mHour, mMinute, false);
      timePickerDialog.show();

    });

    btnSave.setOnClickListener(v -> {

      String time = tvTimeSelect.getText().toString().trim();
      Intent i = new Intent();
      i.putExtra("TIME", time);
      setResult(RESULT_OK, i);
      finish();
    });


  }

  @SuppressLint("SetTextI18n")
  private void setDataForRangePicker(String header, String title, String min, String max) {

    numberpickerll.setVisibility(View.VISIBLE);
    setHeader(header);
    tvTitleFee.setTypeface(mAppTypeface.getHind_medium());
    tvPriceText.setTypeface(mAppTypeface.getHind_regular());
    tvPriceSymbol.setTypeface(mAppTypeface.getHind_regular());
    tvTitlePrice.setTypeface(mAppTypeface.getHind_medium());
    tvMinPick.setTypeface(mAppTypeface.getHind_medium());
    tvMaxPick.setTypeface(mAppTypeface.getHind_medium());
    tvTitleFee.setText(title);
    indicatorSeekbar.setMax(Float.parseFloat(max));
    indicatorSeekbar.setMin(Float.parseFloat(min));
    indicatorSeekbar.setProgress(Float.parseFloat(max) / 2);
    seekbarPos = indicatorSeekbar.getProgress();
    tvPriceText.setText(Integer.toString(seekbarPos));

    indicatorSeekbar.setOnSeekChangeListener(new OnSeekChangeListener() {
      @Override
      public void onSeeking(SeekParams seekParams) {
        Log.d("Seekbar",
            "setDataForRangePicker: " + seekParams.progress + "  " + seekParams.thumbPosition + "  "
                + seekParams.tickText);
        seekbarPos = seekParams.progress;
        tvPriceText.setText(Integer.toString(seekbarPos));

      }

      @Override
      public void onStartTrackingTouch(IndicatorSeekBar seekBar) {

      }

      @Override
      public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

      }
    });

    btnSave.setOnClickListener(view -> {
      Intent i = new Intent();
      i.putExtra("NUMBERSLIDER", tvPriceText.getText().toString().trim());
      setResult(RESULT_OK, i);
      finish();
    });

    Log.d("Seekbar", "setDataForRangePicker: " + indicatorSeekbar.getIndicator() + "   "
        + indicatorSeekbar.getProgress() + "   " + indicatorSeekbar.getMax() + "   "
        + indicatorSeekbar.getMin());
  }

  private void setDataForDatePicker(String header) {

    datell.setVisibility(View.VISIBLE);
    setHeader(header);
    tvStartDateSelect.setTypeface(appTypeface.getHind_regular());
    tvStartDate.setTypeface(appTypeface.getHind_regular());
    tvEndDate.setTypeface(appTypeface.getHind_regular());
    tvEndDateSelect.setTypeface(appTypeface.getHind_regular());

    tvStartDateSelect.setOnClickListener(v -> {
      if (mYearStart == 0 || mMonthStart == 0 || mDayStart == 0) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        c.setTimeZone(Utility.getTimeZone());
        mYearStart = c.get(Calendar.YEAR);
        mMonthStart = c.get(Calendar.MONTH);
        mDayStart = c.get(Calendar.DAY_OF_MONTH);
      }

      DatePickerDialog datePickerDialog = new DatePickerDialog(QuestionSelectionActivity.this,
          (view, year, monthOfYear, dayOfMonth) -> {
            mYearStart = year;
            mMonthStart = monthOfYear;
            mDayStart = dayOfMonth;
            mYearEnd = mYearStart;
            mMonthEnd = mMonthStart;
            mDayEnd = mDayStart;
            Calendar c = Calendar.getInstance();
            c.setTimeZone(Utility.getTimeZone());
            c.set(mYearStart, mMonthStart, mDayStart);
            Log.d("Month", "onDateSet: " + monthOfYear + "    " + (monthOfYear + 1));

            SimpleDateFormat df = new SimpleDateFormat("dd/MMMM/yyyy", Locale.US);
            df.setTimeZone(Utility.getTimeZone());
            String formattedDate = df.format(c.getTime());
            tvStartDateSelect.setText(formattedDate);
            mMinDate = c.getTimeInMillis();
            // tvEndDateSelect.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
          }, mYearStart, mMonthStart, mDayStart);

      datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
      datePickerDialog.show();

    });


    tvEndDateSelect.setOnClickListener(v -> {

      if (mYearEnd == 0 || mMonthEnd == 0 || mDayEnd == 0) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        c.setTimeZone(Utility.getTimeZone());
        mYearEnd = c.get(Calendar.YEAR);
        mMonthEnd = c.get(Calendar.MONTH);
        mDayEnd = c.get(Calendar.DAY_OF_MONTH);
      }


      DatePickerDialog datePickerDialog = new DatePickerDialog(QuestionSelectionActivity.this,
          (view, year, monthOfYear, dayOfMonth) -> {

            mYearEnd = year;
            mMonthEnd = monthOfYear;
            mDayEnd = dayOfMonth;
            Calendar c = Calendar.getInstance();
            c.setTimeZone(Utility.getTimeZone());
            c.set(mYearEnd, mMonthEnd, mDayEnd);
            Log.d("Month", "onDateSet: " + monthOfYear + "    " + (monthOfYear + 1));

            SimpleDateFormat df = new SimpleDateFormat("dd/MMMM/yyyy", Locale.US);
            df.setTimeZone(Utility.getTimeZone());
            String formattedDate = df.format(c.getTime());
            tvEndDateSelect.setText(formattedDate);

            // tvEndDateSelect.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

          }, mYearEnd, mMonthEnd, mDayEnd);
      if(mMinDate == 0)
      {
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() + (1000 * 60 * 60 * 24));
      }else {
        datePickerDialog.getDatePicker().setMinDate(mMinDate + (1000 * 60 * 60 * 24));
      }
      datePickerDialog.show();
    });


    btnSave.setOnClickListener(v -> {

      String dateAppend = tvStartDateSelect.getText().toString().trim() + " - "
          + tvEndDateSelect.getText().toString().trim();
      Intent i = new Intent();
      i.putExtra("DATE_APPEND", dateAppend);
      setResult(RESULT_OK, i);
      finish();
    });

  }

  private void setDataForImageUpload(String header, String name, String min, String max) {
    imgll.setVisibility(View.VISIBLE);
    setHeader(header);
    handlePicEvent = new HandlePictureEvents(this);
    questionImages = new ArrayList<>();
    tvTitle.setTypeface(mAppTypeface.getHind_regular());
    tvImgUpload.setTypeface(mAppTypeface.getHind_regular());
    tvTitle.setText(name);

    // imageArray = new HashMap<>();
    imagePositionTaken = 0;
       /* if (imagePositionTaken == -1){
            posimage = 0;
        }
        if(questionImages.size() -1 == -1)
            pos = 0;
        questionImages.add(pos,new QuestionImage("",false,posimage));*/
    Log.d("Index", "onActivityResult: " + questionImages.size() + "   " + imagePositionTaken);
    imgrecyclerView.setHasFixedSize(true);
    questionAdapterGrid = new ImageSelctionAdapterGrid( questionImages);
    GridLayoutManager linearLayoutManager = new GridLayoutManager(this, 2,
        GridLayoutManager.VERTICAL, false);
    imgrecyclerView.setLayoutManager(linearLayoutManager);
    imgrecyclerView.setItemAnimator(new DefaultItemAnimator());
    imgrecyclerView.setAdapter(questionAdapterGrid);

    // this is needed if you are working with CollapsingToolbarLayout, I am adding this here just
    // in case I forget.
    // mRecyclerView.setNestedScrollingEnabled(false);
    ViewCompat.setNestedScrollingEnabled(imgrecyclerView, false);

    tvImgUpload.setOnClickListener(v -> {
      if (questionImages.size() < Integer.parseInt(max)) {
        checkPermission();
      } else {
        mAlertProgress.alertinfo(this, "Max Limit Image");
      }

    });

    btnSave.setOnClickListener(v -> {
      StringBuilder stringBuilder = new StringBuilder();

      if (questionImages.size() > 0) {
        for (QuestionImage questionImage : questionImages) {
          stringBuilder.append(questionImage.getImage());
          stringBuilder.append(",");
        }
        Intent i = new Intent();
        Bundle args = new Bundle();
        args.putSerializable("QuestionImages", questionImages);
        i.putExtra("Path", stringBuilder.toString().trim());
        i.putExtra("BUNDLE", args);
        setResult(RESULT_OK, i);
        finish();
      } else {
        mAlertProgress.alertinfo(this, "Please select atleast one photo");
      }

    });

  }

  /**
   * <h2>checkPermission</h2>
   * <p>checking for the permission for camera and file storage at run time for
   * build version more than 22
   */
  private void checkPermission() {
    if (Build.VERSION.SDK_INT >= 23) {
      // Marshmallow+
      ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList =
          new ArrayList<>();
      myPermissionConstantsArrayList.clear();
      myPermissionConstantsArrayList.add(
          AppPermissionsRunTime.MyPermissionConstants.PERMISSION_CAMERA);
      myPermissionConstantsArrayList.add(
          AppPermissionsRunTime.MyPermissionConstants.PERMISSION_WRITE_EXTERNAL_STORAGE);
      myPermissionConstantsArrayList.add(
          AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_EXTERNAL_STORAGE);
      if (AppPermissionsRunTime.checkPermission(this, myPermissionConstantsArrayList,
          REQUEST_CODE_PERMISSION_IMAGE)) {

        selectImage();
      }
    } else {
      selectImage();
    }
  }

  /**
   * <h2>selectImage</h2>
   *
   * @see HandlePictureEvents
   * This mehtod is used to show the popup where we can select our images.
   */
  private void selectImage() {
    handlePicEvent.openDialog();
  }

  private void setDataForCheckBox(String header) {
    chckbxll.setVisibility(View.VISIBLE);
    setHeader(header);
    mRecyclerView.setHasFixedSize(true);
    layoutManager = new LinearLayoutManager(this);
    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
    mRecyclerView.setLayoutManager(layoutManager);
    mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
    final LayoutAnimationController controller =
        AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_right_to_left);

    mRecyclerView.setLayoutAnimation(controller);
    mCheckBoxTypeQuestionAdapter = new CheckBoxTypeQuestionAdapter( mPreDefineds,
        mAppTypeface);
    // mCheckBoxTypeQuestionAdapter.SetOnItemClickListener(mItemClickListener);
    mRecyclerView.setAdapter(mCheckBoxTypeQuestionAdapter);
    mCheckBoxTypeQuestionAdapter.setEmployees(mPreDefineds);

    // this is needed if you are working with CollapsingToolbarLayout, I am adding this here just
    // in case I forget.
    // mRecyclerView.setNestedScrollingEnabled(false);
    ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);
    btnSave.setOnClickListener(v -> {
      if (mCheckBoxTypeQuestionAdapter.getSelected().size() > 0) {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder stringBuilderId = new StringBuilder();
        for (int i = 0; i < mCheckBoxTypeQuestionAdapter.getSelected().size(); i++) {
          stringBuilder.append(mCheckBoxTypeQuestionAdapter.getSelected().get(i).getName());
          stringBuilderId.append(mCheckBoxTypeQuestionAdapter.getSelected().get(i).get_id());
          stringBuilder.append(",");
          stringBuilderId.append(",");
        }

        Intent i = new Intent();
        i.putExtra("CARDID", stringBuilder.toString().trim());
        i.putExtra("ID", stringBuilderId.toString().trim());
        setResult(RESULT_OK, i);
        finish();
      }
    });
  }

  private void setHeader(String header) {
    tv_center.setTextSize(TypedValue.COMPLEX_UNIT_PX,
        getResources().getDimension(R.dimen.dp_14));
    tv_center.setTypeface(mAppTypeface.getHind_semiBold());
    //tv_center.setMaxLines(1);
    tv_center.setSelected(true);
    tv_center.setText(getResources().getString(R.string.symptomQuestion));
   // tv_center.setText(header);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (resultCode == RESULT_OK) {
      switch (requestCode) {

        case Constants.CAMERA_PIC:
          handlePicEvent.startCropImage(handlePicEvent.newFile);
          break;
        case Constants.GALLERY_PIC:
          if (data != null) {
            handlePicEvent.gallery(data.getData());
          }
          break;
        case Constants.CROP_IMAGE:
          //  onShowProgressPromo();
          String path = data.getStringExtra(CropImage.IMAGE_PATH);
          imagePositionTaken++;
          if (path != null) {
            try {
              File fileExist = new File(path);

              //imageFlag = true;
              // imageArray.put(imagePositionTaken, fileExist.getAbsolutePath());
/*
                            handlePicEvent.uploadToAmazon(ConstantsInteface.Amazonbucket + "/" +
                            ConstantsInteface.AmazonUploadJobImage, fileExist, new ImageUploadedAmazon() {
                                @Override
                                public void onSuccessAdded(String image) {
                                    imageArray.put(imagePositionTaken,image);
                                    //onHideProgressPromo();
                                }

                                @Override
                                public void onerror(String errormsg) {
                                }

                            });
*/
              Log.d("Index",
                  "onActivityResult: " + questionImages.size() + "   " + imagePositionTaken);
              questionImages.add(questionImages.size(),
                  new QuestionImage(path, true, imagePositionTaken,false));
              questionAdapterGrid.notifyDataSetChanged();
            } catch (Exception e) {
              e.printStackTrace();
            }
          }
          break;
        case Constants.GALLERY:
          Log.d("what", "gale");
          // if (data != null) {
          // Uri contentURI = data.getData();
          imagePositionTaken++;
          //  String selectedVideoPath = getPath(contentURI);
          String selectedVideoPath = getSelectedVideos(data);
          Log.d("path", selectedVideoPath);
          if (selectedVideoPath != null) {
            try {
              File fileExist = new File(selectedVideoPath);

              //imageFlag = true;
              // videoArray.put(imagePositionTaken, fileExist.getAbsolutePath());
/*
                            handlePicEvent.uploadToAmazon(ConstantsInteface.Amazonbucket + "/" +
                            ConstantsInteface.AmazonUploadJobImage, fileExist, new ImageUploadedAmazon() {
                                @Override
                                public void onSuccessAdded(String image) {
                                    imageArray.put(imagePositionTaken,image);
                                    //onHideProgressPromo();
                                }

                                @Override
                                public void onerror(String errormsg) {
                                }

                            });
*/
              Log.d("Index",
                  "onActivityResult: " + questionVideo.size() + "   " + imagePositionTaken);
              questionVideo.add(questionVideo.size(),
                  new QuestionVideo(selectedVideoPath, true, imagePositionTaken,true));
              videoSelctionAdapterGrid.notifyDataSetChanged();
              //  saveVideoToInternalStorage(selectedVideoPath);
            } catch (Exception e) {
              e.printStackTrace();
            }
          }

          // }
          break;
        case Constants.CAMERA:
          Uri contentURI = data.getData();
          imagePositionTaken++;
          String recordedVideoPath = getPath(contentURI);
          Log.d("frrr", recordedVideoPath);
          if (recordedVideoPath != null) {
            try {
              File fileExist = new File(recordedVideoPath);

              //imageFlag = true;
              // videoArray.put(imagePositionTaken, fileExist.getAbsolutePath());
/*
                            handlePicEvent.uploadToAmazon(ConstantsInteface.Amazonbucket + "/" +
                            ConstantsInteface.AmazonUploadJobImage, fileExist, new ImageUploadedAmazon() {
                                @Override
                                public void onSuccessAdded(String image) {
                                    imageArray.put(imagePositionTaken,image);
                                    //onHideProgressPromo();
                                }

                                @Override
                                public void onerror(String errormsg) {
                                }

                            });
*/
/*
              Log.d("Index",
                  "onActivityResult: " + questionImages.size() + "   " + imagePositionTaken);
*/
              questionVideo.add(questionVideo.size(),
                  new QuestionVideo(recordedVideoPath, true, imagePositionTaken,true));
              videoSelctionAdapterGrid.notifyDataSetChanged();
              //saveVideoToInternalStorage(recordedVideoPath);
            } catch (Exception e) {
              e.printStackTrace();
            }
          }

          break;

      }
    }
  }

  @Override
  public void onFragmentInteraction(Date uri, boolean isSchedule) {

  }

  @Override
  public void onExplanationNeeded(String[] permissionsToExplain) {

  }

  @Override
  public void onPermissionResult(boolean granted) {
    if (granted) {
      handlePicEvent.openDialog();
    }
  }

  @Override
  public void deleteVideoPhoto(int adapterPosition, int imagePostion) {
    mAlertProgress.alertPositiveNegativeOnclick(this,
        getString(R.string.areYouSureYouWantOTDeleteImage),
        getString(R.string.delete), getString(R.string.ok), getString(R.string.cancel), true,
        isClicked -> {
          if (isClicked) {
            questionVideo.remove(adapterPosition);
            //  videoArray.remove(imagePostion);
            imagePositionTaken--;
            Log.d("Index",
                "onActivityResult: " + questionVideo.size() + "   " + imagePositionTaken);

            videoSelctionAdapterGrid.notifyDataSetChanged();
          }
        });

  }

  @Override
  public void deleteImagePhoto(int adapterPosition, int imagePostion) {
    mAlertProgress.alertPositiveNegativeOnclick(this,
        getString(R.string.areYouSureYouWantOTDeleteImage),
        getString(R.string.delete), getString(R.string.ok), getString(R.string.cancel), true,
        isClicked -> {
          if (isClicked) {
            questionImages.remove(adapterPosition);
            // imageArray.remove(imagePostion);
            imagePositionTaken--;
            Log.d("Index",
                "onActivityResult: " + questionImages.size() + "   " + imagePositionTaken);

            questionAdapterGrid.notifyDataSetChanged();
          }
        });

  }

  @Override
  public void onSessionExpired() {

  }

  @Override
  public void onLogout(String message, SessionManagerImpl sessionManager) {
    mAlertProgress.alertPositiveOnclick(this, message, getString(R.string.logout),
        getString(R.string.ok), isClicked -> Utility.setMAnagerWithBID(this,mSessionManager));

  }

  @Override
  public void onError(String error) {

  }

  @Override
  public void onConnectionError(String connectionError) {

  }

  @Override
  public void onShowProgress() {

  }

  @Override
  public void onHideProgress() {

  }

  String getValuesFromDynamicFields(ArrayList<EditText> etMyListEditDynamics) {
    StringBuilder values = new StringBuilder();
    String prefix = "";
    for (EditText editText : etMyListEditDynamics) {
      if (!editText.getText().toString().equals("")) {
        values.append(prefix);
        prefix = ", ";
        values.append(editText.getText().toString());
      }
    }
    Log.d("etMyListEditDynamics", "getValuesFromDynamicFields: " + values.toString());
    return values.toString();
  }

  /**
   * <h2>checkPermission</h2>
   * <p>checking for the permission for camera and file storage at run time for
   * build version more than 22
   */
  private void checkPermissionVideo() {

    if (Build.VERSION.SDK_INT >= 23) {
      // Marshmallow+
      ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList =
          new ArrayList<>();
      myPermissionConstantsArrayList.clear();
      myPermissionConstantsArrayList.add(
          AppPermissionsRunTime.MyPermissionConstants.PERMISSION_CAMERA);
      myPermissionConstantsArrayList.add(
          AppPermissionsRunTime.MyPermissionConstants.PERMISSION_WRITE_EXTERNAL_STORAGE);
      myPermissionConstantsArrayList.add(
          AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_EXTERNAL_STORAGE);

      if (AppPermissionsRunTime.checkPermission(this, myPermissionConstantsArrayList,
          REQUEST_CODE_PERMISSION_VIDEO)) {

        showVideoDialog();
      }
    } else {
      showVideoDialog();
    }
  }

  /**
   * <h2>showVideoDialog</h2>
   * <p> method to choose a option from gallery or camera</p>
   */
  private void showVideoDialog() {
    AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
    pictureDialog.setTitle("Select Action");
    String[] pictureDialogItems = {
        "Select video from gallery",
        "Record video from camera"};
    pictureDialog.setItems(pictureDialogItems,
        (dialog, which) -> {
          switch (which) {
            case 0:
              chooseVideoFromGallary();
              break;
            case 1:
              takeVideoFromCamera();
              break;
          }
        });
    pictureDialog.show();
  }

  /**
   * <h2>takeVideoFromCamera</h2>
   * <p> method to open a video gallery of a device</p>
   */
  public void chooseVideoFromGallary() {
    if (Build.VERSION.SDK_INT < 19) {
      Intent intent = new Intent();
      intent.setType("video/mp4");
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
      }
      intent.setAction(Intent.ACTION_GET_CONTENT);
      //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
      startActivityForResult(intent, Constants.GALLERY);
    } else {
      Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
      intent.addCategory(Intent.CATEGORY_OPENABLE);
      intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
      intent.setType("video/*");
      //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
      startActivityForResult(intent, Constants.GALLERY);
    }
       /* Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, ConstantsInteface.GALLERY);*/
  }

  /**
   * <h2>takeVideoFromCamera</h2>
   * <p> method to open a video camera of a device</p>
   */
  private void takeVideoFromCamera() {
    Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
    startActivityForResult(intent, Constants.CAMERA);
  }

  /**
   * <h2>saveVideoToInternalStorage</h2>
   * <p> method to save file in device</p>
   *
   * @param filePath save videofile into your device which is capture from camera
   */
  private void saveVideoToInternalStorage(String filePath) {

    File newfile;

    try {

      File currentFile = new File(filePath);
      File wallpaperDirectory = new File(
          Environment.getExternalStorageDirectory() + VIDEO_DIRECTORY);
      newfile = new File(wallpaperDirectory, Calendar.getInstance().getTimeInMillis() + ".mp4");

      if (!wallpaperDirectory.exists()) {
        wallpaperDirectory.mkdirs();
      }

      if (currentFile.exists()) {

        InputStream in = new FileInputStream(currentFile);
        OutputStream out = new FileOutputStream(newfile);

        // Copy the bits from instream to outstream
        byte[] buf = new byte[1024];
        int len;

        while ((len = in.read(buf)) > 0) {
          out.write(buf, 0, len);
        }
        in.close();
        out.close();
        Log.v("vii", "Video file saved successfully.");
      } else {
        Log.v("vii", "Video saving failed. Source file missing.");
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

  }


  /**
   * <h2>getPath</h2>
   * <p> method to get video path</p>
   *
   * @param uri return path for video which has been capture from camera
   */
  public String getPath(Uri uri) {
    String[] projection = {MediaStore.Video.Media.DATA};
    Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
    if (cursor != null) {
      // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
      // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
      int column_index = cursor
          .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
      cursor.moveToFirst();
      return cursor.getString(column_index);
    } else {
      return null;
    }
  }

  /**
   * predefined method to check run time permissions list call back
   *
   * @param requestCode   request code
   * @param permissions:  contains the list of requested permissions
   * @param grantResults: contains granted and un granted permissions result list
   */
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    boolean isDenine = false;
    switch (requestCode) {
      case REQUEST_CODE_PERMISSION_VIDEO:
        for (int grantResult : grantResults) {
          if (grantResult != PackageManager.PERMISSION_GRANTED) {
            isDenine = true;
          }
        }
        if (isDenine) {
          Toast.makeText(this, "Permission denied by the user", Toast.LENGTH_SHORT).show();
        } else {
          showVideoDialog();
        }
        break;
      case REQUEST_CODE_PERMISSION_IMAGE:
        for (int grantResult : grantResults) {
          if (grantResult != PackageManager.PERMISSION_GRANTED) {
            isDenine = true;
          }
        }
        if (isDenine) {
          Toast.makeText(this, "Permission denied by the user", Toast.LENGTH_SHORT).show();
        } else {
          selectImage();
        }
        break;
      default:
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        break;
    }
  }

  /**
   * <h2>getSelectedVideos</h2>
   * <p> method to get selected video path</p>
   *
   * @param data return filePath for video which has been selected from gallery
   */
  private String getSelectedVideos(Intent data) {

    //  List<String> result = new ArrayList<>();
    String filePath = "";
    ClipData clipData = data.getClipData();
    if (clipData != null) {
      for (int i = 0; i < clipData.getItemCount(); i++) {
        ClipData.Item videoItem = clipData.getItemAt(i);
        Uri videoURI = videoItem.getUri();
        filePath = getPath(this, videoURI);
        // result.add(filePath);
      }
    } else {
      Uri videoURI = data.getData();
      filePath = getPath(this, videoURI);
      // result.add(filePath);
    }

    return filePath;
  }

  /**
   * RecyclerView: Implementing single item click and long press (Part-II)
   * <p>
   * - creating an Interface for single tap and long press
   * - Parameters are its respective view and its position
   */

  public interface ClickListener {
    void onClick(View view, int position);
    /*public void onLongClick(View view,int position);*/
  }

  /**
   * RecyclerView: Implementing single item click and long press (Part-II)
   * <p>
   * - creating an innerclass implementing RevyvlerView.OnItemTouchListener
   * - Pass clickListener interface as parameter
   */

  class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

    private ClickListener clicklistener;
    private GestureDetector gestureDetector;

    RecyclerTouchListener(Context context, final ClickListener clicklistener) {

      this.clicklistener = clicklistener;
      gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
        @Override
        public boolean onSingleTapUp(MotionEvent e) {
          return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {
          // View child = recycleView.findChildViewUnder(e.getX(), e.getY());
                    /*if (child != null && clicklistener != null) {
                        // clicklistener.onLongClick(child,recycleView.getChildAdapterPosition
                        (child));
                    }*/
        }
      });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
      View child = rv.findChildViewUnder(e.getX(), e.getY());
      if (child != null && clicklistener != null && gestureDetector.onTouchEvent(e)) {
        clicklistener.onClick(child, rv.getChildAdapterPosition(child));
      }

      return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
  }

  @Override
  protected void onStop() {
    super.onStop();

  }

  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);

    //Utility.checkAndShowNetworkError(this);
  }
}