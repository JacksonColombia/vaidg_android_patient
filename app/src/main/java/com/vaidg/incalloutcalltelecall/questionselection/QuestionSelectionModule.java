package com.vaidg.incalloutcalltelecall.questionselection;

import com.vaidg.Dagger2.ActivityScoped;
import dagger.Binds;
import dagger.Module;

/**
 * <h2>EditRelationModule</h2>
 *
 * This is a Dagger module. We use this to pass in the View dependency to the
 * {@link QuestionSelectionContract.QuestionSelectionPresenter}.
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
@Module
public interface QuestionSelectionModule {

  @ActivityScoped
  @Binds
  QuestionSelectionContract.QuestionSelectionPresenter questionSelectionPresenter(
      QuestionSelectionPresenterImpl questionSelectionPresenterImp);

  @ActivityScoped
  @Binds
  QuestionSelectionContract.QuestionSelectionView questionSelectionView(
      QuestionSelectionActivity questionSelectionActivity);

}