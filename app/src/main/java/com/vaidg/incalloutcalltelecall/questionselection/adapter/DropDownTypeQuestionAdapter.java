package com.vaidg.incalloutcalltelecall.questionselection.adapter;


import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.questionselection.model.SymptomResponsePojo;

import com.vaidg.utilities.AppTypeface;
import java.util.ArrayList;


public class DropDownTypeQuestionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private AppTypeface mAppTypeface;
    private ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> mPreDefineds = new ArrayList<>();
    // if mCheckedPosition = -1, there is no default selection
    // if mCheckedPosition = 0, 1st item is selected by default
    private int mCheckedPosition = -1;

    public DropDownTypeQuestionAdapter(
        ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds,
        AppTypeface appTypeface) {
        this.mPreDefineds = preDefineds;
        this.mAppTypeface = appTypeface;
    }


    private class DropDownViewHolder extends RecyclerView.ViewHolder implements  View.OnLongClickListener {
        private TextView radioButtonBidding;
        private RelativeLayout rvYes;

        public DropDownViewHolder(View itemView) {
            super(itemView);
            radioButtonBidding = itemView.findViewById(R.id.radioButtonBidding);
            // rvYes = itemView.findViewById(R.id.rvYes);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {

            return false;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_predefined_chckbox, viewGroup, false);
        mContext = viewGroup.getContext();
        return new DropDownViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        ((DropDownViewHolder) viewHolder).radioButtonBidding.setText(mPreDefineds.get(position).getName());
        ((DropDownViewHolder) viewHolder).radioButtonBidding.setTag(position);


        if(mCheckedPosition == position || mPreDefineds.get(position).isChecked())
        {
            mPreDefineds.get(position).setChecked(true);
            ((DropDownViewHolder) viewHolder).radioButtonBidding.setTypeface(mAppTypeface.getHind_medium());
            ((DropDownViewHolder) viewHolder).itemView.setSelected(true);
            //   ((DropDownViewHolder) viewHolder).textView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            ((DropDownViewHolder) viewHolder).radioButtonBidding.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
            ((DropDownViewHolder) viewHolder).radioButtonBidding.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check,0,0,0);

        }
        else{
            mPreDefineds.get(position).setChecked(false);
            ((DropDownViewHolder) viewHolder).radioButtonBidding.setTypeface(mAppTypeface.getHind_regular());
            ((DropDownViewHolder) viewHolder).itemView.setSelected(false);
            //   ((DropDownViewHolder) viewHolder).textView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            ((DropDownViewHolder) viewHolder).radioButtonBidding.setTextColor(mContext.getResources().getColor(R.color.black));
            ((DropDownViewHolder) viewHolder).radioButtonBidding.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_unselected,0,0,0);

        }


        ((DropDownViewHolder) viewHolder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < mPreDefineds.size(); i++) {

                    if(i==position) {
                        mPreDefineds.get(i).setChecked(true);
                        ((DropDownViewHolder) viewHolder).radioButtonBidding.setTypeface(mAppTypeface.getHind_medium());

                        ((DropDownViewHolder) viewHolder).itemView.setSelected(true); //using selector drawable
                        ((DropDownViewHolder) viewHolder).radioButtonBidding.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
                        ((DropDownViewHolder) viewHolder).radioButtonBidding.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check,0,0,0);
                    }else{
                        mPreDefineds.get(i).setChecked(false);
                        ((DropDownViewHolder) viewHolder).radioButtonBidding.setTypeface(mAppTypeface.getHind_regular());
                        ((DropDownViewHolder) viewHolder).itemView.setSelected(false);
                        ((DropDownViewHolder) viewHolder).radioButtonBidding.setTextColor(mContext.getResources().getColor(R.color.black));
                        ((DropDownViewHolder) viewHolder).radioButtonBidding.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_unselected,0,0,0);
                    }

                }

                if (mCheckedPosition >= 0)
                    notifyItemChanged(mCheckedPosition);
                mCheckedPosition = viewHolder.getAdapterPosition();
                notifyItemChanged(mCheckedPosition);
            }
        });

    }

    public ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> getSelected() {
        ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> selected = new ArrayList<>();
        for (int i = 0; i < mPreDefineds.size(); i++) {
            if (mPreDefineds.get(i).isChecked()) {
                selected.add(mPreDefineds.get(i));
            }
        }
        return selected;
    }

    @Override
    public int getItemCount() {
        return mPreDefineds == null ? 0 : mPreDefineds.size();
    }

}