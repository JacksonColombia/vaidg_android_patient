package com.vaidg.incalloutcalltelecall.questionselection;

import javax.inject.Inject;

/**
 * <h2>QuestionSelectionPresenterImpl</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class QuestionSelectionPresenterImpl implements
    QuestionSelectionContract.QuestionSelectionPresenter {

  @Inject
  QuestionSelectionContract.QuestionSelectionView questionSelectionView;

  @Inject
  QuestionSelectionPresenterImpl() {
  }

  @Override
  public void attachView(Object view) {

  }

  @Override
  public void detachView() {

  }
}