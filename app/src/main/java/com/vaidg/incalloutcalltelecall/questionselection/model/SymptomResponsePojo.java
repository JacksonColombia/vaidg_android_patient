package com.vaidg.incalloutcalltelecall.questionselection.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;


public class SymptomResponsePojo implements Serializable {

  @SerializedName("message")
  @Expose
  private String message;

  @SerializedName("data")
  @Expose
  private ArrayList<SymptomCategory> data;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public ArrayList<SymptomCategory> getData() {
    return data;
  }

  public class SymptomCategory implements Serializable {
    @SerializedName("_id")
    @Expose
    private String _id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("symptomGroup")
    @Expose
    private String symptomGroup;
    @SerializedName("symptomCategory")
    @Expose
    private String symptomCategory;
    @SerializedName("shortDescription")
    @Expose
    private String shortDescription;
    @SerializedName("detailedDescription")
    @Expose
    private String detailedDescription;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("banner")
    @Expose
    private String banner;
    @SerializedName("num")
    @Expose
    private int num;
    /* private ArrayList<SymptomCategoryNameLang> symptomCategoryNameLang = new ArrayList<>();;
     private ArrayList<ShortDescriptionLang> shortDescriptionLang = new ArrayList<>();;
     private ArrayList<DetailedDescriptionLang> detailedDescriptionLang = new ArrayList<>();;*/
    @SerializedName("isChecked")
    @Expose
    private boolean isChecked = false;
    @SerializedName("questionArr")
    @Expose
    private ArrayList<QuestionArr> questionArr;

    public boolean isChecked() {
      return isChecked;
    }

    public void setChecked(boolean checked) {
      isChecked = checked;
    }

    public ArrayList<QuestionArr> getQuestionArr() {
      return questionArr;
    }

    /*  public ArrayList<DetailedDescriptionLang> getDetailedDescriptionLang() {
          return detailedDescriptionLang;
      }

      public ArrayList<ShortDescriptionLang> getShortDescriptionLang() {
          return shortDescriptionLang;
      }

      public ArrayList<SymptomCategoryNameLang> getSymptomCategoryNameLang() {
          return symptomCategoryNameLang;
      }
*/

    public String get_id() {
      return _id;
    }

    public String getTitle() {
      return title;
    }

    public String getSymptomGroup() {
      return symptomGroup;
    }

    public String getSymptomCategory() {
      return symptomCategory;
    }

    public String getShortDescription() {
      return shortDescription;
    }

    public String getDetailedDescription() {
      return detailedDescription;
    }

    public String getIcon() {
      return icon;
    }

    public String getBanner() {
      return banner;
    }

    public int getNum() {
      return num;
    }

    /* public class SymptomCategoryNameLang implements Serializable{
         private String ar, en;

         public String getAr() {
             return ar;
         }

         public String getEn() {
             return en;
         }
     }

     public class ShortDescriptionLang implements Serializable{
         private String ar, en;

         public String getAr() {
             return ar;
         }

         public String getEn() {
             return en;
         }
     }

     public class DetailedDescriptionLang implements Serializable{
         private String ar, en;

         public String getAr() {
             return ar;
         }

         public String getEn() {
             return en;
         }
     }*/
    public class QuestionArr implements Serializable {
      //questionDescription
      @SerializedName("_id")
      @Expose
      String _id;
      @SerializedName("name")
      @Expose
      String  name;
      @SerializedName("description")
      @Expose
      String  description;
      @SerializedName("minimum")
      @Expose
      String  minimum;
      @SerializedName("maximum")
      @Expose
      String  maximum;
      @SerializedName("unit")
      @Expose
      String  unit;
      @SerializedName("num")
      @Expose
      int num;
      @SerializedName("type")
      @Expose
      int type;
      @SerializedName("isMandatory")
      @Expose
      boolean isMandatory;
      @SerializedName("preDefined")
      @Expose
      private ArrayList<PreDefined> preDefined;

      private boolean isChecked = false;

      private String data;

      private String chckBoxTxt;

      private String dropDownTxt;

      private String datePastToCurrentTxt;

      private String dateCurrentToFutureTxt;

      private String timeTxt;

      private String numberSliderTxt;

      private String textCommaSeparatedTxt;

      private String numberTxt = "";

      private String feeTxt;

      public String getFeeTxt() {
        return feeTxt;
      }

      public void setFeeTxt(String feeTxt) {
        this.feeTxt = feeTxt;
      }

      public String getNumberTxt() {
        return numberTxt;
      }

      public void setNumberTxt(String numberTxt) {
        this.numberTxt = numberTxt;
      }

      public void setName(String name) {
        this.name = name;
      }

      public String getDatePastToCurrentTxt() {
        return datePastToCurrentTxt;
      }

      public void setDatePastToCurrentTxt(String datePastToCurrentTxt) {
        this.datePastToCurrentTxt = datePastToCurrentTxt;
      }

      public String getDateCurrentToFutureTxt() {
        return dateCurrentToFutureTxt;
      }

      public void setDateCurrentToFutureTxt(String dateCurrentToFutureTxt) {
        this.dateCurrentToFutureTxt = dateCurrentToFutureTxt;
      }

      public String getTimeTxt() {
        return timeTxt;
      }

      public void setTimeTxt(String timeTxt) {
        this.timeTxt = timeTxt;
      }

      public String getNumberSliderTxt() {
        return numberSliderTxt;
      }

      public void setNumberSliderTxt(String numberSliderTxt) {
        this.numberSliderTxt = numberSliderTxt;
      }

      public String getTextCommaSeparatedTxt() {
        return textCommaSeparatedTxt;
      }

      public void setTextCommaSeparatedTxt(String textCommaSeparatedTxt) {
        this.textCommaSeparatedTxt = textCommaSeparatedTxt;
      }

      public String getDropDownTxt() {
        return dropDownTxt;
      }

      public void setDropDownTxt(String dropDownTxt) {
        this.dropDownTxt = dropDownTxt;
      }

      public String getChckBoxTxt() {
        return chckBoxTxt;
      }

      public void setChckBoxTxt(String chckBoxTxt) {
        this.chckBoxTxt = chckBoxTxt;
      }

      public String getData() {
        return data;
      }

      public void setData(String data) {
        this.data = data;
      }

      public boolean isChecked() {
        return isChecked;
      }

      public void setChecked(boolean checked) {
        isChecked = checked;
      }

      public String get_id() {
        return _id;
      }

      public String getName() {
        return name;
      }

      public String getDescription() {
        return description;
      }

      public String getMinimum() {
        return minimum;
      }

      public String getMaximum() {
        return maximum;
      }

      public String getUnit() {
        return unit;
      }

      public int getNum() {
        return num;
      }

      public int getType() {
        return type;
      }

      public boolean isMandatory() {
        return isMandatory;
      }

      public ArrayList<PreDefined> getPreDefined() {
        return preDefined;
      }


      public class PreDefined implements Serializable {
        //answerLanArr
        @SerializedName("_id")
        @Expose
        String _id;
        @SerializedName("name")
        @Expose
        String  name;
        @SerializedName("icon")
        @Expose
        String icon;
        @SerializedName("isChecked")
        @Expose
        boolean isChecked = false;



        public boolean isChecked() {
          return isChecked;
        }

        public void setChecked(boolean checked) {
          isChecked = checked;
        }


        public String get_id() {
          return _id;
        }

        public String getName() {
          return name;
        }

        public String getIcon() {
          return icon;
        }
      }
    }

  }
}




