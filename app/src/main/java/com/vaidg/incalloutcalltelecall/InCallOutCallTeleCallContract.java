package com.vaidg.incalloutcalltelecall;

import android.content.Context;
import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;
import com.vaidg.incalloutcalltelecall.questionselection.model.SymptomResponsePojo;
import java.util.ArrayList;
import java.util.Date;

/**
 * <h2>InCallOutCallTeleCallContract</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */

public interface InCallOutCallTeleCallContract {
  interface InCallOutCallTeleCallView extends BaseView {

    /*  void onToTakeImage(int adapterPositio, HandlePictureEvents handlePicEvent, int
    imagePositionTaken, ArrayList<QuestionImage> questionImages, SymptomQuestionsContract
    .SymptomQuestionsView symptomQuestionsView, RecyclerView recyclerViewQuestions,
    ImageTypeAdapterGrid imageTypeAdapterGrid);
      void onHideProgressPromo();
      void onShowProgressPromo();*/

    /**
     * <h2>onResponseSuccess</h2>
     * This method is used to get symptom after the call of APIS
     *
     * @param data list of the symptom
     * @param symptomSelectedID
     */
    void onResponseSuccess(ArrayList<SymptomResponsePojo.SymptomCategory> data,
        String symptomSelectedID);

    void redirectTodoctorList();


    /**
     * <h2>deleteVideoPhoto</h2>
     * This method is used to delete paticular image from list
     *
     * @param imagePostion  key of the value  of image added in hashmap
     * @param adapterPosition  position of the list of image added in list
     */
    void deleteImagePhoto(int adapterPosition, int imagePostion);

    /**
     * <h2>deleteVideoPhoto</h2>
     * This method is used to delete paticular video from list
     *
     * @param imagePostion  key of the value  of video added in hashmap
     * @param adapterPosition  position of the list of video added in list
     */
    void deleteVideoPhoto(int adapterPosition, int imagePostion);



  }

  interface InCallOutCallTeleCallPresenter extends BasePresenter {
    /**
     * <h2>onNowLaterSelected</h2>
     * This method is used to get symptom after the call of APIS
     *
     * @param isNow list of the symptom
     * @param selectedScheduledDateTime selected date and time
     * @param selectedDuration duration of time
     */
    void onNowLaterSelected(boolean isNow, long selectedScheduledDateTime, int selectedDuration);

    /**
     * <h2>onRepeatSelected</h2>
     * This method is used to select the repeated date
     *
     * @param selectedScheduledDateTime selected date and time
     * @param selectedEndDate selected end date
     * @param selectedDuration duration of time
     * @param repeatBooking list of booking list
     */
    void onRepeatSelected(long selectedScheduledDateTime, long selectedEndDate,
        int selectedDuration, ArrayList<String> repeatBooking);

    /**
     * <h2>openGetSymptoms</h2>
     * This method is used to get symptom after the call of APIS
     *
     * @param context list of the symptom
     * @param managerAUTH Token Auth
     * @param symptomSelectedID symptom id
     */
    void openGetSymptoms(Context context, String managerAUTH, String symptomSelectedID);

  }

  interface InCallOutCallTeleCallDateTIme {

    /**
     * <h2>onDateTimeSel</h2>
     * This method is used to set date and time
     *
     * @param uri date selected
     * @param isSchedule is to checked whether it is schedule date or not
     */
    void onDateTimeSel(Date uri, boolean isSchedule);
  }
}
