/*
 *
 *  * Copyright (C) 2014 Antonio Leiva Gordillo.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.vaidg.incalloutcalltelecall.dependent.editrelationship;


import com.vaidg.home.BaseView;
import com.vaidg.incalloutcalltelecall.dependent.model.EditRelationResponsePojo;
import java.util.ArrayList;

/**
 * <h2>EditRelationshipView</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public interface EditRelationshipView extends BaseView {

  /**
   * <h2>showProgress</h2>
   * This method is used to show progress bar on call of APIS
   */
  void showProgress();

  /**
   * <h2>hideProgress</h2>
   * This method is used to hide progress bar after the call of API success
   */
  void hideProgress();


  /**
   * <h2>onResponseSuccess</h2>
   * This method is used to get  Relations of Dependents on Call of APIS
   *
   * @param data list of Dependents
   */
  void onResponseSuccess(ArrayList<EditRelationResponsePojo.EditRelation> data);

}
