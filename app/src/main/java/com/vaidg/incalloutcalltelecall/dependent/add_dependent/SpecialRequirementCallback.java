package com.vaidg.incalloutcalltelecall.dependent.add_dependent;


import com.vaidg.incalloutcalltelecall.dependent.model.SpecialRequirementsData;
import java.util.List;

/**
 * <h2>SpecialRequirementCallback</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public interface SpecialRequirementCallback {
  void onSpecialRequirementsSelected(List<SpecialRequirementsData> specialRequirementsList);
}
