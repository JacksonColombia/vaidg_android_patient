package com.vaidg.incalloutcalltelecall.dependent.model;

import static com.vaidg.utilities.Constants.DATA;
import static com.vaidg.utilities.Constants.MESSAGE;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;


/**
 * <h2>DependentsGet</h2>
 *
 * <p>Get Dependents Response POJO</p>
 *
 * @author 3Embed
 * @since 12-07-2019
 */
public class DependentsGet implements Parcelable {

  public final static Parcelable.Creator<DependentsGet> CREATOR = new Creator<DependentsGet>() {


    @SuppressWarnings({
        "unchecked"
    })
    public DependentsGet createFromParcel(Parcel in) {
      return new DependentsGet(in);
    }

    public DependentsGet[] newArray(int size) {
      return (new DependentsGet[size]);
    }

  };
  @SerializedName(MESSAGE)
  @Expose
  private String message;
  @SerializedName(DATA)
  @Expose
  private ArrayList<DependentsData> data = new ArrayList<>();

  protected DependentsGet(Parcel in) {
    this.message = ((String) in.readValue((String.class.getClassLoader())));
    in.readList(this.data, (DependentsData.class.getClassLoader()));
  }

  /**
   * No args constructor for use in serialization
   */
  public DependentsGet() {
  }

  /**
   * @param data
   * @param message
   */
  public DependentsGet(String message, ArrayList<DependentsData> data) {
    super();
    this.message = message;
    this.data = data;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public ArrayList<DependentsData> getData() {
    return data;
  }

  public void setData(ArrayList<DependentsData> data) {
    this.data = data;
  }

  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(message);
    dest.writeList(data);
  }

  public int describeContents() {
    return 0;
  }

}