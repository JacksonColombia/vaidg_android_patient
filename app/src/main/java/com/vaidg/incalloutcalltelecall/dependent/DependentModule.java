package com.vaidg.incalloutcalltelecall.dependent;

import com.vaidg.Dagger2.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>EditRelationModule</h2>
 * <p>
 * This is a Dagger module. We use this to pass in the View dependency to the
 * {@link  DependentContract.DependentPresenter}.
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
@Module
public interface DependentModule {

    @ActivityScoped
    @Binds
    DependentContract.DependentPresenter dependentSelectionPresenter(
            DependentPresenterImpl dependentSelectionPresenterImp);

    @ActivityScoped
    @Binds
    DependentContract.DependentView dependentSelectionView(
            DependentActivity dependentSelectionActivity);

}