package com.vaidg.incalloutcalltelecall.dependent.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.dependent.add_dependent.SpecialChipAdapterRemoveCallback;
import com.vaidg.incalloutcalltelecall.dependent.model.SpecialRequirementsData;
import java.util.List;

/**
 * <h2>SpecialRequirementChipsAdapter</h2>
 *
 * <p>This is an inner class of adapter type, to populate the specialrequirment on confirm
 * screen.</p>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class SpecialRequirementChipsAdapter extends RecyclerView.Adapter {
  private final boolean isChangeable;
  Context context;
  private SpecialChipAdapterRemoveCallback callback;
  private List<SpecialRequirementsData> specialRequirementsDataList;

  public SpecialRequirementChipsAdapter(Context context, SpecialChipAdapterRemoveCallback callback,
      List<SpecialRequirementsData> items, boolean changeAble) {
    this.context = context;
    this.callback = callback;
    this.specialRequirementsDataList = items;
    this.isChangeable = changeAble;
  }

  public void setList(List<SpecialRequirementsData> items) {
    this.specialRequirementsDataList = items;
    notifyDataSetChanged();
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_special_req_chip, parent, false);
    return new ChipViewHolder(view);
  }

  @SuppressLint("SetTextI18n")
  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    final ChipViewHolder viewHolder = (ChipViewHolder) holder;
    if (specialRequirementsDataList.size() > 0) {
      viewHolder.tv_special_req_name.setText(specialRequirementsDataList.get(position).getName());
    }
  }

  @Override
  public int getItemCount() {
    return specialRequirementsDataList.size();
  }

  /**
   * @param position position to delete
   */
  private void removeAt(int position) {
    callback.onRemove(specialRequirementsDataList.get(position).getId());
    specialRequirementsDataList.remove(position);
    notifyItemRemoved(position);
  }

  private class ChipViewHolder extends RecyclerView.ViewHolder {

    private ImageView iv_clear_special_req;
    private TextView tv_special_req_name;

    private ChipViewHolder(final View itemView) {
      super(itemView);
      iv_clear_special_req = itemView.findViewById(R.id.iv_clear_special_req);
      tv_special_req_name = itemView.findViewById(R.id.tvSpecialReqName);
      if (isChangeable) {
        iv_clear_special_req.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            Log.e("TAG", "onClick: Delete " + getAdapterPosition());
            removeAt(getAdapterPosition());
          }
        });
      } else {
        iv_clear_special_req.setVisibility(View.GONE);
      }

    }
  }
}
