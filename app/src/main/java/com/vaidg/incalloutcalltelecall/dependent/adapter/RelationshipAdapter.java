package com.vaidg.incalloutcalltelecall.dependent.adapter;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.dependent.model.EditRelationResponsePojo;
import com.vaidg.utilities.AppTypeface;
import java.util.List;

import static com.vaidg.utilities.Constants.RELATION_TYPE;

/**
 * <h2>RelationshipAdapter</h2>
 *
 * <p>This is an inner class of adapter type, to populate the relationship types on screen.</p>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */

public class RelationshipAdapter extends BaseAdapter {
  private Context mContext;
 private List<EditRelationResponsePojo.EditRelation> items;

  public RelationshipAdapter(List<EditRelationResponsePojo.EditRelation> items) {
/*
    super( 0, items);
*/
    this.items = items;
  }

    @Override
    public int getCount() {
        return items == null ? 0 : items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
  @NonNull
  @Override
  public View getView(int position, View convertView, @NonNull ViewGroup parent) {
    ViewHolder holder = null;
    mContext = parent.getContext();
    //   final String rowItem = getItem(position);
    LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(
        Activity.LAYOUT_INFLATER_SERVICE);
    if (convertView == null) {
      assert mInflater != null;
      convertView = mInflater.inflate(R.layout.list_row, null);
      holder = new ViewHolder();
      holder.tvRelationName = convertView.findViewById(R.id.row_title);
      convertView.setTag(holder);
    } else {
      holder = (ViewHolder) convertView.getTag();
    }
    if(RELATION_TYPE.equals(items.get(position).getName()))
        holder.tvRelationName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check,0,0,0);
    else
        holder.tvRelationName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_unselected,0,0,0);
    //  assert rowItem!=null;
    holder.tvRelationName.setTypeface(holder.appTypeface.getHind_regular());
    holder.tvRelationName.setText(items.get(position).getName());

    return convertView;
  }

  private class ViewHolder {
    TextView tvRelationName;
    AppTypeface appTypeface = AppTypeface.getInstance(mContext);
  }
}

