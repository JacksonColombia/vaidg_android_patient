package com.vaidg.incalloutcalltelecall.dependent.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

/**
 * <h2>SpecialRequirementResponse</h2>
 *
 * <p>Special Requirements Response POJO</p>
 *
 * @author 3Embed
 * @since 12-07-2019
 */
public class SpecialRequirementResponse implements Serializable {

  @SerializedName("message")
  @Expose
  private String message;
  @SerializedName("data")
  @Expose
  private List<SpecialRequirementsData> data = null;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public List<SpecialRequirementsData> getData() {
    return data;
  }

  public void setData(List<SpecialRequirementsData> data) {
    this.data = data;
  }

}
