package com.vaidg.incalloutcalltelecall.dependent.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * <h2>SpecialRequirementsData</h2>
 *
 * <p>Special Requirements DATA inside Response POJO</p>
 *
 * @author 3Embed
 * @since 12-07-2019
 */
public class SpecialRequirementsData implements Serializable {

  @SerializedName("_id")
  @Expose
  private String id;
  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("selectedIcon")
  @Expose
  private String selectedIcon;
  @SerializedName("unSelectedIcon")
  @Expose
  private String unSelectedIcon;
  @SerializedName("num")
  @Expose
  private Integer num;

  private boolean isSelected;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSelectedIcon() {
    return selectedIcon;
  }

  public void setSelectedIcon(String selectedIcon) {
    this.selectedIcon = selectedIcon;
  }

  public String getUnSelectedIcon() {
    return unSelectedIcon;
  }

  public void setUnSelectedIcon(String unSelectedIcon) {
    this.unSelectedIcon = unSelectedIcon;
  }

  public Integer getNum() {
    return num;
  }

  public void setNum(Integer num) {
    this.num = num;
  }

  public boolean isSelected() {
    return isSelected;
  }

  public void setSelected(boolean selected) {
    isSelected = selected;
  }
}
