package com.vaidg.incalloutcalltelecall.dependent.adapter;


import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.dependent.DependentContract;
import com.vaidg.incalloutcalltelecall.dependent.model.DependentsData;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.SessionManager;
import com.vaidg.utilities.Utility;
import com.utility.AlertProgress;
import com.utility.PicassoTrustAll;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.vaidg.utilities.Constants.W_30;
import static com.vaidg.utilities.Constants.W_320;
import static com.vaidg.utilities.Constants.W_50;
import static com.vaidg.utilities.Constants.W_54;
import static com.vaidg.utilities.Constants.W_59;

/**
 * <h2>DependentAdapter</h2>
 *
 * <p>This is an inner class of adapter type, to integrate the dependents list on screen.</p>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */

public class DependentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity mActivity;
    private Context mContext;
    private ArrayList<DependentsData> mDependentsDataArrayList = new ArrayList<>();
    private DependentContract.DependentPresenter mDependentPresenter;
    private OnItemClickListener mOnItemClickListener;
    private boolean isSelect = false;
    private AppTypeface mAppTypeface;
    private boolean isNotFromDependent = false;
    private int w50;

    public DependentAdapter(ArrayList<DependentsData> dependentsDataList,
                            DependentContract.DependentPresenter presenter, AppTypeface appTypeface,
                             boolean isNotFromDependent) {
        this.mDependentsDataArrayList = dependentsDataList;
        this.mDependentPresenter = presenter;
        this.mAppTypeface = appTypeface;
        this.isNotFromDependent = isNotFromDependent;
        calculate();
    }

    private void calculate() {
        w50 = Utility.getScreenWidth() * W_50 / W_320;
    }


    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_dependent,
                viewGroup, false);
        mContext = viewGroup.getContext();
        mActivity = (Activity)  viewGroup.getContext();
        return new SingleViewHolder(view);
    }

    public void setItemSelected(int position) {
        if (position != -1) {
            mDependentsDataArrayList.get(position).setChecked(!mDependentsDataArrayList.get(position).isChecked());
            Constants.dependentId = mDependentsDataArrayList.get(position).getDependentId();
            Constants.dependentName = mDependentsDataArrayList.get(position).getFirstName() + " "+mDependentsDataArrayList.get(position).getLastName();
            Constants.dependentRelation = mDependentsDataArrayList.get(position).getRelationship();
            Constants.dependentImageUrl= mDependentsDataArrayList.get(position).getProfilePic();

            notifyDataSetChanged();
        }
    }

    public void setOnItemClickListener(final OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        SingleViewHolder singleViewHolder = (SingleViewHolder) viewHolder;
        singleViewHolder.tvDependentName.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
        singleViewHolder.tvDependentRelation.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));

        if (!isNotFromDependent) {
            final int sdk = android.os.Build.VERSION.SDK_INT;
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                singleViewHolder.lvLinear.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.dependents_unselection) );
            } else {
                singleViewHolder.lvLinear.setBackground(ContextCompat.getDrawable(mContext, R.drawable.dependents_unselection));
            }
        }

        if (TextUtils.isEmpty(mDependentsDataArrayList.get(position).getProfilePic())) {
            singleViewHolder.ivDependentProfilePic.setImageDrawable(
                    mContext.getResources().getDrawable(R.drawable.default_photo));
        } else {
            if(mDependentsDataArrayList.get(position).getProfilePic() != null && !mDependentsDataArrayList.get(position).getProfilePic().isEmpty()) {
                PicassoTrustAll.getInstance(mContext)
                    .load(mDependentsDataArrayList.get(position).getProfilePic())
                    .placeholder(R.drawable.default_photo)   // optional
                    .error(R.drawable.default_photo)      // optional
                    .transform(new PicassoCircleTransform())
                    .into(singleViewHolder.ivDependentProfilePic);
            }

        }
        String depentName = mDependentsDataArrayList.get(position).getFirstName() + " "
                + mDependentsDataArrayList.get(
                position).getLastName();
        singleViewHolder.tvDependentName.setText(depentName);
        singleViewHolder.tvDependentRelation.setText(mDependentsDataArrayList.get(position).getRelationship());
        mDependentsDataArrayList.get(position).setChecked(mDependentsDataArrayList.get(position).isChecked());
        if (isNotFromDependent) {
            if (mDependentsDataArrayList.get(position).getDependentId().equals(Constants.dependentId)) {
                isSelect = true;
                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    singleViewHolder.lvLinear.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.dependents_selection) );
                } else {
                    singleViewHolder.lvLinear.setBackground(ContextCompat.getDrawable(mContext, R.drawable.dependents_selection));
                }
            } else {
                    isSelect = false;
                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    singleViewHolder.lvLinear.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.dependents_unselection) );
                } else {
                    singleViewHolder.lvLinear.setBackground(ContextCompat.getDrawable(mContext, R.drawable.dependents_unselection));
                }
            }
        }

        singleViewHolder.tvEditDependent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utility.isNetworkAvailable(mContext)) {
                    mOnItemClickListener.onItemEditClick(view, position, mDependentsDataArrayList);
                } else {
                    singleViewHolder.alertProgress.tryAgain(mContext, mContext.getResources().getString(R.string.pleaseCheckInternet), mContext.getResources().getString(R.string.system_error), isClicked -> {
                    });
                }
            }
        });

        singleViewHolder.tvDeleteDependent.setOnClickListener(view -> {
            {
                if (Utility.isNetworkAvailable(mContext)) {
                    showCustomDialog(mContext, singleViewHolder.viewGroup, mDependentsDataArrayList, position);
                } else {
                    singleViewHolder.alertProgress.tryAgain(mContext, mContext.getResources().getString(R.string.pleaseCheckInternet), mContext.getResources().getString(R.string.system_error), isClicked -> {
                    });
                }
            }
        });
    }

    private void showCustomDialog(Context context, ViewGroup viewGroup, ArrayList<DependentsData> dependentsData, int position) {


        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(context).inflate(R.layout.layout_dialog, viewGroup, false);

        TextView tvTitle = dialogView.findViewById(R.id.tvTitle);
        TextView tvTitleDesc = dialogView.findViewById(R.id.tvTitleDesc);
        Button buttonOk = dialogView.findViewById(R.id.buttonOk);
        Button buttonCancel = dialogView.findViewById(R.id.buttonCancel);

        tvTitle.setTypeface(mAppTypeface.getHind_semiBold());
        tvTitleDesc.setTypeface(mAppTypeface.getHind_medium());
        buttonOk.setTypeface(mAppTypeface.getHind_medium());
        buttonCancel.setTypeface(mAppTypeface.getHind_medium());
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);


        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                alertDialog.cancel();
                if(Utility.isNetworkAvailable(mContext))
                mDependentPresenter.onDeleteDependent(dependentsData, position, mContext);
                notifyDataSetChanged();
            }
        });
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                alertDialog.cancel();
            }
        });

    }


    @Override
    public int getItemCount() {
        return mDependentsDataArrayList == null ? 0 : mDependentsDataArrayList.size();
    }

    public interface OnItemClickListener {

        void onItemClick(View view, int position,
                         ArrayList<DependentsData> mDependentsDataArrayList, boolean isSelect);

        void onItemEditClick(View view, int position,
                             ArrayList<DependentsData> mDependentsDataArrayList);
    }

    class SingleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.ivDependentProfilePic)
        ImageView ivDependentProfilePic;
        @BindView(R.id.lvLinear)
        LinearLayout lvLinear;
        @BindView(R.id.tvDependentName)
        TextView tvDependentName;
        @BindView(R.id.tvDependentRelation)
        TextView tvDependentRelation;
        @BindView(R.id.tvEditDependent)
        ImageView tvEditDependent;
        @BindView(R.id.tvDeleteDependent)
        ImageView tvDeleteDependent;
        ViewGroup viewGroup;
        AlertProgress alertProgress;

        SingleViewHolder(@NonNull View convertView) {
            super(convertView);
            ButterKnife.bind(this, convertView);
            itemView.setOnClickListener(this);
            alertProgress = new AlertProgress(mContext);
            tvDependentName.setTypeface(mAppTypeface.getHind_semiBold());
            tvDependentRelation.setTypeface(mAppTypeface.getHind_regular());
            //before inflating the custom alert dialog layout, we will get the current activity viewgroup
            viewGroup = convertView.findViewById(android.R.id.content);
            ivDependentProfilePic.getLayoutParams().width = w50;
            ivDependentProfilePic.getLayoutParams().height = w50;

        }

        @Override
        public void onClick(View view) {

            if (mOnItemClickListener != null) {
                for(int i = 0; i <mDependentsDataArrayList.size(); i++)
                {
                    mDependentsDataArrayList.get(i).setChecked(false);
                }
                isSelect = true;
                mOnItemClickListener.onItemClick(view, getAdapterPosition(), mDependentsDataArrayList,
                        isSelect);
            }
        }
    }

}
