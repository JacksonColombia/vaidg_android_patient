package com.vaidg.incalloutcalltelecall.dependent;

import android.content.Context;
import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;
import com.vaidg.incalloutcalltelecall.dependent.model.DependentsData;
import com.vaidg.utilities.SessionManager;
import com.utility.AlertProgress;
import java.util.ArrayList;

/**
 * <h2>DependentContract</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public interface DependentContract {

  interface DependentView extends BaseView {

    /**
     * <h2>refreshData</h2>
     * This method is used to refresh the data of the dependent
     *
     * @param dependentsData list od dependent
     * @param position       position of the dependent added in list
     */
    void refreshData(ArrayList<DependentsData> dependentsData, int position);


    /**
     * <h2>getDependentDataList</h2>
     * This method is used to get the list of the dependent
     *
     * @param dependentsData list od dependent
     */
    void getDependentDataList(ArrayList<DependentsData> dependentsData);

    /**
     * <h2>getDependentDataList</h2>
     * This method is used to show no list of the dependent
     *
     */
    void noDependentDataList();
  }

  interface DependentPresenter extends BasePresenter<DependentView> {


    /**
     * <h2>deleteDependent</h2>
     * This method is used to delete the selected position of the dependent
     * @param dependentsData list of dependent
     * @param position       position of the dependent added in list
     */
    void onDeleteDependent(ArrayList<DependentsData> dependentsData, int position,
        Context context);

    /**
     * <h2>getDependents</h2>
     * This method is used to get the data of the dependent
     * @param mcontext  instance of class
     */
    void getDependents(Context mcontext);


  }

}
