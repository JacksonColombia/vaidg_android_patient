package com.vaidg.incalloutcalltelecall.dependent.editrelationship;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vaidg.R;
import com.vaidg.countrypic.CountryPicker;
import com.vaidg.incalloutcalltelecall.dependent.adapter.RelationshipAdapter;
import com.vaidg.incalloutcalltelecall.dependent.model.EditRelationResponsePojo;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManager;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.utility.AlertProgress;
import dagger.android.support.DaggerAppCompatActivity;
import java.util.ArrayList;
import javax.inject.Inject;

/**
 * <h2>EditRelationshipActivity</h2>
 *
 * <p>
 * Edit the Relationship of the dependent
 * </p>
 *
 * @author Pramod
 * @since 04-03-2018
 */
public class EditRelationshipActivity extends DaggerAppCompatActivity implements EditRelationshipView {

  // private String mEditRelations;
  @Inject
  EditRelationshipPresenter mEditRelationshipPresenter;
  @Inject
  AppTypeface appTypeface;
  @Inject
  SessionManager mSessionManager;
  private boolean isRelation;
  private ArrayList<EditRelationResponsePojo.EditRelation> mEditRelations = new ArrayList<>();
  private LinearLayout llEditRelation;
  private RelativeLayout rlEditMob;
  private EditText etEidtMobileNumber;
  //private Spinner spRelation;
  private ListView lvRelations;
  @Inject
  AlertProgress mAlertProgress;
  private ProgressBar mProgressBar;
  private ImageView ivEditCountryFlg;
  private TextView tvEditCountryCode;
  private CountryPicker mCountryPicker;
  private String phoneEmail;
  private RelationshipAdapter mRelationshipAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_edit_relationship);
    initializeView();
    initializeToolBar();
    mEditRelationshipPresenter.openFetchRelation(this, mSessionManager, LSPApplication.getInstance().getAuthToken(mSessionManager.getSID()),mAlertProgress);
  }

  /**
   * <h2>initializeView</h2>
   *
   * <p>Intiliazing view elements</p>
   */
  private void initializeView() {
    etEidtMobileNumber = findViewById(R.id.etEidtMobileNumber);
    mCountryPicker = CountryPicker.newInstance(getResources().getString(R.string.selectcountry));
    mProgressBar = findViewById(R.id.progressDialog);
    if (getIntent().getExtras() != null) {
      isRelation = getIntent().getBooleanExtra("IS_RELATION", false);
    }
    lvRelations = findViewById(R.id.listRelations);
    tvEditCountryCode = findViewById(R.id.tvEditCountryCode);
    ivEditCountryFlg = findViewById(R.id.ivEditCountryFlg);
    llEditRelation = findViewById(R.id.llEditRelations);
    rlEditMob = findViewById(R.id.rlEditMob);
  }

  /*
  initializing tool bar
   */
  private void initializeToolBar() {

    Toolbar login_tool = findViewById(R.id.toolbar);
    setSupportActionBar(login_tool);
    TextView tv_center = findViewById(R.id.tv_center);
    if (getSupportActionBar() != null) {
      getSupportActionBar().setTitle("");
      getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
    tv_center.setTypeface(appTypeface.getHind_semiBold());
    TextView tveditMobInfo = findViewById(R.id.tveditMobInfo);
    if (isRelation) {
      TextView tvEditRelation = findViewById(R.id.tvEditRelation);
      tvEditRelation.setTypeface(appTypeface.getHind_regular());
      llEditRelation.setVisibility(View.VISIBLE);
      rlEditMob.setVisibility(View.GONE);
      tv_center.setText(R.string.add_relationship);
      tveditMobInfo.setVisibility(View.GONE);
    } else {
      tveditMobInfo.setVisibility(View.VISIBLE);
      tveditMobInfo.setTypeface(appTypeface.getHind_medium());
      etEidtMobileNumber.setTypeface(appTypeface.getHind_medium());
      etEidtMobileNumber.setBackgroundColor(Color.TRANSPARENT);
      llEditRelation.setVisibility(View.GONE);
      rlEditMob.setVisibility(View.VISIBLE);
      tv_center.setText(R.string.changePhone);
      etEidtMobileNumber.requestFocus();
    }

    login_tool.setNavigationIcon(R.drawable.ic_back);
    login_tool.setNavigationOnClickListener(v -> onBackPressed());
  }

  @Override
  public void showProgress() {

  }

  @Override
  public void hideProgress() {

  }

  @Override
  public void onResponseSuccess(ArrayList<EditRelationResponsePojo.EditRelation> data) {
    mEditRelations.addAll(data);
    mRelationshipAdapter = new RelationshipAdapter( data);
    lvRelations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        if (mEditRelations.size() > 0) {
          String relationship = mEditRelations.get(position).getName();
          Constants.isRelationCalled = true;
          Intent intent = new Intent();
          intent.putExtra("relationship", relationship);
          setResult(RESULT_OK, intent);
          finish();
        }
      }
    });
    lvRelations.setAdapter(mRelationshipAdapter);
  }

  @Override
  public void onSessionExpired() {

  }

  @Override
  public void onLogout(String message, SessionManagerImpl sessionManager) {
    mAlertProgress.alertPositiveOnclick(this, message, getString(R.string.logout),
        getString(R.string.ok), isClicked -> Utility.setMAnagerWithBID(this, mSessionManager));

  }

  @Override
  public void onError(String error) {

  }

  @Override
  public void onConnectionError(String connectionError) {

  }

  @Override
  public void onShowProgress() {

  }

  @Override
  public void onHideProgress() {

  }
}
