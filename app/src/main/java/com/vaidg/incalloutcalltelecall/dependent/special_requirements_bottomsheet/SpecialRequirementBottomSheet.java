package com.vaidg.incalloutcalltelecall.dependent.special_requirements_bottomsheet;

import android.annotation.SuppressLint;
import android.app.Dialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.dependent.adapter.SpecialRequirementAdapter;
import com.vaidg.incalloutcalltelecall.dependent.add_dependent.SpecialRequirementCallback;
import com.vaidg.incalloutcalltelecall.dependent.model.SpecialRequirementsData;
import com.vaidg.utilities.AppTypeface;
import java.util.ArrayList;

/**
 * <h2>SpecialRequirementBottomSheet</h2>
 *
 * <p>used to show the Special Requirements bottom sheet dialog</p>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */

@SuppressLint("ValidFragment")
public class SpecialRequirementBottomSheet extends BottomSheetDialogFragment {
  private static final String TAG = "SpecialReqBS";

  @BindView(R.id.tv_special_req_cancel)
  TextView tv_special_req_cancel;

  @BindView(R.id.tv_special_req_title)
  TextView tv_special_req_title;

  @BindView(R.id.tv_special_req_done)
  TextView tv_special_req_done;

  @BindView(R.id.rv_special_req_list)
  RecyclerView rv_special_req_list;

  private SpecialRequirementAdapter specialRequirementAdapter;
  private AppTypeface appTypeface;
  private SpecialRequirementCallback callback;

  public SpecialRequirementBottomSheet(SpecialRequirementCallback callback, AppTypeface appTypeface,
      SpecialRequirementAdapter specialRequirementAdapter) {
    this.appTypeface = appTypeface;
    this.callback = callback;
    this.specialRequirementAdapter = specialRequirementAdapter;
  }

  @SuppressLint("RestrictedApi")
  @Override
  public void setupDialog(Dialog dialog, int style) {
    super.setupDialog(dialog, style);
    Log.e(TAG, "setupDialog ");
    View contentView = View.inflate(getContext(), R.layout.dialog_special_requirements, null);
    dialog.setContentView(contentView);
    initialize(contentView);
    setTypeface();
  }

  @Override
  public void show(FragmentManager manager, String tag) {
    super.show(manager, tag);
    specialRequirementAdapter.notifyDataSetChanged();
  }

  /**
   * <h2>setTypeface</h2>
   * Used to set the typeface
   */
  private void setTypeface() {
    tv_special_req_cancel.setTypeface(appTypeface.getHind_semiBold());
    tv_special_req_title.setTypeface(appTypeface.getHind_semiBold());
    tv_special_req_done.setTypeface(appTypeface.getHind_semiBold());
  }

  /**
   * <h2>clearList</h2>
   * This method is used to initialize the variables
   */
  public void clearList() {

  }

  /**
   * <h2>initialize</h2>
   * This method is used to initialize the variables
   */
  @SuppressLint("SetTextI18n")
  private void initialize(View contentView) {
    Log.e(TAG, " initialize bottomsheet ");
    ButterKnife.bind(this, contentView);
    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
    rv_special_req_list.setLayoutManager(layoutManager);
    rv_special_req_list.setAdapter(specialRequirementAdapter);
  }

  @OnClick({R.id.tv_special_req_cancel, R.id.tv_special_req_done})
  public void clickEvent(View view) {
    this.getDialog().dismiss();
    switch (view.getId()) {
      case R.id.tv_special_req_done:
        callback.onSpecialRequirementsSelected(specialRequirementAdapter.getSelectedRequirements());
        break;
      case R.id.tv_special_req_cancel:
        specialRequirementAdapter.clearSelections();
        callback.onSpecialRequirementsSelected(new ArrayList<SpecialRequirementsData>());
        break;

    }
  }
}

