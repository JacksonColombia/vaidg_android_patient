package com.vaidg.incalloutcalltelecall.dependent.add_dependent;


import android.app.DatePickerDialog;
import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.widget.DatePicker;

import com.vaidg.incalloutcalltelecall.dependent.model.AddDependentModel;
import com.vaidg.incalloutcalltelecall.dependent.model.DependentsAddPojo;
import com.vaidg.incalloutcalltelecall.dependent.model.DependentsPost;
import com.vaidg.incalloutcalltelecall.dependent.model.SpecialRequirementResponse;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManager;
import com.google.gson.Gson;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.pojo.CheckFlagForValidity;
import com.pojo.ErrorHandel;
import com.utility.AlertProgress;
import com.utility.RefreshToken;
import com.vaidg.utilities.Utility;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.SESSION_EXPIRED;
import static com.vaidg.utilities.Constants.SESSION_LOGOUT;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;
import static com.vaidg.utilities.Constants.isDependentCalled;

/**
 * <h2>AddDependentPresenterImpl</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class AddDependentPresenterImpl implements AddDependentPresenter {

    @Inject
    LSPServices mLSPServices;
    @Inject
    AddDependentView view;
    @Inject
    Gson mGson;
    @Inject
    SessionManager sessionManager;
    @Inject
    AddDependentModel addDependentModel;
    private CheckFlagForValidity checkFlag;

    @Inject
    AddDependentPresenterImpl() {
        this.checkFlag = new CheckFlagForValidity();
    }

    @Override
    public void addDependent(Context context, DependentsAddPojo dependentsAddPojo,
                             SessionManager sessionManager, AlertProgress alertProgress) {

        if (view != null) {
            view.showProgress("ADD_DEPENDENT");
        }
        Map<String, Object> jsonParams = new HashMap<>();
        jsonParams.put("firstName", dependentsAddPojo.getFirstName());
        jsonParams.put("lastName", dependentsAddPojo.getLastName());
        jsonParams.put("profilePic", dependentsAddPojo.getProfilePic());
        jsonParams.put("gender", dependentsAddPojo.getGender());
        jsonParams.put("relationship", dependentsAddPojo.getRelationship());
        jsonParams.put("countryCode", dependentsAddPojo.getCountryCode());
        jsonParams.put("countryCodeSymbol",dependentsAddPojo.getCountryCodeSymbol());
        jsonParams.put("phone", dependentsAddPojo.getPhone());
        jsonParams.put("dateOfBirth", dependentsAddPojo.getDateOfBirth());
        jsonParams.put("specialRequirement", dependentsAddPojo.getSpecialRequirement());
        jsonParams.put("specialInstruction", dependentsAddPojo.getSpecialInstruction());

        Observable<Response<ResponseBody>> services = mLSPServices.addDependent(
                LSPApplication.getInstance().getAuthToken(sessionManager.getSID()), Constants.selLang, Constants.PLATFORM_ANDROID, jsonParams);
        services.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int code = responseBodyResponse.code();
                        try {
                            String response =
                                    responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
                            String errorBody =
                                    responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null;
                            JSONObject errJsonD;
                            switch (code) {
                                case SUCCESS_RESPONSE:
                                    if (response != null && !response.isEmpty()) {
                                        DependentsPost responsePojo = mGson.fromJson(response,
                                                DependentsPost.class);
                                      if(view != null)
                                        view.navToDependentsList(false);
                                    }
                                  if(view != null)
                                    view.hideProgress();
                                    break;
                                case SESSION_LOGOUT:
                                    if (errorBody != null && !errorBody.isEmpty()) {
                                        errJsonD = new JSONObject(errorBody);
                                      errJsonD.getString(MESSAGE);
                                      if (!errJsonD.getString(MESSAGE).isEmpty()) {
                                          if(view != null)
                                            view.onLogout(errJsonD.getString(MESSAGE),sessionManager);
                                        }
                                    }
                                  if(view != null)
                                    view.onHideProgress();
                                    break;
                                case SESSION_EXPIRED:
                                    if (errorBody != null && !errorBody.isEmpty()) {
                                        ErrorHandel errorHandel = mGson.fromJson(errorBody, ErrorHandel.class);
                                        RefreshToken.onRefreshToken(
                                                LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),
                                                sessionManager.getREFRESHAUTH(), mLSPServices,
                                                new RefreshToken.RefreshTokenImple() {
                                                    @Override
                                                    public void onSuccessRefreshToken(String newToken) {
                                                      if(view != null)
                                                        view.onHideProgress();
                                                        LSPApplication.getInstance().setAuthToken(sessionManager.getSID(),sessionManager.getSID(),newToken);
                                                        addDependent(context, dependentsAddPojo,
                                                                sessionManager, alertProgress);
                                                    }

                                                    @Override
                                                    public void onFailureRefreshToken() {

                                                    }

                                                    @Override
                                                    public void sessionExpired(String msg) {
                                                      if(view != null) {
                                                        view.onLogout(msg, sessionManager);
                                                        view.onHideProgress();
                                                      }
                                                    }
                                                });
                                    }
                                    break;
                                default:
                                    if (errorBody != null && !errorBody.isEmpty()) {
                                        errJsonD = new JSONObject(errorBody);
                                      errJsonD.getString(MESSAGE);
                                      if (!errJsonD.getString(MESSAGE).isEmpty()) {
                                        if(view != null)
                                            view.onError(errJsonD.getString(MESSAGE));
                                        }
                                    }
                                  if(view != null)
                                    view.onHideProgress();
                                    break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                          if(view != null)
                            view.onHideProgress();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                      if(view != null)
                        view.onHideProgress();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


    @Override
    public void editDependent(String dependentId,
                              String firstName,
                              String lastName,
                              String profilePic,
                              String gender,
                              String relationship,
                              String countryCodeSymbol,
                              String countryCode,
                              String phone,
                              String dateOfBirth,
                              String specialRequirement,
                              String specialInstruction, SessionManager sessionManager, Context context,
                              AlertProgress alertDialog) {
        if (view != null) {
            view.showProgress("EDIT_DEPENDENT");
        }

        Map<String, Object> jsonParams = new HashMap<>();
        jsonParams.put("dependentId", dependentId);
        jsonParams.put("firstName", firstName);
        jsonParams.put("lastName", lastName);
        jsonParams.put("profilePic", profilePic);
        jsonParams.put("gender", gender);
        jsonParams.put("relationship", relationship);
        jsonParams.put("countryCodeSymbol", countryCodeSymbol);
        jsonParams.put("countryCode", countryCode);
        jsonParams.put("phone", phone);
        jsonParams.put("dateOfBirth", dateOfBirth);
        jsonParams.put("specialRequirement", specialRequirement);
        jsonParams.put("specialInstruction", specialInstruction);


        Observable<Response<ResponseBody>> services = mLSPServices.editDependent(
                LSPApplication.getInstance().getAuthToken(sessionManager.getSID()), Constants.PLATFORM_ANDROID, jsonParams);

        services.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> value) {

                        int code = value.code();
                        try {
                            String response = value.body() != null ? value.body().string() : null;
                            String errorBody = value.errorBody() != null ? value.errorBody().string() : null;
                            JSONObject errJsonD;
                            switch (code) {
                                case SUCCESS_RESPONSE:
                                    if (response != null && !response.isEmpty()) {
                                        isDependentCalled = true;
                                        DependentsPost responsePojo = mGson.fromJson(response,
                                                DependentsPost.class);
                                      if(view != null)
                                        view.navToDependentsList(isDependentCalled);
                                    }
                                  if(view != null)
                                    view.hideProgress();
                                    break;
                                case SESSION_LOGOUT:
                                    if (errorBody != null && !errorBody.isEmpty()) {
                                        errJsonD = new JSONObject(errorBody);
                                      errJsonD.getString(MESSAGE);
                                      if (!errJsonD.getString(MESSAGE).isEmpty()) {
                                          if(view != null)
                                            view.onLogout(errJsonD.getString(MESSAGE),sessionManager);
                                        }
                                    }
                                  if(view != null)
                                    view.onHideProgress();
                                    break;
                                case SESSION_EXPIRED:
                                    if (errorBody != null && !errorBody.isEmpty()) {
                                        ErrorHandel errorHandel = mGson.fromJson(errorBody, ErrorHandel.class);
                                        RefreshToken.onRefreshToken(
                                                LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),
                                                sessionManager.getREFRESHAUTH(), mLSPServices,
                                                new RefreshToken.RefreshTokenImple() {
                                                    @Override
                                                    public void onSuccessRefreshToken(String newToken) {
                                                      if(view != null)
                                                      view.onHideProgress();
                                                        LSPApplication.getInstance().setAuthToken(sessionManager.getSID(),sessionManager.getSID(),newToken);
                                                        editDependent(dependentId,
                                                                firstName,
                                                                lastName,
                                                                profilePic,
                                                                gender,
                                                                relationship,
                                                                countryCodeSymbol,
                                                                countryCode,
                                                                phone,
                                                                dateOfBirth,
                                                                specialRequirement,
                                                                specialInstruction, sessionManager, context,
                                                                alertDialog);
                                                    }

                                                    @Override
                                                    public void onFailureRefreshToken() {

                                                    }

                                                    @Override
                                                    public void sessionExpired(String msg) {
                                                      view.onLogout(msg,sessionManager);
                                                      view.onHideProgress();
                                                    }
                                                });
                                    }
                                    break;
                                default:
                                    if (errorBody != null && !errorBody.isEmpty()) {
                                        errJsonD = new JSONObject(errorBody);
                                      errJsonD.getString(MESSAGE);
                                      if (!errJsonD.getString(MESSAGE).isEmpty()) {
                                        if(view != null)
                                            view.onError(errJsonD.getString(MESSAGE));
                                        }
                                    }
                                  if(view != null)
                                  view.onHideProgress();

                                  break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                          if(view != null)
                            view.onHideProgress();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                      if(view != null)
                        view.onHideProgress();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


    @Override
    public void getSpecialRequirements() {
        if (view != null) {
            view.showProgress("GET_SPECIAL_REQ");
        }

        Observable<Response<SpecialRequirementResponse>> services =
                mLSPServices.getSpecialRequirements(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()), Constants.selLang,
                        Constants.PLATFORM_ANDROID);
        services.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<SpecialRequirementResponse>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<SpecialRequirementResponse> value) {
                        int code = value.code();
                        JSONObject errJsonD;
                        try {
                            String response = value.body() != null ? value.body().toString() : null;
                            switch (code) {
                                case SUCCESS_RESPONSE:
                                    SpecialRequirementResponse specialRequirementResponse = mGson.fromJson(response, SpecialRequirementResponse.class);
                                    if (specialRequirementResponse.getData().size() > 0) {
                                      if(view != null)
                                        view.setSpecialRequirements(specialRequirementResponse.getData());
                                    }
                                  if(view != null)
                                    view.hideProgress();
                                    break;
                                case SESSION_LOGOUT:
                                    errJsonD = new JSONObject(response);
                                  if(view != null) {
                                    view.onHideProgress();
                                    view.onLogout(errJsonD.getString(MESSAGE), sessionManager);
                                  }
                                    break;
                                default:
                                    errJsonD = new JSONObject(response);
                                  if(view != null) {
                                    view.onHideProgress();
                                    view.onError(errJsonD.getString(MESSAGE));
                                  }
                                    break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                          if(view != null)
                            view.onHideProgress();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                      if(view != null)
                        view.onHideProgress();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public boolean validateFirstName(String firstName) {
        return addDependentModel.validateFirstName(firstName);
    }

    @Override
    public boolean validateFields(String firstName,
                                  String lastName,
                                  String phone, int max,
                                  String dateOfBirth, String gender,
                                  String relationShip) {
        return false;
    }

    @Override
    public void validatePhoneNumber(AddDependent addDependent, String countryCode,
                                    String mobileNumber) {
        String phoneNumberE164Format = countryCode.concat(mobileNumber);
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber phoneNumberProto = null;
        boolean isValid = false;
        try {
            phoneNumberProto = phoneUtil.parse(phoneNumberE164Format, null);
            isValid = phoneUtil.isValidNumber(phoneNumberProto); // returns true if valid
        } catch (NumberParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (isValid && (phoneUtil.getNumberType(phoneNumberProto)
                == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType(phoneNumberProto)
                == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE)) {
            view.setMobileErrorMsg("");
        } else {
            // Do api call for the existence of mobile number in the system already.
            view.setMobileError();
        }
    }

    @Override
    public void openCalendar(
            Context mContext) {
        final Calendar newCalendar = Calendar.getInstance();
        newCalendar.setTimeZone(Utility.getTimeZone());
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(mContext,
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                        newCalendar.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                        dateFormatter.setTimeZone(Utility.getTimeZone());
                        Constants.dateOB = dateFormatter.format(newCalendar.getTime());
                        String title = DateUtils.formatDateTime(mContext,
                                newCalendar.getTimeInMillis(),
                                DateUtils.FORMAT_SHOW_DATE
                                        | DateUtils.FORMAT_SHOW_YEAR
                                        | DateUtils.FORMAT_ABBREV_MONTH);

                        view.setDateOfBirth(dateFormatter.format(newCalendar.getTime()));
                        checkDobValidity();
                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));

        newCalendar.add(Calendar.YEAR, -18);
        fromDatePickerDialog.getDatePicker().setMaxDate(newCalendar.getTimeInMillis());
        fromDatePickerDialog.show();
    }


    @Override
    public boolean checkDobValidity() {
        return true;
    }


    /**
     * <h2>checkDob</h2>
     * checking the required mininum age and return according
     *
     * @param userDob    user's input Date of birth
     * @param minimumAge required minimum Age
     * @return true if the DoB is more than minimum Age
     */
    private boolean checkDob(String userDob, int minimumAge) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        sdf.setTimeZone(Utility.getTimeZone());
        try {
            Date parseddate = sdf.parse(userDob);
            Calendar c2 = Calendar.getInstance();
            c2.setTimeZone(Utility.getTimeZone());
            c2.add(Calendar.DAY_OF_YEAR, -minimumAge);
            return parseddate.before(c2.getTime());

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
    }


    @Override
    public void clearDependentValues() {
        sessionManager.setDependentFname("");
        sessionManager.setDependentLname("");
        sessionManager.setDateOBirth("");
        sessionManager.setDependentMobileNo("");
        sessionManager.setDependentCountryCodeSymbol("");
        sessionManager.setGender("");
        sessionManager.setDependentImage("");
    }

    @Override
    public void setValuesToSessionMgr(String dependent_fname, String dependent_lname,
                                      String date_of_Birth, String mobile_no, String gender_str) {
        sessionManager.setDependentFname(dependent_fname);
        sessionManager.setDependentLname(dependent_lname);
        sessionManager.setDateOBirth(date_of_Birth);
        sessionManager.setDependentMobileNo(mobile_no);
        sessionManager.setGender(gender_str);
    }

    @Override
    public void setValuesToViews() {
        view.setValuesToFields(sessionManager.getDependentFname(),
                sessionManager.getDependentLname(), sessionManager.getDateOBirth(),
                sessionManager.getDependentMobileNo(), sessionManager.getGender());
    }

    @Override
    public String getDependentImageUrl() {
        String dep_image = sessionManager.getDependentImage();
        if (!TextUtils.isEmpty(dep_image)) {
            return dep_image;
        } else {
            return "";
        }
    }

    @Override
    public void setDependentImageUrl(String url) {
        sessionManager.setDependentImage(url);
    }

    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }
}
