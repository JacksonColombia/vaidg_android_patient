package com.vaidg.incalloutcalltelecall.dependent;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.dependent.adapter.DependentAdapter;
import com.vaidg.incalloutcalltelecall.dependent.add_dependent.AddDependent;
import com.vaidg.incalloutcalltelecall.dependent.model.DependentsData;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManager;
import com.vaidg.utilities.SessionManagerImpl;
import com.utility.AlertProgress;
import com.vaidg.utilities.Utility;
import dagger.android.support.DaggerAppCompatActivity;
import java.util.ArrayList;
import javax.inject.Inject;

public class DependentActivity extends DaggerAppCompatActivity implements View.OnClickListener,
    DependentContract.DependentView {

  @Inject
  AppTypeface appTypeface;
  @Inject
  SessionManagerImpl sessionManager;
  @Inject
  DependentContract.DependentPresenter dependentPresenter;
  @Inject
  AlertProgress alertProgress;
  @BindView(R.id.iv_add_dependent)
  ImageView iv_add_dependent;
  Resources resources;
  @BindView(R.id.rvDependents)
  RecyclerView rvDependents;
  @BindView(R.id.no_dependent_added)
  TextView tvNoDependentsAdded;
  @BindView(R.id.tvConsultationLabel)
  TextView tvConsultationLabel;
  @BindView(R.id.tvDependents)
  TextView tvDependents;
  TextView tv_tab_title;
  @BindView(R.id.radioButtonBidding)
  TextView radioButtonBidding;
  @BindView(R.id.txt_add_dependent)
  TextView txt_add_dependent;
  @BindView(R.id.vDivider)
  View vDivider;
  @BindView(R.id.progressBarDep)
  ProgressBar mProgressBar;
  @BindView(R.id.rlListDependentEmpty)
  RelativeLayout rlListDependentEmpty;
  @BindView(R.id.lvmainDependent)
  NestedScrollView lvmainDependent;
  @BindView(R.id.tvDependentNew)
  TextView tvDependentNew;
  @BindView(R.id.btn_add_dependent)
  TextView btn_add_dependent;
  private Context mContext;
  private AlertDialog mAlertProgress;
  private DependentAdapter mDependentAdapter;
  private ArrayList<DependentsData> mDependentsDataArrayList = new ArrayList<>();
  private boolean isNotFromDependent = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_dependent_list);

    ButterKnife.bind(this);
    dependentPresenter.attachView(this);
    mContext = this;
    onShowProgress();
    tvConsultationLabel.setClickable(false);
    if(Utility.isNetworkAvailable(mContext))
    dependentPresenter.getDependents(mContext);
    if (getIntent().getExtras() != null)
      isNotFromDependent = getIntent().getBooleanExtra("isNotFromDependent", false);


    initializeView();
  }

  /**
   * <h2>initializeView</h2>
   *
   * <p>Intiliazing view elements</p>
   */
  private void initializeView() {
    resources = mContext.getResources();

    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayShowHomeEnabled(true);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
    TextView textViewCenter = findViewById(R.id.tv_center);
    if(isNotFromDependent)
    {
      textViewCenter.setText(R.string.select_dependents);
    }else{
      textViewCenter.setText(R.string.add_dependents);
    }
    textViewCenter.setTypeface(appTypeface.getHind_semiBold());
    toolbar.setNavigationIcon(R.drawable.ic_back);
    toolbar.setNavigationOnClickListener(view -> onBackPressed());
    btn_add_dependent.setTypeface(appTypeface.getHind_semiBold());

    iv_add_dependent.setOnClickListener(this);
    tvConsultationLabel.setTypeface(appTypeface.getHind_medium());
    tvDependents.setTypeface(appTypeface.getHind_regular());
    radioButtonBidding.setTypeface(appTypeface.getHind_medium());
    txt_add_dependent.setTypeface(appTypeface.getHind_medium());
    radioButtonBidding.setOnClickListener(this);
    txt_add_dependent.setOnClickListener(this);
    rvDependents.setHasFixedSize(true);
    if (isNotFromDependent) {
      tvConsultationLabel.setVisibility(View.VISIBLE);
      radioButtonBidding.setVisibility(View.VISIBLE);
      vDivider.setVisibility(View.VISIBLE);
      tvDependents.setVisibility(View.VISIBLE);


      if (Constants.dependentId.equals("1")) {
        radioButtonBidding.setSelected(true);
        radioButtonBidding.setCompoundDrawablesWithIntrinsicBounds(
            0, 0, R.drawable.ic_check, 0);
      }

      radioButtonBidding.setOnClickListener(view -> {
        selectedDependentMyself();
      });

    } else {
      radioButtonBidding.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
      tvConsultationLabel.setVisibility(View.GONE);
      radioButtonBidding.setVisibility(View.GONE);
      vDivider.setVisibility(View.GONE);
      tvDependents.setVisibility(View.GONE);

    }


    rvDependents.setLayoutManager(new LinearLayoutManager(mContext));
    mDependentAdapter = new DependentAdapter(
        mDependentsDataArrayList, dependentPresenter, appTypeface, isNotFromDependent);
    rvDependents.setAdapter(mDependentAdapter);

    mDependentAdapter.setOnItemClickListener(new DependentAdapter.OnItemClickListener() {
      @Override
      public void onItemClick(View view, int position, ArrayList<DependentsData> dependentsData,
                              boolean isSelect) {
        dependentitemListClicked(position, dependentsData, isSelect);
      }

      @Override
      public void onItemEditClick(View view, int position,
                                  ArrayList<DependentsData> dependentsData) {

        Constants.isRelationCalled = false;
        DependentsData row_details = dependentsData.get(position);
        Intent intent = new Intent(mContext, AddDependent.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("row_details", row_details);
        intent.putExtras(bundle);
        //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 2);
        overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
      }
    });

    mDependentAdapter.notifyDataSetChanged();
  }

  private void selectedDependentMyself() {
    Constants.dependentId = "1";
    Constants.dependentName = sessionManager.getFirstName() + " " + sessionManager.getLastName();
    Constants.dependentImageUrl = sessionManager.getProfilePicUrl();
    Constants.dependentRelation = "Myself";
    returnSelectedDependent();
  }

  private void dependentitemListClicked(int position,
                                        ArrayList<DependentsData> dependentsData, boolean isSelect) {
    if (isNotFromDependent) {
      Constants.dependentId = dependentsData.get(position).getDependentId();
      Constants.dependentName = dependentsData.get(position).getFirstName() + " " + dependentsData.get(position).getLastName();
      Constants.dependentRelation = dependentsData.get(position).getRelationship();
      Constants.dependentImageUrl = dependentsData.get(position).getProfilePic();

      returnSelectedDependent();
    }


  }

  private void returnSelectedDependent() {
    Intent intent = new Intent();
    setResult(RESULT_OK, intent);
    finish();
    overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
  }

  /**
   * This is onResume() method, which is getting call each time.
   */
  @Override
  public void onResume() {
    super.onResume();
  }

  @Override
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.txt_add_dependent:
      case R.id.iv_add_dependent:
        Intent intent = new Intent(this, AddDependent.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 220);
        overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
        break;
      case R.id.radioButtonBidding:

        break;
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == 220) {
      if (resultCode == RESULT_OK) {
        if(Utility.isNetworkAvailable(mContext))
        dependentPresenter.getDependents(mContext);

      }
    }

    if (requestCode == 2) {
      if (resultCode == RESULT_OK) {
        if(Utility.isNetworkAvailable(mContext))
        dependentPresenter.getDependents(mContext);

      }
    }

  }

  @Override
  public void refreshData(ArrayList<DependentsData> dependentsData, int position) {
    if (mDependentAdapter != null) {
      if (dependentsData.size() > 0) {
        mDependentsDataArrayList.remove(position);
            /*    mDependentAdapter.notifyItemRemoved(position);
                mDependentAdapter.notifyItemRangeRemoved(position, dependentsData.size());*/
        mDependentAdapter.notifyDataSetChanged();
        if (mDependentAdapter.getItemCount() == 0) {
          noDependentDataList();
        }
      } else {
        selectedDependentMyself();
        noDependentDataList();
      }
    }
    // dependentPresenter.getDependents(mContext, sessionManager.getAUTH(), mAlertProgress, sessionManager);
  }

  @Override
  public void getDependentDataList(ArrayList<DependentsData> dependentsData) {
    if (isNotFromDependent) {
      tvNoDependentsAdded.setVisibility(View.GONE);
      lvmainDependent.setVisibility(View.VISIBLE);
      rvDependents.setVisibility(View.VISIBLE);
    } else {
      rlListDependentEmpty.setVisibility(View.GONE);
      lvmainDependent.setVisibility(View.VISIBLE);
      rvDependents.setVisibility(View.VISIBLE);
    }
    mDependentsDataArrayList.clear();
    mDependentsDataArrayList.addAll(dependentsData);
    mDependentAdapter.notifyDataSetChanged();
  }

  @Override
  public void noDependentDataList() {
    if (isNotFromDependent) {
      tvNoDependentsAdded.setVisibility(View.GONE);
      rvDependents.setVisibility(View.GONE);
    } else {
      rlListDependentEmpty.setVisibility(View.VISIBLE);
      lvmainDependent.setVisibility(View.GONE);
    }
    mDependentsDataArrayList.clear();
    mDependentAdapter.notifyDataSetChanged();
  }

  @OnClick(R.id.btn_add_dependent)
  void btn_add_dependent() {
    Intent intent = new Intent(DependentActivity.this, AddDependent.class);
    //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    startActivityForResult(intent, 220);
    overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
  }

  @Override
  public void onSessionExpired() {

  }

  @Override
  public void onLogout(String message, SessionManagerImpl sessionManager) {
    mAlertProgress = alertProgress.logOut(mContext, getString(R.string.logout), message, getString(R.string.ok), sessionManager);

  }

  @Override
  public void onError(String error) {

  }

  @Override
  public void onConnectionError(String connectionError) {

  }

  @Override
  public void onShowProgress() {
    mProgressBar.setVisibility(View.VISIBLE);
  }

  @Override
  public void onHideProgress() {
    mProgressBar.setVisibility(View.GONE);
  }


  @Override
  public void onDestroy() {
    super.onDestroy();
    if (mAlertProgress != null && mAlertProgress.isShowing()) {
      mAlertProgress.dismiss();
    }
    dependentPresenter.detachView();
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    finish();
    overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
  }
}
