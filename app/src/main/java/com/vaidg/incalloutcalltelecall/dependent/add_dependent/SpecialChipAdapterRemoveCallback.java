package com.vaidg.incalloutcalltelecall.dependent.add_dependent;

/**
 * <h2>SpecialChipAdapterRemoveCallback</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public interface SpecialChipAdapterRemoveCallback {
  void onRemove(String Id);
}
