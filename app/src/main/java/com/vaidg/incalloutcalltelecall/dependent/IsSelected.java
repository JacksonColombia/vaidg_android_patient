package com.vaidg.incalloutcalltelecall.dependent;

/**
 * <h2>IsSelected</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public interface IsSelected {

  /**
   * <h2>isSelect</h2>
   * This method is used to select the dependent
   *
   * @param isSelect  flag to chck whether it is selcted or not
   */
  void isSelect(boolean isSelect);
}
