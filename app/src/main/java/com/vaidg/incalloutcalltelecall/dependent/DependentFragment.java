package com.vaidg.incalloutcalltelecall.dependent;

import static android.app.Activity.RESULT_OK;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import com.vaidg.Login.LoginActivity;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.dependent.adapter.DependentAdapter;
import com.vaidg.incalloutcalltelecall.dependent.add_dependent.AddDependent;
import com.vaidg.incalloutcalltelecall.dependent.model.DependentsData;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.utility.AlertProgress;
import dagger.android.support.DaggerFragment;
import java.util.ArrayList;
import javax.inject.Inject;

/**
 * <h2>DependentFragment</h2>
 *
 * <p>A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link DependentFragment} factory method to
 * create an instance of this fragment.<p/>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class DependentFragment extends DaggerFragment implements View.OnClickListener,
    DependentContract.DependentView {
  @Inject
  AppTypeface appTypeface;
  @Inject
  DependentContract.DependentPresenter dependentPresenter;
  @Inject
  AlertProgress alertProgress;
  @BindView(R.id.iv_add_dependent)
  ImageView iv_add_dependent;
  @BindView(R.id.rvDependents)
  RecyclerView rvDependents;
  @BindView(R.id.no_dependent_added)
  TextView tvNoDependentsAdded;
  @BindView(R.id.tvConsultationLabel)
  TextView tvConsultationLabel;
  @BindView(R.id.tvDependents)
  TextView tvDependents;
  @BindView(R.id.radioButtonBidding)
  TextView radioButtonBidding;
  @BindView(R.id.txt_add_dependent)
  TextView txt_add_dependent;
  @BindView(R.id.progressBarDep)
  ProgressBar mProgressBar;
  private AlertDialog mAlertProgress;
  private IsSelected mIsSelected;
  private Context mContext;
  private DependentAdapter mDependentAdapter;
  private ArrayList<DependentsData> mDependentsDataArrayList = new ArrayList<>();
  //boolean field member initialized as false by default
  private boolean isLightOn;

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_dependent, container, false);
    ButterKnife.bind(this, view);
    try {
      mIsSelected = (IsSelected) getActivity();

    } catch (ClassCastException ex) {
      //.. should log the error or throw and exception
      Log.e("MyAdapter", "Must implement the CallbackInterface in the Activity", ex);
    }
    if (mIsSelected != null) {
      mIsSelected.isSelect(false);
    }
    isLightOn = false;
    onShowProgress();
    getDependentApiCall();
    initializeView(view);

    return view;
  }

  private void getDependentApiCall() {
    if (Utility.isNetworkAvailable(mContext)) {
      dependentPresenter.getDependents(mContext);
    } else {
      alertProgress.tryAgain(mContext, mContext.getResources().getString(R.string.pleaseCheckInternet),
          mContext.getResources().getString(R.string.system_error), isClicked -> {
            if (isClicked) {
              if(Utility.isNetworkAvailable(mContext))
              dependentPresenter.getDependents(mContext);
              Log.w("TAG", "onError: isNetwork Not Alert");
            }

          });

    }
  }

  /**
   * <h2>initializeView</h2>
   *
   * <p>Intiliazing view elements</p>
   */
  private void initializeView(View view) {
    iv_add_dependent.setOnClickListener(this);
    tvConsultationLabel.setTypeface(appTypeface.getHind_semiBold());
    tvDependents.setTypeface(appTypeface.getHind_medium());
    radioButtonBidding.setTypeface(appTypeface.getHind_medium());
    txt_add_dependent.setTypeface(appTypeface.getHind_medium());

    tvConsultationLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_18));
    radioButtonBidding.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_16));
    tvDependents.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_14));
    txt_add_dependent.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_16));

    radioButtonBidding.setOnClickListener(this);
    txt_add_dependent.setOnClickListener(this);
    rvDependents.setHasFixedSize(true);
    rvDependents.setLayoutManager(new LinearLayoutManager(mContext));
    mDependentAdapter = new DependentAdapter(
        mDependentsDataArrayList, dependentPresenter, appTypeface, true);
    rvDependents.setAdapter(mDependentAdapter);

    mDependentAdapter.setOnItemClickListener(new DependentAdapter.OnItemClickListener() {
      @Override
      public void onItemClick(View view, int position, ArrayList<DependentsData> dependentsData,
                              boolean isSelect) {
        dependentitemListClicked(position, dependentsData, isSelect);
      }

      @Override
      public void onItemEditClick(View view, int position,
                                  ArrayList<DependentsData> dependentsData) {
        Constants.isRelationCalled = false;
        DependentsData row_details = dependentsData.get(position);
        Intent intent = new Intent(mContext, AddDependent.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("row_details", row_details);
        intent.putExtras(bundle);
        //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 2);
        ((Activity) mContext).overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
      }
    });
    mDependentAdapter.notifyDataSetChanged();
  }

  private void dependentitemListClicked(int position,
                                        ArrayList<DependentsData> dependentsData, boolean isSelect) {
    radioButtonBidding.setSelected(false);
    radioButtonBidding.setCompoundDrawablesWithIntrinsicBounds(
        0, 0, R.drawable.ic_unselected, 0);
    mDependentAdapter.setItemSelected(position);
    isLightOn = false;
    mIsSelected.isSelect(isSelect);
  }

  /**
   * This is onResume() method, which is getting call each time.
   */
  @Override
  public void onResume() {
    super.onResume();
  }

  @Override
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.txt_add_dependent:
      case R.id.iv_add_dependent:
        Intent intent = new Intent(getActivity(), AddDependent.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, 220);
        break;
      case R.id.radioButtonBidding:
        if (isLightOn) {
          radioButtonBidding.setSelected(false);
          radioButtonBidding.setCompoundDrawablesWithIntrinsicBounds(
              0, 0, R.drawable.ic_unselected, 0);
          mIsSelected.isSelect(false);
        } else {
          radioButtonBidding.setSelected(true);
          radioButtonBidding.setCompoundDrawablesWithIntrinsicBounds(
              0, 0, R.drawable.ic_check, 0);
          mIsSelected.isSelect(true);
          Constants.dependentId = "1";
          for (DependentsData dependentsData : mDependentsDataArrayList) {
            dependentsData.setChecked(false);
          }
          mDependentAdapter.notifyDataSetChanged();
        }
        isLightOn = !isLightOn;
        break;
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == 220) {
      if (resultCode == RESULT_OK) {

        getDependentApiCall();

      }
    }

    if (requestCode == 2) {
      if (resultCode == RESULT_OK) {

        getDependentApiCall();

      }
    }

  }

  @Override
  public void refreshData(ArrayList<DependentsData> dependentsData, int position) {
    if (mDependentAdapter != null) {
      if (dependentsData.size() > 0) {
        mDependentsDataArrayList.remove(position);
        mDependentAdapter.notifyDataSetChanged();
        if (mDependentAdapter.getItemCount() == 0) {
          noDependentDataList();
        }
      } else {
        noDependentDataList();
      }
    }
  }

  @Override
  public void getDependentDataList(ArrayList<DependentsData> dependentsData) {
    tvConsultationLabel.setClickable(true);
    tvNoDependentsAdded.setVisibility(View.GONE);
    rvDependents.setVisibility(View.VISIBLE);
    for (DependentsData dependents : dependentsData) {
      if (dependents.isChecked()) {
        mIsSelected.isSelect(true);
      } else {
        mIsSelected.isSelect(false);
      }
    }
    mDependentsDataArrayList.clear();
    mDependentsDataArrayList.addAll(dependentsData);
    mDependentAdapter.notifyDataSetChanged();
  }

  @Override
  public void noDependentDataList() {
    tvConsultationLabel.setClickable(true);
    mIsSelected.isSelect(false);
    tvNoDependentsAdded.setVisibility(View.GONE);
    rvDependents.setVisibility(View.GONE);
    mDependentsDataArrayList.clear();
    mDependentAdapter.notifyDataSetChanged();
  }

  @Override
  public void onSessionExpired() {

  }

  @Override
  public void onLogout(String message, SessionManagerImpl sessionManager) {
    mAlertProgress = alertProgress.logOut(mContext, getString(R.string.logout), message, getString(R.string.ok), sessionManager);
  }

  @Override
  public void onError(String error) {

  }

  @Override
  public void onConnectionError(String connectionError) {
    getDependentApiCall();
  }

  @Override
  public void onPause() {
    super.onPause();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    if (mAlertProgress != null && mAlertProgress.isShowing()) {
      mAlertProgress.dismiss();
    }
  }

  @Override
  public void onShowProgress() {
    mProgressBar.setVisibility(View.GONE);
  }

  @Override
  public void onHideProgress() {
    mProgressBar.setVisibility(View.GONE);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    dependentPresenter.attachView(this);
    this.mContext = context;
  }

  @Override
  public void onDetach() {
    dependentPresenter.detachView();
    mContext = null;
    super.onDetach();
  }

}
