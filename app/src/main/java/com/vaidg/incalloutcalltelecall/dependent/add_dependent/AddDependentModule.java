package com.vaidg.incalloutcalltelecall.dependent.add_dependent;


import android.app.Activity;
import com.vaidg.Dagger2.ActivityScoped;
import dagger.Binds;
import dagger.Module;

/**
 * <h2>AddDependentModule</h2>
 *
 * This is a Dagger module. We use this to pass in the View dependency to the
 * {@link AddDependentPresenter}.
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
@Module
public abstract class AddDependentModule {

  @ActivityScoped
  @Binds
  abstract Activity addDependentActivity(AddDependent addDependent);

  @ActivityScoped
  @Binds
  abstract AddDependentPresenter addDependentPresenter(AddDependentPresenterImpl presenter);

  @ActivityScoped
  @Binds
  abstract AddDependentView addDependentView(AddDependent addDependent);

}
