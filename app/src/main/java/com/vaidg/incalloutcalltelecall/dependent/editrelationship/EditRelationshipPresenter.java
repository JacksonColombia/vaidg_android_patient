package com.vaidg.incalloutcalltelecall.dependent.editrelationship;

import android.content.Context;
import com.vaidg.utilities.SessionManager;
import com.utility.AlertProgress;

/**
 * <h2>EditRelationshipPresenter</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public interface EditRelationshipPresenter {


  /**
   * <h2>getRelation</h2>
   * This method is used to get  Relations of Dependents
   *
   * @param relation Relation of the dependent
   */
  void getRelation(String relation);

  /**
   * <h2>openFetchRelation</h2>
   * This method is used to get  Relations of Dependents
   *  @param sessionManager
   * @param auth Auth token
   * @param alertProgress
   */
  void openFetchRelation(Context context, SessionManager sessionManager,
      String auth, AlertProgress alertProgress);

}
