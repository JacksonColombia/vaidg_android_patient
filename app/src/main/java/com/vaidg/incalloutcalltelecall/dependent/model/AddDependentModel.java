package com.vaidg.incalloutcalltelecall.dependent.model;

import android.util.Patterns;

/**
 * <h2>AddDependentModel</h2>
 *
 * <p>Add Dependents Request DATA object </p>
 *
 * @author 3Embed
 * @since 12-07-2019
 */

public class AddDependentModel {

  public boolean validateFirstName(final String firstName) {
    return firstName == null || "".equals(firstName);
  }

  public boolean validateLastName(final String lastName) {
    return lastName == null || "".equals(lastName);
  }

  public boolean emptyPhone(String phone) {
    return phone == null || phone.length() == 0 || "".equals(phone);
  }

  public boolean validatePhone(String phone) {
    return !Patterns.PHONE.matcher(phone).matches();
  }

  public boolean irregularPhone(String phone, int max) {
    return phone.length() < max;
  }

  public boolean emptyDateOfBirth(String date_of_birth) {
    return date_of_birth == null || date_of_birth.length() == 0 || "".equals(date_of_birth);
  }


}