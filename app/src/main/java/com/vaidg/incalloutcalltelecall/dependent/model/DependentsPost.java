package com.vaidg.incalloutcalltelecall.dependent.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h2>DependentsPost</h2>
 *
 * <p>Add Dependents Response POJO</p>
 *
 * @author 3Embed
 * @since 12-07-2019
 */
public class DependentsPost implements Serializable {

  @SerializedName("message")
  @Expose
  private String message;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

}
