package com.vaidg.incalloutcalltelecall.dependent.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 * <h2>DependentsData</h2>
 *
 * <p>Get Dependents Response DATA object</p>
 *
 * @author 3Embed
 * @since 12-07-2019
 */
public class DependentsData implements Parcelable {

  public final static Parcelable.Creator<DependentsData> CREATOR = new Creator<DependentsData>() {


    @SuppressWarnings({
        "unchecked"
    })
    public DependentsData createFromParcel(Parcel in) {
      return new DependentsData(in);
    }

    public DependentsData[] newArray(int size) {
      return (new DependentsData[size]);
    }

  };
  @SerializedName("isChecked")
  @Expose
  private boolean isChecked;
  @SerializedName("dependentId")
  @Expose
  private String dependentId;
  @SerializedName("firstName")
  @Expose
  private String firstName;
  @SerializedName("lastName")
  @Expose
  private String lastName;
  @SerializedName("profilePic")
  @Expose
  private String profilePic;
  @SerializedName("relationship")
  @Expose
  private String relationship;
  @SerializedName("countryCode")
  @Expose
  private String countryCode;
  @SerializedName("countryCodeSymbol")
  @Expose
  private String countryCodeSymbol;
  @SerializedName("phone")
  @Expose
  private String phone;
  @SerializedName("gender")
  @Expose
  private int gender;
  @SerializedName("dateOfBirth")
  @Expose
  private String dateOfBirth;
  @SerializedName("specialRequirement")
  @Expose
  private ArrayList<Object> specialRequirement = new ArrayList<>();
  @SerializedName("specialInstruction")
  @Expose
  private String specialInstruction;
  @SerializedName("deleted")
  @Expose
  private boolean deleted;

  protected DependentsData(Parcel in) {
    this.dependentId = ((String) in.readValue((String.class.getClassLoader())));
    this.firstName = ((String) in.readValue((String.class.getClassLoader())));
    this.lastName = ((String) in.readValue((String.class.getClassLoader())));
    this.profilePic = ((String) in.readValue((String.class.getClassLoader())));
    this.relationship = ((String) in.readValue((String.class.getClassLoader())));
    this.countryCode = ((String) in.readValue((String.class.getClassLoader())));
    this.countryCodeSymbol = ((String) in.readValue((String.class.getClassLoader())));
    this.phone = ((String) in.readValue((String.class.getClassLoader())));
    this.gender = ((int) in.readValue((int.class.getClassLoader())));
    this.dateOfBirth = ((String) in.readValue((String.class.getClassLoader())));
    in.readList(this.specialRequirement, (java.lang.Object.class.getClassLoader()));
    this.specialInstruction = ((String) in.readValue((String.class.getClassLoader())));
    this.deleted = ((boolean) in.readValue((boolean.class.getClassLoader())));
    this.isChecked = ((boolean) in.readValue((boolean.class.getClassLoader())));
  }

  /**
   * No args constructor for use in serialization
   */
  public DependentsData() {
  }

  /**
   * @param lastName
   * @param gender
   * @param specialInstruction
   * @param profilePic
   * @param dateOfBirth
   * @param specialRequirement
   * @param dependentId
   * @param firstName
   * @param deleted
   * @param phone
   * @param countryCode
   * @param relationship
   * @param countryCodeSymbol
   */
  public DependentsData(String dependentId, String firstName, String lastName, String profilePic, String relationship, String countryCode, String countryCodeSymbol, String phone, int gender, String dateOfBirth, ArrayList<Object> specialRequirement, String specialInstruction, boolean deleted, boolean isChecked) {
    super();
    this.dependentId = dependentId;
    this.firstName = firstName;
    this.lastName = lastName;
    this.profilePic = profilePic;
    this.relationship = relationship;
    this.countryCode = countryCode;
    this.countryCodeSymbol = countryCodeSymbol;
    this.phone = phone;
    this.gender = gender;
    this.dateOfBirth = dateOfBirth;
    this.specialRequirement = specialRequirement;
    this.specialInstruction = specialInstruction;
    this.deleted = deleted;
    this.isChecked = isChecked;
  }

  public boolean isChecked() {
    return isChecked;
  }

  public void setChecked(boolean checked) {
    isChecked = checked;
  }

  public String getDependentId() {
    return dependentId;
  }

  public void setDependentId(String dependentId) {
    this.dependentId = dependentId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getProfilePic() {
    return profilePic;
  }

  public void setProfilePic(String profilePic) {
    this.profilePic = profilePic;
  }

  public String getRelationship() {
    return relationship;
  }

  public void setRelationship(String relationship) {
    this.relationship = relationship;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getCountryCodeSymbol() {
    return countryCodeSymbol;
  }

  public void setCountryCodeSymbol(String countryCodeSymbol) {
    this.countryCodeSymbol = countryCodeSymbol;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public int getGender() {
    return gender;
  }

  public void setGender(int gender) {
    this.gender = gender;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public ArrayList<Object> getSpecialRequirement() {
    return specialRequirement;
  }

  public void setSpecialRequirement(ArrayList<Object> specialRequirement) {
    this.specialRequirement = specialRequirement;
  }

  public String getSpecialInstruction() {
    return specialInstruction;
  }

  public void setSpecialInstruction(String specialInstruction) {
    this.specialInstruction = specialInstruction;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }


  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(dependentId);
    dest.writeValue(firstName);
    dest.writeValue(lastName);
    dest.writeValue(profilePic);
    dest.writeValue(relationship);
    dest.writeValue(countryCode);
    dest.writeValue(countryCodeSymbol);
    dest.writeValue(phone);
    dest.writeValue(gender);
    dest.writeValue(dateOfBirth);
    dest.writeList(specialRequirement);
    dest.writeValue(specialInstruction);
    dest.writeValue(deleted);
    dest.writeValue(isChecked);
  }

  public int describeContents() {
    return 0;
  }

}
