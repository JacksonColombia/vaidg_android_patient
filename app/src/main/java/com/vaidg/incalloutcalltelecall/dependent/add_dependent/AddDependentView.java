package com.vaidg.incalloutcalltelecall.dependent.add_dependent;


import com.vaidg.home.BaseView;
import com.vaidg.incalloutcalltelecall.dependent.model.SpecialRequirementsData;
import java.util.List;

/**
 * <h2>AddDependentView</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public interface AddDependentView extends BaseView {

  /**
   * <h2>showProgress</h2>
   * This method is used to show progress bar on call of APIS
   *
   * @param text_message message to show on ProgressBar
   */
  void showProgress(String text_message);

  /**
   * <h2>hideProgress</h2>
   * This method is used to hide progress bar after the call of API success
   */
  void hideProgress();

  /**
   * <h2>setMobileErrorMsg</h2>
   * This method is used to set Mobile Error message
   *
   * @param mobileErrorMsg error message to show
   */
  void setMobileErrorMsg(String mobileErrorMsg);


  /**
   * <h2>setMobileErrorMsg</h2>
   * This method is used to show invalid mobile number.
   * Acts as a generic method where view will display the message.
   */
  void setMobileError();

  /**
   * <h2>setDateOfBirth</h2>
   * This method is used to set Date of Birth editText
   *
   * @param dateOfBirth date of birth text
   */
  void setDateOfBirth(String dateOfBirth);

  /**
   * <h2>setDateOfBirth</h2>
   * This method is used to set Error message from API calls
   *
   * @param message Message
   */
  void onError(String message);

  /**
   * <h2>navToDependentsList</h2>
   * This method is used to navigate to the dependents list screen
   * @param isDependentCalled
   */
  void navToDependentsList(boolean isDependentCalled);


  /**
   * <h2>setSpecialRequirements</h2>
   * This method is used to set the special Requirements from the API call.
   */
  void setSpecialRequirements(List<SpecialRequirementsData> specialRequirements);


  /**
   * <h2>setRelationValue</h2>
   *
   * This method is used to set the relationship of the dependent from the
   *  EditRelationShip screen.
   *
   * @param dependent_fname First Name of the dependent
   * @param dependent_lname Last Name of the dependent
   * @param dob             Date of birth of the dependent
   * @param mobile_no       Mobile no of the dependent
   * @param gender          Gender of the dependent
   *
   */
  void setValuesToFields(String dependent_fname, String dependent_lname, String dob,
      String mobile_no, String gender);


}
