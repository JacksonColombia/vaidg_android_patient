package com.vaidg.incalloutcalltelecall.dependent.editrelationship;

import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.SESSION_EXPIRED;
import static com.vaidg.utilities.Constants.SESSION_LOGOUT;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;

import android.content.Context;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.dependent.model.EditRelationResponsePojo;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManager;
import com.google.gson.Gson;
import com.pojo.ErrorHandel;
import com.utility.AlertProgress;
import com.utility.RefreshToken;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * <h2>EditRelationshipPresenterImpl</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class EditRelationshipPresenterImpl implements EditRelationshipPresenter {

  @Inject
  EditRelationshipView view;
  @Inject
  Gson mGson;

  @Inject
  LSPServices mLSPServices;

  @Inject
  public EditRelationshipPresenterImpl() {
  }

  @Override
  public void getRelation(String relation) {

  }

  @Override
  public void openFetchRelation(Context context, SessionManager sessionManager,
      String auth, AlertProgress alertProgress) {

    Observable<Response<ResponseBody>> observable = mLSPServices.onToGetRelation(auth,
        Constants.selLang, Constants.PLATFORM_ANDROID);
    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int code = responseBodyResponse.code();
            JSONObject errJsonD;
            try {
              String response =
                  responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
              String errorBody =
                  responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null;
              switch (code) {
                case SUCCESS_RESPONSE:
                  if (response!= null && !response.isEmpty()) {
                    EditRelationResponsePojo responsePojo = mGson.fromJson(response,
                        EditRelationResponsePojo.class);
                    if(view != null)
                    view.onResponseSuccess(responsePojo.getData());
                  }
                  break;
                case SESSION_LOGOUT:
                  if (errorBody!= null && !errorBody.isEmpty()) {
                    errJsonD = new JSONObject(errorBody);
                    errJsonD.getString(MESSAGE);
                    if (!errJsonD.getString(MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onHideProgress();
                        view.onLogout(errJsonD.getString(MESSAGE), sessionManager);
                      }
                    }
                  }
                  break;
                case SESSION_EXPIRED:
                  if(errorBody != null && !errorBody.isEmpty()){
                    ErrorHandel errorHandel = mGson.fromJson(errorBody,ErrorHandel.class);
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),
                        sessionManager.getREFRESHAUTH(), mLSPServices,
                        new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {
                            if(view != null)
                              view.onHideProgress();
                              LSPApplication.getInstance().setAuthToken(sessionManager.getSID(),sessionManager.getSID(),newToken);

                          }

                          @Override
                          public void onFailureRefreshToken() {
                            if(view != null)
                              view.onHideProgress();
                          }

                          @Override
                          public void sessionExpired(String msg) {
                            if(view != null)
                              view.onHideProgress();
                          }
                        });}
                  break;
                default:
                  if (errorBody!= null && !errorBody.isEmpty()) {
                    errJsonD = new JSONObject(errorBody);
                    errJsonD.getString(MESSAGE);
                    if (!errJsonD.getString(MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onHideProgress();
                        view.onError(errJsonD.getString(MESSAGE));
                      }
                    }
                  }
                  break;
              }
            } catch (IOException | JSONException e) {
              if(view != null)
                view.onHideProgress();
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null)
              view.onHideProgress();
          }

          @Override
          public void onComplete() {

          }
        });
  }

}
