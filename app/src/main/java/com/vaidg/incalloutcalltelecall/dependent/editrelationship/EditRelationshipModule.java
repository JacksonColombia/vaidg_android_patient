package com.vaidg.incalloutcalltelecall.dependent.editrelationship;


import android.app.Activity;
import com.vaidg.Dagger2.ActivityScoped;
import dagger.Binds;
import dagger.Module;

/**
 * <h2>EditRelationshipModule</h2>
 *
 * This is a Dagger module. We use this to pass in the View dependency to the
 * {@link EditRelationshipPresenter}.
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
@Module
public abstract class EditRelationshipModule {

  @ActivityScoped
  @Binds
  abstract Activity editRelationshipActivity(EditRelationshipActivity editRelationshipActivity);


  @ActivityScoped
  @Binds
  abstract EditRelationshipPresenter editRelationshipPresenter(
      EditRelationshipPresenterImpl presenter);

  @ActivityScoped
  @Binds
  abstract EditRelationshipView editRelationshipView(
      EditRelationshipActivity editRelationshipActivity);

}
