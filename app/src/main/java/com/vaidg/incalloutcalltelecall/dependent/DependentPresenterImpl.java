package com.vaidg.incalloutcalltelecall.dependent;

import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.SESSION_EXPIRED;
import static com.vaidg.utilities.Constants.SESSION_LOGOUT;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;

import android.content.Context;
import android.util.Log;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.dependent.model.DependentsData;
import com.vaidg.incalloutcalltelecall.dependent.model.DependentsGet;
import com.vaidg.incalloutcalltelecall.dependent.model.DependentsPost;
import com.vaidg.networking.LSPServices;
import com.vaidg.networking.NoConnectivityException;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.google.gson.Gson;
import com.pojo.ErrorHandel;
import com.utility.RefreshToken;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * <h2>DependentPresenter</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class DependentPresenterImpl implements DependentContract.DependentPresenter {

  @Inject
  LSPServices mLSPServices;
  @Inject
  Gson mGson;
  @Inject
  SessionManagerImpl sessionManager;
  private DependentContract.DependentView view;

  @Inject
  DependentPresenterImpl() {
  }


  @Override
  public void attachView(DependentContract.DependentView view) {
    this.view = view;
  }

  @Override
  public void detachView() {
    view = null;
  }

  @Override
  public void onDeleteDependent(ArrayList<DependentsData> dependentsData, int position, Context context) {
    Observable<Response<ResponseBody>> services = mLSPServices.deleteDependent(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),
        PLATFORM_ANDROID,
        dependentsData.get(position).getDependentId());
    services.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> dependentsPostResponse) {
            int code = dependentsPostResponse.code();
            JSONObject jsonObject;
            try {
              String response =
                  dependentsPostResponse.body() != null ? dependentsPostResponse.body().string()
                      : null;
              String errorBody = dependentsPostResponse.errorBody() != null
                  ? dependentsPostResponse.errorBody().string() : null;
              switch (code) {
                case SUCCESS_RESPONSE:
                  if (response != null && !response.isEmpty()) {
                    DependentsPost dependentsPost = mGson.fromJson(response,
                        DependentsPost.class);
                    if(view != null)
                    view.refreshData(dependentsData, position);
                  }
                  if(view != null)
                    view.onHideProgress();
                  break;
                case SESSION_EXPIRED:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    // jsonObject = new JSONObject(response);
                    ErrorHandel errorHandel = mGson.fromJson(errorBody, ErrorHandel.class);
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),
                        sessionManager.getREFRESHAUTH(), mLSPServices,
                        new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {
                            if(view != null)
                              view.onHideProgress();
                              LSPApplication.getInstance().setAuthToken(sessionManager.getSID(),sessionManager.getSID(),newToken);
                            onDeleteDependent(dependentsData, position, context);
                          }

                          @Override
                          public void onFailureRefreshToken() {

                          }

                          @Override
                          public void sessionExpired(String msg) {
                            if(view != null) {
                              view.onLogout(msg, sessionManager);
                              view.onHideProgress();
                            }
                          }
                        });
                  }
                  break;
                case SESSION_LOGOUT:
                  if (errorBody != null && !errorBody.isEmpty()) {

                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onHideProgress();
                        view.onLogout(jsonObject.getString(MESSAGE), sessionManager);
                      }
                    }
                  }
                  break;
                default:
                  if (errorBody != null && !errorBody.isEmpty()) {

                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      {
                        view.onHideProgress();
                        view.onError(jsonObject.getString(MESSAGE));
                      }
                    }
                  }
                  break;

              }
            } catch (Exception e) {
              e.printStackTrace();
              if(view != null)
                view.onHideProgress();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null)
              view.onHideProgress();
          }

          @Override
          public void onComplete() {

          }
        });
  }


  public void getDependents(Context mcontext) {
    Observable<Response<ResponseBody>> responseObservable = mLSPServices.getDependents(
        LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),
        Constants.selLang, PLATFORM_ANDROID);
    responseObservable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> dependentsGetResponse) {
            int code = dependentsGetResponse.code();
            JSONObject jsonObject;
            try {
              String response =
                  dependentsGetResponse.body() != null ? dependentsGetResponse.body().string()
                      : null;
              String errorBody = dependentsGetResponse.errorBody() != null
                  ? dependentsGetResponse.errorBody().string() : null;

              switch (code) {
                case SUCCESS_RESPONSE:
                  if (response != null && !response.isEmpty()) {

                    Constants.isDependentCalled = false;

                    DependentsGet dependentsGet = mGson.fromJson(response,
                        DependentsGet.class);

                    if (dependentsGet.getData() != null && dependentsGet.getData().size() > 0) {
                      if(view != null)
                      view.getDependentDataList(dependentsGet.getData());
                    } else {
                      if(view != null)
                      view.noDependentDataList();
                    }
                  }
                  if(view != null)
                  view.onHideProgress();
                  break;

                case SESSION_EXPIRED:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    // jsonObject = new JSONObject(response);
                    ErrorHandel errorHandel = mGson.fromJson(errorBody, ErrorHandel.class);

                    //  jsonObject = new JSONObject(response);
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),
                        sessionManager.getREFRESHAUTH(), mLSPServices,
                        new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {
                            if(view != null)
                            view.onHideProgress();
                              LSPApplication.getInstance().setAuthToken(sessionManager.getSID(),sessionManager.getSID(),newToken);
                            getDependents(mcontext);
                          }

                          @Override
                          public void onFailureRefreshToken() {

                          }

                          @Override
                          public void sessionExpired(String msg) {
                            if(view != null) {
                              view.onLogout(msg, sessionManager);
                              view.onHideProgress();
                            }
                          }
                        });
                  }
                  break;
                case SESSION_LOGOUT:
                  if (errorBody != null && !errorBody.isEmpty()) {

                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onLogout(jsonObject.getString(MESSAGE), sessionManager);
                      }
                    }
                  }
                  if(view != null)
                  view.onHideProgress();
                  break;
                default:
                  if (errorBody != null && !errorBody.isEmpty()) {

                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      if(view != null)
                      view.onError(jsonObject.getString(MESSAGE));
                    }
                  }
                  if(view != null)
                  view.onHideProgress();
                  break;

              }
            } catch (Exception e) {
              e.printStackTrace();
              if(view != null)
                view.onHideProgress();
            }
          }

          @Override
          public void onError(Throwable e) {
            e.printStackTrace();
            if(view != null)
              view.onHideProgress();
            if (e instanceof NoConnectivityException) {
              Log.w("TAG", "onError: isNetwork Not" );
              if(view != null)
              view.onConnectionError(mcontext.getResources().getString(R.string.pleaseCheckInternet));
            }
          }


          @Override
          public void onComplete() {

          }
        });


  }


}

