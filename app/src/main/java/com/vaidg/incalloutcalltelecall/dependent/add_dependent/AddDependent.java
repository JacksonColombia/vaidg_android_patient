package com.vaidg.incalloutcalltelecall.dependent.add_dependent;

import static com.vaidg.utilities.Constants.CAMERAIMAGE;
import static com.vaidg.utilities.Constants.RELATION_TYPE;
import static com.vaidg.utilities.Constants.dateOB;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import com.vaidg.R;
import com.vaidg.countrypic.Country;
import com.vaidg.countrypic.CountryPicker;
import com.vaidg.countrypic.CountryPickerListener;
import com.vaidg.incalloutcalltelecall.dependent.editrelationship.EditRelationshipActivity;
import com.vaidg.incalloutcalltelecall.dependent.model.DependentsAddPojo;
import com.vaidg.incalloutcalltelecall.dependent.model.DependentsData;
import com.vaidg.incalloutcalltelecall.dependent.model.SpecialRequirementsData;
import com.vaidg.utilities.AppPermissionsRunTime;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.HandlePictureEvents;
import com.vaidg.utilities.ImageUploadedAmazon;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.Scaler;
import com.vaidg.utilities.SessionManager;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;
import com.utility.AlertProgress;
import dagger.android.support.DaggerAppCompatActivity;
import eu.janmuller.android.simplecropimage.CropImage;
import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

/**
 * /**
 * <h2>AddDependent</h2>
 * <p>
 * This class is used to add new dependent in dependent screen
 * </p>
 *
 * @author DJ
 * @since 29-11-2019.
 */
public class AddDependent extends DaggerAppCompatActivity implements AddDependentView,
    SpecialRequirementCallback, SpecialChipAdapterRemoveCallback {

  private static final String TAG = "AddDependent";

  /*=========================TextInputLayout===============================*/
  private final int EDITRELATIONSHIPREQUEST = 434;
  @Inject
  AddDependentPresenter mAddDependentPresenter;
  @BindView(R.id.til_dep_fnm)
  TextInputLayout til_dep_fnm;
  @BindView(R.id.til_dep_lnm)
  TextInputLayout til_dep_lnm;

  /*=========================EditText===============================*/
  @BindView(R.id.til_dep_dob)
  TextInputLayout til_dep_dob;
  @BindView(R.id.til_dep_reltn)
  TextInputLayout til_dep_reltn;
  @BindView(R.id.et_dep_fnm)
  EditText et_dep_fnm;
  @BindView(R.id.et_dep_lnm)
  EditText et_dep_lnm;

  /*=========================Mobile no & country Picker===============================*/
  @BindView(R.id.etRelation)
  EditText etRelation;
  @BindView(R.id.etDepDob)
  EditText et_dep_dob;
  @BindView(R.id.et_dep_mobno)
  EditText et_dep_mobno;


  /*=========================ImageView===============================*/
  @BindView(R.id.countryFlag)
  ImageView ivCountryFlag;
  @BindView(R.id.countryCode)
  TextView tvCountryCode;
  @BindView(R.id.ivDependentPic)
  ImageView ivDependentPic;
  @BindView(R.id.ivDepCalendr)
  ImageView ivDepCalendr;
  @BindView(R.id.tvSaveProfile)
  TextView tvSaveProfile;
  @BindView(R.id.rgGender)
  RadioGroup rgGender;
  @BindView(R.id.tvGender)
  TextView tvGender;
  @BindView(R.id.radioM)
  RadioButton radioM;
  @BindView(R.id.radioF)
  RadioButton radioF;
  //@Inject
  HandlePictureEvents mHandlePictureEvents;
  @Inject
  AppTypeface mAppTypeface;
  @Inject
  SessionManager mSessionManager;
  @Inject
  AlertProgress mAlertProgress;
  @Inject
  CountryPicker mCountryPicker;
  //
  boolean isValid = false;
  private View focusView;
  private DependentsData mDependentsData;
  private AlertDialog mAlertDialog;
  private AlertDialog.Builder mBuilder;
  private String image_url = "";
  private String selectedIds = "",countryCodeSymbol = "";
  private int phoneNumLength = 0;
  @Inject
  AlertProgress alertProgress;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    // TODO Auto-generated method stub
    super.onCreate(savedInstanceState);
    setContentView(R.layout.add_dependent_layout);
    ButterKnife.bind(this);
    initializeObjects();
    initializeBundle();
    initializeToolBar();
  }


  private void initializeObjects() {
    mHandlePictureEvents = new HandlePictureEvents(this);
    mCountryPicker = new CountryPicker();

    et_dep_fnm.setTypeface(mAppTypeface.getHind_regular());
    et_dep_lnm.setTypeface(mAppTypeface.getHind_regular());
    til_dep_dob.setTypeface(mAppTypeface.getHind_regular());
    tvCountryCode.setTypeface(mAppTypeface.getHind_regular());
    et_dep_mobno.setTypeface(mAppTypeface.getHind_regular());
    et_dep_dob.setTypeface(mAppTypeface.getHind_regular());
    tvGender.setTypeface(mAppTypeface.getHind_regular());
    radioM.setTypeface(mAppTypeface.getHind_regular());
    radioF.setTypeface(mAppTypeface.getHind_regular());
    etRelation.setTypeface(mAppTypeface.getHind_regular());

    til_dep_reltn.setTypeface(mAppTypeface.getHind_regular());
    tvSaveProfile.setTypeface(mAppTypeface.getHind_regular());

    mCountryPicker.setListener(new CountryPickerListener() {
      @Override
      public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID,
                                  int maxDigit) {
        countryCodeSymbol = code;
        mCountryPicker.dismiss();
        setPhoneNumber(dialCode, flagDrawableResID, maxDigit);
      }
    });
    Country userCountryInfo = mCountryPicker.getUserCountryInfo(this);
    countryCodeSymbol = userCountryInfo.getCode();
    setPhoneNumber(userCountryInfo.getDial_code(), userCountryInfo.getFlag(),
        Integer.parseInt(userCountryInfo.getMax_digits()));
  }

  private void initializeToolBar() {
    Toolbar toolBarMainAct = findViewById(R.id.toolbar);
    setSupportActionBar(toolBarMainAct);
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayShowHomeEnabled(true);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    TextView tvAbarTitle = toolBarMainAct.findViewById(R.id.tvAbarTitle);
    ImageView iv_load_contact = toolBarMainAct.findViewById(R.id.ivAbarRightBtn);
    iv_load_contact.setVisibility(View.INVISIBLE);
    iv_load_contact.setImageResource(R.drawable.ic_contacts);
    iv_load_contact.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        checkContactPermission();
      }
    });

    if (mDependentsData != null) {
      tvAbarTitle.setText(getResources().getString(R.string.edit_dependent));
    } else {
      tvAbarTitle.setText(getResources().getString(R.string.add_dependent));
    }

    toolBarMainAct.setNavigationIcon(R.drawable.ic_back);
    toolBarMainAct.setNavigationOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        //ConstantsInteface.isDependentCalled = true;
        onBackPressed();
      }
    });
    tvAbarTitle.setTypeface(mAppTypeface.getHind_semiBold());
  }

  private void setPhoneNumber(String dialCode, int flagDrawableResID, int maxDigit) {
    tvCountryCode.setText(dialCode);
    ivCountryFlag.setImageResource(flagDrawableResID);
    InputFilter[] inputFilters = new InputFilter[1];
    inputFilters[0] = new InputFilter.LengthFilter(maxDigit);
    et_dep_mobno.setFilters(inputFilters);
    phoneNumLength = maxDigit;
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    finish();
    overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
  }


  /**
   * @author Pramod
   * @since 01-04-2018
   * <h2>initializeBundle</h2>
   * This method is used to initialize the bundle
   */
  private void initializeBundle() {
    Bundle bundle = getIntent().getExtras();
    if (bundle != null) {
      if (Constants.isRelationCalled) {
        String relation = bundle.getString("relationship");
        setRelationValue(relation);
      } else {
        mDependentsData = (DependentsData) bundle.getParcelable("row_details");
        if (mDependentsData != null) {
          et_dep_fnm.setText(mDependentsData.getFirstName());
          et_dep_lnm.setText(mDependentsData.getLastName());
          et_dep_mobno.setText(mDependentsData.getPhone());
          et_dep_dob.setText(mDependentsData.getDateOfBirth());
          etRelation.setText(mDependentsData.getRelationship());
          image_url = mDependentsData.getProfilePic();
          if (mDependentsData.getGender() == 1)
            radioM.setChecked(true);
          else
            radioF.setChecked(true);

          if (image_url != null && !image_url.isEmpty()) {
            double[] size = Scaler.getScalingFactor(this);
            double height = (140) * size[1];
            double width = (140) * size[0];
            Picasso.with(AddDependent.this).load(image_url)
                .resize((int) width, (int) height)
                .centerCrop().transform(new PicassoCircleTransform())
                .into(ivDependentPic);
          }
        }
        String drawableName = "flag_"
                + mDependentsData.getCountryCodeSymbol().trim().toLowerCase(Locale.ENGLISH);
        ivCountryFlag.setImageResource(getResId(drawableName));

      }
    } else {
      mAddDependentPresenter.clearDependentValues();
    }
  }
  public static int getResId(String drawableName) {
    try {
      Class res = R.drawable.class;
      Field field = res.getField(drawableName);
      int drawableId = field.getInt(null);
      System.out.println("resource ids****" + drawableId);
      return drawableId;
    } catch (Exception e) {
      Log.e("MyTag", "Failure to get drawable id.", e);
      return 0;
    }
  }


  @Override
  public void showProgress(String text) {
    if (!isFinishing()) {
      mBuilder = new AlertDialog.Builder(AddDependent.this);
      LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      assert inflater != null;
      View dialogView = inflater.inflate(R.layout.progress_dialog_layout, null);
      TextView tv_progress = dialogView.findViewById(R.id.tv_progress);
      if (text.equals("ADD_DEPENDENT")) {
        tv_progress.setText(getString(R.string.adding_dependent));
      } else if (text.equals("EDIT_DEPENDENT")) {
        tv_progress.setText(getString(R.string.edit_dependent));
      } else if (text.equals("GET_SPECIAL_REQ")) {
        tv_progress.setText(getString(R.string.loading));
      } else {
        tv_progress.setText(getString(R.string.loading));
      }
      mBuilder.setView(dialogView);
      mBuilder.setCancelable(false);
      mAlertDialog = mBuilder.create();
      mAlertDialog.show();
    }
  }

  @Override
  public void hideProgress() {
    if (mAlertProgress != null && mAlertDialog.isShowing()) {
      mAlertDialog.dismiss();
      if (Constants.isDependentCalled) {
        new Handler().postDelayed(new Runnable() {
          @Override
          public void run() {
            finish();
          }
        }, 1000);
      }
    }
  }


  @Override
  public void onSessionExpired() {

  }

  @Override
  public void onLogout(String message, SessionManagerImpl sessionManager) {
    mAlertProgress.alertPositiveOnclick(this, message, getString(R.string.logout),
        getString(R.string.ok), isClicked -> Utility.setMAnagerWithBID(this, mSessionManager));

  }

  @Override
  public void onError(String message) {
    Toast.makeText(AddDependent.this, "" + message, Toast.LENGTH_LONG).show();
  }

  @Override
  public void onConnectionError(String connectionError) {

  }

  @Override
  public void onShowProgress() {

  }

  @Override
  public void onHideProgress() {

  }

  @Override
  public void setSpecialRequirements(List<SpecialRequirementsData> specialRequirements) {
    if (mDependentsData != null) {
      for (int i = 0; i < specialRequirements.size(); i++) {
      }
    }
  }

  @Override
  public void navToDependentsList(boolean isDependentCalled) {
    if (isDependentCalled) {
      Intent intent = new Intent();
      setResult(RESULT_OK, intent);
      // startActivityForResult(intent, ConstantsInteface.ADD_DEPENDENT_CODE);
      finish();
    } else {
      Intent intent = new Intent();
      setResult(RESULT_OK, intent);
      // startActivityForResult(intent, ConstantsInteface.ADD_DEPENDENT_CODE);
      finish();
    }
  }

  @Override
  public void setMobileErrorMsg(String mobileErrorMsg) {
    // Toast.makeText(AddDependent.this, mobileErrorMsg, Toast.LENGTH_LONG).show();
    isValid = false;
  }

  @Override
  public void setMobileError() {
    isValid = true;
    if (et_dep_mobno.getText().toString().trim().isEmpty()) {
      et_dep_mobno.setError(getString(R.string.errMobile));
    } else {
      et_dep_mobno.setError(getString(R.string.mobile_invalid));
    }

  }

  @Override
  public void setDateOfBirth(String dateOfBirth) {
    et_dep_dob.setText(dateOfBirth);
  }


  /**
   * @param value Value for Relationship
   *              <h2>setRelationValue</h2>
   *              This method is used to set the relationship of the dependent from the
   *              EditRelationShip screen
   * @author Pramod
   * @since 04-04-2018
   */
  public void setRelationValue(String value) {
        /*et_dep_fnm.setText(sessionManager.getDependentFname());
        et_dep_lnm.setText(sessionManager.getDependentLname());
        et_dep_dob.setText(sessionManager.getDateOBirth());
        et_dep_mobno.setText(sessionManager.getMobileNo());

        if ("Male".equals(sessionManager.getGender()))
            radioM.setChecked(true);
        else
            radioF.setChecked(true);*/

    //Sets values to the views such as editText from the sessionManager
    mAddDependentPresenter.setValuesToViews();

    etRelation.setText(value);
  }

  /**
   * <h2>focusVieW</h2>
   * this set the focus to the number
   *
   * @param til_reg_lyout is a TextInputLayout
   */
  private void focusVieW(TextInputLayout til_reg_lyout) {
    focusView = til_reg_lyout;
  }

  /**
   * <h2>focusviewNull</h2>
   * set the focus view to null
   */
  private void focusviewNull() {
    focusView = null;
  }

  /**
   * this is to null the focusview
   *
   * @param til_dep_fnm is a TextInputLayout
   */
  private void errorFocusNull(TextInputLayout til_dep_fnm) {

    til_dep_fnm.setErrorEnabled(false);
    til_dep_fnm.setError("");
    focusView = null;
  }


  @OnFocusChange({R.id.et_dep_fnm, R.id.et_dep_lnm, R.id.et_dep_mobno, R.id.etDepDob,
      R.id.etRelation})
  void onFocusChange(View v, boolean hasFocus) {
    switch (v.getId()) {

      case R.id.et_dep_fnm:
        if (!hasFocus) {
          if (!mAddDependentPresenter.validateFirstName(et_dep_fnm.getText().toString().trim())) {
            focusviewNull();
            errorFocusNull(til_dep_fnm);
          } else {
            til_dep_fnm.setError(getString(R.string.first_name_mandatory));
            focusVieW(til_dep_fnm);
          }
        }
        break;

      case R.id.et_dep_lnm:
        if (!hasFocus) {
          errorFocusNull(til_dep_lnm);
        }

        break;

      case R.id.et_dep_mobno:
        if (!hasFocus) {
          Utility.hideKeyboard(AddDependent.this);
          mAddDependentPresenter.validatePhoneNumber(AddDependent.this, tvCountryCode.getText().toString(),
              et_dep_mobno.getText().toString());

        }
        break;

      case R.id.etDepDob:
        if (!hasFocus) {
          et_dep_dob.setCursorVisible(false);
          if (et_dep_dob.getText().toString().trim().length() > 0) {
            focusviewNull();

            if (mAddDependentPresenter.checkDobValidity()) {
              et_dep_dob.setFocusable(false);
            } else {
              et_dep_dob.setFocusable(false);
              //et_dep_dob.setText("");
              mAddDependentPresenter.openCalendar(this);

            }
          } else {

          }
        } else {
          if (et_dep_dob.getText().toString().trim().length() > 0) {
            if (mAddDependentPresenter.checkDobValidity()) {
              errorFocusNull(til_dep_dob);
              et_dep_dob.setFocusable(false);
            } else {
              et_dep_dob.setText("");
            }

          } else {
            mAddDependentPresenter.openCalendar(this);
          }
          InputMethodManager imm = (InputMethodManager) getSystemService(
              Context.INPUT_METHOD_SERVICE);
          if (imm != null) {
            imm.hideSoftInputFromWindow(et_dep_dob.getWindowToken(), 0);
          }
        }

        break;
    }
  }

  @Override
  public void setValuesToFields(String dependent_fname, String dependent_lname, String dob,
                                String mobile_no, String gender) {

    et_dep_fnm.setText(dependent_fname);
    et_dep_lnm.setText(dependent_lname);
    et_dep_dob.setText(dob);
    et_dep_mobno.setText(mobile_no);

    if ("Male".equals(gender)) {
      radioM.setChecked(true);
    } else {
      radioF.setChecked(true);
    }
    String drawableName = "flag_"
            + mSessionManager.getDependentCountryCodeSymbol().trim().toLowerCase(Locale.ENGLISH);
    ivCountryFlag.setImageResource(getResId(drawableName));
  }

  /**
   * handling onclick event for different action
   *
   * @param v the view which is clicked
   */
  @OnClick({R.id.ivDependentPic, R.id.etDepDob, R.id.ivDepCalendr, R.id.countryCode,
      R.id.etRelation, R.id.tvSaveProfile})
  public void onClick(View v) {
    // TODO Auto-generated method stub
    switch (v.getId()) {
      case R.id.ivDependentPic:
        checkPermission();
        break;
      case R.id.etDepDob:
      case R.id.ivDepCalendr:
        mAddDependentPresenter.openCalendar(this);
        Utility.hideKeyboard(AddDependent.this);
        mAddDependentPresenter.validatePhoneNumber(AddDependent.this, tvCountryCode.getText().toString(),
            et_dep_mobno.getText().toString());
        break;

      case R.id.countryCode:
        mCountryPicker.show(getSupportFragmentManager(),
            getResources().getString(R.string.Countrypicker));
        break;
      case R.id.etRelation:
                /*sessionManager.setDependentFname(et_dep_fnm.getText().toString());
                sessionManager.setDependentLname(et_dep_lnm.getText().toString());
                sessionManager.setDateOBirth(et_dep_dob.getText().toString());
                sessionManager.setMobileNo(et_dep_mobno.getText().toString());*/

        int selected = rgGender.getCheckedRadioButtonId();

        RadioButton radioSexBtn = findViewById(selected);

        String gender_string = radioSexBtn.getText().toString();

        mSessionManager.setDependentCountryCodeSymbol(countryCodeSymbol);
        mAddDependentPresenter.setValuesToSessionMgr(et_dep_fnm.getText().toString(),
            et_dep_lnm.getText().toString(),
            et_dep_dob.getText().toString(),
            et_dep_mobno.getText().toString(), gender_string);
        RELATION_TYPE = etRelation.getText().toString().trim();
        Intent intent = new Intent(AddDependent.this, EditRelationshipActivity.class);
        intent.putExtra("IS_RELATION", true);
        //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, EDITRELATIONSHIPREQUEST);
        break;

      case R.id.tvSaveProfile:
        if (Utility.isNetworkAvailable(this)) {
          String firstName = et_dep_fnm.getText().toString();
        String lastName = et_dep_lnm.getText().toString();

        String mobile_no = et_dep_mobno.getText().toString();

        String date_str = dateOB;

        String relationType = etRelation.getText().toString();

        image_url = mAddDependentPresenter.getDependentImageUrl();
        // Utility.hideKeyboard(AddDependent.this);
        mAddDependentPresenter.validatePhoneNumber(AddDependent.this, tvCountryCode.getText().toString(),
            et_dep_mobno.getText().toString());

        if (TextUtils.isEmpty(firstName)) {
          et_dep_fnm.requestFocus();
          return;
        }

        if (TextUtils.isEmpty(mobile_no)) {
          et_dep_mobno.requestFocus();
          return;
        }


        if (isValid) {
          et_dep_mobno.requestFocus();
          return;
        }


        if (TextUtils.isEmpty(date_str)) {
          Toast.makeText(this, this.getResources().getString(R.string.dob_dependent_mandatory),
              Toast.LENGTH_SHORT).show();
          mAddDependentPresenter.openCalendar(this);
          return;
        }
        if (TextUtils.isEmpty(relationType)) {
          intent = new Intent(AddDependent.this, EditRelationshipActivity.class);
          intent.putExtra("IS_RELATION", true);
          //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
          startActivityForResult(intent, EDITRELATIONSHIPREQUEST);
          return;
        }

        int selectedId = rgGender.getCheckedRadioButtonId();

        RadioButton radioSexButton = findViewById(selectedId);

        String gender_str = radioSexButton.getText().toString();
        String gender_val = "1";
        if (getResources().getString(R.string.male).equals(gender_str)) {
          gender_val = "1";
        }else if (getResources().getString(R.string.female).equals(gender_str)) {
            gender_val = "2";
          }  else {
          gender_val = "3";
        }

        DependentsAddPojo dependentsAddPojo = new DependentsAddPojo();
        dependentsAddPojo.setFirstName(firstName);
        dependentsAddPojo.setLastName(lastName);

       /* if (!TextUtils.isEmpty(image_url)) {
          dependentsAddPojo.setProfilePic(image_url);
        } else {
          dependentsAddPojo.setProfilePic("");
        }*/

                /*if (!TextUtils.isEmpty(mAddDependentPresenter.getDependentImageUrl()))
                    dependentsAddPojo.setProfilePic(mAddDependentPresenter.getDependentImageUrl());*/
        dependentsAddPojo.setGender(gender_val);
        dependentsAddPojo.setCountryCode(tvCountryCode.getText().toString());
        dependentsAddPojo.setCountryCodeSymbol(countryCodeSymbol);
        dependentsAddPojo.setPhone(mobile_no);
        dependentsAddPojo.setDateOfBirth(date_str);
        dependentsAddPojo.setRelationship(relationType);
        //Special Requirements add Ids
        if (!selectedIds.isEmpty()) {
          dependentsAddPojo.setSpecialRequirement(selectedIds);
        }


        if (TextUtils.isEmpty(image_url)) {
          dependentsAddPojo.setProfilePic("");
        } else {
          dependentsAddPojo.setProfilePic(image_url);
        }
        if (mDependentsData != null) {
          if(Utility.isNetworkAvailable(this))
          mAddDependentPresenter.editDependent(mDependentsData.getDependentId(),
              dependentsAddPojo.getFirstName(), dependentsAddPojo.getLastName(),
              dependentsAddPojo.getProfilePic(),
              dependentsAddPojo.getGender(), dependentsAddPojo.getRelationship(),
                  countryCodeSymbol, dependentsAddPojo.getCountryCode(),
              dependentsAddPojo.getPhone(), dependentsAddPojo.getDateOfBirth(), selectedIds,
              mDependentsData.getSpecialInstruction(), mSessionManager, this, mAlertProgress);
        } else {
          if(Utility.isNetworkAvailable(this))
          mAddDependentPresenter.addDependent(this, dependentsAddPojo, mSessionManager, mAlertProgress);
        }
        } else {
          alertProgress.tryAgain(this, getString(R.string.pleaseCheckInternet), getString(R.string.system_error), isClicked -> {
          });
        }
        break;
    }
  }


  @OnTextChanged(value = R.id.et_dep_fnm,
      callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
  void onFnameTextChange(Editable editable) {
    int fname_str_len = editable.toString().length();

    if (fname_str_len <= 0) {
      til_dep_fnm.setError(getString(R.string.first_name_mandatory));
      focusVieW(til_dep_fnm);
      // isValid = true;
    }
    if (fname_str_len > 0) {
      focusviewNull();
      errorFocusNull(til_dep_fnm);
      // isValid = false;
    }
  }

  @OnTextChanged(value = R.id.et_dep_mobno,
      callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
  void onMobileTextChange(Editable editable) {
    int fname_str_len = editable.toString().length();
    if (fname_str_len <= 0) {
      et_dep_mobno.setError(getString(R.string.first_name_mandatory));
      // focusVieW(til_dep_fnm);
    }
    if (fname_str_len > 0) {
      focusviewNull();
      // errorFocusNull(til_dep_fnm);
    }
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    if (mHandlePictureEvents.newFile != null) {
      outState.putString(CAMERAIMAGE, mHandlePictureEvents.newFile.getPath());
    }
    super.onSaveInstanceState(outState);
  }

  @Override
  protected void onRestoreInstanceState(Bundle savedInstanceState) {
    if (savedInstanceState != null) {
      if (savedInstanceState.containsKey(CAMERAIMAGE)) {
        Uri uri = Uri.parse(savedInstanceState.getString(CAMERAIMAGE));
        mHandlePictureEvents.newFile = new File(uri.getPath());
      }
    }
    super.onRestoreInstanceState(savedInstanceState);
  }

  /**
   * <h2>checkPermission</h2>
   * <p>checking for the permission for camera and file storage at run time for
   * build version more than 22
   */
  private void checkPermission() {
    if (Build.VERSION.SDK_INT >= 23) {
      ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList =
          new ArrayList<>();
      myPermissionConstantsArrayList.clear();
      myPermissionConstantsArrayList.add(
          AppPermissionsRunTime.MyPermissionConstants.PERMISSION_CAMERA);
      myPermissionConstantsArrayList.add(
          AppPermissionsRunTime.MyPermissionConstants.PERMISSION_WRITE_EXTERNAL_STORAGE);
      myPermissionConstantsArrayList.add(
          AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_EXTERNAL_STORAGE);

      if (AppPermissionsRunTime.checkPermission(this, myPermissionConstantsArrayList,
          Constants.REQUEST_CODE_DEPENDENT_IMAGE)) {
        selectImage();
      }

    } else {
      selectImage();
    }

  }

  /**
   * <h2>checkContactPermission</h2>
   * <p>checking for the permission for read contacts for
   * build version more than 22
   */
  private void checkContactPermission() {
    if (Build.VERSION.SDK_INT >= 23) {
      ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList =
          new ArrayList<>();
      myPermissionConstantsArrayList.clear();
      myPermissionConstantsArrayList.add(
          AppPermissionsRunTime.MyPermissionConstants.PERMISSSION_READ_CONTACTS);

      if (AppPermissionsRunTime.checkPermission(this, myPermissionConstantsArrayList,
          Constants.REQUEST_CODE_CONTACTS)) {
        openContacts();
      }

    } else {
      openContacts();
    }
  }

  /**
   * <h2>selectImage</h2>
   *
   * @see HandlePictureEvents
   * This mehtod is used to show the popup where we can select our images.
   */

  private void selectImage() {
    mHandlePictureEvents.openDialog();
  }

  private void openContacts() {
    Intent contactsIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
    //contactsIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    startActivityForResult(contactsIntent, Constants.PICK_CONTACT);
  }

  @Override
  protected void onPause() {
    super.onPause();
  }

  @Override
  protected void onResume() {
    super.onResume();
  }


  /**
   * predefined method to check run time permissions list call back
   *
   * @param requestCode   request code
   * @param permissions:  contains the list of requested permissions
   * @param grantResults: contains granted and un granted permissions result list
   */
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
    boolean isDenine = false;
    switch (requestCode) {
      case Constants.REQUEST_CODE_DEPENDENT_IMAGE:
        for (int grantResult : grantResults) {
          if (grantResult != PackageManager.PERMISSION_GRANTED) {
            isDenine = true;
          }
        }
        if (isDenine) {
          mAlertProgress.alertinfo(getApplicationContext(),
              getResources().getString(R.string.permissiondenied) + " "
                  + Constants.REQUEST_CODE_DEPENDENT_IMAGE);
        } else {
          selectImage();
        }
        break;

      case Constants.REQUEST_CODE_CONTACTS:
        for (int grantResult : grantResults) {
          if (grantResult != PackageManager.PERMISSION_GRANTED) {
            isDenine = true;
          }
        }
        if (isDenine) {
          mAlertProgress.alertinfo(getApplicationContext(),
              getResources().getString(R.string.permissiondenied) + " "
                  + Constants.REQUEST_CODE_CONTACTS);
        } else {
          openContacts();
        }
        break;

      default:
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        break;
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    switch (requestCode) {
     /* case ConstantsInteface.ADD_DEPENDENT_CODE:
        Intent intent = new Intent(this, ConfirmBookActivity.class);
        intent.putExtra("DEP_ADD", true);
        setResult(RESULT_OK);
        startActivityForResult(intent, ConstantsInteface.ADD_DEPENDENT_CODE);
        finish();
        break;*/
      case Constants.CAMERA_PIC:
        mHandlePictureEvents.startCropImage(mHandlePictureEvents.newFile);
        break;
      case Constants.GALLERY_PIC:
        if (data != null) {
          mHandlePictureEvents.gallery(data.getData());
        }
        break;
      case Constants.PICK_CONTACT:
        Uri contactData = data.getData();
        Cursor c = getContentResolver().query(contactData, null, null, null, null);
        if (c.moveToFirst()) {
          String id =
              c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

          String hasPhone =
              c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

          if (hasPhone.equalsIgnoreCase("1")) {
            Cursor phones = getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                null, null);
            phones.moveToFirst();
            String phn_no = phones.getString(phones.getColumnIndex("data1"));
            String name = c.getString(
                c.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.DISPLAY_NAME));
            String[] phone_no = phn_no.split(" ");
            StringBuilder phn = new StringBuilder();
            if (phone_no.length > 0) {
              for (int i = 1; i < phone_no.length; i++) {
                phn.append(phone_no[i]);
              }
            }


            String formatted_phn = PhoneNumberUtils.stripSeparators(phn_no);
            et_dep_mobno.setText(getMobileNumber(formatted_phn));

            String[] nameArr = name.split(" ");

            if (nameArr.length > 1) {
              et_dep_fnm.setText(nameArr[0] + "");
              et_dep_lnm.setText(nameArr[1] + "");
            } else {
              et_dep_fnm.setText(name);
            }

          }
        }

        c.close();
        break;

      case Constants.CROP_IMAGE:
        if (data != null) {
          String path = data.getStringExtra(CropImage.IMAGE_PATH);
          if (path != null) {
            try {
              File fileExist = new File(path);
              double[] size = Scaler.getScalingFactor(this);
              double height = (140) * size[1];
              double width = (140) * size[0];
              Picasso.with(AddDependent.this).load(fileExist)
                  .resize((int) width, (int) height)
                  .centerCrop().transform(new PicassoCircleTransform())
                  .into(ivDependentPic);

              mHandlePictureEvents.uploadToAmazon(
                  this, Constants.AmazonDependentFolderName, new File(path),
                  new ImageUploadedAmazon() {
                    @Override
                    public void onSuccessAdded(String image) {
                      mAddDependentPresenter.setDependentImageUrl(image);
                      image_url = image;
                    }

                    @Override
                    public void onerror(String errormsg) {

                    }
                  });
            } catch (Exception e) {
              e.printStackTrace();
            }

          }
        }
        break;
      case EDITRELATIONSHIPREQUEST:
        if (data != null) {
          String relationship = data.getStringExtra("relationship");
          setRelationValue(relationship);
        }
        break;
    }
  }

  private String getMobileNumber(String contactNumber) {
    contactNumber = contactNumber.replaceAll("[^0-9]", "");
    if (contactNumber.length() > phoneNumLength) {
      contactNumber = contactNumber.substring(contactNumber.length() - phoneNumLength
      );
    }
    return contactNumber;
  }

  /**
   * this is to request the focus view
   *
   * @param til_reg_fnm is a TextInputLayout
   * @param error       Error Message
   */
  private void focusVieWRequestFocus(TextInputLayout til_reg_fnm, String error) {
    focusView = til_reg_fnm;
    focusView.requestFocus();
    til_reg_fnm.setErrorEnabled(true);
    til_reg_fnm.setError(error);
  }

  public void onValidationError(int errAt) {
        /*if (generatePdialog != null) {
            generatePdialog.dismiss();
        }*/

    switch (errAt) {
      case 1:
        focusVieWRequestFocus(til_dep_fnm, getString(R.string.first_name_mandatory));
        break;
            /*case 2:
                focusVieWRequestFocus(til_dep_dob,getString(R.string.email_mandatory));
                break;
            case 3:
                focusVieWRequestFocus(til_reg_paswrd,getString(R.string.password_mandatory));
                break;
            case 4:
                focusviewMobile(et_reg_mobno, getString(R.string.mobile_invalid));
                break;*/
      case 5:
        focusVieWRequestFocus(til_dep_dob, getString(R.string.dob_mandatory));
        break;
      default:
        break;
    }
  }

  @Override
  public void onSpecialRequirementsSelected
      (List<SpecialRequirementsData> specialRequirmentsList) {

  }

  @Override
  public void onRemove(String id) {
  }
}
