package com.vaidg.incalloutcalltelecall.dependent.add_dependent;


import android.app.AlertDialog;
import android.content.Context;
import com.vaidg.home.BasePresenter;
import com.vaidg.incalloutcalltelecall.dependent.model.DependentsAddPojo;
import com.vaidg.utilities.SessionManager;
import com.utility.AlertProgress;

/**
 * <h2>AddDependentPresenter</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public interface AddDependentPresenter extends BasePresenter {


  /**
   * <h2>addDependent</h2>
   * <p>This method is used to add Dependents.</p>
   *
   * @param context
   * @param dependentsAddPojo Dependents details to add for in the system.
   * @param sessionManager
   * @param alertProgress
   */
  void addDependent(Context context, DependentsAddPojo dependentsAddPojo,
      SessionManager sessionManager, AlertProgress alertProgress);


  /**
   * <h2>editDependent</h2>
   * <p>This method is used to edit Dependents.</p>
   *  @param dependentId        dependentId of the dependent to be changed
   * @param firstName          First Name of the dependent
   * @param lastName           LastName of the dependent
   * @param profilePic         profilePic of the dependent
   * @param gender             Gender
   * @param relationship       Relationship of the dependent
   * @param countryCode        CountryCode of the dependent
   * @param phone              mobile no of the dependent
   * @param dateOfBirth        Date of birth of the dependent
   * @param specialRequirement Special requirements of the dependent
   * @param specialInstruction Special instructions of the dependent
   * @param sessionManager
   * @param context
   * @param alertDialog
   */
  void editDependent(String dependentId,
      String firstName,
      String lastName,
      String profilePic,
      String gender,
      String relationship,
      String countryCodeSymbol,
      String countryCode,
      String phone,
      String dateOfBirth,
      String specialRequirement,
      String specialInstruction, SessionManager sessionManager, Context context,
      AlertProgress alertDialog);


  /**
   * <h2>getSpecialRequirements</h2>
   * <p> This method is used to call the get SpecialRequirements API to choose for a dependent.</p>
   */
  void getSpecialRequirements();


  /**
   * @author Pramod
   * @since 04-04-2018
   * @param value Value for Relationship
   * <h2>setRelationValue</h2>
   * This method is used to set the relationship of the dependent from the EditRelationShip screen
   */
  //void setRelationValue(String value);


  /**
   * <h2>validateFirstName</h2>
   * This method is used to validate the FirstName of the dependent.
   *
   * @param firstName FirstName of the dependent
   */
  boolean validateFirstName(String firstName);

  /**
   * <h2>validateFields</h2>
   * This method is used to validate the fields of a dependent
   *
   * @param firstName    First Name of the dependent
   * @param lastName     LastName of the dependent
   * @param phone        Phone of the dependent
   * @param max          Max digits of the phone number
   * @param dateOfBirth  Date of Birth of the dependent
   * @param gender       Gender of the dependent
   * @param relationShip Relationship of the dependent
   */
  boolean validateFields(String firstName, String lastName, String phone, int max,
      String dateOfBirth, String gender, String relationShip);

  /**
   * <h2>validatePhoneNumber</h2>
   * This method is used to validate the phone number from Google utils
   *
   * @param addDependent AddDependent of the activity
   * @param countryCode  countryCode of the dependent
   * @param mobileNumber mobileNumber of the dependent
   */
  void validatePhoneNumber(AddDependent addDependent, String countryCode, String mobileNumber);

  /**
   * <h2>openDate_Picker</h2>
   * Method is used to open a date picker and Pick a date.
   * <p>
   * This method open a Date Picker dialog by DatePickerDialog object.
   * Hen set the Max time as Current Date by the help of the {@code fromDatePickerDialog
   * .getDatePicker().setMaxDate(new
   * Date().getTime())}.
   * Also listening the response From the DatePicker and setting the Date to the Given Edit text.
   * </p>
   * @param context
   */
  void openCalendar(
      Context context);

  /**
   * <h2>checkDobValidity</h2>
   * This method is used to check the validation of the date of birth field.
   */
  boolean checkDobValidity();

  /**
   * <h2>getDependentImageUrl</h2>
   * This method is used to get the image url from sharedPreferences to show the image.
   */
  String getDependentImageUrl();

  /**
   * <h2>setDependentImageUrl</h2>
   * This method is used to set the image url to the sharedPreferences to show the image.
   *
   * @param url URL of the image uploaded to Amazon S3
   */
  void setDependentImageUrl(String url);

  /**
   * <h2>clearDependentValues</h2>
   * This method is used to clear the dependent from the list.
   */
  void clearDependentValues();

  /**
   * <h2>setValuesToViews</h2>
   * This method is used to set the value for dependent.
   */
  void setValuesToViews();

  /**
   * <h2>validateFields</h2>
   * This method is used to save data in sharepref
   *
   * @param dependent_fname First Name of the dependent
   * @param dependent_lname LastName of the dependent
   * @param mobile_no       Phone of the dependent
   * @param date_of_Birth   Date of Birth of the dependent
   * @param gender_str      Gender of the dependent
   */
  void setValuesToSessionMgr(String dependent_fname, String dependent_lname, String date_of_Birth,
      String mobile_no, String gender_str);


}
