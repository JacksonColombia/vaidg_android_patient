package com.vaidg.incalloutcalltelecall.dependent.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.dependent.model.SpecialRequirementsData;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.RoundedCornersTransformation;
import com.utility.PicassoTrustAll;

import java.util.List;


/**
 * <h2>SpecialReqChipsAdapter</h2>
 *
 * <p>This is an inner class of adapter type, to populate the specialchips types on screen.</p>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class SpecialReqChipsAdapter extends RecyclerView.Adapter {
  private static boolean[] check_flags;
  private Context mContext;
  private List<SpecialRequirementsData> requirements_list;

  public SpecialReqChipsAdapter(Context mcontext, List<SpecialRequirementsData> req) {
    this.mContext = mcontext;
    this.requirements_list = req;
    check_flags = new boolean[req.size()];
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_special_req, parent, false);
    return new ViewHoldr(view);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
    final ViewHoldr hldr = (ViewHoldr) holder;
    hldr.tvReqName.setText(requirements_list.get(position).getName());
if(requirements_list.get(position).getUnSelectedIcon() != null && !requirements_list.get(position).getUnSelectedIcon().isEmpty()) {
  PicassoTrustAll.getInstance(mContext)
      .load(requirements_list.get(position).getUnSelectedIcon())
      .into(hldr.ivReqImage);
}

  }

  @Override
  public int getItemCount() {
    return requirements_list.size();
  }

  private class ViewHoldr extends RecyclerView.ViewHolder {
    private TextView tvReqName;
    private ImageView ivReqImage;
    private ImageView ivSelectReq;
    private RelativeLayout reqRow;

    private ViewHoldr(View itemView) {
      super(itemView);
      tvReqName = itemView.findViewById(R.id.tv_special_req_title);
      ivReqImage = itemView.findViewById(R.id.iv_special_req_icon);
      ivSelectReq = itemView.findViewById(R.id.iv_select_req_icon);
      reqRow = itemView.findViewById(R.id.rl_special_req_layout);
      AppTypeface appTypeface = AppTypeface.getInstance(mContext);
      tvReqName.setTypeface(appTypeface.getHind_medium());

      reqRow.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if (check_flags[getAdapterPosition()]) {
            check_flags[getAdapterPosition()] = false;
            ivSelectReq.setSelected(false);
          } else {
            check_flags[getAdapterPosition()] = true;
            ivSelectReq.setSelected(true);
          }
        }
      });
    }
  }
}
