package com.vaidg.incalloutcalltelecall.dependent.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h2>EditRelationResponsePojo</h2>
 *
 * <p>Edit Relation Response POJO</p>
 *
 * @author 3Embed
 * @since 12-07-2019
 */
public class EditRelationResponsePojo implements Serializable {

  @SerializedName("data")
  @Expose
  private ArrayList<EditRelation> data;


  public ArrayList<EditRelation> getData() {
    return data;
  }

  public class EditRelation implements Serializable {

    @SerializedName("_id")
    @Expose
    public String _id;
    @SerializedName("name")
    @Expose
    public  String name;
    @SerializedName("selectionIcon")
    @Expose
    public String selectionIcon;
    @SerializedName("unSelectedIcon")
    @Expose
    public String unSelectedIcon;
    @SerializedName("num")
    @Expose
    public String num;

    public String get_id() {
      return _id;
    }

    public void set_id(String _id) {
      this._id = _id;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getSelectionIcon() {
      return selectionIcon;
    }

    public void setSelectionIcon(String selectionIcon) {
      this.selectionIcon = selectionIcon;
    }

    public String getUnSelectedIcon() {
      return unSelectedIcon;
    }

    public void setUnSelectedIcon(String unSelectedIcon) {
      this.unSelectedIcon = unSelectedIcon;
    }

    public String getNum() {
      return num;
    }

    public void setNum(String num) {
      this.num = num;
    }

  }
}
