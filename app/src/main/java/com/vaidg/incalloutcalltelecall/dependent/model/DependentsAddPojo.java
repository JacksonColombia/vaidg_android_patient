package com.vaidg.incalloutcalltelecall.dependent.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h2>DependentsAddPojo</h2>
 *
 * <p>Add/Edit Dependents Request DATA object </p>
 *
 * @author 3Embed
 * @since 12-07-2019
 */

public class DependentsAddPojo implements Serializable {

  @SerializedName("dependentId")
  @Expose
  private String dependentId;
  @SerializedName("firstName")
  @Expose
  private String firstName;
  @SerializedName("lastName")
  @Expose
  private String lastName;
  @SerializedName("profilePic")
  @Expose
  private String profilePic;
  @SerializedName("gender")
  @Expose
  private String gender;
  @SerializedName("relationship")
  @Expose
  private String relationship;
  @SerializedName("countryCode")
  @Expose
  private String countryCode;
  @SerializedName("countryCodeSymbol")
  @Expose
  private String countryCodeSymbol;
  @SerializedName("phone")
  @Expose
  private String phone;
  @SerializedName("dateOfBirth")
  @Expose
  private String dateOfBirth;
  @SerializedName("specialRequirement")
  @Expose
  private String specialRequirement;
  @SerializedName("specialInstruction")
  @Expose
  private String specialInstruction;

  public String getDependentId() {
    return dependentId;
  }

  public void setDependentId(String dependentId) {
    this.dependentId = dependentId;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getProfilePic() {
    return profilePic;
  }

  public void setProfilePic(String profilePic) {
    this.profilePic = profilePic;
  }

  public String getRelationship() {
    return relationship;
  }

  public void setRelationship(String relationship) {
    this.relationship = relationship;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getSpecialRequirement() {
    return specialRequirement;
  }

  public void setSpecialRequirement(String specialRequirement) {
    this.specialRequirement = specialRequirement;
  }

  public String getSpecialInstruction() {
    return specialInstruction;
  }

  public void setSpecialInstruction(String specialInstruction) {
    this.specialInstruction = specialInstruction;
  }

  public String getCountryCodeSymbol() {
    return countryCodeSymbol;
  }

  public void setCountryCodeSymbol(String countryCodeSymbol) {
    this.countryCodeSymbol = countryCodeSymbol;
  }
}
