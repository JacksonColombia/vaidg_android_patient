package com.vaidg.incalloutcalltelecall;


import com.vaidg.Dagger2.ActivityScoped;
import com.vaidg.Dagger2.FragmentScoped;
import com.vaidg.incalloutcalltelecall.addaddress.YourAddressFragment;
import com.vaidg.incalloutcalltelecall.addaddress.YourAddressFragmentPresenter;
import com.vaidg.incalloutcalltelecall.addaddress.YourAddressFragmentPresenterImpl;
import com.vaidg.incalloutcalltelecall.askquestions.AskQuestionsFragment;
import com.vaidg.incalloutcalltelecall.dependent.DependentContract;
import com.vaidg.incalloutcalltelecall.dependent.DependentFragment;
import com.vaidg.incalloutcalltelecall.dependent.DependentPresenterImpl;
import com.vaidg.incalloutcalltelecall.nwlaterrepeat.NowLaterRepeatTypeFragment;
import com.vaidg.incalloutcalltelecall.symotomquestion.SymptomQuestionFragment;
import com.vaidg.incalloutcalltelecall.symotomquestion.SymptomQuestionsContract;
import com.vaidg.incalloutcalltelecall.symotomquestion.SymptomQuestionsPresenterImp;
import com.vaidg.incalloutcalltelecall.symptom.SymptomContract;
import com.vaidg.incalloutcalltelecall.symptom.SymptomFragment;
import com.vaidg.incalloutcalltelecall.symptom.SymptomPresenterImp;
import com.vaidg.incalloutcalltelecall.symptom.SymptomSearchFilter;
import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;


/**
 * <h2>InCallOutCallTeleCallModule</h2>
 *
 * This is a Dagger module. We use this to pass in the View dependency to the
 * {@link InCallOutCallTeleCallContract.InCallOutCallTeleCallPresenter}.
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
@Module
public interface InCallOutCallTeleCallModule {
  @FragmentScoped
  @ContributesAndroidInjector
  AskQuestionsFragment providetAskQuestionsFragment();

  @FragmentScoped
  @ContributesAndroidInjector
  NowLaterRepeatTypeFragment provideNowLaterRepeatTypeFragment();

  @FragmentScoped
  @ContributesAndroidInjector
  DependentFragment providetDependentFragment();

  @FragmentScoped
  @ContributesAndroidInjector
  YourAddressFragment providetYourAddressFragment();

  @FragmentScoped
  @ContributesAndroidInjector
  SymptomFragment providetSymptomFragment();

  @FragmentScoped
  @ContributesAndroidInjector
  SymptomQuestionFragment providetSymptomQuestionFragment();

  @ActivityScoped
  @Binds
  InCallOutCallTeleCallContract.InCallOutCallTeleCallPresenter inCallOutCallTeleCallPresenter(
      InCallOutCallTeleCallPresenter inCallOutCallTeleCallPresenter);

  @ActivityScoped
  @Binds
  InCallOutCallTeleCallContract.InCallOutCallTeleCallView inCallOutCallTeleCallView(
      InCallOutCallTeleCallActivity inCallOutCallTeleCallActivity);


  /*
      @Binds
      @ActivityScoped
      YourAddressPresenter provideYourAddressPre(InCallOutCallTeleCallPresenter
      inCallOutCallTeleCallPresenter);
  */
  @Binds
  @ActivityScoped
  DependentContract.DependentPresenter provideDependentPresenter(
      DependentPresenterImpl dependentPresenter);


  @Binds
  @ActivityScoped
  YourAddressFragmentPresenter provideYourAddressPresenter(
      YourAddressFragmentPresenterImpl presenter);


  @Binds
  @ActivityScoped
  SymptomContract.SymptomPresenter provideSymptomPresenter(SymptomPresenterImp presenter);

  @Binds
  @ActivityScoped
  SymptomQuestionsContract.SymptomQuestionsPresenter provideSymptomQuestionsPresenter(
      SymptomQuestionsPresenterImp presenter);

}
