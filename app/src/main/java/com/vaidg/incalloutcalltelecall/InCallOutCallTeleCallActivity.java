package com.vaidg.incalloutcalltelecall;

import static com.vaidg.BuildConfig.AMAZON_BASE_URL;
import static com.vaidg.utilities.Constants.AmazonUploadJobImage;
import static com.vaidg.utilities.Constants.CAMERA;
import static com.vaidg.utilities.Constants.CAMERA_PIC;
import static com.vaidg.utilities.Constants.COUNT;
import static com.vaidg.utilities.Constants.CROP_IMAGE;
import static com.vaidg.utilities.Constants.GALLERY;
import static com.vaidg.utilities.Constants.GALLERY_PIC;
import static com.vaidg.utilities.Constants.POS;
import static com.vaidg.utilities.Constants.POSCOUNT;
import static com.vaidg.utilities.Constants.REQ_CODE_FOR_CHCKBOX;
import static com.vaidg.utilities.Constants.REQ_CODE_FOR_DATE_CURRENT_TO_FUTURE;
import static com.vaidg.utilities.Constants.REQ_CODE_FOR_DATE_PAST_TO_CURRENT;
import static com.vaidg.utilities.Constants.REQ_CODE_FOR_DROPDOWN;
import static com.vaidg.utilities.Constants.REQ_CODE_FOR_NUMBERSLIDER;
import static com.vaidg.utilities.Constants.REQ_CODE_FOR_TEXT_AREA_COMMASEPARATED;
import static com.vaidg.utilities.Constants.REQ_CODE_FOR_TIME;
import static com.vaidg.utilities.Constants.W_320;
import static com.vaidg.utilities.Constants.W_40;
import static com.vaidg.utilities.Constants.bookingTypeAction;
import static com.vaidg.utilities.Constants.bookingTypeNowSchedule;
import static com.vaidg.utilities.Constants.callType;
import static com.vaidg.utilities.Constants.callTypeInOutTele;
import static com.vaidg.utilities.Constants.catName;
import static com.vaidg.utilities.Constants.isAddressSelect;
import static com.vaidg.utilities.Constants.isFromChckBox;
import static com.vaidg.utilities.Constants.isFromDatePicker;
import static com.vaidg.utilities.Constants.isFromDropDown;
import static com.vaidg.utilities.Constants.isFromImageUpload;
import static com.vaidg.utilities.Constants.isFromNumberPicker;
import static com.vaidg.utilities.Constants.isFromTextAreaCommaSeparated;
import static com.vaidg.utilities.Constants.isFromTime;
import static com.vaidg.utilities.Constants.isFromVideoUpload;
import static com.vaidg.utilities.Constants.isMax;
import static com.vaidg.utilities.Constants.isMin;
import static com.vaidg.utilities.Constants.isQuestionHeader;
import static com.vaidg.utilities.Constants.isQuestionTitle;
import static com.vaidg.utilities.Constants.jsonArray;
import static com.vaidg.utilities.Constants.proId;
import static com.vaidg.utilities.Constants.questionArrs;
import static com.vaidg.utilities.Constants.responseData;
import static com.vaidg.utilities.Constants.searchSymptomSelectedID;
import static com.vaidg.utilities.Constants.symptom;
import static com.vaidg.utilities.Constants.symptomId;
import static com.vaidg.utilities.Utility.isNetworkAvailable;

import adapters.ImageTypeAdapterGrid;
import adapters.VideoTypeAdapterGrid;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.vaidg.Login.LoginActivity;
import com.vaidg.R;
import com.vaidg.bookingtype.DialogTimeFragment;
import com.vaidg.confirmbookactivity.ConfirmBookActivity;
import com.vaidg.inCallOutCall.TimeSlots;
import com.vaidg.incalloutcalltelecall.addaddress.YourAddressFragment;
import com.vaidg.incalloutcalltelecall.askquestions.AskQuestionsFragment;
import com.vaidg.incalloutcalltelecall.dependent.DependentFragment;
import com.vaidg.incalloutcalltelecall.dependent.IsSelected;
import com.vaidg.incalloutcalltelecall.incalloutcalltellcall.InCallOutCallFragment;
import com.vaidg.incalloutcalltelecall.nwlaterrepeat.NowLaterRepeatTypeFragment;
import com.vaidg.incalloutcalltelecall.questionselection.QuestionSelectionActivity;
import com.vaidg.incalloutcalltelecall.questionselection.model.SymptomResponsePojo;
import com.vaidg.incalloutcalltelecall.symotomquestion.AnsSymptomHashMap;
import com.vaidg.incalloutcalltelecall.symotomquestion.SymptomAnswer;
import com.vaidg.incalloutcalltelecall.symotomquestion.SymptomQuestionFragment;
import com.vaidg.incalloutcalltelecall.symptom.SymptomFragment;
import com.vaidg.model.CallType;
import com.vaidg.model.Category;
import com.vaidg.providerList.DoctorList;
import com.vaidg.providerdetails.ProviderDetails;
import com.vaidg.utilities.AppPermissionsRunTime;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.HandlePictureEvents;
import com.vaidg.utilities.ImageUploadedAmazon;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.pojo.QuestionImage;
import com.pojo.QuestionVideo;
import com.utility.AlertProgress;
import com.utility.PermissionsListener;
import com.utility.PicassoTrustAll;

import dagger.android.support.DaggerAppCompatActivity;
import eu.janmuller.android.simplecropimage.CropImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

/**
 * /**
 * <h2>InCallOutCallTeleCallActivity</h2>
 * <p>
 * This class is used to provide the Question flow, where a customer
 * can book an appointment for doctor.
 * </p>
 *
 * @author 3Embed
 * @since 29-11-2019.
 */
public class InCallOutCallTeleCallActivity extends DaggerAppCompatActivity implements
        InCallOutCallTeleCallContract.InCallOutCallTeleCallView,
        NowLaterRepeatTypeFragment.OnFragmentInteractionListener,
        SymptomQuestionFragment.OnSypmtomPickedListenerCallBack,
        DialogTimeFragment.OnFragmentInteractionListener,
        SymptomFragment.OnSymptomSelectedCallBack, IsSelected, PermissionsListener {


    private final int SEARCH_RESULT = 101;
    private final int REQUEST_CODE_PERMISSION_VIDEO = 1;
    private final int REQUEST_CODE_PERMISSION_IMAGE = 0;
    @Inject
    InCallOutCallTeleCallContract.InCallOutCallTeleCallPresenter presenter;
    @Inject
    SessionManagerImpl manager;
    @BindView(R.id.tv_center)
    TextView tv_center;
    @BindView(R.id.tv_centerCount)
    TextView tv_centerCount;
    @BindView(R.id.toolbar)
    Toolbar toolBookingType;
    int startQuestion = 0;
    @Inject
    AlertProgress alertProgress;
    @Inject
    AppTypeface appTypeface;
    @BindView(R.id.tv_skip)
    TextView tv_skip;
    @BindString(R.string.next)
    String next;
    @BindString(R.string.call_a_doctor_home)
    String call_a_doctor_home;
    int pos = 0;
    // create list one and store values
    List<String> valSetOne = new ArrayList<String>();
    @BindView(R.id.progressQuestion)
    ProgressBar progressQuestion;
    @BindView(R.id.tvBookTypeSave)
    Button tvBookTypeSave;
    @BindString(R.string.getStarted)
    String getStarted;
    @BindView(R.id.ivMyEventProPic)
    ImageView ivMyEventProPic;
    @BindView(R.id.tvMyEventProName)
    TextView tvMyEventProName;
    @BindView(R.id.llDocFavDet)
    LinearLayout llDocFavDet;
    @BindView(R.id.tvMyEvnetProStatus)
    TextView tvMyEvnetProStatus;
   @BindView(R.id.tvViewProfile)
    TextView tvViewProfile;
    TextView tvSelectChckBox, tvSelectDateCurrentToFuture, tvSelectDatePastToCurrent, tvSelectTime,
            tvSelectTextAreaCommaSeparated, tvDropDown, tvNumberSlider;
    InCallOutCallTeleCallContract.InCallOutCallTeleCallDateTIme biddingDateTIme;
    Map<String, SymptomAnswer> mStringAnsHashMapHashMap = new HashMap<String, SymptomAnswer>();
    Map<String, Map<String, SymptomAnswer>> stringMapMap = new HashMap<String, Map<String, SymptomAnswer>>();
    private ArrayList<AnsSymptomHashMap> ansSymptomHashMaps = new ArrayList<>();
    private ArrayList<SymptomAnswer> ansHashMaps = new ArrayList<>();
    private LatLng latLng = null;
    private boolean flag_guest_login = false;
    private ArrayList<QuestionImage> questionImages = new ArrayList<>();
    private ArrayList<QuestionVideo> questionVideo = new ArrayList<>();
    private HashMap<Integer,String>imageArray = new HashMap<>();
    private ImageTypeAdapterGrid imageTypeAdapterGrid;
    private VideoTypeAdapterGrid videoTypeAdapterGrid;
    private RecyclerView recyclerViewQuestions;
    private String symptomSelectedID = "";
    private String name = "";
    private int questionType = 1;
    private boolean isSelect = false;
    private boolean isSymptomSelectedID = false;
    private int adapterPosition = 0;
    private SymptomResponsePojo.SymptomCategory.QuestionArr mQuestionArr;
    private String questionId = "";
    private boolean inCall = false;
    private boolean outCall = false;
    private boolean teleCall = false;
    private Boolean isFavDoc = false;
    private String docImage = "", docName = "", docExpertise = "";
    private HandlePictureEvents handlePicEvent;
    private int imagePositionTaken = 0;
    private int videoPositionTaken = 0;
    private Context mContext;

    /**
     * This is the onCreate LoginActivity method that is called firstly, when user came to Login
     * screen.
     *
     * @param savedInstanceState contains an instance of Bundle.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_call_out_call_tele_call);
        ButterKnife.bind(this);
        mContext = this;
        bookingTypeNowSchedule = 0;
        //getIntentValue();
        isSymptomSelectedID = false;
        COUNT = -1;
        POS = -1;
        calculate();
        initialize();
    }

    private void calculate() {
        int w22 = Utility.getScreenWidth() * W_40 / W_320;
        tvBookTypeSave.getLayoutParams().height = w22;
    }


    /**
     * <h2>initialize</h2>
     * <p> method to initialize the views</p>
     */
    private void initialize() {
        Bundle bundle;
        bundle = getIntent().getExtras();
        if (bundle != null) {
            callType = (CallType) bundle.getSerializable("CallType");

            bookingTypeAction = (Category.BookingTypeAction) bundle.getSerializable(
                        "BookingTypeAction");
            inCall = bundle.getBoolean(
                    "inCall", false);
            outCall = bundle.getBoolean(
                    "outCall", false);
            teleCall = bundle.getBoolean(
                    "teleCall", false);
            isFavDoc = getIntent().getBooleanExtra("isFavDoc", false);
            if (isFavDoc) {
                docImage = bundle.getString("ImagePro");
                docName = bundle.getString("NAME");
                proId = bundle.getString("ProviderId");
                docExpertise = bundle.getString("expertise");
                if(docImage != null && !docImage.isEmpty()) {
                    PicassoTrustAll.getInstance(this)
                        .load(docImage)
                        .placeholder(R.drawable.register_profile_default_image)   // optional
                        .error(R.drawable.register_profile_default_image)// optional
                        .transform(new PicassoCircleTransform())
                        .into(ivMyEventProPic);
                }
                tvMyEventProName.setText(docName);
                tvMyEvnetProStatus.setText(docExpertise);
            }
        }
        toolBarSetting();
        if (startQuestion == POSCOUNT) {
            tv_skip.setClickable(false);
        }
        if (isFavDoc) {
            if (manager.getGuestLogin()) {
                Intent intent = new Intent(this, LoginActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                llDocFavDet.setVisibility(View.VISIBLE);
                tvMyEventProName.setTypeface(appTypeface.getHind_medium());
                tvViewProfile.setTypeface(appTypeface.getHind_medium());
                tvMyEvnetProStatus.setTypeface(appTypeface.getHind_regular());
                tvMyEventProName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_16));
                tvMyEvnetProStatus.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_13));
                tvViewProfile.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_14));
                tvViewProfile.setOnClickListener(v -> {
                    Intent intent = new Intent(this, ProviderDetails.class);
                    intent.putExtra("ProviderId",proId);
                    intent.putExtra("isProFileView",true);
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                });
                saveButtonVisibility();
                saveNextQuestionClick();
            }
        } else {
            llDocFavDet.setVisibility(View.GONE);
            Utility.showFragment(new AskQuestionsFragment(), "AskQuestionsFragment",
                    getSupportFragmentManager());

            tv_center.setTypeface(appTypeface.getHind_semiBold());
            tv_center.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    getResources().getDimension(R.dimen.dp_15));
            tv_centerCount.setTypeface(appTypeface.getHind_medium());
            tv_center.setText(catName);
            tv_centerCount.setVisibility(View.GONE);
            tv_skip.setVisibility(View.GONE);
            tvBookTypeSave.setTypeface(appTypeface.getHind_medium());
            tvBookTypeSave.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_18));
            tvBookTypeSave.setText(getStarted);
        }
    }

    private void saveNextQuestionClick() {

        Intent intent;
        if (flag_guest_login) {
            intent = new Intent(this, LoginActivity.class);
            //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else {
            if (callTypeInOutTele == 1 && inCall) {
                methodForIncall();
            } else if (callTypeInOutTele == 2 && outCall) {
                methodForOutcallType();
            } else if (callTypeInOutTele == 3 && teleCall) {
                methodForIncall();
            } else if (callTypeInOutTele == 2) {
                methodForOutCall();
            } else {
                methodForTeleCallIncall();
            }
            setProgressCount();
            Log.d("ABC", "saveNextQuestionClick: " + startQuestion);
        }
    }

    private void methodForOutcallType() {
        Intent intent;
        switch (startQuestion) {
            case 0:
                startQuestion++;
                Utility.showFragment(new NowLaterRepeatTypeFragment(), "NowLaterRepeatTypeFragment",
                        getSupportFragmentManager());
                break;
            case 1:
                startQuestion++;
                Utility.showFragment(new YourAddressFragment(), "YourAddressFragment",
                        getSupportFragmentManager());
                break;
            case 2:
                if(isAddressSelect) {
                    startQuestion++;
                    Utility.showFragment(new DependentFragment(), "DependentFragment",
                            getSupportFragmentManager());
                } else {
                    alertProgress.alertinfo(this, "Please make a selection to proceed further");
                }
                break;
            case 3:
                if (isFavDoc)
                    llDocFavDet.setVisibility(View.GONE);
                if (isSelect) {
                    startQuestion++;
                    Utility.showFragment(new SymptomFragment(), "SymptomFragment",
                            getSupportFragmentManager());
                } else {
                    alertProgress.alertinfo(this, "Please make a selection to proceed further");
                }
                break;
            default:
                if (COUNT > 0) {
                    if (startQuestion < responseData.size() + 4) {
                        if (POS < COUNT - 1) {
                            startQuestion++;
                            POS++;
                            saveSelectedData();
                            Log.i("before", "" + POS);
                            //POS++;
                            Log.i("after", "" + POS);
                            Utility.showFragment(new SymptomQuestionFragment(),
                                    SymptomQuestionFragment.class.getName(),
                                    getSupportFragmentManager());
                        }
                    } else {
                        redirectTodoctorList();
                    }
                }
                break;
        }
    }

    private void methodForIncall() {
        Intent intent;
        switch (startQuestion) {
     /* case 0:
        startQuestion++;
        Utility.showFragment(new InCallOutCallFragment(), "InCallOutCallFragment",
                getSupportFragmentManager());
        break;*/
            case 0:
                startQuestion++;
                Utility.showFragment(new NowLaterRepeatTypeFragment(), "NowLaterRepeatTypeFragment",
                        getSupportFragmentManager());
                break;
            case 1:
                startQuestion++;
                Utility.showFragment(new DependentFragment(), "DependentFragment",
                        getSupportFragmentManager());
                break;
            case 2:
                if (isFavDoc)
                    llDocFavDet.setVisibility(View.GONE);
                if (isSelect) {
                    startQuestion++;
                    Utility.showFragment(new SymptomFragment(), "SymptomFragment",
                            getSupportFragmentManager());
                } else {
                    alertProgress.alertinfo(this, "Please make a selection to proceed further");
                }
                break;
            default:
                if (COUNT > 0) {
                    if (startQuestion < responseData.size() + 3) {
                        if (POS < COUNT - 1) {
                            startQuestion++;
                            POS++;
                            saveSelectedData();
                            Log.i("before", "" + POS);
                            //POS++;
                            Log.i("after", "" + POS);
                            Utility.showFragment(new SymptomQuestionFragment(),
                                    SymptomQuestionFragment.class.getName(),
                                    getSupportFragmentManager());
                        }
                    } else {

                        redirectTodoctorList();
                    }
                }
                break;
        }

    }

    private void methodForTeleCallIncall() {
        Intent intent;
        switch (startQuestion) {
            case 0:
                startQuestion++;
                Utility.showFragment(new InCallOutCallFragment(), "InCallOutCallFragment",
                        getSupportFragmentManager());
                break;
            case 1:
                startQuestion++;
                Utility.showFragment(new NowLaterRepeatTypeFragment(), "NowLaterRepeatTypeFragment",
                        getSupportFragmentManager());
                break;
            case 2:
                startQuestion++;
                Utility.showFragment(new DependentFragment(), "DependentFragment",
                        getSupportFragmentManager());
                break;
            case 3:
                if (isFavDoc)
                    llDocFavDet.setVisibility(View.GONE);
                if (isSelect) {
                    startQuestion++;
                    Utility.showFragment(new SymptomFragment(), "SymptomFragment",
                            getSupportFragmentManager());
                } else {
                    alertProgress.alertinfo(this, "Please make a selection to proceed further");
                }
                break;
            default:
                if (COUNT > 0) {
                    if (startQuestion < responseData.size() + 4) {
                        if (POS < COUNT - 1) {
                            startQuestion++;
                            POS++;
                            saveSelectedData();
                            Utility.showFragment(new SymptomQuestionFragment(),
                                    SymptomQuestionFragment.class.getName(),
                                    getSupportFragmentManager());
                            Log.i("before", "" + POS);
                            //POS++;
                            Log.i("after", "" + POS);
                        }
                    } else {
                        redirectTodoctorList();
                    }
                }
                break;
        }

    }

    private void saveSelectedData() {

   /* if (mStringAnsHashMapHashMap != null && mStringAnsHashMapHashMap.size() > 0) {
      stringMapMap.put(symptom,mStringAnsHashMapHashMap);
    }*/
        if (ansHashMaps.size() > 0) {
            ansSymptomHashMaps.add(new AnsSymptomHashMap(Constants.symptomId, Constants.symptom, ansHashMaps));
            if (POS <= COUNT)
                ansHashMaps = new ArrayList<>();
        }
        Log.w("DEBUG", "saveNextQuestionClick: " + ansSymptomHashMaps.size());
    }


    private void methodForOutCall() {
        Intent intent;
        switch (startQuestion) {
            case 0:
                startQuestion++;
                Utility.showFragment(new InCallOutCallFragment(), "InCallOutCallFragment",
                        getSupportFragmentManager());
                break;
            case 1:
                startQuestion++;
                Utility.showFragment(new NowLaterRepeatTypeFragment(), "NowLaterRepeatTypeFragment",
                        getSupportFragmentManager());
                break;
            case 2:
                startQuestion++;
                Utility.showFragment(new YourAddressFragment(), "YourAddressFragment",
                        getSupportFragmentManager());
                break;
            case 3:
                if(isAddressSelect){
                startQuestion++;
                Utility.showFragment(new DependentFragment(), "DependentFragment",
                        getSupportFragmentManager());
        } else {
            alertProgress.alertinfo(this, "Please make a selection to proceed further");
        }
        break;
            case 4:
                if (isFavDoc)
                    llDocFavDet.setVisibility(View.GONE);
                if (isSelect) {
                    startQuestion++;
                    Utility.showFragment(new SymptomFragment(), "SymptomFragment",
                            getSupportFragmentManager());
                } else {
                    alertProgress.alertinfo(this, "Please make a selection to proceed further");
                }
                break;
            default:
                if (COUNT > 0) {
                    if (startQuestion < responseData.size() + 5) {
                        if (POS < COUNT - 1) {
                            startQuestion++;
                            POS++;
                            saveSelectedData();
                            Log.i("before", "" + POS);
                            //POS++;
                            Log.i("after", "" + POS);
                            Utility.showFragment(new SymptomQuestionFragment(),
                                    SymptomQuestionFragment.class.getName(),
                                    getSupportFragmentManager());
                        }
                    } else {
                        redirectTodoctorList();
                    }
                }
                break;
        }

    }

    @Override
    public void onBackPressed() {
        //  super.onBackPressed();
        flag_guest_login = manager.getGuestLogin();
        Utility.printLog("getBackStackEntryCount", "This is me getBackStackEntryCount  " + getSupportFragmentManager().getBackStackEntryCount());
        startQuestion--;
        setProgressCount();
        Fragment f = this.getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (f instanceof AskQuestionsFragment) {
            Utility.printLog("AskQuestionsFragment", "This is me AskQuestionsFragment");
            finish();
            overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
        } else if (f instanceof InCallOutCallFragment) {
            Utility.printLog("InCallOutCallFragment", "This is me InCallOutCallFragment");
            if (isFavDoc) {
                finish();
                overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
            } else {
                showGetStartedQuestion();
            }
        } else if (f instanceof NowLaterRepeatTypeFragment) {
            // do something with f
            Utility.printLog("NowLaterRepeatTypeFragment", "This is me NowLaterRepeatTypeFragment");
            if (outCall || inCall || teleCall) {
                showGetStartedQuestion();
            } else {
                super.onBackPressed();
            }
        } else if (f instanceof YourAddressFragment) {
            // do something with f
            Utility.printLog("YourAddressFragment", "This is me YourAddressFragment");
            isAddressSelect = false;
            super.onBackPressed();
        } else if (f instanceof DependentFragment) {
            Constants.dependentId = "";
            Constants.dependentName = "";
            Constants.dependentRelation = "";
            Constants.dependentImageUrl = "";
            // do something with f
            Utility.printLog("DependentFragment", "This is me DependentFragment");

                super.onBackPressed();
        } else if (f instanceof SymptomFragment) {
            // do something with f
            Utility.printLog("SymptomFragment", "This is me SymptomFragment");
            if (isFavDoc) {
                llDocFavDet.setVisibility(View.VISIBLE);
            }
            super.onBackPressed();
        } else if (POS >= 0) {
            POS--;

            if (POS >= 0) {
                super.onBackPressed();
            } else {
                POS = -1;
                COUNT = -1;
       /* mStringAnsHashMapHashMap.clear();
        stringMapMap.clear();*/
                ansSymptomHashMaps.clear();
                ansHashMaps.clear();
                super.onBackPressed();
            }
        } else {
            finish();
            overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
        }
    }

    private void showGetStartedQuestion() {
        startQuestion = 0;
        COUNT = -1;
        POS = -1;
        responseData.clear();
        isSelect = false;
        isSymptomSelectedID = false;
        tv_center.setTypeface(appTypeface.getHind_semiBold());
        tv_center.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimension(R.dimen.dp_15));
        tv_centerCount.setTypeface(appTypeface.getHind_medium());
        tv_center.setText(catName);
        tv_centerCount.setVisibility(View.GONE);
        tv_skip.setVisibility(View.GONE);
        tvBookTypeSave.setTypeface(appTypeface.getHind_medium());
        tvBookTypeSave.setText(getStarted);
        tvBookTypeSave.setVisibility(View.VISIBLE);
        progressQuestion.setVisibility(View.INVISIBLE);
        progressQuestion.setProgress(startQuestion);
        super.onBackPressed();
    }

    private void setProgressCount() {
        String temp = "";

        Log.d("Index", "setProgressCount: " + startQuestion);
    /*if (startQuestion > 2 && callTypeInOutTele != 2) {
      temp = String.format(Locale.ENGLISH, "%02d", startQuestion - 1);
    } else if (callTypeInOutTele == 2) {
      temp = String.format(Locale.ENGLISH, "%02d", startQuestion);
    } else {
      temp = String.format(Locale.ENGLISH, "%02d", startQuestion);
    }*/
        temp = String.format(Locale.ENGLISH, "%02d", startQuestion);

        tv_centerCount.setText(temp + ".");
        String size = String.format(Locale.ENGLISH, "%02d", POSCOUNT);
        Fragment f = this.getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (f instanceof AskQuestionsFragment) {
            tv_center.setText(catName);
        }else {
            tv_center.setText(size);
        }
        progressQuestion.setProgress(Integer.parseInt(temp));
        progressQuestion.setMax(POSCOUNT);
    }

    private void toolBarSetting() {
        //getString(R.string.bookingType)
        setSupportActionBar(toolBookingType);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        toolBookingType.setNavigationIcon(R.drawable.ic_back);
        toolBookingType.setNavigationOnClickListener(view -> onBackPressed());

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onItemClick(View view, int adapvterPosition,
                            ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds,
                            String questionId, String name, int type,
                            SymptomResponsePojo.SymptomCategory.QuestionArr questionArr) {
        this.name = name;
        this.questionType = type;
        this.mQuestionArr = questionArr;

        dataStore(questionId, preDefineds.get(adapvterPosition).getName());
    }


    private void dataStore(String pos, String answer) {
        symptom = responseData.get(POS).getTitle();
        symptomId = responseData.get(POS).get_id();
/*
    mStringAnsHashMapHashMap.put(responseData.get(POS).get_id() + "_" + pos,
        new SymptomAnswer(symptomId, pos, answer, name, questionType, symptom, mQuestionArr.isMandatory(), mQuestionArr.isChecked()));
*/
        Log.w("TAG", "dataStore: "+symptomId+" "+questionType+" "+ name+" "+answer+" "+pos);

        if (responseData.get(POS).get_id().equals(symptomId)) {
            if (ansHashMaps.size() > 0) {
                for (int i = 0; i < ansHashMaps.size(); i++) {
                    if (ansHashMaps.get(i).getId().equals(pos)) {
                        ansHashMaps.remove(i);
                    }
                }
            }
            Log.w("TAG", "dataStore: "+symptomId+" "+questionType+" "+ name+" "+answer+" "+pos);
            ansHashMaps.add(new SymptomAnswer(symptomId, pos, answer, name, questionType, symptom, mQuestionArr.isMandatory(), mQuestionArr.isChecked()));
        }
    }


    @Override
    public void onItemClickChecboxResult(
            ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds,
            TextView tvSelect,
            SymptomResponsePojo.SymptomCategory.QuestionArr questionArr, String questionId,
            String questionname, int questiontype) {
        this.tvSelectChckBox = tvSelect;
        this.mQuestionArr = questionArr;
        this.questionId = questionId;
        this.name = questionname;
        this.questionType = questiontype;
        Intent intent = new Intent(this, QuestionSelectionActivity.class);
        Bundle args = new Bundle();
        args.putSerializable("ARRAYLIST", preDefineds);
        args.putString(isQuestionHeader, questionname);
        args.putBoolean(isFromChckBox, true);
        args.putBoolean(isFromImageUpload, false);
        args.putBoolean(isFromVideoUpload, false);
        args.putBoolean(isFromDatePicker, false);
        args.putBoolean(isFromTime, false);
        args.putBoolean(isFromNumberPicker, false);
        args.putBoolean(isFromDropDown, false);
        args.putBoolean(isFromTextAreaCommaSeparated, false);
        intent.putExtra("BUNDLE", args);
        startActivityForResult(intent, REQ_CODE_FOR_CHCKBOX);
    }

    @Override
    public void onItemClickImageUploadResult(RecyclerView recyclerViewQuestion,
                                             ImageTypeAdapterGrid questionAdapter, String name, RecyclerView.ViewHolder viewHld,
                                             String maximum, String minimum, String questionId,
                                             SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
                                             String questionname, int questiontype) {
        this.imageTypeAdapterGrid = questionAdapter;
        this.questionId = questionId;
        this.name = questionname;
        this.questionType = questiontype;
        this.recyclerViewQuestions = recyclerViewQuestion;
        this.mQuestionArr = questionArr;
        recyclerViewQuestions.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewQuestions.setLayoutManager(layoutManager);
        //  questionImages.add(0,new QuestionImage("",false,0));

        imageTypeAdapterGrid = new ImageTypeAdapterGrid(questionImages,this);
        recyclerViewQuestions.setAdapter(imageTypeAdapterGrid);

        //recyclerViewQuestions.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(recyclerViewQuestion, false);
        handlePicEvent = new HandlePictureEvents(this);
        if (questionImages.size() < Integer.parseInt(maximum)) {
            checkPermission();
        } else {
            alertProgress.alertinfo(this, "Max Limit Image");
        }

      /*  Intent intent = new Intent(this, QuestionSelectionActivity.class);
        Bundle args = new Bundle();
        args.putString(isQuestionTitle, name);
        args.putString(isQuestionHeader, questionname);
        args.putString(isMin, minimum);
        args.putString(isMax, maximum);
        args.putBoolean(isFromChckBox, false);
        args.putBoolean(isFromImageUpload, true);
        args.putBoolean(isFromVideoUpload, false);
        args.putBoolean(isFromDatePicker, false);
        args.putBoolean(isFromTime, false);
        args.putBoolean(isFromNumberPicker, false);
        args.putBoolean(isFromTextAreaCommaSeparated, false);
        args.putBoolean(isFromDropDown, false);
        intent.putExtra("BUNDLE", args);
        startActivityForResult(intent, REQ_CODE_FOR_IMAGEUPLOAD);*/
    }

    @Override
    public void onItemClickDateCurrentToFutureResult(TextView tvSelect,
                                                     SymptomResponsePojo.SymptomCategory.QuestionArr questionArr, String questionId,
                                                     String questionname, int questiontype) {
        this.tvSelectDateCurrentToFuture = tvSelect;
        this.mQuestionArr = questionArr;
        this.questionId = questionId;
        this.name = questionname;
        this.questionType = questiontype;
        Intent intent = new Intent(this, QuestionSelectionActivity.class);
        Bundle args = new Bundle();
        args.putBoolean(isFromChckBox, false);
        args.putString(isQuestionHeader, questionname);
        args.putBoolean(isFromImageUpload, false);
        args.putBoolean(isFromVideoUpload, false);
        args.putBoolean(isFromDatePicker, true);
        args.putBoolean(isFromTextAreaCommaSeparated, false);
        args.putBoolean(isFromDropDown, false);
        intent.putExtra("BUNDLE", args);
        startActivityForResult(intent, REQ_CODE_FOR_DATE_CURRENT_TO_FUTURE);
    }

    @Override
    public void onItemClickDatePastToCurrentResult(TextView tvSelect,
                                                   SymptomResponsePojo.SymptomCategory.QuestionArr questionArr, String questionId,
                                                   String questionname, int questiontype) {
        this.tvSelectDatePastToCurrent = tvSelect;
        this.mQuestionArr = questionArr;
        this.questionId = questionId;
        this.name = questionname;
        this.questionType = questiontype;
        Intent intent = new Intent(this, QuestionSelectionActivity.class);
        Bundle args = new Bundle();
        args.putBoolean(isFromChckBox, false);
        args.putString(isQuestionHeader, questionname);
        args.putBoolean(isFromImageUpload, false);
        args.putBoolean(isFromVideoUpload, false);
        args.putBoolean(isFromDatePicker, true);
        args.putBoolean(isFromTime, false);
        args.putBoolean(isFromTextAreaCommaSeparated, false);
        args.putBoolean(isFromNumberPicker, false);
        args.putBoolean(isFromDropDown, false);
        intent.putExtra("BUNDLE", args);
        startActivityForResult(intent, REQ_CODE_FOR_DATE_PAST_TO_CURRENT);
    }

    @Override
    public void onItemClickTimeResult(TextView tvSelect,
                                      SymptomResponsePojo.SymptomCategory.QuestionArr questionArr, String questionId,
                                      String questionname, int questiontype) {
        this.tvSelectTime = tvSelect;
        this.mQuestionArr = questionArr;
        this.name = questionname;
        this.questionType = questiontype;
        this.questionId = questionId;
        Intent intent = new Intent(this, QuestionSelectionActivity.class);
        Bundle args = new Bundle();
        args.putString(isQuestionHeader, questionname);
        args.putBoolean(isFromChckBox, false);
        args.putBoolean(isFromImageUpload, false);
        args.putBoolean(isFromVideoUpload, false);
        args.putBoolean(isFromDatePicker, false);
        args.putBoolean(isFromTime, true);
        args.putBoolean(isFromNumberPicker, false);
        args.putBoolean(isFromDropDown, false);
        args.putBoolean(isFromTextAreaCommaSeparated, false);
        intent.putExtra("BUNDLE", args);
        startActivityForResult(intent, REQ_CODE_FOR_TIME);

    }

    @Override
    public void onItemClickTextAreaCommaSeparatedResult(TextView tvSelect,
                                                        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr, String questionId,
                                                        String questionname, int questiontype) {
        this.tvSelectTextAreaCommaSeparated = tvSelect;
        this.mQuestionArr = questionArr;
        this.questionId = questionId;
        this.name = questionname;
        this.questionType = questiontype;
        Intent intent = new Intent(this, QuestionSelectionActivity.class);
        Bundle args = new Bundle();
        args.putString(isQuestionTitle, questionArr.getDescription());
        args.putBoolean(isFromChckBox, false);
        args.putBoolean(isFromImageUpload, false);
        args.putBoolean(isFromVideoUpload, false);
        args.putBoolean(isFromDatePicker, false);
        args.putBoolean(isFromTime, false);
        args.putBoolean(isFromNumberPicker, false);
        args.putString(isQuestionHeader, questionname);
        args.putBoolean(isFromDropDown, false);
        args.putBoolean(isFromTextAreaCommaSeparated, true);
        intent.putExtra("BUNDLE", args);
        startActivityForResult(intent, REQ_CODE_FOR_TEXT_AREA_COMMASEPARATED);
    }

    @Override
    public void onItemClickDropDownResult(
            ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds,
            TextView tvSelect,
            SymptomResponsePojo.SymptomCategory.QuestionArr questionArr, String questionid,
            String questionname, int questiontype) {
        this.tvDropDown = tvSelect;
        this.mQuestionArr = questionArr;
        this.questionId = questionid;
        this.name = questionname;
        this.questionType = questiontype;
        Intent intent = new Intent(this, QuestionSelectionActivity.class);
        Bundle args = new Bundle();
        args.putSerializable("ARRAYLIST", preDefineds);
        args.putString(isQuestionHeader, questionname);
        args.putBoolean(isFromChckBox, false);
        args.putBoolean(isFromImageUpload, false);
        args.putBoolean(isFromVideoUpload, false);
        args.putBoolean(isFromDatePicker, false);
        args.putBoolean(isFromTime, false);
        args.putBoolean(isFromNumberPicker, false);
        args.putBoolean(isFromDropDown, true);
        args.putBoolean(isFromTextAreaCommaSeparated, false);
        intent.putExtra("BUNDLE", args);
        startActivityForResult(intent, REQ_CODE_FOR_DROPDOWN);
    }

    @Override
    public void onItemClickVideoUploadResult(RecyclerView recyclerViewQuestion,
                                             VideoTypeAdapterGrid imageTypeAdapterGrid, String name, RecyclerView.ViewHolder viewHolder,
                                             String maximum, String minimum, String questionId, VideoTypeAdapterGrid adapterGrid,
                                             SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
                                             String questionname, int questiontype) {
        this.videoTypeAdapterGrid = imageTypeAdapterGrid;
        this.recyclerViewQuestions = recyclerViewQuestion;
        this.questionId = questionId;
        this.name = questionname;
        this.questionType = questiontype;
        this.mQuestionArr = questionArr;
        recyclerViewQuestions.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerViewQuestions.setLayoutManager(layoutManager);
        //  questionVideo.add(0,new QuestionImage("",false,0));

        videoTypeAdapterGrid = new VideoTypeAdapterGrid(questionVideo, this);
        recyclerViewQuestions.setAdapter(videoTypeAdapterGrid);

        //recyclerViewQuestions.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(recyclerViewQuestion, false);
        handlePicEvent = new HandlePictureEvents(this);
        if (questionVideo.size() < Integer.parseInt(maximum)) {
            checkPermissionVideo();
        } else {
            alertProgress.alertinfo(this, "Max Limit Video");
        }

/*    Intent intent = new Intent(this, QuestionSelectionActivity.class);
    Bundle args = new Bundle();
    args.putString(isQuestionHeader, questionname);
    args.putString(isQuestionTitle, name);
    args.putString(isMin, minimum);
    args.putString(isMax, maximum);
    args.putBoolean(isFromChckBox, false);
    args.putBoolean(isFromImageUpload, false);
    args.putBoolean(isFromVideoUpload, true);
    args.putBoolean(isFromDatePicker, false);
    args.putBoolean(isFromTime, false);
    args.putBoolean(isFromNumberPicker, false);
    args.putBoolean(isFromTextAreaCommaSeparated, false);
    args.putBoolean(isFromDropDown, false);
    intent.putExtra("BUNDLE", args);
    startActivityForResult(intent, REQ_CODE_FOR_VIDEOUPLOAD);*/
    }


    @Override
    public void onItemClickNumberResult(
            String name, SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
            String questionId, String questionname, int questiontype) {
        this.mQuestionArr = questionArr;
        this.questionId = questionId;
        this.name = questionname;
        this.questionType = questiontype;
        dataStore(questionId, name);
    }

    /**
     * <h2>takeVideoFromCamera</h2>
     * <p> method to open a video gallery of a device</p>
     */
    public void chooseVideoFromGallary() {
        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent();
            intent.setType("video/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, GALLERY);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            intent.setType("video/*");
            //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, GALLERY);
        }
       /* Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, ConstantsInteface.GALLERY);*/
    }

    /**
     * Method for return file path of Gallery image
     *
     * @return path of the selected image file from gallery
     */

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @SuppressLint("NewApi")
    public static String getPath(final Context context,final Uri uri) {

        // check here to KITKAT or new version
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            final String documentId = DocumentsContract.getDocumentId(uri);
            if (documentId.startsWith("raw:")) {
                return documentId.replaceFirst("raw:", "");
            }
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return context.getExternalFilesDir(null) + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {
                // "content://downloads/public_downloads",
                // "content://downloads/my_downloads",
                // "content://downloads/all_downloads"
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri =
                        ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"),
                                Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] { split[1] };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri)) return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is <span id="IL_AD2"
     * class="IL_AD">useful</span> for MediaStore Uris, and other file-based
     * ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = { column };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } catch (IllegalArgumentException e) {

            return null;
        } finally {
            if (cursor != null) cursor.close();
        }
        return null;
    }

    public static String getPathAboveN(Context context, Uri returnUri) {

        try {
            Cursor returnCursor = context.getContentResolver().query(returnUri, null, null, null, null);
        /*
         * Get the column indexes of the data in the Cursor,
               move to the first row in the Cursor, get the data,
               and display it.
          */
            int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
            int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
            returnCursor.moveToFirst();
            String name = (returnCursor.getString(nameIndex));
            String size = (Long.toString(returnCursor.getLong(sizeIndex)));
            File file = new File(context.getFilesDir(), name);

            InputStream inputStream = context.getContentResolver().openInputStream(returnUri);
            FileOutputStream outputStream = new FileOutputStream(file);
            int read = 0;
            int maxBufferSize = 1024 * 1024;
            int bytesAvailable = inputStream.available();

            //int bufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);

            final byte[] buffers = new byte[bufferSize];
            while ((read = inputStream.read(buffers)) != -1) {
                outputStream.write(buffers, 0, read);
            }
            inputStream.close();
            outputStream.close();

            return file.getPath();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * <h2>takeVideoFromCamera</h2>
     * <p> method to open a video camera of a device</p>
     */
    private void takeVideoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
       /* intent.putExtra("android.intent.extra.durationLimit", VIDEO_DURATION);
        intent.putExtra("android.intent.extra.videoQuality",1);*/
        startActivityForResult(intent, CAMERA);
    }


    @Override
    public void onItemClickNumberSiderResult(String maximum, String minimum, TextView tvSelect,
                                             String name, SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
                                             String questionId, String questionname, int questiontype) {
        this.tvNumberSlider = tvSelect;
        this.mQuestionArr = questionArr;
        this.questionId = questionId;
        this.name = questionname;
        this.questionType = questiontype;
        Intent intent = new Intent(this, QuestionSelectionActivity.class);
        Bundle args = new Bundle();
        args.putString(isQuestionHeader, questionname);
        args.putString(isQuestionTitle, name);
        args.putString(isMin, minimum);
        args.putString(isMax, maximum);
        args.putBoolean(isFromChckBox, false);
        args.putBoolean(isFromImageUpload, false);
        args.putBoolean(isFromVideoUpload, false);
        args.putBoolean(isFromDatePicker, false);
        args.putBoolean(isFromTime, false);
        args.putBoolean(isFromNumberPicker, true);
        args.putBoolean(isFromTextAreaCommaSeparated, false);
        args.putBoolean(isFromDropDown, false);
        intent.putExtra("BUNDLE", args);
        startActivityForResult(intent, REQ_CODE_FOR_NUMBERSLIDER);

    }

    /**
     * <h2>checkPermission</h2>
     * <p>checking for the permission for camera and file storage at run time for
     * build version more than 22
     */
    private void checkPermissionVideo() {

        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList =
                    new ArrayList<>();
            myPermissionConstantsArrayList.clear();
            myPermissionConstantsArrayList.add(
                    AppPermissionsRunTime.MyPermissionConstants.PERMISSION_CAMERA);
            myPermissionConstantsArrayList.add(
                    AppPermissionsRunTime.MyPermissionConstants.PERMISSION_WRITE_EXTERNAL_STORAGE);
            myPermissionConstantsArrayList.add(
                    AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_EXTERNAL_STORAGE);

            if (AppPermissionsRunTime.checkPermission(this, myPermissionConstantsArrayList,
                    REQUEST_CODE_PERMISSION_VIDEO)) {

                showVideoDialog();
            }
        } else {
            showVideoDialog();
        }
    }

    /**
     * <h2>showVideoDialog</h2>
     * <p> method to choose a option from gallery or camera</p>
     */
    private void showVideoDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select video from gallery",
                "Record video from camera"};
        pictureDialog.setItems(pictureDialogItems,
                (dialog, which) -> {
                    switch (which) {
                        case 0:
                            chooseVideoFromGallary();
                            break;
                        case 1:
                            takeVideoFromCamera();
                            break;
                    }
                });
        pictureDialog.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // TODO Auto-generated method stub


/*
        if (requestCode == REQ_CODE_FOR_IMAGEUPLOAD) {

            if (resultCode == RESULT_OK) {
                String path = data.getStringExtra("Path");
                if (path.endsWith(",")) {
                    path = path.substring(0, path.length() - 1);
                }
                Bundle args = data.getBundleExtra("BUNDLE");
                questionImages = (ArrayList<QuestionImage>) args.getSerializable("QuestionImages");
                Log.d("RESULT_OK",
                        "onActivityResult: ABC" + questionImages.size() + "    " + questionImages.get(
                                0).getImage());

                if (questionImages.size() > 0) {
                    recyclerViewQuestions.setVisibility(View.VISIBLE);
                    recyclerViewQuestions.setHasFixedSize(true);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                    layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                    recyclerViewQuestions.setLayoutManager(layoutManager);
                    //  questionImages.add(0,new QuestionImage("",false,0));
                    final LayoutAnimationController controller =
                            AnimationUtils.loadLayoutAnimation(this, R.anim
                            .layout_animation_right_to_left);



                    recyclerViewQuestions.setItemAnimator(new DefaultItemAnimator());
                    imageTypeAdapterGrid = new ImageTypeAdapterGrid(questionImages);
                    recyclerViewQuestions.setAdapter(imageTypeAdapterGrid);

                    // recyclerViewQuestions.setNestedScrollingEnabled(false);
                    ViewCompat.setNestedScrollingEnabled(recyclerViewQuestions, false);

                    //  imageTypeAdapterGrid.notifyDataSetChanged();
                    dataStore(mQuestionArr.get_id(), path);
                }
            } else if (resultCode == RESULT_CANCELED) {

                Log.d("RESULT_OK", "onActivityResult: " + " RESULT_CANCELED ");

            }

        }
*/

/*
        if (requestCode == REQ_CODE_FOR_VIDEOUPLOAD) {

            if (resultCode == RESULT_OK) {
                String path = data.getStringExtra("Path");
                if (path.endsWith(",")) {
                    path = path.substring(0, path.length() - 1);
                }
                Bundle args = data.getBundleExtra("BUNDLE");
                questionVideo = (ArrayList<QuestionImage>) args.getSerializable("QuestionVideos");
                Log.d("RESULT_OK",
                        "onActivityResult: ABC" + questionVideo.size() + "    " + questionVideo.get(
                                0).getImage());

                if (questionVideo.size() > 0) {
                    recyclerViewQuestions.setVisibility(View.VISIBLE);
                    recyclerViewQuestions.setHasFixedSize(true);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                    layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                    recyclerViewQuestions.setLayoutManager(layoutManager);
                    //  questionVideo.add(0,new QuestionImage("",false,0));
                    final LayoutAnimationController controller =
                            AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_right_to_left);

                    recyclerViewQuestions.setItemAnimator(new DefaultItemAnimator());
                    videoTypeAdapterGrid = new VideoTypeAdapterGrid(questionVideo, this);
                    recyclerViewQuestions.setAdapter(videoTypeAdapterGrid);

                    // recyclerViewQuestions.setNestedScrollingEnabled(false);
                    //  ViewCompat.setNestedScrollingEnabled(recyclerViewQuestions, false);

                    //  imageTypeAdapterGrid.notifyDataSetChanged();
                    dataStore(mQuestionArr.get_id(), path);
                }

            } else if (resultCode == RESULT_CANCELED) {

                Log.d("RESULT_OK", "onActivityResult: " + " RESULT_CANCELED ");

            }

        }
*/



        if (resultCode == RESULT_OK) {
            String names = "";
            String id = "";
            switch (requestCode) {
                case REQ_CODE_FOR_CHCKBOX:
                    names = data.getStringExtra("CARDID");
                    if (names != null && names.endsWith(",")) {
                        names = names.substring(0, names.length() - 1);
                    }
                    id = data.getStringExtra("ID");

                    if (id != null && id.endsWith(",")) {
                        id = id.substring(0, id.length() - 1);
                    }
                    tvSelectChckBox.setTextColor(ContextCompat.getColor(this, R.color.black));

                    tvSelectChckBox.setText(names);

                    mQuestionArr.setChckBoxTxt(names);
                    Log.d("RESULT_OK", "onActivityResult: " + data.getData() + "   " + names + "   " + id);

                    dataStore(mQuestionArr.get_id(), names);
                    break;
                case REQ_CODE_FOR_DATE_CURRENT_TO_FUTURE:
                    names = data.getStringExtra("DATE_APPEND");
                    tvSelectDateCurrentToFuture.setTextColor(ContextCompat.getColor(this, R.color.black));
                    tvSelectDateCurrentToFuture.setText(names);
                    mQuestionArr.setDateCurrentToFutureTxt(names);
                    Log.d("RESULT_OK", "onActivityResult: " + data.getData() + "   " + names);
                    dataStore(mQuestionArr.get_id(), names);
                    break;
                case REQ_CODE_FOR_DATE_PAST_TO_CURRENT:
                    names = data.getStringExtra("DATE_APPEND");
                    tvSelectDatePastToCurrent.setTextColor(ContextCompat.getColor(this, R.color.black));

                    tvSelectDatePastToCurrent.setText(names);
                    mQuestionArr.setDatePastToCurrentTxt(names);
                    Log.d("RESULT_OK", "onActivityResult: " + data.getData() + "   " + names);
                    dataStore(mQuestionArr.get_id(), names);
                    break;
                case REQ_CODE_FOR_TEXT_AREA_COMMASEPARATED:
                    names = data.getStringExtra("TEXTAREACOMMASEPARATED");
                    tvSelectTextAreaCommaSeparated.setTextColor(ContextCompat.getColor(this, R.color.black));
                    tvSelectTextAreaCommaSeparated.setText(names);
                    mQuestionArr.setTextCommaSeparatedTxt(names);
                    Log.d("RESULT_OK", "onActivityResult: " + data.getData() + "   " + names);
                    dataStore(mQuestionArr.get_id(), names);
                    break;
                case REQ_CODE_FOR_TIME:
                    names = data.getStringExtra("TIME");
                    tvSelectTime.setTextColor(ContextCompat.getColor(this, R.color.black));
                    tvSelectTime.setText(names);
                    mQuestionArr.setTimeTxt(names);
                    Log.d("RESULT_OK", "onActivityResult: " + data.getData() + "   " + names);
                    dataStore(mQuestionArr.get_id(), names);

                    break;
                case REQ_CODE_FOR_DROPDOWN:
                    names = data.getStringExtra("DROPDOWN");
                    id = data.getStringExtra("ID");

                    if (id != null && id.endsWith(",")) {
                        id = id.substring(0, id.length() - 1);
                    }
                    tvDropDown.setTextColor(ContextCompat.getColor(this, R.color.black));

                    tvDropDown.setText(names);

                    mQuestionArr.setDropDownTxt(names);
                    Log.d("RESULT_OK", "onActivityResult: " + data.getData() + "   " + names);
                    dataStore(mQuestionArr.get_id(), names);
                    break;
                case REQ_CODE_FOR_NUMBERSLIDER:
                    names = data.getStringExtra("NUMBERSLIDER");

                    tvNumberSlider.setTextColor(ContextCompat.getColor(this, R.color.black));

                    tvNumberSlider.setText(names);
                    mQuestionArr.setNumberSliderTxt(names);
                    Log.d("RESULT_OK", "onActivityResult: " + data.getData() + "   " + names);
                    dataStore(mQuestionArr.get_id(), names);
                    break;
                case CAMERA_PIC:
                    handlePicEvent.startCropImage(handlePicEvent.newFile);
                    break;
                case CAMERA:
                    Uri contentURI = data.getData();
                    videoPositionTaken++;
                    String recordedVideoPath = "";
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        recordedVideoPath = getPathAboveN(mContext, data.getData());
                    } else {

                        recordedVideoPath = getPath(mContext, data.getData());
                    }

                    Log.d("frrr", recordedVideoPath);

                    try {
                        File fileExist = new File(recordedVideoPath);
                        dataStore(mQuestionArr.get_id(),"https://" + Constants.Amazonbucket + "." + AMAZON_BASE_URL
                            + AmazonUploadJobImage + "/"
                            + fileExist.getName());
                        handlePicEvent.uploadToAmazon(this, AmazonUploadJobImage, fileExist, new ImageUploadedAmazon() {
                            @Override
                            public void onSuccessAdded(String image) {
                                // imageArray.put(imagePositionTaken,image);
                                Log.w("TAG", "onSuccessAdded: "+image );

                            }

                            @Override
                            public void onerror(String errormsg) {
                            }

                        });

                        Log.d("Index",
                                "onActivityResult: " + questionImages.size() + "   " + videoPositionTaken);


                        questionVideo.add(questionVideo.size(),
                                new QuestionVideo(recordedVideoPath, true, videoPositionTaken, true));
                        if(questionVideo.size() > 0)
                        {
                            recyclerViewQuestions.setVisibility(View.VISIBLE);

                            names = getVideos();
                            if (names.endsWith(",")) {
                                names = names.substring(0, names.length() - 1);
                            }
                        }
                        videoTypeAdapterGrid.notifyDataSetChanged();
                        //saveVideoToInternalStorage(recordedVideoPath);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case GALLERY_PIC:
                    if (data != null) {
                        handlePicEvent.gallery(data.getData());
                    }
                    break;
                case GALLERY:
                    //  if (resultCode == RESULT_OK) {
                    Log.d("what", "gale");
                    // if (data != null) {
                    // Uri contentURI = data.getData();
                    videoPositionTaken++;
                    //  String selectedVideoPath = getPath(contentURI);
                    String selectedVideoPath = "";
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        selectedVideoPath = getPathAboveN(mContext, data.getData());
                    } else {

                        selectedVideoPath = getPath(mContext, data.getData());
                    }
                    Log.d("frrr", selectedVideoPath);

                    try {
                        File fileExist = new File(selectedVideoPath);
                        dataStore(mQuestionArr.get_id(),"https://" + Constants.Amazonbucket + "." + AMAZON_BASE_URL
                            + AmazonUploadJobImage + "/"
                            + fileExist.getName());
                        handlePicEvent.uploadToAmazon(this, AmazonUploadJobImage, fileExist, new ImageUploadedAmazon() {
                            @Override
                            public void onSuccessAdded(String image) {
                               // imageArray.put(imagePositionTaken,image);
                                Log.w("TAG", "onSuccessAdded: "+image );

                            }

                            @Override
                            public void onerror(String errormsg) {
                            }

                        });


                        Log.d("Index",
                                "onActivityResult: " + questionVideo.size() + "   " + videoPositionTaken);
                        questionVideo.add(questionVideo.size(),
                                new QuestionVideo(selectedVideoPath, true, videoPositionTaken, true));
                        if(questionVideo.size() > 0)
                        {
                            recyclerViewQuestions.setVisibility(View.VISIBLE);
                            names = getVideos();
                            if (names.endsWith(",")) {
                                names = names.substring(0, names.length() - 1);
                            }
                        }
                        videoTypeAdapterGrid.notifyDataSetChanged();
                        //  saveVideoToInternalStorage(selectedVideoPath);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case CROP_IMAGE:
                    //  if (resultCode == RESULT_OK) {
                    //  onShowProgressPromo();
                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    imagePositionTaken++;
                    if (path != null) {
                        try {
                            File fileExist = new File(path);

                            handlePicEvent.uploadToAmazon(this, AmazonUploadJobImage, fileExist, new ImageUploadedAmazon() {
                                @Override
                                public void onSuccessAdded(String image) {
                                    // imageArray.put(imagePositionTaken,image);
                                    Log.w("TAG", "onSuccessAdded: "+image );
                                    dataStore(mQuestionArr.get_id(),image);

                                }

                                @Override
                                public void onerror(String errormsg) {
                                }

                            });


                            Log.d("Index",
                                    "onActivityResult: " + questionImages.size() + "   " + imagePositionTaken);
                            questionImages.add(questionImages.size(),
                                    new QuestionImage(path, true, imagePositionTaken, false));
                            if (questionImages.size() > 0) {
                                recyclerViewQuestions.setVisibility(View.VISIBLE);
                                names = getImages();
                                if (names.endsWith(",")) {
                                    names = names.substring(0, names.length() - 1);
                                }
                                //dataStore(mQuestionArr.get_id(),names);
                            }
                            imageTypeAdapterGrid.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
            }
        }

        if (resultCode == RESULT_CANCELED) {
            switch (requestCode) {
                case REQ_CODE_FOR_CHCKBOX:
                    tvSelectChckBox.setTextColor(ContextCompat.getColor(this, R.color.grey));

                    Log.d("RESULT_OK", "onActivityResult: " + " RESULT_CANCELED ");

                    break;
                case REQ_CODE_FOR_DATE_CURRENT_TO_FUTURE:
                    tvSelectDateCurrentToFuture.setTextColor(ContextCompat.getColor(this, R.color.grey));
                    Log.d("RESULT_OK", "onActivityResult: " + " RESULT_CANCELED ");

                    break;
                case REQ_CODE_FOR_DATE_PAST_TO_CURRENT:
                    tvSelectDatePastToCurrent.setTextColor(ContextCompat.getColor(this, R.color.grey));
                    Log.d("RESULT_OK", "onActivityResult: " + " RESULT_CANCELED ");
                    break;
                case REQ_CODE_FOR_TEXT_AREA_COMMASEPARATED:
                    tvSelectTextAreaCommaSeparated.setTextColor(ContextCompat.getColor(this, R.color.grey));
                    Log.d("RESULT_OK", "onActivityResult: " + " RESULT_CANCELED ");
                    break;
                case REQ_CODE_FOR_TIME:
                    tvSelectTime.setTextColor(ContextCompat.getColor(this, R.color.grey));
                    Log.d("RESULT_OK", "onActivityResult: " + " RESULT_CANCELED ");
                    break;
                case REQ_CODE_FOR_DROPDOWN:
                    tvDropDown.setTextColor(ContextCompat.getColor(this, R.color.grey));
                    Log.d("RESULT_OK", "onActivityResult: " + " RESULT_CANCELED ");
                    break;
                case REQ_CODE_FOR_NUMBERSLIDER:
                    tvNumberSlider.setTextColor(ContextCompat.getColor(this, R.color.grey));
                    Log.d("RESULT_OK", "onActivityResult: " + " RESULT_CANCELED ");
                    break;
            }
        }


    }

    private String getVideos() {
        StringBuilder stringBuilder = new StringBuilder();
        for (QuestionVideo questionVideo : questionVideo) {
            stringBuilder.append(questionVideo.getImage());
            stringBuilder.append(",");
        }
        return stringBuilder.toString().trim();
    }
    private String getImages() {
        StringBuilder stringBuilder = new StringBuilder();
        for (QuestionImage questionImage : questionImages) {
            stringBuilder.append(questionImage.getImage());
            stringBuilder.append(",");
        }
        return stringBuilder.toString().trim();
    }

    @Override
    public void onSessionExpired() {

    }

    @Override
    public void onLogout(String message, SessionManagerImpl sessionManager) {
        alertProgress.alertPositiveOnclick(this, message, getString(R.string.logout),
                getString(R.string.ok), isClicked -> Utility.setMAnagerWithBID(this, manager));

    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onConnectionError(String connectionError) {

    }

    @Override
    public void onShowProgress() {

    }

    @Override
    public void onHideProgress() {

    }

    @OnClick({R.id.tvBookTypeSave, R.id.tv_skip})
    public void onContinueClicked(View v) {
        switch (v.getId()) {
            case R.id.tv_skip:
                if (isNetworkAvailable(this)) {
                if (startQuestion == 5 && callTypeInOutTele == 2) {
                    symptomQuestion();
                } else if ((startQuestion == 4 && callTypeInOutTele == 1 && !inCall) || (startQuestion == 4 && callTypeInOutTele == 3 && !teleCall)) {
                    symptomQuestion();
                } else if ((startQuestion == 3 && inCall) || (startQuestion == 3 && teleCall)) {
                    symptomQuestion();
                } else if (startQuestion == 4 && outCall) {
                    symptomQuestion();
                } else {
                    saveNextQuestionClick();
                }
                } else {
                    alertProgress.tryAgain(this, getString(R.string.pleaseCheckInternet), getString(R.string.system_error), isClicked -> {
                    });
                }
                break;
            case R.id.tvBookTypeSave:
                if (isNetworkAvailable(this)) {
                if (manager.getGuestLogin()) {
                    Intent intent = new Intent(this, LoginActivity.class);
                    //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                } else {
                    saveButtonVisibility();
                    saveNextQuestionClick();
                }
                } else {
                    alertProgress.tryAgain(this, getString(R.string.pleaseCheckInternet), getString(R.string.system_error), isClicked -> {
                    });
                }
                break;

        }


    }

    private void symptomQuestion() {
        if(searchSymptomSelectedID != null &&  !searchSymptomSelectedID.isEmpty() && symptomSelectedID.isEmpty())
        {
            isSymptomSelectedID = true;
            symptomSelectedID = searchSymptomSelectedID;
        }

        if (isSymptomSelectedID) {
            pos = 0;
            Log.d("symptomSelectedID", "OnSymptomSelectedID:One " + symptomSelectedID);
            if(isNetworkAvailable(mContext))
            presenter.openGetSymptoms(this, LSPApplication.getInstance().getAuthToken(manager.getSID()), symptomSelectedID);
        } else {
            alertProgress.alertinfo(this, "Please select a symptom");
        }
    }

    private void saveButtonVisibility() {
        tv_center.setTypeface(appTypeface.getHind_medium());
        tv_skip.setTypeface(appTypeface.getHind_medium());
        tv_skip.setText(next);
        tv_centerCount.setTypeface(appTypeface.getHind_medium());
        tv_centerCount.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimension(R.dimen.dp_15));
        tv_center.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                getResources().getDimension(R.dimen.dp_11));

        tv_centerCount.setVisibility(View.VISIBLE);
        tv_skip.setVisibility(View.VISIBLE);
        tvBookTypeSave.setVisibility(View.GONE);
        progressQuestion.setVisibility(View.VISIBLE);
        setProgressCount();

        Utility.hideKeyboard(this);
    }

    @Override
    public void onFragmentInteraction(Date uri, boolean isSchedule) {
        if (biddingDateTIme != null) {
            biddingDateTIme.onDateTimeSel(uri, isSchedule);
        }
    }


    public void biddingSelected(NowLaterRepeatTypeFragment myBiddingBookTypeFrag) {
        biddingDateTIme = myBiddingBookTypeFrag;
    }

    @Override
    public void onFragmentNowLater(boolean isNow, long selectedScheduledDateTime,
                                   int selectedDuration) {
        presenter.onNowLaterSelected(isNow, selectedScheduledDateTime, selectedDuration);
    }

    @Override
    public void onFragmentRepeat(long selectedScheduledDateTime, long selectedEndDate,
                                 int selectedDuration, ArrayList<String> repeatBooking) {
        presenter.onRepeatSelected(selectedScheduledDateTime, selectedEndDate, selectedDuration,
                repeatBooking);

    }

    @Override
    public void onResponseSuccess(
            ArrayList<SymptomResponsePojo.SymptomCategory> symptomResponseData,
            String symptomSelectedID) {

        //   Log.d("onResponseSuccess", "onResponseSuccess: "+symptomResponseData.toString());
        this.symptomSelectedID = symptomSelectedID;
        questionArrs.clear();
        responseData.clear();


        COUNT = symptomResponseData.size();
        POSCOUNT = progressQuestion.getProgress() + COUNT;
        progressQuestion.setMax(POSCOUNT);
        responseData.addAll(symptomResponseData);

        for (int i = 0; i < symptomResponseData.size(); i++) {

            questionArrs.addAll(symptomResponseData.get(i).getQuestionArr());

        }

        saveNextQuestionClick();

    }

    @Override
    public void redirectTodoctorList() {
   /* if (mStringAnsHashMapHashMap != null && mStringAnsHashMapHashMap.size() > 0) {
      jsonArray = new Gson().toJson(mStringAnsHashMapHashMap.values());
    }*/
        saveSelectedData();
        if (ansSymptomHashMaps.size() > 0)
            jsonArray = new Gson().toJson(ansSymptomHashMaps);
            else{
                ansHashMaps.add(new SymptomAnswer(symptomId, symptomId, "", "", 1, symptom, false,false));
                ansSymptomHashMaps.add(new AnsSymptomHashMap(Constants.symptomId, Constants.symptom, ansHashMaps));
            jsonArray = new Gson().toJson(ansSymptomHashMaps);
        }
            Log.w("DEBUG", "saveNextQuestionClick: jsonArray" + jsonArray);
        Intent intent;
        if (isFavDoc) {
            if(callTypeInOutTele != 2) {
                intent = new Intent(this, TimeSlots.class);
                intent.putExtra("ProviderId", proId);
                intent.putExtra("ImagePro", docImage);
                intent.putExtra("expertise", docExpertise);
                intent.putExtra("NAME",
                    docName);
                intent.putExtra("isProFileView", false);
            }else{
                intent = new Intent(mContext, ConfirmBookActivity.class);
            }
        } else {
            intent = new Intent(this, DoctorList.class);
            intent.putExtra("isFavDoc", false);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
    }


    @Override
    public void OnSymptomSelectedID(String symptomSelectedId, boolean isSelect) {
        if (!symptomSelectedId.isEmpty()) {
            if (symptomSelectedId.endsWith(",")) {
                symptomSelectedId = symptomSelectedId.substring(0, symptomSelectedId.length() - 1);
            }
            this.symptomSelectedID = symptomSelectedId;
            this.isSymptomSelectedID = isSelect;
            Log.d("symptomSelectedID", "OnSymptomSelectedID: " + symptomSelectedID);
        } else {
            this.isSymptomSelectedID = false;
        }
    }

    @Override
    public void isSelect(boolean isSelect) {
        this.isSelect = isSelect;

    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        // //Utility.checkAndShowNetworkError(this);
    }



    /**
     * predefined method to check run time permissions list call back
     *
     * @param requestCode   request code
     * @param permissions:  contains the list of requested permissions
     * @param grantResults: contains granted and un granted permissions result list
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        boolean isDenine = false;
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_VIDEO:
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        isDenine = true;
                    }
                }
                if (isDenine) {
                    Toast.makeText(this, "Permission denied by the user", Toast.LENGTH_SHORT).show();
                } else {
                    showVideoDialog();
                }
                break;
            case REQUEST_CODE_PERMISSION_IMAGE:
                for (int grantResult : grantResults) {
                    if (grantResult != PackageManager.PERMISSION_GRANTED) {
                        isDenine = true;
                    }
                }
                if (isDenine) {
                    Toast.makeText(this, "Permission denied by the user", Toast.LENGTH_SHORT).show();
                } else {
                    selectImage();
                }
                break;
           /* default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;*/
        }
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    @Override
    public void onExplanationNeeded(String[] permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            handlePicEvent.openDialog();
        }
    }

    @Override
    public void deleteVideoPhoto(int adapterPosition, int imagePostion) {
        alertProgress.alertPositiveNegativeOnclick(this,
                getString(R.string.areYouSureYouWantOTDeleteImage),
                getString(R.string.delete), getString(R.string.ok), getString(R.string.cancel), true,
                isClicked -> {
                    if (isClicked) {
                        questionVideo.remove(adapterPosition);
                        //  videoArray.remove(imagePostion);
                        videoPositionTaken--;
                        Log.d("Index",
                                "onActivityResult: " + questionVideo.size() + "   " + videoPositionTaken);

                        videoTypeAdapterGrid.notifyDataSetChanged();
                    }
                });

    }

    @Override
    public void deleteImagePhoto(int adapterPosition, int imagePostion) {
        alertProgress.alertPositiveNegativeOnclick(this,
                getString(R.string.areYouSureYouWantOTDeleteImage),
                getString(R.string.delete), getString(R.string.ok), getString(R.string.cancel), true,
                isClicked -> {
                    if (isClicked) {
                        questionImages.remove(adapterPosition);
                        imageArray.remove(imagePostion);
                        imagePositionTaken--;
                        Log.d("Index",
                                "onActivityResult: " + questionImages.size() + "   " + imagePositionTaken);

                        imageTypeAdapterGrid.notifyDataSetChanged();
                    }
                });

    }


    /**
     * <h2>checkPermission</h2>
     * <p>checking for the permission for camera and file storage at run time for
     * build version more than 22
     */
    private void checkPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList =
                    new ArrayList<>();
            myPermissionConstantsArrayList.clear();
            myPermissionConstantsArrayList.add(
                    AppPermissionsRunTime.MyPermissionConstants.PERMISSION_CAMERA);
            myPermissionConstantsArrayList.add(
                    AppPermissionsRunTime.MyPermissionConstants.PERMISSION_WRITE_EXTERNAL_STORAGE);
            myPermissionConstantsArrayList.add(
                    AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_EXTERNAL_STORAGE);
            if (AppPermissionsRunTime.checkPermission(this, myPermissionConstantsArrayList,
                    REQUEST_CODE_PERMISSION_IMAGE)) {

                selectImage();
            }
        } else {
            selectImage();
        }
    }

    /**
     * <h2>selectImage</h2>
     *
     * @see HandlePictureEvents
     * This mehtod is used to show the popup where we can select our images.
     */
    private void selectImage() {
        handlePicEvent.openDialog();
    }


}