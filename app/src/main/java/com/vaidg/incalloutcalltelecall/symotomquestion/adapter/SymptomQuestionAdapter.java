package com.vaidg.incalloutcalltelecall.symotomquestion.adapter;


import static com.vaidg.utilities.Constants.POSCOUNT;

import adapters.ImageTypeAdapterGrid;
import adapters.VideoTypeAdapterGrid;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.questionselection.model.SymptomResponsePojo;
import com.vaidg.incalloutcalltelecall.symotomquestion.SymptomQuestionsContract;
import com.vaidg.utilities.AppTypeface;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by 3Embed on 29/11/19.
 */
public class SymptomQuestionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


  private static final int RADIO_BUTTON = 1;
  private static final int CHECK_BOX = 2;
  private static final int DROP_DOWN = 3;
  private static final int NUMBER = 4;
  private static final int NUMBER_SLIDER = 5;
  /*private static final int FEE = 6;*/
  private static final int TEXT_AREA = 7;
  private static final int TEXT_AREA_COMMASEPARATED = 8;
  private static final int DATE_CURRENT_TO_FUTURE = 9;
  private static final int DATE_PAST_TO_CURRENT = 10;
  private static final int TIME = 11;
  private static final int IMAGE_UPLOAD = 12;
  private static final int VIDEO_UPLOAD = 13;
  private Context mContext;
  private ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr> mQuestionArrs;
  private SymptomQuestionsContract.SymptomQuestionsView mSymptomQuestionsView;
  private AppTypeface mAppTypeface;

  public SymptomQuestionAdapter(Context context,
      ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr> responseData,
      SymptomQuestionsContract.SymptomQuestionsView symptomQuestionsView,
      AppTypeface appTypeface) {
    this.mContext = context;
    this.mQuestionArrs = responseData;
    this.mSymptomQuestionsView = symptomQuestionsView;
    this.mAppTypeface = appTypeface;
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {

    View view;
    if (viewType == RADIO_BUTTON) { // for radio layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chk_rdio_button, parent, false);
      return new RadioTypeViewHolder(view);
    } else if (viewType == CHECK_BOX) { // for checkbox layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chk_button, parent, false);
      return new CheckTypeViewHolder(view);
    } else if (viewType == DROP_DOWN) { // for dropdown layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_drop_down, parent, false);
      return new DropDownTypeViewHolder(view);
    } else if (viewType == NUMBER_SLIDER) { // for number_slider layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_drop_down, parent, false);
      return new NumberSiderTypeViewHolder(view);
    } else if (viewType == NUMBER) { // for number layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_text_area, parent, false);
      return new NumberTypeViewHolder(view);
    } else if (viewType == TEXT_AREA) { // for text_area layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_text_area, parent, false);
      return new TextAreaTypeViewHolder(view);
    } else if (viewType == TEXT_AREA_COMMASEPARATED) { // for text_area_commaseparated layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_drop_down, parent, false);
      return new TextAreaCommaSeparatedTypeViewHolder(view);
    } else if (viewType == DATE_CURRENT_TO_FUTURE) { // for date_current_to_future layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chk_button, parent, false);
      return new DateCurrentToFutureViewHolder(view);
    } else if (viewType == DATE_PAST_TO_CURRENT) { // for date_pass_to_current layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chk_button, parent, false);
      return new DatePastToCurrentViewHolder(view);
    } else if (viewType == TIME) { // for time layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chk_button, parent, false);
      return new TimeViewHolder(view);
    } else if (viewType == IMAGE_UPLOAD) { // for image_upload layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_upload, parent, false);
      return new ImageTypeViewHolder(view);
    } else if (viewType == VIDEO_UPLOAD) { // for video_upload layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_upload, parent, false);
      return new VideoTypeViewHolder(view);
    } else {
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_text_area, parent, false);
      return new NullViewHolder(view);
    }
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
    final SymptomResponsePojo.SymptomCategory.QuestionArr questionArr = mQuestionArrs.get(
        position);
    if (getItemViewType(position) == RADIO_BUTTON) {
      ((RadioTypeViewHolder) viewHolder).setRadioTypeData(questionArr);

    } else if (getItemViewType(position) == CHECK_BOX) {
      ((CheckTypeViewHolder) viewHolder).setCheckTypeData(questionArr);

    } else if (getItemViewType(position) == DROP_DOWN) {
      ((DropDownTypeViewHolder) viewHolder).setDropDownTypeData(questionArr);

    } else if (getItemViewType(position) == NUMBER_SLIDER) {
      ((NumberSiderTypeViewHolder) viewHolder).setNumberSiderTypeData(questionArr);

    } else if (getItemViewType(position) == NUMBER) {
      ((NumberTypeViewHolder) viewHolder).setNumberTypeData(questionArr);
      ((NumberTypeViewHolder) viewHolder).etTextDesc.setTag(position);
    } else if (getItemViewType(position) == TEXT_AREA) {
      ((TextAreaTypeViewHolder) viewHolder).setTextAreaTypeData(questionArr);

      ((TextAreaTypeViewHolder) viewHolder).etTextDesc.setTag(position);
    } else if (getItemViewType(position) == TEXT_AREA_COMMASEPARATED) {
      ((TextAreaCommaSeparatedTypeViewHolder) viewHolder).setTextAreaCommaSeparatedTypeData(
          questionArr);

    } else if (getItemViewType(position) == DATE_CURRENT_TO_FUTURE) {

      ((DateCurrentToFutureViewHolder) viewHolder).setDateCurrentToFutureData(questionArr);

    } else if (getItemViewType(position) == DATE_PAST_TO_CURRENT) {
      ((DatePastToCurrentViewHolder) viewHolder).setDatePastToCurrentData(questionArr);

    } else if (getItemViewType(position) == TIME) {
      ((TimeViewHolder) viewHolder).setTimeViewData(questionArr);

    } else if (getItemViewType(position) == IMAGE_UPLOAD) {
      ((ImageTypeViewHolder) viewHolder).setImageTypeData(questionArr, viewHolder);

    } else if (getItemViewType(position) == VIDEO_UPLOAD) {
      ((VideoTypeViewHolder) viewHolder).setVideoTypeData(questionArr, viewHolder);

    } else {
      ((NullViewHolder) viewHolder).setNullData(questionArr);
      ((NullViewHolder) viewHolder).etTextDesc.setTag(position);
    }

  }
    /*@Override
    public long getItemId(int position) {
        return position;
    }*/

  @Override
  public int getItemViewType(int position) {
    if (mQuestionArrs.get(position).getType() == RADIO_BUTTON) {
      return RADIO_BUTTON;
    } else if (mQuestionArrs.get(position).getType() == CHECK_BOX) {
      return CHECK_BOX;
    } else if (mQuestionArrs.get(position).getType() == DROP_DOWN) {
      return DROP_DOWN;
    } else if (mQuestionArrs.get(position).getType() == NUMBER_SLIDER) {
      return NUMBER_SLIDER;
    } else if (mQuestionArrs.get(position).getType() == NUMBER) {
      return NUMBER;
    } else if (mQuestionArrs.get(position).getType() == TEXT_AREA) {
      return TEXT_AREA;
    } else if (mQuestionArrs.get(position).getType() == TEXT_AREA_COMMASEPARATED) {
      return TEXT_AREA_COMMASEPARATED;
    } else if (mQuestionArrs.get(position).getType() == DATE_CURRENT_TO_FUTURE) {
      return DATE_CURRENT_TO_FUTURE;
    } else if (mQuestionArrs.get(position).getType() == DATE_PAST_TO_CURRENT) {
      return DATE_PAST_TO_CURRENT;
    } else if (mQuestionArrs.get(position).getType() == TIME) {
      return TIME;
    } else if (mQuestionArrs.get(position).getType() == IMAGE_UPLOAD) {
      return IMAGE_UPLOAD;
    } else if (mQuestionArrs.get(position).getType() == VIDEO_UPLOAD) {
      return VIDEO_UPLOAD;
    } else {
      return -1;
    }

  }

  @Override
  public int getItemCount() {
    return mQuestionArrs == null ? 0 : mQuestionArrs.size();
  }

  public class RadioTypeViewHolder extends RecyclerView.ViewHolder {

    RadioTypeQuestionAdapter adapter;
    LinearLayoutManager layoutManager;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.tvTitle)
    TextView tvTitle;


    RadioTypeViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
      tvTitle.setTypeface(mAppTypeface.getHind_medium());
      tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
    }

    void setRadioTypeData(SymptomResponsePojo.SymptomCategory.QuestionArr questionArr) {
      tvTitle.setText(questionArr.getName());

      if (questionArr.getPreDefined() != null) {
        mRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerView.setLayoutManager(layoutManager);

        adapter = new RadioTypeQuestionAdapter(mContext, questionArr.getPreDefined(),
            questionArr.get_id(), questionArr.getName(), questionArr.getType());
        // adapter.SetOnItemClickListener(mItemClickListener);
        mRecyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new RadioTypeQuestionAdapter.OnItemClickListener() {
          @Override
          public void onItemClick(View view, int position,
              ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds,
              String questionId, String name, int type) {
            adapter.setItemSelected(position);
            mSymptomQuestionsView.onItemClick(view, position, preDefineds, questionId, name, type,
                questionArr);
          }
        });
        adapter.notifyDataSetChanged();
        // this is needed if you are working with CollapsingToolbarLayout, I am adding this here
        // just in case I forget.
        // mRecyclerView.setNestedScrollingEnabled(false);
        // ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);

        //optional
           /* StartSnapHelper snapHelper = new StartSnapHelper();
            snapHelper.attachToRecyclerView(mRecyclerView);*/
      }
    }

  }

  public class CheckTypeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvSelect)
    TextView tvSelect;
    @BindView(R.id.rvSelected)
    RelativeLayout rvSelected;

    CheckTypeViewHolder(View view) {
      super(view);
      ButterKnife.bind(this,view);
      tvTitle.setTypeface(mAppTypeface.getHind_medium());
      tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvSelect.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
    }

    void setCheckTypeData(SymptomResponsePojo.SymptomCategory.QuestionArr questionArr) {
      tvTitle.setText(questionArr.getName());

      if (questionArr.getChckBoxTxt() != null) {
        tvSelect.setText(questionArr.getChckBoxTxt());
      }

      if (questionArr.getPreDefined() != null) {


        ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds =
            new ArrayList<>();
        preDefineds.clear();
        preDefineds.addAll(questionArr.getPreDefined());

        rvSelected.setOnClickListener(v -> {
          if (mSymptomQuestionsView != null) {
            mSymptomQuestionsView.onItemClickChecboxResult(preDefineds,
                tvSelect, questionArr, questionArr.get_id(), questionArr.getName(),
                questionArr.getType());
          }

        });
      }
    }
  }


  public class DatePastToCurrentViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvSelect)
    TextView tvSelect;
    @BindView(R.id.rvSelected)
    RelativeLayout rvSelected;

    DatePastToCurrentViewHolder(View view) {
      super(view);
      ButterKnife.bind(this,view);
      tvTitle.setTypeface(mAppTypeface.getHind_medium());
      tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvSelect.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));

    }

    void setDatePastToCurrentData(SymptomResponsePojo.SymptomCategory.QuestionArr questionArr) {
      tvTitle.setText(questionArr.getName());

      if (questionArr.getDatePastToCurrentTxt() != null) {
        tvSelect.setText(questionArr.getDatePastToCurrentTxt());
      }

      rvSelected.setOnClickListener(v -> {
        if (mSymptomQuestionsView != null) {
          mSymptomQuestionsView.onItemClickDatePastToCurrentResult(tvSelect, questionArr,
              questionArr.get_id(), questionArr.getName(), questionArr.getType());
        }
      });
    }
  }

  public class TimeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvSelect)
    TextView tvSelect;
    @BindView(R.id.rvSelected)
    RelativeLayout rvSelected;

    TimeViewHolder(View view) {
      super(view);
     ButterKnife.bind(this,view);
      tvTitle.setTypeface(mAppTypeface.getHind_medium());
      tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvSelect.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));

    }

    void setTimeViewData(SymptomResponsePojo.SymptomCategory.QuestionArr questionArr) {
      tvTitle.setText(questionArr.getName());

      if (questionArr.getTimeTxt() != null) {
        tvSelect.setText(questionArr.getTimeTxt());
      }

      rvSelected.setOnClickListener(v -> {

        if (mSymptomQuestionsView != null) {
          mSymptomQuestionsView.onItemClickTimeResult(tvSelect, questionArr, questionArr.get_id(),
              questionArr.getName(), questionArr.getType());
        }

      });
    }
  }


  public class DateCurrentToFutureViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvSelect)
    TextView tvSelect;
    @BindView(R.id.rvSelected)
    RelativeLayout rvSelected;

    DateCurrentToFutureViewHolder(View view) {
      super(view);
     ButterKnife.bind(this,view);
      tvTitle.setTypeface(mAppTypeface.getHind_medium());
      tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvSelect.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));

    }

    void setDateCurrentToFutureData(
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr) {
      tvTitle.setText(questionArr.getName());

      if (questionArr.getDateCurrentToFutureTxt() != null) {
        tvSelect.setText(questionArr.getDateCurrentToFutureTxt());
      }

      rvSelected.setOnClickListener(v -> {

        if (mSymptomQuestionsView != null) {
          mSymptomQuestionsView.onItemClickDateCurrentToFutureResult(tvSelect, questionArr,
              questionArr.get_id(), questionArr.getName(), questionArr.getType());
        }

      });
    }
  }

  public class NumberTypeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.etTextDesc)
    EditText etTextDesc;
    @BindView(R.id.tv_centerCount)
    TextView tv_centerCount;
    @BindView(R.id.tv_center)
    TextView tv_center;

    NumberTypeViewHolder(View view) {
      super(view);
      ButterKnife.bind(this,view);
      tvTitle.setTypeface(mAppTypeface.getHind_medium());
      tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      etTextDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
      tv_center.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_12));
      tv_centerCount.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_12));
      tv_center.setTypeface(mAppTypeface.getHind_medium());
      tv_centerCount.setTypeface(mAppTypeface.getHind_medium());
      tv_centerCount.setVisibility(View.GONE);
      tv_center.setVisibility(View.GONE);
      etTextDesc.setTypeface(mAppTypeface.getHind_regular());
      etTextDesc.setSingleLine(true);
      etTextDesc.setMinLines(1);
      etTextDesc.setMaxLines(1);
      etTextDesc.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL
          | InputType.TYPE_NUMBER_FLAG_SIGNED);
      etTextDesc.setImeOptions(EditorInfo.IME_ACTION_DONE);

      etTextDesc.addTextChangedListener(new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
          if(etTextDesc.getText().length() > 0) {
          mQuestionArrs.get(getAdapterPosition()).setNumberTxt(etTextDesc.getText().toString());

        }
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
      });

    }

    void setNumberTypeData(SymptomResponsePojo.SymptomCategory.QuestionArr questionArr) {
      if (!questionArr.getName().isEmpty()) {
        tvTitle.setText(questionArr.getName());
      }

      /*InputFilter[] FilterArray = new InputFilter[1];
      FilterArray[0] = new InputFilter.LengthFilter(Integer.parseInt(questionArr.getMaximum()));
      etTextDesc.setFilters(FilterArray);
      etTextDesc.setMinLines(Integer.parseInt(questionArr.getMinimum()));
      etTextDesc.setMaxLines(Integer.parseInt(questionArr.getMaximum()));*/

      etTextDesc.setText(questionArr.getNumberTxt());
      if(etTextDesc.getText().toString().trim().isEmpty())
      {
        String size = String.format(Locale.ENGLISH, "%02d", 0);
        tv_centerCount.setText(size+ "/");
      }
      etTextDesc.addTextChangedListener(new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
          if(etTextDesc.getText().length() > 0) {
          if (mSymptomQuestionsView != null) {
            mSymptomQuestionsView.onItemClickNumberResult(etTextDesc.getText().toString().trim(),
                questionArr, questionArr.get_id(), questionArr.getName(), questionArr.getType());
          }

          String size = String.format(Locale.ENGLISH, "%02d", charSequence.length());

          tv_centerCount.setText(size+ "/");

        }  }

        @Override
        public void afterTextChanged(Editable editable) {

        }
      });

      if(!questionArr.getMaximum().toString().isEmpty())
      {
        String max = String.format(Locale.ENGLISH, "%02d", Integer.parseInt(questionArr.getMaximum().toString()));
        tv_center.setText(max);
      }

    }
  }

  public class TextAreaTypeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.etTextDesc)
    EditText etTextDesc;
    @BindView(R.id.tv_centerCount)
    TextView tv_centerCount;
    @BindView(R.id.tv_center)
    TextView tv_center;

    TextAreaTypeViewHolder(View view) {
      super(view);
      ButterKnife.bind(this,view);
      tvTitle.setTypeface(mAppTypeface.getHind_medium());
      tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      etTextDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
      tv_center.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_12));
      tv_centerCount.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_12));
      tv_center.setTypeface(mAppTypeface.getHind_medium());
      tv_centerCount.setTypeface(mAppTypeface.getHind_medium());
     etTextDesc.setTypeface(mAppTypeface.getHind_regular());
      etTextDesc.setSingleLine(false);
      etTextDesc.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
      etTextDesc.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
      etTextDesc.setGravity(Gravity.TOP | Gravity.START);

      etTextDesc.addTextChangedListener(new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
          if(etTextDesc.getText().length() > 0) {
            mQuestionArrs.get(getAdapterPosition()).setData(etTextDesc.getText().toString());
          }
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
      });



    }

    void setTextAreaTypeData(SymptomResponsePojo.SymptomCategory.QuestionArr questionArr) {
      tvTitle.setText(questionArr.getName());
      InputFilter[] FilterArray = new InputFilter[1];
      FilterArray[0] = new InputFilter.LengthFilter(Integer.parseInt(questionArr.getMaximum()));
      etTextDesc.setFilters(FilterArray);
      etTextDesc.setMinLines(Integer.parseInt(questionArr.getMinimum()));
      etTextDesc.setMaxLines(Integer.parseInt(questionArr.getMaximum()));
      //  etTextDesc.setLines(Integer.parseInt(questionArr.getMaximum()));
/*
      etTextDesc.setFilters(
          new InputFilter[]{new MaxLinesInputFilter(Integer.parseInt(questionArr.getMaximum()))});
*/

      etTextDesc.setText(questionArr.getData());
      if(etTextDesc.getText().toString().trim().isEmpty())
      {
        String size = String.format(Locale.ENGLISH, "%02d", 0);
        tv_centerCount.setText(size+ "/");
      }
      etTextDesc.addTextChangedListener(new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
          if(etTextDesc.getText().length() > 0) {
          if (mSymptomQuestionsView != null) {
            mSymptomQuestionsView.onItemClickNumberResult(etTextDesc.getText().toString().trim(),
                questionArr, questionArr.get_id(), questionArr.getName(), questionArr.getType());
          }
          String size = String.format(Locale.ENGLISH, "%02d", charSequence.length());

          tv_centerCount.setText(size+ "/");

        }}

        @Override
        public void afterTextChanged(Editable editable) {

        }
      });

      if(!questionArr.getMaximum().trim().toString().isEmpty()) {
        String max = String.format(Locale.ENGLISH, "%02d",
            Integer.parseInt(questionArr.getMaximum().toString()));
        tv_center.setText(max);
      }
    }
  }

  public class TextAreaCommaSeparatedTypeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvSelect)
    TextView tvSelect;
    @BindView(R.id.rvSelected)
    RelativeLayout rvSelected;

    TextAreaCommaSeparatedTypeViewHolder(View view) {
      super(view);
     ButterKnife.bind(this,view);
      tvTitle.setTypeface(mAppTypeface.getHind_medium());
      tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvSelect.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));

    }

    void setTextAreaCommaSeparatedTypeData(
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr) {
      tvTitle.setText(questionArr.getName());

      if (questionArr.getTextCommaSeparatedTxt() != null) {
        tvSelect.setText(questionArr.getTextCommaSeparatedTxt());
      }


      itemView.setOnClickListener(view -> {
        if (mSymptomQuestionsView != null) {
          mSymptomQuestionsView.onItemClickTextAreaCommaSeparatedResult(tvSelect, questionArr,
              questionArr.get_id(), questionArr.getName(), questionArr.getType());
        }

      });
    }
  }

  public class NumberSiderTypeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.rvSelected)
    RelativeLayout rvSelected;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvSelect)
    TextView tvSelect;

    NumberSiderTypeViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
      tvTitle.setTypeface(mAppTypeface.getHind_medium());
      tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvSelect.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));

    }

    void setNumberSiderTypeData(SymptomResponsePojo.SymptomCategory.QuestionArr questionArr) {
      if (!questionArr.getName().isEmpty()) {
        tvTitle.setText(questionArr.getName());
      }

      if (questionArr.getNumberSliderTxt() != null) {
        tvSelect.setText(questionArr.getNumberSliderTxt());
      }

      itemView.setOnClickListener(view -> {
        if (mSymptomQuestionsView != null) {
          mSymptomQuestionsView.onItemClickNumberSiderResult(questionArr.getMaximum(),
              questionArr.getMinimum(), tvSelect, questionArr.getName(), questionArr,
              questionArr.get_id(), questionArr.getName(), questionArr.getType());
        }

      });

    }
  }


  public class DropDownTypeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.rvSelected)
    RelativeLayout rvSelected;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvSelect)
    TextView tvSelect;

    DropDownTypeViewHolder(View view) {
      super(view);
      ButterKnife.bind(this,view);
      tvTitle.setTypeface(mAppTypeface.getHind_medium());
      tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvSelect.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));

    }

    void setDropDownTypeData(SymptomResponsePojo.SymptomCategory.QuestionArr questionArr) {
      tvTitle.setText(questionArr.getName());
      if (questionArr.getDropDownTxt() != null) {
        tvSelect.setText(questionArr.getDropDownTxt());
      }

      if (questionArr.getPreDefined() != null) {
        ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds =
            new ArrayList<>();
        preDefineds.clear();
        preDefineds.addAll(questionArr.getPreDefined());

        itemView.setOnClickListener(view -> {
          if (mSymptomQuestionsView != null) {
            mSymptomQuestionsView.onItemClickDropDownResult(preDefineds, tvSelect, questionArr,
                questionArr.get_id(), questionArr.getName(), questionArr.getType());
          }

        });
      }
    }
  }

  public class ImageTypeViewHolder extends RecyclerView.ViewHolder {
    ImageTypeAdapterGrid questionAdapterGrid;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvUpload)
    TextView tvUpload;
    @BindView(R.id.recyclerViewQuestions)
    RecyclerView recyclerViewQuestions;

    ImageTypeViewHolder(View view) {
      super(view);
     ButterKnife.bind(this,view);
      tvTitle.setTypeface(mAppTypeface.getHind_medium());
      tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvUpload.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
      tvUpload.setTypeface(mAppTypeface.getHind_regular());
    }

    void setImageTypeData(SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        RecyclerView.ViewHolder viewHolder) {
      tvTitle.setText(questionArr.getName());

      tvUpload.setOnClickListener(v -> {
        if (mSymptomQuestionsView != null) {
          mSymptomQuestionsView.onItemClickImageUploadResult(recyclerViewQuestions,
              questionAdapterGrid, questionArr.getName(), viewHolder, questionArr.getMaximum(),
              questionArr.getMinimum(), questionArr.get_id(), questionArr, questionArr.getName(),
              questionArr.getType());
        }
      });


    }
  }

  public class VideoTypeViewHolder extends RecyclerView.ViewHolder {
    VideoTypeAdapterGrid questionAdapterGrid;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvUpload)
    TextView tvUpload;
    @BindView(R.id.recyclerViewQuestions)
    RecyclerView recyclerViewQuestions;

    VideoTypeViewHolder(View view) {
      super(view);
      ButterKnife.bind(this,view);
      tvTitle.setTypeface(mAppTypeface.getHind_medium());
      tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvUpload.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
      tvUpload.setTypeface(mAppTypeface.getHind_regular());
    }

    void setVideoTypeData(SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        RecyclerView.ViewHolder viewHolder) {
      tvTitle.setText(questionArr.getName());

    /*  if(mQuestionArrs.size() > 0)
      {
        questionAdapterGrid.notifyDataSetChanged();
      }*/

      tvUpload.setOnClickListener(v -> {
        if (mSymptomQuestionsView != null) {
          mSymptomQuestionsView.onItemClickVideoUploadResult(recyclerViewQuestions,
              questionAdapterGrid, questionArr.getName(), viewHolder, questionArr.getMaximum(),
              questionArr.getMinimum(), questionArr.get_id(), questionArr, questionArr.getName(),
              questionArr.getType());
        }
      });


    }
  }


  public class NullViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.etTextDesc)
    EditText etTextDesc;
    @BindView(R.id.tv_centerCount)
    TextView tv_centerCount;
    @BindView(R.id.tv_center)
    TextView tv_center;

    NullViewHolder(View view) {
      super(view);
      ButterKnife.bind(this,view);
      tvTitle.setTypeface(mAppTypeface.getHind_medium());
      tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      etTextDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
      tv_center.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_12));
      tv_centerCount.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_12));
      tv_center.setTypeface(mAppTypeface.getHind_medium());
      tv_centerCount.setTypeface(mAppTypeface.getHind_medium());
      etTextDesc.setSingleLine(true);
      etTextDesc.setMinLines(1);
      etTextDesc.setMaxLines(1);
      //etTextDesc.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_NUMBER_FLAG_DECIMAL |
      // InputType.TYPE_NUMBER_FLAG_SIGNED);
      etTextDesc.setImeOptions(EditorInfo.IME_ACTION_DONE);

      etTextDesc.addTextChangedListener(new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
          if(etTextDesc.getText().length() > 0) {
            mQuestionArrs.get(getAdapterPosition()).setFeeTxt(etTextDesc.getText().toString());
          }
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
      });

    }

    void setNullData(SymptomResponsePojo.SymptomCategory.QuestionArr questionArr) {
      tvTitle.setText(questionArr.getName());

      etTextDesc.setText(questionArr.getFeeTxt());
      if(etTextDesc.getText().toString().trim().isEmpty())
      {
        String size = String.format(Locale.ENGLISH, "%02d", 0);
        tv_centerCount.setText(size+ "/");
      }

      etTextDesc.addTextChangedListener(new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
          if(etTextDesc.getText().length() > 0) {
            if (mSymptomQuestionsView != null) {
              mSymptomQuestionsView.onItemClickNumberResult(etTextDesc.getText().toString().trim(),
                      questionArr, questionArr.get_id(), questionArr.getName(), questionArr.getType());
            }

            String size = String.format(Locale.ENGLISH, "%02d", charSequence.length());

            tv_centerCount.setText(size + "/");
          }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
      });
      if(!questionArr.getMaximum().trim().toString().isEmpty()) {
        String max = String.format(Locale.ENGLISH, "%02d",
            Integer.parseInt(questionArr.getMaximum().toString()));
        tv_center.setText(max);
      }
    }
  }
}


