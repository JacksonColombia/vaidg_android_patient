package com.vaidg.incalloutcalltelecall.symotomquestion;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ali on 6/12/2018.
 */
public class AnsSymptomHashMap implements Serializable {


  @SerializedName("symptomId")
  @Expose
  private String symptomId;
  @SerializedName("symptomName")
  @Expose
  private String symptomName;
  @SerializedName("symptomAnswers")
  @Expose
  private ArrayList<SymptomAnswer> symptomAnswers;

  public AnsSymptomHashMap(String symptomId, String symptomName, ArrayList<SymptomAnswer> symptomAnswers) {
    this.symptomId = symptomId;
    this.symptomName = symptomName;
    this.symptomAnswers = symptomAnswers;
  }

  public String getSymptomId() {
    return symptomId;
  }

  public String getSymptomName() {
    return symptomName;
  }

  public ArrayList<SymptomAnswer> getSymptomAnswers() {
    return symptomAnswers;
  }

}
