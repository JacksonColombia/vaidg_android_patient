package com.vaidg.incalloutcalltelecall.symotomquestion;

import adapters.ImageTypeAdapterGrid;
import adapters.VideoTypeAdapterGrid;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;
import com.vaidg.incalloutcalltelecall.questionselection.model.SymptomResponsePojo;
import java.util.ArrayList;

/**
 * <h2>QuestionSelectionContract</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public interface SymptomQuestionsContract {
  interface SymptomQuestionsView extends BaseView {

    /**
     * <h2>onItemClickDateCurrentToFutureResult</h2>
     * This method is used to set date
     * @param tvSelect  text to be set
     * @param questionArr
     * @param questionId
     * @param name
     * @param type
     */
    void onItemClickDateCurrentToFutureResult(TextView tvSelect,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionId, String name, int type);

    /**
     * <h2>onItemClickDatePastToCurrentResult</h2>
     * This method is used to set date
     * @param tvSelect  text to be set
     * @param questionArr
     * @param questionId
     * @param name
     * @param type
     */
    void onItemClickDatePastToCurrentResult(TextView tvSelect,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionId, String name, int type);

    /**
     * <h2>onItemClickTimeResult</h2>
     * This method is used to set the time selected
     * @param tvSelect  text to be set
     * @param questionArr
     * @param questionId
     * @param name
     * @param type
     */
    void onItemClickTimeResult(TextView tvSelect,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionId, String name, int type);

    /**
     * <h2>onItemClickTextAreaCommaSeparatedResult</h2>
     * This method is used to set the data of text
     * @param tvSelect  text to be set
     * @param questionArr
     * @param questionId
     * @param name
     * @param type
     */
    void onItemClickTextAreaCommaSeparatedResult(TextView tvSelect,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionId, String name, int type);

    /**
     * <h2>onItemClickDropDownResult</h2>
     * This method is used to select the single selection
     * @param preDefineds  list of data for selection
     * @param tvSelect  text to be set
     * @param questionArr
     * @param questionId
     * @param name
     * @param type
     */
    void onItemClickDropDownResult(
        ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds,
        TextView tvSelect,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionId, String name, int type);


    /**
     * <h2>onItemClick</h2>
     * This method is used to select single data from the list
     * @param view  View  of adapter
     * @param layoutPosition position of view
     * @param preDefineds  list of data for selection
     * @param questionId position of list
     * @param name
     * @param type
     * @param questionArr
     */
    void onItemClick(View view, int layoutPosition,
        ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds,
        String questionId, String name, int type,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr);

    /**
     * <h2>onItemClickChecboxResult</h2>
     * This method is used to select the multiple selection
     * @param preDefineds  list of data for selection
     * @param tvSelect  text to be set
     * @param questionArr
     * @param questionId
     * @param name
     * @param type
     */
    void onItemClickChecboxResult(
        ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds,
        TextView tvSelect,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionId, String name, int type);

    /**
     * <h2>onItemClickImageUploadResult</h2>
     * This method is used to set the list of image
     * @param recyclerViewQuestions  key of the value  of image added in hashmap
     * @param questionAdapterGrid  position of the list of image added in list
     * @param name  title of the text
     * @param viewHolder ViewHolder of adapter
     * @param maximum max limit of selection
     * @param minimum min limit of selection
     * @param questionId
     * @param questionArr
     * @param questionArrName
     * @param type
     */
    void onItemClickImageUploadResult(RecyclerView recyclerViewQuestions,
        ImageTypeAdapterGrid questionAdapterGrid, String name, RecyclerView.ViewHolder viewHolder,
        String maximum, String minimum, String questionId,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionArrName, int type);

    /**
     * <h2>onItemClickVideoUploadResult</h2>
     * This method is used to set the list of video
     * @param recyclerViewQuestions Recyclerview
     * @param questionAdapterGrid  Adapter
     * @param name  title of the text
     * @param viewHolder ViewHolder of adapter
     * @param maximum  max limit of selection
     * @param minimum  min limit of selection
     * @param questionId
     * @param questionArr
     * @param questionArrName
     * @param type
     */
    void onItemClickVideoUploadResult(RecyclerView recyclerViewQuestions,
        VideoTypeAdapterGrid questionAdapterGrid, String name, RecyclerView.ViewHolder viewHolder,
        String maximum, String minimum, String questionId,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionArrName, int type);

    /**
     * <h2>onItemClickNumberSiderResult</h2>
     * This method is used to set the range selection
     * @param maximum  max limit of selection
     * @param minimum  min limit of selection
     * @param tvSelect text to be set
     * @param name title of the text
     * @param questionArr
     * @param questionId
     * @param questionArrName
     * @param type
     */
    void onItemClickNumberSiderResult(String maximum, String minimum, TextView tvSelect,
        String name,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionId, String questionArrName, int type);


    /**
     * <h2>onItemClickNumberSiderResult</h2>
     * This method is used to set the range selection
     * @param name title of the text
     * @param questionArr
     * @param questionId
     * @param questionArrName
     * @param type
     */
    void onItemClickNumberResult(
        String name,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionId, String questionArrName, int type);
  }

  interface SymptomQuestionsPresenter extends BasePresenter<SymptomQuestionsView> {


  }
}
