package com.vaidg.incalloutcalltelecall.symotomquestion;

import android.content.Context;
import android.util.Log;
import com.vaidg.incalloutcalltelecall.questionselection.model.SymptomResponsePojo;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.pojo.ErrorHandel;
import com.utility.AlertProgress;
import com.utility.PermissionsManager;
import com.utility.RefreshToken;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * <h2>SymptomQuestionsPresenterImp</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class SymptomQuestionsPresenterImp implements
    SymptomQuestionsContract.SymptomQuestionsPresenter {

  @Inject
  LSPServices lspServices;
  @Inject
  SessionManagerImpl manager;
  @Inject
  Gson gson;
  @Inject
  AppTypeface appTypeface;
  @Inject
  PermissionsManager permissionsManager;
  @Inject
  AlertProgress alertProgress;
  private SymptomQuestionsContract.SymptomQuestionsView symptomView;
  private Context mContext;

  @Inject
  SymptomQuestionsPresenterImp() {

  }


  @Override
  public void attachView(SymptomQuestionsContract.SymptomQuestionsView view) {
    symptomView = view;
  }

  @Override
  public void detachView() {
    symptomView = null;
  }


}