package com.vaidg.incalloutcalltelecall.symotomquestion;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ali on 6/12/2018.
 */
public class SymptomAnswer implements Serializable {

  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("answer")
  @Expose
  private String answer;
  @SerializedName("question")
  @Expose
  private String question;
  @SerializedName("symptomId")
  @Expose
  private String symptomId;
  @SerializedName("symptomName")
  @Expose
  private String symptomName;
  @SerializedName("questionType")
  @Expose
  private int questionType;
  @SerializedName("isMandatory")
  @Expose
  private boolean isMandatory;
  @SerializedName("isChecked")
  @Expose
  private boolean isChecked;

  public SymptomAnswer(String symptomId, String id, String answer, String question, int questionType, String symptomName,boolean isMandatory,boolean isChecked) {
    this.symptomId =symptomId;
    this.id = id;
    this.answer = answer;
    this.question = question;
    this.questionType = questionType;
    this.symptomName = symptomName;
    this.isMandatory = isMandatory;
    this.isChecked = isChecked;
  }

  public boolean isMandatory() {
    return isMandatory;
  }

  public boolean isChecked() {
    return isChecked;
  }

  public String getId() {
    return id;
  }

  public String getAnswer() {
    return answer;
  }

  public String getQuestion() {
    return question;
  }

  public String getSymptomId() {
    return symptomId;
  }

  public String getSymptomName() {
    return symptomName;
  }

  public int getQuestionType() {
    return questionType;
  }
}
