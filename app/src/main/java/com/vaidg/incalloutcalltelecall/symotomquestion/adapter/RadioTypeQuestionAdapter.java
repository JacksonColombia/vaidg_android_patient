package com.vaidg.incalloutcalltelecall.symotomquestion.adapter;


import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.questionselection.model.SymptomResponsePojo;
import com.vaidg.utilities.AppTypeface;
import java.util.ArrayList;


public class RadioTypeQuestionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
  private OnItemClickListener mItemClickListener;
  private Context mContext;
  private String pos = "";
  private String name = "";
  private int type = 1;
  private ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> mPreDefineds =
      new ArrayList<>();

  public RadioTypeQuestionAdapter(Context context,
                                  ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds,
                                  String questionId, String name, int type) {
    this.mContext = context;
    this.mPreDefineds = preDefineds;
    this.pos = questionId;
    this.name = name;
    this.type = type;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup viewGroup, int viewType) {

    View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_predefined,
        viewGroup, false);
    // RadioTypeViewHolder viewHolder = new RadioTypeViewHolder(view);
    // viewHolder.setIsRecyclable(false);
    return new RadioTypeViewHolder(view);

  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

    ((RadioTypeViewHolder) viewHolder).textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
    ((RadioTypeViewHolder) viewHolder).textView.setText(mPreDefineds.get(position).getName());
    ((RadioTypeViewHolder) viewHolder).textView.setTag(position);


    mPreDefineds.get(position).setChecked(mPreDefineds.get(position).isChecked());
    final int sdk = android.os.Build.VERSION.SDK_INT;

    if (mPreDefineds.get(position).isChecked()) {
      ((RadioTypeViewHolder) viewHolder).textView.setTextColor(
          mContext.getResources().getColor(R.color.white));
      if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
        ((RadioTypeViewHolder) viewHolder).textView.setBackgroundDrawable(
            ContextCompat.getDrawable(mContext, R.drawable.border_txtviewselected));
      } else {
        ((RadioTypeViewHolder) viewHolder).textView.setBackground(
            ContextCompat.getDrawable(mContext, R.drawable.border_txtviewselected));
      }
    } else {
      ((RadioTypeViewHolder) viewHolder).textView.setTextColor(
          mContext.getResources().getColor(R.color.grey));
      if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
        ((RadioTypeViewHolder) viewHolder).textView.setBackgroundDrawable(
            ContextCompat.getDrawable(mContext, R.drawable.border_txtviewunselect));
      } else {
        ((RadioTypeViewHolder) viewHolder).textView.setBackground(
            ContextCompat.getDrawable(mContext, R.drawable.border_txtviewunselect));
      }
    }
  }

  @Override
  public int getItemCount() {
    return mPreDefineds == null ? 0 : mPreDefineds.size();
  }

  public void setItemSelected(int position) {
    if (position != -1) {
      mPreDefineds.get(position).setChecked(!mPreDefineds.get(position).isChecked());
      notifyDataSetChanged();
    }
  }

  public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
    this.mItemClickListener = mItemClickListener;
  }


  public interface OnItemClickListener {

    void onItemClick(View view, int position,
                     ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> mPreDefineds,
                     String questionId, String name, int type);
  }

  private class RadioTypeViewHolder extends RecyclerView.ViewHolder implements
      View.OnLongClickListener,
      View.OnClickListener {
    private TextView textView;
    private RelativeLayout rvYes;
    private AppTypeface appTypeface;

    public RadioTypeViewHolder(View itemView) {
      super(itemView);
      textView = itemView.findViewById(R.id.tvYes);
      rvYes = itemView.findViewById(R.id.rvYes);
      appTypeface = AppTypeface.getInstance(mContext);
      textView.setTypeface(appTypeface.getHind_regular());
      itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
      for (int i = 0; i < mPreDefineds.size(); i++) {
        mPreDefineds.get(i).setChecked(false);
      }
      if (mItemClickListener != null) {
        mItemClickListener.onItemClick(view, getAdapterPosition(), mPreDefineds, pos, name, type);
      }
    }

    @Override
    public boolean onLongClick(View view) {

      return false;
    }
  }


}

