package com.vaidg.incalloutcalltelecall.symotomquestion;

import static com.vaidg.utilities.Constants.COUNT;
import static com.vaidg.utilities.Constants.POS;
import static com.vaidg.utilities.Constants.responseData;

import adapters.ImageTypeAdapterGrid;
import adapters.VideoTypeAdapterGrid;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.questionselection.model.SymptomResponsePojo;
import com.vaidg.incalloutcalltelecall.symotomquestion.adapter.SymptomQuestionAdapter;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.SessionManager;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.utility.AlertProgress;
import dagger.android.support.DaggerFragment;
import java.util.ArrayList;
import javax.inject.Inject;

/**
 * <h2>YourAddressFragment</h2>
 * <p>A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link SymptomQuestionFragment} factory method to
 * create an instance of this fragment.<p/>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class SymptomQuestionFragment extends DaggerFragment implements View.OnClickListener,
    SymptomQuestionsContract.SymptomQuestionsView {

  Context mContext;
  @Inject
  SessionManager mSessionManager;
  @Inject
  AlertProgress mAlertProgress;
  @BindView(R.id.tv_center)
  TextView tv_center;
  @Inject
  AppTypeface mAppTypeface;
  @BindView(R.id.recyclerviewSearch)
  RecyclerView recyclerviewSearch;
  SymptomQuestionAdapter mSymptomQuestionAdapter;
  @Inject
  SymptomQuestionsContract.SymptomQuestionsPresenter mSymptomQuestionsPresenter;
  OnSypmtomPickedListenerCallBack mOnSypmtomPickedListenerCallBack;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_symptomquestion, container, false);
    ButterKnife.bind(this, view);
    initializeView();
    try {
      mOnSypmtomPickedListenerCallBack = (OnSypmtomPickedListenerCallBack) mContext;
    } catch (ClassCastException ex) {
      //.. should log the error or throw and exception
      Log.e("MyAdapter", "Must implement the CallbackInterface in the Activity", ex);
    }
    return view;
  }

  /**
   * <h2>initializeView</h2>
   *
   * <p>Intiliazing view elements</p>
   */
  private void initializeView() {
    tv_center.setTypeface(mAppTypeface.getHind_semiBold());
    tv_center.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_16));
    if (POS >= 0) {
      tv_center.setText(POS + 1 + "-" + COUNT + ". " + responseData.get(POS).getTitle());

      recyclerviewSearch.setHasFixedSize(true);
      LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
      recyclerviewSearch.setLayoutManager(linearLayoutManager);
      recyclerviewSearch.addItemDecoration(
              new DividerItemDecoration(mContext, LinearLayoutManager.VERTICAL));
      mSymptomQuestionAdapter = new SymptomQuestionAdapter(getActivity(),
              responseData.get(POS).getQuestionArr(), this, mAppTypeface);
      recyclerviewSearch.setAdapter(mSymptomQuestionAdapter);

      recyclerviewSearch.setOnTouchListener((v, event) -> {

        Utility.hideKeyboard(getActivity());
        return false;
      });
    }

  }

  @Override
  public void onClick(View view) {

  }

  @Override
  public void onItemClick(View view, int layoutPosition,
      ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds,
      String questionId, String name, int questiontype,
      SymptomResponsePojo.SymptomCategory.QuestionArr questionArr) {
    mOnSypmtomPickedListenerCallBack.onItemClick(view, layoutPosition, preDefineds, questionId,
        name, questiontype, questionArr);
  }

  @Override
  public void onItemClickChecboxResult(
      ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds,
      TextView tvSelect,
      SymptomResponsePojo.SymptomCategory.QuestionArr questionArr, String questionId,
      String name, int questiontype) {
    mOnSypmtomPickedListenerCallBack.onItemClickChecboxResult(preDefineds, tvSelect, questionArr,
        questionId, name, questiontype);
  }

  @Override
  public void onItemClickImageUploadResult(RecyclerView recyclerViewQuestions,
      ImageTypeAdapterGrid questionAdapterGrid, String name, RecyclerView.ViewHolder viewHolder,
      String maximum, String minimum, String questionId,
      SymptomResponsePojo.SymptomCategory.QuestionArr questionArr, String questionname,
      int questiontype) {
    mOnSypmtomPickedListenerCallBack.onItemClickImageUploadResult(recyclerViewQuestions,
        questionAdapterGrid, name,
        viewHolder, maximum, minimum, questionId, questionArr, questionname, questiontype);
  }

  @Override
  public void onItemClickDateCurrentToFutureResult(TextView tvSelect,
      SymptomResponsePojo.SymptomCategory.QuestionArr questionArr, String questionId,
      String name, int questiontype) {
    mOnSypmtomPickedListenerCallBack.onItemClickDateCurrentToFutureResult(tvSelect, questionArr,
        questionId, name, questiontype);

  }

  @Override
  public void onItemClickDatePastToCurrentResult(TextView tvSelect,
      SymptomResponsePojo.SymptomCategory.QuestionArr questionArr, String questionId,
      String name, int questiontype) {
    mOnSypmtomPickedListenerCallBack.onItemClickDatePastToCurrentResult(tvSelect, questionArr,
        questionId, name, questiontype);

  }

  @Override
  public void onItemClickTimeResult(TextView tvSelect,
      SymptomResponsePojo.SymptomCategory.QuestionArr questionArr, String questionId,
      String name, int questiontype) {
    mOnSypmtomPickedListenerCallBack.onItemClickTimeResult(tvSelect, questionArr, questionId, name,
        questiontype);
  }

  @Override
  public void onItemClickTextAreaCommaSeparatedResult(TextView tvSelect,
      SymptomResponsePojo.SymptomCategory.QuestionArr questionArr, String questionId,
      String name, int questiontype) {
    mOnSypmtomPickedListenerCallBack.onItemClickTextAreaCommaSeparatedResult(tvSelect, questionArr,
        questionId, name, questiontype);

  }

  @Override
  public void onItemClickDropDownResult(
      ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds,
      TextView tvSelect,
      SymptomResponsePojo.SymptomCategory.QuestionArr questionArr, String questionId,
      String name, int questiontype) {
    mOnSypmtomPickedListenerCallBack.onItemClickDropDownResult(preDefineds, tvSelect, questionArr,
        questionId, name, questiontype);

  }

  @Override
  public void onItemClickVideoUploadResult(RecyclerView recyclerViewQuestions,
      VideoTypeAdapterGrid questionAdapterGrid, String name, RecyclerView.ViewHolder viewHolder,
      String maximum, String minimum, String questionId,
      SymptomResponsePojo.SymptomCategory.QuestionArr questionArr, String questionname,
      int questiontype) {
    mOnSypmtomPickedListenerCallBack.onItemClickVideoUploadResult(recyclerViewQuestions,
        questionAdapterGrid, name,
        viewHolder, maximum, minimum, questionId, questionAdapterGrid, questionArr, questionname,
        questiontype);

  }

  @Override
  public void onItemClickNumberSiderResult(String maximum, String minimum, TextView tvSelect,
      String name, SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
      String questionId, String questionname, int questiontype) {
    mOnSypmtomPickedListenerCallBack.onItemClickNumberSiderResult(maximum, minimum, tvSelect, name,
        questionArr, questionId, questionname, questiontype);
  }

  @Override
  public void onItemClickNumberResult(
      String name, SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
      String questionId, String questionname, int questiontype) {
    mOnSypmtomPickedListenerCallBack.onItemClickNumberResult(name, questionArr, questionId,
        questionname, questiontype);
  }


  @Override
  public void onSessionExpired() {

  }

  @Override
  public void onLogout(String message, SessionManagerImpl sessionManager) {
    mAlertProgress.alertPositiveOnclick(mContext, message, getString(R.string.logout),
        getString(R.string.ok), isClicked -> Utility.setMAnagerWithBID(mContext, mSessionManager));

  }

  @Override
  public void onError(String error) {

  }

  @Override
  public void onConnectionError(String connectionError) {

  }

  @Override
  public void onShowProgress() {

  }

  @Override
  public void onHideProgress() {

  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    mSymptomQuestionsPresenter.attachView(this);
    this.mContext = context;
  }

  @Override
  public void onDetach() {
    mSymptomQuestionsPresenter.detachView();
    this.mContext = null;
    super.onDetach();
  }

  /**
   * <h2>OnSymptomSelected</h2>
   *
   * <p>
   * An interface containing method signature.
   * Container Activity must implement this interface.
   * </p>
   */
  public interface OnSypmtomPickedListenerCallBack {
    /**
     * <h2>onItemClickDateCurrentToFutureResult</h2>
     * This method is used to set date
     *
     * @param tvSelect text to be set
     */
    void onItemClickDateCurrentToFutureResult(TextView tvSelect,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionId, String questionname, int questiontype);

    /**
     * <h2>onItemClickDatePastToCurrentResult</h2>
     * This method is used to set date
     *
     * @param tvSelect text to be set
     */
    void onItemClickDatePastToCurrentResult(TextView tvSelect,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionId, String questionname, int questiontype);

    /**
     * <h2>onItemClickTimeResult</h2>
     * This method is used to set the time selected
     *
     * @param tvSelect text to be set
     */
    void onItemClickTimeResult(TextView tvSelect,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionId, String questionname, int questiontype);

    /**
     * <h2>onItemClickTextAreaCommaSeparatedResult</h2>
     * This method is used to set the data of text
     *
     * @param tvSelect text to be set
     */
    void onItemClickTextAreaCommaSeparatedResult(TextView tvSelect,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionId, String questionname, int questiontype);

    /**
     * <h2>onItemClickDropDownResult</h2>
     * This method is used to select the single selection
     *
     * @param preDefineds list of data for selection
     * @param tvSelect    text to be set
     */
    void onItemClickDropDownResult(
        ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds,
        TextView tvSelect,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionId, String questionname, int questiontype);


    /**
     * <h2>onItemClick</h2>
     * This method is used to select single data from the list
     *
     * @param view           View  of adapter
     * @param layoutPosition position of view
     * @param preDefineds    list of data for selection
     * @param questionId     position of list
     */
    void onItemClick(View view, int layoutPosition,
        ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds,
        String questionId, String name, int questiontype,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr);

    /**
     * <h2>onItemClickChecboxResult</h2>
     * This method is used to select the multiple selection
     *
     * @param preDefineds list of data for selection
     * @param tvSelect    text to be set
     */
    void onItemClickChecboxResult(
        ArrayList<SymptomResponsePojo.SymptomCategory.QuestionArr.PreDefined> preDefineds,
        TextView tvSelect,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionId, String questionname, int questiontype);

    /**
     * <h2>onItemClickImageUploadResult</h2>
     * This method is used to set the list of image
     *
     * @param recyclerViewQuestions key of the value  of image added in hashmap
     * @param questionAdapterGrid   position of the list of image added in list
     * @param name                  title of the text
     * @param viewHolder            ViewHolder of adapter
     * @param maximum               max limit of selection
     * @param minimum               min limit of selection
     */
    void onItemClickImageUploadResult(RecyclerView recyclerViewQuestions,
        ImageTypeAdapterGrid questionAdapterGrid, String name, RecyclerView.ViewHolder viewHolder,
        String maximum, String minimum, String questionId,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionname, int questiontype);

    /**
     * <h2>onItemClickVideoUploadResult</h2>
     * This method is used to set the list of video
     *
     * @param recyclerViewQuestions Recyclerview
     * @param questionAdapterGrid   Adapter
     * @param name                  title of the text
     * @param viewHolder            ViewHolder of adapter
     * @param maximum               max limit of selection
     * @param minimum               min limit of selection
     */
    void onItemClickVideoUploadResult(RecyclerView recyclerViewQuestions,
        VideoTypeAdapterGrid questionAdapterGrid, String name, RecyclerView.ViewHolder viewHolder,
        String maximum, String minimum, String questionId,
        VideoTypeAdapterGrid adapterGrid,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionname, int questiontype);

    /**
     * <h2>onItemClickNumberSiderResult</h2>
     * This method is used to set the range selection
     *
     * @param maximum  max limit of selection
     * @param minimum  min limit of selection
     * @param tvSelect text to be set
     * @param name     title of the text
     */
    void onItemClickNumberSiderResult(String maximum, String minimum, TextView tvSelect,
        String name,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionId, String questionname, int questiontype);

    /* This method is used to set the range selection
     *  @param maximum  max limit of selection
     * @param minimum  min limit of selection
     * @param tvSelect text to be set
     * @param name     title of the text
     * @param questionId
     */
    void onItemClickNumberResult(
        String name,
        SymptomResponsePojo.SymptomCategory.QuestionArr questionArr,
        String questionId, String questionname, int questiontype);

  }
}
