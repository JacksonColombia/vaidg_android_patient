package com.vaidg.incalloutcalltelecall.incalloutcalltellcall;


import static com.vaidg.utilities.Constants.POSCOUNT;
import static com.vaidg.utilities.Constants.bookingType;
import static com.vaidg.utilities.Constants.callType;
import static com.vaidg.utilities.Constants.callTypeInOutTele;

import android.content.Context;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.gridlayout.widget.GridLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.vaidg.R;
import com.vaidg.model.CallType;
import com.vaidg.utilities.AppTypeface;

/**
 * <h2>InCallOutCallFragment</h2>
 *
 * <p>A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link InCallOutCallFragment} factory method to
 * create an instance of this fragment.<p/>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class InCallOutCallFragment extends Fragment {

  private static final String TAG = InCallOutCallFragment.class.getName();
  @BindView(R.id.tvBookingTypeWhenYOu)
  TextView tvBookingTypeWhenYOu;
  @BindView(R.id.glInCall)
  GridLayout glInCall;
  @BindView(R.id.ivInCallCheck)
  ImageView ivInCallCheck;
  @BindView(R.id.tvInCall)
  TextView tvInCall;
  @BindView(R.id.tvInCallDesc)
  TextView tvInCallDesc;
  @BindView(R.id.veInCallDivider)
  View veInCallDivider;
  @BindView(R.id.glOutCall)
  GridLayout glOutCall;
  @BindView(R.id.ivOutCallCheck)
  ImageView ivOutCallCheck;
  @BindView(R.id.tvOutCall)
  TextView tvOutCall;
  @BindView(R.id.tvOutCallDesc)
  TextView tvOutCallDesc;
  @BindView(R.id.veOutCallDivider)
  View veOutCallDivider;
  @BindView(R.id.glTeleCall)
  GridLayout glTeleCall;
  @BindView(R.id.ivTeleCallCheck)
  ImageView ivTeleCallCheck;
  @BindView(R.id.tvTeleCall)
  TextView tvTeleCall;
  @BindView(R.id.tvTeleCallDesc)
  TextView tvTeleCallDesc;
  private AppTypeface mAppTypeface;
  private Context mContext;

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.activity_in_call_out_call, container, false);
    ButterKnife.bind(this, view);
    // Fragment locked in portrait screen orientation
    initializeTypeFace();
    return view;
  }

  private void initializeTypeFace() {
    mAppTypeface = AppTypeface.getInstance(mContext);
    tvBookingTypeWhenYOu.setTypeface(mAppTypeface.getHind_medium());
    tvInCall.setTypeface(mAppTypeface.getHind_medium());
    tvInCallDesc.setTypeface(mAppTypeface.getHind_regular());
    tvOutCall.setTypeface(mAppTypeface.getHind_medium());
    tvOutCallDesc.setTypeface(mAppTypeface.getHind_regular());
    tvTeleCall.setTypeface(mAppTypeface.getHind_medium());
    tvTeleCallDesc.setTypeface(mAppTypeface.getHind_regular());

    tvBookingTypeWhenYOu.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_17));
    tvBookingTypeWhenYOu.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_17));
    tvInCall.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_16));
    tvOutCall.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_16));
    tvTeleCall.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_16));
    tvInCallDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_13));
    tvOutCallDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_13));
    tvTeleCallDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_13));

    CallType mCallType = callType;

    if (!mCallType.isIncall() && mCallType.isOutcall() && mCallType.isTelecall()) {
      veOutCallDivider.setVisibility(View.VISIBLE);
      glOutCall.setVisibility(View.VISIBLE);
      glTeleCall.setVisibility(View.VISIBLE);
      outCall();
    } else if (mCallType.isIncall() && !mCallType.isOutcall() && mCallType.isTelecall()) {
      veOutCallDivider.setVisibility(View.VISIBLE);
      glInCall.setVisibility(View.VISIBLE);
      glTeleCall.setVisibility(View.VISIBLE);
      inCall();
    } else if (mCallType.isIncall() && mCallType.isOutcall() && !mCallType.isTelecall()) {
      veInCallDivider.setVisibility(View.VISIBLE);
      glOutCall.setVisibility(View.VISIBLE);
      glInCall.setVisibility(View.VISIBLE);
      inCall();
    } else {
      glOutCall.setVisibility(View.VISIBLE);
      glInCall.setVisibility(View.VISIBLE);
      glTeleCall.setVisibility(View.VISIBLE);
      veInCallDivider.setVisibility(View.VISIBLE);
      veOutCallDivider.setVisibility(View.VISIBLE);
      inCall();
    }
  }


  @OnClick({R.id.glInCall, R.id.glOutCall, R.id.glTeleCall})
  public void onClickGrid(View view) {
    switch (view.getId()) {
      case R.id.glInCall:
        inCall();
        break;
      case R.id.glOutCall:
        outCall();
        break;
      case R.id.glTeleCall:
        teleCall();
        break;
    }
  }

  private void inCall() {
    callTypeInOutTele = 1;
    bookingType = 2;
    POSCOUNT = 4;

    ivInCallCheck.setSelected(true);
    tvInCall.setSelected(true);
    ivOutCallCheck.setSelected(false);
    tvOutCall.setSelected(false);
    ivTeleCallCheck.setSelected(false);
    tvTeleCall.setSelected(false);
    ivInCallCheck.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_check));
    ivOutCallCheck.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_unselected));
    ivTeleCallCheck.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_unselected));
    tvInCall.setTextColor(ContextCompat.getColor(mContext, R.color.colorAccent));
    tvOutCall.setTextColor(ContextCompat.getColor(mContext, R.color.black));
    tvTeleCall.setTextColor(ContextCompat.getColor(mContext, R.color.black));
  }

  private void outCall() {
    callTypeInOutTele = 2;
    bookingType = 1;
    POSCOUNT = 5;

    ivInCallCheck.setSelected(false);
    tvInCall.setSelected(false);
    ivOutCallCheck.setSelected(true);
    tvOutCall.setSelected(true);
    ivTeleCallCheck.setSelected(false);
    tvTeleCall.setSelected(false);
    ivOutCallCheck.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_check));
    ivInCallCheck.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_unselected));
    ivTeleCallCheck.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_unselected));
    tvInCall.setTextColor(ContextCompat.getColor(mContext, R.color.black));
    tvOutCall.setTextColor(ContextCompat.getColor(mContext, R.color.colorAccent));
    tvTeleCall.setTextColor(ContextCompat.getColor(mContext, R.color.black));
  }

  private void teleCall() {
    callTypeInOutTele = 3;
    bookingType = 2;
    POSCOUNT = 4;

    ivInCallCheck.setSelected(false);
    tvInCall.setSelected(false);
    ivOutCallCheck.setSelected(false);
    tvOutCall.setSelected(false);
    ivTeleCallCheck.setSelected(true);
    tvTeleCall.setSelected(true);
    ivTeleCallCheck.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_check));
    ivInCallCheck.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_unselected));
    ivOutCallCheck.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_unselected));
    tvInCall.setTextColor(ContextCompat.getColor(mContext, R.color.black));
    tvOutCall.setTextColor(ContextCompat.getColor(mContext, R.color.black));
    tvTeleCall.setTextColor(ContextCompat.getColor(mContext, R.color.colorAccent));
  }

  @Override
  public void onAttach(@NonNull Context context) {
    super.onAttach(context);
    this.mContext = context;
  }

  @Override
  public void onDetach() {
    mContext = null;
    super.onDetach();
  }
}
