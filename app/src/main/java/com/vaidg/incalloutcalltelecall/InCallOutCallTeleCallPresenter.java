package com.vaidg.incalloutcalltelecall;

import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.SESSION_EXPIRED;
import static com.vaidg.utilities.Constants.SESSION_LOGOUT;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;

import android.content.Context;
import com.vaidg.incalloutcalltelecall.questionselection.model.SymptomResponsePojo;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.google.gson.Gson;
import com.pojo.ErrorHandel;
import com.utility.AlertProgress;
import com.utility.PermissionsManager;
import com.utility.RefreshToken;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * <h2>InCallOutCallTeleCallPresenter</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class InCallOutCallTeleCallPresenter implements
    InCallOutCallTeleCallContract.InCallOutCallTeleCallPresenter {
  private static String device_id = "";

  @Inject
  InCallOutCallTeleCallContract.InCallOutCallTeleCallView view;

  /*  @Inject
    InCallOutCallTeleCallActivity mContext;*/
  @Inject
  SessionManagerImpl manager;
  @Inject
  Gson gson;
  @Inject
  AppTypeface appTypeface;
  @Inject
  PermissionsManager permissionsManager;
  @Inject
  AlertProgress alertProgress;
  @Inject
  AlertProgress aProgress;
  @Inject
  LSPServices mLSPServices;
  private Calendar calendar;
  private TimeZone timeZone;
  private boolean isScheduled;
  private Context mContext;

  @Inject
  InCallOutCallTeleCallPresenter() {

  }

  @Override
  public void attachView(Object view) {

  }

  @Override
  public void detachView() {

  }

  @Override
  public void onNowLaterSelected(boolean isNow, long selectedScheduledDateTime,
      int selectedDuration) {

    if (isNow) {
      Constants.bookingType = 1;
      Constants.scheduledDate = "";
      Constants.scheduledTime = selectedDuration;
      Constants.onRepeatEnd = 0;
      Constants.onRepeatDays = new ArrayList<>();

    } else {

      Constants.bookingType = 2;
      Constants.scheduledDate = selectedScheduledDateTime + "";
      Constants.scheduledTime = selectedDuration;
      Constants.onRepeatEnd = 0;
      Constants.onRepeatDays = new ArrayList<>();
    }
  }

  @Override
  public void onRepeatSelected(long selectedScheduledDateTime, long selectedEndDate,
      int selectedDuration, ArrayList<String> repeatBooking) {
    Constants.bookingType = 3;
    Constants.scheduledDate = selectedScheduledDateTime + "";
    Constants.scheduledTime = selectedDuration;
    Constants.onRepeatEnd = selectedEndDate;
    Constants.onRepeatDays = repeatBooking;
  }

  @Override
  public void openGetSymptoms(Context context, String auth, String symptomSelectedID) {

    mContext = context;
    //  lspServices = ServiceFactory.createRetrofitService(LSPServices.class);
    Observable<Response<ResponseBody>> response = mLSPServices.onToGesymptomsQuestions(auth,
        Constants.selLang, Constants.PLATFORM_ANDROID, symptomSelectedID);
    response.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {


          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> value) {

            int code = value.code();
            JSONObject errJsonD;
            try {
              String response = value.body() != null ? value.body().string() : null;
              String errorBody = value.errorBody() != null ? value.errorBody().string() : null;
              switch (code) {
                case SUCCESS_RESPONSE:
                  if (response!= null && !response.isEmpty()) {
                    SymptomResponsePojo responsePojo = gson.fromJson(response,
                        SymptomResponsePojo.class);
                    if (responsePojo.getData() != null && responsePojo.getData().size() > 0) {
                      if(view != null)
                      view.onResponseSuccess(responsePojo.getData(),
                          symptomSelectedID);
                    }else{
                      if(view != null)
                      view.redirectTodoctorList();
                    }
                    if(view != null)
                    view.onHideProgress();
                  }
                  break;
                case SESSION_LOGOUT:
                  if (errorBody!= null && !errorBody.isEmpty()) {
                    errJsonD = new JSONObject(errorBody);
                    errJsonD.getString(MESSAGE);
                    if (!errJsonD.getString(
                                            MESSAGE).isEmpty()) {
                      if(view != null)
                      view.onError(errJsonD.getString(MESSAGE));
                    }
                    if(view != null)
                    view.onHideProgress();
                  }
                  break;
                case SESSION_EXPIRED:
                  if(errorBody != null && !errorBody.isEmpty()) {
                    ErrorHandel errorHandel = gson.fromJson(response, ErrorHandel.class);
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(),
                        mLSPServices,
                        new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {

                            LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                            openGetSymptoms(context, LSPApplication.getInstance().getAuthToken(manager.getSID()), symptomSelectedID);

                          }

                          @Override
                          public void onFailureRefreshToken() {

                          }

                          @Override
                          public void sessionExpired(String msg) {
                            if(view != null) {
                              view.onHideProgress();
                              view.onLogout(msg, manager);
                            }
                          }
                        });
                  }
                  break;
                default:
                  if (errorBody!= null && !errorBody.isEmpty()) {
                    errJsonD = new JSONObject(errorBody);
                    if (errJsonD.getString(MESSAGE) != null && !errJsonD.getString(
                        MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onHideProgress();
                        view.onError(errJsonD.getString(MESSAGE));
                      }
                    }
                  }
                  break;
              }
            } catch (IOException | JSONException | NumberFormatException e) {
              if(view != null)
                view.onHideProgress();
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null)
              view.onHideProgress();
          }

          @Override
          public void onComplete() {
          }
        });

  }


}


