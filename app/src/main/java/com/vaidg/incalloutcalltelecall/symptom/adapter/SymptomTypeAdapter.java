package com.vaidg.incalloutcalltelecall.symptom.adapter;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import eu.janmuller.android.simplecropimage.Util;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.symptom.SymptomSearchFilter;
import com.vaidg.incalloutcalltelecall.symptom.model.GetSymptom;
import com.vaidg.incalloutcalltelecall.symptom.model.Symptom;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.Utility;
import com.utility.PicassoTrustAll;

import java.util.ArrayList;

import static com.vaidg.utilities.Constants.W_10;
import static com.vaidg.utilities.Constants.W_100;
import static com.vaidg.utilities.Constants.W_320;
import static com.vaidg.utilities.Constants.searchSymptomSelectedID;
import static com.vaidg.utilities.Constants.symptom;
import static com.vaidg.utilities.Constants.symptomId;

public class SymptomTypeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>/* implements Filterable*/ {

  private Context mContext;
  private ArrayList<Symptom> symptomResponseData = new ArrayList<>();
  private ArrayList<Symptom> mFilteredList = new ArrayList<>();
  private ArrayList<String> stringArrayList = new ArrayList<>();
  private OnItemClickListener mItemClickListener;
  private boolean isFromSearch = false;
  private boolean isFromSearchFilter = false;
  private int w100;

  public SymptomTypeAdapter(
          ArrayList<Symptom> symptoms, ArrayList<String> stringArrayList, boolean isFromSearch, boolean isFromSearchFilter) {
    this.symptomResponseData = symptoms;
    this.stringArrayList = stringArrayList;
    this.isFromSearch = isFromSearch;
    this.isFromSearchFilter = isFromSearchFilter;
    calculate();
  }

  private void calculate() {
    w100 = Utility.getScreenWidth()* W_100 / W_320;
  }


  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
    View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_symptomlist, viewGroup, false);
    mContext = viewGroup.getContext();
    return new SymptomViewHolder(view);
  }

  @Override
  public int getItemViewType(int position) {
    return super.getItemViewType(position);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
    //setHasStableIds(true);
    if(!isFromSearch)
    {
      ((SymptomViewHolder) viewHolder).tvServiceName.getLayoutParams().width = w100;
    }

    ((SymptomViewHolder) viewHolder).tvServiceName.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
    ((SymptomViewHolder) viewHolder).bind(symptomResponseData, symptomResponseData.get(position),
        position, stringArrayList);

  }

  //Inside the Adapter class
  @Override
  public long getItemId(int position) {
    try {
      return Long.parseLong(symptomResponseData.get(position).getId());
    } catch (NumberFormatException e) {
      System.out.println("NumberFormatException: " + e.getMessage());
    }
    return position;
  }

  @Override
  public int getItemCount() {
    return symptomResponseData == null ? 0 : symptomResponseData.size();
  }

  public void setItemSelected(int position) {
    if (position != -1) {
      symptomResponseData.get(position).setChecked(symptomResponseData.get(position).isChecked());
      notifyDataSetChanged();
    }
  }

  public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
    this.mItemClickListener = mItemClickListener;
  }

  // method to access in activity after updating selection
  public ArrayList<Symptom> getFilterPreDefinedsList() {
    return symptomResponseData;
  }

  public interface OnItemClickListener {

    void onItemClick(View view, int position,
                     ArrayList<Symptom> symptoms, ArrayList<String> stringArrayList);
  }

/*
  @Override
  public Filter getFilter() {

    return new Filter() {
      @Override
      protected FilterResults performFiltering(CharSequence charSequence) {

        String charString = charSequence.toString();

        if (charString.isEmpty()) {

          mFilteredList = symptomResponseData;
        } else {

          ArrayList<SymptomResponsePojoOne.Symptom> filteredList = new ArrayList<>();

          for (SymptomResponsePojoOne.Symptom androidVersion : symptomResponseData) {

            if (androidVersion.getTitle().toLowerCase().contains(charString)) {

              filteredList.add(androidVersion);
            }
          }

          mFilteredList = symptomResponseData;
        }

        FilterResults filterResults = new FilterResults();
        filterResults.values = mFilteredList;
        return filterResults;
      }

      @Override
      protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
        symptomResponseData = (ArrayList<SymptomResponsePojoOne.Symptom>) filterResults.values;
        notifyDataSetChanged();
      }
    };
  }
*/


  class SymptomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.tvServiceName)
    TextView tvServiceName;
    @BindView(R.id.ivServicePic)
    ImageView ivServicePic;
    AppTypeface mAppTypeface;


    SymptomViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      mAppTypeface = AppTypeface.getInstance(mContext);
      itemView.setOnClickListener(this);
      tvServiceName.setTypeface(mAppTypeface.getHind_medium());
    }


    public void bind(
            ArrayList<Symptom> responseData,
            Symptom symptomResponseData, int position, ArrayList<String> stringArrayList) {
      tvServiceName.setText(symptomResponseData.getTitle());

      if(isFromSearchFilter) {
        if (stringArrayList.size() > 0) {
          for (int i = 0; i < responseData.size(); i++) {
            if (i == getAdapterPosition()) {
              if (stringArrayList.size() > 0) {
                for (int j = 0; j < stringArrayList.size(); j++) {
                  if (stringArrayList.get(j).equals(responseData.get(getAdapterPosition()).getId())) {
                    responseData.get(i).setChecked(true);
                  } else {
                    responseData.get(i).setChecked(false);
                  }
                }
              }
            }
          }
        }else{
          responseData.get(getAdapterPosition()).setChecked(false);
        }
      }

      if(stringArrayList.size() == 0)
      {
        if(searchSymptomSelectedID != null &&  !searchSymptomSelectedID.isEmpty())
        {
          if(searchSymptomSelectedID.equals(responseData.get(position).getId())) {
            responseData.get(position).setChecked(true);
            stringArrayList.add(responseData.get(position).getId());
          }
        }else
        responseData.get(getAdapterPosition()).setChecked(false);
      }
      selectItem(symptomResponseData);

    }

    private void selectItem(Symptom symptomResponseData) {
      if (symptomResponseData.isChecked()) {
        tvServiceName.setTextColor(Utility.getColor(mContext, R.color.colorAccent));
        if (symptomResponseData.getIcon() != null && !symptomResponseData.getIcon().isEmpty()) {
          PicassoTrustAll.getInstance(mContext)
                  .load(symptomResponseData.getIcon())
                  .placeholder(R.drawable.ic_fever_placeholder)   // optional
                  .error(R.drawable.ic_fever_placeholder)      // optional
                  .transform(new PicassoCircleTransform())
                  .into(ivServicePic);

        } else {
          ivServicePic.setImageDrawable(
              mContext.getResources().getDrawable(R.drawable.default_photo));
        }
      } else {
        tvServiceName.setTextColor(Utility.getColor(mContext, R.color.black));
        if (symptomResponseData.getIconUnselected() != null && !symptomResponseData.getIconUnselected().isEmpty()) {
          PicassoTrustAll.getInstance(mContext)
                  .load(symptomResponseData.getIconUnselected())
                  .placeholder(R.drawable.ic_fever_placeholder)   // optional
                  .error(R.drawable.ic_fever_placeholder)      // optional
                  .transform(new PicassoCircleTransform())
                  .into(ivServicePic);
        } else {
          ivServicePic.setImageDrawable(mContext.getResources().getDrawable(R.drawable.default_photo));
        }
      }
      symptomId = symptomResponseData.getId();
      symptom = symptomResponseData.getTitle();
    }

    @Override
    public void onClick(View view) {

      for (int i = 0; i < symptomResponseData.size(); i++) {
        if (i == getAdapterPosition()) {
          if (symptomResponseData.get(i).isChecked()) {
            symptomResponseData.get(i).setChecked(false);
            if (stringArrayList.size() > 0) {
              for (int j = 0; j < stringArrayList.size(); j++) {
                if (stringArrayList.get(j).equals(symptomResponseData.get(getAdapterPosition()).getId())) {
                  stringArrayList.remove(j);
                }
              }
              if(searchSymptomSelectedID != null &&  !searchSymptomSelectedID.isEmpty())
              {
                if(searchSymptomSelectedID.equals(symptomResponseData.get(i).getId())) {
                  searchSymptomSelectedID ="";
                }
              }
            }

          } else {
            symptomResponseData.get(i).setChecked(true);
            stringArrayList.add(symptomResponseData.get(getAdapterPosition()).getId());
          }
        }
      }
      if (isFromSearch) {
        if (mContext instanceof SymptomSearchFilter) {
          setItemSelected(getAdapterPosition());
          ((SymptomSearchFilter) mContext).list(stringArrayList);
        }
      } else {
        if (mItemClickListener != null) {
          mItemClickListener.onItemClick(view, getAdapterPosition(), symptomResponseData, stringArrayList);
        }
      }
    }
  }
}

