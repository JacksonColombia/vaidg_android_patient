package com.vaidg.incalloutcalltelecall.symptom.model;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Symptom implements Parcelable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("symptomGroup")
    @Expose
    private String symptomGroup;
    @SerializedName("symptomCategory")
    @Expose
    private String symptomCategory;
    @SerializedName("shortDescription")
    @Expose
    private String shortDescription;
    @SerializedName("detailedDescription")
    @Expose
    private String detailedDescription;
    @SerializedName("icon")
    @Expose
    private String icon;
  /*  @SerializedName("banner")
    @Expose
    private String banner;
    @SerializedName("questionArr")
    @Expose
    private ArrayList<String> questionArr = null;
    @SerializedName("num")
    @Expose
    private int num;*/
    @SerializedName("iconUnselected")
    @Expose
    private String iconUnselected;
    private boolean isChecked = false;

    public final static Parcelable.Creator<Symptom> CREATOR = new Creator<Symptom>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Symptom createFromParcel(Parcel in) {
            return new Symptom(in);
        }

        public Symptom[] newArray(int size) {
            return (new Symptom[size]);
        }

    };

    protected Symptom(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.symptomGroup = ((String) in.readValue((String.class.getClassLoader())));
        this.symptomCategory = ((String) in.readValue((String.class.getClassLoader())));
        this.shortDescription = ((String) in.readValue((String.class.getClassLoader())));
        this.detailedDescription = ((String) in.readValue((String.class.getClassLoader())));
        this.icon = ((String) in.readValue((String.class.getClassLoader())));
       // this.banner = ((String) in.readValue((String.class.getClassLoader())));
       // in.readList(this.questionArr, (java.lang.String.class.getClassLoader()));
      //  this.num = ((int) in.readValue((int.class.getClassLoader())));
        this.iconUnselected = ((String) in.readValue((String.class.getClassLoader())));
        this.isChecked = ((boolean) in.readValue((boolean.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     */
    public Symptom() {
    }

    /**
     * @param symptomGroup
     * @param detailedDescription
     * @param iconUnselected
     * @param symptomCategory
     * @param icon
     * @param id
     * @param shortDescription
     * @param title
     */
    public Symptom(String id, String title, String symptomGroup, String symptomCategory, String shortDescription, String detailedDescription, String icon,/* String banner, ArrayList<String> questionArr, int num,*/ String iconUnselected,boolean isChecked) {
        super();
        this.id = id;
        this.title = title;
        this.symptomGroup = symptomGroup;
        this.symptomCategory = symptomCategory;
        this.shortDescription = shortDescription;
        this.detailedDescription = detailedDescription;
        this.icon = icon;
       // this.banner = banner;
       // this.questionArr = questionArr;
       // this.num = num;
        this.iconUnselected = iconUnselected;
        this.isChecked = isChecked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSymptomGroup() {
        return symptomGroup;
    }

    public void setSymptomGroup(String symptomGroup) {
        this.symptomGroup = symptomGroup;
    }

    public String getSymptomCategory() {
        return symptomCategory;
    }

    public void setSymptomCategory(String symptomCategory) {
        this.symptomCategory = symptomCategory;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDetailedDescription() {
        return detailedDescription;
    }

    public void setDetailedDescription(String detailedDescription) {
        this.detailedDescription = detailedDescription;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

  /*  public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public ArrayList<String> getQuestionArr() {
        return questionArr;
    }

    public void setQuestionArr(ArrayList<String> questionArr) {
        this.questionArr = questionArr;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }*/

    public String getIconUnselected() {
        return iconUnselected;
    }

    public void setIconUnselected(String iconUnselected) {
        this.iconUnselected = iconUnselected;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(title);
        dest.writeValue(symptomGroup);
        dest.writeValue(symptomCategory);
        dest.writeValue(shortDescription);
        dest.writeValue(detailedDescription);
        dest.writeValue(icon);
      //  dest.writeValue(banner);
      //  dest.writeList(questionArr);
      //  dest.writeValue(num);
        dest.writeValue(iconUnselected);
        dest.writeValue(isChecked);
    }

    public int describeContents() {
        return 0;
    }

}