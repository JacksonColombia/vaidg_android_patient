package com.vaidg.incalloutcalltelecall.symptom;

import com.vaidg.Dagger2.ActivityScoped;
import com.vaidg.intro.IntroActivity;
import com.vaidg.intro.IntroActivityContract;
import com.vaidg.intro.IntroActivityPresenter;
import dagger.Binds;
import dagger.Module;

/**
 * <h2>IntroModule</h2>
 * <p>This class is used to provide the IntroActivityPresenter object</p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 12-07-2019.
 */
@Module
public abstract class SearchFilterModule {
  @ActivityScoped
  @Binds
  abstract SymptomContract.SymptomPresenter symptomPresenter(SymptomPresenterImp symptomPresenterImp);

  @ActivityScoped
  @Binds
  abstract SymptomContract.SymptomView symptomView(SymptomSearchFilter symptomSearchFilter);

}
