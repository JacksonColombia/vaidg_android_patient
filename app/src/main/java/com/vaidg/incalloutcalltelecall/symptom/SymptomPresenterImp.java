package com.vaidg.incalloutcalltelecall.symptom;

import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.SESSION_LOGOUT;
import static com.vaidg.utilities.Constants.catId;
import static com.vaidg.utilities.Constants.selLang;

import android.content.Context;
import com.vaidg.incalloutcalltelecall.symptom.model.GetSymptom;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.google.gson.Gson;
import com.pojo.ErrorHandel;
import com.utility.RefreshToken;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * <h2>SymptomPresenterImp</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class SymptomPresenterImp implements SymptomContract.SymptomPresenter {

  private SymptomContract.SymptomView view;
  @Inject
  Gson gson;
  @Inject
  Context mContext;
  @Inject
  SessionManagerImpl sessionManager; 
  @Inject
  LSPServices lspServices;

  @Inject
  SymptomPresenterImp() {

  }


  @Override
  public void getSymptomList(String filterString) {

    Observable<Response<ResponseBody>> observable = lspServices.onToGeSymptoms(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),
        selLang,PLATFORM_ANDROID,catId, filterString);
    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int responseCode = responseBodyResponse.code();
            JSONObject errJson;
            try {
              String responseBody =
                  responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
              String errorBody =
                  responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null;
              switch (responseCode) {
                case Constants.SUCCESS_RESPONSE:
                  if(responseBody != null && !responseBody.isEmpty()) {
                    GetSymptom responsePojo = gson.fromJson(responseBody, GetSymptom.class);
                    if (responsePojo.getData() != null && responsePojo.getData().size() > 0) {
                      if(view != null)
                      view.onResponseSuccess(responsePojo.getData());
                    }
                  }
                    break;
                case SESSION_LOGOUT:
                  if(errorBody != null && !errorBody.isEmpty()) {
                    errJson = new JSONObject(errorBody);
                    errJson.getString(MESSAGE);
                    if (!errJson.getString(MESSAGE).isEmpty()) {
                      if(view != null)
                      view.onLogout(errJson.getString(MESSAGE),sessionManager);
                    }
                  }
                  break;
                case Constants.SESSION_EXPIRED:
                  if(errorBody != null && !errorBody.isEmpty()) {
                    ErrorHandel errorHandel = gson.fromJson(errorBody, ErrorHandel.class);
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),
                        sessionManager.getREFRESHAUTH(), lspServices,
                        new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {
                              LSPApplication.getInstance().setAuthToken(sessionManager.getSID(),sessionManager.getSID(),newToken);
                            getSymptomList(filterString);
                          }

                          @Override
                          public void onFailureRefreshToken() {

                          }

                          @Override
                          public void sessionExpired(String msg) {
                            if(view != null)
                            view.onLogout(msg,sessionManager);
                          }
                        });
                  }
                    break;
                default:
                  if(errorBody != null && !errorBody.isEmpty()) {
                    errJson = new JSONObject(errorBody);
                    errJson.getString(MESSAGE);
                    if (!errJson.getString(MESSAGE).isEmpty()) {
                      if(view != null)
                      view.onError(errJson.getString(MESSAGE));
                    }
                  }
                  break;
              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {

          }
          @Override
          public void onComplete() {

          }
        });
  }



  @Override
  public void attachView(SymptomContract.SymptomView symptomView) {
    view = symptomView;
  }

  @Override
  public void detachView() {
    view = null;
  }
}