package com.vaidg.incalloutcalltelecall.symptom.model;

import java.util.ArrayList;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SymptomList implements Parcelable
{

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("symptomCategory")
    @Expose
    private ArrayList<SymptomCategory> symptomCategory = new ArrayList<>();
    public final static Parcelable.Creator<SymptomList> CREATOR = new Creator<SymptomList>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SymptomList createFromParcel(Parcel in) {
            return new SymptomList(in);
        }

        public SymptomList[] newArray(int size) {
            return (new SymptomList[size]);
        }

    };

    protected SymptomList(Parcel in) {
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.symptomCategory, (SymptomCategory.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public SymptomList() {
    }

    /**
     *
     * @param symptomCategory
     * @param title
     */
    public SymptomList(String title, ArrayList<SymptomCategory> symptomCategory) {
        super();
        this.title = title;
        this.symptomCategory = symptomCategory;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<SymptomCategory> getSymptomCategory() {
        return symptomCategory;
    }

    public void setSymptomCategory(ArrayList<SymptomCategory> symptomCategory) {
        this.symptomCategory = symptomCategory;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(title);
        dest.writeList(symptomCategory);
    }

    public int describeContents() {
        return 0;
    }

}