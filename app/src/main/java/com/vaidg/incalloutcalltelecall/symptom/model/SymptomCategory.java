package com.vaidg.incalloutcalltelecall.symptom.model;

import java.util.ArrayList;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SymptomCategory implements Parcelable
{

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("symptom")
    @Expose
    private ArrayList<Symptom> symptom = new ArrayList<>();
    public final static Parcelable.Creator<SymptomCategory> CREATOR = new Creator<SymptomCategory>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SymptomCategory createFromParcel(Parcel in) {
            return new SymptomCategory(in);
        }

        public SymptomCategory[] newArray(int size) {
            return (new SymptomCategory[size]);
        }

    };

    protected SymptomCategory(Parcel in) {
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.symptom, (Symptom.class.getClassLoader()));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public SymptomCategory() {
    }

    /**
     *
     * @param symptom
     * @param title
     */
    public SymptomCategory(String title, ArrayList<Symptom> symptom) {
        super();
        this.title = title;
        this.symptom = symptom;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Symptom> getSymptom() {
        return symptom;
    }

    public void setSymptom(ArrayList<Symptom> symptom) {
        this.symptom = symptom;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(title);
        dest.writeList(symptom);
    }

    public int describeContents() {
        return 0;
    }

}