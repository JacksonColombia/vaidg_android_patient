package com.vaidg.incalloutcalltelecall.symptom;


import static com.vaidg.utilities.Utility.*;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.symptom.adapter.SymptomListAdapter;
import com.vaidg.incalloutcalltelecall.symptom.model.GetSymptom;
import com.vaidg.incalloutcalltelecall.symptom.model.Symptom;
import com.vaidg.incalloutcalltelecall.symptom.model.SymptomList;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import dagger.android.support.DaggerAppCompatActivity;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import javax.inject.Inject;

/**
 * Created by Ali on 4/4/2018.
 */
public class SymptomSearchFilter extends DaggerAppCompatActivity implements SymptomContract.SymptomView {
  @BindView(R.id.recyclerviewSearch)
  RecyclerView recyclerviewSearch;
  @BindView(R.id.etSearch)
  EditText etSearch;
  @BindView(R.id.tvTitle)
  TextView tvTitle;
  @BindView(R.id.ivSearchClear)
  ImageView ivSearchClear;
  @BindView(R.id.progressBarShow)
  ProgressBar progressBarShow;
  AppTypeface appTypeface;
  @BindView(R.id.llNoSymptomAvailable)
  LinearLayout llNoSymptomAvailable;
  private ArrayList<SymptomList> mSymptoms = new ArrayList<SymptomList>();
  private SymptomListAdapter mSymptomSearchAdapter;
  private ArrayList<String> stringArrayList = new ArrayList<>();
  public  ArrayList<Symptom> array_sort = new ArrayList<>();
  private   int textlength = 0;
  private boolean isFromSearchFilter = false;
  private   String searchFilter = "0";
  private Timer timer=new Timer();
  private final long DELAY = 1000; // milliseconds
  @Inject
  SymptomContract.SymptomPresenter symptomPresenter;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.search_list_adapter);
    symptomPresenter.attachView(this);
    ButterKnife.bind(this);
    appTypeface = AppTypeface.getInstance(this);
    getIntentValue();
    setTypeFaceValue();
  }

  private void getIntentValue() {

    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayShowHomeEnabled(true);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
    toolbar.setNavigationIcon(R.drawable.ic_back);
    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });
    tvTitle.setVisibility(View.GONE);
    TextView tvTbTitle = toolbar.findViewById(R.id.tv_center);
    tvTbTitle.setText(R.string.searchAndselect);
    tvTbTitle.setTypeface(appTypeface.getHind_semiBold());
    tvTitle.setTypeface(appTypeface.getHind_medium());
    tvTitle.setText(R.string.search_symptom);
    etSearch.setHint(R.string.searchSymptom);

    mSymptoms = getIntent().getParcelableArrayListExtra("mSymptomsList");
    stringArrayList = getIntent().getStringArrayListExtra("stringArrayList");
    recyclerviewSearch.setVisibility(View.VISIBLE);
    recyclerviewSearch.setHasFixedSize(true);
    recyclerviewSearch.setLayoutManager(new LinearLayoutManager(this));
    mSymptomSearchAdapter = new SymptomListAdapter(mSymptoms.get(0).getSymptomCategory(),stringArrayList, true,isFromSearchFilter);
    recyclerviewSearch.setAdapter(mSymptomSearchAdapter);
    etSearch.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Constants.isFirstTime = false;
      }
    });

    etSearch.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        

      }

      @Override
      public void afterTextChanged(Editable editable) {
        timer.cancel();
        timer = new Timer();
        timer.schedule(
            new TimerTask() {
              @Override
              public void run() {
                // TODO: do what you need here (refresh list)
                // you will probably need to use runOnUiThread(Runnable action) for some specific actions (e.g. manipulating views)
                textlength = etSearch.getText().length();
                searchFilter = etSearch.getText().toString().toLowerCase().trim();
                if(!searchFilter.isEmpty())
                isFromSearchFilter = true;
                if(isNetworkAvailable(SymptomSearchFilter.this))
                symptomPresenter.getSymptomList(searchFilter);
              }
            },
            DELAY
        );
      }
    });
    
  }

  public void list(ArrayList<String> stringArrayList) {
    Log.w("TAG", "list: "+stringArrayList.size());
    Bundle bundle = new Bundle();
    bundle.putParcelableArrayList("Symptom",mSymptoms);
    bundle.putStringArrayList("StringArrayList",stringArrayList);
    Intent intent = new Intent();
    intent.putExtras(bundle);
    setResult(RESULT_OK, intent);
    finish();
    overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
  }

  private void setTypeFaceValue() {
    etSearch.setTypeface(appTypeface.getHind_regular());
    etSearch.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_12));
  }

  private void showProgress() {
    progressBarShow.setVisibility(View.VISIBLE);
  }



  @OnClick({R.id.ivSearchClear})
  public void clicked(View v) {
    if (v.getId() == R.id.ivSearchClear) {
      isFromSearchFilter = false;
      recyclerviewSearch.setVisibility(View.VISIBLE);
      etSearch.setText("");
      symptomPresenter.getSymptomList(searchFilter);
    } else {
      recyclerviewSearch.setVisibility(View.GONE);
      onBackPressed();
    }
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    finish();
    overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
  }


  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);
    //Utility.checkAndShowNetworkError(this);
  }


  @Override
  public void onResponseSuccess(ArrayList<SymptomList> symptomCategories) {
    if(symptomCategories.size() > 0) {
      recyclerviewSearch.setHasFixedSize(true);
      recyclerviewSearch.setLayoutManager(new LinearLayoutManager(this));
      mSymptomSearchAdapter = new SymptomListAdapter(symptomCategories.get(0).getSymptomCategory(), stringArrayList, true,isFromSearchFilter);
      recyclerviewSearch.setAdapter(mSymptomSearchAdapter);
    }

    Calendar mCalendar = Calendar.getInstance(Utility.getTimeZone());
    long millies = mCalendar.getTimeInMillis();
    Log.w("TAG", "onResponseSuccess: "+millies);
  }

  @Override
  public void onSessionExpired() {

  }

  @Override
  public void onLogout(String message, SessionManagerImpl sessionManager) {

  }

  @Override
  public void onError(String error) {

  }

  @Override
  public void onConnectionError(String connectionError) {

  }

  @Override
  public void onShowProgress() {

  }

  @Override
  public void onHideProgress() {

  }


  @Override
  protected void onDestroy() {
    symptomPresenter.detachView();
    super.onDestroy();
  }
}

