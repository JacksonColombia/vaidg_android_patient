package com.vaidg.incalloutcalltelecall.symptom;

import static android.app.Activity.RESULT_OK;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.symptom.adapter.SymptomListAdapter;
import com.vaidg.incalloutcalltelecall.symptom.adapter.SymptomTypeAdapter;
import com.vaidg.incalloutcalltelecall.symptom.model.GetSymptom;
import com.vaidg.incalloutcalltelecall.symptom.model.Symptom;
import com.vaidg.incalloutcalltelecall.symptom.model.SymptomList;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.utility.AlertProgress;
import dagger.android.support.DaggerFragment;
import java.util.ArrayList;
import javax.inject.Inject;

/**
 * <h2>SymptomFragment</h2>
 * <p>A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link SymptomFragment} factory method to
 * create an instance of this fragment.<p/>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class SymptomFragment extends DaggerFragment implements View.OnClickListener,
    SymptomContract.SymptomView {

  @Inject
  AlertProgress alertProgress;
  @Inject
  AppTypeface appTypeface;
  @BindView(R.id.rvSymptom)
  RecyclerView rvSymptom;
  @BindView(R.id.tvTitle)
  TextView tvTitle;
  @BindView(R.id.tvSearch)
  TextView tvSearch;
  @Inject
  SymptomContract.SymptomPresenter symptomPresenter;
  private ArrayList<SymptomList> mSymptomCategories =
      new ArrayList<SymptomList>();
  private ArrayList<String> stringArrayList = new ArrayList<>();
  private Context mContext;
  private String selectedSymptomId = "";
  private String filterString = "0";
  private SymptomListAdapter mSymptomListAdapter;
  private OnSymptomSelectedCallBack mOnSymptomSelectedCallBack;
  private SymptomTypeAdapter symptomTypeAdapter;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_symptom, container, false);
    ButterKnife.bind(this, view);
    try {
      mOnSymptomSelectedCallBack = (OnSymptomSelectedCallBack) mContext;
    } catch (ClassCastException ex) {
      //.. should log the error or throw and exception
      Log.e("MyAdapter", "Must implement the CallbackInterface in the Activity", ex);
    }
    if (mOnSymptomSelectedCallBack != null) {
      mOnSymptomSelectedCallBack.OnSymptomSelectedID(selectedSymptomId, false);
    }
    if(Utility.isNetworkAvailable(mContext))
    symptomPresenter.getSymptomList(filterString);
    initializeView(view);
    return view;
  }

  /**
   * <h2>initializeView</h2>
   *
   * <p>Intiliazing view elements</p>
   */
  private void initializeView(View view) {
    tvTitle.setTypeface(appTypeface.getHind_semiBold());
    tvSearch.setTypeface(appTypeface.getHind_medium());
    tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_16));
    tvSearch.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_12));

    tvSearch.setCursorVisible(false);

    tvSearch.setOnClickListener(v -> {
      if (mSymptomCategories.size() > 0) {
        Intent intent = new Intent(mContext, SymptomSearchFilter.class);
        intent.putParcelableArrayListExtra("mSymptomsList", mSymptomCategories);
        intent.putExtra("stringArrayList", stringArrayList);
        startActivityForResult(intent, 12);
        ((Activity) mContext).overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
      }
      // symptomTypeAdapter.getFilter().filter("Fever");
    });
  }

  @Override
  public void onClick(View view) {

  }

  @Override
  public void onResponseSuccess(
      ArrayList<SymptomList> symptomCategoryArrayList) {
//    mSymptomCategories.clear();
    if (symptomCategoryArrayList.size() > 0) {
      mSymptomCategories.addAll(symptomCategoryArrayList);
      recyclerviewSetup(mSymptomCategories, stringArrayList);
    }
  }

  private void recyclerviewSetup(ArrayList<SymptomList> symptoms, ArrayList<String> stringArrayList) {
    rvSymptom.setVisibility(View.VISIBLE);
    rvSymptom.setHasFixedSize(true);
    mSymptomListAdapter = new SymptomListAdapter(symptoms.get(0).getSymptomCategory(), stringArrayList, false,false);
    rvSymptom.setLayoutManager(new LinearLayoutManager(mContext));
    if (!mSymptomListAdapter.hasObservers()) {
      mSymptomListAdapter.setHasStableIds(true);
    } else {
      throw new IllegalStateException("Cannot change whether this adapter has "
          + "stable IDs while the adapter has registered observers.");
    }
    rvSymptom.setAdapter(mSymptomListAdapter);
    mSymptomListAdapter.setOnItemClickListener(new SymptomListAdapter.OnItemClickListener() {
      @Override
      public void onItemClick(View view, int position, ArrayList<Symptom> symptoms, ArrayList<String> stringArrayList) {
        selectSymptomId(stringArrayList);
      }

      @Override
      public void onGetAdapter(SymptomTypeAdapter adapter) {
        // symptomTypeAdapter = adapter;
      }
    });

    mSymptomListAdapter.notifyDataSetChanged();
  }

  private void selectSymptomId(ArrayList<String> stringArrayList) {
    if (stringArrayList.size() > 0) {
      selectedSymptomId = selectedSymptomId(stringArrayList);
      if (!selectedSymptomId.isEmpty()) {
        if (mOnSymptomSelectedCallBack != null) {
          mOnSymptomSelectedCallBack.OnSymptomSelectedID(selectedSymptomId, true);
        }
      } else {
        mOnSymptomSelectedCallBack.OnSymptomSelectedID(selectedSymptomId, false);
      }
    }else {
      mOnSymptomSelectedCallBack.OnSymptomSelectedID(selectedSymptomId, false);
    }
  }

  @Override
  public void onSessionExpired() {

  }

  @Override
  public void onLogout(String message, SessionManagerImpl sessionManager) {
    alertProgress.alertPositiveOnclick(mContext, message, getString(R.string.logout),
        getString(R.string.ok), isClicked -> Utility.setMAnagerWithBID(mContext, sessionManager));

  }

  @Override
  public void onError(String error) {

  }

  @Override
  public void onConnectionError(String connectionError) {

  }

  @Override
  public void onShowProgress() {

  }

  @Override
  public void onHideProgress() {

  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    this.mContext = context;
    symptomPresenter.attachView(this);
  }

  @Override
  public void onDetach() {
    mContext = null;
    symptomPresenter.detachView();
    super.onDetach();
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == 12) {
      if (resultCode == RESULT_OK) {
        if (data != null) {
          mSymptomCategories.clear();
          stringArrayList.clear();
          if (data.getParcelableArrayListExtra("Symptom") != null) {
            mSymptomCategories.addAll(data.getParcelableArrayListExtra("Symptom"));
          }
          if (data.getParcelableArrayListExtra("Symptom")
              != null) {
            stringArrayList.addAll(data.getStringArrayListExtra("StringArrayList"));
          }
          selectSymptomId(stringArrayList);
          recyclerviewSetup(mSymptomCategories, stringArrayList);
        }
      }
    }
  }

  private String selectedSymptomId(ArrayList<String> symptoms) {
    this.stringArrayList = symptoms;
    StringBuilder stringBuilder = new StringBuilder();
    if (symptoms.size() > 0) {
      for (String symptom : symptoms) {
        stringBuilder.append(symptom);
        stringBuilder.append(",");
      }
    }
    return stringBuilder.toString().trim();
  }

  /**
   * <h2>OnSymptomSelected</h2>
   *
   * <p>
   * An interface containing method signature.
   * Container Activity must implement this interface.
   * </p>
   */
  public interface OnSymptomSelectedCallBack {

    /**
     * <h2>symptomSelected</h2>
     * This method is used to select the symptoms from list
     *
     * @param symptomSelectedID id of the symptom
     * @param isSelect          flag to check whether selected or not
     */
    void OnSymptomSelectedID(String symptomSelectedID, boolean isSelect);


  }

}


