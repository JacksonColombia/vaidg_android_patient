package com.vaidg.incalloutcalltelecall.symptom.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.vaidg.R;
import com.vaidg.incalloutcalltelecall.symptom.model.GetSymptom;
import com.vaidg.incalloutcalltelecall.symptom.model.Symptom;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.Utility;
import com.utility.PicassoTrustAll;

import java.util.ArrayList;


/**
 * <h2>SymptomSearchAdapter</h2>
 * Created by ${3Embed} on 5/10/17.
 */

public class SymptomSearchAdapter extends RecyclerView.Adapter {
  private ArrayList<Symptom> mSymptoms;
  private boolean isSelect = false;
  // private OnServiceClicked context;
  private Context activityContext;
  private Activity mActivity;
  private OnItemClickListener mItemClickListener;
  private boolean isVertical = false;

  public SymptomSearchAdapter(ArrayList<Symptom> mSymptoms,
      Context activityContext,
      boolean isVertical) {//OnServiceClicked ui,

    this.mSymptoms = mSymptoms;
    this.activityContext = activityContext;
    mActivity = (Activity) activityContext;
    // this.context= ui;
    this.isVertical = isVertical;
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_list_item_v,
        parent, false);

        /*if(isVertical){
            view= LayoutInflater.from(parent.getContext()).inflate(R.layout.service_list_item_v,
            parent,false);
        }else{
            view= LayoutInflater.from(parent.getContext()).inflate(R.layout.service_list_item,
            parent,false);
        }*/
    return new ServicesViewHolder(view);
  }


  public void setItemSelected(int position,
                              ArrayList<Symptom> symptoms, String name, boolean b) {
    /*if (position != -1) {*/
    for (Symptom symptom : mSymptoms) {
/*
      mSymptoms.get(position).setChecked(
          !mSymptoms.get(position).isChecked());
*/
      if (symptom.getTitle().equals(name)) {
        if (!symptom.isChecked()) {
          symptom.setChecked(!symptom.isChecked());
        } else {
          symptom.setChecked(symptom.isChecked());
        }
      }
      // notifyDataSetChanged();
      notifyItemChanged(position);
      notifyItemRangeChanged(position, mSymptoms.size());
    }

  }

  public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
    this.mItemClickListener = mItemClickListener;
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    ServicesViewHolder viewHolder = (ServicesViewHolder) holder;
    if (mSymptoms.size() > 0) {
      viewHolder.serviceName.setText(mSymptoms.get(position).getTitle());


      mSymptoms.get(position).setChecked(mSymptoms.get(position).isChecked());
      if (mSymptoms.get(position).isChecked()) {
        isSelect = true;
        if (mSymptoms.get(position).getIcon()!= null &&!mSymptoms.get(position).getIcon().isEmpty()) {
          PicassoTrustAll.getInstance(activityContext)
                  .load(mSymptoms.get(position).getIcon())
                  .placeholder(R.drawable.ic_fever_placeholder)   // optional
                  .error(R.drawable.ic_fever_placeholder)      // optional
                  .transform(new PicassoCircleTransform())
                  .into(viewHolder.serviceImage);

        } else {
          viewHolder.serviceImage.setImageDrawable(
              activityContext.getResources().getDrawable(R.drawable.default_photo));
        }
      } else {
        isSelect = false;
        if (mSymptoms.get(position).getIconUnselected()!=null  && !mSymptoms.get(position).getIconUnselected().isEmpty()) {
          PicassoTrustAll.getInstance(activityContext)
                  .load(mSymptoms.get(position).getIconUnselected())
                  .placeholder(R.drawable.ic_fever_placeholder)   // optional
                  .error(R.drawable.ic_fever_placeholder)      // optional
                  .transform(new PicassoCircleTransform())
                  .into(viewHolder.serviceImage);

        } else {
          viewHolder.serviceImage.setImageDrawable(
              activityContext.getResources().getDrawable(R.drawable.default_photo));
        }
      }

      /*if (mSymptoms.get(position).getIconUnselected() != null) {


        RequestOptions options = new RequestOptions()
            .centerCrop()
            .placeholder(R.drawable.profile_price_bg)
            .error(R.drawable.profile_price_bg)
            .priority(Priority.HIGH);
        Glide.with(activityContext)
            .load(mSymptoms.get(position).getIconUnselected())
            .apply(options)
            .apply(new RequestOptions().transform(new CircleTransform(activityContext)))
            .into(viewHolder.serviceImage);

        // .apply(new RequestOptions().transforms(new CenterCrop(), new RoundedCorners(20)))


      }*/
    }
  }

  @Override
  public int getItemCount() {
    return mSymptoms == null ? 0 : mSymptoms.size();
  }

  public interface OnItemClickListener {

    void onItemClick(View view, int position,
                     ArrayList<Symptom> symptoms, String title,
                     boolean isSelect, boolean b);
  }

  class ServicesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.tvServiceName)
    TextView serviceName;
    @BindView(R.id.ivServicePic)
    ImageView serviceImage;
    private AppTypeface appTypeface;
    private Bundle bundle;

    ServicesViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      itemView.setOnClickListener(this);
      appTypeface = AppTypeface.getInstance(activityContext);
      serviceName.setTypeface(appTypeface.getHind_semiBold());
      bundle = new Bundle();


    }

    @Override
    public void onClick(View view) {
      if (mItemClickListener != null) {
        mItemClickListener.onItemClick(view, getAdapterPosition(), mSymptoms,mSymptoms.get(getAdapterPosition()).getTitle(), isSelect, false);
      }
    }

  }
}
