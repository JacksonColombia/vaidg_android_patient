package com.vaidg.incalloutcalltelecall.symptom;

import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;
import com.vaidg.incalloutcalltelecall.symptom.model.GetSymptom;
import com.vaidg.incalloutcalltelecall.symptom.model.SymptomList;

import java.util.ArrayList;

/**
 * <h2>SymptomContract</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */

public interface SymptomContract {
  interface SymptomView extends BaseView {

    /**
     * <h2>onResponseSuccess</h2>
     * This method is used to get symptom after the call of APIS
     *
     * @param data  list of the symptom
     */
    void onResponseSuccess(ArrayList<SymptomList> data);

  }

  interface SymptomPresenter extends BasePresenter<SymptomView> {

    /**
     * <h2>getSymptomList</h2>
     * This method is used to get the symptom from call of APIS
     *
     * @param filterString
     */
    void getSymptomList(String filterString);

   }
}
