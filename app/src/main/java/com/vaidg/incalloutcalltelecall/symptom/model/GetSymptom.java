package com.vaidg.incalloutcalltelecall.symptom.model;

import java.util.ArrayList;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetSymptom implements Parcelable
{

    @SerializedName("data")
    @Expose
    private ArrayList<SymptomList> data = new ArrayList<>();
    @SerializedName("message")
    @Expose
    private String message;
    public final static Parcelable.Creator<GetSymptom> CREATOR = new Creator<GetSymptom>() {


        @SuppressWarnings({
                "unchecked"
        })
        public GetSymptom createFromParcel(Parcel in) {
            return new GetSymptom(in);
        }

        public GetSymptom[] newArray(int size) {
            return (new GetSymptom[size]);
        }

    }
            ;

    protected GetSymptom(Parcel in) {
        in.readList(this.data, (SymptomList.class.getClassLoader()));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
    }

    /**
     * No args constructor for use in serialization
     *
     */
    public GetSymptom() {
    }

    /**
     *
     * @param data
     * @param message
     */
    public GetSymptom(ArrayList<SymptomList> data, String message) {
        super();
        this.data = data;
        this.message = message;
    }

    public ArrayList<SymptomList> getData() {
        return data;
    }

    public void setData(ArrayList<SymptomList> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(data);
        dest.writeValue(message);
    }

    public int describeContents() {
        return 0;
    }

}