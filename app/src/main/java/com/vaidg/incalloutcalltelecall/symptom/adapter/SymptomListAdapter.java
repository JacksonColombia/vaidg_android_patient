package com.vaidg.incalloutcalltelecall.symptom.adapter;


import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vaidg.R;
import com.vaidg.home.AutoFitGridRecyclerView;
import com.vaidg.incalloutcalltelecall.symptom.model.GetSymptom;
import com.vaidg.incalloutcalltelecall.symptom.model.Symptom;
import com.vaidg.incalloutcalltelecall.symptom.model.SymptomCategory;
import com.vaidg.utilities.AppTypeface;
import java.util.ArrayList;

public class SymptomListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private Context mContext;
  private ArrayList<SymptomCategory> mSymptomCategories = new ArrayList<>();
  private OnItemClickListener mItemClickListener;
  private ArrayList<String> stringArrayList = new ArrayList<>();
  private boolean isFromSearch = false;
  private boolean isFromSearchFilter = false;

  public SymptomListAdapter(ArrayList<SymptomCategory> symptomCategories, ArrayList<String> stringArrayList, boolean isFromSearch, boolean isFromSearchFilter) {
    this.mSymptomCategories = symptomCategories;
    this.stringArrayList = stringArrayList;
    this.isFromSearch = isFromSearch;
    this.isFromSearchFilter = isFromSearchFilter;
  }

  public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
    this.mItemClickListener = mItemClickListener;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
    View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_category, viewGroup, false);
    mContext = viewGroup.getContext();
    return new SymptomViewHolder(view);
  }

  @Override
  public int getItemViewType(int position) {
    return super.getItemViewType(position);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
    //setHasStableIds(true);
    ((SymptomViewHolder) viewHolder).tvSymTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
    ((SymptomViewHolder) viewHolder).bind(mSymptomCategories, mSymptomCategories.get(position),
        position,stringArrayList);

  }
  //Inside the Adapter class
  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public int getItemCount() {
    return mSymptomCategories == null ? 0 : mSymptomCategories.size();
  }

  class SymptomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.tvSymTitle)
    TextView tvSymTitle;
    @BindView(R.id.viewTime)
    View viewTime;
    @BindView(R.id.rvSymptom)
    AutoFitGridRecyclerView rvSymptom;
    AppTypeface appTypeface;
    SymptomTypeAdapter mSymptomTypeAdapter;

    SymptomViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this,itemView);
      appTypeface =  AppTypeface.getInstance(mContext);
      itemView.setOnClickListener(this);
      tvSymTitle.setTypeface(appTypeface.getHind_semiBold());
    }


    public void bind(
            ArrayList<SymptomCategory> symptomCategories,
            SymptomCategory symptomCategory, int position, ArrayList<String> stringArrayList) {
      rvSymptom.setHasFixedSize(true);
      GridLayoutManager linearLayoutManager;
      if(isFromSearch) {
        tvSymTitle.setVisibility(View.GONE);
        viewTime.setVisibility(View.GONE);
         linearLayoutManager = new GridLayoutManager(mContext, 1,
            GridLayoutManager.VERTICAL, false);
      }else{
        tvSymTitle.setVisibility(View.VISIBLE);
        viewTime.setVisibility(View.VISIBLE);
        tvSymTitle.setText(symptomCategory.getTitle());
         linearLayoutManager = new GridLayoutManager(mContext, 2,
            GridLayoutManager.VERTICAL, false);
      }
      mSymptomTypeAdapter = new SymptomTypeAdapter(symptomCategory.getSymptom(),stringArrayList,isFromSearch,isFromSearchFilter);
      rvSymptom.setLayoutManager(linearLayoutManager);
      if (!mSymptomTypeAdapter.hasObservers()) {
        mSymptomTypeAdapter.setHasStableIds(true);
      }else{
        throw new IllegalStateException("Cannot change whether this adapter has "
            + "stable IDs while the adapter has registered observers.");
      }
      rvSymptom.setAdapter(mSymptomTypeAdapter);

      mSymptomTypeAdapter.setOnItemClickListener((view, pos, symptoms, strings) -> {
        mSymptomTypeAdapter.setItemSelected(pos);
        if (mItemClickListener != null) {
          mItemClickListener.onItemClick(view, pos, symptoms,strings);
        }

      });
      mSymptomTypeAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
    }

  }

  public interface OnItemClickListener {

    void onItemClick(View view, int position,
                     ArrayList<Symptom> symptoms, ArrayList<String> stringArrayList);
    void onGetAdapter(SymptomTypeAdapter symptomTypeAdapter);
  }

}

