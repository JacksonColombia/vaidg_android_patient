package com.vaidg.incalloutcalltelecall.askquestions;


import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vaidg.R;
import com.vaidg.utilities.AppTypeface;
import dagger.android.support.DaggerFragment;
import javax.inject.Inject;

/**
 * <h2>AskQuestionsFragment</h2>
 *
 * <p>A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link AskQuestionsFragment} factory method to
 * create an instance of this fragment.<p/>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class AskQuestionsFragment extends DaggerFragment {

  @Inject
  AppTypeface appTypeface;
  @BindView(R.id.tvBiddingQuestionInfo)
  TextView tvBiddingQuestionInfo;
  @BindString(R.string.biddingInfoQuestion)
  String biddingInfoQuestion;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_askquestions, container, false);
    ButterKnife.bind(this, view);
    initializeView();
    return view;
  }

  /**
   * <h2>initializeView</h2>
   *
   * <p>Intiliazing view elements</p>
   */
  private void initializeView() {
    tvBiddingQuestionInfo.setTypeface(appTypeface.getHind_regular());
    tvBiddingQuestionInfo.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_16));
    tvBiddingQuestionInfo.setText(biddingInfoQuestion);
  }
}
