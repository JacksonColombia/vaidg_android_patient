package com.vaidg.incalloutcalltelecall.addaddress;

import android.content.Context;
import com.vaidg.home.BasePresenter;
import com.vaidg.youraddress.model.YourAddrData;

/**
 * <h2>YourAddressFragmentPresenter</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public interface YourAddressFragmentPresenter extends BasePresenter<YourAddressFragmentView> {

  /**
   * <h2>getAddress</h2>
   * This method is used to call of APIS getAddress
   *
   * @param auth                Auth token
   * @param yourAddressActivity Context of activuty
   */
  void getAddress(String auth, Context yourAddressActivity);

  /**
   * <h2>deleteAddress</h2>
   * This method is used to call of APIS deleteAddress
   *
   * @param auth                Auth token
   * @param cardId              id of the paticular address
   * @param yourAddrDataRowItem list of address added in list
   * @param adapterPosition     position of the list of address added in list
   */
  void deleteAddress(String auth, String cardId, YourAddrData yourAddrDataRowItem,
      int adapterPosition);

  /**
   * <h2>deleteAddress</h2>
   * This method is used to get the click position of address from the list
   *
   * @param adapterPosition position of the list of address added in list
   */
  void onItemClicked(int adapterPosition);
}
