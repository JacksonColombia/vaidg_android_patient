/*
 *
 *  * Copyright (C) 2014 Antonio Leiva Gordillo.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.vaidg.incalloutcalltelecall.addaddress;

import com.vaidg.home.BaseView;
import com.vaidg.youraddress.model.YourAddrData;
import java.util.ArrayList;

/**
 * <h2>YourAddressFragmentView</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public interface YourAddressFragmentView extends BaseView {

  /**
   * <h2>showProgress</h2>
   * This method is used to show progress bar on call of APIS
   */
  void showProgress();

  /**
   * <h2>hideProgress</h2>
   * This method is used to hide progress bar after the call of API success
   */
  void hideProgress();

  /**
   * <h2>showProgress</h2>
   * This method is used to show error message on call of APIS
   *
   * @param message message to show an error message
   */
  void setError(String message);

  /**
   * <h2>hideProgress</h2>
   * This method is used to message if no address are avialable after the call of API success
   */
  void setNoAddressAvailable();

  /**
   * <h2>addItems</h2>
   * This method is used to get address after the success of the Login API CALL,
   *
   * @param list getting total list of address which are added.
   */
  void addItems(ArrayList<YourAddrData> list);

  /**
   * <h2>refreshItems</h2>
   * This method is used to refresh list of address after the success of the Login API CALL,
   *
   * @param data            total list of address  after the address which has been deleted by the
   *                        user
   * @param adapterPosition email to store in Account
   */
  void refreshItems(YourAddrData data, int adapterPosition);

  /**
   * <h2>onAddressSelected</h2>
   * This method is used to set Callback for position of the address click.
   *
   * @param adapterPosition position of the address selected
   */
  void onAddressSelected(int adapterPosition);

  /**
   * <h2>onError</h2>
   * This method is used to set Error message from API calls
   *
   * @param message to show error text msg.
   */
  void onError(String message);


}
