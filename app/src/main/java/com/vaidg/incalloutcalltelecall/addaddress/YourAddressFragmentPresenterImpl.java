package com.vaidg.incalloutcalltelecall.addaddress;

import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.SESSION_LOGOUT;

import android.content.Context;
import com.vaidg.R;
import com.vaidg.youraddress.model.YourAddrData;
import com.vaidg.youraddress.model.YourAddressResponse;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.google.gson.Gson;
import com.pojo.ErrorHandel;
import com.utility.AlertProgress;
import com.utility.RefreshToken;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * <h2>YourAddressFragmentPresenterImpl</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class YourAddressFragmentPresenterImpl implements YourAddressFragmentPresenter {

  @Inject
  LSPServices lspServices;

  @Inject
  AlertProgress alertProgress;
  @Inject
  Gson gson;
  @Inject
  SessionManagerImpl manager;
  private Context mContext;
  private YourAddressFragmentView view;


  @Inject
  YourAddressFragmentPresenterImpl() {
  }

  @Override
  public void getAddress(final String auth, Context yourAddressActivity) {
       /* if (view!=null) {
            view.showProgress();
        }*/
    mContext = yourAddressActivity;
    Observable<Response<ResponseBody>> response = lspServices.getAddress(auth, Constants.selLang,
        Constants.PLATFORM_ANDROID);
    response.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> value) {

            int code = value.code();
            JSONObject errJsonD;
            try {
              String response = value.body() != null ? value.body().string() : null;
              String errorBody = value.errorBody() != null ? value.errorBody().string() : null;
              switch (code) {
                case Constants.SUCCESS_RESPONSE:
                  if (response != null && !response.isEmpty()) {
                    YourAddressResponse yourAddrResponse = gson.fromJson(response,
                        YourAddressResponse.class);
                    if (yourAddrResponse.getData() != null
                        && yourAddrResponse.getData().size() > 0) {
                      if (view != null) {
                        view.addItems(yourAddrResponse.getData());
                      }
                    }else {
                      if (view != null) {
                        view.setNoAddressAvailable();
                      }
                    }
                  }
                  if (view != null) {
                    view.hideProgress();
                  }
                  break;
                case SESSION_LOGOUT:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    errJsonD = new JSONObject(errorBody);
                    errJsonD.getString(MESSAGE);
                    if (!errJsonD.getString(MESSAGE).isEmpty()) {
                      if (view != null) {
                        view.onLogout(errJsonD.getString(MESSAGE), manager);
                      }
                    }
                  }
                  if (view != null) {
                    view.onHideProgress();
                  }
                  break;
                case Constants.SESSION_EXPIRED:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    ErrorHandel errorHandel = gson.fromJson(response, ErrorHandel.class);
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(),
                        lspServices,
                        new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {
                            if (view != null) {
                              view.onHideProgress();
                            }
                              LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                              getAddress(newToken, yourAddressActivity);
                          }

                          @Override
                          public void onFailureRefreshToken() {

                          }

                          @Override
                          public void sessionExpired(String msg) {
                            if (view != null) {
                              view.hideProgress();
                              view.onLogout(msg, manager);
                            }
                          }
                        });
                    break;
                  }
                default:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    errJsonD = new JSONObject(errorBody);
                    errJsonD.getString(MESSAGE);
                    if (!errJsonD.getString(MESSAGE).isEmpty()) {
                      if (view != null) {
                        view.onError(errJsonD.getString(MESSAGE));
                      }
                    }
                  }
                  if (view != null) {
                    view.onHideProgress();
                  }
                  break;
              }
            } catch (IOException | JSONException | NumberFormatException e) {
              e.printStackTrace();
              if (view != null) {
                view.onHideProgress();
              }
            }
          }

          @Override
          public void onError(Throwable e) {
            e.printStackTrace();
          }

          @Override
          public void onComplete() {
          }
        });

  }

  @Override
  public void deleteAddress(final String auth, final String cardId, final YourAddrData rowItem,
      int adapterPosition) {

    alertProgress.alertPositiveNegativeOnclick(mContext,
        mContext.getResources().getString(R.string.areYouSureYouWantOTDelete),
        mContext.getResources().getString(R.string.system_error),
        mContext.getResources().getString(R.string.ok),
        mContext.getResources().getString(R.string.cancel), false, isClicked -> {
          if (isClicked) {
            Observable<Response<ResponseBody>> response = lspServices.deleteAddress(LSPApplication.getInstance().getAuthToken(manager.getSID()),
                Constants.selLang, Constants.PLATFORM_ANDROID, cardId);
            response.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {


                  @Override
                  public void onSubscribe(Disposable d) {

                  }

                  @Override
                  public void onNext(Response<ResponseBody> value) {
                    int code = value.code();
                    JSONObject errJsonD;
                    try {
                      String response =
                          value.body() != null ? value.body().toString() : null;
                      String errorBody =
                          value.errorBody() != null ? value.errorBody().toString() : null;
                      switch (code) {
                        case Constants.SUCCESS_RESPONSE:
                          if (response != null && !response.isEmpty()) {

                           /* ServerResponse serverResponse = gson.fromJson(response,
                                ServerResponse.class);
                            if (serverResponse.getMessage() != null
                                && !serverResponse.getMessage().isEmpty()) {
                              String message = serverResponse.getMessage();

                            }*/
                            if (view != null) {
                              view.refreshItems(rowItem, adapterPosition);
                            }
                          }
                          if (view != null) {
                            view.hideProgress();
                          }
                          break;
                        case SESSION_LOGOUT:
                          if (errorBody != null && !errorBody.isEmpty()) {

                            errJsonD = new JSONObject(errorBody);
                            errJsonD.getString(MESSAGE);
                            if (!errJsonD.getString(MESSAGE).isEmpty()) {
                              if (view != null) {
                                view.onLogout(errJsonD.getString(MESSAGE), manager);
                              }
                            }
                          }
                          if (view != null) {
                            view.onHideProgress();
                          }
                            break;
                        case Constants.SESSION_EXPIRED:
                          if (errorBody != null && !errorBody.isEmpty()) {


                            ErrorHandel errorHandel = gson.fromJson(response,
                                ErrorHandel.class);

                            // errJsonD = new JSONObject(response);
                            RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),
                                manager.getREFRESHAUTH(),
                                lspServices,
                                new RefreshToken.RefreshTokenImple() {
                                  @Override
                                  public void onSuccessRefreshToken(String newToken) {
                                    if (view != null) {
                                      view.onHideProgress();
                                    }
                                      LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                      deleteAddress(newToken, cardId, rowItem, adapterPosition);

                                  }

                                  @Override
                                  public void onFailureRefreshToken() {

                                  }

                                  @Override
                                  public void sessionExpired(String msg) {
                                    if (view != null) {
                                      view.hideProgress();
                                      view.onLogout(msg, manager);
                                    }
                                  }
                                });
                          }
                          break;
                        default:
                          if (errorBody != null && !errorBody.isEmpty()) {
                            errJsonD = new JSONObject(errorBody);
                            errJsonD.getString(MESSAGE);
                            if (!errJsonD.getString(MESSAGE).isEmpty()) {
                              if (view != null) {
                                view.onError(errJsonD.getString(MESSAGE));
                              }
                            }
                          }
                          if (view != null) {
                            view.onHideProgress();
                          }
                          break;
                      }
                    } catch (Exception e) {
                      e.printStackTrace();
                      if (view != null) {
                        view.onHideProgress();
                      }
                    }


                  }

                  @Override
                  public void onError(Throwable e) {
                    e.printStackTrace();
                    if (view != null) {
                      view.onHideProgress();
                    }
                  }

                  @Override
                  public void onComplete() {

                  }
                });
          }
        });


  }

  @Override
  public void onItemClicked(int adapterPosition) {
    if(view != null) {
      view.onAddressSelected(adapterPosition);
    }
  }

  @Override
  public void attachView(YourAddressFragmentView view) { this.view = view;
  }

  @Override
  public void detachView() {
    view = null;
  }
}
