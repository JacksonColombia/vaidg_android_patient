package com.vaidg.incalloutcalltelecall.addaddress;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.app.Activity.RESULT_OK;
import static com.vaidg.utilities.Constants.isAddressSelect;
import static com.vaidg.utilities.Constants.latitude;
import static com.vaidg.utilities.Constants.longitude;
import static com.vaidg.utilities.Constants.proAddress;

import adapters.AddressListOneAdapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


import com.vaidg.R;
import com.vaidg.add_address.AddAddressActivity;
import com.vaidg.add_address.SearchAddressLocation;
import com.vaidg.youraddress.model.YourAddrData;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.google.android.gms.maps.model.LatLng;
import com.utility.AlertProgress;
import com.utility.LocationUtil;

import org.jetbrains.annotations.NotNull;

import dagger.android.support.DaggerFragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * <h2>YourAddressFragment</h2>
 *
 * <p>A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link YourAddressFragment} factory method to
 * create an instance of this fragment.<p/>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class YourAddressFragment extends DaggerFragment implements YourAddressFragmentView,LocationUtil.LocationNotifier {

  public final String TAG = YourAddressFragment.class.getName();
  private final int SEARCH_RESULT = 101;
  @BindView(R.id.linAddress)
  NestedScrollView linAddress;
  @BindView(R.id.recyclerViewAddress)
  RecyclerView recyclerViewAddress;
  @BindView(R.id.etAddress)
  EditText etAddress;
  @BindView(R.id.tvChangeAdd)
  TextView tvChangeAdd;
  @BindView(R.id.ivSearchAddress)
  ImageView ivSearchAddress;
  @BindView(R.id.tvAddNewAddress)
  TextView tvAddNewAddress;
  @Inject
  YourAddressFragmentPresenter yourAddressPresenter;
  @BindView(R.id.tvOrAddress)
  TextView tvOrAddress;
  @BindView(R.id.lvViewOrAddress)
  LinearLayout lvViewOrAddress;
  @Inject
  SessionManagerImpl sessionManager;
  @Inject
  AppTypeface appTypeface;
  @BindView(R.id.tvLocTitle)
  TextView tvLocTitle;
  @BindView(R.id.tvLocSubTitle)
  TextView tvLocSubTitle;
  @BindString(R.string.location_of)
  String location_of;
  @BindString(R.string.doctor_address)
  String doctor_address;
  @BindString(R.string.patient_address)
  String patient_address;
  private AddressListOneAdapter mAddressListAdapter;
  private ArrayList<YourAddrData> mYourAddrData = new ArrayList<>();
  private String mAdressTitle = "";
  private LatLng latLng = null;
  private String bidAddress = "";
  private double bidLatitude = 0;
  private double bidLongitude = 0;
  private Context mContext;
  private AlertProgress alertProgress;
  public static final int REQUEST_PERMISSIONS_CODE = 5000;
  private LocationUtil mLocationUtil;


  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_youraddress, container, false);
    ButterKnife.bind(this, view);
    alertProgress = new AlertProgress(mContext);
      checkPermission();
     initialize();
     AddressSaved();
   // biddingAddress();
      return view;
  }



  private void checkPermission() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (ContextCompat.checkSelfPermission(getActivity(),
              ACCESS_FINE_LOCATION)
              != PackageManager.PERMISSION_GRANTED) {

        // request the permission
        requestPermissions(new String[]{ACCESS_FINE_LOCATION},
                REQUEST_PERMISSIONS_CODE);
      }
      else {
        // has the permission.
        etAddress.setText(mContext.getResources().getString(R.string.detect_current_location));
      }
    }else {
     /* if(proAddress.isEmpty()) {
        createLocationObj();
      }else{
        etAddress.setText(sessionManager.getAddress());
      }*/
      etAddress.setText(mContext.getResources().getString(R.string.detect_current_location));
    }
  }

  private void biddingAddress() {

    etAddress.setText(bidAddress);
    if (!"".equals(sessionManager.getLatitude())) {
      latLng = new LatLng(bidLatitude, bidLongitude);
    }
  }

  private void AddressSaved() {
    String addAddress = "+ " + getString(R.string.addNewAddress);
   // tvAddNewAddress.setText(addAddress);
   // bidAddress = sessionManager.getAddress();
    etAddress.setFocusable(false);
    bidLatitude = Constants.latitude;
    bidLongitude = Constants.longitude;
    //  yourAddressPresenter.getAddress(LSPApplication.getInstance().getAuthToken(mSessionManager.getSID()),this);
    recyclerViewAddress.setHasFixedSize(true);
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
    mAddressListAdapter = new AddressListOneAdapter(mYourAddrData, yourAddressPresenter,
        LSPApplication.getInstance().getAuthToken(sessionManager.getSID()), true);
    recyclerViewAddress.setLayoutManager(linearLayoutManager);
    recyclerViewAddress.setAdapter(mAddressListAdapter);
    recyclerViewAddress.setNestedScrollingEnabled(false);

    if (mYourAddrData.size() > 0) {
      lvViewOrAddress.setVisibility(View.VISIBLE);
    } else {
      lvViewOrAddress.setVisibility(View.INVISIBLE);
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    Constants.filteredAddress = "";
    Constants.filteredLat = 0;
    Constants.filteredLng = 0;
    if (Utility.isNetworkAvailable(mContext)) {
      yourAddressPresenter.getAddress(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()), getActivity());
    } else {
      alertProgress.tryAgain(mContext, getString(R.string.pleaseCheckInternet), getString(R.string.system_error), isClicked -> {
      });
    }
  }

  private void initialize() {
    tvLocTitle.setTypeface(appTypeface.getHind_semiBold());
    tvLocSubTitle.setTypeface(appTypeface.getHind_regular());
    tvChangeAdd.setTypeface(appTypeface.getHind_semiBold());
    etAddress.setTypeface(appTypeface.getHind_medium());
    tvAddNewAddress.setTypeface(appTypeface.getHind_semiBold());
    tvOrAddress.setTypeface(appTypeface.getHind_regular());

    tvLocTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_18));
    tvLocSubTitle.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_16));
    etAddress.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_14));
    tvChangeAdd.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_14));
    tvOrAddress.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_15));

    if (Constants.callTypeInOutTele != 2) {
      mAdressTitle = location_of + " " + doctor_address;
      tvLocSubTitle.setText(mAdressTitle);
    } else {
      mAdressTitle = location_of + " " + patient_address;
      tvLocSubTitle.setText(mAdressTitle);
    }
  }

  @Override
  public void showProgress() {

  }

  @Override
  public void hideProgress() {

  }

  @Override
  public void setError(String message) {

  }

  @Override
  public void setNoAddressAvailable() {

  }

  @Override
  public void addItems(ArrayList<YourAddrData> list) {
    mYourAddrData.clear();
    mYourAddrData.addAll(list);
    mAddressListAdapter.notifyDataSetChanged();
    if (mYourAddrData.size() > 0) {
      lvViewOrAddress.setVisibility(View.VISIBLE);
    }
  }

  @Override
  public void refreshItems(YourAddrData data, int adapterPosition) {
    if (mAddressListAdapter != null) {
      mAddressListAdapter.deleteItem(adapterPosition);
      if (bidAddress.equals(data.getAddLine2())) {
        myCurrentLocation();
      }
      if (mAddressListAdapter.getItemCount() == 0) {
        lvViewOrAddress.setVisibility(View.INVISIBLE);
        setNoAddressAvailable();
      } else {
        lvViewOrAddress.setVisibility(View.VISIBLE);
      }
    }
  }

  private void myCurrentLocation() {
    bidLatitude = Constants.latitude;
    bidLongitude = Constants.longitude;
    bidAddress = sessionManager.getAddress();
    etAddress.setText(bidAddress);
  }

  @Override
  public void onAttach(Context mContext) {
    super.onAttach(mContext);
    yourAddressPresenter.attachView(this);
    this.mContext = mContext;
  }

  @Override
  public void onDetach() {
    yourAddressPresenter.detachView();
    this.mContext = null;
    super.onDetach();
  }

  @Override
  public void onAddressSelected(int adapterPosition) {
    bidLatitude = mYourAddrData.get(adapterPosition).getLatitude();
    bidLongitude = mYourAddrData.get(adapterPosition).getLongitude();
    bidAddress = mYourAddrData.get(adapterPosition).getAddLine1();
    proAddress = bidAddress;
    tvChangeAdd.setSelected(true);
    Constants.isAddressSelect = true;
  }

  @Override
  public void onError(String message) {

  }

  @Override
  public void onConnectionError(String connectionError) {

  }

  @Override
  public void onShowProgress() {

  }

  @Override
  public void onHideProgress() {

  }

  @Override
  public void onSessionExpired() {

  }

  @Override
  public void onLogout(String message, SessionManagerImpl sessionManager) {

  }

  @OnClick({R.id.etAddress, R.id.tvAddNewAddress, R.id.tvChangeAdd})
  public void onClickGrid(View view) {
    switch (view.getId()) {
      case R.id.ivSearchAddress:
      case R.id.etAddress:
       if (Utility.isNetworkAvailable(mContext)) {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
              // request the permission
              createLocationObj();
            }
            else{
              requestPermissions(new String[]{ACCESS_FINE_LOCATION},
                      REQUEST_PERMISSIONS_CODE);
            }

          }
        } else {
          alertProgress.tryAgain(mContext, getString(R.string.pleaseCheckInternet), getString(R.string.system_error), isClicked -> {
          });
        }
        break;
      case R.id.tvAddNewAddress:
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          if (ContextCompat.checkSelfPermission(getActivity(),
                  ACCESS_FINE_LOCATION)
                  == PackageManager.PERMISSION_GRANTED) {
            // request the permission
        Intent intent = new Intent(getActivity(), AddAddressActivity.class);
        startActivity(intent);
          }
          else{
            requestPermissions(new String[]{ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_CODE);
          }

        }
        break;
      case R.id.tvChangeAdd:
        if (mYourAddrData.size() > 0) {
          mAddressListAdapter.removeSelectedItem();
        }
        Intent intentOne = new Intent(getActivity(), SearchAddressLocation.class);
        intentOne.putExtra("CominFROM", "HOMEFRAG");
        //intentOne.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intentOne, SEARCH_RESULT);
        tvChangeAdd.setSelected(false);
        // myCurrentLocation();
        break;
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (resultCode == RESULT_OK) {
      switch (requestCode) {

        case SEARCH_RESULT:
          if (data != null) {
            isAddressSelect = true;
            String addrssname = data.getStringExtra("placename");
            String featureAddressName = data.getStringExtra("formatedaddes");
            bidLatitude = data.getDoubleExtra("LATITUDE", 0);
            bidLongitude = data.getDoubleExtra("LONGITUDE", 0);
            if ("NO".equals(addrssname)) {
              myCurrentLocation();
            } else {
              proAddress = addrssname + "," + featureAddressName;
              latitude = bidLatitude;
              longitude = bidLongitude;
              etAddress.setText(addrssname + "," + featureAddressName);
              bidAddress = featureAddressName;
            }
            latLng = new LatLng(bidLatitude, bidLongitude);

            etAddress.setFocusable(false);
          }
          break;
        case Constants.ADDRESS_RESULT_CODE:
          if (data != null) {
            bidLatitude = data.getDoubleExtra("lat", 0.0);
            bidLongitude = data.getDoubleExtra("lng", 0.0);
            String bookingAddress = data.getStringExtra("AddressLine1");
            latLng = new LatLng(bidLatitude, bidLongitude);
                       /* String tag = data.getStringExtra("TAGAS");
                        String bookingAddress2 = data.getStringExtra("AddressLine2");
                       */
            etAddress.setText(bookingAddress);
            bidAddress = bookingAddress;
            Constants.isAddressSelect = true;
          }
          break;

      }
    }
  }
  @Override
  public void onRequestPermissionsResult(int requestCode, @NotNull String permissions[], @NotNull int[] grantResults) {
    if (requestCode == REQUEST_PERMISSIONS_CODE) {// If request is cancelled, the result arrays are empty.
      if (grantResults.length > 0
              && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        // permission was granted.
/*          if(proAddress.isEmpty()) {
            createLocationObj();
          }else{
            etAddress.setText(sessionManager.getAddress());
          }*/
        etAddress.setText(mContext.getResources().getString(R.string.detect_current_location));
      } else {
        // permission denied.
        // tell the user the action is cancelled
        requestPermissions(new String[]{ACCESS_FINE_LOCATION},
                REQUEST_PERMISSIONS_CODE);
      }
    }
  }
  private void createLocationObj() {
    mLocationUtil = new LocationUtil(getActivity());
    mLocationUtil.setListener(this);
    if (!mLocationUtil.isGoogleApiConnected()) {
      mLocationUtil.checkLocationSettings();
    }
  }

  @Override
  public void updateLocation(Location location) {

    //sessionManager.setLatitude(location.getLatitude() + "");
    //sessionManager.setLongitude(location.getLongitude() + "");

    Constants.latitude = location.getLatitude();
    Constants.longitude = location.getLongitude();
    String[] params = {"" + location.getLatitude(), "" + location.getLongitude()};
    new BackgroundGetAddress().execute(params);
    if (mLocationUtil != null && mLocationUtil.isGoogleApiConnected()) {
      mLocationUtil.stoppingLocationUpdate();
    }
  }
  @SuppressLint("StaticFieldLeak")
  private class BackgroundGetAddress extends AsyncTask<String, Void, String> {
    List<Address> address;
    String lat, lng;

    @Override
    protected String doInBackground(String... params) {
      try {
        lat = params[0];
        lng = params[1];
        if (lat != null && lng != null) {
          if (mContext != null) {
            Geocoder geocoder = new Geocoder(mContext);
            address = geocoder.getFromLocation(Double.parseDouble(params[0]),
                    Double.parseDouble(params[1]), 1);

          }
        }
      } catch (IOException e) {
        e.printStackTrace();
        ((Activity) mContext).runOnUiThread(() -> {
          sessionManager.setLatitude(lat);
          sessionManager.setLongitude(lng);
          Constants.latitude = Double.parseDouble(lat);
          Constants.longitude = Double.parseDouble(lng);
        });

      }
      return null;
    }


    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      if (address != null && address.size() > 0) {
        Address obj = address.get(0);
        sessionManager.setAddress(obj.getAddressLine(0));
        proAddress = obj.getAddressLine(0);
        etAddress.setText(sessionManager.getAddress());
        Constants.isAddressSelect = true;
      }
    }
  }


  @Override
  public void locationMsg(String error) {

  }
}