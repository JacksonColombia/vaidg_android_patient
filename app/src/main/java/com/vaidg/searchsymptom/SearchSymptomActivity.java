package com.vaidg.searchsymptom;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;

import com.vaidg.R;
import com.vaidg.home.AutoFitGridRecyclerView;
import com.vaidg.model.CatDataArray;
import com.vaidg.model.Category;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import adapters.SearchSymptomAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;

import static com.vaidg.utilities.Constants.cityId;
import static com.vaidg.utilities.Constants.cityName;

/**
 * Created by Ali on 4/4/2018.
 */
public class SearchSymptomActivity extends DaggerAppCompatActivity implements SearchSymptomActivityContract.SearchSymptomActivityView{
    @BindView(R.id.recyclerviewSearch)
    AutoFitGridRecyclerView recyclerviewSearch;
    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSearchClear)
    ImageView ivSearchClear;
    @BindView(R.id.progressBarShow)
    ProgressBar progressBarShow;
    AppTypeface appTypeface;
    private ArrayList<Category> categoryArrayList = new ArrayList<>();
    private SearchSymptomAdapter searchSymptomAdapter;
    private String searchType = "0";
    @BindView(R.id.llNoSymptomAvailable)
    LinearLayout llNoSymptomAvailable;
    @Inject
    SearchSymptomActivityContract.SearchSymptomActivityPresenter searchSymptomPresenter;
    private Timer timer;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_list);
        ButterKnife.bind(this);
        appTypeface = AppTypeface.getInstance(this);
        getIntentValue();
        setTypeFaceValue();
    }

    private void getIntentValue() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        TextView tvTbTitle = toolbar.findViewById(R.id.tv_center);
        tvTbTitle.setText(R.string.action_search);
        tvTbTitle.setTypeface(appTypeface.getHind_semiBold());
        tvTitle.setVisibility(View.GONE);
        tvTitle.setTypeface(appTypeface.getHind_medium());
        tvTitle.setText(R.string.search_results);
        recyclerviewSearch.setHasFixedSize(true);
        GridLayoutManager linearLayoutManager = new GridLayoutManager(this, 1, GridLayoutManager.VERTICAL, false);
        recyclerviewSearch.setLayoutManager(linearLayoutManager);
        searchSymptomAdapter = new SearchSymptomAdapter(categoryArrayList);
        recyclerviewSearch.setAdapter(searchSymptomAdapter);
    }

    private void setTypeFaceValue() {
        etSearch.setCursorVisible(false);
        etSearch.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelOffset(R.dimen.sp_12));
        etSearch.setHint(getResources().getString(R.string.searchSymptom));
        etSearch.setTypeface(appTypeface.getHind_regular());

        etSearch.setOnClickListener(view -> {
            etSearch.requestFocus();
            etSearch.setCursorVisible(true);
            etSearch.setFocusableInTouchMode(true);
            etSearch.setFocusable(true);
            InputMethodManager im = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            im.showSoftInput(etSearch, 0);
        });
        etSearch.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    searchType = s.toString();
                    //Your query to fetch Data
                    if(!searchType.isEmpty())
                    {
                        // user typed: start the timer
                        timer = new Timer();
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                // do your actual work here
                                Constants.searchType = searchType;
                                if(Utility.isNetworkAvailable(SearchSymptomActivity.this))
                                searchSymptomPresenter.onGetCategory(searchType,cityId);
                            }
                        }, 600); // 600ms delay before the timer executes the „run“ method from TimerTask
                    }
                }else{
                    categoryArrayList.clear();
                    llNoSymptomAvailable.setVisibility(View.GONE);
                    recyclerviewSearch.setVisibility(View.GONE);
                    tvTitle.setVisibility(View.GONE);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence query, int start, int before, int count) {
                // user is typing: reset already started timer (if existing)
                if (timer != null) {
                    timer.cancel();
                }
            }
        });
    }


    @OnClick({R.id.ivSearchClear})
    public void clicked(View v) {
            if(!etSearch.getText().toString().trim().isEmpty()) {
                categoryArrayList.clear();
                searchSymptomAdapter.notifyDataSetChanged();
                etSearch.setText("");
            }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        //Utility.checkAndShowNetworkError(this);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    etSearch.setCursorVisible(false);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }


    @Override
    public void onSuccess(ArrayList<CatDataArray> data) {
        if(data.size() > 0)
        {
            tvTitle.setVisibility(View.VISIBLE);
            categoryArrayList.clear();
            for (int i = 0; i < data.size(); i++) {
                categoryArrayList.addAll(data.get(i).getCategory());
                cityId = data.get(i).getCategory().get(0).getCityId();
                cityName = data.get(i).getCategory().get(0).getCity();
            }
            if(categoryArrayList.size() > 0) {
                llNoSymptomAvailable.setVisibility(View.GONE);
                recyclerviewSearch.setVisibility(View.VISIBLE);
                searchSymptomAdapter.notifyDataSetChanged();// data set changed
            }else{
                onNotOperational("");
            }
        }
    }

    @Override
    public void onNotOperational(String message) {
        llNoSymptomAvailable.setVisibility(View.VISIBLE);
        recyclerviewSearch.setVisibility(View.GONE);
        tvTitle.setVisibility(View.GONE);
    }

    @Override
    public void onSessionExpired() {

    }

    @Override
    public void onLogout(String message, SessionManagerImpl sessionManager) {

    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onConnectionError(String connectionError) {

    }

    @Override
    public void onShowProgress() {
/*
        if(progressBarShow != null)
        progressBarShow.setVisibility(View.VISIBLE);
*/

    }

    @Override
    public void onHideProgress() {
/*
        if(progressBarShow != null)
            progressBarShow.setVisibility(View.GONE);
*/

    }
}
