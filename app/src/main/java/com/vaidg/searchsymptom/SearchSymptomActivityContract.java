package com.vaidg.searchsymptom;

import android.app.Activity;
import android.content.Context;
import android.widget.LinearLayout;

import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;
import com.vaidg.model.CatDataArray;
import com.vaidg.model.Category;
import com.vaidg.model.CategoryCity;
import com.vaidg.utilities.SessionManagerImpl;

import java.util.ArrayList;

/**
 * <h2>ServiceFragContract</h2>
 * Created by Ali on 1/29/2018.
 */

public interface SearchSymptomActivityContract
{
    interface SearchSymptomActivityPresenter extends BasePresenter{
        void onGetCategory(String search, String cityId);
        void categorySuccess(String resultBody);
        void onHideProgress();
    }
    interface SearchSymptomActivityView extends BaseView{
        void onSuccess(ArrayList<CatDataArray> data);
        void onNotOperational(String message);
      }
}
