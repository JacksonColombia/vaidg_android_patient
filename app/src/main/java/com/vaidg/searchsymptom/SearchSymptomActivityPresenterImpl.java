package com.vaidg.searchsymptom;

import android.content.Context;
import android.util.Log;
import com.vaidg.R;
import com.vaidg.model.CatDataArray;
import com.vaidg.model.CategoryResponse;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.google.gson.Gson;
import com.pojo.ErrorHandel;
import com.utility.RefreshToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.NOT_FOUND;
import static com.vaidg.utilities.Constants.SESSION_LOGOUT;

/**
 * <h2>ServicesFragPresenter</h2>
 * Created by Ali on 1/29/2018.
 */
public class SearchSymptomActivityPresenterImpl implements SearchSymptomActivityContract.SearchSymptomActivityPresenter {
  @Inject
  LSPServices lspServices;
  @Inject
  Context context;
  @Inject
  SessionManagerImpl sessionManager;
  @Inject
  Gson gson;
  private String TAG = SearchSymptomActivityContract.class.getSimpleName();
  @Inject
  SearchSymptomActivityContract.SearchSymptomActivityView  view;
  @Inject
  SearchSymptomActivityPresenterImpl() {

  }
  @Override
  public void onGetCategory(String search, String cityId) {
    if(view != null)
    {
      view.onShowProgress();
    }
    Observable<Response<ResponseBody>> responseObservable = lspServices.getCategories(
        LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),
        Constants.selLang, Constants.PLATFORM_ANDROID,sessionManager.getIpAddress(),search,Double.parseDouble(sessionManager.getLatitude()),Double.parseDouble(sessionManager.getLongitude()),cityId);
    responseObservable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> result) {
            int code = result.code();
            JSONObject jsonObject;
            try {
              String resultBody = result.body() != null ? result.body().string() : null;
              String errorBody = result.errorBody() != null ? result.errorBody().string() : null;
              switch (code)  {
                case Constants.SUCCESS_RESPONSE:
                  if (resultBody != null && !resultBody.isEmpty()) {
                    Constants.searchType = search;
                    categorySuccess(resultBody);
                  }
                  onHideProgress();
                  break;
                case Constants.SESSION_EXPIRED:
                  if (errorBody != null && !errorBody.isEmpty()) {

                    ErrorHandel errorHandel = gson.fromJson(errorBody, ErrorHandel.class);
                    Log.w(TAG, "TokenHandler: " + errorBody + errorHandel.getData());
                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()), sessionManager.getREFRESHAUTH(),
                        lspServices,
                        new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {
                            onHideProgress();
                            LSPApplication.getInstance().setAuthToken(sessionManager.getSID(),sessionManager.getSID(),newToken);
                            onGetCategory(search, cityId);

                          }

                          @Override
                          public void onFailureRefreshToken() {
                            onHideProgress();
                            if(view != null) {
                              view.onConnectionError(context.getResources().getString(R.string.pleaseCheckInternet));
                            }
                          }

                          @Override
                          public void sessionExpired(String msg) {
                            onHideProgress();
                            if(view != null)
                            view.onLogout(msg, sessionManager);

                          }
                        });
                  }
                  break;
                case NOT_FOUND:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      if(view != null)
                      view.onNotOperational(jsonObject.getString(MESSAGE));
                    }
                  }

                  onHideProgress();
                  break;
                case SESSION_LOGOUT:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    Log.w(TAG, "GetCategory: " + errorBody);
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      if(view != null)
                      view.onLogout(jsonObject.getString(MESSAGE), sessionManager);
                    }
                  }
                  onHideProgress();
                  break;
                default:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      if(view != null)
                      view.onError(jsonObject.getString(MESSAGE));
                    }
                  }
                  onHideProgress();
                  break;

              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
              onHideProgress();
              if(view != null) {
                view.onConnectionError(context.getResources().getString(R.string.pleaseCheckInternet));
              }
            }

          }

          @Override
          public void onError(Throwable e) {
            e.printStackTrace();
            onHideProgress();
            if(view != null) {
              view.onConnectionError(context.getResources().getString(R.string.pleaseCheckInternet));
            }
          }

          @Override
          public void onComplete() {
            Utility.printLog(TAG, "This is me onComplete");
          }
        });
  }

  @Override
  public void categorySuccess(String resultBody) {
    CategoryResponse response = gson.fromJson(resultBody, CategoryResponse.class);
    if (response != null && response.getData() != null) {
        callOnDataSet(response.getData().getCatArr());
    }

  }

  @Override
  public void onHideProgress() {
    if (view != null)
      view.onHideProgress();
  }

  private void callOnDataSet(ArrayList<CatDataArray> catArr) {
      setDataOn(catArr);
  }

  private void setDataOn(ArrayList<CatDataArray> catArr) {
    if (catArr.size() > 0) {
      if(view != null)
      view.onSuccess(catArr);
      onHideProgress();
    } else {
      if(view != null)
      view.onNotOperational("");
      onHideProgress();
    }
  }


    @Override
    public void attachView(Object view) {
        
    }

    @Override
  public void detachView() {
    
  }
  

}

