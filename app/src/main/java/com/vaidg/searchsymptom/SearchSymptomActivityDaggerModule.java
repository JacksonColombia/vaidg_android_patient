package com.vaidg.searchsymptom;

import com.vaidg.Dagger2.ActivityScoped;
import dagger.Binds;
import dagger.Module;

/**
 * <h2>MainActivityDaggerModule</h2>
 * Created by Ali on 1/24/2018.
 */
@Module
public interface SearchSymptomActivityDaggerModule
{

    @Binds
    @ActivityScoped
    SearchSymptomActivityContract.SearchSymptomActivityPresenter provideSearchSymptomActivityPresenter(SearchSymptomActivityPresenterImpl searchSymptomActivityPresenter);

    @Binds
    @ActivityScoped
    SearchSymptomActivityContract.SearchSymptomActivityView provideSearchSymptomActivityView(SearchSymptomActivity searchSymptomActivity);
}
