package com.vaidg.payment_edit_card;

import android.util.Log;
import com.vaidg.model.ServerResponse;
import com.vaidg.model.card.DeleteCard;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * @author Pramod
 * @since 31-01-2018.
 */
public class CardEditPresenterImpl implements CardEditPresenter {

  @Inject
  CardEditView view;

  @Inject
  LSPServices lspServices;

  @Inject
  CompositeDisposable compositeDisposable;

  @Inject
  CardEditPresenterImpl() {
  }

  @Override
  public void deleteCard(String auth, String cardId) {
    if (view != null) {
      view.showProgress();
    }
    DeleteCard deleteCard = new DeleteCard(cardId);
    Observable<Response<ServerResponse>> bad = lspServices.deleteCard(auth, Constants.selLang,
        deleteCard);
    bad.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ServerResponse>>() {
          @Override
          public void onSubscribe(Disposable d) {
            compositeDisposable.add(d);
          }

          @Override
          public void onNext(Response<ServerResponse> value) {
            if (200 == value.code()) {
              if (view != null) {
                view.hideProgress();
                view.navToPayment();
              }
            } else {
              try {
                if (value.errorBody() != null) {
                  JSONObject errJson = new JSONObject(value.errorBody().string());
                  if (view != null) {
                    view.setErrorMsg(errJson.getString("message"));
                    view.hideProgress();
                  }
                }
              } catch (Exception e) {
                e.printStackTrace();
              }
            }
          }

          @Override
          public void onError(Throwable e) {
            Log.e("Error", "error" + e.getMessage());
            e.printStackTrace();
            if (view != null) {
              view.setErrorMsg(e.getMessage());
              view.hideProgress();
            }
          }

          @Override
          public void onComplete() {
          }
        });
  }

  @Override
  public void makeDefault(String auth, String cardId) {
    if (view != null) {
      view.showProgress();
    }
    Map<String, Object> jsonParams = new HashMap<>();
    jsonParams.put("cardId", cardId);
    Observable<Response<ServerResponse>> bad = lspServices.makeDefaultCard(auth, Constants.selLang,
        Constants.PLATFORM_ANDROID, jsonParams);
    bad.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ServerResponse>>() {
          @Override
          public void onSubscribe(Disposable d) {
            compositeDisposable.add(d);
          }

          @Override
          public void onNext(Response<ServerResponse> value) {
            if (200 == value.code()) {
              if (view != null) {
                view.hideProgress();
                view.navToPayment();
              }
            } else {
              try {
                if (value.errorBody() != null) {
                  JSONObject errJson = new JSONObject(value.errorBody().string());
                  if (view != null) {
                    view.setErrorMsg(errJson.getString("message"));
                    view.hideProgress();
                  }
                }
              } catch (Exception e) {
                e.printStackTrace();
              }
            }
          }

          @Override
          public void onError(Throwable e) {
            Log.e("Error", "error" + e.getMessage());
            e.printStackTrace();
            if (view != null) {
              view.setErrorMsg(e.getMessage());
              view.hideProgress();
            }
          }

          @Override
          public void onComplete() {
          }
        });
  }

}
