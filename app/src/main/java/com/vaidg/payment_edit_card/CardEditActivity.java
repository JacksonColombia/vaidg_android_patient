package com.vaidg.payment_edit_card;

import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.appscrip.stripe.AccountsDelegate;
import com.appscrip.stripe.UserAccounts;
import com.vaidg.R;
import com.vaidg.model.card.DeleteCard;
import com.vaidg.model.payment_method.CardDetail;
import com.vaidg.payment_method.PaymentMethodActivity;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.utility.AlertProgress;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;
import org.jetbrains.annotations.NotNull;

/**
 * @author Pramod
 * @since 16-01-2018.
 */

public class CardEditActivity extends DaggerAppCompatActivity implements CardEditView {

    private static String TAG="CardEditActivity";
    @Inject
    SessionManagerImpl sessionManager;
    @Inject
    CardEditPresenter cardEditPresenter;
    @BindView(R.id.tv_cardNumber)
    TextView tv_cardNumber;

    @BindView(R.id.btnMarkDefault) Button btnMarkDefault;
    @BindView(R.id.btnDeleteCard) Button btnDeleteCard;

    @BindView(R.id.iv_card_logo)
    ImageView iv_card_logo;

    @Inject
    AlertProgress alertProgress;

    //@BindString(R.string.wait_make_def_card) String wait_card;
    @BindView(R.id.tv_expiryDate)
    TextView tv_expiryDate;
    AlertDialog alertDialog;
  //  AlertDialog alertDialog;
    AlertDialog.Builder dialogBuilder;
    String cardId,auth;
    private boolean isDefault;

    @Inject
    AppTypeface appTypeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_card);
        ButterKnife.bind(this);
        initialize();
    }

    private void initialize() {
        alertDialog = alertProgress.getProgressDialog(this,getString(R.string.wait_card));
        //Setting toolbar
        Toolbar toolbar=  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        TextView tvTbTitle=toolbar.findViewById(R.id.tv_center);
        tvTbTitle.setText(R.string.your_card_details);
        tvTbTitle.setTypeface(appTypeface.getHind_semiBold());

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null) {
            CardDetail data = (CardDetail) bundle.getSerializable("data");
            AccountManager accountManager = AccountManager.get(this);

         //   auth  = Utility.getAuthToken(accountManager,sessionManager.getUSER_NAME());
            auth  = LSPApplication.getInstance().getAuthToken(sessionManager.getSID());
            if (data!=null) {
                cardId = data.getId();
                isDefault = data.isIsDefault();
                if (data.getLast4()!=null)
                    tv_cardNumber.setText("" + data.getLast4());
                if (data.getExpMonth()!=null && data.getExpYear()!=null)
                    tv_expiryDate.setText(" "+data.getExpMonth() +"/"+ data.getExpYear());
                if (data.getBrand()!=null)
                    iv_card_logo.setImageBitmap(Utility.setCreditCardLogo(data.getBrand(),this));

            }
        }

        if(!isDefault)
        {
            btnDeleteCard.setVisibility(View.VISIBLE);
            btnMarkDefault.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(R.id.btnMarkDefault)
    void setBtnMarkDefault() {
        Log.e(TAG,"Make Default");
        if (cardId!=null && auth!=null)
        {
            alertDialog = alertProgress.getProgressDialog(this,getString(R.string.saving));
            showProgress();
            if(Utility.isNetworkAvailable(this))
            cardEditPresenter.makeDefault(auth,cardId);
        }
    }

    @OnClick(R.id.btnDeleteCard)
    void setBtnDeleteCard() {
        Log.e(TAG,"Delete");
        if (cardId!=null && auth!=null)
        {
            alertProgress.alertPositiveNegativeOnclick(CardEditActivity.this, getString(R.string.want_to_delete), getString(R.string.system_error),getResources().getString(R.string.ok), getResources().getString(R.string.cancel),false , isClicked -> {
                if(isClicked)
                {
                    alertDialog = alertProgress.getProgressDialog(CardEditActivity.this,getString(R.string.deleting));
                    showProgress();
                    //cardEditPresenter.deleteCard(auth, cardId);
                  DeleteCard deleteCard = new DeleteCard(cardId);
                  UserAccounts.INSTANCE.deleteCard(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()),Constants.selLang, deleteCard.getCardId(),
                      new AccountsDelegate() {
                        @Override
                        public void onSuccess(@NotNull Object successData) {
                          runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                              hideProgress();
                              navToPayment();
                            }
                          });

                        }

                        @Override
                        public void onFailure(@NotNull String failure) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                   hideProgress();
                                }
                            });

                        }
                      });
                }
            });

        }
    }

    @Override
    public void navToPayment() {
        Intent intent = new Intent(CardEditActivity.this, PaymentMethodActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void showProgress() {
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.show();
            /*dialogBuilder = new AlertDialog.Builder(CardEditActivity.this);
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(R.layout.progress_dialog_layout, null);
            TextView tv_progress = dialogView.findViewById(R.id.tv_progress);
            tv_progress.setText("");
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);
            alertDialog = dialogBuilder.create();
            alertDialog.show();*/
        }
    }

    @Override
    public void hideProgress() {
        //progressBar.setVisibility(View.GONE);
        if (alertDialog!= null && alertDialog.isShowing())
            alertDialog.dismiss();
    }

    @Override
    public void setErrorMsg(String errorMsg) {
        Toast.makeText(this,""+errorMsg,Toast.LENGTH_LONG).show();
    }

}
