package com.vaidg.splashscreen;

import static com.vaidg.utilities.Constants.isJobDetailsOpen;
import static com.vaidg.utilities.Constants.latitude;
import static com.vaidg.utilities.Constants.longitude;
import static com.vaidg.utilities.Constants.selLang;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.vaidg.BuildConfig;
import com.vaidg.R;
import com.vaidg.home.MainActivity;
import com.vaidg.intro.IntroActivity;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.utility.AlertProgress;
import dagger.android.support.DaggerAppCompatActivity;
import java.security.MessageDigest;
import javax.inject.Inject;

/**
 * <h2>SplashActivity</h2>
 * <p>
 * Launching Activity or the landing page where login signUp button are there.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class SplashActivity extends DaggerAppCompatActivity implements SplashContract.View {
  private static final String TAG = SplashActivity.class.getName();
  @Inject
  SplashContract.Presenter splashPresenter;
  @Inject
  AlertProgress alertProgress;
  private String mCurrentVersion = "";
  private Context mContext;
  private boolean mUpdateFlag = true;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mContext = this;
    mCurrentVersion = BuildConfig.VERSION_NAME;
    splashPresenter.subscribeNetworkObserver();
    printHashKey();
  }

  @Override
  protected void onResume() {
    super.onResume();
    if (Utility.isNetworkAvailable(mContext)) {
      if (mUpdateFlag) {
        mUpdateFlag = false;
        splashPresenter.callUpdateVersionApi(mCurrentVersion, alertProgress);
      }
    } else {
      alertProgress.tryAgain(mContext, getString(R.string.pleaseCheckInternet), getString(R.string.systemError),
          isClicked -> {
            if (isClicked) {
              if (mUpdateFlag) {
                mUpdateFlag = false;
                splashPresenter.callUpdateVersionApi(mCurrentVersion, alertProgress);
              }
            }
          });
    }
  }

  @Override
  public void onToMainActivity() {
    isJobDetailsOpen = false;
    Intent intent = new Intent(mContext, MainActivity.class);
    startActivity(intent);
    onFinishCalled();
  }
  @Override
  public void callNextIntent(SessionManagerImpl sessionManager, AlertProgress alertProgress) {
    alertProgress.IPAddress((ipAddress, lat, lng) -> {
      sessionManager.setIpAddress(ipAddress);
      latitude = lat;
      longitude = lng;
    });
    if ("".equals(LSPApplication.getInstance().getAuthToken(sessionManager.getSID()))
        || LSPApplication.getInstance().getAuthToken(sessionManager.getSID()) == null
        || TextUtils.isEmpty(
        sessionManager.getSID().trim())) {
      Intent intent = new Intent(mContext, IntroActivity.class);
      startActivity(intent);
      onFinishCalled();
    } else {
      if(TextUtils.isEmpty(sessionManager.getRegisterId()))
      {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
          @Override
          public void onSuccess(InstanceIdResult instanceIdResult) {
            String refreshedToken = instanceIdResult.getToken();
            Log.d(TAG, "Refreshed token: " + refreshedToken);

            // If you want to send messages to this application instance or
            // manage this apps subscriptions on the server side, send the
            // Instance ID token to your app server.
            sessionManager.setRegisterId(refreshedToken);

          }
        });

      }
      if (!TextUtils.isEmpty(sessionManager.getFcmTopic())) {
        FirebaseMessaging.getInstance().subscribeToTopic(
                sessionManager.getFcmTopic());
      }
      selLang = sessionManager.getLanguageSettings().getCode();
      onToMainActivity();
    }
  }


  @Override
  public void onFinishCalled() {
    finish();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    splashPresenter.releaseSubscriber();
  }

  @Override
  protected void onPause() {
    super.onPause();
    mUpdateFlag = true;
  }

  @SuppressLint("PackageManagerGetSignatures")
  public void printHashKey() {
    try {
      final PackageInfo info = this.getPackageManager().getPackageInfo(this.getPackageName(),
          PackageManager.GET_SIGNATURES);
      for (Signature signature : info.signatures) {
        final MessageDigest md = MessageDigest.getInstance("SHA");
        md.update(signature.toByteArray());
        final String hashKey = new String(Base64.encode(md.digest(), 0));
        // Log.i("Facebook", "key:" + hashKey + "=");
      }
    } catch (Exception e) {
      // Log.e("Facebook", "error:", e);
    }
  }

  @Override
  public void onSessionExpired() {
  }

  @Override
  public void onLogout(String message, SessionManagerImpl sessionManager) {
  }

  @Override
  public void onError(String error) {
  }

  @Override
  public void onConnectionError(String connectionError) {
  }

  @Override
  public void onShowProgress() {
  }

  @Override
  public void onHideProgress() {
    mUpdateFlag = true;
  }
}