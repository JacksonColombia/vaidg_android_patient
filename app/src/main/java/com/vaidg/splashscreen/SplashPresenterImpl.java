package com.vaidg.splashscreen;

import static com.vaidg.utilities.Constants.ANDROID_CUSTOMER_USERTYPE;
import static com.vaidg.utilities.Constants.BASE_AUTH_PASSWORD;
import static com.vaidg.utilities.Constants.BASE_AUTH_USERNAME;
import static com.vaidg.utilities.Constants.INTERNAL_SERVER_ERROR;
import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;
import static com.vaidg.utilities.Constants.selLang;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.text.TextUtils;
import com.vaidg.BuildConfig;
import com.vaidg.R;
import com.vaidg.RxObservers.RxNetworkObserver;
import com.vaidg.intro.IntroActivity;
import com.vaidg.networking.BasicAuthentication;
import com.vaidg.networking.LSPServices;
import com.vaidg.networking.NetworkStateHolder;
import com.vaidg.splashscreen.model.AppUpdateDataPojo;
import com.vaidg.splashscreen.model.AppUpdatePojo;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.google.gson.Gson;
import com.utility.AlertProgress;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * <h2>SplashPresenterImpl</h2>
 * <p>
 * This class is used to call the API.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class SplashPresenterImpl implements SplashContract.Presenter {
  private static final String TAG = SplashPresenterImpl.class.getName();
  @Inject
  SplashContract.View view;
  @Inject
  LSPServices lspServices;
  @Inject
  Gson gson;
  @Inject
  Context context;
  @Inject
  SessionManagerImpl sessionManager;
  private RxNetworkObserver mRxNetworkObserver;
  private NetworkStateHolder mNetworkStateHolder;
  private CompositeDisposable mCompositeDisposable;
  private String mAppVersion = "";
  private boolean mMandatory = false;

  @Inject
  SplashPresenterImpl() {
    this.mNetworkStateHolder = new NetworkStateHolder();
    this.mRxNetworkObserver = new RxNetworkObserver();
  }

  @Override
  public void subscribeNetworkObserver() {
    Observer<NetworkStateHolder> observer = new Observer<NetworkStateHolder>() {
      @Override
      public void onSubscribe(Disposable d) {
        mCompositeDisposable.add(d);
      }

      @Override
      public void onNext(NetworkStateHolder value) {
      }

      @Override
      public void onError(Throwable e) {
        e.printStackTrace();
      }

      @Override
      public void onComplete() {
      }
    };
    mRxNetworkObserver.subscribeOn(Schedulers.io());
    mRxNetworkObserver.observeOn(AndroidSchedulers.mainThread());
    mRxNetworkObserver.subscribe(observer);
  }

  @Override
  public boolean checkInternet() {
    return mNetworkStateHolder.isConnected();
  }

  @Override
  public void releaseSubscriber() {
  }

  @Override
  public void callUpdateVersionApi(String currentVersion,
                                   AlertProgress alertProgress) {
    lspServices = BasicAuthentication.ServiceGenerator.createService(LSPServices.class,
        BASE_AUTH_USERNAME,
        BASE_AUTH_PASSWORD);
    Observable<Response<ResponseBody>> obsResponseBody = lspServices.onTOGetAppVersion(
        selLang, PLATFORM_ANDROID, ANDROID_CUSTOMER_USERTYPE, BuildConfig.VERSION_NAME);
    obsResponseBody.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {
          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int code = responseBodyResponse.code();
            Resources res = context.getResources();
            JSONObject jsonObject;
            try {
              String response = responseBodyResponse.body() != null
                  ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null
                  ? responseBodyResponse.errorBody().string() : null;
              switch (code) {
                case SUCCESS_RESPONSE:
                  if (response != null && !response.isEmpty()) {
                    AppUpdatePojo appUpdatePojo = gson.fromJson(response, AppUpdatePojo.class);
                    if (appUpdatePojo.getData() != null) {
                      AppUpdateDataPojo appUpdateDataPojo = appUpdatePojo.getData();
                      mAppVersion = appUpdateDataPojo.getAppVersion();
                      mMandatory = appUpdateDataPojo.isMandatory();
                      if (mMandatory) {
                        alertProgress.alertPositiveOnclick(context,
                            res.getString(R.string.msgMandatoryupdate),
                            res.getString(R.string.app_name),
                            context.getResources().getString(R.string.update),
                            isClicked -> {
                              if (isClicked) {
                                final String appPackageName = context.getPackageName();
                                try {
                                  context.startActivity(new Intent(Intent.ACTION_VIEW,
                                      Uri.parse(BuildConfig.ANDROID_MARKET_APP + appPackageName)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                  context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(
                                      BuildConfig.ANDROID_PLAYSTORE_LINK
                                          + appPackageName)));
                                }
                              }
                            });
                      } else {
                        if(view != null)
                        view.callNextIntent(sessionManager, alertProgress);
                      }
                    }
                  }
                  break;
                case INTERNAL_SERVER_ERROR:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      alertProgress.alertinfo(context, jsonObject.getString(MESSAGE));
                      if(view != null)
                      view.callNextIntent( sessionManager, alertProgress);
                    }
                  } else {
                    alertProgress.alertinfo(context, res.getString(R.string.internalServerError));
                  }
                  break;
                default:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    jsonObject = new JSONObject(errorBody);
                    jsonObject.getString(MESSAGE);
                    if (!jsonObject.getString(
                        MESSAGE).isEmpty()) {
                      alertProgress.alertinfo(context, jsonObject.getString(MESSAGE));
                      if(view != null)
                      view.callNextIntent(sessionManager, alertProgress);
                    }
                  }
                  break;
              }
            } catch (Exception e) {
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            Utility.printLog(TAG, e.getMessage());
            Utility.printLog(TAG, e.getLocalizedMessage());
          }

          @Override
          public void onComplete() {
          }
        });
  }


  @Override
  public void attachView(Object view) {
  }

  @Override
  public void detachView() {
  }
}