package com.vaidg.splashscreen;

import android.app.Activity;
import com.vaidg.Dagger2.ActivityScoped;
import com.utility.NotificationHandler;
import dagger.Binds;
import dagger.Module;

/**
 * <h2>SplashModule</h2>
 * <p>
 * This is a Dagger module. We use this to pass in the View dependency to the
 * {@link SplashPresenterImpl}.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
@Module
public interface SplashModule {

  @ActivityScoped
  @Binds
  Activity provideNotificationActivity(NotificationHandler notificationHandler);

  @ActivityScoped
  @Binds
  SplashContract.Presenter presenter(SplashPresenterImpl presenter);

  @ActivityScoped
  @Binds
  SplashContract.View splashView(SplashActivity splashActivity);
}