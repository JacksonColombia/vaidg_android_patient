package com.vaidg.splashscreen.model;

import static com.vaidg.utilities.Constants.DATA;
import static com.vaidg.utilities.Constants.MESSAGE;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppUpdatePojo implements Parcelable {
  public final static Parcelable.Creator<AppUpdatePojo> CREATOR = new Creator<AppUpdatePojo>() {
    @SuppressWarnings({"unchecked"})
    public AppUpdatePojo createFromParcel(Parcel in) {
      return new AppUpdatePojo(in);
    }

    public AppUpdatePojo[] newArray(int size) {
      return (new AppUpdatePojo[size]);
    }
  };
  @SerializedName(MESSAGE)
  @Expose
  private String message;
  @SerializedName(DATA)
  @Expose
  private AppUpdateDataPojo data;

  protected AppUpdatePojo(Parcel in) {
    this.message = ((String) in.readValue((String.class.getClassLoader())));
    this.data = ((AppUpdateDataPojo) in.readValue((AppUpdateDataPojo.class.getClassLoader())));
  }

  /**
   * No args constructor for use in serialization
   */
  public AppUpdatePojo() {
  }

  /**
   * @param data
   * @param message
   */
  public AppUpdatePojo(String message, AppUpdateDataPojo data) {
    super();
    this.message = message;
    this.data = data;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public AppUpdateDataPojo getData() {
    return data;
  }

  public void setData(AppUpdateDataPojo data) {
    this.data = data;
  }

  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(message);
    dest.writeValue(data);
  }

  public int describeContents() {
    return 0;
  }
}
