package com.vaidg.splashscreen.model;

import static com.vaidg.utilities.Constants.APPVERSION;
import static com.vaidg.utilities.Constants.MANDATORY;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppUpdateDataPojo implements Parcelable {
  public final static Parcelable.Creator<AppUpdateDataPojo> CREATOR = new Creator<AppUpdateDataPojo>() {
    @SuppressWarnings({"unchecked"})
    public AppUpdateDataPojo createFromParcel(Parcel in) {
      return new AppUpdateDataPojo(in);
    }

    public AppUpdateDataPojo[] newArray(int size) {
      return (new AppUpdateDataPojo[size]);
    }
  };
  @SerializedName(APPVERSION)
  @Expose
  private String appVersion;
  @SerializedName(MANDATORY)
  @Expose
  private boolean mandatory;

  protected AppUpdateDataPojo(Parcel in) {
    this.appVersion = ((String) in.readValue((String.class.getClassLoader())));
    this.mandatory = ((boolean) in.readValue((boolean.class.getClassLoader())));
  }

  /**
   * No args constructor for use in serialization
   */
  public AppUpdateDataPojo() {
  }

  /**
   * @param appVersion
   * @param mandatory
   */
  public AppUpdateDataPojo(String appVersion, boolean mandatory) {
    super();
    this.appVersion = appVersion;
    this.mandatory = mandatory;
  }

  public String getAppVersion() {
    return appVersion;
  }

  public void setAppVersion(String appVersion) {
    this.appVersion = appVersion;
  }

  public boolean isMandatory() {
    return mandatory;
  }

  public void setMandatory(boolean mandatory) {
    this.mandatory = mandatory;
  }

  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(appVersion);
    dest.writeValue(mandatory);
  }

  public int describeContents() {
    return 0;
  }
}