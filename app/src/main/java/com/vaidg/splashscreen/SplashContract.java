package com.vaidg.splashscreen;

import android.content.Context;
import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;
import com.vaidg.utilities.SessionManagerImpl;
import com.utility.AlertProgress;

/**
 * <h2>SplashContract</h2>
 * <p>
 * This class is used to act as a link between view and presenter.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public interface SplashContract {
  interface View extends BaseView {
    /**
     * <h2>onFinishCalled</h2>
     * <p>This method is used to finish the activity</p>
     */
    void onFinishCalled();

    /**
     * <h2>onToMainActivity</h2>
     * <p>This method is used to navigate to another activity</p>
     */
    void onToMainActivity();
    /**
     * <h2>callNextIntent</h2>
     * <p>This method is used to navigate to another activity</p>
     *
     * @param sessionManager  This is use to store data in preference
     * @param alertProgress  This is use to show alert message
     *
     */
    void callNextIntent(SessionManagerImpl sessionManager, AlertProgress alertProgress);
  }

  interface Presenter extends BasePresenter {
    /**
     * <h2>subscribeNetworkObserver</h2>
     * <p>This method is used subscribe the network is there or not</p>
     */
    void subscribeNetworkObserver();

    /**
     * <h2>releaseSubscriber</h2>
     * <p>This method is used unsubcribe the network is subscribe on activity finish</p>
     *
     */
    void releaseSubscriber();

    /**
     * <h2>checkInternet</h2>
     * <p>This method is used to check is internet is conencted or disconnected</p>
     */
    boolean checkInternet();

    /**
     * <h2>callUpdateVersionApi</h2>
     * <p>This method is used to check the version of the application which is installed in mobile device</p>
     *  @param currentVersion The current version of the application.
     * @param alertProgress  This is use to show alert message
     */
    void callUpdateVersionApi(String currentVersion, AlertProgress alertProgress);
  }
}