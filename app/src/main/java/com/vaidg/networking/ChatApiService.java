package com.vaidg.networking;

import static com.vaidg.utilities.Constants.AUTHORIZATION;
import static com.vaidg.utilities.Constants.LAN;

import java.util.Map;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import io.reactivex.Observable;
import retrofit2.http.Query;

/**
 * @author Pramod
 * @since 15 Jan-2018
 */

public interface ChatApiService {

 /*   @GET ("/chatHistory/{bookingId}/{pageNo}")
    Observable<Response<ResponseBody>> getChatHistory(@Header(AUTHORIZATION)String auth,
                                                @Header(LAN)int lang,
                                                @Path("bookingId")long bookingId,
                                                @Path("pageNo")int pageNo);

    @FormUrlEncoded
    @POST("/message")
    Observable<Response<ResponseBody>> postMessage(@Header(AUTHORIZATION)String auth,
                                                   @Header(LAN)int lang,
                                                   @Field("type")int type,
                                                   @Field("timestamp")long timeStamp,
                                                   @Field("content")String msgContent,
                                                   @Field("fromID")String customerId,
                                                   @Field("bid")String bid,
                                                   @Field("targetId")String proId);

    @Multipart
    @POST("/upload")
    Call<ResponseBody> upload(
            @Part("bookingId") RequestBody description,
            @Part MultipartBody.Part file);*/


    @GET("/getCityList")
    Observable<Response<ResponseBody>> getCty();


  //  @FormUrlEncoded
    @POST("/call")
    Observable<Response<ResponseBody>> initCall(@Header(AUTHORIZATION)String auth,
                                                @Header(LAN)String lang,
                                                @Body Map<String, Object> objectMap);
    @GET("/call")
    Observable<Response<ResponseBody>> checkIsAvailable(@Header(AUTHORIZATION)String auth,
                                                        @Header(LAN)String lang,
                                                        @Query("callId") String callId);

    @PUT("/call")
    Observable<Response<ResponseBody>> callAnswer(@Header(AUTHORIZATION)String auth,
                                                  @Header(LAN)String lang,
                                                  @Query("callId") String callId);

    //@FormUrlEncoded
    @HTTP(method = "DELETE", path = "/call",hasBody = true)
    Observable<Response<ResponseBody>> endCall(@Header(AUTHORIZATION)String auth,
                                               @Header(LAN)String lang,
                                               @Field("callId") String callId,
                                               @Field("callFrom") String callFrom);



}
