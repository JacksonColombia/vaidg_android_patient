package com.vaidg.networking;

import static com.vaidg.SlideLayout.SlideLayout.TAG;
import static com.vaidg.utilities.Constants.AUTHORIZATION;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;

import com.vaidg.BuildConfig;
import com.vaidg.sslcertificate.SSLCertificate;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;
import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class BasicAuthentication {
  public static boolean isConnected() {
    ConnectivityManager connectivityManager = (ConnectivityManager) LSPApplication.getInstance().getSystemService(
        Context.CONNECTIVITY_SERVICE);
    NetworkInfo netInfo =
        connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
    return (netInfo != null && netInfo.isConnected());
  }

  public static class ServiceGenerator {

    private static OkHttpClient.Builder httpClient = new OkHttpClient().newBuilder();//SSLCertificate.getSSLCertificate(LSPApplication.get()).newBuilder();
    private static Retrofit.Builder builder =
        new Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create());
    private static Retrofit retrofit = builder.build();

    public static <S> S createService(Class<S> serviceClass, String username, String password) {
      if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
        String authToken = Credentials.basic(username, password);
        return createService(serviceClass, authToken);
      }
      return createService(serviceClass, null);
    }

    public static <S> S createService(Class<S> serviceClass, final String authToken) {
      if (!TextUtils.isEmpty(authToken)) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        AuthenticationInterceptor interceptor = new AuthenticationInterceptor(authToken);
        if (!httpClient.interceptors().contains(interceptor)) {
          httpClient.readTimeout(50, TimeUnit.SECONDS);
          httpClient.writeTimeout(30, TimeUnit.SECONDS);
          httpClient.connectTimeout(80, TimeUnit.SECONDS);
          httpClient.retryOnConnectionFailure(false);
          httpClient.addInterceptor(interceptor);
          httpClient.addInterceptor(loggingInterceptor);
          builder.client(httpClient.build());
          retrofit = builder.build();
        }
      }
      return retrofit.create(serviceClass);
    }

    public <S> S createService(Class<S> serviceClass) {
      return createService(serviceClass, null, null);
    }
  }

  private static class AuthenticationInterceptor implements Interceptor {
    private String authToken;

    public AuthenticationInterceptor(String token) {
      this.authToken = token;
    }

    @Override
    public okhttp3.Response intercept(Chain chain) throws IOException {
      if (!isConnected()) {
        throw new NoConnectivityException();
        // Throwing our custom exception 'NoConnectivityException'
      }
      okhttp3.Request original = chain.request();
      okhttp3.Request.Builder builder = original.newBuilder()
          .header(AUTHORIZATION, authToken);
      okhttp3.Request request = builder.build();
      return chain.proceed(request);
    }
  }


}
