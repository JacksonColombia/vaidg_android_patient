package com.vaidg.networking;

import static com.vaidg.utilities.Constants.AUTHORIZATION;
import static com.vaidg.utilities.Constants.LAN;
import static com.vaidg.utilities.Constants.PLATFORM;
import static com.vaidg.utilities.Constants.REFRESHTOKEN;
import static com.vaidg.utilities.Constants.USERTYPE;


import com.vaidg.incalloutcalltelecall.dependent.model.SpecialRequirementResponse;
import com.vaidg.model.ForgotPwdReq;
import com.vaidg.model.ForgotPwdResponse;
import com.vaidg.model.ProfileResponse;
import com.vaidg.model.ServerOtpResponse;
import com.vaidg.model.ServerResponse;
import com.vaidg.model.SignUpReq;
import com.vaidg.model.SignUpResponse;
import com.vaidg.model.ValidationReq;
import com.vaidg.model.card.DeleteCard;
import com.vaidg.model.payment_method.CardGetResponse;
import com.google.gson.JsonObject;
import com.pojo.EditProfileBody;
import com.pojo.RequestPojo.OtpValidationRequestPojo;
import com.pojo.RequestPojo.PhoneValidationRequestPojo;
import io.reactivex.Observable;
import java.util.Map;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * @author Pramod
 * @since 15 Dec-2017
 */

public interface LSPServices {

  //@Header(LAN) String lang
  //Guest Login API
  // @FormUrlEncoded
  @POST("customer/guestSignIn")
  Observable<Response<ResponseBody>> doGuestLogin(@Header(LAN) String lang,
                                                  @Header(PLATFORM) String platform,
                                                  @Body Map<String, Object> stringObjectMap);

  //@Header(LAN) String lang,
  //LoginResponse
  @POST("customer/signIn")
  Observable<Response<ResponseBody>> performLogin(@Header(LAN) String lang,
                                                  @Header(PLATFORM) String platform,
                                                  @Body Map<String, Object> stringObjectMap);


  @POST("customer/logout")
  Observable<Response<ServerResponse>> logout(@Header(AUTHORIZATION) String authorization,
                                              @Header(LAN) String lang, @Header(PLATFORM) String platform);


  @GET("customer/profile/me")
  Observable<Response<ResponseBody>> getProfile(@Header(AUTHORIZATION) String authorization,
                                                   @Header(LAN) String lang, @Header(PLATFORM) String platform);


  @PATCH("customer/profile/me")
  Observable<Response<ResponseBody>> editProfile(@Header(AUTHORIZATION) String authorization,
                                                   @Header(LAN) String lang, @Header(PLATFORM) String platform,
                                                   @Body EditProfileBody editProfileBody);

  //  @GET("customer/categories")
  @GET("customer/categories")
  Observable<Response<ResponseBody>> getCategories(@Header(AUTHORIZATION) String authorization,
                                                   @Header(LAN) String lang,
                                                   @Header(PLATFORM) String platform,
                                                   @Query("ipAddress") String ipAddress,
                                                   @Query("search") String search,
                                                   @Query("lat") double lat,
                                                   @Query("long") double lng,
                                                   @Query("cityId") String cityId);

  @GET("customer/bookings/prescription")
  Observable<Response<ResponseBody>> getPrescription(@Header(AUTHORIZATION) String authorization,
                                                   @Header(LAN) String lang,
                                                   @Header(PLATFORM) String platform,
                                                   @Query("dependentId") String dependentId,
                                                   @Query("from") String from,
                                                   @Query("to") String to,
                                                   @Query("filterBaseOnDate") String filterBaseOnDate,
                                                   @Query("limit") Double limit,
                                                   @Query("skip") Double skip);


  @PATCH("customer/password")
  Observable<Response<ServerResponse>> changePassword(@Header(LAN) String lang,
                                                      @Header(PLATFORM) String platform,
                                                      @Body Map<String, Object> password);

  // @FormUrlEncoded
  @PATCH("customer/password/me")
  Observable<Response<ServerResponse>> profChangePwd(@Header(AUTHORIZATION) String authorization,
                                                     @Header(LAN) String lang, @Header(PLATFORM) String platform,
                                                     @Body Map<String, Object> stringObjectMap);

  @POST("customer/registerUser")
  Observable<Response<SignUpResponse>> doRegister(@Header(LAN) String lang,
                                                  @Header(PLATFORM) String platform,
                                                  @Body SignUpReq signUpReq);

  @POST("customer/emailValidation")
  Observable<Response<ServerResponse>> checkEmailExists(@Header(LAN) String lang,
                                                        @Header(PLATFORM) String platform, @Body ValidationReq validationReq);

  @POST("customer/phoneNumberValidation")
  Observable<Response<ServerResponse>> checkPhoneExists(@Header(LAN) String lang,
                                                        @Header(PLATFORM) String platform, @Body PhoneValidationRequestPojo jsonObject);

  @POST("customer/forgotPassword")
  Observable<Response<ForgotPwdResponse>> forgotPassword(@Header(LAN) String lang,
                                                         @Header(PLATFORM) String platform, @Body ForgotPwdReq forgotPwdReq);


  @POST("customer/verifyPhoneNumber")
  Observable<Response<ServerOtpResponse>> verifyPhone(@Header(LAN) String lang,
                                                      @Header(PLATFORM) String platform, @Body Map<String, Object> verifyPhone);

  @POST("customer/verifyVerificationCode")
  Observable<Response<ServerResponse>> verifyOtp(@Header(LAN) String lang,
                                                 @Header(PLATFORM) String platform, @Body OtpValidationRequestPojo requestPojo);

  //Change Email API
  // @FormUrlEncoded
  @PATCH("customer/email")
  Observable<Response<ServerResponse>> changeEmail(@Header(AUTHORIZATION) String authorization,
                                                   @Header(LAN) String lang,
                                                   @Header(PLATFORM) String platform,
                                                   @Body Map<String, Object> stringObjectMap);

  //Change phone number API
  // @FormUrlEncoded
  @PATCH("customer/phoneNumber")
  Observable<Response<ServerResponse>> changePhoneNo(@Header(AUTHORIZATION) String authorization,
                                                     @Header(LAN) String lang,
                                                     @Header(PLATFORM) String platform,
                                                     @Body Map<String, Object> stringObjectMap);


  @POST("customer/resendOtp")
  Observable<Response<ServerResponse>> resendOtp(@Header(LAN) String lang,
                                                 @Header(PLATFORM) String platform,
                                                 @Body Map<String, Object> objectMap);


  @GET("card")
  Observable<Response<CardGetResponse>> getCard(@Header(AUTHORIZATION) String authorization,
                                                @Header(LAN) String lang, @Header(PLATFORM) String platform);

  // @FormUrlEncoded
  @POST("card")
  Observable<Response<ServerResponse>> addCard(@Header(AUTHORIZATION) String authorization,
                                               @Header(LAN) String lang,
                                               @Header(PLATFORM) String platform,
                                               @Body Map<String, Object> stringObjectMap);

  // @FormUrlEncoded
  @PATCH("card")
  Observable<Response<ServerResponse>> makeDefaultCard(
      @Header(AUTHORIZATION) String authorization,
      @Header(LAN) String lang, @Header(PLATFORM) String platform,
      @Body Map<String, Object> stringObjectMap);

  @HTTP(method = "DELETE", path = "card", hasBody = true)
  Observable<Response<ServerResponse>> deleteCard(@Header(AUTHORIZATION) String authorization,
                                                  @Header(LAN) String lang,
                                                  @Body DeleteCard deleteCard);


  //ServerResponse
  // @FormUrlEncoded
  // @Headers("Content-Type: application/json")
  @POST("customer/location")
  Observable<Response<ResponseBody>> getLocation(@Header(AUTHORIZATION) String authorization,
                                                 @Header(LAN) String selLang, @Header(PLATFORM) String platform,
                                                 @Body Map<String, Object> stringObjectMap);


  // GET customer/provider/{lat}/{long}/{categoryId}/{ipAddress}

  //@GET("customer/provider/{lat}/{long}/{categoryId}/{ipAddress}")
  // @FormUrlEncoded
  // @Headers("Content-Type: application/json")
  @POST("customer/provider")
  Observable<Response<ResponseBody>> getProviders(@Header(AUTHORIZATION) String authorization,
                                                  @Header(LAN) String selLang, @Header(PLATFORM) String platform,
                                                  @Body Map<String, Object> objectMap);


  @GET("customer/address")
  Observable<Response<ResponseBody>> getAddress(@Header(AUTHORIZATION) String authorization,
                                                @Header(LAN) String lan, @Header(PLATFORM) String platform);

  //@FormUrlEncoded
  @POST("customer/address")
  Observable<Response<ResponseBody>> addAddress(@Header(AUTHORIZATION) String authorization,@Header(LAN) String lan, @Header(PLATFORM) String platform,
                                                @Body Map<String, Object> addAddress);

  //@FormUrlEncoded
  @PATCH("customer/address")
  Observable<Response<ResponseBody>> editAddress(@Header(AUTHORIZATION) String authorization,
                                                 @Header(LAN) String lngId, @Header(PLATFORM) String platform,
                                                 @Body Map<String, Object> editAddress);

  @DELETE("customer/address")
  Observable<Response<ResponseBody>> deleteAddress(@Header(AUTHORIZATION) String authorization,
                                                   @Header(LAN) String lang, @Header(PLATFORM) String platform,
                                                   @Query("id") String id);


  @GET("customer/support")
  Observable<Response<ResponseBody>> getFAQ(@Header(AUTHORIZATION) String authorization, @Header(LAN) String lanId, @Header(PLATFORM) String platform,
                                           @Query(USERTYPE) Integer userType);


  //GET customer/providerDetails/{providerId}/{categoryId}/{lat}/{long}
  @GET("customer/providerDetails")
  Observable<Response<ResponseBody>> getProviderDetails(
      @Header(AUTHORIZATION) String authorization,
      @Header(LAN) String lanId,
      @Header(PLATFORM) String platform,
      @Query("providerId") String proId,
      @Query("categoryId") String CatId,
      @Query("lat") double lat,
      @Query("long") double lng,
      @Query("ipAddress") String ipAddress,
      @Query("callType") int callType);

  //customer/accessToken
  @GET("customer/accessToken")
  Observable<Response<ResponseBody>> getAccessToken(@Header(AUTHORIZATION) String authorization,
                                                    @Header(REFRESHTOKEN) String refreshToken,
                                                    @Header(LAN) String lanId, @Header(PLATFORM) String platform);

  //GET customer/services/{categoryId}/{providerId}


  @GET("customer/services")
  Observable<Response<ResponseBody>> getSubServices(@Header(AUTHORIZATION) String authorization,
                                                    @Header(LAN) String lanId,
                                                    @Header(PLATFORM) String platform,
                                                    @Query("categoryId") String catId,
                                                    @Query("providerId") String proId);

  //http://45.77.190.140:9999customer/cart
  // @FormUrlEncoded
  @POST("customer/cart")
  Observable<Response<ResponseBody>> onCatModification(
      @Header(AUTHORIZATION) String authorization,
      @Header(LAN) String lanId, @Header(PLATFORM) String platform,
      @Body Map<String, Object> objectMap);

  //GET customer/cart/{categoryId}/{providerId}
  @GET("customer/cart")
  Observable<Response<ResponseBody>> getSubCart(@Header(AUTHORIZATION) String authorization,
                                                @Header(LAN) String lan, @Header(PLATFORM) String platform,
                                                @Query("categoryId") String catId,
                                                @Query("providerId") String proId,
                                                @Query("callType") int callType,
                                                @Query("bookingType") int bookingType);

  // @FormUrlEncoded
  @POST("customer/booking")
  Observable<Response<ResponseBody>> onLiveBooking(@Header(AUTHORIZATION) String authorization,
                                                   @Header(LAN) String lng, @Header(PLATFORM) String platform,
                                                   @Body Map<String, Object> objectMap);

  // @FormUrlEncoded
  @PATCH("customer/responseBooking")
  Observable<Response<ResponseBody>> onBookingHire(@Header(AUTHORIZATION) String authorization,
                                                   @Header(LAN) String lng, @Header(PLATFORM) String platform,
                                                   @Body Map<String, Object> stringObjectMap);


  @GET("customer/bookings")
  Observable<Response<ResponseBody>> onToGetAllBookings(
      @Header(AUTHORIZATION) String authorization,
      @Header(LAN) String lng, @Header(PLATFORM) String platform);

  @GET("customer/bookings")
  Observable<Response<ResponseBody>> onToGetAllBookingsUpComing(
      @Header(AUTHORIZATION) String authorization,
      @Header(LAN) String lng, @Header(PLATFORM) String platform,
      @Query("from") long fromDate,
      @Query("to") long toDate);

  @GET("customer/booking")
  Observable<Response<ResponseBody>> onToGetBookingDetails(
      @Header(AUTHORIZATION) String authorization,
      @Header(LAN) String lng,
      @Header(PLATFORM) String platform,
      @Query("bookingId") long bid);

  @GET("customer/booking/invoice")
  Observable<Response<ResponseBody>> onToGetInvoiceDetails(
      @Header(AUTHORIZATION) String authorization,
      @Header(LAN) String lng
      , @Header(PLATFORM) String platform,
      @Query("bookingId") long bid);


  //@FormUrlEncoded
  @POST("customer/reviewAndRating")
  Observable<Response<ResponseBody>> onUpdateReview(@Header(AUTHORIZATION) String authorization,
                                                    @Header(LAN) String lng
                                                    , @Header(PLATFORM) String platform,
                                                    @Body /*Map<String, Object>*/  JsonObject stringObjectMap);

  @GET("customer/reviewAndRatingPending")
  Observable<Response<ResponseBody>> onTOGetPendingBooking(
      @Header(AUTHORIZATION) String authorization,
      @Header(LAN) String lng, @Header(PLATFORM) String platform);


  @GET("customer/bookingPaymentPending")
  Observable<Response<ResponseBody>> onTOGetPendingInvoiceBooking(
      @Header(AUTHORIZATION) String authorization,
      @Header(LAN) String lng, @Header(PLATFORM) String platform);


  //GET /zendesk/user/ticket/{emailId}
  @GET("zendesk/user/ticket")
  Observable<Response<ResponseBody>> onToGetZendeskTicket(
      @Header(AUTHORIZATION) String authorization,
      @Header(LAN) String lng, @Header(PLATFORM) String platform,
      @Query("emailId") String emailId);

  // @FormUrlEncoded
  @PUT("zendesk/ticket/comments")
  Observable<Response<ResponseBody>> commentOnTicket(@Header(AUTHORIZATION) String authorization,
                                                     @Header(LAN) String lng,
                                                     @Header(PLATFORM) String platform,
                                                     @Body Map<String, Object> stringObjectMap);

  @GET("zendesk/ticket/history")
  Observable<Response<ResponseBody>> onToGetZendeskHistory(
      @Header(AUTHORIZATION) String authorization,
      @Header(LAN) String lng,
      @Header(PLATFORM) String platform,
      @Query("id") int zenId);

  //@FormUrlEncoded
  @POST("zendesk/ticket")
  Observable<Response<ResponseBody>> createTicket(@Header(AUTHORIZATION) String authorization,
                                                  @Header(LAN) String lng,
                                                  @Header(PLATFORM) String platform,
                                                  @Body Map<String, Object> stringObjectMap);


  @GET("chatHistory")
  Observable<Response<ResponseBody>> getChatHistory(@Header(AUTHORIZATION) String auth,
                                                    @Header(LAN) String lang,
                                                    @Header(PLATFORM) String platform,
                                                    @Query("bookingId") long bookingId,
                                                    @Query("providerId") String proId,
                                                    @Query("pageNo") int pageNo);


  @GET("publickKey")
  Observable<Response<ResponseBody>> getPublicKey(@Header(AUTHORIZATION) String auth,
                                                    @Header(LAN) String lang,
                                                    @Header(PLATFORM) String platform,
                                                    @Query("userType") String userType,
                                                    @Query("userId") String userId);


  //@FormUrlEncoded
  @POST("message")
  Observable<Response<ResponseBody>> postMessage(@Header(AUTHORIZATION) String auth,
                                                 @Header(LAN) String lang,
                                                 @Header(PLATFORM) String platform,
                                                 @Body Map<String, Object> stringObjectMap);


  @Multipart
  @POST("uploadImage")
  Call<ResponseBody> upload(
      @Part MultipartBody.Part file);

  @Multipart
  @POST("imageUpload")
  Call<ResponseBody> image(@Part MultipartBody.Part file, @Part("uploadTo") RequestBody uploadTo, @Part("folder") RequestBody folder);// @Part("bookingId") RequestBody description,

  @GET("customer/cancelReasons")
  Observable<Response<ResponseBody>> onCancelReasons(@Header(AUTHORIZATION) String authorization,
                                                     @Header(LAN) String lan,
                                                     @Header(PLATFORM) String platform,
                                                     @Query("bookingId") String userType);

  // PATCH customer/cancelBooking
  // @FormUrlEncoded
  @PATCH("customer/cancelBooking")
  Observable<Response<ResponseBody>> cancelBooking(@Header(AUTHORIZATION) String authorization,
                                                   @Header(LAN) String lng,
                                                   @Header(PLATFORM) String platform,
                                                   @Body Map<String, Object> stringObjectMap);

  @GET("customer/lastDues")
  Observable<Response<ResponseBody>> lastDues(@Header(AUTHORIZATION) String authorization,
                                              @Header(LAN) String lng,
                                              @Header(PLATFORM) String platform);

  @GET("customer/wallet")
  Observable<Response<ResponseBody>> getWalletLimits(@Header(AUTHORIZATION) String authorization,
                                                     @Header(LAN) String lng,
                                                     @Header(PLATFORM) String platform);

  // @FormUrlEncoded
  @POST("customer/wallet/recharge")
  Observable<Response<ResponseBody>> rechargeWallet(@Header(AUTHORIZATION) String authorization,
                                                    @Header(LAN) String lng,
                                                    @Header(PLATFORM) String platform,
                                                    @Body Map<String, Object> stringObjectMap);

  @GET("customer/wallet/transction")
  Observable<Response<ResponseBody>> getWalletTransaction(
      @Header(AUTHORIZATION) String authorization,
      @Header(LAN) String lng,
      @Header(PLATFORM) String platform,
      @Query("pageIndex") int pageIndex);


  @GET("customer/providerReview")
  Observable<Response<ResponseBody>> getReviews(@Header(AUTHORIZATION) String authorization,
                                                @Header(LAN) String lng,
                                                @Header(PLATFORM) String platform,
                                                @Query("providerId") String proId,
                                                @Query("categoryId") String catId,
                                                @Query("pageNo") int page);

  @GET("server/serverTime")
  Observable<Response<ResponseBody>> onTogetServerTime(@Header(LAN) String selLang,
                                                       @Header(PLATFORM) String platform);

  // @FormUrlEncoded
  @POST("customer/promoCodeValidation")
  Observable<Response<ResponseBody>> postPromoCodeValidation(
      @Header(AUTHORIZATION) String authorization,
      @Header(LAN) String lang, @Header(PLATFORM) String platform,
      @Body Map<String, Object> objectMap);

  @GET("customer/promoCode")
  Observable<Response<ResponseBody>> getPromoCode(@Header(AUTHORIZATION) String authorization,
                                                  @Header(LAN) String lang,
                                                  @Header(PLATFORM) String platform,
                                                  @Query("lat") double lat,
                                                  @Query("long") double lng);

  // @FormUrlEncoded
  @POST("customer/referralCodeValidation")
  Observable<Response<ResponseBody>> onReferralCodeCheck(@Header(LAN) String lang,
                                                         @Header(PLATFORM) String platform,
                                                         @Body Map<String, Object> stringObjectMap);

  @GET("customer/language")
  Observable<Response<ResponseBody>> onLanguageCalled(@Header(LAN) String lang,
                                                      @Header(PLATFORM) String platform);

  @GET("customer/referralCode")
  Observable<Response<ResponseBody>> getReferralCode(@Header(AUTHORIZATION) String authorization,
                                                     @Header(LAN) String lang, @Header(PLATFORM) String platform);

  @GET("customer/booking/chat")
  Observable<Response<ResponseBody>> getBookingChat(@Header(AUTHORIZATION) String authorization,
                                                    @Header(LAN) String lang, @Header(PLATFORM) String platform);

  // @FormUrlEncoded
  @POST("customer/favouriteProvider")
  Observable<Response<ResponseBody>> addTOFav(@Header(AUTHORIZATION) String authorization,
                                              @Header(LAN) String lang, @Header(PLATFORM) String platform,
                                              @Body Map<String, Object> objectMap);

  @DELETE("customer/favouriteProvider")
  Observable<Response<ResponseBody>> removeFromFav(@Header(AUTHORIZATION) String authorization,
                                                   @Header(LAN) String lang, @Header(PLATFORM) String platform,
                                                   @Query("categoryId") String categoryId, @Query("providerId") String providerId);

  @PATCH("customer/reminderBooking")
  Observable<Response<ResponseBody>> reminderEvent(@Header(AUTHORIZATION) String authorization,
                                                   @Header(LAN) String lang,
                                                   @Header(PLATFORM) String platform,
                                                   @Body Map<String, Object> objectMap);

  //Invoice raising for telecalling
  // @FormUrlEncoded
  @PATCH("customer/bookingStatus")
  Observable<Response<ResponseBody>> bookingstatus(@Header(AUTHORIZATION) String authorization,
                                                   @Header(LAN) String lang, @Header(PLATFORM) String platform,
                                                   @Body Map<String, Object> objectMap);


/*
    @GET ("customer/booking/invoice/{bookingId}")
    Observable<Response<ResponseBody>> bookingInvoice(@Header(AUTHORIZATION) String authorization,
                                                     @Header(LAN) String lang,
                                                     @Query("bookingId") long bid);
*/


  @GET("customer/favouriteProvider")
  Observable<Response<ResponseBody>> onToGetFavProvider(
      @Header(AUTHORIZATION) String authorization,
      @Header(LAN) String lang, @Header(PLATFORM) String platform);

  @GET("server/appVersion")
  Observable<Response<ResponseBody>> onTOGetAppVersion(@Header(LAN) String lang,
                                                       @Header(PLATFORM) String platform,
                                                       @Query(USERTYPE) int userType,
                                                       @Query("appVersion") String appVersion);

  @GET("/calls")
  Observable<Response<ResponseBody>> onTOGetcalls(@Header(AUTHORIZATION) String authorization,@Header(LAN) String lang);

  @GET("cognitoToken")
  Observable<Response<ResponseBody>> onTOGetCognitoToken(@Header(AUTHORIZATION) String authorization, @Header(LAN) String lang,
                                                         @Header(PLATFORM) String platform);


  @PATCH("server/call")
  Observable<Response<ResponseBody>> telCallAnsDec(@Header(AUTHORIZATION) String authorization,
                                                   @Header(LAN) String lang,
                                                   @Query("callId") String callId,
                                                   @Query("callStatus") int callStatus);

  @POST("/session")
  Observable<Response<ResponseBody>> onToGetSession(@Header(AUTHORIZATION) String authorization,
                                                  @Header(LAN) String lan, @Header(PLATFORM) String platform,
                                                  @Body Map<String, Object> objectMap);

  @GET("server/call")
  Observable<Response<ResponseBody>> getCallDetails(@Header(AUTHORIZATION) String authorization,
                                                    @Header(LAN) String lang, @Header(PLATFORM) String platform);

  @FormUrlEncoded
  @POST("server/call")
  Observable<Response<ResponseBody>> onMakeCall(@Header(AUTHORIZATION) String authorization,
                                                @Header(LAN) String lang,
                                                @Query("callType") String callType,
                                                @Query("targetId") String callerId,
                                                @Query("bookingId") String callId);

  @GET("customer/providerSlot")
  Observable<Response<ResponseBody>> getSlotsTimes(@Header(AUTHORIZATION) String authorization,
                                                   @Header(LAN) String lang,
                                                   @Header(PLATFORM) String platform,
                                                   @Query("providerId") String proId,
                                                   @Query("categoryId") String catId,
                                                   @Query("date") long date,
                                                   @Query("callType") int callType);


  @GET("customer/schedule")
  Observable<Response<ResponseBody>> getSchdeuleSlots(@Header(AUTHORIZATION) String authorization,
                                                      @Header(LAN) String lang, @Header(PLATFORM) String platform,
                                                      @Query("month") String month,
                                                      @Query("providerId") String providerId);

  //@FormUrlEncoded
  @PATCH("customer/booking/bidAccept")
  Observable<Response<ResponseBody>> acceptBidBooking(@Header(AUTHORIZATION) String authorization,
                                                      @Header(LAN) String lng, @Header(PLATFORM) String platform,
                                                      @Body Map<String, Object> objectMap);

  //Add Dependents API
  @POST("customer/dependent")
  Observable<Response<ResponseBody>> addDependent(@Header(AUTHORIZATION) String authorization,
                                                  @Header(LAN) String lan, @Header(PLATFORM) String platform,
                                                  @Body Map<String, Object> objectMap);

  @GET("customer/dependent")
  Observable<Response<ResponseBody>> getDependents(@Header(AUTHORIZATION) String authorization,
                                                   @Header(LAN) String lan, @Header(PLATFORM) String platform);

  // @FormUrlEncoded
  @PATCH("customer/dependent")
  Observable<Response<ResponseBody>> editDependent(@Header(AUTHORIZATION) String authorization, @Header(PLATFORM) String platform,
                                                   @Body Map<String, Object> objectMap);


  @DELETE("customer/dependent")
  Observable<Response<ResponseBody>> deleteDependent(
      @Header(AUTHORIZATION) String authorization, @Header(PLATFORM) String platform, @Query("dependentId") String dependentId);

  // Special Requirements API
  @GET("customer/specialRequirement")
  Observable<Response<SpecialRequirementResponse>> getSpecialRequirements(
      @Header(AUTHORIZATION) String authorization, @Header(LAN) String lan, @Header(PLATFORM) String platform);


  @GET("customer/relation")
  Observable<Response<ResponseBody>> onToGetRelation(@Header(AUTHORIZATION) String authorization,
                                                     @Header(LAN) String lang, @Header(PLATFORM) String platform);


  @GET("customer/symptoms")
  Observable<Response<ResponseBody>> onToGeSymptoms(@Header(AUTHORIZATION) String authorization,
                                                    @Header(LAN) String lang, @Header(PLATFORM) String platform,
                                                    @Query("categoryId") String categoryId,
                                                    @Query("searchStr") String searchStr);

  @GET("customer/symptomsQuestions")
  Observable<Response<ResponseBody>> onToGesymptomsQuestions(
      @Header(AUTHORIZATION) String authorization,
      @Header(LAN) String lang,
      @Header(PLATFORM) String platform,
      @Query("symptoms") String symptoms);


  @GET("customer/filters")
  Observable<Response<ResponseBody>> onToGetFilterData(
      @Header(AUTHORIZATION) String authorization,
      @Header(LAN) String lang,
      @Header(PLATFORM) String platform,
      @Query("categoryId") String categoryId);

  @POST("meet/call")
  Observable<Response<ResponseBody>> calling(@Header(AUTHORIZATION) String authorization,
                                             @Header(LAN) String lang, @Header(PLATFORM) String platform,@Body Map<String, Object> objectMap);

  @GET("customer/language")
  Observable<Response<ResponseBody>> onLanguageBasicCalled(
          @Header(LAN) String lang,
          @Header(PLATFORM) String platform);

  @POST("customer/language")
  Observable<Response<ResponseBody>>onLanguageChange(@Header(AUTHORIZATION) String authorization,
                                                     @Header(LAN) String lang, @Header(PLATFORM) String platform);


}
