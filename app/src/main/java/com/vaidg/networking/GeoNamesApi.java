package com.vaidg.networking;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GeoNamesApi {

  @GET("/findNearestIntersectionOSMJSON")
  Observable<Response<ResponseBody>> getGeoNamesFromLocation(
          @Query("lat") double lat,
          @Query("lng") double lng,
          @Query("username") String username);
}
