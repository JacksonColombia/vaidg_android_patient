package com.vaidg.filter;

import com.vaidg.Dagger2.ActivityScoped;
import com.vaidg.filter.model.FilteredResponse;
import dagger.Module;
import dagger.Provides;

/**
 * <h2>FilterDaggerResponseModule</h2>
 * Created by Ali on 2/2/2018.
 */

@Module
public class FilterDaggerResponseModule
{
    @Provides
    @ActivityScoped
    FilteredResponse provideFilteredPrice()
    {
        return new FilteredResponse();
    }

}
