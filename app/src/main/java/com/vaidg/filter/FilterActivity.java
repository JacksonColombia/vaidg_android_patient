package com.vaidg.filter;

import static com.vaidg.utilities.Constants.W_15;
import static com.vaidg.utilities.Constants.W_22;
import static com.vaidg.utilities.Constants.W_320;
import static com.vaidg.utilities.Constants.W_40;
import static com.vaidg.utilities.Constants.callTypeInOutTele;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.vaidg.R;
import com.vaidg.filter.adapter.FilterDataAdapter;
import com.vaidg.filter.adapter.FilterPredefinedDataAdapter;
import com.vaidg.filter.model.FilterData;
import com.vaidg.filter.model.FilterPreDefined;
import com.vaidg.model.SubCategory;
import com.vaidg.rangeseekbar.RangeSeekBar;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.vaidg.youraddress.YourAddressActivity;
import com.ma.bubbleseekmodule.BubbleSeekBar;
import com.utility.AlertProgress;
import dagger.android.support.DaggerAppCompatActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.inject.Inject;


public class FilterActivity extends DaggerAppCompatActivity implements FilterContract.filterView {

  private static final int SORT_BY = 1;
  private static final int AVAILABILITY = 2;
  private static final int GENDER = 3;
  private static final int FEE = 4;
  private static final int IN_HOSPITAL = 5;
  //Toolbar
  @BindView(R.id.toolbar)
  Toolbar toolFilter;
  @BindView(R.id.tv_center)
  TextView tvCenterText;
  @BindView(R.id.rangeSeekBar)
  RangeSeekBar rangeSeekBar;
  @BindView(R.id.tvMin)
  TextView tvMin;
  @BindView(R.id.tvMax)
  TextView tvMax;
  @BindView(R.id.tvPriceName)
  TextView tvPriceName;
  @BindView(R.id.tv_skip)
  TextView tv_skip;
  @BindView(R.id.ivFilter)
  ImageView ivFilter;
  @BindView(R.id.rlToolImage)
  LinearLayout rlToolImage;
  //initializeView
  @BindView(R.id.tvFilterLocation)
  TextView tvFilterLocation;
  @BindView(R.id.rvDistance)
  RelativeLayout rvDistance;
  @BindView(R.id.tvFilterLocationIcon)
  TextView tvFilterLocationIcon;
  @BindView(R.id.tvFilterDistanceIcon)
  TextView tvFilterDistanceIcon;
  @BindView(R.id.tvFilterSubCategory)
  TextView tvFilterSubCategory;
  @BindView(R.id.tvFilterSave)
  Button tvFilterSave;
  @BindView(R.id.recyclerViewFilterCategory)
  RecyclerView mRecyclerViewFilterCategory;
  @BindView(R.id.seekBarBubble)
  BubbleSeekBar bubbleSeekBar;
  @BindView(R.id.tvDistanceKm)
  TextView tvDistanceKm;
  @Inject
  SessionManagerImpl mSessionManager;
  @Inject
  AppTypeface mAppTypeface;
  @Inject
  FilterContract.filterPresent mFilterPresent;
  @Inject
  AlertProgress mAlertProgress;
  private boolean isSelected = false,isPrice = false;
  private ArrayList<SubCategory> subCategoryArrayList;
  private String TAG = FilterActivity.class.getSimpleName();
  private double lat, lng;
  private int distance = 30;
  private boolean isDistance, isLocation, isFilteredReset;
  private ArrayList<FilterData> mFilterData;
  private ArrayList<FilterPreDefined> mFilterPreDefinedsSortBy;
  private ArrayList<FilterPreDefined> mFilterPreDefinedsAvailability;
  private ArrayList<FilterPreDefined> mFilterPreDefinedsGender;
  private ArrayList<FilterPreDefined> mFilterPreDefinedsFee;
  private ArrayList<FilterPreDefined> mFilterPreDefinedsInHospital;
  private FilterDataAdapter mFilterDataAdapter;
  private String tagAddress;
  private String gender = "0";
  private String sortBy = "0";
  private String availability = "0";
  private String minConsultancy = "0";
  private String maxConsultancy = "0";
  private String inHospital = "0";
  private boolean isMinMaxSelected = true;
  private HashMap<String, String> mHashMap = new HashMap<>();


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_filter);
    ButterKnife.bind(this);
    // filteredResponseArray = new ArrayList<>();
 /*   mFilterPresent.onFetchFilterDataList(this, mAlertProgress, mSessionManager,
        mSessionManager.getAUTH(), selLang, catId);*/
    if (callTypeInOutTele != 3) {
      tvFilterLocationIcon.setVisibility(View.VISIBLE);
      tvFilterLocation.setVisibility(View.VISIBLE);
      tvFilterDistanceIcon.setVisibility(View.VISIBLE);
      rvDistance.setVisibility(View.VISIBLE);
    }
    populateDatList(true);
    toolBarSetting();
    seekBarWork();
    priceRangeSeekbar();
    calculate();
    typeFaceValue();

  }

  private void priceRangeSeekbar() {
    tvMax.setTypeface(mAppTypeface.getHind_medium());
    tvMin.setTypeface(mAppTypeface.getHind_medium());
    rangeSeekBar.setMax((int) Constants.maxFee);
    if (minConsultancy.equals("0"))
      minConsultancy = String.valueOf((int) Constants.minFee);
    if (maxConsultancy.equals("0"))
      maxConsultancy = String.valueOf((int) Constants.maxFee);
    rangeSeekBar.setProgress(Integer.parseInt(String.valueOf((int) Constants.minFee)), Integer.parseInt(String.valueOf((int) Constants.maxFee)));
    tvMin.setText(String.valueOf((int) Constants.minFee) + " " + Constants.currencySymbol);
    tvMax.setText(String.valueOf((int) Constants.maxFee) + " " + Constants.currencySymbol);
    rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
      @Override
      public void onProgressChanged(
          final RangeSeekBar seekBar, final int progressStart, final int progressEnd, final boolean fromUser) {
        isSelected = true;
        isPrice = true;
        tvMin.setText(progressStart + " " + Constants.currencySymbol);
        tvMax.setText(progressEnd + " " + Constants.currencySymbol);
        minConsultancy = String.valueOf(progressStart);
        maxConsultancy = String.valueOf(progressEnd);
        mHashMap.put("8", minConsultancy + " " + Constants.currencySymbol + "-" + maxConsultancy + " " + Constants.currencySymbol);
        mHashMap.put("6", minConsultancy);
        mHashMap.put("7", maxConsultancy);
      }

      @Override
      public void onStartTrackingTouch(final RangeSeekBar seekBar) {
      }

      @Override
      public void onStopTrackingTouch(final RangeSeekBar seekBar) {
      }
    });

    /*  mHashMap.put("4",minConsultancy+" "+Constants.currencySymbol+"-"+maxConsultancy+" "+Constants.currencySymbol);
      mHashMap.put("5",minConsultancy);
      mHashMap.put("6",maxConsultancy);*/
  }

  private void populateDatList(boolean b) {
    mFilterData = new ArrayList<>();
    mFilterPreDefinedsSortBy = new ArrayList<>();
    mFilterPreDefinedsAvailability = new ArrayList<>();
    mFilterPreDefinedsGender = new ArrayList<>();
    //mFilterPreDefinedsFee = new ArrayList<>();
    mFilterPreDefinedsInHospital = new ArrayList<>();


    mFilterPreDefinedsSortBy.add(new FilterPreDefined("0", getResources().getString(R.string.consulationFee), 1, false, "1"));
    if (Constants.bookingType != 1) {
      mFilterPreDefinedsAvailability.add(new FilterPreDefined("0", getResources().getString(R.string.available_any_day), 2, false, "1"));
      mFilterPreDefinedsAvailability.add(new FilterPreDefined("1", getResources().getString(R.string.available_today), 2, false, "2"));
      mFilterPreDefinedsAvailability.add(new FilterPreDefined("2", getResources().getString(R.string.available_next_day), 2, false, "3"));
    }
    mFilterPreDefinedsGender.add(new FilterPreDefined("0", getResources().getString(R.string.maleDoctor), 3, false, "1"));
    mFilterPreDefinedsGender.add(new FilterPreDefined("1", getResources().getString(R.string.femaleDoctor), 3, false, "2"));
    mFilterPreDefinedsGender.add(new FilterPreDefined("2", getResources().getString(R.string.others), 3, false, "0"));
    // mFilterPreDefinedsFee.add(new FilterPreDefined("1", "", 4, false, "0"));
        /*        mFilterPreDefinedsFee.add(new FilterPreDefined("0", "Free", 4, false, "1"));
        mFilterPreDefinedsFee.add(new FilterPreDefined("1", "$10 - $100", 4, false, "2"));
        mFilterPreDefinedsFee.add(new FilterPreDefined("2", "$100 - $200", 4, false, "3"));
        mFilterPreDefinedsFee.add(new FilterPreDefined("3", "$200+", 4, false, "3"));*/
    mFilterPreDefinedsInHospital.add(new FilterPreDefined("1", getResources().getString(R.string.inHospital), 5, false, "1"));
    if (b) {
      if (getIntent().getExtras() != null) {
        mHashMap = (HashMap<String, String>) getIntent().getExtras().getSerializable("map");

        if (mHashMap != null && mHashMap.size() > 0) {
          Iterator myVeryOwnIterator = mHashMap != null ? mHashMap.keySet().iterator() : null;
          while (myVeryOwnIterator != null && myVeryOwnIterator.hasNext()) {
            String key = (String) myVeryOwnIterator.next();
            String value = mHashMap.get(key);
            Log.v("HashMapTest", value);
            Log.v("HashMapTest", key);

            if ("6".equals(key)) {
              minConsultancy = value;
              Log.v("HashMapTest", "minConsultancy" + key + "  " + value);

            }

            if ("7".equals(key)) {
              maxConsultancy = value;
              Log.v("HashMapTest", "maxConsultancy" + key + "  " + value);
            }

            for (int i = 0; i < mFilterPreDefinedsSortBy.size(); i++) {
              if (mFilterPreDefinedsSortBy.get(i).getName().equals(value)) {
                mFilterPreDefinedsSortBy.get(i).setSelected(true);
                sortBy = mFilterPreDefinedsSortBy.get(i).getSelectedValue();
                mHashMap.put(String.valueOf(mFilterPreDefinedsSortBy.get(i).getType()), mFilterPreDefinedsSortBy.get(i).getName());
                //break;
              }
            }

            if (Constants.bookingType != 1) {
              for (int i = 0; i < mFilterPreDefinedsAvailability.size(); i++) {
                if (mFilterPreDefinedsAvailability.get(i).getName().equals(value)) {
                  mFilterPreDefinedsAvailability.get(i).setSelected(true);
                  availability = mFilterPreDefinedsAvailability.get(i).getSelectedValue();
                  mHashMap.put(String.valueOf(mFilterPreDefinedsAvailability.get(i).getType()), mFilterPreDefinedsAvailability.get(i).getName());
                  //break;
                }
              }
            }
            for (int i = 0; i < mFilterPreDefinedsGender.size(); i++) {
              if (mFilterPreDefinedsGender.get(i).getName().equals(value)) {
                mFilterPreDefinedsGender.get(i).setSelected(true);
                gender = mFilterPreDefinedsGender.get(i).getSelectedValue();
                mHashMap.put(String.valueOf(mFilterPreDefinedsGender.get(i).getType()), mFilterPreDefinedsGender.get(i).getName());
                //break;
              }
            }

            for (int i = 0; i < mFilterPreDefinedsInHospital.size(); i++) {
              if (mFilterPreDefinedsInHospital.get(i).getName().equals(value)) {
                mFilterPreDefinedsInHospital.get(i).setSelected(true);
                inHospital = mFilterPreDefinedsInHospital.get(i).getSelectedValue();
                mHashMap.put(String.valueOf(mFilterPreDefinedsInHospital.get(i).getType()), mFilterPreDefinedsInHospital.get(i).getName());
                //break;
              }
            }
          }
          isSelected = true;
        }
      }
    }

    mFilterData.add(new FilterData("0", getResources().getString(R.string.sortBy), 1, mFilterPreDefinedsSortBy));
    if (Constants.bookingType != 1) {
      mFilterData.add(new FilterData("1", getResources().getString(R.string.availability), 2, mFilterPreDefinedsAvailability));
    }
    mFilterData.add(new FilterData("2", getResources().getString(R.string.gender), 3, mFilterPreDefinedsGender));
    // mFilterData.add(new FilterData("3", "Price", 4, mFilterPreDefinedsFee));
    mFilterData.add(new FilterData("4", getResources().getString(R.string.inHospital), 4, mFilterPreDefinedsInHospital));


  }

  private void typeFaceValue() {
    tvDistanceKm.setTypeface(mAppTypeface.getHind_medium());
    tvMin.setTypeface(mAppTypeface.getHind_medium());
    tvMax.setTypeface(mAppTypeface.getHind_medium());
    tvFilterLocation.setTypeface(mAppTypeface.getHind_medium());
    tvFilterLocationIcon.setTypeface(mAppTypeface.getHind_semiBold());
    tvPriceName.setTypeface(mAppTypeface.getHind_semiBold());
    tvFilterDistanceIcon.setTypeface(mAppTypeface.getHind_semiBold());
    tvFilterSubCategory.setTypeface(mAppTypeface.getHind_semiBold());
    String distance = "30 " + Constants.distanceUnit;
    tvDistanceKm.setText(distance);

    tvFilterLocationIcon.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_16));
    tvFilterSave.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_18));
    tvPriceName.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_16));
    tvFilterLocation.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_16));
    tvFilterDistanceIcon.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_16));
    tvDistanceKm.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_13));
    tvMax.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_13));
    tvMin.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_13));
  }


  private void calculate() {
   int w22 = Utility.getScreenWidth() * W_40 / W_320;
    tvFilterSave.getLayoutParams().height = w22;
  }

  private void seekBarWork() {
    bubbleSeekBar.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListener() {
      @Override
      public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress,
                                    float progressFloat) {

      }

      @Override
      public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress,
                                        float progressFloat) {

      }

      @Override
      public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress,
                                       float progressFloat) {

        if (progress < 30) {
          distance = progress;
          isDistance = true;
          isSelected = true;
          //  mHashMap.put("7",String.valueOf(distance));
        }

        Log.w(TAG, "getProgressOnFinally: " + distance);
      }
    });
  }

  //chandrakanta771950@gmail.com
  private void toolBarSetting() {
    ivFilter.setVisibility(View.GONE);
    rlToolImage.setVisibility(View.GONE);
    tv_skip.setVisibility(View.VISIBLE);
    tv_skip.setText(getString(R.string.reset));
    tv_skip.setTextSize(TypedValue.COMPLEX_UNIT_PX,getResources().getDimensionPixelSize(R.dimen.sp_14));
    tv_skip.setTypeface(mAppTypeface.getHind_medium());
    tvCenterText.setText(getString(R.string.filter));
    setSupportActionBar(toolFilter);
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayShowHomeEnabled(true);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
    tvCenterText.setTypeface(mAppTypeface.getHind_semiBold());
    toolFilter.setNavigationIcon(R.drawable.ic_back);
    toolFilter.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onBackPressed();
      }
    });
    Constants.filteredAddress = mSessionManager.getAddress();
    tvFilterLocation.setText(mSessionManager.getAddress());

    mRecyclerViewFilterCategory.setHasFixedSize(true);
    mFilterDataAdapter = new FilterDataAdapter(this, mFilterData, mAppTypeface, mSessionManager, this, mHashMap);
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
    mRecyclerViewFilterCategory.setLayoutManager(linearLayoutManager);
    mRecyclerViewFilterCategory.setItemAnimator(new DefaultItemAnimator());
    mRecyclerViewFilterCategory.setAdapter(mFilterDataAdapter);

  }

  @OnClick({R.id.tvFilterLocation, R.id.tv_skip})
  public void address(View v) {
    switch (v.getId()) {
      case R.id.tv_skip:
        Log.d(TAG, "addressSkip: ");
        mHashMap.clear();
        populateDatList(false);
        toolBarSetting();
        seekBarWork();
        priceRangeSeekbar();
        isSelected = true;
        isPrice = false;
        break;
      case R.id.tvFilterLocation:
        Intent intent = new Intent(this, YourAddressActivity.class);
        intent.putExtra("isNotFromAddress", false);
        //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, Constants.ADDRESS_RESULT_CODE);
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay_still);
        break;
    }

  }

  @Override
  public void onBackPressed() {
    finish();
    overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);

  }


  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (resultCode == RESULT_OK) {
      if (requestCode == Constants.ADDRESS_RESULT_CODE) {
        if (data != null) {
          lat = data.getDoubleExtra("lat", 0.0);
          lng = data.getDoubleExtra("lng", 0.0);
          String bookingAddress = data.getStringExtra("AddressLine1");
          String bookingAddress2 = data.getStringExtra("AddressLine2");
          tagAddress = data.getStringExtra("TAGAS");
          Log.d(TAG, "onActivityResult: " + lat + " lang " + lng + " add "
              + bookingAddress + " add2 " + bookingAddress2);
          tvFilterLocation.setText(bookingAddress);
          Constants.filteredAddress = bookingAddress;

        }
      }
    }
  }


  @Override
  public void onSessionExpired() {

  }

  @Override
  public void onLogout(String message, SessionManagerImpl sessionManager) {
    mAlertProgress.alertPositiveOnclick(this, message, getString(R.string.logout),
        getString(R.string.ok), isClicked -> Utility.setMAnagerWithBID(this, mSessionManager));

  }

  @Override
  public void onError(String error) {

  }

  @Override
  public void onConnectionError(String connectionError) {

  }

  @Override
  public void onShowProgress() {

  }

  @Override
  public void onHideProgress() {

  }

  @Override
  public void onGetFilterDataList(ArrayList<FilterData> mFilterDataArrayList) {
    if (mFilterDataArrayList.size() > 0) {
      mFilterData.clear();
      mFilterData.addAll(mFilterDataArrayList);
      mFilterDataAdapter.notifyDataSetChanged();
    }
  }

  @Override
  public void onItemClick(int position, FilterPredefinedDataAdapter filterPredefinedDataAdapter,
                          ArrayList<FilterPreDefined> filterPreDefineds, HashMap<String, String> hashMap) {

    filterPredefinedDataAdapter.setItemSelected(position, hashMap);
    if (filterPreDefineds.get(position).isSelected()) {
      isSelected = true;
      switch (filterPreDefineds.get(position).getType()) {
        case SORT_BY:
          sortBy = filterPreDefineds.get(position).getSelectedValue();
          hashMap.put(String.valueOf(filterPreDefineds.get(position).getType()), filterPreDefineds.get(position).getName());
          break;
        case AVAILABILITY:
          availability = filterPreDefineds.get(position).getSelectedValue();
          hashMap.put(String.valueOf(filterPreDefineds.get(position).getType()), filterPreDefineds.get(position).getName());
          break;
        case GENDER:
          gender = filterPreDefineds.get(position).getSelectedValue();
          hashMap.put(String.valueOf(filterPreDefineds.get(position).getType()), filterPreDefineds.get(position).getName());
          break;
        case IN_HOSPITAL:
          inHospital = filterPreDefineds.get(position).getSelectedValue();
          hashMap.put(String.valueOf(filterPreDefineds.get(position).getType()), filterPreDefineds.get(position).getName());
          break;
      }

      this.mHashMap = hashMap;
    } else {
      isSelected = false;
    }
    Log.w(TAG, "onItemClick:mHashMap " + mHashMap.get(String.valueOf(filterPreDefineds.get(position).getType())));

    Log.d(TAG, "onItemClick: " + filterPreDefineds.get(position).getSelectedValue() + "   " + filterPreDefineds.get(position).getName());

  }


  /* btnSelection.setOnClickListener(new OnClickListener() {

     @Override
     public void onClick(View v) {
       String data = "";
       List<Student> stList = ((CardViewDataAdapter) mAdapter)
           .getStudentist();

       for (int i = 0; i < stList.size(); i++) {
         Student singleStudent = stList.get(i);
         if (singleStudent.isSelected() == true) {

           data = data + "\n" + singleStudent.getName().toString();
           *//*
   * Toast.makeText( CardViewActivity.this, " " +
   * singleStudent.getName() + " " +
   * singleStudent.getEmailId() + " " +
   * singleStudent.isSelected(),
   * Toast.LENGTH_SHORT).show();
   *//*
        }

      }

      Toast.makeText(CardViewActivity.this,
          "Selected Students: \n" + data, Toast.LENGTH_LONG)
          .show();
    }
  });*/
  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);

    //Utility.checkAndShowNetworkError(this);
  }


  @OnClick(R.id.tvFilterSave)
  public void saveClicked() {
    //mHashMap.put("8",mSessionManager.getAddress());
    if (isSelected) {
      Intent intent = new Intent();
      intent.putExtra("map", mHashMap);
      intent.putExtra("gender", gender);
      intent.putExtra("sortBy", sortBy);
      intent.putExtra("address", Constants.filteredAddress);
      intent.putExtra("distance", distance);
      intent.putExtra("availability", availability);
      if(isPrice) {
        intent.putExtra("minConsultancy", minConsultancy);
        intent.putExtra("maxConsultancy", maxConsultancy);
      }else{
        minConsultancy = "0";
        maxConsultancy = "0";
        intent.putExtra("minConsultancy", minConsultancy);
        intent.putExtra("maxConsultancy", maxConsultancy);
      }
      intent.putExtra("inHospital", inHospital);
      setResult(RESULT_OK, intent);
      finish();
      overridePendingTransition(R.anim.mainfadein, R.anim.slide_down_acvtivity);
    } else {
      mAlertProgress.alertinfo(this, "Please select any one to filter a doctor");
    }
  }

}

