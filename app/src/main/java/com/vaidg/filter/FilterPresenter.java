package com.vaidg.filter;

import static com.vaidg.utilities.Constants.BASE_AUTH_PASSWORD;
import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;

import android.content.Context;
import com.vaidg.R;
import com.vaidg.filter.model.FilteredResponse;
import com.vaidg.networking.LSPServices;
import com.vaidg.providerList.DoctorPresenter;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.google.gson.Gson;
import com.mqtt.MQTTManager;
import com.pojo.ErrorHandel;
import com.utility.AlertProgress;
import com.utility.RefreshToken;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * <h2>FilterPresenter</h2>
 * Created by Ali on 2/2/2018.
 */

public class FilterPresenter implements FilterContract.filterPresent {

  @Inject
  LSPServices mLSPServices;
  @Inject
  FilterContract.filterView view;
  @Inject
  Gson mGson;
  @Inject
  MQTTManager mMQTTManager;
  private String TAG = DoctorPresenter.class.getSimpleName();

  @Inject
  public FilterPresenter() {
  }

  @Override
  public void attachView(Object view) {

  }

  @Override
  public void detachView() {

  }

  @Override
  public void onFetchFilterDataList(Context mContext,
      AlertProgress mAlertProgress, SessionManagerImpl mSessionManager, String auth,
      String lang,
      String catId) {


    mLSPServices.onToGetFilterData(auth,
        lang, PLATFORM_ANDROID, catId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            String responseBody;
            JSONObject jsonObject;
            int responseCode = +responseBodyResponse.code();
            try {
              responseBody =
                  responseBodyResponse.body() != null ? responseBodyResponse.body().string()
                      : null;

              switch (responseCode) {
                case SUCCESS_RESPONSE:
                  if(responseBody != null && !responseBody.isEmpty()) {
                    FilteredResponse response = mGson.fromJson(responseBody,
                        FilteredResponse.class);
                    if (response.getData() != null && response.getData().size() > 0) {
                      if (view != null) {
                        view.onGetFilterDataList(response.getData());
                      }
                    }
                  }
                  break;
                case Constants.SESSION_EXPIRED:
                  ErrorHandel errorHandel = mGson.fromJson(responseBody, ErrorHandel.class);
                    RefreshToken.onRefreshToken(auth, mSessionManager.getREFRESHAUTH(),
                        mLSPServices,
                        new RefreshToken.RefreshTokenImple() {
                          @Override
                          public void onSuccessRefreshToken(String newToken) {

                              LSPApplication.getInstance().setAuthToken(mSessionManager.getSID(),mSessionManager.getSID(),newToken);
                            onFetchFilterDataList(mContext, mAlertProgress,
                                mSessionManager, auth, lang, catId);

                          }

                          @Override
                          public void onFailureRefreshToken() {

                          }

                          @Override
                          public void sessionExpired(String msg) {
                            if (view != null) {
                              view.onLogout(msg, mSessionManager);
                            }
                          }
                        });
                  if (view != null) {
                    view.onHideProgress();
                  }
                  break;

                default:
                  if(responseBody != null && !responseBody.isEmpty()) {

                    jsonObject = new JSONObject(responseBody);
                    jsonObject.getString(MESSAGE);
                    if(!jsonObject.getString(MESSAGE).isEmpty()) {
                      if (view != null) {
                        view.onError(jsonObject.getString(MESSAGE));
                      }
                    }
                  }
                  if (view != null) {
                    view.onHideProgress();
                  }
                  break;

              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
              if (view != null) {
                view.onHideProgress();
              }
            }
          }

          @Override
          public void onError(Throwable e) {
      if(view != null)
      {
        view.onHideProgress();
      }
          }

          @Override
          public void onComplete() {

          }
        });

  }
}
