package com.vaidg.filter;

import android.content.Context;
import com.vaidg.filter.adapter.FilterPredefinedDataAdapter;
import com.vaidg.filter.model.FilterData;
import com.vaidg.filter.model.FilterPreDefined;
import com.vaidg.home.BasePresenter;

import com.vaidg.home.BaseView;
import com.vaidg.utilities.SessionManagerImpl;
import com.utility.AlertProgress;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * <h2>FilterContract</h2>
 * Created by Ali on 1/31/2018.
 */

public interface FilterContract
{
    interface filterPresent extends BasePresenter
    {

        void onFetchFilterDataList(Context mContext,
            AlertProgress mAlertProgress, SessionManagerImpl mSessionManager,
            String auth, String selLang, String catId);
    }
    interface filterView extends BaseView
    {

        void onGetFilterDataList(ArrayList<FilterData> mFilterDataArrayList);

        void onItemClick(int position,
                         FilterPredefinedDataAdapter filterPredefinedDataAdapter,
                         ArrayList<FilterPreDefined> filterPreDefineds, HashMap<String, String> hashMap);

    }

}
