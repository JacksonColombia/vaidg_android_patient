package com.vaidg.filter;


import com.vaidg.Dagger2.ActivityScoped;

import com.vaidg.providerList.DoctorList;
import com.vaidg.providerList.DoctorListContract;
import dagger.Binds;
import dagger.Module;

/**
 * <h2>FilterDaggerModule</h2>
 * Created by Ali on 1/31/2018.
 */

@Module
public interface FilterDaggerModule
{
    @Binds
    @ActivityScoped
    FilterContract.filterPresent provideFilterPresenter(FilterPresenter filterPresenter);

    @Binds
    @ActivityScoped
    FilterContract.filterView providerFilterView(FilterActivity filterActivity);
}
