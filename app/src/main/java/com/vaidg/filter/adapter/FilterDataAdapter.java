package com.vaidg.filter.adapter;

import android.content.Context;
import android.util.TypedValue;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vaidg.R;
import com.vaidg.filter.FilterContract;
import com.vaidg.filter.model.FilterData;
import com.vaidg.filter.model.FilterPreDefined;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.SessionManagerImpl;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * <h2>FilterAdapter</h2>
 * Created by Ali on 2/1/2018.
 */

public class FilterDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
  private Context mContext;
  private ArrayList<FilterData> mFilterData;
  private AppTypeface mAppTypeface;
  private SessionManagerImpl mSessionManager;
  private FilterContract.filterView mFilterView;
  private static final int SORT_BY = 1;
  private static final int AVAILABILITY = 2;
  private static final int GENDER = 3;
  private static final int FEE = 4;
  private static final int IN_HOSPITAL = 5;
  private HashMap<String,String> hashMap  = new HashMap<>();

  public FilterDataAdapter(Context context,
                           ArrayList<FilterData> filterDataArrayList,
                           AppTypeface appTypeface, SessionManagerImpl sessionManager, FilterContract.filterView filterView, HashMap<String, String> hashMap) {
    this.mContext = context;
    this.mFilterData = filterDataArrayList;
    this.mAppTypeface = appTypeface;
    this.mSessionManager = sessionManager;
    this.mFilterView = filterView;
    this.hashMap = hashMap;
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view;
    if (viewType == SORT_BY) { // for radio layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filterdata_adapter, parent, false);
      return new SortByTypeViewHolder(view);
    } else if (viewType == AVAILABILITY) { // for checkbox layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filterdata_adapter, parent, false);
      return new AvailabilityTypeViewHolder(view);
    } else if (viewType == GENDER) { // for dropdown layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filterdata_adapter, parent, false);
      return new GenderTypeViewHolder(view);
    } else if (viewType == FEE) { // for number_slider layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filterdata_adapter, parent, false);
      return new FeeTypeViewHolder(view);
    } else if (viewType == IN_HOSPITAL) { // for number_slider layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filterdata_adapter, parent, false);
      return new InHospitalTypeViewHolder(view);
    } else {
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filterdata_adapter, parent, false);
      return new NullViewHolder(view);
    }

  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

    final FilterData filterData = mFilterData.get(position);
    if (getItemViewType(position) == SORT_BY) {
      ((SortByTypeViewHolder) viewHolder).setSortByTypeData(filterData);

    } else if (getItemViewType(position) == AVAILABILITY) {
      ((AvailabilityTypeViewHolder) viewHolder).setAvailabilityTypeData(filterData);

    } else if (getItemViewType(position) == GENDER) {
      ((GenderTypeViewHolder) viewHolder).setGenderTypeData(filterData);

    } else if (getItemViewType(position) == FEE) {
      ((FeeTypeViewHolder) viewHolder).setFeeTypeData(filterData);

    } else if (getItemViewType(position) == IN_HOSPITAL) {
      ((InHospitalTypeViewHolder) viewHolder).setInHospitalTypeData(filterData);
    } else {
      ((NullViewHolder) viewHolder).setNullData(filterData);
    }
  }

  private void filterValitemListClicked(int position,
                                        FilterPredefinedDataAdapter filterPredefinedDataAdapter,
                                        ArrayList<FilterPreDefined> filterPreDefineds, HashMap<String, String> hashMap) {
    mFilterView.onItemClick(position, filterPredefinedDataAdapter,filterPreDefineds,hashMap);
  }

  @Override
  public int getItemCount() {
    return mFilterData == null ? 0 : mFilterData.size();
  }


  @Override
  public int getItemViewType(int position) {
    if (mFilterData.get(position).getType() == SORT_BY) {
      return SORT_BY;
    } else if (mFilterData.get(position).getType() == AVAILABILITY) {
      return AVAILABILITY;
    } else if (mFilterData.get(position).getType() == GENDER) {
      return GENDER;
    } else if (mFilterData.get(position).getType() == FEE) {
      return FEE;
    } else if (mFilterData.get(position).getType() == IN_HOSPITAL) {
      return IN_HOSPITAL;
    } else {
      return -1;
    }

  }

  public class AvailabilityTypeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.recyclerviewPredefinedFilterData)
    RecyclerView mRecyclerviewPredefinedFilterData;
    FilterPredefinedDataAdapter mFilterPredefinedDataAdapter;
  //  ArrayList<FilterPreDefined> mFilterPreDefineds;

    AvailabilityTypeViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      tvName.setTypeface(mAppTypeface.getHind_semiBold());
   //   mFilterPreDefineds = new ArrayList<>();
    }

    public void setAvailabilityTypeData(FilterData filterData) {
      tvName.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvName.setText(filterData.getFilterTitle());


      //  viewHolder.mFilterPreDefineds.clear();
      //   mFilterPreDefineds.addAll(filterData.getPreDefined());
      mRecyclerviewPredefinedFilterData.setHasFixedSize(true);
      mFilterPredefinedDataAdapter = new FilterPredefinedDataAdapter(mContext, mFilterData.get(getAdapterPosition()).getPreDefined(), mAppTypeface, mSessionManager,hashMap);
      LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
      mRecyclerviewPredefinedFilterData.setLayoutManager(linearLayoutManager);
      mRecyclerviewPredefinedFilterData.setItemAnimator(new DefaultItemAnimator());
      mRecyclerviewPredefinedFilterData.setAdapter(mFilterPredefinedDataAdapter);
      mFilterPredefinedDataAdapter.setOnItemClickListener(new FilterPredefinedDataAdapter.OnItemClickListener() {

        @Override
        public void onItemClick(View view, int position,
                                ArrayList<FilterPreDefined> filterPreDefineds, HashMap<String, String> hashMap) {
          filterValitemListClicked(position, mFilterPredefinedDataAdapter,filterPreDefineds, hashMap);
        }
      });
      mFilterPredefinedDataAdapter.notifyDataSetChanged();
    }
  }

  public class SortByTypeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.recyclerviewPredefinedFilterData)
    RecyclerView mRecyclerviewPredefinedFilterData;
    FilterPredefinedDataAdapter mFilterPredefinedDataAdapter;
  //  ArrayList<FilterPreDefined> mFilterPreDefineds;

    SortByTypeViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      tvName.setTypeface(mAppTypeface.getHind_semiBold());
   //   mFilterPreDefineds = new ArrayList<>();
    }

    public void setSortByTypeData(FilterData filterData) {
      tvName.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvName.setText(filterData.getFilterTitle());


      //  viewHolder.mFilterPreDefineds.clear();
   //   mFilterPreDefineds.addAll(filterData.getPreDefined());
      mRecyclerviewPredefinedFilterData.setHasFixedSize(true);
      mFilterPredefinedDataAdapter = new FilterPredefinedDataAdapter(mContext, mFilterData.get(getAdapterPosition()).getPreDefined(), mAppTypeface, mSessionManager, hashMap);
      LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
      mRecyclerviewPredefinedFilterData.setLayoutManager(linearLayoutManager);
      mRecyclerviewPredefinedFilterData.setItemAnimator(new DefaultItemAnimator());
      mRecyclerviewPredefinedFilterData.setAdapter(mFilterPredefinedDataAdapter);
      mFilterPredefinedDataAdapter.setOnItemClickListener(new FilterPredefinedDataAdapter.OnItemClickListener() {

        @Override
        public void onItemClick(View view, int position,
                                ArrayList<FilterPreDefined> filterPreDefineds, HashMap<String, String> hashMap) {
          filterValitemListClicked(position, mFilterPredefinedDataAdapter,filterPreDefineds, hashMap);
        }
      });
      mFilterPredefinedDataAdapter.notifyDataSetChanged();
    }
  }

  public class GenderTypeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.recyclerviewPredefinedFilterData)
    RecyclerView mRecyclerviewPredefinedFilterData;
    FilterPredefinedDataAdapter mFilterPredefinedDataAdapter;
  //  ArrayList<FilterPreDefined> mFilterPreDefineds;

    GenderTypeViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      tvName.setTypeface(mAppTypeface.getHind_semiBold());
    //  mFilterPreDefineds = new ArrayList<>();
    }

    public void setGenderTypeData(FilterData filterData) {
      tvName.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvName.setText(filterData.getFilterTitle());


      //  viewHolder.mFilterPreDefineds.clear();
    //  mFilterPreDefineds.addAll(filterData.getPreDefined());
      mRecyclerviewPredefinedFilterData.setHasFixedSize(true);
      mFilterPredefinedDataAdapter = new FilterPredefinedDataAdapter(mContext, mFilterData.get(getAdapterPosition()).getPreDefined(), mAppTypeface, mSessionManager, hashMap);
      LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
      mRecyclerviewPredefinedFilterData.setLayoutManager(linearLayoutManager);
      mRecyclerviewPredefinedFilterData.setItemAnimator(new DefaultItemAnimator());
      mRecyclerviewPredefinedFilterData.setAdapter(mFilterPredefinedDataAdapter);
      mFilterPredefinedDataAdapter.setOnItemClickListener(new FilterPredefinedDataAdapter.OnItemClickListener() {

        @Override
        public void onItemClick(View view, int position,
                                ArrayList<FilterPreDefined> filterPreDefineds, HashMap<String, String> hashMap) {
          filterValitemListClicked(position, mFilterPredefinedDataAdapter,filterPreDefineds, hashMap);
          mRecyclerviewPredefinedFilterData.scrollToPosition(position);
        }
      });
      mFilterPredefinedDataAdapter.notifyDataSetChanged();
    }
  }

  public class FeeTypeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.recyclerviewPredefinedFilterData)
    RecyclerView mRecyclerviewPredefinedFilterData;
    FilterPredefinedDataAdapter mFilterPredefinedDataAdapter;
  //  ArrayList<FilterPreDefined> mFilterPreDefineds;

    FeeTypeViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      tvName.setTypeface(mAppTypeface.getHind_semiBold());
   //   mFilterPreDefineds = new ArrayList<>();
    }

    public void setFeeTypeData(FilterData filterData) {
      tvName.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvName.setText(filterData.getFilterTitle());


      //  viewHolder.mFilterPreDefineds.clear();
//      mFilterPreDefineds.addAll(filterData.getPreDefined());

      mRecyclerviewPredefinedFilterData.setHasFixedSize(true);
      mFilterPredefinedDataAdapter = new FilterPredefinedDataAdapter(mContext, mFilterData.get(getAdapterPosition()).getPreDefined(), mAppTypeface, mSessionManager, hashMap);
      LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
      mRecyclerviewPredefinedFilterData.setLayoutManager(linearLayoutManager);
      mRecyclerviewPredefinedFilterData.setItemAnimator(new DefaultItemAnimator());
      mRecyclerviewPredefinedFilterData.setAdapter(mFilterPredefinedDataAdapter);
      mFilterPredefinedDataAdapter.setOnItemClickListener(new FilterPredefinedDataAdapter.OnItemClickListener() {

        @Override
        public void onItemClick(View view, int position,
                                ArrayList<FilterPreDefined> filterPreDefineds, HashMap<String, String> hashMap) {
          filterValitemListClicked(position, mFilterPredefinedDataAdapter,filterPreDefineds, hashMap);
        }
      });
      mFilterPredefinedDataAdapter.notifyDataSetChanged();
    }
  }

  public class InHospitalTypeViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.recyclerviewPredefinedFilterData)
    RecyclerView mRecyclerviewPredefinedFilterData;
    FilterPredefinedDataAdapter mFilterPredefinedDataAdapter;
  //  ArrayList<FilterPreDefined> mFilterPreDefineds;

    InHospitalTypeViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      tvName.setTypeface(mAppTypeface.getHind_semiBold());
 //     mFilterPreDefineds = new ArrayList<>();
    }

    public void setInHospitalTypeData(FilterData filterData) {
      tvName.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvName.setText(filterData.getFilterTitle());


      //  viewHolder.mFilterPreDefineds.clear();
   //   mFilterPreDefineds.addAll(filterData.getPreDefined());
      mRecyclerviewPredefinedFilterData.setHasFixedSize(true);
      mFilterPredefinedDataAdapter = new FilterPredefinedDataAdapter(mContext, mFilterData.get(getAdapterPosition()).getPreDefined(), mAppTypeface, mSessionManager, hashMap);
      LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
      mRecyclerviewPredefinedFilterData.setLayoutManager(linearLayoutManager);
      mRecyclerviewPredefinedFilterData.setItemAnimator(new DefaultItemAnimator());
      mRecyclerviewPredefinedFilterData.setAdapter(mFilterPredefinedDataAdapter);
      mFilterPredefinedDataAdapter.setOnItemClickListener(new FilterPredefinedDataAdapter.OnItemClickListener() {

        @Override
        public void onItemClick(View view, int position,
                                ArrayList<FilterPreDefined> filterPreDefineds, HashMap<String, String> hashMap) {
          filterValitemListClicked(position, mFilterPredefinedDataAdapter,filterPreDefineds, hashMap);
        }
      });
      mFilterPredefinedDataAdapter.notifyDataSetChanged();
    }
  }

  public class NullViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.recyclerviewPredefinedFilterData)
    RecyclerView mRecyclerviewPredefinedFilterData;
    FilterPredefinedDataAdapter mFilterPredefinedDataAdapter;
  //  ArrayList<FilterPreDefined> mFilterPreDefineds;

    NullViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      tvName.setTypeface(mAppTypeface.getHind_semiBold());
    //  mFilterPreDefineds = new ArrayList<>();
    }

    public void setNullData(FilterData filterData) {
      tvName.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvName.setText(filterData.getFilterTitle());


      //  viewHolder.mFilterPreDefineds.clear();
     // mFilterPreDefineds.addAll(filterData.getPreDefined());
      mRecyclerviewPredefinedFilterData.setHasFixedSize(true);
      mFilterPredefinedDataAdapter = new FilterPredefinedDataAdapter(mContext, mFilterData.get(getAdapterPosition()).getPreDefined(), mAppTypeface, mSessionManager, hashMap);
      LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
      mRecyclerviewPredefinedFilterData.setLayoutManager(linearLayoutManager);
      mRecyclerviewPredefinedFilterData.setItemAnimator(new DefaultItemAnimator());
      mRecyclerviewPredefinedFilterData.setAdapter(mFilterPredefinedDataAdapter);
      mFilterPredefinedDataAdapter.setOnItemClickListener(new FilterPredefinedDataAdapter.OnItemClickListener() {

        @Override
        public void onItemClick(View view, int position,
                                ArrayList<FilterPreDefined> filterPreDefineds, HashMap<String, String> hashMap) {
          filterValitemListClicked(position, mFilterPredefinedDataAdapter,filterPreDefineds, hashMap);
        }
      });
      mFilterPredefinedDataAdapter.notifyDataSetChanged();
    }
  }
}
