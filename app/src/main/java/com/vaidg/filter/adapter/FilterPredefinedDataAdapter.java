package com.vaidg.filter.adapter;

import android.content.Context;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vaidg.R;
import com.vaidg.filter.model.FilterPreDefined;
import com.vaidg.rangeseekbar.RangeSeekBar;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * <h2>FilterAdapter</h2>
 * Created by Ali on 2/1/2018.
 */

public class FilterPredefinedDataAdapter extends
    RecyclerView.Adapter<RecyclerView.ViewHolder> {
  private static final int SORT_BY = 1;
  private static final int AVAILABILITY = 2;
  private static final int GENDER = 3;
  private static final int FEE = 4;
  private static final int IN_HOSPITAL = 5;
  private Context mContext;
  private ArrayList<FilterPreDefined> mFilterPreDefineds;
  private AppTypeface mAppTypeface;
  private SessionManagerImpl mSessionManager;
  private OnItemClickListener mItemClickListener;
  private HashMap<String, String> hashMap = new HashMap<>();

  public FilterPredefinedDataAdapter(Context context,
                                     ArrayList<FilterPreDefined> filterPreDefineds,
                                     AppTypeface appTypeface, SessionManagerImpl sessionManager, HashMap<String, String> hashMap) {
    this.mContext = context;
    this.mFilterPreDefineds = filterPreDefineds;
    this.mAppTypeface = appTypeface;
    this.mSessionManager = sessionManager;
    this.hashMap = hashMap;
  }


  @Override
  public int getItemViewType(int position) {
    if (mFilterPreDefineds.get(position).getType() == SORT_BY) {
      return SORT_BY;
    } else if (mFilterPreDefineds.get(position).getType() == AVAILABILITY) {
      return AVAILABILITY;
    } else if (mFilterPreDefineds.get(position).getType() == GENDER) {
      return GENDER;
    } else if (mFilterPreDefineds.get(position).getType() == FEE) {
      return FEE;
    } else if (mFilterPreDefineds.get(position).getType() == IN_HOSPITAL) {
      return IN_HOSPITAL;
    } else {
      return -1;
    }

  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view;
    if (viewType == SORT_BY) { // for radio layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filterpredefineddata_adapter, parent,
          false);
      return new SortByTypeViewHolder(view);
    } else if (viewType == AVAILABILITY) { // for checkbox layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filterpredefineddata_adapter, parent,
          false);
      return new AvailabilityTypeViewHolder(view);
    } else if (viewType == GENDER) { // for dropdown layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filterpredefineddata_adapter, parent,
          false);
      return new GenderTypeViewHolder(view);
    } else if (viewType == FEE) { // for number_slider layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filterpredefineddatafee_adapter, parent,
          false);
      return new FeeTypeViewHolder(view);
    } else if (viewType == IN_HOSPITAL) { // for number_slider layout
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filterpredefineddata_adapter, parent,
          false);
      return new InHospitalTypeViewHolder(view);
    } else {
      view = LayoutInflater.from(parent.getContext()).inflate(R.layout.filterpredefineddata_adapter, parent,
          false);
      return new NullViewHolder(view);
    }

  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {


    // final FilterPreDefined filterPreDefined = mFilterPreDefineds.get(position);
    if (getItemViewType(position) == SORT_BY) {
      ((SortByTypeViewHolder) viewHolder).setSortByTypeData(mFilterPreDefineds, position);

    } else if (getItemViewType(position) == AVAILABILITY) {
      ((AvailabilityTypeViewHolder) viewHolder).setAvailabilityTypeData(mFilterPreDefineds, position);

    } else if (getItemViewType(position) == GENDER) {
      ((GenderTypeViewHolder) viewHolder).setGenderTypeData(mFilterPreDefineds, position);

    } else if (getItemViewType(position) == FEE) {
      ((FeeTypeViewHolder) viewHolder).setFeeTypeData(mFilterPreDefineds, position);

    } else if (getItemViewType(position) == IN_HOSPITAL) {
      ((InHospitalTypeViewHolder) viewHolder).setInHospitalTypeData(mFilterPreDefineds, position);
    } else {
      ((NullViewHolder) viewHolder).setNullData(mFilterPreDefineds, position);
    }

  }


  public void setItemSelected(int position, HashMap<String, String> hashMap) {
    if (position != -1) {
     // mFilterPreDefineds.get(position).setSelected(!mFilterPreDefineds.get(position).isSelected());
      mFilterPreDefineds.get(position).setSelected(mFilterPreDefineds.get(position).isSelected());
      notifyDataSetChanged();
    }
  }

  public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
    this.mItemClickListener = mItemClickListener;
  }

  @Override
  public int getItemCount() {
    return mFilterPreDefineds == null ? 0 : mFilterPreDefineds.size();
  }

  // method to access in activity after updating selection
  public ArrayList<FilterPreDefined> getFilterPreDefinedsList() {
    return mFilterPreDefineds;
  }

  private void setCheckMethod(ArrayList<FilterPreDefined> filterPreDefined, TextView chkSelected,
                              TextView tvEmailId, int position) {

    final int sdk = android.os.Build.VERSION.SDK_INT;
    if (filterPreDefined.get(position).isSelected()) {
      select(tvEmailId, chkSelected, sdk);
    } else {
      unselect(tvEmailId, chkSelected, sdk);
    }
  }

  private void select(TextView tvEmailId, TextView chkSelected, int sdk) {
    tvEmailId.setTextColor(
        mContext.getResources().getColor(R.color.colorAccent));
    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
      chkSelected.setBackgroundDrawable(
          ContextCompat.getDrawable(mContext, R.drawable.ic_check));
    } else {
      chkSelected.setBackground(
          ContextCompat.getDrawable(mContext, R.drawable.ic_check));
    }
  }

  private void unselect(TextView tvEmailId, TextView chkSelected, int sdk) {
    tvEmailId.setTextColor(
        mContext.getResources().getColor(R.color.black));
    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
      chkSelected.setBackgroundDrawable(
          ContextCompat.getDrawable(mContext, R.drawable.ic_unselected));
    } else {
      chkSelected.setBackground(
          ContextCompat.getDrawable(mContext, R.drawable.ic_unselected));
    }
  }

  public interface OnItemClickListener {

    void onItemClick(View view, int position,
                     ArrayList<FilterPreDefined> filterPreDefineds, HashMap<String, String> hashMap);
  }

  class ViewHolderClass extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.chkSelected)
    TextView chkSelected;
    @BindView(R.id.rlPredifinedChckBox)
    RelativeLayout rlPredifinedChckBox;
    @BindView(R.id.tvEmailId)
    TextView tvEmailId;

    ViewHolderClass(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      tvEmailId.setTypeface(mAppTypeface.getHind_regular());
      itemView.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
      selectValue(getAdapterPosition(),v);
    }
  }

  public class SortByTypeViewHolder extends RecyclerView.ViewHolder implements
      View.OnClickListener {

    @BindView(R.id.chkSelected)
    TextView chkSelected;
    @BindView(R.id.rlPredifinedChckBox)
    RelativeLayout rlPredifinedChckBox;
    @BindView(R.id.tvEmailId)
    TextView tvEmailId;

    SortByTypeViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      tvEmailId.setTypeface(mAppTypeface.getHind_regular());
      itemView.setOnClickListener(this);
    }

    public void setSortByTypeData(ArrayList<FilterPreDefined> filterPreDefined, int position) {
      tvEmailId.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvEmailId.setText(filterPreDefined.get(position).getName());
      chkSelected.setTag(getAdapterPosition());
      filterPreDefined.get(position).setSelected(filterPreDefined.get(position).isSelected());
      setCheckMethod(filterPreDefined, chkSelected, tvEmailId, position);
    }

    @Override
    public void onClick(View v) {
      selectValue(getAdapterPosition(),v);
    }
  }

  public class AvailabilityTypeViewHolder extends RecyclerView.ViewHolder implements
      View.OnClickListener {

    @BindView(R.id.chkSelected)
    TextView chkSelected;
    @BindView(R.id.rlPredifinedChckBox)
    RelativeLayout rlPredifinedChckBox;
    @BindView(R.id.tvEmailId)
    TextView tvEmailId;

    AvailabilityTypeViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      tvEmailId.setTypeface(mAppTypeface.getHind_regular());
      itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
      selectValue(getAdapterPosition(),v);
    }

    public void setAvailabilityTypeData(ArrayList<FilterPreDefined> filterPreDefined, int position) {
      tvEmailId.setText(filterPreDefined.get(position).getName());
      chkSelected.setTag(getAdapterPosition());
      filterPreDefined.get(position).setSelected(filterPreDefined.get(position).isSelected());
      setCheckMethod(filterPreDefined, chkSelected, tvEmailId, position);
    }
  }

  public class GenderTypeViewHolder extends RecyclerView.ViewHolder implements
      View.OnClickListener {

    @BindView(R.id.chkSelected)
    TextView chkSelected;
    @BindView(R.id.rlPredifinedChckBox)
    RelativeLayout rlPredifinedChckBox;
    @BindView(R.id.tvEmailId)
    TextView tvEmailId;

    GenderTypeViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      tvEmailId.setTypeface(mAppTypeface.getHind_regular());
      itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
      selectValue(getAdapterPosition(),v);
    }

    public void setGenderTypeData(ArrayList<FilterPreDefined> filterPreDefined, int position) {
      tvEmailId.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvEmailId.setText(filterPreDefined.get(position).getName());
      chkSelected.setTag(getAdapterPosition());
      filterPreDefined.get(position).setSelected(filterPreDefined.get(position).isSelected());
      setCheckMethod(filterPreDefined, chkSelected, tvEmailId, position);
    }
  }

  private void selectValue(int adapterPosition, View v) {
    for (int i = 0; i < mFilterPreDefineds.size(); i++) {
      if (i == adapterPosition){
        Log.w("TAG", "setCheckMethod:Select "+mFilterPreDefineds.get(i).getName());
        if(mFilterPreDefineds.get(i).isSelected()){
          mFilterPreDefineds.get(i).setSelected(false);
        }else{
          mFilterPreDefineds.get(i).setSelected(true);}}
      else{
        Log.w("TAG", "setCheckMethod:Unselect "+mFilterPreDefineds.get(i).getName());
        mFilterPreDefineds.get(i).setSelected(false);}
    }

    if (mItemClickListener != null) {
      mItemClickListener.onItemClick(v, adapterPosition, mFilterPreDefineds, hashMap);
    }
  }

  public class FeeTypeViewHolder extends RecyclerView.ViewHolder implements
      View.OnClickListener {

    @BindView(R.id.rangeSeekBar)
    RangeSeekBar rangeSeekBar;
    @BindView(R.id.tvMin)
    TextView tvMin;
    @BindView(R.id.tvMax)
    TextView tvMax;

    FeeTypeViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      tvMax.setTypeface(mAppTypeface.getHind_regular());
      tvMin.setTypeface(mAppTypeface.getHind_regular());
      itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
      selectValue(getAdapterPosition(),v);
    }

    public void setFeeTypeData(ArrayList<FilterPreDefined> filterPreDefined, int position) {
      tvMin.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvMax.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      rangeSeekBar.setMax((int) Constants.maxFee);
      rangeSeekBar.setInitialProgress((int) Constants.minFee, (int) Constants.maxFee);
      rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
        @Override
        public void onProgressChanged(
            final RangeSeekBar seekBar, final int progressStart, final int progressEnd, final boolean fromUser) {
          tvMin.setText(String.valueOf(progressStart));
          tvMax.setText(String.valueOf(progressEnd));
        }

        @Override
        public void onStartTrackingTouch(final RangeSeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(final RangeSeekBar seekBar) {
        }
      });
    }
  }

  public class InHospitalTypeViewHolder extends RecyclerView.ViewHolder implements
      View.OnClickListener {

    @BindView(R.id.chkSelected)
    TextView chkSelected;
    @BindView(R.id.rlPredifinedChckBox)
    RelativeLayout rlPredifinedChckBox;
    @BindView(R.id.tvEmailId)
    TextView tvEmailId;

    InHospitalTypeViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      tvEmailId.setTypeface(mAppTypeface.getHind_regular());
      itemView.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
      selectValue(getAdapterPosition(),v);
    }

    public void setInHospitalTypeData(ArrayList<FilterPreDefined> filterPreDefined, int position) {
      tvEmailId.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvEmailId.setText(filterPreDefined.get(position).getName());
      chkSelected.setTag(getAdapterPosition());
      filterPreDefined.get(position).setSelected(filterPreDefined.get(position).isSelected());
      setCheckMethod(filterPreDefined, chkSelected, tvEmailId, position);
    }
  }

  public class NullViewHolder extends RecyclerView.ViewHolder implements
      View.OnClickListener {

    @BindView(R.id.chkSelected)
    TextView chkSelected;
    @BindView(R.id.rlPredifinedChckBox)
    RelativeLayout rlPredifinedChckBox;
    @BindView(R.id.tvEmailId)
    TextView tvEmailId;

    NullViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      tvEmailId.setTypeface(mAppTypeface.getHind_regular());
      itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
      selectValue(getAdapterPosition(),v);
    }

    public void setNullData(ArrayList<FilterPreDefined> filterPreDefined, int position) {
      tvEmailId.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
      tvEmailId.setText(filterPreDefined.get(position).getName());
      chkSelected.setTag(getAdapterPosition());
      filterPreDefined.get(position).setSelected(filterPreDefined.get(position).isSelected());
      setCheckMethod(filterPreDefined, chkSelected, tvEmailId, position);
    }
  }


}
