package com.vaidg.filter.model;

import java.io.Serializable;

/**
 * <h2>FilterData</h2>
 *
 * Created by Ali on 2/1/2018.
 */
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FilterData implements Serializable
{

  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("filterTitle")
  @Expose
  private String filterTitle;
  @SerializedName("unit")
  @Expose
  private String unit;
  @SerializedName("type")
  @Expose
  private long type;
  @SerializedName("description")
  @Expose
  private String description;
  @SerializedName("minimum")
  @Expose
  private long minimum;
  @SerializedName("maximum")
  @Expose
  private long maximum;
  @SerializedName("preDefined")
  @Expose
  private ArrayList<FilterPreDefined> preDefined = null;



  /**
   * No args constructor for use in serialization
   *
   */
  public FilterData() {
  }


  /**
   *
   * @param filterTitle
   * @param preDefined
   * @param id
   * @param type
   */
  public FilterData(String id, String filterTitle, long type, ArrayList<FilterPreDefined> preDefined) {
    super();
    this.id = id;
    this.filterTitle = filterTitle;
    this.type = type;
    this.preDefined = preDefined;
  }
/*

/**
   *
   * @param unit
   * @param filterTitle
   * @param preDefined
   * @param description
   * @param maximum
   * @param id
   * @param type
   * @param minimum
   *//*

  public FilterData(String id, String filterTitle, String unit, long type, String description, long minimum, long maximum, ArrayList<FilterPreDefined> preDefined) {
    super();
    this.id = id;
    this.filterTitle = filterTitle;
    this.unit = unit;
    this.type = type;
    this.description = description;
    this.minimum = minimum;
    this.maximum = maximum;
    this.preDefined = preDefined;
  }
*/

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFilterTitle() {
    return filterTitle;
  }

  public void setFilterTitle(String filterTitle) {
    this.filterTitle = filterTitle;
  }

  public String getUnit() {
    return unit;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public long getType() {
    return type;
  }

  public void setType(long type) {
    this.type = type;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public long getMinimum() {
    return minimum;
  }

  public void setMinimum(long minimum) {
    this.minimum = minimum;
  }

  public long getMaximum() {
    return maximum;
  }

  public void setMaximum(long maximum) {
    this.maximum = maximum;
  }

  public ArrayList<FilterPreDefined> getPreDefined() {
    return preDefined;
  }

  public void setPreDefined(ArrayList<FilterPreDefined> preDefined) {
    this.preDefined = preDefined;
  }

}