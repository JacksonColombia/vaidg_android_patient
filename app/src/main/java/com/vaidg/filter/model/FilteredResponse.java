package com.vaidg.filter.model;

import static com.vaidg.utilities.Constants.DATA;
import static com.vaidg.utilities.Constants.MESSAGE;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h2>FilteredResponse</h2>
 * Created by Ali on 2/2/2018.
 */

public class FilteredResponse implements Serializable
{

    @SerializedName(MESSAGE)
    @Expose
    private String message;
    @SerializedName(DATA)
    @Expose
    private ArrayList<FilterData> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public FilteredResponse() {
    }

    /**
     *
     * @param data
     * @param message
     */
    public FilteredResponse(String message, ArrayList<FilterData> data) {
        super();
        this.message = message;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<FilterData> getData() {
        return data;
    }

    public void setData(ArrayList<FilterData> data) {
        this.data = data;
    }

}
