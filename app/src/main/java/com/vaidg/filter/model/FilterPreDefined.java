package com.vaidg.filter.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class FilterPreDefined implements Serializable {

  @SerializedName("_id")
  @Expose
  private String id;
  @SerializedName("name")
  @Expose
  private String name;

  @SerializedName("type")
  @Expose
  private int type;
  @SerializedName("icon")
  @Expose
  private String icon;

  private boolean isSelected;
  private String selectedValue;

  /**
   * No args constructor for use in serialization
   */
  public FilterPreDefined() {
  }

  /**
   *
   */
  public FilterPreDefined(String id, String name,int type,boolean isSelected,String selectedValue) {
    super();
    this.id = id;
    this.name = name;
    this.type = type;
    this.isSelected = isSelected;
    this.selectedValue = selectedValue;
  }

  public String getSelectedValue() {
    return selectedValue;
  }

  public void setSelectedValue(String selectedValue) {
    this.selectedValue = selectedValue;
  }

  public int getType() {
    return type;
  }

  public void setType(int type) {
    this.type = type;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }


  public boolean isSelected() {
    return isSelected;
  }

  public void setSelected(boolean selected) {
    isSelected = selected;
  }
}
