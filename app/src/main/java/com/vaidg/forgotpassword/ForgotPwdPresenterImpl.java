package com.vaidg.forgotpassword;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import com.vaidg.model.ForgotPwdData;
import com.vaidg.model.ForgotPwdReq;
import com.vaidg.model.ForgotPwdResponse;
import com.vaidg.networking.BasicAuthentication;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManager;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import org.json.JSONObject;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static com.vaidg.utilities.Constants.BASE_AUTH_PASSWORD;
import static com.vaidg.utilities.Constants.BASE_AUTH_USERNAME;

/**
 * @author Pramod
 * @since 21-12-2017
 */


public class ForgotPwdPresenterImpl implements ForgotPwdPresenter {

    @Inject
    ForgotPwdView view;

    @Inject
    Context context;


    @Inject
    LSPServices lspServices;

    @Inject
    SessionManager sessionManager;

    @Inject
    ForgotPwdPresenterImpl() {

    }

    @Override
    public boolean validateEmail(String email) {
        return email == null || email.length() == 0 || "".equals(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches() && !Patterns.DOMAIN_NAME.matcher(email).matches();
    }

    @Override
    public boolean validatePhone(String phone) {
        return TextUtils.isEmpty(phone) || "".equals(phone) || !Patterns.PHONE.matcher(phone).matches();
    }

    @Override
    public void forgotPassword(String emailOrPhone,String countryCode, final int type) {
        ForgotPwdReq req = new ForgotPwdReq(emailOrPhone, countryCode, 1, type);
        lspServices = BasicAuthentication.ServiceGenerator.createService(LSPServices.class, BASE_AUTH_USERNAME, BASE_AUTH_PASSWORD);

        Observable<Response<ForgotPwdResponse>> response = lspServices.forgotPassword(Constants.selLang,Constants.PLATFORM_ANDROID,req);
        response.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ForgotPwdResponse>>() {

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                    @Override
                    public void onNext(Response<ForgotPwdResponse> value)
                    {

                        //switch
                        switch (value.code()) {

                            case Constants.SUCCESS_RESPONSE:
                                ForgotPwdResponse forgotPwdResponse = value.body();
                                if (forgotPwdResponse!=null) {
                                    ForgotPwdData forgotPwdData = forgotPwdResponse.getData();
                                    if (forgotPwdData != null) {
                                      Log.e("FPWD", "sid from response :: " + forgotPwdData.getSid());
                                      String sid = forgotPwdData.getSid();
                                      long expireOtp = forgotPwdData.getExpireOtp();
                                      sessionManager.setExpireOtp(expireOtp);
                                      if (type == 2) {
                                        if (view != null) {
                                          view.navToLogin(forgotPwdResponse.getMessage());
                                        }
                                      } else {
                                        if (view != null) {
                                          view.navtoOTP(sid, expireOtp);
                                        }
                                      }
                                    }
                                }
                                break;
                            default:
                                try {
                                    if (value.errorBody()!=null) {
                                        JSONObject errJson = new JSONObject(value.errorBody().string());
                                      if (view != null) {
                                        view.setError(errJson.getString("message"));
                                    }
                                      }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;

                        }
                    }

                    @Override
                    public void onError(Throwable e)
                    {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }
}
