package com.vaidg.share;


import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.utility.AlertProgress;
import com.vaidg.R;
import com.vaidg.databinding.ActivityShareBinding;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;

import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

import static com.vaidg.utilities.Constants.PLAY_STORE_LINK;
import static com.vaidg.utilities.Constants.SHARESCREEN.EXTRA_REFERRAL_CODE;
import static com.vaidg.utilities.Utility.isTextEmpty;
import static com.vaidg.utilities.Utility.setMAnagerWithBID;
import static com.vaidg.utilities.Utility.setTextSize14Sp;
import static com.vaidg.utilities.Utility.setTextSize16Sp;
import static com.vaidg.utilities.Utility.setTextSize18Sp;
import static com.vaidg.utilities.Utility.setTextSize20Sp;
import static com.vaidg.utilities.Utility.setTextSize24Sp;

/**
 * <h2>ShareActivity</h2>
 * <p>
 * Share page where it will allow to share the link with their connections.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class ShareActivity extends DaggerAppCompatActivity implements View.OnClickListener,
        ShareContract.ShareView {
    public final String TAG = ShareActivity.class.getSimpleName();
    String refCode = "";
    String shareText = "";
    @Inject
    AppTypeface appTypeface;
    @Inject
    AlertProgress alertProgress;
    @Inject
    ShareContract.SharePresenter presenter;
    private ActivityShareBinding binding;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityShareBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        mContext = this;
        initialize();
    }

    /**
     * <h2>initialize</h2>
     * <p> method to initialize the views</p>
     */
    private void initialize() {
        setSupportActionBar(binding.toolbarLayout.toolbar);
        binding.toolbarLayout.toolbar.setElevation(0f);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0f);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.toolbarLayout.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
        binding.toolbarLayout.tvCenter.setTypeface(appTypeface.getHind_semiBold());
        setTextSize16Sp(mContext, binding.toolbarLayout.tvCenter);
        binding.toolbarLayout.tvCenter.setText(mContext.getResources().getString(R.string.share));
        presenter.getPromoCode();
        binding.tvShareText.setTypeface(appTypeface.getHind_semiBold());
        binding.tvReferralCode.setTypeface(appTypeface.getHind_semiBold());
        binding.tvShareSocialText.setTypeface(appTypeface.getHind_regular());
        binding.tvFacebook.setTypeface(appTypeface.getHind_regular());
        binding.tvEmail.setTypeface(appTypeface.getHind_regular());
        binding.tvMessage.setTypeface(appTypeface.getHind_regular());
        binding.tvWhatsApp.setTypeface(appTypeface.getHind_regular());
        binding.tvTwitter.setTypeface(appTypeface.getHind_regular());
        setTextSize18Sp(mContext, binding.tvShareText);
        setTextSize16Sp(mContext, binding.tvReferralCode);
      //  setTextSize14Sp(mContext, binding.tvShareSocialText);
        setTextSize16Sp(mContext, binding.tvFacebook);
        setTextSize16Sp(mContext, binding.tvMessage);
        setTextSize16Sp(mContext, binding.tvWhatsApp);
        setTextSize16Sp(mContext, binding.tvEmail);
        setTextSize16Sp(mContext, binding.tvTwitter);
        refCode = getIntent().getStringExtra(EXTRA_REFERRAL_CODE);
        if (refCode != null) binding.tvReferralCode.setText(new StringBuilder()
                .append(" ").append(refCode));
        shareText = mContext.getResources().getString(R.string.share_text_1) + " " + refCode + " "
                + mContext.getResources().getString(R.string.share_text_2) + PLAY_STORE_LINK;
        binding.tvFacebook.setOnClickListener(this);
        binding.tvEmail.setOnClickListener(this);
        binding.tvMessage.setOnClickListener(this);
        binding.tvWhatsApp.setOnClickListener(this);
        binding.tvTwitter.setOnClickListener(this);
        binding.btnShare.setOnClickListener(this);
        binding.tvReferralCode.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) closeActivity();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    /**
     * <h2>closeActivity</h2>
     * <p> method for closing current page</p>
     */
    private void closeActivity() {
        finish();
        overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.tvFacebook) {
            openFaceBook();
        } else if (id == R.id.tvMessage) {
            openMessage();
        } else if (id == R.id.tvEmail) {
            openEmail();
        } else if (id == R.id.tvTwitter) {
            openTwitter();
        } else if (id == R.id.btnShare) {
            shareApp();
        }else if (id == R.id.tvReferralCode) {
            setClipboard(mContext,shareText);
        } else {
            openWatsapp();
        }
    }

    /**
     * <h2>shareApp</h2>
     * <p> method for sharing app in common way</p>
     */
    private void shareApp() {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TITLE, getString(R.string.subjectForEmailShare));
        i.putExtra(Intent.EXTRA_TEXT,
                shareText );
        startActivity(Intent.createChooser(i, getString(R.string.share_app)));
    }

    private void setClipboard(Context context, String text) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("Copied Text", text);
        clipboard.setPrimaryClip(clip);
    }
    /**
     * <h2>openFaceBook</h2>
     * <p> method for opening the Facebook for sharing the app</p>
     */
    private void openFaceBook() {
        try {

            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, PLAY_STORE_LINK);
            boolean facebookAppFound = false;
            List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
            for (ResolveInfo info : matches) {
                if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook")) {
                    intent.setPackage(info.activityInfo.packageName);
                    facebookAppFound = true;
                    break;
                }
            }

            if (facebookAppFound) {
                startActivity(intent);
            } else {
                String url = "https://facebook.com/sharer.php?u=" + shareText;
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setData(Uri.parse(url));
                // Verify that the intent will resolve to an activity
                if (sendIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(sendIntent);
                } else {
                    alertProgress.alertinfo(ShareActivity.this,
                            "No app supports this action.");
                }

            }

        } catch (Exception e) {

            e.printStackTrace();
            Toast.makeText(this, getString(R.string.facebookNotInstalled), Toast.LENGTH_SHORT).show();

        }
    }

    /**
     * <h2>openInstagram</h2>
     * <p> method for opening the Instagram for sharing the app</p>
     */
    private void openInstagram() {

        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_TITLE, getString(R.string.subjectForEmailShare));
            shareIntent.putExtra(Intent.EXTRA_TEXT,
                    getString(R.string.messageContentForSharingAppInOtherApp)/* + " " +
                    getString(R.string.hereMyCode) + " " +
                    referalCode + ".\n"*/ + " " +
                            PLAY_STORE_LINK);
            shareIntent.setPackage("com.instagram.android");
            startActivity(shareIntent);
        } catch (Exception e) {
            Toast.makeText(this, getString(R.string.instagramNotInstalled), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * <h2>openEmail</h2>
     * <p> method for opening the Emai for sharing the app</p>
     */
    @SuppressLint("IntentResset")
    private void openEmail() {
        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
        i.setDataAndType(Uri.parse("mailto:" + ""), "message/rfc822");
        i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.subjectForEmailShare));
        i.putExtra(Intent.EXTRA_TEXT,
                getString(R.string.messageContentForSharingAppInOtherApp)/* + " " +
                getString(R.string.hereMyCode) + " " +
                referalCode + ".\n"*/ + " " +
                        PLAY_STORE_LINK);
        startActivity(Intent.createChooser(i, "Send email"));
    }

    /**
     * <h2>openMessage</h2>
     * <p> method for opening the Message for sharing the app</p>
     */
    private void openMessage() {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("sms:"));
        intent.putExtra("sms_body",
                shareText /*getString(R.string.messageContentForSharingAppInOtherApp) + " " +
                getString(R.string.hereMyCode) + " " +
                referalCode + ".\n"*/ + " " +
                        PLAY_STORE_LINK);
        startActivity(intent);

    }

    /**
     * <h2>openWatsapp</h2>
     * <p> method for opening the Watsapp for sharing the app</p>
     */
    private void openWatsapp() {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT,
                shareText /*getString(R.string.messageContentForSharingAppInOtherApp) + " " +
                getString(R.string.hereMyCode) + " " +
                referalCode + ".\n"*/ + " " +
                        PLAY_STORE_LINK);
        try {
            startActivity(whatsappIntent);
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(this, getString(R.string.watsappNotInstalled),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * <h2>openSnapChat</h2>
     * <p> method for opening the SnapChat for sharing the app</p>
     */
    private void openSnapChat() {
        try {
            Intent snapIntent = new Intent(Intent.ACTION_VIEW);
            snapIntent.setDataAndType(Uri.parse("https://snapchat.com"), "text/plain");
            snapIntent.putExtra(Intent.EXTRA_TEXT,
                    shareText /*getString(R.string.messageContentForSharingAppInOtherApp)
                     + " " +
                    getString(R.string.hereMyCode) + " " +
                    referalCode + ".\n"*/ + " " +
                            PLAY_STORE_LINK);
            snapIntent.setPackage("com.snapchat.android");
            startActivity(snapIntent);
        } catch (Exception e) {
            Toast.makeText(this, getString(R.string.snapchatNotInstalled), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * <h2>openTwitter</h2>
     * <p>method for opening the Twitter for sharing the app</p>
     */
    private void openTwitter() {
        try {
            String url = "http://www.twitter.com/intent/tweet?url=" + PLAY_STORE_LINK
                    + "&text=" + shareText
                    /*getString(R.string.messageContentForSharingAppInOtherApp)*//* + " " +
                    getString(R.string.hereMyCode) + " " +
                    referalCode + "."*/;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        } catch (Exception e) {
            Toast.makeText(this, getString(R.string.twitterNotInstalled), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void setReferralCode(String desc, String refCode) {
        if (!isTextEmpty(refCode)) binding.tvReferralCode.setText(new StringBuilder()
                .append(" ").append(refCode));
        if (!isTextEmpty(desc)) binding.tvShareText.setText(desc);
    }

    @Override
    public void onSessionExpired() {

    }

    @Override
    public void onLogout(String message, SessionManagerImpl sessionManager) {
        if (alertProgress != null) alertProgress.alertPositiveOnclick(mContext, message,
                mContext.getResources().getString(R.string.logout), mContext.getResources().getString(R.string.ok)
                , isClicked -> setMAnagerWithBID(mContext, sessionManager));
    }

    @Override
    public void onError(String error) {
        if (alertProgress != null) alertProgress.alertinfo(mContext, error);
    }

    @Override
    public void onConnectionError(String connectionError) {

    }

    @Override
    public void onShowProgress() {

    }

    @Override
    public void onHideProgress() {

    }
}