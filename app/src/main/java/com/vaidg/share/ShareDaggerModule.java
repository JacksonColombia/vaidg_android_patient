package com.vaidg.share;

import com.vaidg.Dagger2.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>ShareDaggerModule</h2>
 * <p>
 * This is a Dagger module. We use this to pass in the View dependency to the
 * {@link SharePresenterImpl}.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
@Module
public interface ShareDaggerModule {
    @Binds
    @ActivityScoped
    ShareContract.SharePresenter providerPresenter(SharePresenterImpl presenter);

    @Binds
    @ActivityScoped
    ShareContract.ShareView providerView(ShareActivity activity);
}
