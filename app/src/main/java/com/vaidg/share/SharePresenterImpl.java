package com.vaidg.share;

import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.utility.RefreshToken;
import com.vaidg.networking.LSPServices;
import com.vaidg.networking.NoConnectivityException;
import com.vaidg.utilities.SessionManagerImpl;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.utilities.Constants.DATA;
import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.SESSION_EXPIRED;
import static com.vaidg.utilities.Constants.SESSION_LOGOUT;
import static com.vaidg.utilities.Constants.SHARESCREEN.DESCRIPTION;
import static com.vaidg.utilities.Constants.SHARESCREEN.REFERRALCODE;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;
import static com.vaidg.utilities.Constants.selLang;
import static com.vaidg.utilities.LSPApplication.getInstance;

/**
 * <h2>SharePresenterImpl</h2>
 * <p>
 * This class is used to call the API.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class SharePresenterImpl implements ShareContract.SharePresenter {
    public final String TAG = SharePresenterImpl.class.getSimpleName();
    @Inject
    LSPServices lspServices;
    @Inject
    ShareContract.ShareView view;
    @Inject
    Gson gson;
    @Inject
    SessionManagerImpl sessionManager;
    @Inject
    ShareActivity activity;

    @Inject
    SharePresenterImpl() {
    }

    @Override
    public void getPromoCode() {
        lspServices.getReferralCode(getInstance().getAuthToken(sessionManager.getSID()),
                selLang, PLATFORM_ANDROID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(@NotNull Disposable d) {
                    }

                    @Override
                    public void onNext(@NotNull Response<ResponseBody> responseBodyResponse) {
                        int code = responseBodyResponse.code();
                        try {
                            JSONObject jsonObject;
                            String response = responseBodyResponse.body() != null ?
                                    responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null
                                    ? responseBodyResponse.errorBody().string() : null;
                            switch (code) {
                                case SUCCESS_RESPONSE:
                                    if (response != null && !response.isEmpty()) {
                                        jsonObject = new JSONObject(response);
                                        JSONObject jsonData = jsonObject.getJSONObject(DATA);
                                        String desc = jsonData.getString(DESCRIPTION);
                                        String refCode = jsonData.getString(REFERRALCODE);
                                        if (view != null) view.setReferralCode(desc, refCode);
                                    }
                                    onHideProgress();
                                    break;
                                case SESSION_EXPIRED:
                                    //ErrorHandel errorHandel = gson.fromJson(errorBody,
                                    // ErrorHandel.class);
                                    RefreshToken.onRefreshToken(getInstance()
                                                    .getAuthToken(sessionManager.getSID()),
                                            sessionManager.getREFRESHAUTH(), lspServices,
                                            new RefreshToken.RefreshTokenImple() {
                                                @Override
                                                public void onSuccessRefreshToken(String newToken) {
                                                    onHideProgress();
                                                    getInstance()
                                                            .setAuthToken(sessionManager.getSID(),
                                                                    sessionManager.getSID(),
                                                                    newToken);
                                                    getPromoCode();
                                                }

                                                @Override
                                                public void onFailureRefreshToken() {
                                                    onHideProgress();
                                                }

                                                @Override
                                                public void sessionExpired(String msg) {
                                                    onLogout(msg);
                                                }
                                            });
                                    break;
                                case SESSION_LOGOUT:
                                    onLogout(errorBody);
                                    break;
                                default:
                                    onErrorMsg(errorBody);
                                    break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            onHideProgress();
                        }
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        onHideProgress();
                    }

                    @Override
                    public void onComplete() {
                        onHideProgress();
                    }
                });
    }

    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }

    @Override
    public void onHideProgress() {
        if (view != null) view.onHideProgress();
    }

    @Override
    public void onShowProgress() {
        if (view != null) view.onShowProgress();
    }

    @Override
    public void onLogout(String errorBody) {
        try {
            if (errorBody != null && !errorBody.isEmpty()) {
                JSONObject jsonObject = new JSONObject(errorBody);
                if (!jsonObject.getString(MESSAGE).isEmpty()) {
                    if (view != null) view.onLogout(jsonObject.getString(MESSAGE), sessionManager);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        onHideProgress();
    }

    @Override
    public void onErrorMsg(String errorBody) {
        try {
            if (errorBody != null && !errorBody.isEmpty()) {
                JSONObject jsonObject = new JSONObject(errorBody);
                if (!jsonObject.getString(MESSAGE).isEmpty()) {
                    if (view != null) view.onError(jsonObject.getString(MESSAGE));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        onHideProgress();
    }
}
