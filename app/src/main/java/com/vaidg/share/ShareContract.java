package com.vaidg.share;

import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;

/**
 * <h2>ShareContract</h2>
 * <p>
 * This class is used to act as a link between view and presenter.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public interface ShareContract {
    interface SharePresenter extends BasePresenter {
        /**
         * <h2>getPromoCode</h2>
         * <p>This method is used to get the referral code</p>
         */
        void getPromoCode();

        /**
         * <h2>onHideProgress</h2>
         * <p>This method is used to hide the loading view</p>
         */
        void onHideProgress();

        /**
         * <h2>onShowProgress</h2>
         * <p>This method is used to show the loading view</p>
         */
        void onShowProgress();

        /**
         * <h2>onLogout</h2>
         * <p>This method is used to show msg if session is expired</p>
         */
        void onLogout(String errorBody);

        /**
         * <h2>onErrorMsg</h2>
         * <p>This method is used to show error msg from api calls</p>
         */
        void onErrorMsg(String errorBody);
    }

    interface ShareView extends BaseView {
        /**
         * <h2>setReferralCode</h2>
         * <p>This method is used to set the referral code</p>
         */
        void setReferralCode(String desc, String refCode);
    }
}
