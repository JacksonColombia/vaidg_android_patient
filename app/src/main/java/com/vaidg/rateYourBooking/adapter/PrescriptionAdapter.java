package com.vaidg.rateYourBooking.adapter;


import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.vaidg.R;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Utility;
import com.pojo.InvoiceDetails;
import java.util.ArrayList;

import static com.vaidg.utilities.Constants.W_30;
import static com.vaidg.utilities.Constants.W_320;
import static com.vaidg.utilities.Constants.W_33;

/**
 * @author Pramod
 * @since 19-01-2018.
 */
public class PrescriptionAdapter extends RecyclerView.Adapter {
  private Context mContext;
  private ArrayList<InvoiceDetails.Medication> medications;
  private  int w30,w33;

  public PrescriptionAdapter(ArrayList<InvoiceDetails.Medication> medications) {
    this.medications = medications;
    calculate();
  }

  private void calculate() {
    w30 = Utility.getScreenWidth() * W_30 / W_320;
    w33 = Utility.getScreenWidth() * W_33 / W_320;
  }

  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_medication_desc, parent, false);
    mContext = parent.getContext();
    return new ViewHolderRecycler(itemView);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    ViewHolderRecycler hold = (ViewHolderRecycler) holder;
    hold.tvCompound.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
    hold.tvCompoundName.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_20));
    hold.tvDrugStrength.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
    hold.tvDrugStrengthDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
    hold.tvFrequency.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
    hold.tvFrequencyDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
    hold.tvDuration.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
    hold.tvDurationDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
    hold.tvRoute.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
    hold.tvRouteDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
    hold.tvDirection.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
    hold.tvDirectionDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
    hold.tvInstruction.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));
    hold.tvInstructionDesc.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.sp_14));

    int pos = position;
    hold.tvInstructionDesc.setText(medications.get(position).getInstruction());
    hold.tvDurationDesc.setText(medications.get(position).getDuration());
    hold.tvDrugStrengthDesc.setText(medications.get(position).getDrugStrength() + "" + medications.get(position).getDrugUnit());
    hold.tvRouteDesc.setText(medications.get(position).getDrugRoute());
    hold.tvFrequencyDesc.setText(medications.get(position).getFrequency());
    hold.tvDirectionDesc.setText(medications.get(position).getDirection());
    hold.tvCompound.setText("0" + (pos + 1) + "." + " " + "Compound Name");
    hold.tvCompoundName.setText(medications.get(position).getCompoundName());

  }

  @Override
  public int getItemCount() {
    return medications == null ? 0 : medications.size();
  }


  /**
   * <h2>ViewHolder</h2>
   * This method is used to hold the views
   */
  public class ViewHolderRecycler extends RecyclerView.ViewHolder {

    AppTypeface appTypeface;
    @BindView(R.id.tvCompound)
    TextView tvCompound;
    @BindView(R.id.tvCompoundName)
    TextView tvCompoundName;
    @BindView(R.id.tvInstruction)
    TextView tvInstruction;
    @BindView(R.id.tvInstructionDesc)
    TextView tvInstructionDesc;
    @BindView(R.id.tvDrugStrength)
    TextView tvDrugStrength;
    @BindView(R.id.tvDrugStrengthDesc)
    TextView tvDrugStrengthDesc;
    @BindView(R.id.tvFrequency)
    TextView tvFrequency;
    @BindView(R.id.tvFrequencyDesc)
    TextView tvFrequencyDesc;
    @BindView(R.id.tvDuration)
    TextView tvDuration;
    @BindView(R.id.tvDurationDesc)
    TextView tvDurationDesc;
    @BindView(R.id.tvRoute)
    TextView tvRoute;
    @BindView(R.id.tvRouteDesc)
    TextView tvRouteDesc;
    @BindView(R.id.tvDirection)
    TextView tvDirection;
    @BindView(R.id.tvDirectionDesc)
    TextView tvDirectionDesc;
    @BindView(R.id.llMedicine)
    RelativeLayout llMedicine;
    @BindView(R.id.llPres)
    LinearLayout llPres;
   @BindView(R.id.llDate)
    LinearLayout llDate;

    public ViewHolderRecycler(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
      appTypeface = AppTypeface.getInstance(mContext);
      tvCompound.setTypeface(appTypeface.getHind_semiBold());
      tvCompoundName.setTypeface(appTypeface.getHind_semiBold());
      tvRouteDesc.setTypeface(appTypeface.getHind_light());
      tvRoute.setTypeface(appTypeface.getHind_medium());
      tvDirection.setTypeface(appTypeface.getHind_medium());
      tvDirectionDesc.setTypeface(appTypeface.getHind_light());
      tvFrequency.setTypeface(appTypeface.getHind_medium());
      tvFrequencyDesc.setTypeface(appTypeface.getHind_light());
      tvDrugStrength.setTypeface(appTypeface.getHind_medium());
      tvDrugStrengthDesc.setTypeface(appTypeface.getHind_light());
      tvDuration.setTypeface(appTypeface.getHind_medium());
      tvDurationDesc.setTypeface(appTypeface.getHind_light());
      tvInstruction.setTypeface(appTypeface.getHind_medium());
      tvInstructionDesc.setTypeface(appTypeface.getHind_light());
      llDate.getLayoutParams().width =w33;
      llDate.getLayoutParams().height =w30;
    }
  }

}
