package com.vaidg.rateYourBooking;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.R;
import com.vaidg.home.MainActivity;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.FullscreenProgressDialog;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.mqtt.MQTTManager;
import com.mqtt.MqttEvents;
import com.pojo.AdditionalService;
import com.pojo.BookingAccounting;
import com.pojo.CartInfo;
import com.pojo.InvoiceDetails;
import com.utility.AlertProgress;
import com.utility.CalendarEventHelper;
import com.utility.DialogInterfaceListner;
import com.utility.NotificationUtils;
import com.utility.PicassoTrustAll;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import adapters.RatingAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;

public class RateYourBooking extends DaggerAppCompatActivity implements RateYourProviderContract.ViewContract
        , RatingAdapter.ViewChange {//,RatingBar.OnRatingBarChangeListener
    @BindView(R.id.tbLayout)
    Toolbar tbLayout;
    @BindView(R.id.tb_service_header)
    TextView tb_service_header;
    @BindView(R.id.tbServiceAvailable)
    TextView tbServiceAvailable;
    @BindView(R.id.tvTotalBillAmtTitle)
    TextView tvTotalBillAmtTitle;
    @BindView(R.id.tvTotalBillAmt)
    TextView tvTotalBillAmt;
    @BindView(R.id.tvReceipt)
    TextView tvReceipt;
    @BindView(R.id.tvPrescription)
    TextView tvPrescription;
    @BindView(R.id.tvAddToFav)
    TextView tvAddToFav;
    @BindView(R.id.tvRateProviderTitle)
    TextView tvRateProviderTitle;
    @BindView(R.id.tvRateProviderName)
    TextView tvRateProviderName;
    @BindView(R.id.tvRateProviderNameDesc)
    TextView tvRateProviderNameDesc;
    @BindView(R.id.ivProfilePic)
    ImageView ivProfilePic;
    @BindView(R.id.btnSave)
    Button btnSave;
    @BindView(R.id.tvComments)
    EditText tvComments;
    @BindView(R.id.rvGrid)
    RecyclerView rvGrid;
    @BindView(R.id.progressBarRateYourPro)
    ProgressBar progressBarRateYourPro;
    // @BindView(R.id.rbProvider)RatingBar rbProvider;
    @Inject
    AppTypeface appTypeface;
    @Inject
    RateYourProviderContract.Presenter presenter;
    @Inject
    SessionManagerImpl manager;
    @Inject
    MQTTManager mqttManager;
    @Inject
    AlertProgress alertProgress;
    private RatingAdapter adapter;
    private ArrayList<InvoiceDetails.CustomerRating> stringList;
    private ArrayList<AdditionalService> additionalServices = new ArrayList<>();
    private FullscreenProgressDialog mFullscreenProgressDialog;
    private CartInfo cartInfo;
    private BookingAccounting accounting;
    private String signURL, addressLine, providerId, docDegree, catId, currencySymbol, reminderId, categoryName, pdfFile, name, profilePic;
    private long mDateAndTime;
    private ArrayList<InvoiceDetails.Medication> medications = new ArrayList<>();
    private long bId;
    private int bookingModel;
    private ArrayList<String> pickUpList;
    private ArrayList<String> dropImageList;
    private int calltype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_your_booking);
        ButterKnife.bind(this);
        getIntentValue();
        setToolBarValue();
        initializeTypeFace();
    }

    private void getIntentValue() {

        if (getIntent().getExtras() != null) {

            bId = getIntent().getLongExtra("BID", 0);
            // isImageDrawn = true;
        }
    }

    private void initializeTypeFace() {
        tb_service_header.setTypeface(appTypeface.getHind_semiBold());
        tbServiceAvailable.setTypeface(appTypeface.getHind_semiBold());
        tvTotalBillAmtTitle.setTypeface(appTypeface.getHind_medium());
        tvTotalBillAmt.setTypeface(appTypeface.getHind_medium());
        tvReceipt.setTypeface(appTypeface.getHind_medium());
        tvPrescription.setTypeface(appTypeface.getHind_medium());
        tvAddToFav.setTypeface(appTypeface.getHind_regular());
        tvRateProviderTitle.setTypeface(appTypeface.getHind_medium());
        tvRateProviderName.setTypeface(appTypeface.getHind_medium());
        tvRateProviderNameDesc.setTypeface(appTypeface.getHind_regular());
        btnSave.setTypeface(appTypeface.getHind_semiBold());
        tvComments.setTypeface(appTypeface.getHind_regular());
        tb_service_header.setText(getString(R.string.your_last_booking_on));
        mFullscreenProgressDialog = new FullscreenProgressDialog();
    }

    private void setToolBarValue() {
        setSupportActionBar(tbLayout);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        // tbLayout.setNavigationIcon(R.drawable.ic_back);
        tbLayout.setNavigationOnClickListener(view -> onBackPressed());
        presenter.onInvoiceDetailsCalled(bId);
        stringList = new ArrayList<>();
        setAdapters();
    }

    private void setAdapters() {
        adapter = new RatingAdapter(stringList, this);
        //  rvGrid.setLayoutManager(new GridLayoutManager(this,3));
        rvGrid.setLayoutManager(new LinearLayoutManager(this));
        rvGrid.setAdapter(adapter);
        rvGrid.setNestedScrollingEnabled(false);
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.dp_4);
        RatingAdapter.SpacesItemDecoration itemDecoration = new RatingAdapter.SpacesItemDecoration(spacingInPixels);
        rvGrid.addItemDecoration(itemDecoration);
    }

    @OnClick({R.id.btnSave, R.id.tvReceipt, R.id.tvPrescription, R.id.tvAddToFav})
    public void onRateReviewClicked(View v) {
        switch (v.getId()) {
            case R.id.tvAddToFav:
                onShowProgress();
                if (tvAddToFav.getText().toString().equals(getString(R.string.removeFromFav)))
                    presenter.removeFromFav(providerId, catId);
                else
                    presenter.onAddToFav(providerId, catId);
                break;
            case R.id.tvReceipt:
                presenter.openDialog(RateYourBooking.this, calltype, appTypeface, cartInfo, signURL, accounting, addressLine, additionalServices, bookingModel, currencySymbol,
                        categoryName, pickUpList, dropImageList, this);
                break;
            case R.id.tvPrescription:
                mFullscreenProgressDialog.setDimAmount(0);
                mFullscreenProgressDialog.setPdfFile(pdfFile);
                mFullscreenProgressDialog.setmContext(this);
                mFullscreenProgressDialog.setName(name);
                mFullscreenProgressDialog.setProfilePic(profilePic);
                mFullscreenProgressDialog.setMedications(medications);
                mFullscreenProgressDialog.setCategoryName(categoryName);
                mFullscreenProgressDialog.setmDateAndTime(mDateAndTime);
                mFullscreenProgressDialog.setDocDegree(docDegree);
                mFullscreenProgressDialog.show(RateYourBooking.this);
                break;
            case R.id.btnSave:
                if (Utility.isNetworkAvailable(this)) {
                    onShowProgress();
                    presenter.onUpdateReview(bId, stringList, tvComments.getText().toString());
                    if (!"".equals(reminderId) && reminderId != null) {
                        CalendarEventHelper calendarEventHelper = new CalendarEventHelper(this);
                        calendarEventHelper.deleteEvent(Long.parseLong(reminderId));
                    }
                } else {
                    alertProgress.tryAgain(this, getString(R.string.pleaseCheckInternet), getString(R.string.system_error), isClicked -> {

                    });
                }
                break;
        }
    }

    @Override
    public void onSessionExpired() {

    }

    @Override
    public void onLogout(String message, SessionManagerImpl sessionManager) {
        alertProgress.alertPositiveOnclick(this, message, getString(R.string.logout), getString(R.string.ok), new DialogInterfaceListner() {
            @Override
            public void dialogClick(boolean isClicked) {
                Utility.setMAnagerWithBID(RateYourBooking.this, manager);
            }
        });
    }

    @Override
    public void onError(String error) {

        alertProgress.alertinfo(this, error);
    }

    @Override
    public void onConnectionError(String connectionError) {

    }

    @Override
    public void onShowProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBarRateYourPro.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideProgress() {
        progressBarRateYourPro.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onGetInvoiceDetails(InvoiceDetails.InvoiceData data) {
        additionalServices.clear();
        name = data.getProviderData().getTitle() + " " + data.getProviderData().getFirstName() + " " + data.getProviderData().getLastName();
        tvRateProviderName.setText(name);
        tvRateProviderNameDesc.setText(data.getCategoryName());
        if (data.isFavouriteProvider()) {
            tvAddToFav.setText(getString(R.string.removeFromFav));
        }
        calltype = data.getCart().getCallType();
        currencySymbol = data.getCurrencySymbol();
        if (data.getPickupImages() != null && data.getPickupImages().size() > 0)
            pickUpList = data.getPickupImages();
        if (data.getDropImages() != null && data.getDropImages().size() > 0)
            dropImageList = data.getDropImages();
        Utility.setAmtOnRecept(data.getAccounting().getTotal(), tvTotalBillAmt, currencySymbol);
        bookingModel = data.getBookingModel();
        if (data.getAdditionalService() != null && data.getAdditionalService().size() > 0)
            additionalServices.addAll(data.getAdditionalService());
        /*if(!data.getCallType().equals("3"))
        {
            tvPrescription.setVisibility(View.GONE);
        }*/
        if (data.getCustomerRating() != null && data.getCustomerRating().size() > 0)
            stringList.addAll(data.getCustomerRating());
        adapter.notifyDataSetChanged();
        providerId = data.getProviderData().getProviderId();
        catId = data.getCategoryId();
        presenter.timeMethod(tbServiceAvailable, data.getBookingRequestedFor());
        signURL = data.getSignURL();
        addressLine = data.getAddLine1();
        cartInfo = data.getCart();
        Log.d("TAG", "onGetInvoiceDetailsCART: " + cartInfo);
        accounting = data.getAccounting();
        categoryName = data.getCategoryName();
        profilePic = data.getProviderData().getProfilePic();
        pdfFile = data.getPdfFile();
        mDateAndTime = data.getBookingRequestedFor();
        if (data.getMedication() != null && data.getMedication().size() > 0) {
            medications = data.getMedication();
        }
        if (data.getProviderData().getDegree() != null && data.getProviderData().getDegree().size() > 0) {
            List<String> list = new ArrayList<>();
            for (InvoiceDetails.InvoiceData.DoctorDegree doctorDegree : data.getProviderData().getDegree()) {
                list.add(doctorDegree.getName());
            }
            docDegree = TextUtils.join(", ", list);
        }
        if (data.getProviderData().getProfilePic() != null && !data.getProviderData().getProfilePic().equals("")) {
            PicassoTrustAll.getInstance(this)
                    .load(data.getProviderData().getProfilePic())
                    .placeholder(R.drawable.register_profile_default_image)   // optional
                    .error(R.drawable.register_profile_default_image)// optional
                    .transform(new PicassoCircleTransform())
                    .into(ivProfilePic);

        }
        reminderId = data.getReminderId();
    }

    @Override
    protected void onDestroy() {
        try {
            if (mFullscreenProgressDialog != null && mFullscreenProgressDialog.isShowing()) {
                mFullscreenProgressDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public void onRateProviderSuccess() {

        if (!Constants.isHomeFragment) {
            Constants.isHomeFragment = false;
            Constants.isConfirmBook = false;
            Constants.isJobDetailsOpen = false;
            Intent intent = new Intent(RateYourBooking.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            if (mqttManager.isMQTTConnected())
                mqttManager.unSubscribeToTopic(MqttEvents.JobStatus.value + "/" + manager.getSID());
            startActivity(intent);
        }
        NotificationUtils.clearNotifications(this);
        finish();
    }

    @Override
    public void onGetStarList(ArrayList<String> stringList) {
        //this.stringList=stringList;
    }

    @Override
    public void onFavAdded(String message) {
        alertProgress.alertinfo(this, message);
        tvAddToFav.setText(getString(R.string.removeFromFav));
    }

    @Override
    public void removeFromFav(String message) {
        alertProgress.alertinfo(this, message);
        tvAddToFav.setText(getString(R.string.addToFav));
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }


    @Override
    public void onTextSelected(float rating, int position) {
        stringList.get(position).setRatings(rating);

    }
}
