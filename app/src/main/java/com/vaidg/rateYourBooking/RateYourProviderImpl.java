package com.vaidg.rateYourBooking;

import static com.vaidg.utilities.Constants.MESSAGE;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vaidg.utilities.LSPApplication;
import com.google.gson.Gson;
import com.vaidg.R;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.google.gson.JsonObject;
import com.pojo.AdditionalService;
import com.pojo.BookingAccounting;
import com.pojo.CartInfo;
import com.pojo.ErrorHandel;
import com.pojo.InvoiceDetails;
import com.utility.PicassoTrustAll;
import com.utility.RefreshToken;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import adapters.CourierFlowJobPhotosAdapter;
import adapters.SelectedService;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h2>RateYourProviderImpl</h2>
 * Created by Ali on 2/22/2018.
 */

public class RateYourProviderImpl implements RateYourProviderContract.Presenter {
    @Inject
    LSPServices lspServices;
    @Inject
    SessionManagerImpl manager;

    @Inject
    Gson gson;

    @Inject
    RateYourProviderContract.ViewContract view;
    private Dialog indialog;

    @Inject
    public RateYourProviderImpl() {
    }

    public static String formatHoursAndMinutes(long seconds) {
        long s = seconds % 60;
        long m = (seconds / 60) % 60;
        long h = (seconds / (60 * 60)) % 24;
        return String.format("%02d Hr %02d Mins %02d Secs", h, m, s);
    }

    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }

    @Override
    public void onInvoiceDetailsCalled(final long bId) {
        Log.d("SHIJEN ", "onInvoiceDetailsCalled: " + LSPApplication.getInstance().getAuthToken(manager.getSID()));
        Observable<Response<ResponseBody>> observable = lspServices.onToGetInvoiceDetails(LSPApplication.getInstance().getAuthToken(manager.getSID()),
                Constants.selLang,Constants.PLATFORM_ANDROID, bId);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {

                        int code = responseBodyResponse.code();
                        Log.d("TAG", "onNextInvoiceCode: " + code);
                        String response;
                        JSONObject jsonObject;
                        try {
                            switch (code) {
                                case Constants.SUCCESS_RESPONSE:
                                    response = responseBodyResponse.body().string();
                                    Log.d("TAG", "onNextInvoice: " + response);

                                    InvoiceDetails invoiceDetails = new Gson().fromJson(response, InvoiceDetails.class);
                                    if(view != null) {
                                        view.onGetInvoiceDetails(invoiceDetails.getData());
                                        view.onHideProgress();
                                    }
                                    break;
                                case Constants.SESSION_EXPIRED:
                                    response = responseBodyResponse.errorBody().string();
                                    ErrorHandel errorHandel = gson.fromJson(response, ErrorHandel.class);
                                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),manager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                            onInvoiceDetailsCalled(bId);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {

                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if(view != null) {
                                                view.onLogout(msg, manager);
                                                view.onHideProgress();
                                            }
                                        }
                                    });
                                    break;
                                case Constants.SESSION_LOGOUT:
                                    response = responseBodyResponse.errorBody().string();
                                    jsonObject = new JSONObject(response);
                                    if(view != null) {
                                        view.onLogout(jsonObject.getString("message"), manager);
                                        view.onHideProgress();
                                    }
                                    break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            if(view != null) {
                                view.onHideProgress();
                                view.onError(e.getMessage());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(Throwable e) {
                        if(view != null) {
                            view.onHideProgress();
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void onUpdateReview(final long bId, final ArrayList<InvoiceDetails.CustomerRating> stringList, final String reviewMsg) {


        JSONArray jsonArray = new JSONArray();
            for (InvoiceDetails.CustomerRating strList : stringList) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("name", strList.getName());
                    jsonObject.put("_id", strList.get_id());
                    jsonObject.put("rating", strList.getRatings());

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                jsonArray.put(jsonObject);
            }


       // Map<String, Object> jsonParams = new HashMap<>();
        JsonObject jsonParams = new JsonObject();
            jsonParams.addProperty("bookingId", bId);
            jsonParams.addProperty("rating", String.valueOf(jsonArray));
            jsonParams.addProperty("review", reviewMsg);

        /*Utility.printLog("Param"," "+String.valueOf(jsonArray));*/
        Observable<Response<ResponseBody>> observable = lspServices.onUpdateReview(LSPApplication.getInstance().getAuthToken(manager.getSID()),
                Constants.selLang,Constants.PLATFORM_ANDROID,jsonParams);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {


                        int code = responseBodyResponse.code();
                        Log.d("TAG", "onNextUpdateReview: " + code);
                        JSONObject jsonObject;
                        try {
                            String response = responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
                            switch (code) {
                                case Constants.SUCCESS_RESPONSE:
                                    if(view != null) {
                                        view.onHideProgress();
                                        view.onRateProviderSuccess();
                                    }
                                    break;
                                case Constants.SESSION_EXPIRED:
                                    ErrorHandel errorHandel = gson.fromJson(response, ErrorHandel.class);
                                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),manager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            if(view != null) {
                                                view.onHideProgress();
                                            }
                                            LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                            onUpdateReview(bId, stringList, reviewMsg);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {

                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if(view != null) {
                                                view.onLogout(msg, manager);
                                                view.onHideProgress();
                                            }
                                        }
                                    });
                                    // view.onHideProgress();
                                    break;
                                case Constants.SESSION_LOGOUT:
                                    jsonObject = new JSONObject(response);
                                    if(view != null) {
                                        view.onHideProgress();
                                        view.onLogout(jsonObject.getString(MESSAGE), manager);
                                    }
                                    break;
                                default:
                                    jsonObject = new JSONObject(response);
                                    if(view != null) {
                                        view.onHideProgress();
                                        view.onError(new JSONObject(response).getString(MESSAGE));
                                    }
                                    break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            if(view != null) {
                                view.onHideProgress();
                                view.onError(e.getMessage());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            if(view != null) {
                                view.onHideProgress();
                                view.onError(e.getMessage());
                            }

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(view != null) {
                            view.onHideProgress();
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void timeMethod(TextView tbServiceAvailable, long bookingRequestedFor) {
        try {


            Log.d("TAGTIME", " expireTime " + bookingRequestedFor);
            Date date = new Date(TimeUnit.SECONDS.toMillis(bookingRequestedFor));
            SimpleDateFormat sdf = new SimpleDateFormat("MMMM d, yyyy", Locale.US);
            sdf.setTimeZone(Utility.getTimeZone());
            String formattedDate = sdf.format(date);

          /*  String formattedDated = Utility.getFormattedDate(date);
            String[] dat1 = formattedDated.split("|");
            String[] splitDate = dat1[0].split(" ");

            String fullDate = splitDate[1] + " " + splitDate[2] + " " + splitDate[3];
          */  //   tbServiceAvailable.setText(dat1[0]);
            tbServiceAvailable.setText(formattedDate);

        } catch (Exception e) {
            Log.d("TAG", "timeMethodException: " + e.toString());
        }
    }

    @Override
    public void getStringList() {
        ArrayList<String> stringList = new ArrayList();
        stringList.add("service");
        stringList.add("quality");
        stringList.add("behaviour");
        stringList.add("on time");
        stringList.add("other");
        stringList.add("cool");
        stringList.add("awesome");
        // view.onGetStarList(stringList);
    }


/*
    public static String formatHoursAndMinutes(int totalMinutes) {
        String minutes = Integer.toString(totalMinutes % 60);
        minutes = minutes.length() == 1 ? "0" + minutes : minutes;
        return (totalMinutes / 60) + ":" + minutes;
    }
*/

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void openDialog(RateYourBooking rateYourBooking, int callType, AppTypeface appTypeface, CartInfo cartInfo, String signURL, BookingAccounting accounting, String addressLine,
                           ArrayList<AdditionalService> additionalServices, int bookingModel, String currencySymbol, String categoryName, ArrayList<String> pickUpList, ArrayList<String> dropImageList, Context context) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(rateYourBooking);
        TextView tvInvoiceDialog, tvInvoiceDialogDismiss, tvInvGigTimeFee,
                tvInvGigTimeFeeAmt, tvInvDiscount, tvInvDiscountAmount, tvInvoiceTotal, tvInvoicePayDialog,
                tvInvoiceTotalAmt, tvVisitAmount, tvLocation, tvReceiptDetailsHead, tvTravelLabel, tvTravelAmount,
                tvDropPhotos, tvPickupPhotos, tvJobDialog, tvInvCard, tvInvAmount,tvsignature;
        ImageView ivInvoiceSignature;
        RelativeLayout rlAdditionalService, rldiscount, rlVisitFee, rltravelLayout, rlInvGigTimeFee, rlpayment;
        RecyclerView rv_jobDropPhotos, rv_jobPickUpPhotos;
        LinearLayout containerAdditional;
        indialog = new Dialog(rateYourBooking);
        indialog.setCanceledOnTouchOutside(true);
        indialog.setCancelable(true);
        indialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        indialog.setContentView(R.layout.popup_invoice_fee_breakdown);
        indialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        indialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        tvInvoiceDialog = indialog.findViewById(R.id.tvInvoiceDialog);
        tvInvAmount = indialog.findViewById(R.id.tvInvAmount);
        tvInvCard = indialog.findViewById(R.id.tvInvCard);
        rlpayment = indialog.findViewById(R.id.rlpayment);
        tvInvoiceDialogDismiss = indialog.findViewById(R.id.tvInvoiceDialogDismiss);
        tvInvGigTimeFee = indialog.findViewById(R.id.tvInvGigTimeFee);
        tvInvGigTimeFeeAmt = indialog.findViewById(R.id.tvInvGigTimeFeeAmt);
        tvInvDiscount = indialog.findViewById(R.id.tvInvDiscount);
        tvInvDiscountAmount = indialog.findViewById(R.id.tvInvDiscountAmount);
        tvInvoiceTotal = indialog.findViewById(R.id.tvInvoiceTotal);
        tvInvoiceTotalAmt = indialog.findViewById(R.id.tvInvoiceTotalAmt);
        ivInvoiceSignature = indialog.findViewById(R.id.ivInvoiceSignature);
        tvsignature = indialog.findViewById(R.id.tvsignature);
        tvVisitAmount = indialog.findViewById(R.id.tvVisitAmount);
        tvLocation = indialog.findViewById(R.id.tvLocation);
        tvReceiptDetailsHead = indialog.findViewById(R.id.tvReceiptDetailsHead);
        RecyclerView recyclerView = indialog.findViewById(R.id.llLiveFee);
        rldiscount = indialog.findViewById(R.id.rldiscount);
        rlVisitFee = indialog.findViewById(R.id.rlVisitFee);
        rltravelLayout = indialog.findViewById(R.id.rltravelLayout);
        tvTravelLabel = indialog.findViewById(R.id.tvTravelLabel);
        tvTravelAmount = indialog.findViewById(R.id.tvTravelAmount);
        rlAdditionalService = indialog.findViewById(R.id.rlAdditionalService);
        containerAdditional = indialog.findViewById(R.id.containerAdditional);
        rlInvGigTimeFee = indialog.findViewById(R.id.rlInvGigTimeFee);
        rv_jobPickUpPhotos = indialog.findViewById(R.id.rv_jobPickUpPhotos);
        rv_jobDropPhotos = indialog.findViewById(R.id.rv_jobDropPhotos);
        tvPickupPhotos = indialog.findViewById(R.id.tvPickupPhotos);
        tvDropPhotos = indialog.findViewById(R.id.tvDropPhotos);
        tvJobDialog = indialog.findViewById(R.id.tvJobDialog);
        tvInvoicePayDialog = indialog.findViewById(R.id.tvInvoicePayDialog);
        tvJobDialog.setTypeface(appTypeface.getHind_regular());

        if (pickUpList != null && pickUpList.size() > 0) {
            tvPickupPhotos.setVisibility(View.VISIBLE);
        }
        if (dropImageList != null && dropImageList.size() > 0) {
            tvDropPhotos.setVisibility(View.VISIBLE);
        }
        rv_jobPickUpPhotos.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rv_jobPickUpPhotos.setAdapter(new CourierFlowJobPhotosAdapter(pickUpList, null, false));

        rv_jobDropPhotos.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rv_jobDropPhotos.setAdapter(new CourierFlowJobPhotosAdapter(dropImageList, null, false));

        switch (callType)
        {
            case 2:
                tvJobDialog.setText(context.getString(R.string.job_location));
                tvLocation.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_place_black_24dp, 0, 0, 0);
                tvLocation.setText(addressLine);
                break;
            case 3:
                tvJobDialog.setText(context.getString(R.string.time_slot));
                tvLocation.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                tvLocation.setText(formatHoursAndMinutes(accounting.getTotalActualJobTimeMinutes()));
                //  Log.d("abc", "openDialog: " + formatHoursAndMinutes(accounting.getTotalActualJobTimeMinutes()));
                if(!TextUtils.isEmpty(accounting.getLast4()))
                tvInvCard.setText(context.getString(R.string.card_ending_by) + " " + accounting.getLast4());
                if (accounting.getDiscount() > 0) {
                    Utility.setAmtOnRecept(accounting.getVisitFee(), tvInvAmount, currencySymbol);
                    // rlVisitFee.setVisibility(View.VISIBLE);
                }

                Utility.setAmtOnRecept(accounting.getTotal(), tvInvAmount, currencySymbol);

                break;
            default:
                tvJobDialog.setVisibility(View.GONE);
                tvLocation.setVisibility(View.GONE);
                break;
        }

        if (bookingModel != 3) {
            try {
                SelectedService selectedService = new SelectedService( false);
                ArrayList<CartInfo.CheckOutItem> cartInfoItem = cartInfo.getCheckOutItem();
                Constants.bookingcurrencySymbol = currencySymbol;
                recyclerView.setLayoutManager(linearLayoutManager);
                selectedService.onCheckOutItem(cartInfoItem);
                if (callType == 1 || callType == 3) {
                    selectedService.onInCallValue(1);
                }
                recyclerView.setAdapter(selectedService);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Utility.setAmtOnRecept(accounting.getBidPrice(), tvInvGigTimeFeeAmt, currencySymbol);
            rlInvGigTimeFee.setVisibility(View.VISIBLE);
            tvInvGigTimeFee.setText(categoryName);
        }

        tvReceiptDetailsHead.setTypeface(appTypeface.getHind_semiBold());
        tvVisitAmount.setTypeface(appTypeface.getHind_regular());
        tvInvoiceDialog.setTypeface(appTypeface.getHind_bold());
        tvJobDialog.setTypeface(appTypeface.getHind_bold());
        tvInvoiceDialogDismiss.setTypeface(appTypeface.getHind_bold());
        tvInvoicePayDialog.setTypeface(appTypeface.getHind_bold());
        tvInvoiceTotal.setTypeface(appTypeface.getHind_semiBold());
        tvInvoiceTotalAmt.setTypeface(appTypeface.getHind_semiBold());
        tvInvDiscountAmount.setTypeface(appTypeface.getHind_regular());
        tvInvDiscount.setTypeface(appTypeface.getHind_regular());
        tvInvGigTimeFee.setTypeface(appTypeface.getHind_regular());
        tvInvGigTimeFeeAmt.setTypeface(appTypeface.getHind_regular());
        tvLocation.setTypeface(appTypeface.getHind_regular());
        if(callType != 2)
        {
            tvsignature.setVisibility(View.GONE);
            ivInvoiceSignature.setVisibility(View.GONE);
        }else{
            tvsignature.setVisibility(View.GONE);
            ivInvoiceSignature.setVisibility(View.GONE);
        }
        Log.d("TAG", "openDialog: " + signURL);
        if (signURL != null && !signURL.isEmpty()) {
            PicassoTrustAll.getInstance(rateYourBooking)
                    .load(signURL)
                    .into(ivInvoiceSignature);

        }

        tvInvoiceDialogDismiss.setOnClickListener(view -> indialog.dismiss());
        Utility.setAmtOnRecept(accounting.getTotal(), tvInvoiceTotalAmt, currencySymbol);
        if (accounting.getDiscount() > 0)
            Utility.setAmtOnRecept(accounting.getDiscount(), tvInvDiscountAmount, currencySymbol);
        else
            rldiscount.setVisibility(View.GONE);
        if (accounting.getDiscount() > 0) {
            Utility.setAmtOnRecept(accounting.getVisitFee(), tvVisitAmount, currencySymbol);
            rlVisitFee.setVisibility(View.VISIBLE);
        } else
            rlVisitFee.setVisibility(View.GONE);
        if (accounting.getTravelFee() > 0) {
            Utility.setAmtOnRecept(accounting.getTravelFee(), tvTravelAmount, currencySymbol);
            rltravelLayout.setVisibility(View.VISIBLE);
        }
        if (additionalServices.size() > 0) {
            rlAdditionalService.setVisibility(View.VISIBLE);
            onAdditionalServiceSet(appTypeface, rateYourBooking, containerAdditional, additionalServices, rlAdditionalService);
        }

        /*  private void onAdditionalServiceSet()
    {

    }*/

        indialog.show();
    }

    private void onAdditionalServiceSet(AppTypeface appTypeface, RateYourBooking rateYourBooking, LinearLayout containerAdditional, ArrayList<AdditionalService> additionalServices, RelativeLayout rlAdditionalService) {
        for (int i = 0; i < additionalServices.size(); i++) {
            TextView tvAddtionName, tvAddtionPrice;
            View view = LayoutInflater.from(rateYourBooking).inflate(R.layout.additiona_service, rlAdditionalService, false);
            containerAdditional.addView(view);

            tvAddtionPrice = view.findViewById(R.id.tvAddtionPrice);
            tvAddtionName = view.findViewById(R.id.tvAddtionName);
            tvAddtionPrice.setTypeface(appTypeface.getHind_light());
            tvAddtionName.setTypeface(appTypeface.getHind_light());
            Log.d("Shijen", "onAdditionalServiceSet: " + additionalServices.get(i).getServiceName());
            tvAddtionName.setText(additionalServices.get(i).getServiceName());
            Utility.setAmtOnRecept(additionalServices.get(i).getPrice(), tvAddtionPrice, Constants.bookingcurrencySymbol);
        }
    }

    @Override
    public void onAddToFav(String providerId, String catId) {

        Map<String, Object> jsonParams = new HashMap<>();
        jsonParams.put("categoryId", catId);
        jsonParams.put("providerId", providerId);

        Observable<Response<ResponseBody>> responseObservable = lspServices.addTOFav(LSPApplication.getInstance().getAuthToken(manager.getSID()), Constants.selLang,Constants.PLATFORM_ANDROID,jsonParams);

        responseObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int code = responseBodyResponse.code();
                        JSONObject jsonObject;
                        String response;
                        try {


                            switch (code) {
                                case Constants.SUCCESS_RESPONSE:
                                    response = responseBodyResponse.body().string();
                                    Log.d("TAG", "onNext: " + response);
                                    if(view != null) {
                                        view.onHideProgress();
                                        view.onFavAdded(new JSONObject(response).getString("message"));
                                    }

                                    break;
                                case Constants.SESSION_EXPIRED:
                                    response = responseBodyResponse.errorBody().string();
                                    ErrorHandel errorHandel = gson.fromJson(response, ErrorHandel.class);
                                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),manager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                            onAddToFav(providerId, catId);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {

                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if(view != null) {
                                                view.onLogout(msg, manager);
                                                view.onHideProgress();
                                            }
                                        }
                                    });
                                    // view.onHideProgress();
                                    break;
                                case Constants.SESSION_LOGOUT:
                                    response = responseBodyResponse.errorBody().string();
                                    jsonObject = new JSONObject(response);
                                    if(view != null) {
                                        view.onHideProgress();
                                        view.onLogout(jsonObject.getString("message"), manager);
                                    }
                                    break;
                                case 411:
                                    response = responseBodyResponse.errorBody().string();
                                    if(view != null) {
                                        view.onFavAdded(new JSONObject(response).getString("message"));
                                        view.onHideProgress();
                                    }
                                    break;
                                default:
                                    response = responseBodyResponse.errorBody().string();
                                    jsonObject = new JSONObject(response);
                                    if(view != null) {
                                        view.onHideProgress();
                                        view.onLogout(jsonObject.getString("message"), manager);
                                    }
                                    break;

                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                            if(view != null) {
                                view.onHideProgress();
                                view.onError(e.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(view != null) {
                            view.onHideProgress();
                            view.onError(e.getMessage());
                        }
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void removeFromFav(String providerId, String catId) {
        Observable<Response<ResponseBody>> responseObservable = lspServices.removeFromFav(LSPApplication.getInstance().getAuthToken(manager.getSID()), Constants.selLang,Constants.PLATFORM_ANDROID, catId,
                providerId);

        responseObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {

                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        int code = responseBodyResponse.code();
                        String response;
                        try {


                            switch (code) {
                                case Constants.SUCCESS_RESPONSE:
                                    response = responseBodyResponse.body().string();
                                    Log.d("TAG", "onNext: " + response);
                                    if(view != null) {
                                        view.removeFromFav(new JSONObject(response).getString("message"));
                                        view.onHideProgress();
                                    }
                                    break;
                                case Constants.SESSION_EXPIRED:
                                    response = responseBodyResponse.errorBody().string();
                                    ErrorHandel errorHandel = gson.fromJson(response, ErrorHandel.class);
                                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),manager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                            onAddToFav(providerId, catId);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {

                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if(view != null) {
                                                view.onLogout(msg, manager);
                                                view.onHideProgress();
                                            }
                                        }
                                    });
                                    // view.onHideProgress();
                                    break;
                                case Constants.SESSION_LOGOUT:
                                    response = responseBodyResponse.errorBody().string();
                                    if(view != null) {
                                        view.onHideProgress();
                                        view.onLogout(new JSONObject(response).getString("message"), manager);
                                    }
                                    break;
                                default:
                                    response = responseBodyResponse.errorBody().string();
                                    if(view != null) {
                                        view.onError(new JSONObject(response).getString("message"));
                                        view.onHideProgress();
                                    }
                                    break;

                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                            if(view != null) {
                                view.onHideProgress();
                                view.onError(e.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(view != null) {
                            view.onHideProgress();
                            view.onError(e.getMessage());
                        }
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
