package com.vaidg.cancelledBooking;

import com.vaidg.Dagger2.ActivityScoped;
import com.vaidg.Dagger2.FragmentScoped;
import com.vaidg.jobDetailsStatus.JobDetailsContract;
import com.vaidg.jobDetailsStatus.JobDetailsContractImpl;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * <h2>CancelledBookingModule</h2>
 * Created by Ali on 3/14/2018.
 */

@Module
public interface CancelledBookingModule
{

    @FragmentScoped
    @ContributesAndroidInjector
    ReceiptFragment provideReceiptFragment();

    @FragmentScoped
    @ContributesAndroidInjector
    HelpFragment provideHelpFragment();

    @Binds
    @ActivityScoped
    CancelledBookingContract.Presenter providePresenter(CancelledBookingContractImpl cancelledBookingContract);

    @Binds
    @ActivityScoped
    CancelledBookingContract.JobView provideView(CancelledBooking cancelledBooking);

}
