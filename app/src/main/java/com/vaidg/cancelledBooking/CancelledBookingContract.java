package com.vaidg.cancelledBooking;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.vaidg.cancelledBooking.model.MedicationPojo;
import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;
import com.vaidg.jobDetailsStatus.JobDetailsActivity;
import com.vaidg.networking.ChatApiService;
import com.google.android.gms.maps.model.LatLng;
import com.pojo.BidDispatchLog;
import com.pojo.BookingAccounting;
import com.pojo.BookingTimer;
import com.pojo.CartInfo;
import com.pojo.ProviderDetailsBooking;
import com.pojo.SymptomQuestionAnswer;

import java.util.ArrayList;

/**
 * <h2>JobDetailsContract</h2>
 * Created by Ali on 2/15/2018.
 */

public interface CancelledBookingContract
{
    interface Presenter extends BasePresenter
    {
        void onGetBookingDetails(long bid);
    }
    interface JobView extends BaseView
    {
        void onSuccessBooking(MedicationPojo.Data bookDD);

        void noSymptom();
    }
}
