package com.vaidg.cancelledBooking;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.vaidg.R;
import com.vaidg.cancelledBooking.model.MedicationPojo;
import com.vaidg.encryptiondecryption.CryptographyExample;
import com.vaidg.jobDetailsStatus.JobDetailsActivity;
import com.vaidg.jobDetailsStatus.JobDetailsContract;
import com.vaidg.networking.ChatApiService;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pojo.BookingDetailsPojo;
import com.pojo.ErrorHandel;
import com.pojo.PostCallResponse;
import com.pojo.SymptomQuestionAnswer;
import com.utility.RefreshToken;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilPrivateKey;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.utilities.Constants.AES_MODE;
import static com.vaidg.utilities.Constants.ANDROID_KEYSTORE;
import static com.vaidg.utilities.Constants.FIXED_IV;
import static com.vaidg.utilities.Constants.IV_PARAMETER_SPEC;
import static com.vaidg.utilities.Constants.KEY_PRIVATEALIAS;
import static com.vaidg.utilities.Constants.KEY_PUBLICALIAS;
import static com.vaidg.utilities.Constants.RSA_MODE;

/**
 * <h2>JobDetailsContractImpl</h2>
 * Created by Ali on 2/15/2018.
 */

public class CancelledBookingContractImpl implements CancelledBookingContract.Presenter {
  @Inject
  SessionManagerImpl manager;
  @Inject
  LSPServices lspServices;
  @Inject
  CancelledBookingContract.JobView view;
  @Inject
  Gson gson;
  private String TAG = CancelledBookingContractImpl.class.getSimpleName();
  private String callId;
  private KeyStore keyStore;

  @Inject
  public CancelledBookingContractImpl() {

  }

  @Override
  public void attachView(Object view) {

  }

  @Override
  public void detachView() {

  }

  @Override
  public void onGetBookingDetails(final long bid) {
    Log.d(TAG, "onGetBookingDetails: " + bid);
    Observable<Response<ResponseBody>> observable = lspServices.onToGetBookingDetails(LSPApplication.getInstance().getAuthToken(manager.getSID())
            , Constants.selLang, Constants.PLATFORM_ANDROID, bid);

    observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Observer<Response<ResponseBody>>() {
              @Override
              public void onSubscribe(Disposable d) {

              }

              @Override
              public void onNext(Response<ResponseBody> responseBodyResponse) {

                int code = responseBodyResponse.code();
                Log.d(TAG, "onNextJobDetails: " + code);
                String response;
                JSONObject jsonObject;
                try {
                  switch (code) {
                    case Constants.SUCCESS_RESPONSE:
                      response = responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
                      Log.d(TAG, "onNextJobDetailsRes: " + response);
                      try {
                        MedicationPojo bookingDetailsPojo = gson.fromJson(response, MedicationPojo.class);
                        MedicationPojo.Data bookDD = bookingDetailsPojo.getData();
                        if(view != null) {
                          view.onSuccessBooking(bookDD);
                        }
                      } catch (Exception e) {
                        e.printStackTrace();
                      }
                      // gson
                      break;
                    case Constants.SESSION_LOGOUT:
                      response = responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null;
                      jsonObject = new JSONObject(response);
                      if(view != null) {
                        view.onHideProgress();
                        view.onLogout(jsonObject.getString("message"), manager);
                      }
                      break;
                    case Constants.SESSION_EXPIRED:
                      response = responseBodyResponse.errorBody().string();
                      ErrorHandel errorHandel = gson.fromJson(response, ErrorHandel.class);

                      // String
                      RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                        @Override
                        public void onSuccessRefreshToken(String newToken) {
                          LSPApplication.getInstance().setAuthToken(manager.getSID(), manager.getSID(), newToken);
                          onGetBookingDetails(bid);
                        }

                        @Override
                        public void onFailureRefreshToken() {

                        }

                        @Override
                        public void sessionExpired(String msg) {
                          if(view != null) {
                            view.onHideProgress();
                            view.onLogout(msg, manager);
                          }
                        }
                      });
                      break;
                  }
                } catch (IOException | JSONException e) {
                  e.printStackTrace();
                }

              }

              @Override
              public void onError(Throwable e) {

              }

              @Override
              public void onComplete() {

              }
            });
  }
}