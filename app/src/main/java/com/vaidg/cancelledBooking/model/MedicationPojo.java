package com.vaidg.cancelledBooking.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pojo.InvoiceDetails;

import java.io.Serializable;
import java.util.ArrayList;

public class MedicationPojo implements Serializable
{

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public Data data;

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public class Data implements Serializable
    {

        @SerializedName("bookingId")
        @Expose
        public long bookingId;
        @SerializedName("bookingModel")
        @Expose
        public int bookingModel;
        @SerializedName("callType")
        @Expose
        public int callType;
        @SerializedName("bookingRequestedFor")
        @Expose
        public long bookingRequestedFor;
        @SerializedName("bookingRequestedAt")
        @Expose
        public long bookingRequestedAt;
        @SerializedName("bookingExpireTime")
        @Expose
        public long bookingExpireTime;
        @SerializedName("bookingEndtime")
        @Expose
        public long bookingEndtime;
        @SerializedName("currencySymbol")
        @Expose
        public String currencySymbol;
        @SerializedName("currency")
        @Expose
        public String currency;
        @SerializedName("currencyAbbr")
        @Expose
        public String currencyAbbr;
        @SerializedName("serverTime")
        @Expose
        public long serverTime;
        @SerializedName("bookingType")
        @Expose
        public int bookingType;
        @SerializedName("status")
        @Expose
        public int status;
        @SerializedName("statusMsg")
        @Expose
        public String statusMsg;
        @SerializedName("providerDetail")
        @Expose
        public ProviderDetail providerDetail;
        @SerializedName("categoryName")
        @Expose
        public String categoryName;
        @SerializedName("categoryId")
        @Expose
        public String categoryId;
        @SerializedName("medication")
        @Expose
        public ArrayList<InvoiceDetails.Medication> medication;
        @SerializedName("pdfFile")
        @Expose
        public String pdfFile;
        @SerializedName("degree")
        @Expose
        private String degree;

        public String getDegree() {
            return degree;
        }

        public long getBookingId() {
            return bookingId;
        }

        public int getBookingModel() {
            return bookingModel;
        }

        public int getCallType() {
            return callType;
        }

        public long getBookingRequestedFor() {
            return bookingRequestedFor;
        }

        public long getBookingRequestedAt() {
            return bookingRequestedAt;
        }

        public long getBookingExpireTime() {
            return bookingExpireTime;
        }

        public long getBookingEndtime() {
            return bookingEndtime;
        }

        public String getCurrencySymbol() {
            return currencySymbol;
        }

        public String getCurrency() {
            return currency;
        }

        public String getCurrencyAbbr() {
            return currencyAbbr;
        }

        public long getServerTime() {
            return serverTime;
        }

        public int getBookingType() {
            return bookingType;
        }

        public int getStatus() {
            return status;
        }

        public String getStatusMsg() {
            return statusMsg;
        }

        public ProviderDetail getProviderDetail() {
            return providerDetail;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public ArrayList<InvoiceDetails.Medication> getMedication() {
            return medication;
        }

        public String getPdfFile() {
            return pdfFile;
        }


        public class ProviderDetail implements Serializable
        {

            @SerializedName("title")
            @Expose
            public String title;
            @SerializedName("firstName")
            @Expose
            public String firstName;
            @SerializedName("lastName")
            @Expose
            public String lastName;
            @SerializedName("profilePic")
            @Expose
            public String profilePic;

            public String getTitle() {
                return title;
            }

            public String getFirstName() {
                return firstName;
            }

            public String getLastName() {
                return lastName;
            }

            public String getProfilePic() {
                return profilePic;
            }
        }


    }



}

