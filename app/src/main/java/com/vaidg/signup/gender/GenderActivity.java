package com.vaidg.signup.gender;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import com.vaidg.R;
import com.vaidg.model.CategoryCity;
import com.vaidg.signup.gender.adapter.GenderListAdapter;
import com.vaidg.signup.gender.model.GenderListPojo;
import com.vaidg.utilities.AppTypeface;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.vaidg.utilities.Constants.ADDRESSPOJO;
import static com.vaidg.utilities.Constants.FULLADDRESS;

public class GenderActivity extends AppCompatActivity {

    private GenderListAdapter mGenderListAdapter;
    private ArrayList<GenderListPojo> mGenderArrayList = new ArrayList<>();
    @BindView(R.id.rvGender)
    RecyclerView rvGender;
    private Context mContext;
    private LinearLayoutManager mLayoutManager;
    AppTypeface appTypeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gender);
        ButterKnife.bind(this);
        mContext = this;
        appTypeface = AppTypeface.getInstance(mContext);
        poupulateDataList();
        initialize();
    }

    private void initialize() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        TextView textView = toolbar.findViewById(R.id.tv_center);
        textView.setText(getResources().getString(R.string.selectGender));
        textView.setTypeface(appTypeface.getHind_semiBold());
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        rvGender.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(mContext);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvGender.setLayoutManager(mLayoutManager);
        rvGender.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(mContext, R.anim.layout_animation_right_to_left);
        rvGender.setLayoutAnimation(controller);
        mGenderListAdapter = new GenderListAdapter(mGenderArrayList, (position, genderArrayList, genderName, tvGenderName) -> {
            Intent intent = new Intent();
            intent.putExtra("GenderName",genderName.getId());
            intent.putExtra("mGenderArrayList",genderArrayList);
            setResult(RESULT_OK, intent);
            finish();
            overridePendingTransition(R.anim.mainfadein,R.anim.slide_down_acvtivity);

        });
        rvGender.setAdapter(mGenderListAdapter);
    }

    private void poupulateDataList() {
        if(getIntent().getExtras()!=null) {
            mGenderArrayList = (ArrayList<GenderListPojo>) getIntent().getSerializableExtra("mGenderArrayList");
        }

        if(mGenderArrayList != null && mGenderArrayList.size() == 0) {
            mGenderArrayList.add(new GenderListPojo("1", "Male"));
            mGenderArrayList.add(new GenderListPojo("2", "Female"));
            mGenderArrayList.add(new GenderListPojo("3", "Others"));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.mainfadein,R.anim.slide_down_acvtivity);
    }
}
