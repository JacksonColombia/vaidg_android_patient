package com.vaidg.signup.gender.adapter;


import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.R;
import com.vaidg.signup.gender.model.GenderListPojo;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.PicassoCircleTransform;
import com.utility.PicassoTrustAll;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * <h2>GenderListAdapter</h2>
 *
 * <p>This is an inner class of adapter type, to integrate the gender list on screen.</p>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */

public class GenderListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<GenderListPojo> mGenderArrayList = new ArrayList<>();
    private OnItemClickListener listener;


    public GenderListAdapter(ArrayList<GenderListPojo> genderArrayList, OnItemClickListener listener) {
        this.mGenderArrayList = genderArrayList;
        this.listener = listener;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_gender,
                viewGroup, false);
        mContext = viewGroup.getContext();
        return new SingleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        SingleViewHolder singleViewHolder = (SingleViewHolder) viewHolder;
        singleViewHolder.tvGenderName.setTextSize(TypedValue.COMPLEX_UNIT_PX,mContext.getResources().getDimensionPixelSize(R.dimen.sp_16));
        singleViewHolder.tvGenderName.setTag(position);
        mGenderArrayList.get(position).setGenderName(mGenderArrayList.get(position).getGenderName());
        mGenderArrayList.get(position).setId(mGenderArrayList.get(position).getId());
        singleViewHolder.tvGenderName.setText(mGenderArrayList.get(position).getGenderName());

        if(mGenderArrayList.get(position).isChecked())
        {
            mGenderArrayList.get(position).setChecked(true);
            singleViewHolder.itemView.setSelected(true);
            singleViewHolder.ivGender.setImageResource(R.drawable.ic_check);
        }
        else{
            mGenderArrayList.get(position).setChecked(false);
            singleViewHolder.itemView.setSelected(false);
            singleViewHolder.ivGender.setImageResource(R.drawable.ic_unselected);
        }

    }



    @Override
    public int getItemCount() {
        return mGenderArrayList == null ? 0 : mGenderArrayList.size();
    }

    class SingleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.lvLinear)
        LinearLayout lvLinear;
        @BindView(R.id.tvGenderName)
        TextView tvGenderName;
        @BindView(R.id.ivGender)
        ImageView ivGender;
        private AppTypeface mAppTypeface;

        SingleViewHolder(@NonNull View convertView) {
            super(convertView);
            ButterKnife.bind(this, convertView);
            itemView.setOnClickListener(this);
            mAppTypeface = AppTypeface.getInstance(mContext);
            tvGenderName.setTypeface(mAppTypeface.getHind_medium());
        }

        @Override
        public void onClick(View view) {
            if(listener != null)
            {
                for(int i = 0; i < mGenderArrayList.size();i++)
                {
                        if(i ==  getAdapterPosition())
                            mGenderArrayList.get(i).setChecked(true);
                        else
                             mGenderArrayList.get(i).setChecked(false);
                }
                listener.onItemClick(getAdapterPosition(),mGenderArrayList,mGenderArrayList.get(getAdapterPosition()),tvGenderName);
            }

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position, ArrayList<GenderListPojo> mGenderArrayList, GenderListPojo genderName, TextView tvGenderName);

    }

}
