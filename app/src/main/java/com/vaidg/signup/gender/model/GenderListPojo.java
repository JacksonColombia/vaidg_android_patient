package com.vaidg.signup.gender.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GenderListPojo implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("genderName")
    @Expose
    private String genderName;
    @SerializedName("checked")
    @Expose
    private boolean checked;

    public GenderListPojo(String id, String genderName) {
        this.id = id;
        this.genderName = genderName;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getId() {
        return id;
    }

    public String getGenderName() {
        return genderName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }

    public boolean isChecked() {
        return checked;
    }
}
