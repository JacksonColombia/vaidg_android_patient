package com.vaidg.signup;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.vaidg.countrypic.CountryPicker;
import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;

/**
 * <h2>SignUpPresenter</h2>
 * <h4>This is an interface for SignUpPresenterImpl</h4>
 * this interface hides the implementation of SignUpPresenterImpl and abstracts them to the view.
 *
 * @author Pramod
 * @version 1.0
 * @since 21-12-2017.
 */


public interface SignUpPresenter {


  /**
   * <h2>validateFname</h2>
   * This method is used to validate the FirstName for signup
   *
   * @param firstName First Name of the user
   */
  void validateFname(String firstName);

  /**
   * <h2>validateLname</h2>
   * This method is used to validate the LastName for signup
   *
   * @param lastName Last Name of the user
   */
  void validateLname(String lastName);

  void validateBusiness(String business);

  /**
   * <h2>validateEmail</h2>
   * This method is used to validate the Email Id for signup
   *
   * @param email Email of the user
   */
  void validateEmail(String flag_show_dialog, String email);

  /**
   * <h2>initializeFacebook</h2>
   * This method is used to initialize the facebook
   */
  void initializeFacebook();


  /**
   * <h2>fbLogin</h2>
   * <P>
   * This method is used for Facebook login.
   * </P>
   *
   * @param callbackManager: facebook call back manager interface
   */
  void handleResultFromFB(CallbackManager callbackManager);


  /**
   * <h2>handleResultFromGoogle</h2>
   * <P>
   * This method is used for Google Login.
   * </P>
   *
   * @param googleSignInResult : Google SignIn result object
   */


  void handleResultFromGoogle(GoogleSignInResult googleSignInResult);

  /**
   * <h2>storeLoginType</h2>
   * This method is used to store the login type
   *
   * @param loginType login type 1 for facebook and 2 for google
   */
  void storeLoginType(int loginType);


  /**
   * <h2>handleLoginType</h2>
   * This method is used to handle the login type
   *
   * @param callbackManager facebook call back manager
   */
  void handleLoginType(CallbackManager callbackManager);

  /**
   * <h2>initializeFBGoogle</h2>
   * This method is used to initialize FB and google SDK
   */
  void initializeFBGoogle();


  /**
   * <h2>googleLogin</h2>
   * This method is used to login via google into the System
   */
  void googleLogin();

  void validatePhone(String flag_show_dialog, String countryCode, String phone);

  /**
   * <h2>validatePassword</h2>
   * This method is used to validate the password for signup
   *
   * @param password Email of the user
   */
  void validatePassword(String password);

  /**
   * <h2>openDate_Picker</h2>
   * Method is used to open a date picker and Pick a date.
   * <p>
   * This method open a Date Picker dialog by DatePickerDialog object.
   * Hen set the Max time as Current Date by the help of the {@code fromDatePickerDialog
   * .getDatePicker().setMaxDate(new
   * Date().getTime())}.
   * Also listening the response From the DatePicker and setting the Date to the Given Edit text.
   * </p>
   * @param context
   */
  void openCalendar(
      Context context);

  /**
   * <h2>checkDobValidity</h2>
   * This method is used to check the validation of the date of birth field.
   */
  boolean checkDobValidity();

  boolean irregularPhone(CountryPicker mCountryPicker, Context context, String phone);

  void emailAlreadyExists(String flag_show_dialog, String email);

  void phoneAlreadyExists(String flag_show_dialog, String countryCode, String phone);

  void doRegister(String profilePic, String firstName, String lastName, String email,
                  String password,String countryCodeName, String countryCode, String phone, String referralCode, String gender_val, String dateOfBirth);

  boolean validatePhoneNumber(String countryCode, String mobileNumber);

  void setTermsCondition(TextView tv_termsclick, String privacyTerms);

  void setOnclickHighlighted(TextView tv_termsclick, String privacyPolicy,
      View.OnClickListener onClickListener);

  void checkReferralCode(String trim);

  void storeFbId(String facebookId);

  void storeGoogleId(String googleId);


  /**
   * <h2>checkForKeystore</h2>
   * This method is used to check for keystore
   */
  void checkForKeystore();
  /**
   * <h2>generateEncryptionKeyPostLollipop</h2>
   * This method is used to generate the encryption key required for encryption for post lollipop
   * @param exportedPublicKey
   */
  void generateEncryptionPublicKeyPostLollipop(String exportedPublicKey);

  /**
   * <h2>generateEncryptionKeyPostLollipop</h2>
   * This method is used to generate the encryption key required for encryption for post lollipop
   * @param exportedPrivateKey
   */
  void generateEncryptionPrivateKeyPostLollipop(String exportedPrivateKey);
  /**
   * <h2>generateEncryptionKeyPostLollipop</h2>
   * This method is used to generate the encryption key required for encryption for pre marshmallow
   * @param exportedPublicKey
   */
  void generateEncryptionPublicKeyPreMarshMallow(String exportedPublicKey);
  /**
   * <h2>generateEncryptionKeyPostLollipop</h2>
   * This method is used to generate the encryption key required for encryption for pre marshmallow
   * @param exportedPrivateKey
   */
  void generateEncryptionPrivateKeyPreMarshMallow(String exportedPrivateKey);
  /**
   * <h2>encryptPrivateKeyPostLollipop</h2>
   * This method is used to encrypt the data for post lollipop
   * @param input data to be encrypted
   */
  void encryptPrivateKeyPostLollipop(String input);
  /**
   * <h2>encryptPrivateKeyPostLollipop</h2>
   * This method is used to encrypt the data for pre marshmallow
   * @param input data to be encrypted
   */
  void encryptPrivateKeyPreMarshMallow(String input);
  /**
   * <h2>encryptPublicKeyPostLollipop</h2>
   * This method is used to encrypt the data for post lollipop
   * @param input data to be encrypted
   */
  void encryptPublicKeyPostLollipop(String input);
  /**
   * <h2>encryptPublicKeyPostLollipop</h2>
   * This method is used to encrypt the data for pre marshmallow
   * @param input data to be encrypted
   */
  void encryptPublicKeyPreMarshMallow(String input);


}
