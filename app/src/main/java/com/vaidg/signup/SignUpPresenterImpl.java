package com.vaidg.signup;

import static com.vaidg.utilities.Constants.AES_MODE;
import static com.vaidg.utilities.Constants.ANDROID_KEYSTORE;
import static com.vaidg.utilities.Constants.APPVERSION;
import static com.vaidg.utilities.Constants.APP_VERSION;
import static com.vaidg.utilities.Constants.BAD_REQUEST;
import static com.vaidg.utilities.Constants.BASE_AUTH_PASSWORD;
import static com.vaidg.utilities.Constants.BASE_AUTH_USERNAME;
import static com.vaidg.utilities.Constants.CHECKBUSSINESS;
import static com.vaidg.utilities.Constants.CHECKEMAIL;
import static com.vaidg.utilities.Constants.CHECKFIRSTNAME;
import static com.vaidg.utilities.Constants.CHECKPASSWORD;
import static com.vaidg.utilities.Constants.CONFLICT;
import static com.vaidg.utilities.Constants.DEVICEID;
import static com.vaidg.utilities.Constants.DEVICEMODEL;
import static com.vaidg.utilities.Constants.DEVICEOSVERSION;
import static com.vaidg.utilities.Constants.DEVICETIME;
import static com.vaidg.utilities.Constants.DEVICE_MAKER;
import static com.vaidg.utilities.Constants.DEVICE_MODEL;
import static com.vaidg.utilities.Constants.DEVICE_OS_VERSION;
import static com.vaidg.utilities.Constants.DEVICE_TYPE;
import static com.vaidg.utilities.Constants.DEVMAKE;
import static com.vaidg.utilities.Constants.DEVTYPE;
import static com.vaidg.utilities.Constants.DIALOGMESSAGESHOW_NO;
import static com.vaidg.utilities.Constants.DIALOGMESSAGESHOW_YES;
import static com.vaidg.utilities.Constants.EMAIL;
import static com.vaidg.utilities.Constants.EMAIL_OR_PHONE;
import static com.vaidg.utilities.Constants.FACEBOOKID;
import static com.vaidg.utilities.Constants.FACEBOOK_LOGIN;

import static com.vaidg.utilities.Constants.FIXED_IV;
import static com.vaidg.utilities.Constants.FORBIDDEN;
import static com.vaidg.utilities.Constants.GOOGLEID;
import static com.vaidg.utilities.Constants.GOOGLE_LOGIN;
import static com.vaidg.utilities.Constants.INTERNAL_SERVER_ERROR;
import static com.vaidg.utilities.Constants.IV_PARAMETER_SPEC;
import static com.vaidg.utilities.Constants.KEY_PRIVATEALIAS;
import static com.vaidg.utilities.Constants.KEY_PUBLICALIAS;
import static com.vaidg.utilities.Constants.LATITUDE;
import static com.vaidg.utilities.Constants.LENGTH_REQUIRED;
import static com.vaidg.utilities.Constants.LOGINTYPE;
import static com.vaidg.utilities.Constants.LONGITUDE;
import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.NORMAL_LOGIN;
import static com.vaidg.utilities.Constants.NOT_FOUND;
import static com.vaidg.utilities.Constants.PASSWORD;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.PRECONDITION_FAILED;
import static com.vaidg.utilities.Constants.PUSHKITTOKEN;
import static com.vaidg.utilities.Constants.PUSH_TOKEN;
import static com.vaidg.utilities.Constants.RSA_MODE;
import static com.vaidg.utilities.Constants.SESSION_NoDues;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;
import static com.vaidg.utilities.Constants.UNAUTHORIZATION;
import static com.vaidg.utilities.Constants.latitude;
import static com.vaidg.utilities.Constants.longitude;
import static com.vaidg.utilities.Constants.selLang;
import static com.vaidg.utilities.Utility.getCurrentTime;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import androidx.annotation.NonNull;

import com.vaidg.BuildConfig;
import com.vaidg.Login.FacebookLoginHelper;
import com.vaidg.Login.pojo.Data;
import com.vaidg.Login.pojo.LoginResponse;
import com.vaidg.R;
import com.vaidg.countrypic.CountryPicker;
import com.vaidg.encryptiondecryption.CryptographyExample;
import com.vaidg.model.FacebookLoginPojo;
import com.vaidg.model.ServerResponse;
import com.vaidg.model.SignUpReq;
import com.vaidg.model.SignUpResponse;
import com.vaidg.model.UserDetailsDataModel;
import com.vaidg.model.ValidationReq;
import com.vaidg.networking.BasicAuthentication;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManager;
import com.vaidg.utilities.Utility;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.pojo.CheckFlagForValidity;
import com.pojo.RequestPojo.PhoneValidationRequestPojo;
import com.utility.AlertProgress;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilKeyPair;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;

import eu.janmuller.android.simplecropimage.Util;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;
import javax.security.auth.x500.X500Principal;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * <h2>SignUpPresenterImpl</h2>
 *
 * <h4>This is a Controller class for SignUp Activity</h4>
 * This class is used for performing the business logic for our Activity/View and
 * this class is getting called from SignUpActivity and give a call to SignUpPresenter class.
 *
 * @author Pramod
 * @version 1.0
 * @since 21-12-2017.
 */

public class SignUpPresenterImpl implements SignUpPresenter,
    GoogleApiClient.OnConnectionFailedListener {

  private static final String TAG = SignUpPresenterImpl.class.getName();
  private static String device_id = "";
  @Inject
  LSPServices mLSPServices;
  @Inject
  SignUpActivity mContext;
  @Inject
  SessionManager mSessionManager;
  @Inject
  SignUpView view;
  @Inject
  FacebookLoginHelper mFacebookLoginHelper;
  @Inject
  SignUpModel mSignUpModel;
  @Inject
  Gson mGson;
  @Inject
  AlertProgress mAlertProgress;
  private String profilePic, firstName, lastName, email, password, countryCode,countryCodeName, phone, referralCode, bussinessName, gender, dateOfBirth;
  private CompositeDisposable mCompositeDisposable;
  private CompositeDisposable mCompositeEmailDisposable;
  private CompositeDisposable mCompositePhoneDisposable;
  private CheckFlagForValidity mCheckFlagForValidity;
  private UserDetailsDataModel mUserDetailsDataModel;
  private String socialMediaId;
  private GoogleApiClient mGoogleApiClient;
  private KeyStore keyStore;
  private String exportedPrivateKey = "", exportedPublicKey = "";


  @Inject
  SignUpPresenterImpl() {
    this.mCompositeDisposable = new CompositeDisposable();
    this.mCompositeEmailDisposable = new CompositeDisposable();
    this.mCompositePhoneDisposable = new CompositeDisposable();
    this.mUserDetailsDataModel = new UserDetailsDataModel();
    mCheckFlagForValidity = new CheckFlagForValidity();


  }

  @Override
  public void validateFname(String firstName) {
    if (mSignUpModel.validateFname(firstName)) {
      if(view != null) {
        view.setFirstNameError();
      }
      mCheckFlagForValidity.setnFlg(false);
    } else {
      if(view != null) {
        view.clearError(CHECKFIRSTNAME);
      }
      mCheckFlagForValidity.setnFlg(true);
    }
  }

  @Override
  public void validateLname(String lastname) {
    if (mSignUpModel.validateLname(lastname)) {
      if (view != null) {
        view.setLastNameError();
      }
    }
  }

  @Override
  public boolean irregularPhone(CountryPicker mCountryPicker, Context context, String phone) {
    int max = Utility.getCountryMax(mCountryPicker, context);
    if (mSignUpModel.irregularPhone(phone, max)) {
      if(view != null) {
        view.setMobileInvalid();
      }
      return true;
    } else {
      return false;
    }
  }

  @Override
  public void validatePhone(String flag_show_dialog, String countryCode, String phone) {

    if (TextUtils.isEmpty(phone)) {
      if(view != null) {
        view.setMobileError();
      }
      mCheckFlagForValidity.setmFlg(false);
    } else if (validatePhoneNumber(countryCode, phone)) {
      if(view != null) {
        view.setMobileErrorMsg("");
      }
      phoneAlreadyExists(flag_show_dialog, countryCode, phone);
    } else {
      mCheckFlagForValidity.setmFlg(false);
      if (view != null) {
        view.setMobileInvalid();
      }
    }

  }

  @Override
  public void validatePassword(String password) {
    if (TextUtils.isEmpty(password)) {
      if(view != null) {
        view.setPasswordError();
      }
      mCheckFlagForValidity.setpFlg(false);
    } else if (mSignUpModel.validPassword(password)) {
      if(view != null) {
        view.setPasswordInvalid();
      }
      mCheckFlagForValidity.setpFlg(false);
    } else {
      if(view != null) {
        view.clearError(CHECKPASSWORD);
      }
      mCheckFlagForValidity.setpFlg(true);
    }
  }

  @Override
  public void handleLoginType(CallbackManager callbackManager) {
    switch (mSignUpModel.getLoginType()) {
      //2 - Facebook , 3 - Google
      case FACEBOOK_LOGIN:
        handleResultFromFB(callbackManager);
        break;
      case GOOGLE_LOGIN:
        googleLogin();
        break;
    }
  }

  @Override
  public void storeLoginType(int loginType) {
    mSignUpModel.setLoginType(loginType);
    mUserDetailsDataModel.setLoginType(loginType);
  }

  @Override
  public void storeFbId(String facebookId) {
    mUserDetailsDataModel.setFacebookId(facebookId);
  }

  @Override
  public void storeGoogleId(String googleId) {
    mUserDetailsDataModel.setGoogleId(googleId);
  }

  @Override
  public void initializeFacebook() {
    mFacebookLoginHelper.initializeFacebookSdk(mContext);
  }

  @Override
  public void googleLogin() {
    if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
      Auth.GoogleSignInApi.signOut(mGoogleApiClient);
      if (view != null) {
        view.openGoogleActivity(Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient));
      }
    }else {
      initializeFBGoogle();
      new android.os.Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
          mGoogleApiClient.connect();
          Auth.GoogleSignInApi.signOut(mGoogleApiClient);
          if (view != null) {
            view.openGoogleActivity(Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient));
          }
        }
      }, 1000);
    }

  }

  @Override
  public void handleResultFromGoogle(GoogleSignInResult result) {
    if (result.isSuccess()) {
      GoogleSignInAccount account = result.getSignInAccount();
      firstName = account != null ? account.getGivenName() : null;
      lastName = account != null ? account.getFamilyName() : null;
      email = account != null ? account.getEmail() : null;
      mUserDetailsDataModel.setLoginType(GOOGLE_LOGIN);
      mUserDetailsDataModel.setEmailOrPhone(account != null ? account.getEmail() : null);
      socialMediaId = account != null ? account.getId() : null;
      profilePic = account != null ? String.valueOf(account.getPhotoUrl()) : null;
      if (view != null) {
        view.setFieldsFromFB(firstName, lastName, email, "", profilePic);
      }
    }
  }

  @Override
  public void handleResultFromFB(CallbackManager callbackManager) {
    mFacebookLoginHelper.refreshToken();
    mFacebookLoginHelper.facebookLogin(callbackManager,
        mFacebookLoginHelper.createFacebook_requestData(),
        new FacebookLoginHelper.Facebook_callback() {
          @Override
          public void success(JSONObject json) {
            Gson mGson = new Gson();
            FacebookLoginPojo facebookLogin_pojo = mGson.fromJson(json.toString(),
                FacebookLoginPojo.class);
            mUserDetailsDataModel.setLoginType(FACEBOOK_LOGIN);
            mUserDetailsDataModel.setEmailOrPhone(facebookLogin_pojo.getEmail());
            firstName = facebookLogin_pojo.getFirst_name();
            lastName = facebookLogin_pojo.getLast_name();
            email = facebookLogin_pojo.getEmail();
            profilePic = BuildConfig.FACEBOOK_PROFILE_URL + facebookLogin_pojo.getId() + BuildConfig.FACEBOOK_PROFILE_TYPE;
            socialMediaId = facebookLogin_pojo.getId();
            // mSessionManager.setProfilePicUrl(profilePic);
            if (view != null) {
              view.setFieldsFromFB(firstName, lastName, email, "", profilePic);
            }
          }

          @Override
          public void error(String error) {
          }

          @Override
          public void cancel(String cancel) {
          }
        });

  }

  @Override
  public void initializeFBGoogle() {
    FacebookSdk.sdkInitialize(mContext.getApplicationContext());
    GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder
        (GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestEmail()
        //.requestIdToken(REQUEST_ID_TOKEN)
        .build();
    mGoogleApiClient = new GoogleApiClient.Builder(mContext)
        .enableAutoManage(mContext, this)
        .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
        .build();
  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
  }

  @Override
  public void validateEmail(String flag_to_dialog, String email) {
    if (TextUtils.isEmpty(email)) {
      if(view != null) {
        view.setEmailError();
      }
      mCheckFlagForValidity.seteFlg(false);
    } else if (mSignUpModel.validEmail(email)) {
      if(view != null) {
        view.setEmailInvalid("");
      }
      mCheckFlagForValidity.seteFlg(false);
    } else {
      emailAlreadyExists(flag_to_dialog, email);
    }
  }

  @Override
  public void emailAlreadyExists(String dia, String email) {
    if (view != null) {
      if (DIALOGMESSAGESHOW_YES.equals(dia)) {
        view.showProgress(EMAIL);
      }
    }
    ValidationReq req = new ValidationReq(email);
    mLSPServices = BasicAuthentication.ServiceGenerator.createService(LSPServices.class,
        BASE_AUTH_USERNAME, BASE_AUTH_PASSWORD);
    final Observable<Response<ServerResponse>> response = mLSPServices.checkEmailExists(
        selLang, PLATFORM_ANDROID, req);
    response.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ServerResponse>>() {
          @Override
          public void onSubscribe(Disposable d) {
            mCompositeEmailDisposable.add(d);
          }

          @Override
          public void onNext(Response<ServerResponse> value) {
            switch (value.code()) {
              case SUCCESS_RESPONSE:
                // Passing empty string to make the setError to null as this means the Email is
                // valid i.e.,. it is not present in our system.
                if(view != null) {
                  view.clearError(CHECKEMAIL);
                  view.hideProgress();
                }
                mCheckFlagForValidity.seteFlg(true);
                if (DIALOGMESSAGESHOW_NO.equals(dia)) {
                  if (!mCheckFlagForValidity.ispFlg()) {
                    checkPasFlag();
                  } else {
                    checkMobFlag(DIALOGMESSAGESHOW_NO);
                  }
                }
                break;
              case PRECONDITION_FAILED:
              case INTERNAL_SERVER_ERROR:
              default:
                if(view != null) {
                  view.hideProgress();
                }
                try {
                  JSONObject errJson = new JSONObject(
                      value.errorBody() != null ? value.errorBody().string() : null);
                  if(view != null) {
                    view.setEmailInvalid(errJson.getString(MESSAGE));
                  }
                  mCheckFlagForValidity.seteFlg(false);
                  //       isEmailValid[0] = false;
                } catch (Exception e) {
                  e.printStackTrace();
                  if(view != null) {
                    view.hideProgress();
                  }
                }
                break;
            }
          }

          @Override
          public void onError(Throwable e) {
            // show No Connectivity message to user or do whatever you want.
            if(view != null) {
              view.hideProgress();
            }
            mAlertProgress.tryAgain(mContext, mContext.getString(
                R.string.pleaseCheckInternet)/*message + ", " + getString(R.string
                  .pleaseCheckInternet)*/,
                mContext.getString(R.string.system_error),
                isClicked -> {
                  if (isClicked) {
                    emailAlreadyExists(dia, email);
                  }

                });
          }

          @Override
          public void onComplete() {
            if(view != null) {
              view.hideProgress();
            }
          }
        });
    //return isEmailValid[0];
  }

  @Override
  public void phoneAlreadyExists(String dia, String countryCode, String phone) {
    if (view != null) {
      if (DIALOGMESSAGESHOW_YES.equals(dia)) {
        view.showProgress("PHONE");
      }
    }
    PhoneValidationRequestPojo jsonObject = new PhoneValidationRequestPojo(countryCode, phone);
    mLSPServices = BasicAuthentication.ServiceGenerator.createService(LSPServices.class,
        BASE_AUTH_USERNAME, BASE_AUTH_PASSWORD);

    final Observable<Response<ServerResponse>> response = mLSPServices.checkPhoneExists(
        selLang, PLATFORM_ANDROID, jsonObject);
    response.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ServerResponse>>() {
          @Override
          public void onSubscribe(Disposable d) {
            mCompositePhoneDisposable.add(d);
          }

          @Override
          public void onNext(Response<ServerResponse> value) {
            switch (value.code()) {
              case SUCCESS_RESPONSE:
                // Passing empty string to make the setError to null as this means the mobile
                // number is valid as its not present in our system.
                if(view != null) {
                  view.setMobileErrorMsg("");
                }
                mCheckFlagForValidity.setmFlg(true);
                if (DIALOGMESSAGESHOW_NO.equals(dia)) {
                  checkPasBusinessFlag();
                } else {
                  if (view != null) {
                    view.hideProgress();
                  }
                }
                break;

              case CONFLICT:
              case PRECONDITION_FAILED:
              case INTERNAL_SERVER_ERROR:
              default:
                mCheckFlagForValidity.setmFlg(false);
                if(view != null) {
                  view.hideProgress();
                }
                try {
                  JSONObject errJson = new JSONObject(
                      value.errorBody() != null ? value.errorBody().string() : null);
                  if(view != null) {
                    view.setMobileErrorMsg(errJson.getString(MESSAGE));
                }
                }
                catch (Exception e) {
                  e.printStackTrace();
                }
                break;
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.hideProgress();
            }

            // show No Connectivity message to user or do whatever you want.
            mAlertProgress.tryAgain(mContext, mContext.getString(
                R.string.pleaseCheckInternet)/*message + ", " + getString(R.string
                  .pleaseCheckInternet)*/,
                mContext.getString(R.string.system_error),
                isClicked -> {
                  if (isClicked) {
                    phoneAlreadyExists(dia, countryCode, phone);
                  }

                });

          }


          @Override
          public void onComplete() {
            if(view != null) {
              view.hideProgress();
            }

          }
        });
  }

  @Override
  public void doRegister(String profilePic, String firstname, String lastName, final String email,
                         String password,
                         String countryCodeName,String countryCode, String phone, String referralCode, String gender_val, String dateOfBirth) {
    if (view != null) {
      view.showPro();
    }
    if (mUserDetailsDataModel.getLoginType() == null
            || mUserDetailsDataModel.getLoginType() == NORMAL_LOGIN) {
      mUserDetailsDataModel.setLoginType(NORMAL_LOGIN);
    }

    switch (mUserDetailsDataModel.getLoginType()) {
      case FACEBOOK_LOGIN:
        mUserDetailsDataModel.setFacebookId(socialMediaId);

        break;
      case GOOGLE_LOGIN:
        mUserDetailsDataModel.setGoogleId(socialMediaId);
        break;

    }
    if (gender_val.equals(mContext.getResources().getString(R.string.male))) {
      this.gender = "1";
    } else if (gender_val.equals(mContext.getResources().getString(R.string.female))){
      this.gender = "2";
    } else {
      this.gender = "3";
    }
    this.profilePic = profilePic;
    this.firstName = firstname;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
    this.countryCodeName = countryCodeName;
    this.countryCode = countryCode;
    this.phone = phone;
    this.referralCode = referralCode;
  //  this.gender = gender_val;
    this.dateOfBirth = dateOfBirth;
    try {
      device_id = Utility.getDeviceId(this.mContext);
    } catch (Exception e) {
      e.printStackTrace();
    }

    if (!mCheckFlagForValidity.isnFlg()) {
      if (mSignUpModel.validateFname(firstname)) {
        mCheckFlagForValidity.setnFlg(false);
        if(view != null) {
          view.setFirstNameError();
          view.hideProgress();
        }
      } else {
        mCheckFlagForValidity.setnFlg(true);
        if(view != null) {
          view.clearError(CHECKFIRSTNAME);
        }
        checkEmailFlag(email);
      }

    } else if (!mCheckFlagForValidity.iseFlg()) {
      checkEmailFlag(email);

    } else if (!mCheckFlagForValidity.ispFlg()) {
      checkPasFlag();

    } else if (!mCheckFlagForValidity.ismFlg()) {
      checkMobFlag(DIALOGMESSAGESHOW_NO);
    }
       /* else if (!mCheckFlagForValidity.isbNFlg())
        {
            checkPasBusinessFlag();
        }*/
    else {
      if(Utility.isNetworkAvailable(mContext))
      signUp();
    }

  }

  private void checkEmailFlag(String email) {

    if (!mCheckFlagForValidity.iseFlg()) {
      if (validateEmailFlag(email)) {
        emailAlreadyExists(DIALOGMESSAGESHOW_NO, email);
      }
    } else {
      checkPasFlag();
    }
  }

  private boolean validateEmailFlag(String email) {
    if (TextUtils.isEmpty(email)) {
      if(view != null) {
        view.setEmailError();
      }
      mCheckFlagForValidity.seteFlg(false);
      if(view != null) {
        view.hideProgress();
      }
      return false;
    } else if (mSignUpModel.validEmail(email)) {
      if(view != null) {
        view.setEmailInvalid("");
        view.hideProgress();
      }
      mCheckFlagForValidity.seteFlg(false);
      return false;
    } else {
      mCheckFlagForValidity.seteFlg(true);
      return true;
    }
  }

  private void checkPasFlag() {

    if (!mCheckFlagForValidity.ispFlg()) {

      if (checkPasswordVal(password)) {
        checkMobFlag(DIALOGMESSAGESHOW_NO);
      } /*else
                view.onValidationError(3);*/
    } else {
      checkMobFlag(DIALOGMESSAGESHOW_NO);
    }
  }

  private boolean checkPasswordVal(String password) {
    if (TextUtils.isEmpty(password)) {
      mCheckFlagForValidity.setpFlg(false);
      if(view != null) {
        view.setPasswordError();
        view.hideProgress();
      }
      return false;
    } else if (mSignUpModel.validPassword(password)) {
      mCheckFlagForValidity.setpFlg(false);
      if(view != null) {
        view.setPasswordInvalid();
        view.hideProgress();
      }
      return false;
    } else {
      mCheckFlagForValidity.setpFlg(true);
      if(view != null) {
        view.clearError(CHECKPASSWORD);
      }
      return true;
    }


  }


  private void checkMobFlag(String isDialog) {

    if (!mCheckFlagForValidity.ismFlg()) {
      checkMobileValidation(isDialog);
    } else {
      checkPasBusinessFlag();
    }
  }

  private void checkMobileValidation(String dialog) {
    if (TextUtils.isEmpty(phone)) {
      mCheckFlagForValidity.setmFlg(false);
      if(view != null) {
        view.setMobileError();
        view.hideProgress();
      }

    } else if (validatePhoneNumber(countryCode, phone)) {
      if(view != null) {
        view.setMobileErrorMsg("");
      }
      phoneAlreadyExists(dialog, countryCode, phone);
    } else {
      mCheckFlagForValidity.setmFlg(false);
      if(view != null) {
        view.setMobileInvalid();
        view.hideProgress();
      }
      //  return true;

    }
  }

  private void checkPasBusinessFlag() {
    //for now pu business flag always true
    mCheckFlagForValidity.setbNFlg(true);
    if (!mCheckFlagForValidity.isbNFlg()) {
      if (checkBusinessVal()) {
        if(view != null) {
          view.onBusinessNameIsEmpty();
          view.hideProgress();
        }
      } else {
        if(view != null) {
          view.clearError(CHECKBUSSINESS);
        }
        mCheckFlagForValidity.setbNFlg(true);
        if(Utility.isNetworkAvailable(mContext))
          signUp();
      }
    } else {
      if(Utility.isNetworkAvailable(mContext))
        signUp();
    }
  }

  private boolean checkBusinessVal() {
    return mSignUpModel.validateFname(bussinessName);
  }

  @Override
  public void validateBusiness(String business) {
    if (mSignUpModel.validateFname(business)) {
      if(view != null) {
        view.onBusinessNameIsEmpty();
      }
      mCheckFlagForValidity.setbNFlg(false);
    } else {
      if(view != null) {
        view.clearError(CHECKBUSSINESS);
      }
      mCheckFlagForValidity.setbNFlg(true);
    }
  }

  private void signUp() {
    String deviceTime = getCurrentTime();
    deviceTime = deviceTime.replace('T', ' ');
    SignUpReq req = new SignUpReq(firstName, lastName, email, password,countryCodeName, countryCode, phone,
        profilePic,
        mUserDetailsDataModel.getLoginType(), mUserDetailsDataModel.getFacebookId(),
        mUserDetailsDataModel.getGoogleId(), Constants.latitude, Constants.longitude,
        "", 1, Constants.DEVICE_TYPE, device_id, "",
        Constants.APP_VERSION, Constants.DEVICE_OS_VERSION, Constants.DEVICE_MAKER,
        Constants.DEVICE_MODEL, deviceTime, referralCode, mSessionManager.getIpAddress(), mSessionManager.getVirgilPrivateKeyT(), mSessionManager.getVirgilPublicKeyT(), gender, dateOfBirth);
    mLSPServices = BasicAuthentication.ServiceGenerator.createService(LSPServices.class,
        BASE_AUTH_USERNAME, BASE_AUTH_PASSWORD);

    final Observable<Response<SignUpResponse>> response = mLSPServices.doRegister(selLang,
        Constants.PLATFORM_ANDROID, req);
    response.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<SignUpResponse>>() {
          @Override
          public void onSubscribe(Disposable d) {
            mCompositeDisposable.add(d);
          }

          @Override
          public void onNext(Response<SignUpResponse> value) {
            switch (value.code()) {
              case SUCCESS_RESPONSE:
                SignUpResponse signUpResponse = value.body();
                String sid = signUpResponse != null ? signUpResponse.getData().getSid() : null;
                long expireOtp =
                    signUpResponse != null ? signUpResponse.getData().getExpireOtp() : 0;
                mSessionManager.setSID(sid);
                mSessionManager.setExpireOtp(expireOtp);
                mSessionManager.setGuestLogin(false);
                if(view != null) {
                  view.hideProgress();
                  view.navigateToOtp();
                }
                break;
             /* case 412:
                if (mUserDetailsDataModel.getLoginType() == FACEBOOK_LOGIN
                    || mUserDetailsDataModel.getLoginType() == GOOGLE_LOGIN) {
                  loginApi(email, "");
                }
                break;*/
              case INTERNAL_SERVER_ERROR:
              default:

                try {
                  JSONObject errJson = new JSONObject(
                      value.errorBody() != null ? value.errorBody().string() : null);
                  if(view != null) {
                    view.onError(errJson.getString(MESSAGE));
                  }
                } catch (Exception e) {
                  e.printStackTrace();
                }
                if(view != null) {
                  view.hideProgress();
                }
                break;
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.hideProgress();
            }
            // show No Connectivity message to user or do whatever you want.
            mAlertProgress.tryAgain(mContext, mContext.getString(
                R.string.pleaseCheckInternet)/*message + ", " + getString(R.string
                  .pleaseCheckInternet)*/,
                mContext.getString(R.string.system_error),
                isClicked -> {
                  if (isClicked) {
                    if(Utility.isNetworkAvailable(mContext))
                      signUp();
                  }

                });
          }

          @Override
          public void onComplete() {

          }
        });
  }

  private void loginApi(String email, String password) {
    String deviceTime = getCurrentTime();
    deviceTime = deviceTime.replace('T', ' ');
    mLSPServices = BasicAuthentication.ServiceGenerator.createService(LSPServices.class,
        BASE_AUTH_USERNAME, BASE_AUTH_PASSWORD);

    Map<String, Object> jsonParams = new HashMap<>();
    //put something inside the map, could be null*/
    jsonParams.put(EMAIL_OR_PHONE, email);
    jsonParams.put(PASSWORD, password);
    jsonParams.put(DEVICEID, device_id);
    jsonParams.put(PUSH_TOKEN, "");
    jsonParams.put(APPVERSION, APP_VERSION);
    jsonParams.put(DEVMAKE, DEVICE_MAKER);
    jsonParams.put(DEVICEMODEL, DEVICE_MODEL);
    jsonParams.put(DEVTYPE, DEVICE_TYPE);
    jsonParams.put(DEVICETIME, deviceTime);
    jsonParams.put(LOGINTYPE, mUserDetailsDataModel.getLoginType());
    jsonParams.put(DEVICEOSVERSION, DEVICE_OS_VERSION);
    jsonParams.put(FACEBOOKID, mUserDetailsDataModel.getFacebookId());
    jsonParams.put(GOOGLEID, mUserDetailsDataModel.getGoogleId());
    jsonParams.put(LATITUDE, latitude);
    jsonParams.put(LONGITUDE, longitude);
    jsonParams.put(PUSHKITTOKEN, "");

    mLSPServices.performLogin(selLang, PLATFORM_ANDROID, jsonParams)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> value) {
            int code = value.code();
            JSONObject errJson;
            try {
              String response = value.body() != null ? value.body().string() : null;

              switch (code) {
                case SUCCESS_RESPONSE:
                  if (response != null && !response.isEmpty()) {
                    LoginResponse loginResponse = mGson.fromJson(response, LoginResponse.class);
                    Data loginData = loginResponse.getData();
                    String auth = loginData.getToken().getAccessToken();
                    String refreshtoken = loginData.getToken().getRefreshToken();

                    String sid = loginData.getSid();
                    String refCode = loginData.getReferralCode();

                    mSessionManager.setSID(sid);
                    mSessionManager.setREFRESHAUTH(refreshtoken);
                    mSessionManager.setReferralCode(refCode);
                    mSessionManager.setRegisterId(loginData.getRequesterId());
                    mSessionManager.setFirstName(loginData.getFirstName());
                    mSessionManager.setPatientGender(loginData.getGender());
                    mSessionManager.setPatientDateOfBirth(loginData.getDateOfBirth());
                    mSessionManager.setLastName(loginData.getLastName());
                    mSessionManager.setEmail(loginData.getEmail());
                    LSPApplication.getInstance().setAuthToken(sid, sid, auth);
                    mSessionManager.setMobileNo(loginData.getPhone());
                    mSessionManager.setCountryCode(loginData.getCountryCode());
                    mSessionManager.setCountrySymbol(loginData.getCountrySymbol());
                    mSessionManager.setProfilePicUrl(loginData.getProfilePic());
                    mSessionManager.setFcmTopic(loginData.getFcmTopic());
                    mSessionManager.setDefaultCardId(loginData.getCardDetail().getId());
                    mSessionManager.setDefaultCardNum(loginData.getCardDetail().getLast4());
                    mSessionManager.setDefaultCardName(loginData.getCardDetail().getBrand());
                    if (!TextUtils.isEmpty(mSessionManager.getFcmTopic())) {
                      FirebaseMessaging.getInstance().subscribeToTopic(
                          mSessionManager.getFcmTopic());
                    }
                    if(view != null) {
                      view.hideProgress();
                      view.onLoginSuccess(auth, email, password);
                    }
                  }
                  break;
                case NOT_FOUND:
                  if (mUserDetailsDataModel.getLoginType() == FACEBOOK_LOGIN
                      || mUserDetailsDataModel.getLoginType() == GOOGLE_LOGIN) {
                    //   view.navToSignUp(mUserDetailsDataModel.getLoginType(),
                    //   mUserDetailsDataModel,mGoogleApiClient);
                  } else {
                    if (response != null && !response.isEmpty()) {
                      errJson = new JSONObject(
                          value.errorBody() != null ? value.errorBody().string() : null);
                      if(view != null) {
                        view.onError(errJson.getString(MESSAGE));
                      }
                    }
                  }
                  if(view != null) {
                    view.hideProgress();
                  }
                  break;
                case SESSION_NoDues:
                case BAD_REQUEST:
                case UNAUTHORIZATION:
                case FORBIDDEN:
                case CONFLICT:
                case INTERNAL_SERVER_ERROR:
                case LENGTH_REQUIRED:
                default:
                  if (response != null && !response.isEmpty()) {
                    errJson = new JSONObject(
                        value.errorBody() != null ? value.errorBody().string() : null);
                    if(view != null) {
                      view.onError(errJson.getString(MESSAGE));
                    }
                  }
                   if(view != null) {
                     view.hideProgress();
                   }
                  break;
              }
            } catch (IOException | JSONException e) {
              e.printStackTrace();
              if(view != null) {
                view.hideProgress();
              }
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.hideProgress();
            }
            // show No Connectivity message to user or do whatever you want.
            mAlertProgress.tryAgain(mContext, mContext.getString(
                R.string.pleaseCheckInternet)/*message + ", " + getString(R.string
                  .pleaseCheckInternet)*/,
                mContext.getString(R.string.system_error),
                isClicked -> {
                  if (isClicked) {
                    loginApi(email, password);
                  }

                });
          }

          @Override
          public void onComplete() {
          }
        });
  }

  @Override
  public boolean validatePhoneNumber(String countryCode, String mobileNumber) {
    String phoneNumberE164Format = countryCode.concat(mobileNumber);
    PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    try {
      Phonenumber.PhoneNumber phoneNumberProto = phoneUtil.parse(phoneNumberE164Format, null);
      boolean isValid = phoneUtil.isValidNumber(phoneNumberProto); // returns true if valid

      return isValid && (phoneUtil.getNumberType(phoneNumberProto)
          == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType(phoneNumberProto)
          == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE);

    } catch (NumberParseException e) {
      e.printStackTrace();
      return false;
    }
  }

  @Override
  public void setTermsCondition(TextView tv, String privacyTerms) {
    String tvt = tv.getText().toString();
    int ofe = tvt.indexOf(privacyTerms);
    Spannable wordToSpan = new SpannableString(tv.getText());
    for (int ofs = 0; ofs < tvt.length() && ofe != -1; ofs = ofe + 1) {
      ofe = tvt.indexOf(privacyTerms, ofs);
      if (ofe == -1) {
        break;
      } else {
        wordToSpan.setSpan(new BackgroundColorSpan(0xFFFFFFFF), ofe,
            ofe + privacyTerms.length(),
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv.setText(wordToSpan, TextView.BufferType.SPANNABLE);
      }
    }
  }

  @Override
  public void setOnclickHighlighted(TextView tv, String textToHighlight,
                                    View.OnClickListener onClickListener) {
    String tvt = tv.getText().toString();
    int ofe = tvt.indexOf(textToHighlight);
    ClickableSpan clickableSpan = new ClickableSpan() {
      @Override
      public void onClick(View textView) {
        if (onClickListener != null) onClickListener.onClick(textView);
      }

      @Override
      public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        ds.setColor(0xff0000ff);
        ds.setUnderlineText(true);
      }
    };
    SpannableString wordToSpan = new SpannableString(tv.getText());
    for (int ofs = 0; ofs < tvt.length() && ofe != -1; ofs = ofe + 1) {
      ofe = tvt.indexOf(textToHighlight, ofs);
      if (ofe == -1) {
        break;
      } else {
        wordToSpan.setSpan(clickableSpan, ofe, ofe + textToHighlight.length(),
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv.setText(wordToSpan, TextView.BufferType.SPANNABLE);
        tv.setMovementMethod(LinkMovementMethod.getInstance());
      }
    }
  }

  @Override
  public void checkReferralCode(String txtreferralCode) {
    mLSPServices = BasicAuthentication.ServiceGenerator.createService(LSPServices.class,
        BASE_AUTH_USERNAME, BASE_AUTH_PASSWORD);
    Map<String, Object> jsonParams = new HashMap<>();
    jsonParams.put("referralCode", txtreferralCode);

    Observable<Response<ResponseBody>> responseObservable = mLSPServices.onReferralCodeCheck(
        selLang, PLATFORM_ANDROID, jsonParams);

    responseObservable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {

            int code = responseBodyResponse.code();

            switch (code) {
              case SUCCESS_RESPONSE:
                try {
                  String response =
                      responseBodyResponse.body() != null ? responseBodyResponse.body().string()
                          : null;
                  if(view != null) {
                    view.onReferral(txtreferralCode);
                  }
                } catch (IOException e) {
                  e.printStackTrace();
                }
                break;
              case INTERNAL_SERVER_ERROR:
              default:
                mCheckFlagForValidity.setmFlg(false);
                if(view != null) {
                  view.hideProgress();
                }
                try {
                  JSONObject errJson = new JSONObject(responseBodyResponse.errorBody() != null
                      ? responseBodyResponse.errorBody().string() : null);
                  if(view != null) {
                    view.setMobileErrorMsg(errJson.getString(MESSAGE));
                  }
                } catch (Exception e) {
                  e.printStackTrace();
                }
                break;
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.hideProgress();
            }
            // show No Connectivity message to user or do whatever you want.
            mAlertProgress.tryAgain(mContext, mContext.getString(
                R.string.pleaseCheckInternet)/*message + ", " + getString(R.string
                  .pleaseCheckInternet)*/,
                mContext.getString(R.string.system_error),
                isClicked -> {
                  if (isClicked) {
                    checkReferralCode(txtreferralCode);
                  }

                });
          }

          @Override
          public void onComplete() {

          }
        });
  }

  public void call() {
    int n = 7;
    int a = 1;
    int b = n - 1;

    char[][] ch = new char[n][n];

    for (int i = 0; i < 1; i++) {
      for (int j = 0; j < n; j++) {
        ch[i][j] = '*';
        System.out.print(ch[i][j]);
      }
    }
    for (int i = 0; i < n - 1; i++) {
      System.out.print("*");
      if (a < n && a != b) {
        ch[i][a] = '*';
        System.out.print(ch[i][a]);
        a++;
      }
      if (b > 1 && a != b) {
        ch[i][a] = '*';
        System.out.print(ch[i][b]);
        b--;
      }

      if (a == b) {
        ch[i][a] = '3';
        System.out.print(ch[a][b]);

      }
    }
    for (int i = n; i < n + 1; i++) {
      for (int j = 0; j < n; j++) {
        ch[i][a] = '*';
        System.out.print(ch[i][j]);
      }
    }
  }


  @Override
  public void openCalendar(
      Context mContext) {
    final Calendar newCalendar = Calendar.getInstance();
    DatePickerDialog fromDatePickerDialog = new DatePickerDialog(mContext,
        (datePicker, year, monthOfYear, dayOfMonth) -> {
          newCalendar.set(year, monthOfYear, dayOfMonth);
          SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy",
              Locale.US);
          Constants.dateOB = dateFormatter.format(newCalendar.getTime());
          String title = DateUtils.formatDateTime(mContext,
              newCalendar.getTimeInMillis(),
              DateUtils.FORMAT_SHOW_DATE
                  | DateUtils.FORMAT_SHOW_YEAR
                  | DateUtils.FORMAT_ABBREV_MONTH);
          if(view != null) {
            view.setDateOfBirth(title);
          }
          checkDobValidity();
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH),
        newCalendar.get(Calendar.DAY_OF_MONTH));

    newCalendar.add(Calendar.YEAR, -18);
    fromDatePickerDialog.getDatePicker().setMaxDate(newCalendar.getTimeInMillis());
    fromDatePickerDialog.show();
  }

  @Override
  public boolean checkDobValidity() {
    return true;
  }

  @Override
  public void checkForKeystore() {
    try {
      keyStore = KeyStore.getInstance(ANDROID_KEYSTORE);
      keyStore.load(null);
      if(view != null) {
        view.checkForVersion();
      }
    } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
      e.printStackTrace();
    }
    // if (!keyStore.containsAlias(KEY_PRIVATEALIAS)) {

    //}
  }

  private void createVirgilPublicPrivateKey() {
    try {
      CryptographyExample cryptographyExample = new CryptographyExample();
      // Generate keys
      VirgilCrypto crypto = new VirgilCrypto();
      VirgilKeyPair keyPair = cryptographyExample.keyGeneration();
      // Export private key
      exportedPrivateKey = cryptographyExample.exportPrivateKey(keyPair, crypto);
      // Export public key
      exportedPublicKey = cryptographyExample.exportPublicKey(keyPair, crypto);

    } catch (CryptoException e) {
      e.printStackTrace();
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      encryptPrivateKeyPostLollipop(exportedPrivateKey);
      encryptPublicKeyPostLollipop(exportedPublicKey);
    } else {
      encryptPrivateKeyPreMarshMallow(exportedPrivateKey);
      encryptPublicKeyPreMarshMallow(exportedPublicKey);
    }
  }

  /**
   * <hg2>generateAndStorePublicKeyAES</hg2>
   * This method is useed to generate the key for pre marsh mallow and store
   */
  private void generateAndStorePublicKeyAES() {
    String encryptedKeyB64 = mSessionManager.getEncryptionPublicKey();
    if (encryptedKeyB64 == null) {
      byte[] key = new byte[16];
      SecureRandom secureRandom = new SecureRandom();
      secureRandom.nextBytes(key);
      byte[] encryptedKey;
      try {
        encryptedKey = rsaPublicKeyEncrypt(key);
        encryptedKeyB64 = Base64.encodeToString(encryptedKey, Base64.DEFAULT);
        mSessionManager.storeEncryptionPublicKey(encryptedKeyB64);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * <hg2>generateAndStorePrivateKeyAES</hg2>
   * This method is useed to generate the key for pre marsh mallow and store
   */
  private void generateAndStorePrivateKeyAES() {
    String encryptedKeyB64 = mSessionManager.getEncryptionPrivateKey();
    if (encryptedKeyB64 == null) {
      byte[] key = new byte[16];
      SecureRandom secureRandom = new SecureRandom();
      secureRandom.nextBytes(key);
      byte[] encryptedKey;
      try {
        encryptedKey = rsaPrivateKeyEncrypt(key);
        encryptedKeyB64 = Base64.encodeToString(encryptedKey, Base64.DEFAULT);
        mSessionManager.storeEncryptionPrivateKey(encryptedKeyB64);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * <h2>rsaPublicKeyEncrypt</h2>
   * This method is used to do the rsa encrypt
   *
   * @param secret takes secret bytes as input
   * @return returns the RSA encrypt byts
   * @throws Exception throws an exception
   */
  private byte[] rsaPublicKeyEncrypt(byte[] secret) throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PUBLICALIAS, null);
    // Encrypt the text
    Cipher inputCipher = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
    inputCipher.init(Cipher.ENCRYPT_MODE, privateKeyEntry.getCertificate().getPublicKey());
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, inputCipher);
    cipherOutputStream.write(secret);
    cipherOutputStream.close();
    return outputStream.toByteArray();
  }

  /**
   * <h2>rsaPrivateKeyEncrypt</h2>
   * This method is used to do the rsa encrypt
   *
   * @param secret takes secret bytes as input
   * @return returns the RSA encrypt byts
   * @throws Exception throws an exception
   */
  private byte[] rsaPrivateKeyEncrypt(byte[] secret) throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PRIVATEALIAS, null);
    // Encrypt the text
    Cipher inputCipher = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
    inputCipher.init(Cipher.ENCRYPT_MODE, privateKeyEntry.getCertificate().getPublicKey());
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, inputCipher);
    cipherOutputStream.write(secret);
    cipherOutputStream.close();
    return outputStream.toByteArray();
  }


  /**
   * <h2>getSecretPrivateKeyPostLollipop</h2>
   * This method is used to get the secret key for post lollipop device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPrivateKeyPostLollipop() throws Exception {
    Key s = keyStore.getKey(KEY_PRIVATEALIAS, null);
    Log.i("TEST", "key " + s.toString());
    return keyStore.getKey(KEY_PRIVATEALIAS, null);
  }

  /**
   * <h2>getSecretPublicKeyPostLollipop</h2>
   * This method is used to get the secret key for post lollipop device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPublicKeyPostLollipop() throws Exception {
    Key s = keyStore.getKey(KEY_PUBLICALIAS, null);
    Log.i("TEST", "key " + s.toString());
    return keyStore.getKey(KEY_PUBLICALIAS, null);
  }

  /**
   * <h2>getSecretPrivateKeyPreMarshMallow</h2>
   * This method is used to get the secret key for pre marsh mallow device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPrivateKeyPreMarshMallow() throws Exception {
    String enryptedKeyB64 = mSessionManager.getEncryptionPrivateKey();
    Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
    // need to check null, omitted here
    byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
    byte[] key = rsaPrivateKeyDecrypt(encryptedKey);
    Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
    return new SecretKeySpec(key, "AES");
  }

  /**
   * <h2>getSecretPublicKeyPreMarshMallow</h2>
   * This method is used to get the secret key for pre marsh mallow device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPublicKeyPreMarshMallow() throws Exception {
    String enryptedKeyB64 = mSessionManager.getEncryptionPublicKey();
    Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
    // need to check null, omitted here
    byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
    byte[] key = rsaPublicKeyDecrypt(encryptedKey);
    Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
    return new SecretKeySpec(key, "AES");
  }


  private byte[] rsaPrivateKeyDecrypt(byte[] encrypted) throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PRIVATEALIAS, null);
    Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
    output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
    CipherInputStream cipherInputStream = new CipherInputStream(
        new ByteArrayInputStream(encrypted), output);
    ArrayList<Byte> values = new ArrayList<>();
    int nextByte;
    while ((nextByte = cipherInputStream.read()) != -1) {
      values.add((byte) nextByte);
    }

    byte[] bytes = new byte[values.size()];
    for (int i = 0; i < bytes.length; i++) {
      bytes[i] = values.get(i);
    }
    return bytes;
  }

  private byte[] rsaPublicKeyDecrypt(byte[] encrypted) throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PUBLICALIAS, null);
    Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
    output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
    CipherInputStream cipherInputStream = new CipherInputStream(
        new ByteArrayInputStream(encrypted), output);
    ArrayList<Byte> values = new ArrayList<>();
    int nextByte;
    while ((nextByte = cipherInputStream.read()) != -1) {
      values.add((byte) nextByte);
    }

    byte[] bytes = new byte[values.size()];
    for (int i = 0; i < bytes.length; i++) {
      bytes[i] = values.get(i);
    }
    return bytes;
  }

  @SuppressLint("NewApi")
  @Override
  public void generateEncryptionPrivateKeyPostLollipop(String exportedPrivateKey) {
    KeyGenerator keyGenerator = null;
    try {
      keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEYSTORE);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (NoSuchProviderException e) {
      e.printStackTrace();
    }
    try {
      assert keyGenerator != null;
      keyGenerator.init
          (new KeyGenParameterSpec.Builder(KEY_PRIVATEALIAS,
              KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
              .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
              .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
              .setRandomizedEncryptionRequired(false)
              .build());
    } catch (InvalidAlgorithmParameterException e) {
      e.printStackTrace();
    }
    keyGenerator.generateKey();
    createVirgilPrivateKey(exportedPrivateKey);
  }


  @SuppressLint("NewApi")
  @Override
  public void generateEncryptionPublicKeyPostLollipop(String exportedPublicKey) {
    KeyGenerator keyGenerator = null;
    try {
      keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEYSTORE);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (NoSuchProviderException e) {
      e.printStackTrace();
    }
    try {
      assert keyGenerator != null;
      keyGenerator.init
          (new KeyGenParameterSpec.Builder(KEY_PUBLICALIAS,
              KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
              .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
              .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
              .setRandomizedEncryptionRequired(false)
              .build());
    } catch (InvalidAlgorithmParameterException e) {
      e.printStackTrace();
    }
    keyGenerator.generateKey();
    createVirgilPublicKey(exportedPublicKey);
  }

  @SuppressLint("NewApi")
  @Override
  public void generateEncryptionPrivateKeyPreMarshMallow(String exportedPrivateKey) {
    // Generate a key pair for encryption
    Calendar start = Calendar.getInstance();
    Calendar end = Calendar.getInstance();
    end.add(Calendar.YEAR, 30);
    KeyPairGeneratorSpec spec;
    spec = new KeyPairGeneratorSpec.Builder(mContext)
        .setAlias(KEY_PRIVATEALIAS)
        .setSubject(new X500Principal("CN=" + KEY_PRIVATEALIAS))
        .setSerialNumber(BigInteger.TEN)
        .setStartDate(start.getTime())
        .setEndDate(end.getTime())
        .build();

    KeyPairGenerator kpg = null;
    try {
      kpg = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, ANDROID_KEYSTORE);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (NoSuchProviderException e) {
      e.printStackTrace();
    }
    try {
      assert kpg != null;
      kpg.initialize(spec);
    } catch (InvalidAlgorithmParameterException e) {
      e.printStackTrace();
    }
    kpg.generateKeyPair();

    generateAndStorePrivateKeyAES();


    createVirgilPrivateKey(exportedPrivateKey);
  }

  @SuppressLint("NewApi")
  @Override
  public void generateEncryptionPublicKeyPreMarshMallow(String exportedPublicKey) {
    // Generate a key pair for encryption
    Calendar start = Calendar.getInstance();
    Calendar end = Calendar.getInstance();
    end.add(Calendar.YEAR, 30);
    KeyPairGeneratorSpec spec;
    spec = new KeyPairGeneratorSpec.Builder(mContext)
        .setAlias(KEY_PUBLICALIAS)
        .setSubject(new X500Principal("CN=" + KEY_PUBLICALIAS))
        .setSerialNumber(BigInteger.TEN)
        .setStartDate(start.getTime())
        .setEndDate(end.getTime())
        .build();

    KeyPairGenerator kpg = null;
    try {
      kpg = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, ANDROID_KEYSTORE);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (NoSuchProviderException e) {
      e.printStackTrace();
    }
    try {
      assert kpg != null;
      kpg.initialize(spec);
    } catch (InvalidAlgorithmParameterException e) {
      e.printStackTrace();
    }
    kpg.generateKeyPair();

    generateAndStorePublicKeyAES();

    createVirgilPublicKey(exportedPublicKey);
  }

  private void createVirgilPrivateKey(String exportedPrivateKey) {
/*
    try {
      CryptographyExample cryptographyExample = new CryptographyExample();
      // Generate keys
      VirgilCrypto crypto = new VirgilCrypto();
      VirgilKeyPair keyPair = cryptographyExample.keyGeneration();
      // Export private key
      exportedPrivateKey = cryptographyExample.exportPrivateKey(keyPair, crypto);
      // Export public key
      exportedPublicKey = cryptographyExample.exportPublicKey(keyPair, crypto);
    } catch (CryptoException e) {
      e.printStackTrace();
    }
*/
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      encryptPrivateKeyPostLollipop(exportedPrivateKey);
    } else {
      encryptPrivateKeyPreMarshMallow(exportedPrivateKey);
    }
  }

  private void createVirgilPublicKey(String exportedPublicKey) {
/*
    try {
      CryptographyExample cryptographyExample = new CryptographyExample();
      // Generate keys
      VirgilCrypto crypto = new VirgilCrypto();
      VirgilKeyPair keyPair = cryptographyExample.keyGeneration();
      // Export private key
      exportedPrivateKey = cryptographyExample.exportPrivateKey(keyPair, crypto);
      // Export public key
      exportedPublicKey = cryptographyExample.exportPublicKey(keyPair, crypto);
    } catch (CryptoException e) {
      e.printStackTrace();
    }
*/
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      encryptPublicKeyPostLollipop(exportedPublicKey);
    } else {
      encryptPublicKeyPreMarshMallow(exportedPublicKey);
    }
  }

  @SuppressLint("NewApi")
  @Override
  public void encryptPrivateKeyPostLollipop(String input) {
    Cipher c;
    try {
      c = Cipher.getInstance(AES_MODE);
      c.init(Cipher.ENCRYPT_MODE, getSecretPrivateKeyPostLollipop(), new GCMParameterSpec(128,
          FIXED_IV.getBytes()));
      byte[] encodedBytes = c.doFinal(input.getBytes(StandardCharsets.US_ASCII));

      String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
      Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
      mSessionManager.setVirgilPrivateKey(encryptedBase64Encoded);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @SuppressLint("NewApi")
  @Override
  public void encryptPublicKeyPostLollipop(String input) {
    Cipher c;
    try {
      c = Cipher.getInstance(AES_MODE);
      c.init(Cipher.ENCRYPT_MODE, getSecretPublicKeyPostLollipop(), new GCMParameterSpec(128,
          FIXED_IV.getBytes()));
      byte[] encodedBytes = c.doFinal(input.getBytes(StandardCharsets.US_ASCII));

      String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
      Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
      mSessionManager.setVirgilPublicKey(encryptedBase64Encoded);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void encryptPrivateKeyPreMarshMallow(String input) {
    Cipher c;
    try {
      c = Cipher.getInstance(AES_MODE, "BC");
      c.init(Cipher.ENCRYPT_MODE, getSecretPrivateKeyPreMarshMallow(), IV_PARAMETER_SPEC);
      byte[] encodedBytes = c.doFinal(input.getBytes());
      String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
      Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
      mSessionManager.setVirgilPrivateKey(encryptedBase64Encoded);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void encryptPublicKeyPreMarshMallow(String input) {
    Cipher c;
    try {
      c = Cipher.getInstance(AES_MODE, "BC");
      c.init(Cipher.ENCRYPT_MODE, getSecretPublicKeyPreMarshMallow(), IV_PARAMETER_SPEC);
      byte[] encodedBytes = c.doFinal(input.getBytes());
      String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
      Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
      mSessionManager.setVirgilPublicKey(encryptedBase64Encoded);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
