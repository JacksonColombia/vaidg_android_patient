package com.vaidg.signup;

import static com.vaidg.utilities.Constants.CAMERAIMAGE;
import static com.vaidg.utilities.Constants.CHECKEMAIL;
import static com.vaidg.utilities.Constants.CHECKFIRSTNAME;
import static com.vaidg.utilities.Constants.CHECKPASSWORD;
import static com.vaidg.utilities.Constants.COUNTRYSYMBOl;
import static com.vaidg.utilities.Constants.COUNTRY_SYMBOL;
import static com.vaidg.utilities.Constants.FAQSCREEN.EXTRA_COMINFROM;
import static com.vaidg.utilities.Constants.COMINGFROM;
import static com.vaidg.utilities.Constants.COUNTRYCODE;
import static com.vaidg.utilities.Constants.DIALOGMESSAGESHOW_YES;
import static com.vaidg.utilities.Constants.EMAIL;
import static com.vaidg.utilities.Constants.FACEBOOK_LOGIN;
import static com.vaidg.utilities.Constants.GOOGLE_LOGIN;
import static com.vaidg.utilities.Constants.FAQSCREEN.EXTRA_LINK;
import static com.vaidg.utilities.Constants.MOBILENUMBER;
import static com.vaidg.utilities.Constants.PHONE;
import static com.vaidg.utilities.Constants.REFERAL_LENGTH_FIVE;
import static com.vaidg.utilities.Constants.REFERAL_LENGTH_FOUR;
import static com.vaidg.utilities.Constants.REFERAL_LENGTH_SIX;
import static com.vaidg.utilities.Constants.SIGNUP;
import static com.vaidg.utilities.Constants.FAQSCREEN.EXTRA_TITLE;
import static com.vaidg.utilities.Constants.USER_DETAILS;
import static com.vaidg.utilities.Constants.dateOB;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;

import com.vaidg.BuildConfig;
import com.vaidg.R;
import com.vaidg.countrypic.Country;
import com.vaidg.countrypic.CountryPicker;
import com.vaidg.encryptiondecryption.CryptographyExample;
import com.vaidg.faq.faq_detail.WebViewActivity;
import com.vaidg.home.MainActivity;
import com.vaidg.model.UserDetailsDataModel;
import com.vaidg.otp.OtpActivity;
import com.vaidg.signup.gender.GenderActivity;
import com.vaidg.signup.gender.model.GenderListPojo;
import com.vaidg.utilities.AppPermissionsRunTime;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.HandlePictureEvents;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.material.textfield.TextInputLayout;
import com.utility.AlertProgress;
import com.utility.PicassoTrustAll;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilKeyPair;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;
import dagger.android.support.DaggerAppCompatActivity;
import eu.janmuller.android.simplecropimage.CropImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * <h2>SignUp Activity</h2>
 * <p>This class is used to provide the SignUp screen, where we can register a user and after that
 * we get an OTP on our mobile and
 * this class gives a call to SignUpPresenterImpl class.
 * </p>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */

@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
public class SignUpActivity extends DaggerAppCompatActivity implements SignUpView {

  private static final int REQUEST_CODE_PERMISSION_MULTIPLE = 143;

  private static final int REQUEST_ID_SMS_PERMISSION = 12;
  private static final int REQUEST_ID_READ_STORAGE_PERMISSION = 116;
  private static final int REQUEST_ID_GOOGLE_SIGNIN = 66;
  private static final int REQUEST_GENDER = 55;
  public final String TAG = SignUpActivity.class.getName();
  View focusView;
  @BindView(R.id.ivRegProfile)
  ImageView ivRegProfile;
  @BindView(R.id.etRegDob)
  EditText etRegDob;
  @BindView(R.id.etRegGender)
  EditText etRegGender;
  @BindView(R.id.tilRegGender)
  TextInputLayout tilRegGender;
  @BindView(R.id.tilRegDob)
  TextInputLayout tilRegDob;
  @BindView(R.id.tvClicktermscondition)
  TextView tvClicktermscondition;
  @BindView(R.id.countryCode)
  TextView countryCode;
  @BindView(R.id.countryFlag)
  ImageView countryFlag;
  @BindView(R.id.etRegEmail)
  EditText etRegEmail;
  @BindView(R.id.tilRegEmail)
  TextInputLayout tilRegEmail;
  @BindView(R.id.etRegFname)
  EditText etRegFname;
  @BindView(R.id.tilRegFname)
  TextInputLayout tilRegFname;
  @BindView(R.id.tilRegLname)
  TextInputLayout tilRegLname;
  @BindView(R.id.etRegLname)
  EditText etRegLname;
  @BindView(R.id.etRegPasswd)
  EditText etRegPasswd;
  @BindView(R.id.tilRegPaswrd)
  TextInputLayout tilRegPaswrd;
  @BindView(R.id.etRegMobno)
  EditText etRegMobno;
  @BindView(R.id.tilRegMobNo)
  TextInputLayout tilRegMobNo;
  @BindView(R.id.etRegReferral)
  EditText etRegReferral;
  @BindView(R.id.rlFbLogin)
  RelativeLayout rlFbLogin;
  //PercentageButton btnSignup;
  @BindView(R.id.rlGoogleLogin)
  RelativeLayout rlGoogleLogin;
  @BindView(R.id.btnSignup)
  Button btnSignup;
  @BindView(R.id.fbSignupOr)
  View fbSignupOr;
  @BindString(R.string.signupWithGoogle)
  String signupWithGoogle;
  @BindString(R.string.signupWithFacebook)
  String signupWithFacebook;
  private String exportedPrivateKey = "", exportedPublicKey = "";
  @Inject
  SignUpPresenter mSignUpPresenter;
  @Inject
  CountryPicker mCountryPicker;
  AlertDialog mAlertDialog;
  @Inject
  AlertProgress mAlertProgress;
  @Inject
  AppTypeface mAppTypeface;
  @BindView(R.id.tvTermsclick)
  TextView tvTermsclick;
  @BindString(R.string.privacyPolicy)
  String privacyPolicy;
  @BindString(R.string.termsAndCondition)
  String termsCondition;
  @Inject
  SessionManagerImpl mSessionManager;
  String mTakenNewImageFileName;
  boolean imageFlag = true;
  private String regFname, regLname, regEmail, regPassword, regCountryCode,regCountryCodeName, regMobileNo,dateOfBirth,gender;
  private String[] perms_fb = {Manifest.permission.READ_EXTERNAL_STORAGE};
  private HandlePictureEvents mHandlePictureEvents;
  private String profilePicUrl = "";
  private CallbackManager mCallbackManager;
  private boolean isProfilePicSelected;
  private boolean isFbClicked;
  private String referralCode = "";
  private ArrayList<GenderListPojo> mGenderArrayList = new ArrayList<>();
  /**
   * This is the onCreateSignUpActivity method that is called firstly, when user came to signup
   * screen.
   *
   * @param savedInstanceState contains an instance of Bundle.
   */

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_signup);
    mSignUpPresenter.checkForKeystore();
    ButterKnife.bind(this);
    mHandlePictureEvents = new HandlePictureEvents(this);
    initialize();
    getIntentValue();
    termsConditionReferals();
  }

  private void termsConditionReferals() {
    //  mSignUpPresenter.setTermsCondition(tvTermsclick, privacyPolicy);
    mSignUpPresenter.setTermsCondition(tvTermsclick, termsCondition);
/*
        mSignUpPresenter.setOnclickHighlighted(tvTermsclick, privacyPolicy,
                view -> callTermsAndCondition(BuildConfig.PRIVECY_LINK, privacyPolicy));
*/
    mSignUpPresenter.setOnclickHighlighted(tvTermsclick, termsCondition,
        view -> callTermsAndCondition(BuildConfig.TERMS_LINK, termsCondition));
    etRegReferral.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void afterTextChanged(Editable editable) {

        String code = etRegReferral.getText().toString().trim();
        if ("".equalsIgnoreCase(referralCode)) {
          if (code.length() == REFERAL_LENGTH_FOUR || code.length() == REFERAL_LENGTH_FIVE || code.length() == REFERAL_LENGTH_SIX) {
            mSignUpPresenter.checkReferralCode(etRegReferral.getText().toString().trim());
          }
        }

      }
    });
  }

  private void callTermsAndCondition(String link, String title) {
    Intent intent = new Intent(SignUpActivity.this, WebViewActivity.class);
    intent.putExtra(EXTRA_LINK, link);
    intent.putExtra(EXTRA_TITLE, title);
    intent.putExtra(EXTRA_COMINFROM, termsCondition);
    startActivity(intent);
    overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    if (mHandlePictureEvents.newFile != null) {
      outState.putString(CAMERAIMAGE, mHandlePictureEvents.newFile.getPath());
    }
    super.onSaveInstanceState(outState);
  }

  @Override
  protected void onRestoreInstanceState(Bundle savedInstanceState) {
    if (savedInstanceState != null) {
      if (savedInstanceState.containsKey(CAMERAIMAGE)) {
        Uri uri = Uri.parse(savedInstanceState.getString(CAMERAIMAGE));
        if (uri.getPath() != null) {
          mHandlePictureEvents.newFile = new File(uri.getPath());
        }
      }
    }
    super.onRestoreInstanceState(savedInstanceState);
  }

  private void getIntentValue() {

    Bundle bundle = this.getIntent().getExtras();
    if (bundle != null) {
      UserDetailsDataModel userDetailsDataModel = (UserDetailsDataModel) bundle.getSerializable(
          USER_DETAILS);
      if (userDetailsDataModel != null) {
        if (userDetailsDataModel.getLoginType() == FACEBOOK_LOGIN) {
          mSignUpPresenter.storeLoginType(FACEBOOK_LOGIN);
          mSignUpPresenter.storeFbId(userDetailsDataModel.getFacebookId());
          setValuesFromFbGoogle(userDetailsDataModel);

        } else if (userDetailsDataModel.getLoginType() == GOOGLE_LOGIN) {
          mSignUpPresenter.storeGoogleId(userDetailsDataModel.getGoogleId());
          mSignUpPresenter.storeLoginType(GOOGLE_LOGIN);
          setValuesFromFbGoogle(userDetailsDataModel);

        }
      }
    }
  }


  private void setValuesFromFbGoogle(UserDetailsDataModel userDetailsDataModel) {
    if (!TextUtils.isEmpty(userDetailsDataModel.getFirstName())) {
      etRegFname.setText(userDetailsDataModel.getFirstName());
    }
    if (!TextUtils.isEmpty(userDetailsDataModel.getLastName())) {
      etRegLname.setText(userDetailsDataModel.getLastName());
    }
    if (!TextUtils.isEmpty(userDetailsDataModel.getEmailOrPhone())) {
      etRegEmail.setText(userDetailsDataModel.getEmailOrPhone());
    }
    if (!TextUtils.isEmpty(userDetailsDataModel.getProfilePicUrl())) {
      profilePicUrl = userDetailsDataModel.getProfilePicUrl();
      PicassoTrustAll.getInstance(this)
              .load(profilePicUrl)
              .placeholder(R.drawable.default_photo)   // optional
              .error(R.drawable.default_photo)      // optional
              .transform(new PicassoCircleTransform())
              .into(ivRegProfile);

    }
  }

  /**
   * <h2>initialize</h2>
   * <p> method to initialize the views and other resources required for the class</p>
   */
  private void initialize() {
    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayShowHomeEnabled(true);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
    TextView textView = toolbar.findViewById(R.id.tv_center);
    textView.setText(getResources().getString(R.string.signup));
    textView.setTypeface(mAppTypeface.getHind_medium());
    toolbar.setNavigationIcon(R.drawable.ic_back);
    toolbar.setNavigationOnClickListener(view -> onBackPressed());
    mTakenNewImageFileName = "LSPProfilepic" + ".png";

    mCallbackManager = CallbackManager.Factory.create();

    tvTermsclick.setTypeface(mAppTypeface.getHind_regular());
    tvClicktermscondition.setTypeface(mAppTypeface.getHind_regular());
    etRegDob.setTypeface(mAppTypeface.getHind_regular());
    tilRegDob.setTypeface(mAppTypeface.getHind_regular());
    tilRegGender.setTypeface(mAppTypeface.getHind_regular());
    countryCode.setTypeface(mAppTypeface.getHind_regular());
    etRegFname.setTypeface(mAppTypeface.getHind_regular());
    etRegLname.setTypeface(mAppTypeface.getHind_regular());
    etRegEmail.setTypeface(mAppTypeface.getHind_regular());
    etRegMobno.setTypeface(mAppTypeface.getHind_regular());
    etRegPasswd.setTypeface(mAppTypeface.getHind_regular());
    etRegReferral.setTypeface(mAppTypeface.getHind_regular());
    etRegDob.setTypeface(mAppTypeface.getHind_regular());
    etRegGender.setTypeface(mAppTypeface.getHind_regular());
    btnSignup.setTypeface(mAppTypeface.getHind_semiBold());
    TextView tv_fb_signup = fbSignupOr.findViewById(R.id.tvLoginWithfb);
    TextView tv_google_signup = fbSignupOr.findViewById(R.id.tvLoginWithGoogle);

    tv_fb_signup.setText(signupWithFacebook);
    tv_google_signup.setText(signupWithGoogle);

    Animation lAnimation = AnimationUtils.loadAnimation(this, R.anim.lfade);

    rlFbLogin.startAnimation(lAnimation);
    btnSignup.startAnimation(lAnimation);

    Animation rAnimation = AnimationUtils.loadAnimation(this, R.anim.rfade);

    rlGoogleLogin.startAnimation(rAnimation);

    setListener();

    onEditChangeListner();
  }

  private void onEditChangeListner() {

    etRegMobno.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void afterTextChanged(Editable editable) {
        focusViewNull();
      }
    });
  }


  @OnClick({R.id.rlFbLogin, R.id.rlGoogleLogin, R.id.btnSignup, R.id.ivRegProfile,
      R.id.countryCode, R.id.countryFlag, R.id.tvClicktermscondition, R.id.etRegGender, R.id.ivRegArrow, R.id.etRegDob, R.id.ivRegCalender})
  void onClickEvent(View view) {
    switch (view.getId()) {
      case R.id.rlFbLogin:
        //Toast.makeText(this, "Signup with Facebook", Toast.LENGTH_SHORT).show();
        if (Build.VERSION.SDK_INT >= 23) {
          if (ContextCompat.checkSelfPermission(SignUpActivity.this, perms_fb[0])
              == PackageManager.PERMISSION_GRANTED) {
            mSignUpPresenter.storeLoginType(FACEBOOK_LOGIN);
            mSignUpPresenter.handleResultFromFB(mCallbackManager);
          } else {
            isFbClicked = true;
            checkAndReqReadPerms();
          }
        } else {
          mSignUpPresenter.storeLoginType(FACEBOOK_LOGIN);
          mSignUpPresenter.handleResultFromFB(mCallbackManager);
        }
        break;
      case R.id.rlGoogleLogin:
        if (Build.VERSION.SDK_INT >= 23) {
          if (ContextCompat.checkSelfPermission(SignUpActivity.this, perms_fb[0])
              == PackageManager.PERMISSION_GRANTED) {
            mSignUpPresenter.storeLoginType(GOOGLE_LOGIN);
            mSignUpPresenter.googleLogin();
          } else {
            checkAndReqReadPerms();
          }
        } else {
          mSignUpPresenter.storeLoginType(GOOGLE_LOGIN);
          mSignUpPresenter.googleLogin();
        }
        break;
      case R.id.btnSignup:
        regFname = etRegFname.getText().toString();
        regLname = etRegLname.getText().toString();
        regEmail = etRegEmail.getText().toString();
        regPassword = etRegPasswd.getText().toString();
        regCountryCode = countryCode.getText().toString();
        regMobileNo = etRegMobno.getText().toString();
        dateOfBirth = dateOB;
        gender = etRegGender.getText().toString();

        if (dateOfBirth.isEmpty()) {
          Toast.makeText(this, this.getResources().getString(R.string.dob_dependent_mandatory),
              Toast.LENGTH_SHORT).show();
          mSignUpPresenter.openCalendar(this);
          return;
        }
        if (gender.isEmpty()) {
          Toast.makeText(this, this.getResources().getString(R.string.dob_dependent_mandatory),
              Toast.LENGTH_SHORT).show();
          mSignUpPresenter.openCalendar(this);
          return;
        }


        Utility.hideKeyboard(this);

        if (focusView != null) {
          focusView.requestFocus();
        } else {
          mSignUpPresenter.doRegister(profilePicUrl, regFname, regLname, regEmail, regPassword,
              regCountryCodeName,regCountryCode, regMobileNo, referralCode, gender,dateOfBirth);
        }
        break;
      case R.id.ivRegProfile:
        checkForPermission();
        break;
      case R.id.etRegDob:
      case R.id.ivRegCalender:
        Utility.hideKeyboard(SignUpActivity.this);
        mSignUpPresenter.openCalendar(this);
        break;
        case R.id.etRegGender:
      case R.id.ivRegArrow:
        Utility.hideKeyboard(SignUpActivity.this);
        Intent intent = new Intent(this, GenderActivity.class);
        intent.putExtra("mGenderArrayList",mGenderArrayList);
        startActivityForResult(intent,REQUEST_GENDER);
        break;
      case R.id.countryCode:
      case R.id.countryFlag:
        mCountryPicker.show(getSupportFragmentManager(),
            getResources().getString(R.string.Countrypicker));
        break;

      case R.id.tvClicktermscondition:
        break;
    }
  }

  @OnFocusChange({R.id.etRegFname, R.id.etRegLname, R.id.etRegEmail, R.id.etRegPasswd,
      R.id.etRegMobno,  R.id.etRegDob})
  void onFocusChangeEvent(View view, boolean hasFocus) {
    switch (view.getId()) {
      case R.id.etRegFname:
        if (!hasFocus) {
          focusViewNull();
          //Check First name
          mSignUpPresenter.validateFname(etRegFname.getText().toString());
        }
        break;
      case R.id.etRegEmail:
        if (!hasFocus) {
          focusViewNull();
          //Checks email
          mSignUpPresenter.validateEmail(DIALOGMESSAGESHOW_YES,
              etRegEmail.getText().toString().toLowerCase(Locale.US));
        }
        break;

      case R.id.etRegPasswd:
        if (!hasFocus) {
          focusViewNull();
          //Validation for password
          mSignUpPresenter.validatePassword(etRegPasswd.getText().toString());
        }

        break;
      case R.id.etRegMobno:
        if (!hasFocus) {
          mSignUpPresenter.validatePhone(DIALOGMESSAGESHOW_YES, countryCode.getText().toString().trim(),
              etRegMobno.getText().toString().trim());
        }
        break;
      case R.id.etRegDob:
        if (!hasFocus) {
          etRegDob.setCursorVisible(false);
          if (etRegDob.getText().toString().trim().length() > 0) {
            focusViewNull();
            if (mSignUpPresenter.checkDobValidity()) {
              etRegDob.setFocusable(false);
            } else {
              etRegDob.setFocusable(false);
              mSignUpPresenter.openCalendar(this);
            }
          }
        }else {
          if (etRegDob.getText().toString().trim().length() > 0) {
            if (mSignUpPresenter.checkDobValidity()) {
              tilRegDob.setErrorEnabled(false);
              tilRegDob.setError("");
              focusView = null;
              etRegDob.setFocusable(false);
            } else {
              etRegDob.setText("");
            }

          } else {
            mSignUpPresenter.openCalendar(this);
          }
          Utility.hideKeyboard(this);
        }
    }
  }


  @OnTextChanged(value = R.id.etRegFname,
      callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
  void onFnameTextChange(Editable editable) {
    int fname_str_len = editable.toString().length();
    if (fname_str_len <= 0) {
      tilRegFname.setError(null);
      setFirstNameError();
    }
    if (fname_str_len > 0) {
      tilRegFname.setError(null);
    }
  }


  @Override
  public void showProgress(String text) {
    if (!isFinishing()) {

      switch (text) {
        case EMAIL:
          mAlertDialog = mAlertProgress.getProgressDialog(this, getString(R.string.wait_email));
          break;
        case PHONE:
          mAlertDialog = mAlertProgress.getProgressDialog(this, getString(R.string.wait_phone));
          break;
        default:
          mAlertDialog = mAlertProgress.getProgressDialog(this, getString(R.string.wait_register));
      }
      mAlertDialog.show();
    }

  }

  @Override
  public void hideProgress() {
    if (mAlertDialog != null && !isFinishing()) {
      mAlertDialog.dismiss();
    }
  }


  @Override
  public void onLoginSuccess(String auth, String email, String password) {
    if (mSessionManager.getGuestLogin()) {
      mSessionManager.setGuestLogin(false);
    } else {
      mSessionManager.setGuestLogin(false);
      Intent intent = new Intent(this, MainActivity.class);
      //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
          | Intent.FLAG_ACTIVITY_CLEAR_TOP);
      startActivity(intent);
    }
    finish();
  }

  @Override
  public void setFieldsFromFB(String firstName, String lastName, String email, String phone_no,
                              String profilePic) {
    etRegFname.setText(firstName);
    etRegLname.setText(lastName);
    if (!TextUtils.isEmpty(email)) {
      etRegEmail.setText(email);
    }
    if (!TextUtils.isEmpty(phone_no)) {
      etRegMobno.setText(phone_no);
    }
    if (!TextUtils.isEmpty(profilePic)) {
      profilePicUrl = profilePic;
    }
    etRegEmail.requestFocus();
  if(profilePicUrl != null && !profilePicUrl.isEmpty()) {
    PicassoTrustAll.getInstance(this)
        .load(profilePicUrl)
        .placeholder(R.drawable.default_photo)   // optional
        .error(R.drawable.default_photo)      // optional
        .transform(new PicassoCircleTransform())
        .into(ivRegProfile);
  }
  }


  @Override
  public void navigateToOtp() {

    Bundle bundle = new Bundle();
    Intent intent = new Intent(this, OtpActivity.class);

    bundle.putString(COMINGFROM, SIGNUP);
    bundle.putString(MOBILENUMBER, regMobileNo);
    bundle.putString(COUNTRYCODE, regCountryCode);
    bundle.putString(COUNTRYSYMBOl, regCountryCodeName);

    intent.putExtras(bundle);
    startActivity(intent);

    // finish();

  }

  // Check and Request for SMS Permissions for sending OTP while register
  private void checkAndReqSmsPerms() {
    if (focusView != null) {
      focusView.requestFocus();
    } else {
      mSignUpPresenter.doRegister(profilePicUrl, regFname, regLname, regEmail, regPassword,
          regCountryCodeName,regCountryCode, regMobileNo, referralCode, gender,dateOfBirth);
    }
  }

  private void checkForPermission() {
    if (Build.VERSION.SDK_INT >= 23) {
      // Marshmallow+
      ArrayList<AppPermissionsRunTime.MyPermissionConstants> myPermissionConstantsArrayList =
          new ArrayList<>();
      myPermissionConstantsArrayList.clear();
      myPermissionConstantsArrayList.add(
          AppPermissionsRunTime.MyPermissionConstants.PERMISSION_CAMERA);
      myPermissionConstantsArrayList.add(
          AppPermissionsRunTime.MyPermissionConstants.PERMISSION_WRITE_EXTERNAL_STORAGE);
      myPermissionConstantsArrayList.add(
          AppPermissionsRunTime.MyPermissionConstants.PERMISSION_READ_EXTERNAL_STORAGE);
      if (AppPermissionsRunTime.checkPermission(this, myPermissionConstantsArrayList,
          REQUEST_CODE_PERMISSION_MULTIPLE)) {

        if (!isFbClicked) {
          selectImage();
        } else {
          //   new DownloadImage().execute(fb_pic);
        }
        //}
      }
    } else {
      selectImage();
    }
  }


  /**
   * <h2>setListener</h2>
   * <p>This is to set listener for CountryPicker to get all the countryCodes,Flags and map them
   * accordingly</p>
   */
  private void setListener() {
    mCountryPicker.setListener((name, code, dialCode, flagDrawableResID, max) -> {
      countryCode.setText(dialCode);
      regCountryCodeName = code;
      countryFlag.setImageResource(flagDrawableResID);
      mCountryPicker.dismiss();
      //  etRegMobno.setFilters(new InputFilter[]{new InputFilter.LengthFilter(max)});

      // etRegMobno.setFilters(new InputFilter[] { new InputFilter.LengthFilter(max)});
    });
    countryCode.setOnClickListener(v -> mCountryPicker.show(getSupportFragmentManager(),
        getResources().getString(R.string.Countrypicker)));
    //By Default, Current country
    getUserCountryInfo();
  }

  private void getUserCountryInfo() {
    Country country = mCountryPicker.getUserCountryInfo(this);
    countryFlag.setImageResource(country.getFlag());
    countryCode.setText(country.getDial_code());
    regCountryCodeName = country.getCode();
       /* if (country.getMax_digits()!=null) {
            etRegMobno.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Integer
            .parseInt(country.getMax_digits()))});
        }*/

  }

  private void selectImage() {
    mHandlePictureEvents.openDialog();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    switch (requestCode) {
      case Constants.CAMERA_PIC:
        mHandlePictureEvents.startCropImage(mHandlePictureEvents.newFile);
        break;
      case Constants.GALLERY_PIC:
        if (data != null) {
          mHandlePictureEvents.gallery(data.getData());
        }
        break;

      case Constants.CROP_IMAGE:
        if (data != null) {
          String path = data.getStringExtra(CropImage.IMAGE_PATH);
          if (path != null) {
            try {
              File fileExist = new File(path);

              imageFlag = true;
              if(fileExist != null ) {
                PicassoTrustAll.getInstance(this)
                    .load(fileExist)
                    .placeholder(R.drawable.default_photo)   // optional
                    .error(R.drawable.default_photo)      // optional
                    .transform(new PicassoCircleTransform())
                    .into(ivRegProfile);
              }
              new UploadFileToServer().execute(path);
              //UploadFileToServer(fileExist);
/*
              mHandlePictureEvents.uploadToAmazon(
                  Constants.Amazonbucket + "/" + Constants.AmazonProfileFolderName, fileExist,
                  new ImageUploadedAmazon() {
                    @Override
                    public void onSuccessAdded(String image) {
                      profilePicUrl = image;

                    }

                    @Override
                    public void onerror(String errormsg) {
                    }

                  });
*/
                       /* ivRegProfile.setImageURI(Uri.parse(path));
                        ivRegProfile.setImageURI(Uri.parse(fileExist.toString()));*/
            } catch (Exception e) {
              e.printStackTrace();
            }
          }
        }
        break;

      case REQUEST_ID_GOOGLE_SIGNIN:
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        mSignUpPresenter.handleResultFromGoogle(result);
        break;
    case REQUEST_GENDER:
        if(resultCode == RESULT_OK) {
          if (data != null) {
            gender = data.getStringExtra("GenderName");
            mGenderArrayList = (ArrayList<GenderListPojo>) data.getSerializableExtra("mGenderArrayList");
            etRegGender.setText(Utility.gender(gender));
          }
        }
        break;

      default:
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
        break;
    }
  }

  @Override
  public void onPointerCaptureChanged(boolean hasCapture) {

  }

  /**
   * predefined method to check run time permissions list call back
   *
   * @param requestCode   request code
   * @param permissions:  contains the list of requested permissions
   * @param grantResults: contains granted and un granted permissions result list
   */
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
    boolean isDenine = false;
    switch (requestCode) {
      case REQUEST_CODE_PERMISSION_MULTIPLE:
        for (int grantResult : grantResults) {
          if (grantResult != PackageManager.PERMISSION_GRANTED) {
            isDenine = true;
          }
        }
        if (isDenine) {
          Toast.makeText(this, "Permission denied by the user", Toast.LENGTH_SHORT).show();
        } else {
          if (isProfilePicSelected) {
            Drawable myDrawable = getResources().getDrawable(
                R.drawable.register_profile_default_image);
            Bitmap anImage = ((BitmapDrawable) myDrawable).getBitmap();
          } else {
            if (!isFbClicked) {
              selectImage();
            } else {
              //new DownloadImage().execute(fb_pic);
            }
          }
        }
        break;

      case REQUEST_ID_SMS_PERMISSION:

        boolean permissionAllowed = false;
        String deniedPermission = "";
        int i = 0;
        for (int grantResult : grantResults) {
          if (grantResult == PackageManager.PERMISSION_GRANTED) {
            permissionAllowed = true;
            i++;
          } else {
            deniedPermission = permissions[i];
            permissionAllowed = false;
            break;
          }
        }
        if (permissionAllowed) {

          if (focusView != null) {
            focusView.requestFocus();
          } else {
            mSignUpPresenter.doRegister(profilePicUrl, regFname, regLname, regEmail, regPassword,
                    regCountryCodeName,regCountryCode, regMobileNo, referralCode, gender,dateOfBirth);
          }
          //   mSignUpPresenter.doRegister(profilePicUrl,regFname,regLname, regEmail, regPassword,
          //   regCountryCode, regMobileNo, referralCode);

        } else {
          //Toast.makeText(this,deniedPermission+" Permission is denied",Toast.LENGTH_SHORT).show();
          boolean somePermissionsForeverDenied = false;
          for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
              //denied
              somePermissionsForeverDenied = true;
              //Toast.makeText(this,"Permission Denied! Please grant SMS permissions to proceed
              // further!",Toast.LENGTH_LONG).show();
            } else {
              if (ActivityCompat.checkSelfPermission(this, permission)
                  == PackageManager.PERMISSION_GRANTED) {
                //allowed
              } else {
                //set to never ask again
                somePermissionsForeverDenied = true;
              }
            }
          }

          if (somePermissionsForeverDenied) {
            Toast.makeText(this,
                getResources().getString(R.string.permission_denied),
                Toast.LENGTH_SHORT).show();
          }
        }

        break;

      case REQUEST_ID_READ_STORAGE_PERMISSION:

        boolean permissionAllowFlag = false;
        String deniedPermissions = "";
        int k = 0;
        for (int grantResult : grantResults) {
          if (grantResult == PackageManager.PERMISSION_GRANTED) {
            permissionAllowFlag = true;
            k++;
          } else {
            deniedPermissions = permissions[k];
            permissionAllowFlag = false;
            break;
          }
        }
        if (permissionAllowFlag) {
          if (isFbClicked) {
            mSignUpPresenter.storeLoginType(FACEBOOK_LOGIN);
            mSignUpPresenter.handleLoginType(mCallbackManager);
          } else {
            mSignUpPresenter.storeLoginType(GOOGLE_LOGIN);
            mSignUpPresenter.googleLogin();
          }
        } else {
          //Toast.makeText(this,deniedPermission+" Permission is denied",Toast.LENGTH_SHORT).show();
          boolean somePermissionsForeverDenied = false;
          for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
              //denied
              somePermissionsForeverDenied = true;
              //Toast.makeText(this,"Permission Denied! Please grant SMS permissions to proceed
              // further!",Toast.LENGTH_LONG).show();
            } else {
              if (ActivityCompat.checkSelfPermission(this, permission)
                  == PackageManager.PERMISSION_GRANTED) {
                //allowed
              } else {
                //set to never ask again
                somePermissionsForeverDenied = true;
              }
            }
          }

          if (somePermissionsForeverDenied) {
            Toast.makeText(this,
                getResources().getString(R.string.permission_denied),
                Toast.LENGTH_SHORT).show();
          }
        }

        break;

      default:
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        break;
    }
  }


  /*<h2>checkAndReqReadPerms</h2>
   * <p> Check and Request for READ EXTERNAL STORAGE Permissions for facebook login. </p>
   */
  private void checkAndReqReadPerms() {
    if (Build.VERSION.SDK_INT >= 23) {

      int readPerm = ContextCompat.checkSelfPermission(this,
          Manifest.permission.READ_EXTERNAL_STORAGE);

      List<String> listPermissionsNeeded = new ArrayList<>();

      if (readPerm != PackageManager.PERMISSION_GRANTED) {
        listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
      }

      if (!listPermissionsNeeded.isEmpty()) {
        ActivityCompat.requestPermissions(this,
            listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
            REQUEST_ID_READ_STORAGE_PERMISSION);
      }
    }
  }

  @Override
  public void openGoogleActivity(Intent intent) {
    startActivityForResult(intent, REQUEST_ID_GOOGLE_SIGNIN);
  }

  @Override
  protected void onResume() {
    super.onResume();
    mSignUpPresenter.initializeFacebook();
  }

  @Override
  public void showPro() {
    showProgress(getString(R.string.wait_register));
  }


  @Override
  public void onReferral(String txtreferralCode) {
    etRegReferral.setText(txtreferralCode);
    referralCode = txtreferralCode;
  }

  /**
   * <h2>focusVieW</h2>
   * this set the focus to the number
   *
   * @param til is a TextInputLayout
   */
  private void focusVieW(TextInputLayout til) {
    focusView = til;
  }

  /**
   * <h2>focusViewNull</h2>
   * set the focus view to null
   */
  private void focusViewNull() {
    focusView = null;
  }

  @Override
  public void setFirstNameError() {
    focusVieWRequestFocus(tilRegFname, getString(R.string.errFname));
  }

  @Override
  public void setLastNameError() {
    //  tilRegLname.setError(getString(R.string.errLname));
  }


  @Override
  public void setMobileErrorMsg(String message) {
    // If it returns success from the Phone Validation API- message would be empty string,
    // Setting error to null
    if ("".equals(message)) {
      etRegMobno.setError(null);
      focusView = null;
    } else {
      focusView = etRegMobno;
      etRegMobno.setError(message);
      etRegMobno.setText("");
      //   etRegMobno.requestFocus();
    }
  }

  @Override
  public void setMobileInvalid() {
    focusView = etRegMobno;
    etRegMobno.setError(getString(R.string.errMobileInvalid));
    etRegMobno.setText("");
  }

  @Override
  public void setEmailInvalid(String error) {
    etRegEmail.setText("");
    if ("".equals(error)) {
      focusVieWRequestFocus(tilRegEmail, getString(R.string.errInvalidEmail));
    } else {
      focusVieWRequestFocus(tilRegEmail, error);
    }
  }

  @Override
  public void setEmailError() {
    focusVieWRequestFocus(tilRegEmail, getString(R.string.errEmail));
  }


  @Override
  public void setPasswordInvalid() {
    etRegPasswd.setText("");
    focusVieWRequestFocus(tilRegPaswrd, getString(R.string.invalidPassword));
  }

  @Override
  public void setPasswordError() {
    focusVieWRequestFocus(tilRegPaswrd, getString(R.string.errPassword));
  }


  @Override
  public void setMobileError() {
    etRegMobno.setError(getString(R.string.errMobile));
  }

  @Override
  public void onError(String msg) {
    mAlertProgress.alertinfo(this, msg);
  }

  @Override
  public void onBusinessNameIsEmpty() {
    // focusVieWRequestFocus(til_reg_BusinessName,getString(R.string.err_Business));
  }

  @Override
  public void clearError(int i) {

    switch (i) {
      case CHECKFIRSTNAME:
        tilRegFname.setErrorEnabled(false);
        tilRegFname.setError(null);
        focusView = null;
        break;
      case CHECKEMAIL:
        tilRegEmail.setErrorEnabled(false);
        tilRegEmail.setError(null);
        focusView = null;
        break;
      case CHECKPASSWORD:
        tilRegPaswrd.setErrorEnabled(false);
        tilRegPaswrd.setError(null);
        focusView = null;
        break;
           /* case 5:
                til_reg_BusinessName.setErrorEnabled(false);
                til_reg_BusinessName.setError(null);
                focusView = null;
                break;*/
    }
  }

  @Override
  public void setDateOfBirth(String dateOfBirth) {
    etRegDob.setText(dateOfBirth);
  }

  /**
   * this is to request the focus view
   *
   * @param tilRegFname is a TextInputLayout
   * @param error       Error Message
   */
  private void focusVieWRequestFocus(TextInputLayout tilRegFname, String error) {
    focusView = tilRegFname;
    //  focusView.requestFocus();
    tilRegFname.setErrorEnabled(true);
    tilRegFname.setError(error);

  }

  /**
   * Uploading the file to server
   *
   * @param
   */
  @SuppressLint("StaticFieldLeak")
  private class UploadFileToServer extends AsyncTask<String, Integer, String[]> {

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
    }

    @Override
    protected String[] doInBackground(String... params) {
      String result = "";
      String responseCode = "";
      String[] responseWithCode = new String[]{result, responseCode};

      try {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(20, TimeUnit.SECONDS);
        builder.readTimeout(20, TimeUnit.SECONDS);
        builder.writeTimeout(20, TimeUnit.SECONDS);
        OkHttpClient httpClient = builder.build();
        httpClient.readTimeoutMillis();


        RequestBody requestBody = new MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("uploadTo", "1")
            .addFormDataPart("folder", Constants.SIGNUPPROFILEIMAGESFOLDER)
            .addFormDataPart("file", "Filename.png",
                RequestBody.create(MediaType.parse("*/*"), new File(params[0])))
            .build();

        Request request = new Request.Builder()
            .url(BuildConfig.BASE_URL + "imageUpload")
            .post(requestBody)
            .build();

        Response response = httpClient.newCall(request).execute();
        result = response.body().string();
        responseCode = String.valueOf(response.code());
        responseWithCode[0] = responseCode;
        responseWithCode[1] = result;

        Log.d("Shijen", "doInBackground: " + responseCode);
        Log.d("Shijen", "doInBackground: " + result);

      } catch (Exception e) {
        e.printStackTrace();
      }

      return responseWithCode;
    }

    @Override
    protected void onPostExecute(String[] result) {
      try {
        if (result[0].equals(String.valueOf(Constants.SUCCESS_RESPONSE))) {
          JSONObject jsonObject = new JSONObject(result[1]);
          JSONObject jsonObject1 = jsonObject.getJSONObject("data");
          String image = jsonObject1.getString("imageUrl");
          profilePicUrl = image;
          Log.d(TAG, "onPostExecute: " + image + "   " + result.length);
        }

      } catch (JSONException e) {
        e.printStackTrace();
      }
      super.onPostExecute(result);
    }

  }
    /* private void UploadFileToServer(File file) {

      LSPServices  mLSPServices = BasicAuthentication.ServiceGenerator.createService(LSPServices.class,
                BASE_AUTH_USERNAME,
                BASE_AUTH_PASSWORD);


        RequestBody reqFile = RequestBody.create(MultipartBody.FORM, file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), reqFile);
        RequestBody uploadTo = RequestBody.create(MultipartBody.FORM, "1");
        RequestBody folder = RequestBody.create(MultipartBody.FORM, Constants.SIGNUPPROFILEIMAGESFOLDER);


        Log.d("TAG", "launchUploadActivity: "+file.getName() +" reqfile "+reqFile.toString()+" body "+body.toString()+" descrip ");

         // finally, execute the request
         Call<ResponseBody> call = mLSPServices.image(body,uploadTo,folder);//description
         call.enqueue(new Callback<ResponseBody>() {
             @Override
             public void onResponse(Call<ResponseBody> call,
                                    Response<ResponseBody> response)
             {

                 int code = response.code();
                 Log.v("Upload", "success "+code);

                 try {

                     if (response.isSuccessful()) {

                         String responseBody = response.body().string();
                         Log.d("TAG", "onResponse: "+responseBody);
                         JSONObject jsonObject = new JSONObject(responseBody);
                         String image = jsonObject.getString("data");
                         profilePicUrl = image;
                     }else

                     {
                         String errorBody = response.errorBody().string();
                         Log.d("TAG", "onResponse: "+errorBody);
                     }

                 } catch (JSONException e) {
                     e.printStackTrace();
                 } catch (IOException e) {
                     e.printStackTrace();
                 }
             }

             @Override
             public void onFailure(Call<ResponseBody> call, Throwable t) {
                 Log.e("Upload error:", t.getMessage());
             }
         });
    }*/


  @Override
  public void checkForVersion()
  {
    try {
      CryptographyExample cryptographyExample = new CryptographyExample();
      // Generate keys
      VirgilCrypto crypto = new VirgilCrypto();
      VirgilKeyPair keyPair = cryptographyExample.keyGeneration();
      // Export private key
      exportedPrivateKey = cryptographyExample.exportPrivateKey(keyPair, crypto);
      // Export public key
      exportedPublicKey = cryptographyExample.exportPublicKey(keyPair, crypto);

      mSessionManager.setVirgilPublicKeyT(exportedPublicKey);
      mSessionManager.setVirgilPrivateKeyT(exportedPrivateKey);
    } catch (CryptoException e) {
      e.printStackTrace();
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      mSignUpPresenter.generateEncryptionPrivateKeyPostLollipop(exportedPrivateKey);
      mSignUpPresenter.generateEncryptionPublicKeyPostLollipop(exportedPublicKey);
    }
    else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2){
      mSignUpPresenter.generateEncryptionPrivateKeyPreMarshMallow(exportedPrivateKey);
      mSignUpPresenter.generateEncryptionPublicKeyPreMarshMallow(exportedPublicKey);
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if(mSessionManager.getVirgilPrivateKeyT() != null && !mSessionManager.getVirgilPrivateKeyT().isEmpty() &&mSessionManager.getVirgilPublicKeyT() != null && !mSessionManager.getVirgilPublicKeyT().isEmpty())
    mSessionManager.clearVirgilPrivatePublicKeyT();
  }
}

