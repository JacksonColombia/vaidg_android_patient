/*
 *
 *  * Copyright (C) 2014 Antonio Leiva Gordillo.
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.vaidg.signup;

import android.content.Intent;


/**
 * <h2>SignUpView</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public interface SignUpView {

  /**
   * <h2>showProgress</h2>
   * This method is used to show progress bar on call of APIS
   *
   * @param text_message message to show on ProgressBar
   */
  void showProgress(String text_message);

  /**
   * <h2>hideProgress</h2>
   * This method is used to hide progress bar after the call of API success
   */
  void hideProgress();

  /**
   * <h2>setFirstNameError</h2>
   * This method is used to show error if the first name feild is empty
   */
  void setFirstNameError();

  /**
   * <h2>setLastNameError</h2>
   * This method is used to show error if the last name feild is empty
   */
  void setLastNameError();

  /**
   * <h2>setEmailError</h2>
   * This method is used to show error if the email feild is empty
   */
  void setEmailError();

  /**
   * <h2>setPasswordError</h2>
   * This method is used to show error if the password feild is empty
   */
  void setPasswordError();

  /**
   * <h2>setMobileError</h2>
   * This method is used to show error if the mobile feild is empty
   */
  void setMobileError();

  /**
   * <h2>setEmailInvalid</h2>
   * This method is used to show error if the email pattern has been written in wrong way
   *
   * @param emailErrorMsg error message to show
   */
  void setEmailInvalid(String emailErrorMsg);

  //  void setEmailErrorMsg(String message);

  /**
   * <h2>setMobileErrorMsg</h2>
   * This method is used to set Mobile Error message
   *
   * @param mobileErrorMsg error message to show
   */
  void setMobileErrorMsg(String mobileErrorMsg);

  /**
   * <h2>setMobileInvalid</h2>
   * This method is used to show error if enter mobile number is not valid
   */
  void setMobileInvalid();

  /**
   * <h2>setPasswordInvalid</h2>
   * This method is used to show error if the password pattern has been written in wrong way
   */
  void setPasswordInvalid();

  /**
   * <h2>setDateOfBirth</h2>
   * This method is used to set Error message from API calls
   *
   * @param msg error message to show
   */
  void onError(String msg);

  /**
   * <h2>navigateToOtp</h2>
   * This method is used to navigate to the OtpActivity screen
   */
  void navigateToOtp();

  /**
   * <h2>openGoogleActivity</h2>
   * This method is used to set the data of facebook login.
   *
   * @param firstName first name which is on facebook
   * @param lastName  last name which is on facebook
   * @param email      email which is on facebook
   * @param phone      phone which is on facebook
   * @param profilePic profilePic which is on facebook
   */
  void setFieldsFromFB(String firstName, String lastName, String email, String phone,
      String profilePic);

  /**
   * <h2>openGoogleActivity</h2>
   * This method is used to open the google login activity.
   *
   * @param intent Intent to open the google
   */
  void openGoogleActivity(Intent intent);

  /**
   * <h2>onReferral</h2>
   * This method is used to show referral code on call of APIS
   *
   * @param txtreferralCode Referral code text to show
   */
  void onReferral(String txtreferralCode);

  /**
   * <h2>onLoginSuccess</h2>
   * This method is used to navigate to the HomeScreen after the success of the Login API CALL,
   * and save the auth token to the Account via AccountManager of ANDROID
   *
   * @param auth     Auth token to store in Account
   * @param email    email to store in Account
   * @param password Password used to store in Account via AccountManager
   */
  void onLoginSuccess(String auth, String email, String password);

  /**
   * <h2>showPro</h2>
   * This method is used to show progress bar on call of APIS
   */
  void showPro();

  /**
   * <h2>onBusinessNameIsEmpty</h2>
   * This method is used to handle flag of validation
   */
  void onBusinessNameIsEmpty();

  /**
   * <h2>clearError</h2>
   * This method is used to clear rhe error
   *
   * @param i position to clear the error
   */
  void clearError(int i);


  /**
   * <h2>setDateOfBirth</h2>
   * This method is used to set Date of Birth editText
   *
   * @param dateOfBirth date of birth text
   */
  void setDateOfBirth(String dateOfBirth);


  /**
   * <h2>checkForVersion</h2>
   * This method is used to check for version of the device
   */
  void checkForVersion();


}
