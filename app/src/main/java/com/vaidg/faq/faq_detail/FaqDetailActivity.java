package com.vaidg.faq.faq_detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.vaidg.R;
import com.vaidg.databinding.ActivityFaqDetailBinding;
import com.vaidg.faq.adapter.FaqDetailAdapter;
import com.vaidg.faq.model.FaqData;
import com.vaidg.faq.model.Subcat;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.RecyclerTouchListener;

import java.util.ArrayList;

import static com.vaidg.utilities.Constants.FAQSCREEN.BUNDLE_FAQ_LIST;
import static com.vaidg.utilities.Constants.FAQSCREEN.EXTRA_COMINFROM;
import static com.vaidg.utilities.Constants.FAQSCREEN.EXTRA_LINK;
import static com.vaidg.utilities.Constants.FAQSCREEN.EXTRA_TITLE;
import static com.vaidg.utilities.Utility.isTextEmpty;
import static com.vaidg.utilities.Utility.setTextSize16Sp;

/**
 * <h2>FaqDetailActivity</h2>
 * <p>
 * Faq page where it will show the detailed descriptiom for faq.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class FaqDetailActivity extends AppCompatActivity implements
        FaqDetailAdapter.OnItemClickListener, RecyclerTouchListener.ClickListener {

    public final String TAG = FaqDetailActivity.class.getSimpleName();
    private ArrayList<Subcat> subcatList;
    private AppTypeface mAppTypeface;
    private Context mContext;
    private ActivityFaqDetailBinding binding;
    private Bundle args;
    private Intent intent;
    private FaqData mFaqData;
    private FaqDetailAdapter mFaqDetailAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFaqDetailBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        mContext = this;
        mAppTypeface = AppTypeface.getInstance(mContext);
        initialize();
    }

    /**
     * <h2>initialize</h2>
     * <p> method to initialize the views</p>
     */
    private void initialize() {
        args = getIntent().getExtras();
        setSupportActionBar(binding.toolbarLayout.toolbar);
        binding.toolbarLayout.toolbar.setElevation(0f);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0f);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.toolbarLayout.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
        binding.toolbarLayout.tvCenter.setTypeface(mAppTypeface.getHind_semiBold());
        setTextSize16Sp(mContext, binding.toolbarLayout.tvCenter);
        binding.toolbarLayout.tvCenter.setText(mContext.getResources().getString(R.string.faq));
        if (args != null) {
            mFaqData = (FaqData) args.getSerializable(BUNDLE_FAQ_LIST);
            if (mFaqData != null) subcatList = mFaqData.getSubcat();
        }
        if (subcatList != null && subcatList.size() > 0) {
            mFaqDetailAdapter = new FaqDetailAdapter(true, subcatList,
                    null, this);
            binding.rvFaq.setHasFixedSize(true);
            binding.rvFaq.setLayoutManager(new LinearLayoutManager(mContext));
            binding.rvFaq.setAdapter(mFaqDetailAdapter);
            binding.rvFaq.addOnItemTouchListener(new RecyclerTouchListener(mContext,
                    binding.rvFaq, this));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeActivity();
    }

    /**
     * <h2>closeActivity</h2>
     * <p> method for closing current page</p>
     */
    private void closeActivity() {
        finish();
        overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
    }

    @Override
    public void onItemClick(int position, ArrayList<FaqData> faqData, ArrayList<Subcat> items) {
        if (items != null && items.size() > 0) {
            intent = new Intent(FaqDetailActivity.this, WebViewActivity.class);
            intent.putExtra(EXTRA_LINK, items.get(position).getLink());
            intent.putExtra(EXTRA_TITLE, items.get(position).getName());
            intent.putExtra(EXTRA_COMINFROM, getResources().getString(R.string.faq));
        } else if (items != null && !isTextEmpty(items.get(position).getLink())) {
            intent = new Intent(FaqDetailActivity.this, WebViewActivity.class);
            intent.putExtra(EXTRA_LINK, items.get(position).getLink());
            intent.putExtra(EXTRA_TITLE, items.get(position).getName());
            intent.putExtra(EXTRA_COMINFROM, getResources().getString(R.string.faq));
        }
        startActivity(intent);
        overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
    }

    @Override
    public void onClick(View view, int position) {

    }

    @Override
    public void onLongClick(View view, int position) {

    }
}