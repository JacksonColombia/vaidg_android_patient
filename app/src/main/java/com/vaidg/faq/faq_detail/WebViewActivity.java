package com.vaidg.faq.faq_detail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

import com.vaidg.R;
import com.vaidg.databinding.ActivityWebViewBinding;
import com.vaidg.utilities.AppTypeface;

import static com.vaidg.utilities.Constants.FAQSCREEN.EXTRA_COMINFROM;
import static com.vaidg.utilities.Constants.FAQSCREEN.EXTRA_LINK;
import static com.vaidg.utilities.Constants.FAQSCREEN.EXTRA_TITLE;
import static com.vaidg.utilities.Utility.isTextEmpty;
import static com.vaidg.utilities.Utility.setTextSize16Sp;

/**
 * <h2>WebViewActivity</h2>
 * <p>
 * WebView page where it will load the url on browser.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class WebViewActivity extends AppCompatActivity {
    public final String TAG = WebViewActivity.class.getSimpleName();
    private String title = "", url = "", cominFrom = "";
    private AppTypeface appTypeface;
    private Context mContext;
    private ActivityWebViewBinding binding;
    private Intent intent;

    /**
     * <p>Setting content view</p>
     *
     * @param savedInstanceState view saved
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        binding = ActivityWebViewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        mContext = this;
        appTypeface = AppTypeface.getInstance(mContext);
        initialize();
    }

    /**
     * <h2>initialize</h2>
     * <p> method to initialize the views</p>
     */
    private void initialize() {
        intent = getIntent();
        if (intent != null) {
            url = getIntent().getStringExtra(EXTRA_LINK);
            title = getIntent().getStringExtra(EXTRA_TITLE);
            cominFrom = getIntent().getStringExtra(EXTRA_COMINFROM);
        }
        setSupportActionBar(binding.toolbarLayout.toolbar);
        binding.toolbarLayout.toolbar.setElevation(0f);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0f);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.toolbarLayout.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
        binding.toolbarLayout.tvCenter.setTypeface(appTypeface.getHind_semiBold());
        binding.tvSupportTittle.setTypeface(appTypeface.getHind_semiBold());
        setTextSize16Sp(mContext, binding.toolbarLayout.tvCenter);
        setTextSize16Sp(mContext, binding.tvSupportTittle);
        if (mContext.getResources().getString(R.string.faq).equals(cominFrom)) {
            binding.toolbarLayout.tvCenter.setText(getString(R.string.faq));
            binding.tvSupportTittle.setText(title);
            binding.tvSupportTittle.setVisibility(View.VISIBLE);
        } else {
            binding.tvSupportTittle.setVisibility(View.GONE);
            binding.toolbarLayout.tvCenter.setText(title);
        }
        binding.webView.setWebViewClient(new MyWebViewClient());
        binding.webView.setSaveFromParentEnabled(true);
        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.progBar.setVisibility(View.GONE);
        if (!isTextEmpty(url)) {
            binding.webView.loadUrl(url);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeActivity();
    }

    /**
     * <h2>closeActivity</h2>
     * <p> method for closing current page</p>
     */
    private void closeActivity() {
        finish();
        overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            binding.progBar.setVisibility(View.GONE);
            binding.progBar.setProgress(100);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            binding.progBar.setVisibility(View.VISIBLE);
            binding.progBar.setProgress(0);
            super.onPageStarted(view, url, favicon);
        }
    }
}
