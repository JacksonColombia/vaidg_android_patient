package com.vaidg.faq;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.utility.AlertProgress;
import com.vaidg.R;
import com.vaidg.databinding.ActivityFaqBinding;
import com.vaidg.faq.adapter.FaqDetailAdapter;
import com.vaidg.faq.faq_detail.FaqDetailActivity;
import com.vaidg.faq.faq_detail.WebViewActivity;
import com.vaidg.faq.model.FaqData;
import com.vaidg.faq.model.Subcat;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.RecyclerTouchListener;
import com.vaidg.utilities.SessionManagerImpl;

import java.util.ArrayList;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

import static com.vaidg.utilities.Constants.FAQSCREEN.EXTRA_COMINFROM;
import static com.vaidg.utilities.Constants.FAQSCREEN.BUNDLE_FAQ_LIST;
import static com.vaidg.utilities.Constants.FAQSCREEN.EXTRA_LINK;
import static com.vaidg.utilities.Constants.FAQSCREEN.EXTRA_TITLE;
import static com.vaidg.utilities.Utility.isNetworkAvailable;
import static com.vaidg.utilities.Utility.isTextEmpty;
import static com.vaidg.utilities.Utility.setMAnagerWithBID;
import static com.vaidg.utilities.Utility.setTextSize16Sp;

/**
 * <h2>FaqActivity</h2>
 * <p>
 * Faq page where it will show the list of faq.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class FaqActivity extends DaggerAppCompatActivity implements FaqContract.FaqView,
        RecyclerTouchListener.ClickListener, FaqDetailAdapter.OnItemClickListener {

    public final String TAG = FaqActivity.class.getSimpleName();
    @Inject
    FaqContract.FaqPresenter presenter;
    @Inject
    AlertProgress alertProgress;
    private FaqDetailAdapter mFaqAdapter;
    @Inject
    AppTypeface appTypeface;
    private ProgressDialog pDialog;
    private ArrayList<FaqData> mFaqDataArrayList;
    private Context mContext;
    private ActivityFaqBinding binding;
    private Bundle args;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFaqBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        mContext = this;
        initialize();
    }

    /**
     * <h2>initialize</h2>
     * <p> method to initialize the views</p>
     */
    private void initialize() {
        args = new Bundle();
        mFaqDataArrayList = new ArrayList<>();
        if (isNetworkAvailable(mContext))
            presenter.getFAQ(1);
        setSupportActionBar(binding.toolbarLayout.toolbar);
        binding.toolbarLayout.toolbar.setElevation(0f);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0f);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
            binding.toolbarLayout.toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
        binding.toolbarLayout.tvCenter.setTypeface(appTypeface.getHind_semiBold());
        setTextSize16Sp(mContext, binding.toolbarLayout.tvCenter);
        binding.toolbarLayout.tvCenter.setText(mContext.getResources().getString(R.string.faq));
        mFaqAdapter = new FaqDetailAdapter(false, null,
                mFaqDataArrayList, this);
        binding.rvFaq.setHasFixedSize(true);
        binding.rvFaq.addItemDecoration(new DividerItemDecoration(mContext,
                LinearLayoutManager.VERTICAL));
        binding.rvFaq.setLayoutManager(new LinearLayoutManager(mContext));
        binding.rvFaq.setAdapter(mFaqAdapter);
        binding.rvFaq.addOnItemTouchListener(new RecyclerTouchListener(mContext,
                binding.rvFaq, this));
    }

    @Override
    public void setNoFaqAvailable() {

    }

    @Override
    public void addItems(ArrayList<FaqData> faqData) {
        mFaqDataArrayList.clear();
        mFaqDataArrayList.addAll(faqData);
        mFaqAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        closeActivity();
    }

    /**
     * <h2>closeActivity</h2>
     * <p> method for closing current page</p>
     */
    private void closeActivity() {
        finish();
        overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
    }

    @Override
    public void onSessionExpired() {

    }

    @Override
    public void onLogout(String msg, SessionManagerImpl sessionManager) {
        if (alertProgress != null) {
            alertProgress.alertPositiveOnclick(this, msg, mContext.getResources().getString(R.string.logout), mContext.getResources().getString(R.string.ok),
                    isClicked -> setMAnagerWithBID(mContext, sessionManager));
        }
    }

    @Override
    public void onError(String msg) {
        if (alertProgress != null) {
            alertProgress.alertinfo(mContext, msg);
        }
    }

    @Override
    public void onConnectionError(String connectionError) {

    }

    @Override
    public void onShowProgress() {
        pDialog = alertProgress.getProcessDialog(this);
        pDialog.setMessage(mContext.getResources().getString(R.string.waitFaq));
        pDialog.setCancelable(false);
        try {
            if (pDialog != null && !pDialog.isShowing() && !isFinishing()) {
                pDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onHideProgress() {
        if (pDialog != null && !isFinishing() && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    @Override
    public void onClick(View view, int position) {

    }

    @Override
    public void onLongClick(View view, int position) {

    }

    @Override
    public void onItemClick(int position, ArrayList<FaqData> faqData, ArrayList<Subcat> items) {
        if (faqData.get(position).getSubcat() != null &&
                faqData.get(position).getSubcat().size() > 0) {
            args.putSerializable(BUNDLE_FAQ_LIST, faqData);
            intent = new Intent(mContext, FaqDetailActivity.class);
            intent.putExtras(args);
        } else if (!isTextEmpty(faqData.get(position).getLink())) {
            intent = new Intent(mContext, WebViewActivity.class);
            intent.putExtra(EXTRA_LINK, faqData.get(position).getLink());
            intent.putExtra(EXTRA_TITLE, faqData.get(position).getName());
            intent.putExtra(EXTRA_COMINFROM, mContext.getResources().getString(R.string.faq));
        }
        startActivity(intent);
        overridePendingTransition(R.anim.side_slide_out, R.anim.stay_still);
    }
}

