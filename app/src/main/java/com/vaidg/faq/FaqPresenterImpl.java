package com.vaidg.faq;

import com.google.gson.Gson;
import com.utility.RefreshToken;
import com.vaidg.faq.model.FaqResponse;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.SessionManager;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.SESSION_EXPIRED;
import static com.vaidg.utilities.Constants.SESSION_LOGOUT;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;
import static com.vaidg.utilities.Constants.selLang;
import static com.vaidg.utilities.LSPApplication.getInstance;

/**
 * <h2>FaqPresenterImpl</h2>
 * <p>
 * This class is used to call the API.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class FaqPresenterImpl implements FaqContract.FaqPresenter {

    public final String TAG = FaqPresenterImpl.class.getSimpleName();
    @Inject
    FaqContract.FaqView view;
    @Inject
    CompositeDisposable compositeDisposable;
    @Inject
    LSPServices lspServices;
    @Inject
    Gson gson;
    @Inject
    SessionManager sessionManager;

    @Inject
    FaqPresenterImpl() {
    }

    @Override
    public void getFAQ(int userType) {
        onShowProgress();
        Observable<Response<ResponseBody>> response =
                lspServices.getFAQ(getInstance().getAuthToken(sessionManager.getSID()),
                        selLang, PLATFORM_ANDROID, userType);
        response.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {

                    @Override
                    public void onSubscribe(@NotNull Disposable d) {
                    }

                    @Override
                    public void onNext(@NotNull Response<ResponseBody> responseBodyResponse) {
                        int code = responseBodyResponse.code();
                        try {
                            String response = responseBodyResponse.body() != null ?
                                    responseBodyResponse.body().string() : null;
                            String errorBody = responseBodyResponse.errorBody() != null ?
                                    responseBodyResponse.errorBody().string() : null;
                            switch (code) {
                                case SUCCESS_RESPONSE:
                                    if (response != null && !response.isEmpty()) {
                                        FaqResponse FaqResponse = gson.fromJson(response,
                                                FaqResponse.class);
                                        if (FaqResponse.getData() != null
                                                && FaqResponse.getData().size() > 0) {
                                            if (view != null) view.addItems(FaqResponse.getData());
                                        } else {
                                            if (view != null) view.setNoFaqAvailable();
                                        }
                                    }
                                    onHideProgress();
                                    break;
                                case SESSION_LOGOUT:
                                    onLogout(errorBody);
                                    break;
                                case SESSION_EXPIRED:
                                    /*ErrorHandel errorHandel = gson.fromJson(response,
                                     ErrorHandel.class);*/
                                    RefreshToken.onRefreshToken(
                                            getInstance().getAuthToken(sessionManager.getSID()),
                                            sessionManager.getREFRESHAUTH(),
                                            lspServices,
                                            new RefreshToken.RefreshTokenImple() {
                                                @Override
                                                public void onSuccessRefreshToken(String newToken) {
                                                    onHideProgress();
                                                    getInstance()
                                                            .setAuthToken(sessionManager.getSID(),
                                                                    sessionManager.getSID(),
                                                                    newToken);
                                                    getFAQ(userType);
                                                }

                                                @Override
                                                public void onFailureRefreshToken() {
                                                    onHideProgress();
                                                }

                                                @Override
                                                public void sessionExpired(String msg) {
                                                    onLogout(msg);
                                                }
                                            });
                                    break;
                                default:
                                    onErrorMsg(errorBody);
                                    break;
                            }
                        } catch (IOException | NumberFormatException e) {
                            e.printStackTrace();
                            onHideProgress();
                        }
                    }

                    @Override
                    public void onError(@NotNull Throwable e) {
                        onHideProgress();
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        onHideProgress();
                    }
                });
    }

    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }

    @Override
    public void onHideProgress() {
        if (view != null) view.onHideProgress();
    }

    @Override
    public void onShowProgress() {
        if (view != null) view.onShowProgress();
    }

    @Override
    public void onLogout(String errorBody) {
        onHideProgress();
        try {
            if (errorBody != null && !errorBody.isEmpty()) {
                JSONObject jsonObject = new JSONObject(errorBody);
                if (!jsonObject.getString(MESSAGE).isEmpty()) {
                    if (view != null) view.onLogout(jsonObject.getString(MESSAGE), sessionManager);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorMsg(String errorBody) {
        onHideProgress();
        try {
            if (errorBody != null && !errorBody.isEmpty()) {
                JSONObject jsonObject = new JSONObject(errorBody);
                if (!jsonObject.getString(MESSAGE).isEmpty()) {
                    if (view != null) view.onError(jsonObject.getString(MESSAGE));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
