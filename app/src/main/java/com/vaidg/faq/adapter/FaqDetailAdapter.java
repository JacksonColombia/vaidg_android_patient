package com.vaidg.faq.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.databinding.ItemFaqListBinding;
import com.vaidg.faq.FaqActivity;
import com.vaidg.faq.model.FaqData;
import com.vaidg.faq.model.Subcat;
import com.vaidg.utilities.AppTypeface;

import java.util.ArrayList;

import static com.vaidg.utilities.Utility.setTextSize14Sp;

/**
 * <h2>FaqDetailAdapter</h2>
 * <p>
 * Adapter for FaqActivity.
 * </p>
 *
 * @author 3Embed.
 * @version 1.0
 * @since 29-11-2019.
 */
public class FaqDetailAdapter extends RecyclerView.Adapter<FaqDetailAdapter.ItemViewHolder> {
    private final boolean isFaqDetails;
    private final ArrayList<Subcat> items;
    private final ArrayList<FaqData> faqData;
    private final OnItemClickListener listener;
    private Context mContext;
    private AppTypeface mAppTypeface;

    public FaqDetailAdapter(boolean isFaqDetails, ArrayList<Subcat> items,
                            ArrayList<FaqData> faqData, OnItemClickListener listener) {
        this.isFaqDetails = isFaqDetails;
        this.items = items;
        this.faqData = faqData;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemFaqListBinding binding =
                ItemFaqListBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
        mContext = parent.getContext();
        return new ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        if (isFaqDetails)
            holder.bindSubCat(items, items.get(position));
        else
            holder.bindFaqData(faqData, faqData.get(position));
    }

    @Override
    public int getItemCount() {
        if (isFaqDetails)
            return items == null ? 0 : items.size();
        else
            return faqData == null ? 0 : faqData.size();
    }

    public interface OnItemClickListener {
        /**
         * <h2>onItemClick</h2>
         * This method is used to delete the selected address.
         */
        void onItemClick(int position, ArrayList<FaqData> faqData, ArrayList<Subcat> items);
    }

    /**
     * <h2>ItemViewHolder</h2>
     * <p>
     * This itemViewHolder class hold the item state for
     *
     * @author 3Embed.
     * @version 1.0
     * @see FaqActivity , and handle the view click event.
     * </p>
     * @since 29-11-2019.
     */
    class ItemViewHolder extends RecyclerView.ViewHolder {
        private final ItemFaqListBinding binding;

        public ItemViewHolder(ItemFaqListBinding itemView) {
            super(itemView.getRoot());
            this.binding = itemView;
            mAppTypeface = AppTypeface.getInstance(mContext);
            binding.tvFaqName.setTypeface(mAppTypeface.getHind_medium());
            setTextSize14Sp(mContext, binding.tvFaqName);
        }

        public void bindSubCat(ArrayList<Subcat> items, Subcat subcat) {
            binding.tvFaqName.setText(subcat.getName());
            binding.tvFaqName.setOnClickListener(view -> {
                if (items != null) {
                    if (listener != null) listener.onItemClick(getAdapterPosition(),
                            faqData, items);
                }
            });
        }

        public void bindFaqData(ArrayList<FaqData> faqDataList, FaqData faqData) {
            binding.tvFaqName.setText(faqData.getName());
            binding.tvFaqName.setOnClickListener(view -> {
                if (listener != null) listener.onItemClick(getAdapterPosition(),
                        faqDataList, items);
            });
        }
    }
}
