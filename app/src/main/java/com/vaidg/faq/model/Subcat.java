
package com.vaidg.faq.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h2>Subcat</h2>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class Subcat implements Serializable {

    @SerializedName("Name")
    @Expose
    private String name;
   /* @SerializedName("desc")
    @Expose
    private String desc;*/
    @SerializedName("link")
    @Expose
    private String link;

    public String getName() {
        return name;
    }

   /* public String getDesc() {
        return desc;
    }*/

    public String getLink() {
        return link;
    }
}