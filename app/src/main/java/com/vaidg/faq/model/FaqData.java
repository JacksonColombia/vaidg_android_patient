package com.vaidg.faq.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * <h2>FaqData</h2>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public class FaqData implements Parcelable {
    /*
    {
    "Name":"How does this benefit the patient?",
    "subcat":[],
    "desc":"<p><span style=\"font-size:22px\">– He/She is able to find the relevant doctor & establishment for his/her problem.<br />\r\n– He/She is able to get the best visit experience when he/she visits the clinic & hospital.<br />\r\n– He/She is compensated by VaidG in case of a poor experience and for time spent beyond the guarantee.</span></p>\r\n",
    "link":"https://admin.vaidg.com/index.php?/utilities/getDescription/1/en"
    }
    */
    public final static Parcelable.Creator<FaqData> CREATOR = new Creator<FaqData>() {

        @SuppressWarnings({"unchecked"})
        public FaqData createFromParcel(Parcel in) {
            return new FaqData(in);
        }

        public FaqData[] newArray(int size) {
            return (new FaqData[size]);
        }
    };

    @SerializedName("Name")
    @Expose
    public String name;
    @SerializedName("subcat")
    @Expose
    public ArrayList<Subcat> subcat;
    @SerializedName("desc")
    @Expose
    public String desc;
    @SerializedName("link")
    @Expose
    public String link;

    protected FaqData(Parcel in) {
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.subcat, (Subcat.class.getClassLoader()));
        this.desc = ((String) in.readValue((String.class.getClassLoader())));
        this.link = ((String) in.readValue((String.class.getClassLoader())));
    }

    public FaqData() {
    }

    public static Creator<FaqData> getCREATOR() {
        return CREATOR;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);
        dest.writeList(subcat);
        dest.writeValue(desc);
        dest.writeValue(link);
    }

    public int describeContents() {
        return 0;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Subcat> getSubcat() {
        return subcat;
    }

    public String getDesc() {
        return desc;
    }

    public String getLink() {
        return link;
    }
}