package com.vaidg.faq.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * <h2>FaqResponse</h2>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */

public class FaqResponse implements Parcelable {

    public final static Parcelable.Creator<FaqResponse> CREATOR = new Creator<FaqResponse>() {

        @SuppressWarnings({"unchecked"})
        public FaqResponse createFromParcel(Parcel in) {
            return new FaqResponse(in);
        }

        public FaqResponse[] newArray(int size) {
            return (new FaqResponse[size]);
        }
    };

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public ArrayList<FaqData> data;

    protected FaqResponse(Parcel in) {
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.data, (FaqData.class.getClassLoader()));
    }

    public FaqResponse() {
    }

    public static Creator<FaqResponse> getCREATOR() {
        return CREATOR;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeList(data);
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<FaqData> getData() {
        return data;
    }

    public int describeContents() {
        return 0;
    }
}