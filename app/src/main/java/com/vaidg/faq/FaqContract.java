package com.vaidg.faq;

import com.vaidg.faq.model.FaqData;
import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;

import java.util.ArrayList;

/**
 * <h2>FaqContract</h2>
 * <p>
 * This class is used to act as a link between view and presenter.
 * </p>
 *
 * @author 3Embed
 * @version 1.0
 * @since 29-11-2019
 */
public interface FaqContract {

    interface FaqPresenter extends BasePresenter {

        /**
         * <h2>getFAQ</h2>
         * <p>This method is used to get the faq</p>
         */
        void getFAQ(int userType);

        /**
         * <h2>onHideProgress</h2>
         * <p>This method is used to hide the loading view</p>
         */
        void onHideProgress();

        /**
         * <h2>onShowProgress</h2>
         * <p>This method is used to show the loading view</p>
         */
        void onShowProgress();

        /**
         * <h2>onLogout</h2>
         * <p>This method is used to show msg if session is expired</p>
         */
        void onLogout(String errorBody);

        /**
         * <h2>onErrorMsg</h2>
         * <p>This method is used to show error msg from api calls</p>
         */
        void onErrorMsg(String errorBody);
    }

    interface FaqView extends BaseView {

        /**
         * <h2>setNoFaqAvailable</h2>
         * <p>This method is used to when there is no faq available</p>
         */
        void setNoFaqAvailable();

        /**
         * <h2>addItems</h2>
         * <p>This method is used to Add the faq items to the adapter</p>
         *
         * @param faqDataList List of items to add
         */
        void addItems(ArrayList<FaqData> faqDataList);
    }
}
