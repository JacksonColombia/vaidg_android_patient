package com.vaidg.favouriteProvider;

import android.util.Log;

import com.vaidg.utilities.LSPApplication;
import com.google.gson.Gson;
import com.vaidg.networking.LSPServices;
import com.vaidg.rateYourBooking.ResponsePojo;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;
import com.utility.RefreshToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by Ali on 7/2/2018.
 */
public class FavouriteProviderContractImpl implements FavouriteProviderContract.FavouriteProvider
{

    @Inject
    SessionManagerImpl manager;
    @Inject
    LSPServices lspServices;
    @Inject
    Gson gson;
    @Inject FavouriteProviderContract.FavouriteProviderView view;

    @Inject
    public FavouriteProviderContractImpl() {

    }

    @Override
    public void onToGetFavouriteProvider()
    {
        if(view != null)
        view.onShowProgress();
        Observable<Response<ResponseBody>> observable = lspServices.onToGetFavProvider(LSPApplication.getInstance().getAuthToken(manager.getSID()), Constants.selLang,Constants.PLATFORM_ANDROID);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {

                        int code = responseBodyResponse.code();
                        String response;
                        try
                        {
                            switch (code)
                            {
                                case Constants.SUCCESS_RESPONSE:
                                    response = responseBodyResponse.body().string();
                                    Log.d("TAG", "onNext: "+response);
                                    ResponsePojo responsePojo = gson.fromJson(response,ResponsePojo.class);
                                    if(view != null) {
                                        view.onResponseSuccess(responsePojo.getData());
                                        view.onHideProgress();
                                    }
                                    break;
                                case Constants.SESSION_LOGOUT:
                                    if(view != null) {
                                        view.onHideProgress();
                                        view.onLogout(new JSONObject(responseBodyResponse.errorBody().string()).getString("message"), manager);
                                    }
                                    break;
                                case Constants.SESSION_EXPIRED:
                                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),manager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {

                                            LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                            onToGetFavouriteProvider();
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {

                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if(view != null) {
                                                view.onHideProgress();
                                                view.onLogout(msg, manager);
                                            }

                                        }
                                    });
                                    break;
                                    default:
                                        if(view != null) {
                                            view.onHideProgress();
                                            view.onError(new JSONObject(responseBodyResponse.errorBody().string()).getString("message"));
                                        }
                                        break;
                            }
                        }catch (IOException | JSONException e)
                        {
                            if(view != null)
                                view.onHideProgress();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(view != null) {
                            view.onHideProgress();
                            view.onRetry(e.getMessage());
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }
}
