package com.vaidg.change_email;

import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.USERTYPE;

import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import com.vaidg.model.ServerResponse;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;
import javax.inject.Inject;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * @author Pramod
 * @since  12-03-2018.
 */

public class ChangeEmailPresenterImpl implements ChangeEmailPresenter {

    @Inject
    ChangeEmailView view;

    @Inject
    SessionManagerImpl manager;



    @Inject
    LSPServices lspServices;


    @Inject
    ChangeEmailPresenterImpl() {

    }

    @Override
    public boolean validateEmail(String email) {
        return email == null || email.length() == 0 || "".equals(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches() && !Patterns.DOMAIN_NAME.matcher(email).matches();
    }

    @Override
    public boolean validatePhone(String phone) {
        return TextUtils.isEmpty(phone) || "".equals(phone) || !Patterns.PHONE.matcher(phone).matches();
    }


    /**
     * <h2>changeEmail</h2>
     *     <p>This method is used to change the email id of the user.
     *     API CALL for changing the email address of a user.</p>
     * @param emailId emailId to be changed.
     */
    @Override
    public void changeEmail(final String emailId) {
        //userType :- 1 - slave, 2 - master
      Map<String,Object> stringObjectMap = new HashMap<>();
      stringObjectMap.put(USERTYPE,"1");
      stringObjectMap.put("email",emailId);

        Observable<Response<ServerResponse>> response = lspServices.changeEmail(LSPApplication.getInstance().getAuthToken(manager.getSID()), Constants.selLang,PLATFORM_ANDROID,stringObjectMap);
        response.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ServerResponse>>() {
                  @Override
                  public void onSubscribe(Disposable d) {

                  }
                    @Override
                    public void onNext(Response<ServerResponse> value) {
                        switch (value.code()) {
                            case 200:
                                if(view != null) {
                                  Log.e("CHG_EMAIL", "Value : " + value.body().getMessage());
                                  view.setSuccessEmail();
                                  view.navToProfile();
                                }
                               // manager.setEmail(emailId);
                                break;
                            default:
                                try {
                                    if (value.errorBody() != null) {
                                      JSONObject errJson = new JSONObject(value.errorBody().string());
                                      if (view != null) {
                                        view.setError(errJson.getString("message"));
                                      }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                      if(view != null) {
                        view.hideProgress();
                      }
                    }

                    @Override
                    public void onComplete() {
                        //compositeDisposable.dispose();
                    }
                });
    }


    /**
     * <h2>changeMobile</h2>
     *     <p>This method is used to change the mobile number of the user.
     *     API CALL for changing the mobile number of a user.</p>
     * @param regCountryCodeName
     * @param countryCode countryCode of mobile number to be changed.
     * @param mobile mobile number to be changed.
     */
    @Override
    public void changeMobile(String regCountryCodeName, String countryCode, String mobile) {
        //userType :- 1 - slave, 2 - master

      Map<String, Object> jsonParams = new HashMap<>();
      jsonParams.put("phone", mobile);
      jsonParams.put("countryCode", countryCode);
      jsonParams.put("countrySymbol", regCountryCodeName);
      jsonParams.put(USERTYPE, 1);
        Observable<Response<ServerResponse>> response = lspServices.changePhoneNo(LSPApplication.getInstance().getAuthToken(manager.getSID()),Constants.selLang, PLATFORM_ANDROID,jsonParams);
        response.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ServerResponse>>() {

                  @Override
                  public void onSubscribe(Disposable d) {

                  }
                    @Override
                    public void onNext(Response<ServerResponse> value) {
                        switch (value.code()) {
                            case 200:
                                Log.e("CHG_PHONE", "Value : " + value.body().getMessage());
                                //view.setError(value.body().getMessage());
                              if(view != null) {
                                view.navToOTP();
                              }
                                break;
                            default:
                                try {
                                    if (value.errorBody() != null) {
                                      JSONObject errJson = new JSONObject(value.errorBody().string());
                                      if (view != null) {
                                        view.setError(errJson.getString("message"));
                                      }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                break;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                      if(view != null) {
                        view.hideProgress();
                      }
                    }

                    @Override
                    public void onComplete() {
                        //verDisposable.dispose();
                    }
                });
    }
}
