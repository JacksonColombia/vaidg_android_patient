package com.vaidg.addTocart;

import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.SESSION_EXPIRED;

import android.util.Log;

import com.vaidg.utilities.LSPApplication;
import com.google.gson.Gson;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.SessionManagerImpl;
import com.pojo.CartModifiedData;
import com.pojo.ErrorHandel;
import com.pojo.ServiceResponse;
import com.utility.RefreshToken;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

import retrofit2.Response;

/**
 * <h2>AddToCartPresenterImpl</h2>
 * Created by Ali on 2/7/2018.
 */

public class AddToCartPresenterImpl implements AddToCartContractor.presenter
{
    private String TAG = AddToCartPresenterImpl.class.getSimpleName();

    @Inject
    SessionManagerImpl manager;

    @Inject
    LSPServices lspServices;

    @Inject AddToCartContractor.ContractView view;

    @Inject
    Gson gson;

    @Inject
    AddToCartPresenterImpl()
    {

    }
    private ArrayList<ServiceResponse.ServiceDataResponse> serviceResponse = new ArrayList<>();

    @Override
    public void attachView(Object view) {

    }

    @Override
    public void detachView() {

    }


    @Override
    public void addSubCartData(String catId, String serviceId, int action, final int serviceType)
    {
        Map<String,Object> stringObjectMap =new HashMap<>();
        stringObjectMap.put("serviceType",Constants.serviceSelected);
        stringObjectMap.put("bookingType",Constants.bookingType);
        stringObjectMap.put("categoryId",catId);
        stringObjectMap.put("serviceId",serviceId);
        stringObjectMap.put("quntity",1);
        stringObjectMap.put("action",action);
        stringObjectMap.put("providerId",Constants.proId);
        stringObjectMap.put("callType",Constants.callTypeInOutTele);


        Observable<Response<ResponseBody>> observable = lspServices.onCatModification(LSPApplication.getInstance().getAuthToken(manager.getSID()),
            Constants.selLang,PLATFORM_ANDROID,stringObjectMap);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse)
                    {
                        int code = responseBodyResponse.code();
                        String response;
                        try {
                            switch (code)
                            {
                                case Constants.SUCCESS_RESPONSE:

                                     response = responseBodyResponse.body().string();
                                    Log.d(TAG, "onNextAddToCart: "+response );
                                    CartModifiedData cartModifiedData = gson.fromJson(response,CartModifiedData.class);
                                    Constants.serviceSelected = cartModifiedData.getData().getServiceType();
                                    if(Constants.serviceSelected==1)
                                    {if(view != null) {
                                        view.onCartModifiedSuccess(cartModifiedData.getData());
                                        view.removeHourly();
                                    }
                                    }else {
                                        if(view != null) {
                                            view.addHourly(cartModifiedData.getData());
                                            view.removeFixed();
                                        }
                                    }
                                    if(view != null) {
                                        view.onHideProgress();
                                    }
                                    break;

                                case Constants.SESSION_LOGOUT:
                                    response = responseBodyResponse.errorBody().string();
                                    if(view != null) {
                                        view.onLogout(new JSONObject(response).getString("message"), manager);
                                        view.onHideProgress();
                                    }
                                    break;
                                case SESSION_EXPIRED:
                                    response = responseBodyResponse.errorBody().string();
                                    ErrorHandel errorHandel = gson.fromJson(response, ErrorHandel.class);
                                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),manager.getREFRESHAUTH(),lspServices, new RefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken)
                                        {
                                            LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                            addSubCartData(catId, serviceId, action, serviceType);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {

                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if(view != null) {
                                                view.onLogout(msg, manager);
                                                view.onHideProgress();
                                            }
                                        }
                                    });

                                    break;
                                    default:
                                        response = responseBodyResponse.errorBody().string();
                                        if(view != null) {
                                            view.onError(new JSONObject(response).getString("message"));
                                            view.onHideProgress();
                                        }
                                        break;
                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                            if(view != null) {
                                view.onHideProgress();
                                view.onError(e.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(view != null) {
                            view.onHideProgress();
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void onSubServiceApiCalled(final String catId, final String proId)
    {

        Log.d(TAG, "onSubServiceApiCalled: "+LSPApplication.getInstance().getAuthToken(manager.getSID()) +" lang "+Constants.selLang
                +" catId "+catId+" proId "+proId);
        Observable<Response<ResponseBody>> observables = lspServices.getSubServices(LSPApplication.getInstance().getAuthToken(manager.getSID()),
                Constants.selLang,PLATFORM_ANDROID,catId,proId);

        observables.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse)
                    {
                        String responseBody;
                        JSONObject jsonObject;
                        int code = responseBodyResponse.code();

                        try {

                            Log.d(TAG, "onNextOnSubServiceApiCalled: "+code);
                            switch (code)
                            {
                                case Constants.SUCCESS_RESPONSE:
                                    responseBody = responseBodyResponse.body().string();
                                    Log.d(TAG, "onNextOnSubServiceApiCalledRes: "+responseBody);
                                    ServiceResponse response = gson.fromJson(responseBody,ServiceResponse.class);
                                    serviceResponse.addAll(response.getData());
                                    if(view != null) {
                                        view.onSuccessSubService(response.getData());
                                        view.onHideProgress();
                                    }
                                    break;
                                case Constants.SESSION_LOGOUT:
                                    responseBody = responseBodyResponse.errorBody().string();
                                    jsonObject = new JSONObject(responseBody);
                                    if(view != null) {
                                        view.onLogout(jsonObject.getString("message"), manager);
                                        view.onHideProgress();
                                    }
                                    break;
                                case SESSION_EXPIRED:
                                    responseBody = responseBodyResponse.errorBody().string();
                                    ErrorHandel errorHandel = gson.fromJson(responseBody, ErrorHandel.class);
                                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),manager.getREFRESHAUTH(),lspServices, new RefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken)
                                        {
                                            LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                            onSubServiceApiCalled(catId,proId);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {

                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            if(view != null) {
                                                view.onLogout(msg, manager);
                                                view.onHideProgress();
                                            }
                                        }
                                    });

                                    break;
                                default:
                                    responseBody = responseBodyResponse.errorBody().string();
                                    if(view != null) {
                                        view.onError(new JSONObject(responseBody).getString("message"));
                                        view.onHideProgress();
                                    }
                                    break;

                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                            if(view != null) {
                                view.onHideProgress();
                                view.onError(e.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(view != null) {
                            view.onHideProgress();
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }


    @Override
    public void getCartServiceCall(final String catId)
    {

        Log.d(TAG, "getCartServiceCall: "+catId);

        Observable<Response<ResponseBody>> observable = lspServices.getSubCart(LSPApplication.getInstance().getAuthToken(manager.getSID()),
                Constants.selLang,PLATFORM_ANDROID,catId,Constants.proId,Constants.callTypeInOutTele,Constants.bookingType);
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {

                        int code = responseBodyResponse.code();
                        Log.d(TAG, "onNextIntCode: "+code);
                        String response;
                        try {
                            switch (code)
                            {
                                case Constants.SUCCESS_RESPONSE:
                                     response = responseBodyResponse.body().string();
                                    Log.d(TAG, "onNextgetAddedCart: "+response+" size "+serviceResponse);
                                    CartModifiedData cartModifiedData = gson.fromJson(response,CartModifiedData.class);
                                    if(cartModifiedData.getData().getServiceType()==1){
                                        if(view != null) {
                                            view.onAlreadyAddedCart(cartModifiedData.getData(), serviceResponse, true);
                                        }}   else{
                                        if(view != null) {
                                            view.onAlreadyAddedCart(cartModifiedData.getData(), serviceResponse, false);
                                        }}
                                     Constants.serviceSelected = cartModifiedData.getData().getServiceType();
                                    break;

                                case SESSION_EXPIRED:
                                    String responseError = responseBodyResponse.errorBody().string();
                                    ErrorHandel errorHandel = gson.fromJson(responseError,ErrorHandel.class);
                                    RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),manager.getREFRESHAUTH(), lspServices, new RefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {

                                            LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                            getCartServiceCall(catId);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            if(view != null) {
                                                view.onLogout("", manager);
                                                view.onHideProgress();
                                            }
                                        }

                                        @Override
                                        public void sessionExpired(String msg)
                                        {    if(view != null) {
                                            view.onLogout(msg, manager);
                                            view.onHideProgress();
                                        }
                                        }
                                    });
                                    break;
                                case Constants.SESSION_LOGOUT:
                                    response = responseBodyResponse.errorBody().string();
                                    JSONObject jsonObject = new JSONObject(response);
                                    if(view != null) {
                                        view.onLogout(jsonObject.getString("message"), manager);
                                        view.onHideProgress();
                                    }
                                    break;
                                case 409:

                                    response = responseBodyResponse.errorBody().string();
                                    JSONObject jsonObjecterrCart = new JSONObject(response);
                                    Log.d(TAG, "onNextErrorResponse: "+jsonObjecterrCart.getString("message"));
                                    if(view != null) {
                                        view.onAlreadyCartPresent(jsonObjecterrCart.getString("message"), true);
                                    }
                                    break;
                                    default:
                                        response = responseBodyResponse.errorBody().string();
                                        JSONObject jsonObjecterr = new JSONObject(response);
                                        Log.d(TAG, "onNextErrorResponse: "+jsonObjecterr.getString("message"));
                                        break;

                            }
                        } catch (IOException | JSONException e)
                        {
                            e.printStackTrace();
                            if(view != null) {
                                view.onHideProgress();
                                view.onError(e.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(view != null) {
                            view.onHideProgress();
                            view.onError(e.getMessage());
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
