package com.vaidg.addTocart;

import com.vaidg.Dagger2.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>AddToCartModule</h2>
 * Created by Ali on 2/7/2018.
 */

@Module
public interface AddToCartModule
{
    @Binds
    @ActivityScoped
    AddToCartContractor.presenter provideContract(AddToCartPresenterImpl addToCartPresenter);

    @Binds
    @ActivityScoped
    AddToCartContractor.ContractView provideContractView(AddToCart addToCart);
}
