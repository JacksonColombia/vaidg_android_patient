package com.vaidg.ProviderSearch;

import androidx.core.view.MenuItemCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.vaidg.R;
import com.vaidg.providerList.DoctorList;
import com.vaidg.providerList.adapter.DoctorListAdapter;
import com.vaidg.providerList.model.DoctorData;

import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Utility;
import java.util.ArrayList;

public class ProviderSearchActivity extends AppCompatActivity {

    ArrayList<DoctorData> providerData;
    DoctorListAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_search);
        Toolbar toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getIntentValue();
        setUpRecyclerView();
    }

    private void getIntentValue() {
        providerData = (ArrayList<DoctorData>) getIntent().getBundleExtra("BUNDLE").getSerializable(
            DoctorList.PROVIDERLIST);
    }

    public void setUpRecyclerView(){
        RecyclerView rv_searchProviders=findViewById(R.id.rv_searchProviders);
        AppTypeface mAppTypeface = AppTypeface.getInstance(this);
      //  adapter = new DoctorListAdapter(providerData, mAppTypeface, isFavDoc);
        LinearLayoutManager manager=new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rv_searchProviders.setLayoutManager(manager);
        rv_searchProviders.setAdapter(adapter);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu,menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView= (SearchView) MenuItemCompat.getActionView(item);
        searchView.setIconified(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //adapter.getFilter().filter(newText);
                return true;
            }
        });
        return true;
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        //Utility.checkAndShowNetworkError(this);
    }

}
