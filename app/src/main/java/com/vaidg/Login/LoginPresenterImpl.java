package com.vaidg.Login;

import static com.vaidg.utilities.Constants.AES_MODE;
import static com.vaidg.utilities.Constants.ANDROID_KEYSTORE;
import static com.vaidg.utilities.Constants.APPVERSION;
import static com.vaidg.utilities.Constants.APP_VERSION;
import static com.vaidg.utilities.Constants.BASE_AUTH_PASSWORD;
import static com.vaidg.utilities.Constants.BASE_AUTH_USERNAME;
import static com.vaidg.utilities.Constants.DEVICEID;
import static com.vaidg.utilities.Constants.DEVICEMODEL;
import static com.vaidg.utilities.Constants.DEVICEOSVERSION;
import static com.vaidg.utilities.Constants.DEVICETIME;
import static com.vaidg.utilities.Constants.DEVICE_MAKER;
import static com.vaidg.utilities.Constants.DEVICE_MODEL;
import static com.vaidg.utilities.Constants.DEVICE_OS_VERSION;
import static com.vaidg.utilities.Constants.DEVICE_TYPE;
import static com.vaidg.utilities.Constants.DEVMAKE;
import static com.vaidg.utilities.Constants.DEVTYPE;
import static com.vaidg.utilities.Constants.EMAIL_OR_PHONE;
import static com.vaidg.utilities.Constants.FACEBOOKID;
import static com.vaidg.utilities.Constants.FACEBOOK_LOGIN;


import static com.vaidg.utilities.Constants.FIXED_IV;
import static com.vaidg.utilities.Constants.GOOGLEID;
import static com.vaidg.utilities.Constants.GOOGLE_LOGIN;
import static com.vaidg.utilities.Constants.IV_PARAMETER_SPEC;
import static com.vaidg.utilities.Constants.KEY_PRIVATEALIAS;
import static com.vaidg.utilities.Constants.KEY_PUBLICALIAS;
import static com.vaidg.utilities.Constants.LATITUDE;
import static com.vaidg.utilities.Constants.LOGINTYPE;
import static com.vaidg.utilities.Constants.LONGITUDE;
import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.NORMAL_LOGIN;
import static com.vaidg.utilities.Constants.NOT_FOUND;
import static com.vaidg.utilities.Constants.PASSWORD;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.PUSHKITTOKEN;
import static com.vaidg.utilities.Constants.PUSH_TOKEN;
import static com.vaidg.utilities.Constants.RSA_MODE;
import static com.vaidg.utilities.Constants.SESSION_NoDues;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;
import static com.vaidg.utilities.Constants.latitude;
import static com.vaidg.utilities.Constants.longitude;
import static com.vaidg.utilities.Constants.selLang;
import static com.vaidg.utilities.Utility.getCurrentTime;

import android.annotation.SuppressLint;
import android.os.Build;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import androidx.annotation.NonNull;

import com.vaidg.BuildConfig;
import com.vaidg.Login.pojo.Data;
import com.vaidg.Login.pojo.LoginResponse;
import com.vaidg.R;
import com.vaidg.encryptiondecryption.CryptographyExample;
import com.vaidg.model.FacebookLoginPojo;
import com.vaidg.model.UserDetailsDataModel;
import com.vaidg.networking.BasicAuthentication;
import com.vaidg.networking.LSPServices;
import com.vaidg.networking.NoConnectivityException;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManager;
import com.vaidg.utilities.Utility;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.utility.AlertProgress;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilKeyPair;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;
import javax.security.auth.x500.X500Principal;
import okhttp3.ResponseBody;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * <h2>LoginPresenterImpl</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class LoginPresenterImpl implements LoginContract.LoginPresenter,
    GoogleApiClient.OnConnectionFailedListener {
  private static final String TAG = LoginPresenterImpl.class.getName();
  private static String mDeviceId = "";
  @Inject
  LoginContract.LoginView view;
  @Inject
  LoginActivity activity;
  @Inject
  FacebookLoginHelper facebookLoginHelper;
  @Inject
  LSPServices lspServices;
  @Inject
  LoginModel loginModel;
  @Inject
  SessionManager sessionManager;
  @Inject
  AlertProgress alertProgress;
  @Inject
  Gson gson;
  private KeyStore keyStore;
  private String exportedPrivateKey = "", exportedPublicKey = "";
  private UserDetailsDataModel mUserDetailsDataModel;
  private String mSocialMediaId, mProfilePic, mFirstName, mLastName;
  private GoogleApiClient mGoogleApiClient;

  @Inject
  LoginPresenterImpl() {
    this.mUserDetailsDataModel = new UserDetailsDataModel();
  }

  @Override
  public void validateUname(String uname) {
    if (loginModel.validateUserName(uname)) {
      view.setInvalidUname();
    }
  }

  @Override
  public void validateCredentials(String username, String password) {
    if (loginModel.emptyUname(username)) {
      view.setUsernameError();
    } else if (loginModel.validateUserName(username)) {
      view.setInvalidUname();
    } else if (loginModel.validatePassword(password)) {
      view.setPasswordError();
    } else {
      normalLogin(username, password);
    }
  }

  /**
   * <h2>doLogin</h2>
   * <p> Calling login service and if success storing values in session manager and start main
   * activity </p>
   * login_type -> 1- normal login, 2- Fb , 3-google
   *
   * @param username: email id / phone number of user
   * @param password: password to login
   */
  @Override
  public void doLogin(final String username, String password) {
    if (view != null) {
      view.onShowProgress();
    }
    try {
      mDeviceId = Utility.getDeviceId(this.activity);
    } catch (Exception e) {
      e.printStackTrace();
      view.onHideProgress();
    }
    boolean flag_guest_login = sessionManager.getGuestLogin();
    if (flag_guest_login) {
      view.revokeGuestLogin(mDeviceId);
      LSPApplication.getInstance().setAuthToken(sessionManager.getSID(),sessionManager.getSID(),null);
    }
    switch (mUserDetailsDataModel.getLoginType()) {
      case FACEBOOK_LOGIN:
        password = "";
        mUserDetailsDataModel.setFacebookId(mSocialMediaId);
        break;
      case GOOGLE_LOGIN:
        password = "";
        mUserDetailsDataModel.setGoogleId(mSocialMediaId);
        break;
    }
    String deviceTime = getCurrentTime();
    deviceTime = deviceTime.replace('T', ' ');
    final String pass = password;
    lspServices = BasicAuthentication.ServiceGenerator.createService(LSPServices.class,
        BASE_AUTH_USERNAME, BASE_AUTH_PASSWORD);

    Map<String, Object> jsonParams = new HashMap<>();
    //put something inside the map, could be null*/
    jsonParams.put(EMAIL_OR_PHONE, username);
    jsonParams.put(PASSWORD, password);
    jsonParams.put(DEVICEID, mDeviceId);
    jsonParams.put(PUSH_TOKEN, "");
    jsonParams.put(APPVERSION, APP_VERSION);
    jsonParams.put(DEVMAKE, DEVICE_MAKER);
    jsonParams.put(DEVICEMODEL, DEVICE_MODEL);
    jsonParams.put(DEVTYPE, DEVICE_TYPE);
    jsonParams.put(DEVICETIME, deviceTime);
    jsonParams.put(LOGINTYPE, mUserDetailsDataModel.getLoginType());
    jsonParams.put(DEVICEOSVERSION, DEVICE_OS_VERSION);
    jsonParams.put(FACEBOOKID, mUserDetailsDataModel.getFacebookId());
    jsonParams.put(GOOGLEID, mUserDetailsDataModel.getGoogleId());
    jsonParams.put(LATITUDE, latitude);
    jsonParams.put(LONGITUDE, longitude);
    jsonParams.put(PUSHKITTOKEN, "");

    Observable<Response<ResponseBody>> responseObservable = lspServices.performLogin(
        selLang, PLATFORM_ANDROID, jsonParams);

    responseObservable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ResponseBody>>() {
          @Override
          public void onSubscribe(Disposable d) {

          }

          @Override
          public void onNext(Response<ResponseBody> responseBodyResponse) {
            int code = responseBodyResponse.code();
            JSONObject errJson;
            try {
              String responseBody = responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
              String errorBody = responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null;
              switch (code) {
                case SUCCESS_RESPONSE:
                  if (responseBody != null && !responseBody.isEmpty()) {
                    LoginResponse loginResponse = gson.fromJson(responseBody, LoginResponse.class);
                    Data loginData = loginResponse.getData();
                    String auth = loginData.getToken().getAccessToken();
                    String refreshtoken = loginData.getToken().getRefreshToken();
                    String sid = loginData.getSid();
                    String refCode = loginData.getReferralCode();

                    sessionManager.setSID(sid);
                    sessionManager.setReferralCode(refCode);
                    sessionManager.setRegisterId(loginData.getRequesterId());
                    sessionManager.setFirstName(loginData.getFirstName());
                    sessionManager.setPatientDateOfBirth(loginData.getDateOfBirth());
                    sessionManager.setPatientGender(loginData.getGender());
                    sessionManager.setLastName(loginData.getLastName());
                    sessionManager.setEmail(loginData.getEmail());
/*
                    LSPApplication.getInstance().setAuthToken(sid,
                            sid, auth);
*/
                    LSPApplication.getInstance().setAuthToken(sid, sid, auth);
                    sessionManager.setREFRESHAUTH(refreshtoken);
                    sessionManager.setMobileNo(loginData.getPhone());
                    sessionManager.setCountryCode(loginData.getCountryCode());
                    sessionManager.setCountrySymbol(loginData.getCountrySymbol());
                    sessionManager.setProfilePicUrl(loginData.getProfilePic());
                    sessionManager.setFcmTopic(loginData.getFcmTopic());
                    sessionManager.setDefaultCardId(loginData.getCardDetail().getId());
                    sessionManager.setDefaultCardNum(loginData.getCardDetail().getLast4());
                    sessionManager.setDefaultCardName(loginData.getCardDetail().getBrand());
                    sessionManager.setCallToken(loginData.getCall().getAuthToken());
                    sessionManager.setVirgilPrivateKey(loginData.getPrivateKey());
                    sessionManager.setVirgilPublicKey(loginData.getPublickKey());
                    if (!sessionManager.getFcmTopic().isEmpty()
                        || !sessionManager.getFcmTopic().equals("")) {
                      FirebaseMessaging.getInstance().subscribeToTopic(
                          sessionManager.getFcmTopic());
                    }
                    if(view != null)
                    view.onHideProgress();
                    checkForKeystore();
                    if(view != null)
                    view.onLoginSuccess(auth, username, pass);
                  }
                  break;
                case NOT_FOUND:
                  if (mUserDetailsDataModel.getLoginType() == FACEBOOK_LOGIN || mUserDetailsDataModel.getLoginType() == GOOGLE_LOGIN) {
                    if(view != null)
                    view.navToSignUp(mUserDetailsDataModel.getLoginType(),
                        mUserDetailsDataModel,
                        mGoogleApiClient);
                  } else {
                    if (errorBody != null && !errorBody.isEmpty()) {
                      errJson = new JSONObject(errorBody);
                      errJson.getString(MESSAGE);
                      if (!errJson.getString(MESSAGE).isEmpty()) {
                        if(view != null) {
                          view.onError(errJson.getString(MESSAGE));
                          view.onHideProgress();
                        }
                      }
                    }
                  }
                  break;
                case SESSION_NoDues:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    errJson = new JSONObject(errorBody);
                    errJson.getString(MESSAGE);
                    if (!errJson.getString(MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onError(errJson.getString(MESSAGE));
                        view.setEmailView(username);
                        view.onHideProgress();
                      }
                    }
                  }
                  break;
                default:
                  if (errorBody != null && !errorBody.isEmpty()) {
                    errJson = new JSONObject(errorBody);
                    errJson.getString(MESSAGE);
                    if (!errJson.getString(MESSAGE).isEmpty()) {
                      if(view != null) {
                        view.onError(errJson.getString(MESSAGE));
                        view.onHideProgress();
                      }
                    }
                  }
                  break;
              }
            } catch (IOException | JSONException e) {
              if(view != null) {
                view.onHideProgress();
                view.onConnectionError(activity.getResources().getString(R.string.pleaseCheckInternet));
              }
              e.printStackTrace();
            }
          }

          @Override
          public void onError(Throwable e) {
            if(view != null) {
              view.onHideProgress();
              view.onConnectionError(activity.getResources().getString(R.string.pleaseCheckInternet));
            }
            if (e instanceof NoConnectivityException) {
              // show No Connectivity message to user or do whatever you want.
              alertProgress.tryAgain(activity, activity.getString(
                  R.string.pleaseCheckInternet),
                  activity.getString(R.string.system_error),
                  isClicked -> {
                    if (isClicked) {
                      if(Utility.isNetworkAvailable(activity))
                        doLogin(username, pass);
                    }
                  });
            } else {
              e.printStackTrace();
            }
          }

          @Override
          public void onComplete() {
          }
        });
  }

  @Override
  public void initializeFBGoogle() {
    FacebookSdk.sdkInitialize(activity.getApplicationContext());
    GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder
        (GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestEmail()
        //.requestIdToken(REQUEST_ID_TOKEN)
        .build();
    mGoogleApiClient = new GoogleApiClient.Builder(activity)
        .enableAutoManage(activity, this)
        .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
        .build();
  }


  @Override
  public void handleLoginType(CallbackManager callbackManager) {
    switch (loginModel.getLoginType()) {
      case FACEBOOK_LOGIN:
        handleResultFromFB(callbackManager);
        break;
      case GOOGLE_LOGIN:
        googleLogin();
        break;
    }
  }

  @Override
  public void storeLoginType(int loginType) {
    loginModel.setLoginType(loginType);
  }

  @Override
  public void initializeFacebook() {
    facebookLoginHelper.initializeFacebookSdk(activity);
  }

  @Override
  public void normalLogin(String mailId, String password) {
    mUserDetailsDataModel.setLoginType(NORMAL_LOGIN);
    if(Utility.isNetworkAvailable(activity))
    doLogin(mailId, password);
  }

  @Override
  public void handleResultFromGoogle(GoogleSignInResult result) {
    String personPhoto = "";
    GoogleSignInAccount account = result.getSignInAccount();
    if ((account != null ? account.getPhotoUrl() : null) != null) {
      personPhoto = account.getPhotoUrl().toString();
    }
    mUserDetailsDataModel.setLoginType(GOOGLE_LOGIN);
    mUserDetailsDataModel.setEmailOrPhone(account != null ? account.getEmail() : null);
    mUserDetailsDataModel.setPassword(account != null ? account.getId() : null);
    mFirstName = account != null ? account.getGivenName() : null;
    mSocialMediaId = account != null ? account.getId() : null;
    mProfilePic = personPhoto;
    mUserDetailsDataModel.setFirstName(mFirstName);
    mUserDetailsDataModel.setLastName(account != null ? account.getFamilyName() : null);
    mUserDetailsDataModel.setProfilePicUrl(mProfilePic);
    if(Utility.isNetworkAvailable(activity)) {
      if (account != null) {
        doLogin(account != null ? account.getEmail() : null,
                account != null ? account.getId() : null);
      }
    }
  }

  @Override
  public void handleResultFromFB(CallbackManager callbackManager) {
    facebookLoginHelper.refreshToken();
    facebookLoginHelper.facebookLogin(callbackManager,
        facebookLoginHelper.createFacebook_requestData(),
        new FacebookLoginHelper.Facebook_callback() {
          @Override
          public void success(JSONObject json) {
            FacebookLoginPojo facebookLogin_pojo = gson.fromJson(json.toString(), FacebookLoginPojo.class);
            mUserDetailsDataModel.setLoginType(FACEBOOK_LOGIN);
            mUserDetailsDataModel.setEmailOrPhone(facebookLogin_pojo.getEmail());
            mSocialMediaId = facebookLogin_pojo.getId();
            mFirstName = facebookLogin_pojo.getFirst_name();
            mLastName = facebookLogin_pojo.getLast_name();
            mProfilePic = BuildConfig.FACEBOOK_PROFILE_URL + facebookLogin_pojo.getId() + BuildConfig.FACEBOOK_PROFILE_TYPE;
            mUserDetailsDataModel.setFirstName(mFirstName);
            mUserDetailsDataModel.setLastName(mLastName);
            mUserDetailsDataModel.setProfilePicUrl(mProfilePic);
            if(Utility.isNetworkAvailable(activity))
              doLogin(facebookLogin_pojo.getEmail(), facebookLogin_pojo.getId());
          }

          @Override
          public void error(String error) {
          }

          @Override
          public void cancel(String cancel) {
          }
        });
  }

  @Override
  public void googleLogin() {
    if (mGoogleApiClient != null) {
      Auth.GoogleSignInApi.signOut(mGoogleApiClient);
      view.openGoogleActivity(Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient));
    } else {
      view.googleClientIsNotConnected();
    }
  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
  }

  @Override
  public void attachView(Object view) {
  }

  @Override
  public void detachView() {
  }


  @Override
  public void checkForKeystore() {
    try {
      keyStore = KeyStore.getInstance(ANDROID_KEYSTORE);
      keyStore.load(null);
      view.checkForVersion();
    } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
      e.printStackTrace();
    }
    // if (!keyStore.containsAlias(KEY_PRIVATEALIAS)) {

    //}
  }

  @Override
  public void checkForVersion(CryptographyExample cryptographyExample, VirgilCrypto crypto, VirgilKeyPair keyPair) throws CryptoException{
    if (TextUtils.isEmpty(sessionManager.getVirgilPrivateKey())){
      // Export private key
      exportedPrivateKey = cryptographyExample.exportPrivateKey(keyPair, crypto);
    }else{
      // Export private key
      exportedPrivateKey = sessionManager.getVirgilPrivateKey();
    }
    if (TextUtils.isEmpty(sessionManager.getVirgilPublicKey())) {
      // Export public key
      exportedPublicKey = cryptographyExample.exportPublicKey(keyPair, crypto);
    }else{
      // Export public key
      exportedPublicKey = sessionManager.getVirgilPublicKey();
    }


    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      generateEncryptionPrivateKeyPostLollipop(exportedPrivateKey);
      generateEncryptionPublicKeyPostLollipop(exportedPublicKey);
    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
      generateEncryptionPrivateKeyPreMarshMallow(exportedPrivateKey);
      generateEncryptionPublicKeyPreMarshMallow(exportedPublicKey);
    }
  }


  /**
   * <hg2>generateAndStorePublicKeyAES</hg2>
   * This method is useed to generate the key for pre marsh mallow and store
   */
  private void generateAndStorePublicKeyAES() {
    String encryptedKeyB64 = sessionManager.getEncryptionPublicKey();
    if (encryptedKeyB64 == null) {
      byte[] key = new byte[16];
      SecureRandom secureRandom = new SecureRandom();
      secureRandom.nextBytes(key);
      byte[] encryptedKey;
      try {
        encryptedKey = rsaPublicKeyEncrypt(key);
        encryptedKeyB64 = Base64.encodeToString(encryptedKey, Base64.DEFAULT);
        sessionManager.storeEncryptionPublicKey(encryptedKeyB64);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * <hg2>generateAndStorePrivateKeyAES</hg2>
   * This method is useed to generate the key for pre marsh mallow and store
   */
  private void generateAndStorePrivateKeyAES() {
    String encryptedKeyB64 = sessionManager.getEncryptionPrivateKey();
    if (encryptedKeyB64 == null) {
      byte[] key = new byte[16];
      SecureRandom secureRandom = new SecureRandom();
      secureRandom.nextBytes(key);
      byte[] encryptedKey;
      try {
        encryptedKey = rsaPrivateKeyEncrypt(key);
        encryptedKeyB64 = Base64.encodeToString(encryptedKey, Base64.DEFAULT);
        sessionManager.storeEncryptionPrivateKey(encryptedKeyB64);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * <h2>rsaPublicKeyEncrypt</h2>
   * This method is used to do the rsa encrypt
   *
   * @param secret takes secret bytes as input
   * @return returns the RSA encrypt byts
   * @throws Exception throws an exception
   */
  private byte[] rsaPublicKeyEncrypt(byte[] secret) throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PUBLICALIAS, null);
    // Encrypt the text
    Cipher inputCipher = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
    inputCipher.init(Cipher.ENCRYPT_MODE, privateKeyEntry.getCertificate().getPublicKey());
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, inputCipher);
    cipherOutputStream.write(secret);
    cipherOutputStream.close();
    return outputStream.toByteArray();
  }

  /**
   * <h2>rsaPrivateKeyEncrypt</h2>
   * This method is used to do the rsa encrypt
   *
   * @param secret takes secret bytes as input
   * @return returns the RSA encrypt byts
   * @throws Exception throws an exception
   */
  private byte[] rsaPrivateKeyEncrypt(byte[] secret) throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PRIVATEALIAS, null);
    // Encrypt the text
    Cipher inputCipher = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
    inputCipher.init(Cipher.ENCRYPT_MODE, privateKeyEntry.getCertificate().getPublicKey());
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, inputCipher);
    cipherOutputStream.write(secret);
    cipherOutputStream.close();
    return outputStream.toByteArray();
  }


  /**
   * <h2>getSecretPrivateKeyPostLollipop</h2>
   * This method is used to get the secret key for post lollipop device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPrivateKeyPostLollipop() throws Exception {
    Key s = keyStore.getKey(KEY_PRIVATEALIAS, null);
    Log.i("TEST", "key " + s.toString());
    return keyStore.getKey(KEY_PRIVATEALIAS, null);
  }

  /**
   * <h2>getSecretPublicKeyPostLollipop</h2>
   * This method is used to get the secret key for post lollipop device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPublicKeyPostLollipop() throws Exception {
    Key s = keyStore.getKey(KEY_PUBLICALIAS, null);
    Log.i("TEST", "key " + s.toString());
    return keyStore.getKey(KEY_PUBLICALIAS, null);
  }

  /**
   * <h2>getSecretPrivateKeyPreMarshMallow</h2>
   * This method is used to get the secret key for pre marsh mallow device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPrivateKeyPreMarshMallow() throws Exception {
    String enryptedKeyB64 = sessionManager.getEncryptionPrivateKey();
    Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
    // need to check null, omitted here
    byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
    byte[] key = rsaPrivateKeyDecrypt(encryptedKey);
    Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
    return new SecretKeySpec(key, "AES");
  }

  /**
   * <h2>getSecretPublicKeyPreMarshMallow</h2>
   * This method is used to get the secret key for pre marsh mallow device
   *
   * @return returns the secret key needed to encrypt /decrypt
   * @throws Exception throws the exception to be handled
   */
  private java.security.Key getSecretPublicKeyPreMarshMallow() throws Exception {
    String enryptedKeyB64 = sessionManager.getEncryptionPublicKey();
    Log.i("TEST", "enryptedKeyB64 " + enryptedKeyB64);
    // need to check null, omitted here
    byte[] encryptedKey = Base64.decode(enryptedKeyB64, Base64.DEFAULT);
    byte[] key = rsaPublicKeyDecrypt(encryptedKey);
    Log.i("TEST", "key " + new SecretKeySpec(key, "AES"));
    return new SecretKeySpec(key, "AES");
  }


  private byte[] rsaPrivateKeyDecrypt(byte[] encrypted) throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PRIVATEALIAS, null);
    Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
    output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
    CipherInputStream cipherInputStream = new CipherInputStream(
        new ByteArrayInputStream(encrypted), output);
    ArrayList<Byte> values = new ArrayList<>();
    int nextByte;
    while ((nextByte = cipherInputStream.read()) != -1) {
      values.add((byte) nextByte);
    }

    byte[] bytes = new byte[values.size()];
    for (int i = 0; i < bytes.length; i++) {
      bytes[i] = values.get(i);
    }
    return bytes;
  }

  private byte[] rsaPublicKeyDecrypt(byte[] encrypted) throws Exception {
    KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(KEY_PUBLICALIAS, null);
    Cipher output = Cipher.getInstance(RSA_MODE, "AndroidOpenSSL");
    output.init(Cipher.DECRYPT_MODE, privateKeyEntry.getPrivateKey());
    CipherInputStream cipherInputStream = new CipherInputStream(
        new ByteArrayInputStream(encrypted), output);
    ArrayList<Byte> values = new ArrayList<>();
    int nextByte;
    while ((nextByte = cipherInputStream.read()) != -1) {
      values.add((byte) nextByte);
    }

    byte[] bytes = new byte[values.size()];
    for (int i = 0; i < bytes.length; i++) {
      bytes[i] = values.get(i);
    }
    return bytes;
  }

  @SuppressLint("NewApi")
  @Override
  public void generateEncryptionPrivateKeyPostLollipop(String exportedPrivateKey) {
    KeyGenerator keyGenerator = null;
    try {
      keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEYSTORE);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (NoSuchProviderException e) {
      e.printStackTrace();
    }
    try {
      assert keyGenerator != null;
      keyGenerator.init
          (new KeyGenParameterSpec.Builder(KEY_PRIVATEALIAS,
              KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
              .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
              .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
              .setRandomizedEncryptionRequired(false)
              .build());
    } catch (InvalidAlgorithmParameterException e) {
      e.printStackTrace();
    }
    keyGenerator.generateKey();
    createVirgilPrivateKey(exportedPrivateKey);
  }


  @SuppressLint("NewApi")
  @Override
  public void generateEncryptionPublicKeyPostLollipop(String exportedPublicKey) {
    KeyGenerator keyGenerator = null;
    try {
      keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, ANDROID_KEYSTORE);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (NoSuchProviderException e) {
      e.printStackTrace();
    }
    try {
      assert keyGenerator != null;
      keyGenerator.init
          (new KeyGenParameterSpec.Builder(KEY_PUBLICALIAS,
              KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
              .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
              .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
              .setRandomizedEncryptionRequired(false)
              .build());
    } catch (InvalidAlgorithmParameterException e) {
      e.printStackTrace();
    }
    keyGenerator.generateKey();
    createVirgilPublicKey(exportedPublicKey);
  }

  @SuppressLint("NewApi")
  @Override
  public void generateEncryptionPrivateKeyPreMarshMallow(String exportedPrivateKey) {
    // Generate a key pair for encryption
    Calendar start = Calendar.getInstance();
    Calendar end = Calendar.getInstance();
    end.add(Calendar.YEAR, 30);
    KeyPairGeneratorSpec spec;
    spec = new KeyPairGeneratorSpec.Builder(activity)
        .setAlias(KEY_PRIVATEALIAS)
        .setSubject(new X500Principal("CN=" + KEY_PRIVATEALIAS))
        .setSerialNumber(BigInteger.TEN)
        .setStartDate(start.getTime())
        .setEndDate(end.getTime())
        .build();

    KeyPairGenerator kpg = null;
    try {
      kpg = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, ANDROID_KEYSTORE);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (NoSuchProviderException e) {
      e.printStackTrace();
    }
    try {
      assert kpg != null;
      kpg.initialize(spec);
    } catch (InvalidAlgorithmParameterException e) {
      e.printStackTrace();
    }
    kpg.generateKeyPair();

    generateAndStorePrivateKeyAES();


    createVirgilPrivateKey(exportedPrivateKey);
  }

  @SuppressLint("NewApi")
  @Override
  public void generateEncryptionPublicKeyPreMarshMallow(String exportedPublicKey) {
    // Generate a key pair for encryption
    Calendar start = Calendar.getInstance();
    Calendar end = Calendar.getInstance();
    end.add(Calendar.YEAR, 30);
    KeyPairGeneratorSpec spec;
    spec = new KeyPairGeneratorSpec.Builder(activity)
        .setAlias(KEY_PUBLICALIAS)
        .setSubject(new X500Principal("CN=" + KEY_PUBLICALIAS))
        .setSerialNumber(BigInteger.TEN)
        .setStartDate(start.getTime())
        .setEndDate(end.getTime())
        .build();

    KeyPairGenerator kpg = null;
    try {
      kpg = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, ANDROID_KEYSTORE);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (NoSuchProviderException e) {
      e.printStackTrace();
    }
    try {
      assert kpg != null;
      kpg.initialize(spec);
    } catch (InvalidAlgorithmParameterException e) {
      e.printStackTrace();
    }
    kpg.generateKeyPair();

    generateAndStorePublicKeyAES();

    createVirgilPublicKey(exportedPublicKey);
  }

  private void createVirgilPrivateKey(String exportedPrivateKey) {
/*
    try {
      CryptographyExample cryptographyExample = new CryptographyExample();
      // Generate keys
      VirgilCrypto crypto = new VirgilCrypto();
      VirgilKeyPair keyPair = cryptographyExample.keyGeneration();
      // Export private key
      exportedPrivateKey = cryptographyExample.exportPrivateKey(keyPair, crypto);
      // Export public key
      exportedPublicKey = cryptographyExample.exportPublicKey(keyPair, crypto);
    } catch (CryptoException e) {
      e.printStackTrace();
    }
*/
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      encryptPrivateKeyPostLollipop(exportedPrivateKey);
    } else {
      encryptPrivateKeyPreMarshMallow(exportedPrivateKey);
    }
  }

  private void createVirgilPublicKey(String exportedPublicKey) {
/*
    try {
      CryptographyExample cryptographyExample = new CryptographyExample();
      // Generate keys
      VirgilCrypto crypto = new VirgilCrypto();
      VirgilKeyPair keyPair = cryptographyExample.keyGeneration();
      // Export private key
      exportedPrivateKey = cryptographyExample.exportPrivateKey(keyPair, crypto);
      // Export public key
      exportedPublicKey = cryptographyExample.exportPublicKey(keyPair, crypto);
    } catch (CryptoException e) {
      e.printStackTrace();
    }
*/
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      encryptPublicKeyPostLollipop(exportedPublicKey);
    } else {
      encryptPublicKeyPreMarshMallow(exportedPublicKey);
    }
  }

  @SuppressLint("NewApi")
  @Override
  public void encryptPrivateKeyPostLollipop(String input) {
    Cipher c;
    try {
      c = Cipher.getInstance(AES_MODE);
      c.init(Cipher.ENCRYPT_MODE, getSecretPrivateKeyPostLollipop(), new GCMParameterSpec(128,
          FIXED_IV.getBytes()));
      byte[] encodedBytes = c.doFinal(input.getBytes(StandardCharsets.US_ASCII));

      String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
      Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
      sessionManager.setVirgilPrivateKey(encryptedBase64Encoded);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @SuppressLint("NewApi")
  @Override
  public void encryptPublicKeyPostLollipop(String input) {
    Cipher c;
    try {
      c = Cipher.getInstance(AES_MODE);
      c.init(Cipher.ENCRYPT_MODE, getSecretPublicKeyPostLollipop(), new GCMParameterSpec(128,
          FIXED_IV.getBytes()));
      byte[] encodedBytes = c.doFinal(input.getBytes(StandardCharsets.US_ASCII));

      String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
      Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
      sessionManager.setVirgilPublicKey(encryptedBase64Encoded);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void encryptPrivateKeyPreMarshMallow(String input) {
    Cipher c;
    try {
      c = Cipher.getInstance(AES_MODE, "BC");
      c.init(Cipher.ENCRYPT_MODE, getSecretPrivateKeyPreMarshMallow(), IV_PARAMETER_SPEC);
      byte[] encodedBytes = c.doFinal(input.getBytes());
      String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
      Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
      sessionManager.setVirgilPrivateKey(encryptedBase64Encoded);
      sessionManager.setVirgilPrivateKeyT(encryptedBase64Encoded);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void encryptPublicKeyPreMarshMallow(String input) {
    Cipher c;
    try {
      c = Cipher.getInstance(AES_MODE, "BC");
      c.init(Cipher.ENCRYPT_MODE, getSecretPublicKeyPreMarshMallow(), IV_PARAMETER_SPEC);
      byte[] encodedBytes = c.doFinal(input.getBytes());
      String encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
      Log.i("TEST", "encrypted string " + encryptedBase64Encoded);
      sessionManager.setVirgilPublicKey(encryptedBase64Encoded);
      sessionManager.setVirgilPublicKeyT(encryptedBase64Encoded);
    } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
