package com.vaidg.Login;

import android.util.Patterns;

/**
 * <h2>LoginModel</h2>
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
public class LoginModel {

  private int mLoginType; // 1 facebook || 2 google

  int getLoginType() {
    return mLoginType;
  }

  void setLoginType(int loginType) {
    this.mLoginType = loginType;
  }

  boolean emptyUname(final String username) {
    return username == null || "".equals(username);
  }

  boolean validateUserName(final String username) {
    return !(!Patterns.EMAIL_ADDRESS.matcher(username).matches() && Patterns.PHONE.matcher(
        username).matches()) && !(Patterns.EMAIL_ADDRESS.matcher(username).matches()
        && !Patterns.PHONE.matcher(username).matches());
  }

  boolean validatePassword(final String password) {
    return password == null || "".equals(password);
  }
}
