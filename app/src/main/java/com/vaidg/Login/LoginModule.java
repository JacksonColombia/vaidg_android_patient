package com.vaidg.Login;


import com.vaidg.Dagger2.ActivityScoped;
import dagger.Binds;
import dagger.Module;


/**
 * <h2>LoginModule</h2>
 *
 * This is a Dagger module. We use this to pass in the View dependency to the
 * {@link LoginContract.LoginPresenter}.
 *
 * @author 3Embed
 * @since 12-07-2019.
 */
@Module
public abstract class LoginModule {

  @ActivityScoped
  @Binds
  abstract LoginContract.LoginPresenter loginPresenter(LoginPresenterImpl loginPresenter);

  @ActivityScoped
  @Binds
  abstract LoginContract.LoginView loginView(LoginActivity loginActivity);

}
