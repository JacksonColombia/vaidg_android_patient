package com.vaidg.Login;

import android.content.Intent;

import com.vaidg.encryptiondecryption.CryptographyExample;
import com.vaidg.home.BasePresenter;
import com.vaidg.home.BaseView;
import com.vaidg.model.UserDetailsDataModel;
import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilKeyPair;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;

/**
 * <h2>LoginContract</h2>
 * This interface is used to interact between model and view
 * @author  3Embed.
 * @since 22-11-2017
 */
public interface LoginContract {

    interface LoginView extends BaseView {

        /**
         * <h2>setUsernameError</h2>
         * This method is used to show the error message on empty username
         */
        void setUsernameError();

        /**
         * <h2>setInvalidUname</h2>
         * This method is used to show the error message on invalid username
         */
        void setInvalidUname();

        /**
         * <h2>setPasswordError</h2>
         * This method is used to show the error message on invalid password
         */
        void setPasswordError();

        /**
         * <h2>onLoginSuccess</h2>
         * This method is used to navigate to the HomeScreen after the success of the Login API CALL,
         * and save the auth token to the Account via AccountManager of ANDROID
         *
         * @param auth     Auth token to store in Account
         * @param username Username to store in Account
         * @param password Password used to store in Account via AccountManager
         */
        void onLoginSuccess(String auth, String username, String password);

        /**
         * <h2>onError</h2>
         * This method is used to show the error message if the Login API call fails and gives error
         * from
         * server
         *
         * @param msg Message to show via TOAST
         */
        void onError(String msg);

        /**
         * <h2>openGoogleActivity</h2>
         * This method is used to open the google login activity.
         *
         * @param intent Intent to open the google
         */
        void openGoogleActivity(Intent intent);

        /**
         * <h2>revokeGuestLogin</h2>
         * This method is used to revoke the guestLogin and remove the account from account manager.
         *
         * @param emailId deviceId used for GuestLogin - this would be device_id preferably.
         */
        void revokeGuestLogin(String emailId);

        /**
         * <h2>navToSignUp</h2>
         * This method is used to navigate to the SignUpActivity screen
         *
         * @param loginType            loginType
         * @param userDetailsDataModel userDetailsDataModel
         * @param mGoogleApiClient     mGoogleApiClient
         */
        void navToSignUp(Integer loginType, UserDetailsDataModel userDetailsDataModel,
                         GoogleApiClient mGoogleApiClient);

        /**
         * <h2>setEmailView</h2>
         * This method is used to show message on call of APIS
         *
         * @param username username of the Account user
         */
        void setEmailView(String username);

        /**
         * <h2>openGoogleActivity</h2>
         * This method is used to check whether google client is connected or not.
         */
        void googleClientIsNotConnected();

        /**
         * <h2>checkForVersion</h2>
         * This method is used to check for version of the device
         */
        void checkForVersion();

    }

    interface LoginPresenter extends BasePresenter {

        /**
         * <h2>validateCredentials</h2>
         * This method is used to validate the credentials for login
         *
         * @param username Username of the user, email id or phone number
         * @param password Password of the user
         */
        void validateCredentials(String username, String password);

        /**
         * <h2>validateUname</h2>
         * This method is used to validate the User name
         *
         * @param emailOrPhone Username of the user, email id or phone number
         */
        void validateUname(String emailOrPhone);

        /**
         * <h2>initializeFacebook</h2>
         * This method is used to initialize the facebook
         */
        void initializeFacebook();

        /**
         * <h2>fbLogin</h2>
         * <p>
         * This method is used for Facebook login.
         * </P>
         *
         * @param callbackManager: facebook call back manager interface
         */
        void handleResultFromFB(CallbackManager callbackManager);

        /**
         * <h2>handleResultFromGoogle</h2>
         * <p>
         * This method is used for Google Login.
         * </P>
         *
         * @param googleSignInResult : Google SignIn result object
         */
        void handleResultFromGoogle(GoogleSignInResult googleSignInResult);

        /**
         * <h2>normalLogin</h2>
         * This method will call, when user click on Login button and
         * <p>
         * this method will make a call to CallLoginService() method located in Login Model class.
         * </p>
         *
         * @param emailOrPhone contains the email id or phone.
         * @param password     contain the password.
         */
        void normalLogin(String emailOrPhone, String password);

        /**
         * <h2>storeLoginType</h2>
         * This method is used to store the login type
         *
         * @param loginType login type 1 for facebook and 2 for google
         */
        void storeLoginType(int loginType);

        /**
         * <h2>handleLoginType</h2>
         * This method is used to handle the login type
         *
         * @param callbackManager facebook call back manager
         */
        void handleLoginType(CallbackManager callbackManager);

        /**
         * <h2>initializeFBGoogle</h2>
         * This method is used to initialize FB and google SDK
         */
        void initializeFBGoogle();

        /**
         * <h2>googleLogin</h2>
         * This method is used to login via google into the System
         */
        void googleLogin();

        /**
         * <h2>doLogin</h2>
         * This method is used to perform login
         *
         * @param username Email/Mobile number of the user.
         * @param password Password of the user.
         */
        void doLogin(String username, String password);


        /**
         * <h2>checkForKeystore</h2>
         * This method is used to check for keystore
         */
        void checkForKeystore();
        /**
         * <h2>generateEncryptionKeyPostLollipop</h2>
         * This method is used to generate the encryption key required for encryption for post lollipop
         * @param exportedPublicKey
         */
        void generateEncryptionPublicKeyPostLollipop(String exportedPublicKey);

        /**
         * <h2>generateEncryptionKeyPostLollipop</h2>
         * This method is used to generate the encryption key required for encryption for post lollipop
         * @param exportedPrivateKey
         */
        void generateEncryptionPrivateKeyPostLollipop(String exportedPrivateKey);
        /**
         * <h2>generateEncryptionKeyPostLollipop</h2>
         * This method is used to generate the encryption key required for encryption for pre marshmallow
         * @param exportedPublicKey
         */
        void generateEncryptionPublicKeyPreMarshMallow(String exportedPublicKey);
        /**
         * <h2>generateEncryptionKeyPostLollipop</h2>
         * This method is used to generate the encryption key required for encryption for pre marshmallow
         * @param exportedPrivateKey
         */
        void generateEncryptionPrivateKeyPreMarshMallow(String exportedPrivateKey);
        /**
         * <h2>encryptPrivateKeyPostLollipop</h2>
         * This method is used to encrypt the data for post lollipop
         * @param input data to be encrypted
         */
        void encryptPrivateKeyPostLollipop(String input);
        /**
         * <h2>encryptPrivateKeyPostLollipop</h2>
         * This method is used to encrypt the data for pre marshmallow
         * @param input data to be encrypted
         */
        void encryptPrivateKeyPreMarshMallow(String input);
        /**
         * <h2>encryptPublicKeyPostLollipop</h2>
         * This method is used to encrypt the data for post lollipop
         * @param input data to be encrypted
         */
        void encryptPublicKeyPostLollipop(String input);
        /**
         * <h2>encryptPublicKeyPostLollipop</h2>
         * This method is used to encrypt the data for pre marshmallow
         * @param input data to be encrypted
         */
        void encryptPublicKeyPreMarshMallow(String input);

        /**
         * <h2>checkForVersion</h2>
         * This method is used to check for version of the device
         * @param cryptographyExample
         * @param crypto
         * @param keyPair
         */
        void checkForVersion(CryptographyExample cryptographyExample, VirgilCrypto crypto, VirgilKeyPair keyPair) throws CryptoException;

    }
}
