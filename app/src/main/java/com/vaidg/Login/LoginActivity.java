package com.vaidg.Login;

import static com.vaidg.utilities.Constants.FACEBOOK_LOGIN;
import static com.vaidg.utilities.Constants.GOOGLE_LOGIN;
import static com.vaidg.utilities.Constants.H_480;
import static com.vaidg.utilities.Constants.H_5;
import static com.vaidg.utilities.Constants.H_50;
import static com.vaidg.utilities.Constants.REQUEST_ID_GOOGLE_SIGNIN;
import static com.vaidg.utilities.Constants.REQUEST_ID_READ_STORAGE_PERMISSION;
import static com.vaidg.utilities.Constants.USER_DETAILS;
import static com.vaidg.utilities.Constants.W_10;
import static com.vaidg.utilities.Constants.W_11;
import static com.vaidg.utilities.Constants.W_13;
import static com.vaidg.utilities.Constants.W_15;
import static com.vaidg.utilities.Constants.W_20;
import static com.vaidg.utilities.Constants.W_22;
import static com.vaidg.utilities.Constants.W_320;
import static com.vaidg.utilities.Constants.W_40;
import static com.vaidg.utilities.Constants.W_5;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import com.vaidg.R;
import com.vaidg.encryptiondecryption.CryptographyExample;
import com.vaidg.forgotpassword.ForgotPwdActivity;
import com.vaidg.home.MainActivity;
import com.vaidg.intro.IntroActivity;
import com.vaidg.model.UserDetailsDataModel;
import com.vaidg.promocode.PromoCodeActivity;
import com.vaidg.signup.SignUpActivity;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.material.textfield.TextInputLayout;
import com.utility.AlertProgress;
import com.utility.DialogInterfaceListner;
import com.virgilsecurity.sdk.crypto.VirgilCrypto;
import com.virgilsecurity.sdk.crypto.VirgilKeyPair;
import com.virgilsecurity.sdk.crypto.exceptions.CryptoException;

import dagger.android.support.DaggerAppCompatActivity;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

/**
 * <h2>Login Activity</h2>
 * <p>
 * This class is used to provide the Login screen, where we can do our login and if we forget
 * our password then here we also can make a request to forgot password
 * and if login successful, then it directly opens HomeScreen.
 * </p>
 *
 * @author Pramod
 * @since 21-12-2017.
 */
public class LoginActivity extends DaggerAppCompatActivity implements LoginContract.LoginView {
  @Inject
  LoginContract.LoginPresenter loginPresenter;
  @BindView(R.id.atLoginEmail)
  AutoCompleteTextView atLoginEmail;
  @BindView(R.id.atLoginPassword)
  AutoCompleteTextView atLoginPassword;
  @BindView(R.id.btnLogin)
  Button btnLogin;
  @BindView(R.id.iv_login_fb)
  ImageView iv_login_fb;
  @BindView(R.id.iv_login_google)
  ImageView iv_login_google;
  @BindView(R.id.rl_login_signup)
  RelativeLayout rl_login_signup;
  @BindView(R.id.tvSignup)
  TextView tvSignup;
  @BindView(R.id.tvDontHaveAccount)
  TextView tvDontHaveAccount;
  @BindView(R.id.tv_center)
  TextView tv_center;
  @BindView(R.id.rlFbLogin)
  RelativeLayout rlFbLogin;
  @BindView(R.id.rlGoogleLogin)
  RelativeLayout rlGoogleLogin;
  @BindView(R.id.tvForgotPass)
  TextView tvForgotPass;
  @BindView(R.id.tvLoginWithfb)
  TextView tvLoginWithfb;
  @BindView(R.id.tvLoginWithGoogle)
  TextView tvLoginWithGoogle;
  @BindView(R.id.toolbar)
  Toolbar toolBar;
  @BindView(R.id.tilLoginEmail)
  TextInputLayout tilLoginEmail;
  @BindView(R.id.tilLoginPwd)
  TextInputLayout tilLoginPwd;
  @BindView(R.id.loginscrollview)
  ScrollView loginscrollview;
  @BindString(R.string.signinLogin)
  String signinLogin;
  @BindString(R.string.waitSignin)
  String waitSignin;
  @BindString(R.string.errUname)
  String errUname;
  @BindString(R.string.invalidUsername)
  String invalidUsername;
  @BindString(R.string.errPassword)
  String errPassword;
  AlertDialog alertDialog;
  AlertDialog.Builder dialogBuilder;
  @Inject
  SessionManagerImpl sessionManager;
  @Inject
  AlertProgress alertProgress;
  @Inject
  AppTypeface appTypeface;
  private boolean mFlagFbGoogle = false;
  private String[] mPerms = {Manifest.permission.READ_EXTERNAL_STORAGE};
  private CallbackManager mCallbackManager;
  private Context mContext;
  private String mLogoutFlag = "";

  private void calculate() {
    int w10 = Utility.getScreenWidth() * W_10 / W_320;
    int h10 = Utility.getScreenHeight() * W_10 / H_480;
    int w5 = Utility.getScreenWidth() * W_5 / W_320;
    int h50 = Utility.getScreenHeight() * H_50 / H_480;
    int w20 = Utility.getScreenWidth() * W_20 / W_320;
    int h5 = Utility.getScreenHeight() * H_5 / H_480;
    int w22 = Utility.getScreenWidth() * W_40 / W_320;
    int w13 = Utility.getScreenWidth() * W_13 / W_320;
    int w11 = Utility.getScreenWidth() * W_11 / W_320;
    int w15 = Utility.getScreenWidth() * W_15 / W_320;

    tilLoginEmail.setPadding(w10, 0, w10, 0);
    tilLoginPwd.setPadding(w10, 0, w10, 0);
    atLoginEmail.setPadding(w5, h10, w5, h10);
    atLoginPassword.setPadding(w5, h10, w5, h10);
    tvForgotPass.setPadding(w10, 0, w10, h5);
    btnLogin.setPadding(w10, 0, w10, 0);
    rl_login_signup.setPadding(0, h10, 0, h10);
    tvSignup.setPadding(w5, 0, 0, 0);
    btnLogin.getLayoutParams().height = w22;
    iv_login_fb.getLayoutParams().height = w13;
    iv_login_fb.getLayoutParams().width = w13;
    iv_login_google.getLayoutParams().width = w15;
    iv_login_google.getLayoutParams().height = w15;
    rlFbLogin.getLayoutParams().height = w22;
    rlGoogleLogin.getLayoutParams().height = w22;
    rlFbLogin.setPadding(w10, h10, w10, 0);
    rlGoogleLogin.setPadding(w10, h10, w10, 0);
    tvLoginWithfb.setPadding(w10, h5, 0, 0);
    tvLoginWithGoogle.setPadding(w10, 0, 0, 0);
  }

  /**
   * This is the onCreate LoginActivity method that is called firstly, when user came to Login
   * screen.
   *
   * @param savedInstanceState contains an instance of Bundle.
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login2);
    ButterKnife.bind(this);
    Utility.statusbarColor(this);
    mContext = this;
    initialize();
    calculate();
  }

  /**
   * <h2>initialize</h2>
   * <p> method to initialize the views</p>
   */
  private void initialize() {
    setSupportActionBar(toolBar);
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayShowHomeEnabled(true);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
    mCallbackManager = CallbackManager.Factory.create();
    loginPresenter.initializeFBGoogle();
    tv_center.setTypeface(appTypeface.getHind_medium());
    tv_center.setText(signinLogin);
    tv_center.setTypeface(appTypeface.getHind_semiBold());
    toolBar.setNavigationIcon(R.drawable.ic_back);
    toolBar.setNavigationOnClickListener(view -> {
      mLogoutFlag = getIntent().getStringExtra("logout");
      if (mLogoutFlag == null || mLogoutFlag.trim().isEmpty()) {
        onBackPressed();
      } else {
        Intent intent = new Intent(mContext, IntroActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
      }
    });
    tvLoginWithfb.setTypeface(appTypeface.getHind_bold());
    tvLoginWithGoogle.setTypeface(appTypeface.getHind_bold());
    tvSignup.setTypeface(appTypeface.getHind_bold());
    btnLogin.setTypeface(appTypeface.getHind_semiBold());
    tvDontHaveAccount.setTypeface(appTypeface.getHind_regular());
    atLoginEmail.setTypeface(appTypeface.getHind_regular());
    atLoginPassword.setTypeface(appTypeface.getHind_regular());
    tvForgotPass.setTypeface(appTypeface.getHind_regular());
    tilLoginEmail.setErrorEnabled(true);
    tilLoginPwd.setErrorEnabled(true);
    rlFbLogin.startAnimation(Utility.animateFtomLeft(mContext));
    btnLogin.startAnimation(Utility.animateFtomLeft(mContext));
    rlGoogleLogin.startAnimation(Utility.animateFtomRight(mContext));

  }

  @Override
  public void setUsernameError() {
    tilLoginEmail.setError(errUname);
  }

  @Override
  public void setInvalidUname() {
    tilLoginEmail.setError(invalidUsername);
  }

  @Override
  public void setPasswordError() {
    tilLoginPwd.setError(errPassword);
  }

  @Override
  public void revokeGuestLogin(String auth) {
  }

  @Override
  public void onLoginSuccess(String auth, String username, String password) {
    if (sessionManager.getGuestLogin()) {
      sessionManager.setGuestLogin(false);
    } else {
      sessionManager.setGuestLogin(false);
      Intent intent = new Intent(mContext, MainActivity.class);
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
          | Intent.FLAG_ACTIVITY_CLEAR_TOP);
      //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
      startActivity(intent);
    }
    finish();
  }

  @Override
  public void onSessionExpired() {
  }

  @Override
  public void onLogout(String message, SessionManagerImpl sessionManager) {
  }

  @Override
  public void onError(String msg) {
    alertProgress.alertinfo(mContext, msg);
  }

  @Override
  public void onConnectionError(String connectionError) {

  }

  @Override
  public void onShowProgress() {
    if (!isFinishing()) {
      dialogBuilder = new AlertDialog.Builder(mContext);
      LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      View dialogView = inflater != null ? inflater.inflate(R.layout.progress_dialog_layout, null)
          : null;
      TextView tv_progress = dialogView != null ? dialogView.findViewById(R.id.tv_progress) : null;
      if (tv_progress != null) {
        tv_progress.setText(waitSignin);
      }
      dialogBuilder.setView(dialogView);
      dialogBuilder.setCancelable(false);
      alertDialog = dialogBuilder.create();
      alertDialog.show();
    }

  }

  @Override
  public void onHideProgress() {
    if (alertDialog != null && alertDialog.isShowing()) {
      alertDialog.dismiss();
    }
  }

  @OnTextChanged(value = R.id.atLoginPassword,
      callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
  void afterPwdTxtChange(Editable editable) {

    int str_len = editable.toString().length();
    if (str_len <= 0) {
      tilLoginPwd.setError(null);
      setPasswordError();
    }
    if (str_len > 0) {
      tilLoginPwd.setError(null);
    }
  }

  @OnTextChanged(value = R.id.atLoginEmail,
      callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
  void afterEmailTxtChange(Editable editable) {
    int email_str_len = editable.toString().length();
    if (email_str_len <= 0) {
      tilLoginEmail.setError(null);
      setUsernameError();
    }
    if (email_str_len > 0) {
      tilLoginEmail.setError(null);
    }
  }

  @OnFocusChange({R.id.atLoginEmail, R.id.atLoginPassword})
  void onFocusEvent(View view, boolean hasFocus) {
    switch (view.getId()) {
      case R.id.atLoginEmail:
        if (!hasFocus) {
          //Checks Email
          tilLoginEmail.setError(null);
          loginPresenter.validateUname(atLoginEmail.getText().toString());
        }
        break;
      case R.id.atLoginPassword:
        if (!hasFocus) {
          //Checks password
          tilLoginPwd.setError(null);
        }
        break;

      default:
        break;
    }
  }

  @OnClick({R.id.btnLogin, R.id.tvSignup, R.id.rlFbLogin, R.id.rlGoogleLogin, R.id.tvForgotPass})
  void onClickEvent(View view) {
    switch (view.getId()) {
      case R.id.btnLogin:
        if(Utility.isNetworkAvailable(mContext)) {
          loginPresenter.validateCredentials(atLoginEmail.getText().toString(),
                  atLoginPassword.getText().toString());
        }else{
          alertProgress.tryAgain(mContext,  getString(R.string.pleaseCheckInternet), getString(R.string.system_error), new DialogInterfaceListner() {
            @Override
            public void dialogClick(boolean isClicked) {

            }
          });
        }
          break;
      case R.id.tvSignup:
        Intent signUpIntent = new Intent(mContext, SignUpActivity.class);
        startActivity(signUpIntent);
        break;
      case R.id.rlFbLogin:
        if (Build.VERSION.SDK_INT >= 23) {
          if (ContextCompat.checkSelfPermission(mContext, mPerms[0])
              == PackageManager.PERMISSION_GRANTED) {
            loginPresenter.storeLoginType(FACEBOOK_LOGIN);
            loginPresenter.handleResultFromFB(mCallbackManager);
          } else {
            mFlagFbGoogle = true;
            checkAndReqReadPerms();
          }
        } else {
          loginPresenter.storeLoginType(FACEBOOK_LOGIN);
          loginPresenter.handleResultFromFB(mCallbackManager);
        }
        break;
      case R.id.rlGoogleLogin:
        googleLogin();
        break;
      case R.id.tvForgotPass:
        Intent forgotPwdIntent = new Intent(mContext, ForgotPwdActivity.class);
        startActivity(forgotPwdIntent);
        break;
      default:
        break;
    }
  }

  /**
   * <h2>googleLogin</h2>
   * This method is used to login via google into the System
   */
  private void googleLogin() {
    if (Build.VERSION.SDK_INT >= 23) {
      if (ContextCompat.checkSelfPermission(mContext, mPerms[0])
          == PackageManager.PERMISSION_GRANTED) {
        loginPresenter.storeLoginType(GOOGLE_LOGIN);
        loginPresenter.googleLogin();
      } else {
        checkAndReqReadPerms();
      }
    } else {
      loginPresenter.storeLoginType(GOOGLE_LOGIN);
      loginPresenter.googleLogin();
    }
  }

  /*<h2>checkAndReqReadPerms</h2>
   * <p> Check and Request for READ EXTERNAL STORAGE Permissions for facebook login. </p>
   */
  private void checkAndReqReadPerms() {
    if (Build.VERSION.SDK_INT >= 23) {
      int readPerm = ContextCompat.checkSelfPermission(mContext,
          Manifest.permission.READ_EXTERNAL_STORAGE);
      List<String> listPermissionsNeeded = new ArrayList<>();
      if (readPerm != PackageManager.PERMISSION_GRANTED) {
        listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
      }
      if (!listPermissionsNeeded.isEmpty()) {
        ActivityCompat.requestPermissions(((Activity) mContext),
            listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
            REQUEST_ID_READ_STORAGE_PERMISSION);
      }
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    loginPresenter.initializeFacebook();
  }

  /**
   * predefined method to check run time permissions list call back
   *
   * @param requestCode   request code
   * @param permissions:  contains the list of requested permissions
   * @param grantResults: contains granted and un granted permissions result list
   */
  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
    if (requestCode == REQUEST_ID_READ_STORAGE_PERMISSION) {
      boolean permissionAllowed = false;
      String deniedPermission = "";
      int i = 0;
      for (int grantResult : grantResults) {
        if (grantResult == PackageManager.PERMISSION_GRANTED) {
          permissionAllowed = true;
          i++;
        } else {
          deniedPermission = permissions[i];
          permissionAllowed = false;
          break;
        }
      }
      if (permissionAllowed) {
        if (mFlagFbGoogle) {
          loginPresenter.storeLoginType(FACEBOOK_LOGIN);
          loginPresenter.handleLoginType(mCallbackManager);
        } else {
          loginPresenter.storeLoginType(GOOGLE_LOGIN);
          loginPresenter.googleLogin();
        }
      } else {
        boolean somePermissionsForeverDenied = false;
        for (String permission : permissions) {
          if (ActivityCompat.shouldShowRequestPermissionRationale(((Activity) mContext), permission)) {
            //denied
            somePermissionsForeverDenied = true;
          } else {
            if (ActivityCompat.checkSelfPermission(mContext, permission)
                == PackageManager.PERMISSION_GRANTED) {
              //allowed
            } else {
              //set to never ask again
              somePermissionsForeverDenied = true;
            }
          }
        }
        if (somePermissionsForeverDenied) {
          Toast.makeText(mContext,
              getResources().getString(R.string.permission_denied),
              Toast.LENGTH_SHORT).show();
        }
      }
    } else {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }

  @Override
  public void openGoogleActivity(Intent intent) {
    startActivityForResult(intent, REQUEST_ID_GOOGLE_SIGNIN);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    mCallbackManager.onActivityResult(requestCode, resultCode, data);
    if (requestCode == REQUEST_ID_GOOGLE_SIGNIN) {
      if (!TextUtils.isEmpty(atLoginEmail.getText().toString())) {
        atLoginEmail.setText("");
      }
      if (!TextUtils.isEmpty(atLoginPassword.getText().toString())) {
        atLoginPassword.setText("");
      }
      GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
      loginPresenter.handleResultFromGoogle(result);
    }
  }

  @Override
  public void navToSignUp(Integer loginType, UserDetailsDataModel userDetailsDataModel,
                          GoogleApiClient mGoogleApiClient) {
    Bundle bundle = new Bundle();
    bundle.putSerializable(USER_DETAILS, userDetailsDataModel);
    Intent intent = new Intent(mContext, SignUpActivity.class);
    intent.putExtras(bundle);
    //intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
    startActivity(intent);
  }

  @Override
  public void setEmailView(String username) {
    tilLoginEmail.setErrorEnabled(false);
    tilLoginEmail.setError(null);
    atLoginEmail.setText(username);
  }

  @Override
  public void googleClientIsNotConnected() {
    alertProgress.tryAgain(mContext, getString(R.string.googleClientIsNotConnected),
        getString(R.string.try_again)
        , isClicked -> {
          if (isClicked) {
            loginPresenter.initializeFBGoogle();
            googleLogin();
          }
        });
  }

  @Override
  public void checkForVersion() {
    try {
      CryptographyExample cryptographyExample = new CryptographyExample();
      // Generate keys
      VirgilCrypto crypto = new VirgilCrypto();
      VirgilKeyPair keyPair = cryptographyExample.keyGeneration();
      loginPresenter.checkForVersion(cryptographyExample,crypto,keyPair);
    } catch (CryptoException e) {
      e.printStackTrace();
    }
  }

}
