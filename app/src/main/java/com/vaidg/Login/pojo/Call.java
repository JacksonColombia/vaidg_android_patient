package com.vaidg.Login.pojo;

import static com.vaidg.utilities.Constants.AUTH_TOKEN;
import static com.vaidg.utilities.Constants.WILL_TOPIC;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * Created by Ali on 3/20/2019.
 */
public class Call implements Serializable
{

    @SerializedName(AUTH_TOKEN)
    @Expose
    private String authToken;
    @SerializedName(WILL_TOPIC)
    @Expose
    private String willTopic;


    /**
     * No args constructor for use in serialization
     *
     */
    public Call() {
    }

    /**
     *
     * @param willTopic
     * @param authToken
     */
    public Call(String authToken, String willTopic) {
        super();
        this.authToken = authToken;
        this.willTopic = willTopic;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getWillTopic() {
        return willTopic;
    }

    public void setWillTopic(String willTopic) {
        this.willTopic = willTopic;
    }

}