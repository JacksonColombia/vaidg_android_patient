package com.vaidg.Login.pojo;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TokenPojo implements Parcelable {

  public final static Parcelable.Creator<TokenPojo> CREATOR = new Creator<TokenPojo>() {


    @SuppressWarnings({
        "unchecked"
    })
    public TokenPojo createFromParcel(Parcel in) {
      return new TokenPojo(in);
    }

    public TokenPojo[] newArray(int size) {
      return (new TokenPojo[size]);
    }

  };
  @SerializedName("accessExpireAt")
  @Expose
  private int accessExpireAt;
  @SerializedName("accessToken")
  @Expose
  private String accessToken;
  @SerializedName("refreshToken")
  @Expose
  private String refreshToken;

  protected TokenPojo(Parcel in) {
    this.accessExpireAt = ((int) in.readValue((int.class.getClassLoader())));
    this.accessToken = ((String) in.readValue((String.class.getClassLoader())));
    this.refreshToken = ((String) in.readValue((String.class.getClassLoader())));
  }

  /**
   * No args constructor for use in serialization
   */
  public TokenPojo() {
  }

  /**
   * @param accessExpireAt
   * @param accessToken
   * @param refreshToken
   */
  public TokenPojo(int accessExpireAt, String accessToken, String refreshToken) {
    super();
    this.accessExpireAt = accessExpireAt;
    this.accessToken = accessToken;
    this.refreshToken = refreshToken;
  }

  public int getAccessExpireAt() {
    return accessExpireAt;
  }

  public void setAccessExpireAt(int accessExpireAt) {
    this.accessExpireAt = accessExpireAt;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getRefreshToken() {
    return refreshToken;
  }

  public void setRefreshToken(String refreshToken) {
    this.refreshToken = refreshToken;
  }

  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(accessExpireAt);
    dest.writeValue(accessToken);
    dest.writeValue(refreshToken);
  }

  public int describeContents() {
    return 0;
  }

}