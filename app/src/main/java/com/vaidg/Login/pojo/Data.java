package com.vaidg.Login.pojo;

import static com.vaidg.utilities.Constants.AVG_RATING;
import static com.vaidg.utilities.Constants.CALL;
import static com.vaidg.utilities.Constants.CARD_DETAIL;
import static com.vaidg.utilities.Constants.COUNTRY_CODE;
import static com.vaidg.utilities.Constants.COUNTRY_SYMBOL;
import static com.vaidg.utilities.Constants.CURRENCYCODE;
import static com.vaidg.utilities.Constants.FCMTOPIC;
import static com.vaidg.utilities.Constants.FIRST_NAME;
import static com.vaidg.utilities.Constants.LAST_NAME;
import static com.vaidg.utilities.Constants.LOGIN_EMAIL;
import static com.vaidg.utilities.Constants.LOGIN_PHONE;
import static com.vaidg.utilities.Constants.PAYMENT_ID;
import static com.vaidg.utilities.Constants.PROFILE_PIC;
import static com.vaidg.utilities.Constants.PUBLISHABLE_KEY;
import static com.vaidg.utilities.Constants.REFERRAL_CODE;
import static com.vaidg.utilities.Constants.REQUESTER_ID;
import static com.vaidg.utilities.Constants.SID;
import static com.vaidg.utilities.Constants.TOKEN;

import com.vaidg.model.payment_method.CardDetail;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;


/**
 * Created by Pramod on 14/12/17.
 */

public class Data implements Serializable
{

  @SerializedName(TOKEN)
  @Expose
  private TokenPojo token;
  @SerializedName(SID)
  @Expose
  private String sid;
  @SerializedName(LOGIN_EMAIL)
  @Expose
  private String email;
  @SerializedName(FIRST_NAME)
  @Expose
  private String firstName;
  @SerializedName(LAST_NAME)
  @Expose
  private String lastName;
  @SerializedName(AVG_RATING)
  @Expose
  private double averageRating;
  @SerializedName(COUNTRY_CODE)
  @Expose
  private String countryCode;
  @SerializedName(COUNTRY_SYMBOL)
  @Expose
  private String countrySymbol;
  @SerializedName(LOGIN_PHONE)
  @Expose
  private String phone;
  @SerializedName(REFERRAL_CODE)
  @Expose
  private String referralCode;
  @SerializedName(PROFILE_PIC)
  @Expose
  private String profilePic;
  @SerializedName(PAYMENT_ID)
  @Expose
  private String paymentId;
  @SerializedName(FCMTOPIC)
  @Expose
  private String fcmTopic;
  @SerializedName(CURRENCYCODE)
  @Expose
  private String currencyCode;
  @SerializedName(PUBLISHABLE_KEY)
  @Expose
  private String publishableKey;
  @SerializedName(CARD_DETAIL)
  @Expose
  private CardDetail cardDetail;
  @SerializedName(REQUESTER_ID)
  @Expose
  private String requesterId;
  @SerializedName(CALL)
  @Expose
  private Call call;
  @SerializedName("privateKey")
  @Expose
  private String privateKey;
  @SerializedName("publickKey")
  @Expose
  private String publickKey;
  @SerializedName("gender")
  @Expose
  private String gender;
  @SerializedName("dateOfBirth")
  @Expose
  private String dateOfBirth;

  /**
   * No args constructor for use in serialization
   *
   */
  public Data() {
  }

  /**
   *
   * @param publishableKey
   * @param lastName
   * @param requesterId
   * @param profilePic
   * @param fcmTopic
   * @param token
   * @param sid
   * @param call
   * @param firstName
   * @param phone
   * @param countrySymbol
   * @param countryCode
   * @param paymentId
   * @param averageRating
   * @param referralCode
   * @param cardDetail
   * @param currencyCode
   * @param email
   */
  public Data(TokenPojo token, String sid, String email, String firstName, String lastName, double averageRating, String countrySymbol, String countryCode, String phone, String referralCode, String profilePic, String paymentId, String fcmTopic, String currencyCode, String publishableKey, CardDetail cardDetail, String requesterId, Call call) {
    super();
    this.token = token;
    this.sid = sid;
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.averageRating = averageRating;
    this.countrySymbol = countrySymbol;
    this.countryCode = countryCode;
    this.phone = phone;
    this.referralCode = referralCode;
    this.profilePic = profilePic;
    this.paymentId = paymentId;
    this.fcmTopic = fcmTopic;
    this.currencyCode = currencyCode;
    this.publishableKey = publishableKey;
    this.cardDetail = cardDetail;
    this.requesterId = requesterId;
    this.call = call;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public String getGender() {
    return gender;
  }

  public String getPrivateKey() {
    return privateKey;
  }

  public void setPrivateKey(String privateKey) {
    this.privateKey = privateKey;
  }

  public String getPublickKey() {
    return publickKey;
  }

  public void setPublickKey(String publickKey) {
    this.publickKey = publickKey;
  }

  public TokenPojo getToken() {
    return token;
  }

  public void setToken(TokenPojo token) {
    this.token = token;
  }

  public String getSid() {
    return sid;
  }

  public void setSid(String sid) {
    this.sid = sid;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public double getAverageRating() {
    return averageRating;
  }

  public void setAverageRating(double averageRating) {
    this.averageRating = averageRating;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getCountrySymbol() {
    return countrySymbol;
  }

  public void setCountrySymbol(String countrySymbol) {
    this.countrySymbol = countrySymbol;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getReferralCode() {
    return referralCode;
  }

  public void setReferralCode(String referralCode) {
    this.referralCode = referralCode;
  }

  public String getProfilePic() {
    return profilePic;
  }

  public void setProfilePic(String profilePic) {
    this.profilePic = profilePic;
  }

  public String getPaymentId() {
    return paymentId;
  }

  public void setPaymentId(String paymentId) {
    this.paymentId = paymentId;
  }

  public String getFcmTopic() {
    return fcmTopic;
  }

  public void setFcmTopic(String fcmTopic) {
    this.fcmTopic = fcmTopic;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public String getPublishableKey() {
    return publishableKey;
  }

  public void setPublishableKey(String publishableKey) {
    this.publishableKey = publishableKey;
  }

  public CardDetail getCardDetail() {
    return cardDetail;
  }

  public void setCardDetail(CardDetail cardDetail) {
    this.cardDetail = cardDetail;
  }

  public String getRequesterId() {
    return requesterId;
  }

  public void setRequesterId(String requesterId) {
    this.requesterId = requesterId;
  }

  public Call getCall() {
    return call;
  }

  public void setCall(Call call) {
    this.call = call;
  }

}