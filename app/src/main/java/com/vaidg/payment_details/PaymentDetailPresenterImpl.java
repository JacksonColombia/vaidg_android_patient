package com.vaidg.payment_details;

import android.util.Log;
import com.vaidg.model.ServerResponse;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.google.gson.Gson;
import com.pojo.ErrorHandel;
import com.utility.RefreshToken;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import org.json.JSONObject;
import retrofit2.Response;

/**
 * @author Pramod
 * @since 31-01-2018.
 */
public class PaymentDetailPresenterImpl implements PaymentDetailPresenter {

  @Inject
  PaymentDetailView view;

  @Inject
  LSPServices lspServices;

  @Inject
  CompositeDisposable compositeDisposable;

  @Inject
  SessionManagerImpl manager;
  @Inject
  Gson gson;

  @Inject
  PaymentDetailPresenterImpl(PaymentDetailView view) {
    this.view = view;
  }

  @Override
  public void addCard(String auth, String email, String stripe_token) {
        /*if (view !=null) {
            view.showProgress();
        }*/
    Map<String, Object> jsonParams = new HashMap<>();
    jsonParams.put("email", email);
    jsonParams.put("cardToken", stripe_token);

    Observable<Response<ServerResponse>> bad = lspServices.addCard(auth, Constants.selLang,
        Constants.PLATFORM_ANDROID, jsonParams);
    bad.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Observer<Response<ServerResponse>>() {
          @Override
          public void onSubscribe(Disposable d) {
            compositeDisposable.add(d);
          }

          @Override
          public void onNext(Response<ServerResponse> value) {
            Log.e("CARD", "Req url " + value.raw().request().url());

            try {

              switch (value.code()) {
                case Constants.SUCCESS_RESPONSE:
                  ServerResponse serverResponse = value.body();
                  if (serverResponse != null) {
                    Log.e("CARD", serverResponse.getMessage());
                    if(view != null) {
                      view.hideProgress();
                      view.navToPaymentScreen();
                    }
                  } else {
                    if(view != null) {
                      view.hideProgress();
                    }
                  }
                  break;
                case Constants.SESSION_LOGOUT:
                  JSONObject errJson = new JSONObject(value.errorBody().string());
                  if(view != null) {
                    view.logout(errJson.getString("message"));
                    view.hideProgress();
                  }
                  break;
                case Constants.SESSION_EXPIRED:
                  String responseBody = value.errorBody().string();
                  ErrorHandel errorHandel = gson.fromJson(responseBody, ErrorHandel.class);
                  RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),manager.getREFRESHAUTH(), lspServices,
                      new RefreshToken.RefreshTokenImple() {
                        @Override
                        public void onSuccessRefreshToken(String newToken) {
                          if(view != null){
                            view.hideProgress();
                          }
                            LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                          addCard(newToken, email, stripe_token);
                        }

                        @Override
                        public void onFailureRefreshToken() {

                        }

                        @Override
                        public void sessionExpired(String msg) {
                          if(view != null) {
                            view.hideProgress();
                            view.logout(msg);
                          }
                        }
                      });
                  break;
                default:
                  String responseBodyEr = value.errorBody().string();
                  ErrorHandel errorHandelEr = gson.fromJson(responseBodyEr, ErrorHandel.class);
                  if(view != null) {
                    view.hideProgress();
                    view.setErrorMsg(errorHandelEr.getMessage());
                  }
                  break;
              }
            } catch (Exception e) {
              e.printStackTrace();
              if(view != null){
                view.hideProgress();
              }
            }

          }

          @Override
          public void onError(Throwable e) {
            Log.e("Error", "error" + e.getMessage());
            e.printStackTrace();
            if(view != null) {
              view.setErrorMsg(e.getMessage());
              view.hideProgress();
            }
          }

          @Override
          public void onComplete() {
          }
        });
  }
}
