package com.vaidg.inCallOutCall;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.vaidg.R;
import com.vaidg.home.AutoFitGridRecyclerView;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.AppTypeface;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.PicassoCircleTransform;
import com.vaidg.utilities.SessionManagerImpl;
import com.vaidg.utilities.Utility;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.google.gson.Gson;
import com.pojo.ErrorHandel;
import com.pojo.SlotDetails;
import com.pojo.Slots;
import com.shrikanthravi.collapsiblecalendarview.data.Day;
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar;
import com.utility.AlertProgress;
import com.utility.DialogInterfaceListner;
import com.utility.PicassoTrustAll;
import com.utility.RefreshToken;
import java.util.GregorianCalendar;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import adapters.CustomScrollDateAdapter;
import adapters.SlotsTimingAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.SESSION_EXPIRED;
import static com.vaidg.utilities.Constants.SUCCESS_RESPONSE;
import static com.vaidg.utilities.Constants.WAITFORRESPONSEFROMAPIToMQTT;
import static com.vaidg.utilities.Constants.proId;

public class TimeSlots extends DaggerAppCompatActivity implements CompactCalendarView.CompactCalendarViewListener {


    @BindView(R.id.toolbar)
    Toolbar toolBarTimeSlot;
    @BindView(R.id.tv_center)
    TextView tvCenter;
    @BindView(R.id.recyclerViewDate)
    RecyclerView recyclerViewDate;
    @BindView(R.id.scrollLeft)
    ImageView scrollLeft;
    @BindView(R.id.scrollRight)
    ImageView scrollRight;
    @BindView(R.id.recyclerViewMorning)
    AutoFitGridRecyclerView recyclerViewMorning;
    @BindView(R.id.recyclerViewAfterNoon)
    AutoFitGridRecyclerView recyclerViewAfterNoon;
    @BindView(R.id.recyclerViewEvening)
    AutoFitGridRecyclerView recyclerViewEvening;
    @BindView(R.id.recyclerViewNight)
    AutoFitGridRecyclerView recyclerViewNight;
    @BindView(R.id.llMainTimeSlots)
    LinearLayout llMainTimeSlots;
    @BindView(R.id.ivMyEventProPic)
    ImageView ivMyEventProPic;
    @BindView(R.id.tvMyEventProName)
    TextView tvMyEventProName;
    //  @BindView(R.id.viewStatus)View viewStatus;
    @BindView(R.id.tvMyEvnetProStatus)
    TextView tvMyEvnetProStatus;
    @BindView(R.id.llMorning)
    LinearLayout llMorning;
    //  @BindView(R.id.rtProvidrDtls)RatingBar rtProvidrDtls;
    @BindView(R.id.llAfterNoon)
    LinearLayout llAfterNoon;
    @BindView(R.id.llEvening)
    LinearLayout llEvening;
    @BindView(R.id.llNight)
    LinearLayout llNight;
    @BindView(R.id.llMainInfoSlots)
    LinearLayout llMainInfoSlots;
    @BindView(R.id.llNoSlotsAvailable)
    LinearLayout llNoSlotsAvailable;
    @BindView(R.id.tvSlotsNotAvailable)
    TextView tvSlotsNotAvailable;
    @BindView(R.id.compactCalendarView)
    CompactCalendarView compactCalendarView;
    @Inject
    LSPServices lspServices;
    @Inject
    SessionManagerImpl manager;
    @Inject
    Gson gson;
    @Inject
    AppTypeface appTypeface;
    @Inject
    AlertProgress alertProgress;
    Calendar startDate, endDate, defaultSelectedDate, currentTimePlus1hr, currentTime, tempTestingFromDate;
    long fromDate, toDate;
    //String proId;
    Date resultdate;
    long frommiliSeconds = 0;
    long morningMin = 0;
    long morningMax = 12;
    long afterNoonMin = 12;
    long afterNoonMax = 16;
    long eveningMin = 16;
    long eveningMax = 21;
     //long nightMin = 19;long nightMax = 23;
    int hours;
    String currentDate, selectedDate;
    ArrayList<Long> arrayList = new ArrayList<>();
    boolean isTrue = true;
    private String TAG = TimeSlots.class.getSimpleName();
    private SimpleDateFormat displayMonthFormat = new SimpleDateFormat("MMMM yyyy", Locale.US);
    private SimpleDateFormat sendingMonthFormat = new SimpleDateFormat("MM-yyyy", Locale.US);
    SimpleDateFormat formatted = new SimpleDateFormat("yyyy-MM-dd KK:mm:ss", Locale.US);
    private String currentDateSelcted = "";
    private AlertDialog dialog;
    private HorizontalCalendar horizontalCalendar;
    private CustomScrollDateAdapter customScrollDateAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_slots);
        ButterKnife.bind(this);
        getIntentValue();
        setToolBar();
        //onCreateSetDate();
        //setCalendarValue();
        //customCalendar();
        typeFace();
        tempTestingFromDate = Calendar.getInstance();
        tempTestingFromDate.setTimeZone(Utility.getTimeZone());

        currentTimePlus1hr = Calendar.getInstance();
        currentTimePlus1hr.setTimeZone(Utility.getTimeZone());
        currentTimePlus1hr.setTimeInMillis(currentTimePlus1hr.getTimeInMillis() + TimeUnit.HOURS.toMillis(1));

        /*String timeStamp = String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(Utility.getTimeZone());
        cal.add(Calendar.DATE, 0);
        fromDate = cal.getTimeInMillis() / 1000;*/
        Date date = new Date();
        Calendar currentTime = Calendar.getInstance();
        currentTime.setTimeZone(Utility.getTimeZone());
        Date currentDateTime = currentTime.getTime();
        formatted.setTimeZone(Utility.getTimeZone());
        Date newDate ;
        if(Utility.getTimeZone().getID().equals("Asia/Kolkata"))
            newDate = getDateOfTheDay(getStartOfDayInMillis(date));
            else
        newDate = getDateOfTheDay(getStartOfDayInMillisToday(date));
        fromDate = TimeUnit.MILLISECONDS.toSeconds(newDate.getTime());//getStartOfDayInMillisToday(date) / 1000;
        currentDate = formatted.format(currentDateTime);
        selectedDate = formatted.format(currentDateTime);
/*
        currentDate = formatted.format(getDateOfTheDay(getStartOfDayInMillisToday(date)));
        selectedDate = formatted.format(getDateOfTheDay(getStartOfDayInMillisToday(date)));
*/
        dialog = alertProgress.getProgressDialog(this, getString(R.string.wait));
        dialog.show();
        callApi(fromDate);

        final CollapsibleCalendar collapsibleCalendar = findViewById(R.id.calendarView);
        collapsibleCalendar.setSelectedDay(new Day(Calendar.YEAR,Calendar.MONTH,Calendar.DAY_OF_MONTH));
        collapsibleCalendar.setParams(new CollapsibleCalendar.Params(0,100));
        collapsibleCalendar.setCalendarListener(new CollapsibleCalendar.CalendarListener() {
            @Override
            public void onDayChanged() {
            }

            @Override
            public void onClickListener() {
            }

            @Override
            public void onDaySelect() {
                try {
                    Day day = collapsibleCalendar.getSelectedDay();
                    String myDate = day.getYear() + "/" + (day.getMonth() + 1) + "/" + day.getDay();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                    sdf.setTimeZone(Utility.getTimeZone());
                    Date date = sdf.parse(myDate);
                    Date newDate;
                    if(Utility.getTimeZone().getID().equals("Asia/Kolkata"))
                        newDate = getDateOfTheDay(getStartOfDayInMillis(date));
                    else
                        newDate = getDateOfTheDay(getStartOfDayInMillisToday(date));
                    fromDate = TimeUnit.MILLISECONDS.toSeconds(newDate.getTime());
                    Log.i(getClass().getName(), "Selected Day: "
                        + day.getYear() + "/" + (day.getMonth() + 1) + "/" + day.getDay() + fromDate);
                    callApi(fromDate);
                } catch (
                    ParseException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onItemClick(View v) {
            }

            @Override
            public void onDataUpdate() {
            }

            @Override
            public void onMonthChange() {
            }

            @Override
            public void onWeekChange(int position) {
            }
        });
    }

    private void callApi(long fromDate) {

        Log.d(TAG, "callApi: " + fromDate + " ProviderId " + proId + " CatID " + Constants.catId);
        if (Utility.isNetworkAvailable(this)) {
            Observable<Response<ResponseBody>> observable = lspServices.getSlotsTimes(LSPApplication.getInstance().getAuthToken(manager.getSID()), Constants.selLang, PLATFORM_ANDROID
                    , proId, Constants.catId, fromDate, Constants.callTypeInOutTele);
            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<ResponseBody> responseBodyResponse) {

                            int code = responseBodyResponse.code();
                            try {
                                String response = responseBodyResponse.body() != null
                                        ? responseBodyResponse.body().string() : null;
                                String errorBody = responseBodyResponse.errorBody() != null
                                        ? responseBodyResponse.errorBody().string() : null;

                                switch (code) {
                                    case SUCCESS_RESPONSE:
                                        if (response != null && !response.isEmpty()) {
                                            Log.d(TAG, "onNextResponseTime: " + response);
                                            SlotDetails slotDetails = gson.fromJson(response,
                                                    SlotDetails.class);
                                            slotDet(slotDetails.getData());
                                        }
                                        break;
                                    case SESSION_EXPIRED:
                                        if (errorBody != null && !errorBody.isEmpty()) {
                                            ErrorHandel errorHandel = gson.fromJson(errorBody, ErrorHandel.class);
                                            RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),
                                                    manager.getREFRESHAUTH(), lspServices,
                                                    new RefreshToken.RefreshTokenImple() {
                                                        @Override
                                                        public void onSuccessRefreshToken(
                                                                String newToken) {
                                                            LSPApplication.getInstance().setAuthToken(manager.getSID(), manager.getSID(), newToken);
                                                            callApi(fromDate);
                                                        }

                                                        @Override
                                                        public void onFailureRefreshToken() {

                                                        }

                                                        @Override
                                                        public void sessionExpired(String msg) {
                                                            alertProgress.alertPositiveOnclick(
                                                                    TimeSlots.this, msg,
                                                                    getString(R.string.logout),
                                                                    getString(R.string.ok),
                                                                    new DialogInterfaceListner() {
                                                                        @Override
                                                                        public void dialogClick(
                                                                                boolean isClicked) {
                                                                            Utility.setMAnagerWithBID(
                                                                                    TimeSlots.this, manager);
                                                                        }
                                                                    });
                                                        }
                                                    });
                                        }
                                        break;
                                    case Constants.SESSION_LOGOUT:
                                        if (errorBody != null && !errorBody.isEmpty()) {
                                            if (dialog != null && dialog.isShowing())
                                                dialog.dismiss();
                                            alertProgress.alertPositiveOnclick(TimeSlots.this, new JSONObject(response).getString("message"),
                                                    getString(R.string.logout), getString(R.string.ok), new DialogInterfaceListner() {
                                                        @Override
                                                        public void dialogClick(boolean isClicked) {
                                                            Utility.setMAnagerWithBID(TimeSlots.this, manager);
                                                        }
                                                    });
                                        }
                                        break;
                                    default:
                                        if (dialog != null && dialog.isShowing())
                                            dialog.dismiss();
                                        break;
                                }
                            } catch (Exception e) {
                                if (dialog != null && dialog.isShowing())
                                    dialog.dismiss();
                                e.printStackTrace();
                            }

                        }

                        @Override
                        public void onError(Throwable e) {

                            if (dialog != null && dialog.isShowing())
                                dialog.dismiss();
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }//else
        //Utility.checkAndShowNetworkError(this);
    }


    private void getIntentValue() {

        Bundle bundle = getIntent().getExtras();
        String image = bundle != null ? bundle.getString("ImagePro") : "";
        String name = bundle != null ? bundle.getString("NAME") : "";
        proId = bundle != null ? bundle.getString("ProviderId") : "";
        String expertise = bundle != null ? bundle.getString("expertise") : "";
        if(image != null && !image.isEmpty()) {
            PicassoTrustAll.getInstance(TimeSlots.this)
                .load(image)
                .placeholder(R.drawable.register_profile_default_image)   // optional
                .error(R.drawable.register_profile_default_image)// optional
                .transform(new PicassoCircleTransform())
                .into(ivMyEventProPic);
        }
        tvMyEventProName.setText(name);
        tvMyEvnetProStatus.setText(expertise);
    }

    private void setToolBar() {
        //tvCenter.setText(ConstantsInteface.catName);
        tvCenter.setText(getString(R.string.selectTimeSlots));
        setSupportActionBar(toolBarTimeSlot);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolBarTimeSlot.setNavigationIcon(R.drawable.ic_back);
        toolBarTimeSlot.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void typeFace() {
        recyclerViewMorning.setNestedScrollingEnabled(false);
        recyclerViewAfterNoon.setNestedScrollingEnabled(false);
        recyclerViewEvening.setNestedScrollingEnabled(false);
        recyclerViewNight.setNestedScrollingEnabled(false);
        AppTypeface appTypeface = AppTypeface.getInstance(this);
        tvCenter.setTypeface(appTypeface.getHind_semiBold());
        tvMyEventProName.setTypeface(appTypeface.getHind_medium());
        tvSlotsNotAvailable.setTypeface(appTypeface.getHind_semiBold());
        tvMyEvnetProStatus.setTypeface(appTypeface.getHind_regular());

        tvMyEventProName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_16));
        tvMyEvnetProStatus.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_13));
    }

    private void onCreateSetDate() {
        compactCalendarView.setUseThreeLetterAbbreviation(true);
        compactCalendarView.setDayColumnNames(getResources().getStringArray(R.array.calendarDays));
        compactCalendarView.setListener(this);
        currentDateSelcted = sendingMonthFormat.format(compactCalendarView.getFirstDayOfCurrentMonth());
        startDate = Calendar.getInstance();
        startDate.setTimeZone(Utility.getTimeZone());
        startDate.add(Calendar.DAY_OF_MONTH, -1);

        endDate = Calendar.getInstance();
        endDate.setTimeZone(Utility.getTimeZone());
        endDate.add(Calendar.DAY_OF_MONTH, 15);

        defaultSelectedDate = Calendar.getInstance();
        defaultSelectedDate.setTimeZone(Utility.getTimeZone());

        tempTestingFromDate = Calendar.getInstance();
        tempTestingFromDate.setTimeZone(Utility.getTimeZone());

        currentTime = Calendar.getInstance();
        currentTime.setTimeZone(Utility.getTimeZone());

        currentTimePlus1hr = Calendar.getInstance();
        currentTimePlus1hr.setTimeZone(Utility.getTimeZone());
        currentTimePlus1hr.setTimeInMillis(currentTimePlus1hr.getTimeInMillis() + TimeUnit.HOURS.toMillis(1));

        hours = timeMethod(System.currentTimeMillis() / 1000);
        Log.d(TAG, "onCreateSetDate: startdate: " + startDate + " End date: " + endDate + " hours: " + hours);
        if (hours == 23) {
            hours = 0;
        } else {
            hours = hours + 1;
            Log.d(TAG, "onCreateSetDate: startdate: " + startDate + " End date: " + endDate + " hours: hoursIncreased: " + hours);
        }

    }

    private void customCalendar() {
        arrayList.clear();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewDate.setLayoutManager(linearLayoutManager);
        Calendar cal = Calendar.getInstance();
        int j = 0;
        for (int i = 0; i < 5; i++) {
            if (i == 1) {
                j++;
            }
            cal.add(Calendar.DATE, j);
            arrayList.add(cal.getTimeInMillis() / 1000);
        }
        customScrollDateAdapter = new CustomScrollDateAdapter(arrayList);
        recyclerViewDate.setAdapter(customScrollDateAdapter);

        scrollLeft.setOnClickListener(v -> {
            compactCalendarView.showPreviousMonth();
            if (linearLayoutManager.findFirstVisibleItemPosition() > 0) {
                recyclerViewDate.smoothScrollToPosition(linearLayoutManager.findFirstVisibleItemPosition() - 1);
                int pos = linearLayoutManager.findLastVisibleItemPosition() - 1;
                fromDate = arrayList.get(pos);
                Date date = new Date(TimeUnit.SECONDS.toMillis(fromDate));
                //  fromDate = getStartOfDayInMillis(date)/1000;
                fromDate = getStartOfDayInMillisToday(date) / 1000;
                selectedDate = formatted.format(getDateOfTheDay(getStartOfDayInMillisToday(date)));

                callApi(fromDate);

            } else {
                recyclerViewDate.smoothScrollToPosition(0);
                scrollLeft.setVisibility(View.INVISIBLE);
            }
            if (linearLayoutManager.findFirstVisibleItemPosition() == 0)
                scrollLeft.setVisibility(View.INVISIBLE);


        });

        scrollRight.setOnClickListener(v -> {
            compactCalendarView.showNextMonth();
            if (linearLayoutManager.findLastVisibleItemPosition() < arrayList.size() - 1) {
                fromDate = arrayList.get(linearLayoutManager.findLastVisibleItemPosition());

                recyclerViewDate.smoothScrollToPosition(linearLayoutManager.findLastVisibleItemPosition() + 1);
                Date date = new Date(TimeUnit.SECONDS.toMillis(fromDate));
                fromDate = getStartOfDayInMillis(date) / 1000;
                selectedDate = formatted.format(getDateOfTheDay(getStartOfDayInMillis(date)));
                isTrue = true;

                callApi(fromDate);
            } else if (linearLayoutManager.findLastVisibleItemPosition() == arrayList.size()) {
                if (isTrue) {
                    fromDate = arrayList.get(linearLayoutManager.findLastVisibleItemPosition());
                    Date date = new Date(TimeUnit.SECONDS.toMillis(fromDate));
                    fromDate = getStartOfDayInMillis(date) / 1000;
                    selectedDate = formatted.format(getDateOfTheDay(getStartOfDayInMillis(date)));
                    callApi(fromDate);
                    isTrue = false;
                }
            }
            if (linearLayoutManager.findLastVisibleItemPosition() == 0)
                scrollLeft.setVisibility(View.VISIBLE);
        });
    }

    private void setCalendarValue() {

        /* start 2 months ago from now */


     /*   horizontalCalendar = new HorizontalCalendar.Builder(llMainTimeSlots, R.id.calendarViewSlot)
                .range(startDate, endDate)
                .datesNumberOnScreen(7)
                .configure()
                .formatTopText("MMM")
                .formatMiddleText("dd")
                .formatBottomText("EEE")
                .showTopText(true)
                .showBottomText(true)
                .textColor(Color.parseColor("#3A3A3A"),Color.parseColor("#1A54B0"))
                .colorTextMiddle(Color.parseColor("#3A3A3A"), Color.parseColor("#1A54B0"))
                .end()
                .defaultSelectedDate(this.defaultSelectedDate)

                .build();

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                //   SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d, yyyy", Locale.US);
                // String selectedDateStr =  sdf.format(date);

                String selectedDateStr = DateFormat.format("EEE, MMM d, yyyy", date).toString();
                Toast.makeText(TimeSlots.this, selectedDateStr, Toast.LENGTH_SHORT).show();

                //  defaultSelectedDate
                horizontalCalendar.post(() -> {

                    fromDate = getStartOfDayInMillis(date)/1000;
                    toDate = getEndOfDayInMillis(date);

                    selectedDate = formatted.format(getDateOfTheDay(getStartOfDayInMillis(date)));
                    dialog = alertProgress.getProgressDialog(TimeSlots.this,getString(R.string.wait));
                    dialog.show();
                    callApi(fromDate);

                    //  mListener.onDateSelectedApi(fromDate,toDate,true);
                });

            }

        });*/
    }

    public long getStartOfDayInMillisToday(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(Utility.getTimeZone());
        calendar.setTime(date);//.getTime()
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    private Date getDateOfTheDay(long startOfDayInMillisToday) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(Utility.getTimeZone());
        calendar.setTimeInMillis(startOfDayInMillisToday);
        return calendar.getTime();
    }

    public long getStartOfDayInMillis(Date date) {
        Calendar calendar = Calendar.getInstance();//
        calendar.setTimeZone(Utility.getTimeZone());
        calendar.setTime(date);//.getTime()
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        //  if(isStartTime)
        return calendar.getTimeInMillis() + (24 * 60 * 60 * 1000);
       /* else
            return calendar.getTimeInMillis();*/
    }

    public long getEndOfDayInMillis(Calendar date) {
        return getStartOfDayInMillis(date.getTime()) + (24 * 60 * 60 * 1000) - 1000;
    }


    private void slotDet(ArrayList<Slots> slotDetails) {
        ArrayList<Slots> morningSlots = new ArrayList<>();
        ArrayList<Slots> afterNoonSlots = new ArrayList<>();
        ArrayList<Slots> eveningSlots = new ArrayList<>();
        ArrayList<Slots> nightSlots = new ArrayList<>();
        for (int i = 0; i < slotDetails.size(); i++) {
            int hr = timeMethod(slotDetails.get(i).getFrom());
            if (hr >= morningMin && hr < morningMax) {
                if (currentDate.equals(selectedDate)) {
                    tempTestingFromDate.setTimeInMillis(TimeUnit.SECONDS.toMillis(slotDetails.get(i).getFrom()));
                    Log.d(TAG, "slotDet: temptestingFromDate:" + tempTestingFromDate.getTimeInMillis() + " currenttimePlushr:" + currentTimePlus1hr.getTimeInMillis());
                    Log.d(TAG, "slotDet: " + tempTestingFromDate.after(currentTimePlus1hr));
                    if (currentTimePlus1hr.after(tempTestingFromDate))//if(hr<hours)
                        slotDetails.get(i).setIsBook(0);
                }
                if (slotDetails.get(i).getIsBook() == 1)
                    morningSlots.add(slotDetails.get(i));
            } else if (hr >= afterNoonMin && hr < afterNoonMax) {
                if (currentDate.equals(selectedDate)) {
                    tempTestingFromDate.setTimeInMillis(TimeUnit.SECONDS.toMillis(slotDetails.get(i).getFrom()));
                    if (currentTimePlus1hr.after(tempTestingFromDate))//if(hr<hours)
                        slotDetails.get(i).setIsBook(0);
                }
                if (slotDetails.get(i).getIsBook() == 1)
                    afterNoonSlots.add(slotDetails.get(i));
            } else if (hr >= eveningMin && hr < eveningMax) {
                if (currentDate.equals(selectedDate)) {
                    tempTestingFromDate.setTimeInMillis(TimeUnit.SECONDS.toMillis(slotDetails.get(i).getFrom()));
                    if (currentTimePlus1hr.after(tempTestingFromDate))//if(hr<hours)
                        slotDetails.get(i).setIsBook(0);
                }
                if (slotDetails.get(i).getIsBook() == 1)
                    eveningSlots.add(slotDetails.get(i));
            } else {
                if (currentDate.equals(selectedDate)) {
                    tempTestingFromDate.setTimeInMillis(TimeUnit.SECONDS.toMillis(slotDetails.get(i).getFrom()));
                    if (currentTimePlus1hr.after(tempTestingFromDate))//if(hr<hours)
                        slotDetails.get(i).setIsBook(0);
                }
                if (slotDetails.get(i).getIsBook() == 1)
                    nightSlots.add(slotDetails.get(i));
            }

        }

        GridLayoutManager gridLayoutManagerM = new GridLayoutManager(this, 3);
        SlotsTimingAdapter adapter = new SlotsTimingAdapter(false, morningSlots);
        recyclerViewMorning.setLayoutManager(gridLayoutManagerM);
        recyclerViewMorning.setAdapter(adapter);


        if (morningSlots.size() > 0) {
            recyclerViewMorning.setVisibility(View.VISIBLE);
            llMorning.setVisibility(View.VISIBLE);
            llMorning.removeAllViews();
            TextView tvDayTime, tvDayTimeSlotsNo;
            ImageView ivPeriod;
            View v = LayoutInflater.from(this).inflate(R.layout.time_slot_info_header, llMainInfoSlots, false);
            llMorning.addView(v);
            tvDayTime = v.findViewById(R.id.tvDayTime);
            tvDayTimeSlotsNo = v.findViewById(R.id.tvDayTimeSlotsNo);
            ivPeriod = v.findViewById(R.id.ivPeriod);
            tvDayTime.setTypeface(appTypeface.getHind_semiBold());
            tvDayTimeSlotsNo.setTypeface(appTypeface.getHind_semiBold());
            tvDayTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_14));
            tvDayTimeSlotsNo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_10));
            tvDayTimeSlotsNo.setText(morningSlots.size() + " Slots");
            tvDayTime.setText(getString(R.string.morning_slots));
            ivPeriod.setImageResource(R.drawable.ic_morning);
            adapter.notifyDataSetChanged();
        } else {
            recyclerViewMorning.setVisibility(View.GONE);
            llMorning.setVisibility(View.GONE);
            llMorning.removeAllViews();
        }

        GridLayoutManager gridLayoutManagerA = new GridLayoutManager(this, 3);

        SlotsTimingAdapter adapterAfterNoon = new SlotsTimingAdapter(false, afterNoonSlots);
        recyclerViewAfterNoon.setLayoutManager(gridLayoutManagerA);
        recyclerViewAfterNoon.setAdapter(adapterAfterNoon);

        if (afterNoonSlots.size() > 0) {
            recyclerViewAfterNoon.setVisibility(View.VISIBLE);
            llAfterNoon.setVisibility(View.VISIBLE);
            llAfterNoon.removeAllViews();
            TextView tvDayTime, tvDayTimeSlotsNo;
            ImageView ivPeriod;
            View v = LayoutInflater.from(this).inflate(R.layout.time_slot_info_header, llMainInfoSlots, false);
            llAfterNoon.addView(v);
            tvDayTime = v.findViewById(R.id.tvDayTime);
            tvDayTimeSlotsNo = v.findViewById(R.id.tvDayTimeSlotsNo);
            ivPeriod = v.findViewById(R.id.ivPeriod);
            tvDayTime.setTypeface(appTypeface.getHind_semiBold());
            tvDayTimeSlotsNo.setTypeface(appTypeface.getHind_semiBold());
            tvDayTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_14));
            tvDayTimeSlotsNo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_10));
            tvDayTimeSlotsNo.setText(afterNoonSlots.size() + " Slots");
            tvDayTime.setText(getString(R.string.after_noon_slots));
            ivPeriod.setImageResource(R.drawable.ic_afternoon);
            adapterAfterNoon.notifyDataSetChanged();

        } else {
            recyclerViewAfterNoon.setVisibility(View.GONE);
            llAfterNoon.setVisibility(View.GONE);
            llAfterNoon.removeAllViews();
        }
        GridLayoutManager gridLayoutManagerE = new GridLayoutManager(this, 3);

        SlotsTimingAdapter adapterEvening = new SlotsTimingAdapter(false, eveningSlots);
        recyclerViewEvening.setLayoutManager(gridLayoutManagerE);
        recyclerViewEvening.setAdapter(adapterEvening);

        if (eveningSlots.size() > 0) {
            recyclerViewEvening.setVisibility(View.VISIBLE);
            llEvening.setVisibility(View.VISIBLE);
            llEvening.removeAllViews();
            TextView tvDayTime, tvDayTimeSlotsNo;
            ImageView ivPeriod;
            View v = LayoutInflater.from(this).inflate(R.layout.time_slot_info_header, llMainInfoSlots, false);
            llEvening.addView(v);
            tvDayTime = v.findViewById(R.id.tvDayTime);
            ivPeriod = v.findViewById(R.id.ivPeriod);
            tvDayTimeSlotsNo = v.findViewById(R.id.tvDayTimeSlotsNo);
            tvDayTime.setTypeface(appTypeface.getHind_semiBold());
            tvDayTimeSlotsNo.setTypeface(appTypeface.getHind_semiBold());
            tvDayTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_14));
            tvDayTimeSlotsNo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_10));
            tvDayTimeSlotsNo.setText(eveningSlots.size() + " Slots");
            tvDayTime.setText(getString(R.string.evening_slots));
            ivPeriod.setImageResource(R.drawable.ic_evening);
            adapterEvening.notifyDataSetChanged();


        } else {
            llEvening.setVisibility(View.GONE);
            recyclerViewEvening.setVisibility(View.GONE);
            llEvening.removeAllViews();
        }
        GridLayoutManager gridLayoutManagerN = new GridLayoutManager(this, 3);

        SlotsTimingAdapter adapterNight = new SlotsTimingAdapter(false, nightSlots);
        recyclerViewNight.setLayoutManager(gridLayoutManagerN);
        recyclerViewNight.setAdapter(adapterNight);

        if (nightSlots.size() > 0) {
            llNight.setVisibility(View.VISIBLE);
            recyclerViewNight.setVisibility(View.VISIBLE);
            llNight.removeAllViews();
            TextView tvDayTime, tvDayTimeSlotsNo;
            ImageView ivPeriod;
            View v = LayoutInflater.from(this).inflate(R.layout.time_slot_info_header, llMainInfoSlots, false);
            llNight.addView(v);
            tvDayTime = v.findViewById(R.id.tvDayTime);
            ivPeriod = v.findViewById(R.id.ivPeriod);
            tvDayTimeSlotsNo = v.findViewById(R.id.tvDayTimeSlotsNo);
            tvDayTime.setTypeface(appTypeface.getHind_semiBold());
            tvDayTimeSlotsNo.setTypeface(appTypeface.getHind_semiBold());
            tvDayTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_14));
            tvDayTimeSlotsNo.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimensionPixelSize(R.dimen.sp_10));
            tvDayTimeSlotsNo.setText(nightSlots.size() + " Slots");
            tvDayTime.setText(getString(R.string.night_slots));
            ivPeriod.setImageResource(R.drawable.ic_night);
            adapterNight.notifyDataSetChanged();

        } else {
            recyclerViewNight.setVisibility(View.GONE);
            llNight.setVisibility(View.GONE);
            llNight.removeAllViews();
        }
        if (morningSlots.size() == 0 && afterNoonSlots.size() == 0 && eveningSlots.size() == 0 && nightSlots.size() == 0)
            llNoSlotsAvailable.setVisibility(View.VISIBLE);
        else {
            llNoSlotsAvailable.setVisibility(View.GONE);
        }

        if (dialog != null && dialog.isShowing())
            dialog.dismiss();


    }

    /**
     * <h2>timeMethod</h2>
     *
     * @param bookingRequestedFor
     * @return the hour value of timestamp
     */
    public int timeMethod(long bookingRequestedFor) {
        int hour = 0;
        try {
            Date date = new Date(TimeUnit.SECONDS.toMillis(bookingRequestedFor));
            //   SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
            SimpleDateFormat sdf = new SimpleDateFormat("HH", Locale.US);
            //  int hour = sdf
            sdf.setTimeZone(Utility.getTimeZone());
            String formattedDate = sdf.format(date);
            Date d = sdf.parse(formattedDate);

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeZone(Utility.getTimeZone());
            if (d != null) {
                calendar.setTime(d);
            }
            Log.d(TAG, "timeMethod: date.getHours:" + calendar.get(Calendar.HOUR_OF_DAY));
            hour = calendar.get(Calendar.HOUR_OF_DAY);


        } catch (Exception e) {
            Log.d("TAG", "timeMethodException: " + e.toString());
        }
        return hour;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        WAITFORRESPONSEFROMAPIToMQTT = 1;
        finish();
        //  overridePendingTransition(R.anim.mainfadein,R.anim.slide_down_acvtivity);
        overridePendingTransition(R.anim.mainfadein, R.anim.side_slide_in);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        //Utility.checkAndShowNetworkError(this);
    }

    @Override
    public void onDayClick(Date dateClicked) {

    }

    @Override
    public void onMonthScroll(Date firstDayOfNewMonth) {

    }
}
