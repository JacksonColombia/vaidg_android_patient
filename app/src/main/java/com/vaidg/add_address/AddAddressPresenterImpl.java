package com.vaidg.add_address;

import android.util.Log;

import com.vaidg.model.ServerResponse;
import com.vaidg.networking.LSPServices;
import com.vaidg.utilities.Constants;
import com.vaidg.utilities.LSPApplication;
import com.vaidg.utilities.SessionManagerImpl;
import com.google.gson.Gson;
import com.pojo.ErrorHandel;
import com.utility.RefreshToken;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.vaidg.utilities.Constants.MESSAGE;
import static com.vaidg.utilities.Constants.PLATFORM_ANDROID;
import static com.vaidg.utilities.Constants.USERTYPE;
import static com.vaidg.utilities.Constants.selLang;

/**
 * Created by Pramod on 19/12/17.
 */

public class AddAddressPresenterImpl implements AddAddressPresenter {

    @Inject
    AddressView view;

    @Inject
    CompositeDisposable compositeDisposable;

    @Inject
    LSPServices lspServices;

    @Inject
    Gson gson;
    @Inject
    SessionManagerImpl manager;


    @Inject
    AddAddressPresenterImpl() {
        //this.compositeDisposable = new CompositeDisposable();
    }


    @Override
    public void addAddress(String auth, String addLine1, String addLine2, String city, String state,
                           String country, String placeId, String pinCode, Double latitude, Double longitude,
                           String taggedAs, Integer userType, String houseNo, String addressName) {


        Map<String, Object> addaddress = new HashMap<>();
        addaddress.put("addLine1", addLine1);
        addaddress.put("addLine2", addLine2);
        addaddress.put("houseNo", houseNo);
        addaddress.put("name", addressName);
        addaddress.put("city", city);
        addaddress.put("state", state);
        addaddress.put("country", country);
        addaddress.put("placeId", placeId);
        addaddress.put("pincode", pinCode);
        addaddress.put("latitude", latitude);
        addaddress.put("longitude", longitude);
        addaddress.put("taggedAs", taggedAs);
        addaddress.put("neighbourHood","");
        addaddress.put("reference","");

        addaddress.put(USERTYPE, userType);

        Observable<Response<ResponseBody>> response = lspServices.addAddress(auth,selLang, PLATFORM_ANDROID,
                addaddress);
        response.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Response<ResponseBody> value) {
                        int code = value.code();
                        JSONObject errJson;
                        try {
                            String response =
                                    value.body() != null ? value.body().string() : null;
                            String errorBody =
                                    value.errorBody() != null ? value.errorBody().string() : null;
                            switch (code) {
                                case Constants.SUCCESS_RESPONSE:
                                    if (response != null && !response.isEmpty()) {
                                        ServerResponse serverResponse = gson.fromJson(response,
                                                ServerResponse.class);
                                        if (serverResponse.getMessage() != null && !serverResponse.getMessage().isEmpty())
                                        {
                                          if(view != null) {
                                            view.navToAddressScreen();
                                          }
                                    }
                                    }
                                  if(view != null) {
                                    view.hideProgress();
                                  }
                                    break;
                                case Constants.SESSION_LOGOUT:
                                    if (errorBody != null && !errorBody.isEmpty()) {
                                        errJson = new JSONObject(errorBody);
                                      errJson.getString(MESSAGE);
                                      if (!errJson.getString(MESSAGE).isEmpty()) {
                                        if(view != null) {
                                          view.logout(errJson.getString(MESSAGE));
                                        }
                                        }
                                    }
                                  if(view != null) {
                                    view.hideProgress();
                                  }
                                    break;
                                case Constants.SESSION_EXPIRED:
                                    if (errorBody != null && !errorBody.isEmpty()) {
                                        ErrorHandel errorHandel = gson.fromJson(errorBody,
                                                ErrorHandel.class);
                                        RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()),
                                                manager.getREFRESHAUTH(), lspServices,
                                                new RefreshToken.RefreshTokenImple() {
                                                    @Override
                                                    public void onSuccessRefreshToken(String newToken) {
                                                      if(view != null) {
                                                        view.hideProgress();
                                                      }
                                                        LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                                        addAddress(newToken, addLine1, addLine2, city,
                                                                state, country,
                                                                placeId, pinCode, latitude, longitude, taggedAs,
                                                                userType, houseNo, addressName);

                                                    }

                                                    @Override
                                                    public void onFailureRefreshToken() {

                                                    }

                                                    @Override
                                                    public void sessionExpired(String msg) {
                                                      if(view != null) {
                                                        view.hideProgress();
                                                        view.logout(msg);
                                                      }
                                                    }
                                                });
                                    }
                                    break;
                                default:
                                    if (errorBody != null && !errorBody.isEmpty()) {
                                        errJson = new JSONObject(errorBody);
                                      errJson.getString(MESSAGE);
                                      if (!errJson.getString(MESSAGE).isEmpty()) {
                                        if(view != null) {
                                          view.setError(errJson.getString(MESSAGE));
                                        }
                                        }
                                    }
                                  if(view != null) {
                                    view.hideProgress();
                                  }
                                    break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                          if(view != null) {
                            view.hideProgress();
                          }
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                      if(view != null) {
                        view.hideProgress();
                      }
                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }

    @Override
    public void editAddress(String auth, String addressId, String houseNo, String addressName,
                            String addrLine1, String addrLine2, String city, String state, String country, String s,
                            String pinCode, double new_lat, double new_long, String tag, int i) {

        Log.d("TAG", "editAddress: " + tag);
        Map<String, Object> editaddress = new HashMap<>();
        editaddress.put("id", addressId);
        editaddress.put("addLine1", addrLine1);
        editaddress.put("addLine2", addrLine2);
        editaddress.put("houseNo", houseNo);
        editaddress.put("name", addressName);
        editaddress.put("city", city);
        editaddress.put("state", state);
        editaddress.put("country", country);
        editaddress.put("placeId", s);
        editaddress.put("pincode", pinCode);
        editaddress.put("latitude", new_lat);
        editaddress.put("longitude", new_long);
        editaddress.put("taggedAs", tag);
        editaddress.put("neighbourHood","");
        editaddress.put("reference","");

        editaddress.put(USERTYPE, i);
        Observable<Response<ResponseBody>> observable = lspServices.editAddress(auth, Constants.selLang,
                PLATFORM_ANDROID, editaddress);

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {

                        int code = responseBodyResponse.code();
                        JSONObject errJson;
                        Log.d("TAG", "onNextResponseBody: " + code);

                        try {
                            String response =
                                    responseBodyResponse.body() != null ? responseBodyResponse.body().string() : null;
                            String errorBody =
                                    responseBodyResponse.errorBody() != null ? responseBodyResponse.errorBody().string() : null;
                            switch (code) {
                                case Constants.SUCCESS_RESPONSE:
                                    if (response != null && !response.isEmpty()) {
                                      if(view != null) {
                                        view.navToAddressScreen();
                                      }
                                    }
                                  if(view != null) {
                                    view.hideProgress();
                                  }
                                    break;
                                case Constants.SESSION_LOGOUT:
                                    if (errorBody != null && !errorBody.isEmpty()) {
                                        errJson = new JSONObject(errorBody);
                                      errJson.getString(MESSAGE);
                                      if (!errJson.getString(MESSAGE).isEmpty()) {
                                        if(view != null) {
                                          view.logout(errJson.getString(MESSAGE));
                                        }
                                        }
                                    }
                                  if(view != null) {
                                    view.hideProgress();
                                  }
                                    break;
                                case Constants.SESSION_EXPIRED:
                                    if (errorBody != null && !errorBody.isEmpty()) {
                                        ErrorHandel errorHandel = gson.fromJson(response, ErrorHandel.class);
                                        RefreshToken.onRefreshToken(LSPApplication.getInstance().getAuthToken(manager.getSID()), manager.getREFRESHAUTH(),
                                                lspServices,
                                                new RefreshToken.RefreshTokenImple() {
                                                    @Override
                                                    public void onSuccessRefreshToken(String newToken) {
                                                      if(view != null) {
                                                        view.hideProgress();
                                                      }
                                                        LSPApplication.getInstance().setAuthToken(manager.getSID(),manager.getSID(),newToken);
                                                        editAddress(newToken, addressId, houseNo, addressName, addrLine1,
                                                                addrLine2,
                                                                city, state, country, s, pinCode, new_lat, new_long, tag, i);
                                                    }

                                                    @Override
                                                    public void onFailureRefreshToken() {

                                                    }

                                                    @Override
                                                    public void sessionExpired(String msg) {
                                                      if(view != null) {
                                                        view.hideProgress();
                                                        view.logout(msg);
                                                      }
                                                    }
                                                });
                                    }
                                    break;
                                default:
                                    if (errorBody != null && !errorBody.isEmpty()) {
                                        errJson = new JSONObject(errorBody);
                                      errJson.getString(MESSAGE);
                                      if (!errJson.getString(MESSAGE).isEmpty()) {
                                        if(view != null) {
                                          view.setError(errJson.getString(MESSAGE));
                                        }
                                        }
                                    }
                                  if(view != null) {
                                    view.hideProgress();
                                  }
                                    break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                          if(view != null) {
                            view.hideProgress();
                          }
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                      if(view != null) {
                        view.hideProgress();
                      }
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }
}
