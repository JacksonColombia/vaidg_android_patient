package com.pojo;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * Created by Ali on 2/7/2018.
 */
public class JitsiMeet implements Parcelable
{

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public Data data;
    public final static Parcelable.Creator<JitsiMeet> CREATOR = new Creator<JitsiMeet>() {



        @SuppressWarnings({
            "unchecked"
        })
        public JitsiMeet createFromParcel(Parcel in) {
            return new JitsiMeet(in);
        }

        public JitsiMeet[] newArray(int size) {
            return (new JitsiMeet[size]);
        }

    };

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    protected JitsiMeet(Parcel in) {
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.data = ((Data) in.readValue((Data.class.getClassLoader())));
    }

    public JitsiMeet() {
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }


    public class Data implements Parcelable
    {

        @SerializedName("token")
        @Expose
        public String token;
        @SerializedName("expiryTime")
        @Expose
        public Long expiryTime;
        @SerializedName("expiryTimeInString")
        @Expose
        public String expiryTimeInString;
        public final Parcelable.Creator<Data> CREATOR = new Creator<Data>() {


            @SuppressWarnings({
                "unchecked"
            })
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            public Data[] newArray(int size) {
                return (new Data[size]);
            }

        }
            ;

        protected Data(Parcel in) {
            this.token = ((String) in.readValue((String.class.getClassLoader())));
            this.expiryTime = ((Long) in.readValue((Long.class.getClassLoader())));
            this.expiryTimeInString = ((String) in.readValue((String.class.getClassLoader())));
        }

        public Data() {
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeValue(token);
            dest.writeValue(expiryTime);
            dest.writeValue(expiryTimeInString);
        }

        public int describeContents() {
            return 0;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public Long getExpiryTime() {
            return expiryTime;
        }

        public void setExpiryTime(Long expiryTime) {
            this.expiryTime = expiryTime;
        }

        public String getExpiryTimeInString() {
            return expiryTimeInString;
        }

        public void setExpiryTimeInString(String expiryTimeInString) {
            this.expiryTimeInString = expiryTimeInString;
        }
    }

}

/*
package com.pojo;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

*/
/**
 * Created by Ali on 2/7/2018.
 *//*

public class JitsiMeet implements Serializable {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data  data;

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public class Data  implements Serializable{
        @SerializedName("token")
        @Expose
        private String token;
        @SerializedName("expiryTime")
        @Expose
        private String expiryTime;
        @SerializedName("expiryTimeInString")
        @Expose
        private String expiryTimeInString;

        public String getToken() {
            return token;
        }

        public String getExpiryTime() {
            return expiryTime;
        }

        public String getExpiryTimeInString() {
            return expiryTimeInString;
        }
    }
}
*/
