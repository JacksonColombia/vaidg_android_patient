package com.pojo.address_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h2>GoemetryCordinate</h2>
 * Created by user on 6/12/2017.
 */

public class GoemetryCordinate implements Serializable
{
    @SerializedName("location")
    @Expose
    private ProviderLocation location;


    public ProviderLocation getLocation() {
        return location;
    }
}
