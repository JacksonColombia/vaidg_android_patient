package com.pojo.address_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h2>Drop_Location_Google_Pojo</h2>
 * Created by embed on 27/3/17.
 */

public class Drop_Location_Google_Pojo implements Serializable
{
  //  private Result_Google_Pojo result;
  @SerializedName("result")
  @Expose
   private Result_Data result;
    @SerializedName("status")
    @Expose
    private String status;


    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public Result_Data getResult() {
        return result;
    }

    /* @Override
    public String toString()
    {
        return "ClassPojo [result = "+result+", html_attributions = "+html_attributions+", status = "+status+"]";
    }*/
}
