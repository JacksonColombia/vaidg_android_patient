package com.pojo.address_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h2>Result_Data</h2>
 * Created by user on 6/12/2017.
 */

public class Result_Data implements Serializable
{
    @SerializedName("formatted_address")
    @Expose
    private String formatted_address;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("geometry")
    @Expose
    private GoemetryCordinate geometry;

    public String getFormatted_address() {
        return formatted_address;
    }

    public String getName() {
        return name;
    }

    public GoemetryCordinate getGeometry() {
        return geometry;
    }
}
