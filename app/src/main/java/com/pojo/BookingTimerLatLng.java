package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h2>BookingTimerLatLng</h2>
 * Created by Ali on 2/19/2018.
 */

public class BookingTimerLatLng
{
    @SerializedName("bookingTimer")
    @Expose
    private BookingTimer bookingTimer;

   // private ProLocation providerLocation;
   @SerializedName("jobTimerStatus")
   @Expose
    private String jobTimerStatus;
    @SerializedName("status")
    @Expose
    private int status;

    public String getJobTimerStatus() {
        return jobTimerStatus;
    }

    public void setJobTimerStatus(String jobTimerStatus) {
        this.jobTimerStatus = jobTimerStatus;
    }

    public BookingTimer getBookingTimer() {
        return bookingTimer;
    }

    public void setBookingTimer(BookingTimer bookingTimer) {
        this.bookingTimer = bookingTimer;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    /*public ProLocation getProviderLocation() {
        return providerLocation;
    }

    public void setProviderLocation(ProLocation providerLocation) {
        this.providerLocation = providerLocation;
    }*/
}
