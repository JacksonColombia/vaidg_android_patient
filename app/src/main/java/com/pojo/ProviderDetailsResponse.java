package com.pojo;

import static com.vaidg.utilities.Constants.DATA;
import static com.vaidg.utilities.Constants.MESSAGE;

import com.vaidg.model.Location;
import com.vaidg.youraddress.model.YourAddrData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h2>ProviderDetailsResponse</h2>
 * Created by Ali on 2/5/2018.
 */

public class ProviderDetailsResponse implements Serializable {

  @SerializedName(MESSAGE)
  @Expose
  private String message;
  @SerializedName(DATA)
  @Expose
  private ProviderResponseDetails data;

  /**
   * No args constructor for use in serialization
   */
  public ProviderDetailsResponse() {
  }

  /**
   * @param data
   * @param message
   */
  public ProviderDetailsResponse(String message, ProviderResponseDetails data) {
    super();
    this.message = message;
    this.data = data;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public ProviderResponseDetails getData() {
    return data;
  }

  public void setData(ProviderResponseDetails data) {
    this.data = data;
  }


  public class ProviderResponseDetails implements Serializable {

    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("bannerImage")
    @Expose
    private String bannerImage;
    /*
        @SerializedName("lastActive")
        @Expose
        private int lastActive;
    */
/*    @SerializedName("photo")
    @Expose
    private String photo;*/
    @SerializedName("noOfReview")
    @Expose
    private int noOfReview;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("ratingLog")
    @Expose
    private ArrayList<RatingLog> ratingLog;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("review")
    @Expose
    private ArrayList<ReviewList> review;
    @SerializedName("distance")
    @Expose
    private double distance;
/*    @SerializedName("distanceMatrix")
    @Expose
    private double distanceMatrix;*/
    @SerializedName("amount")
    @Expose
    private double amount;
    @SerializedName("metaDataArr")
    @Expose
    private ArrayList<MetaDataArray> metaDataArr;
    @SerializedName("totalBooking")
    @Expose
    private int totalBooking;
    @SerializedName("workImage")
    @Expose
    private ArrayList<String> workImage;
    @SerializedName("address")
    @Expose
    private YourAddrData address;
    private String currencySymbol;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("yearOfExperience")
    @Expose
    private String yearOfExperience;
/*    @SerializedName("age")
    @Expose
    private String age;*/
    @SerializedName("degree")
    @Expose
    private String degree;
    @SerializedName("degreeCollege")
    @Expose
    private String degreeCollege;
    @SerializedName("degreeYear")
    @Expose
    private String degreeYear;

    /**
     * No args constructor for use in serialization
     */
    public ProviderResponseDetails() {
    }

    /**
     *
     */
    public ProviderResponseDetails(String firstName, String lastName, String profilePic,
                                   String email,
                                   String bannerImage/*, int lastActive*//*, String photo*/, int noOfReview, String rating,
                                   ArrayList<RatingLog> ratingLog, Location location, String countryCode, String mobile,
                                   ArrayList<ReviewList> review, double distance, /*double distanceMatrix,*/ double amount,
                                   ArrayList<MetaDataArray> metaDataArr,
                                   int totalBooking, ArrayList<String> workImage, YourAddrData address) {
      super();
      this.firstName = firstName;
      this.lastName = lastName;
      this.profilePic = profilePic;
      this.email = email;
      this.bannerImage = bannerImage;
      //  this.lastActive = lastActive;
      //this.photo = photo;
      this.noOfReview = noOfReview;
      this.rating = rating;
      this.ratingLog = ratingLog;
      this.location = location;
      this.countryCode = countryCode;
   //   this.distanceMatrix = distanceMatrix;
      this.mobile = mobile;
      this.review = review;
      this.distance = distance;
      this.amount = amount;
      this.metaDataArr = metaDataArr;
      this.totalBooking = totalBooking;
      this.workImage = workImage;
      this.address = address;
    }

    public String getTitle() {
      return title;
    }

    public void setTitle(String title) {
      this.title = title;
    }

    public String getFirstName() {
      return firstName;
    }

    public void setFirstName(String firstName) {
      this.firstName = firstName;
    }

    public String getLastName() {
      return lastName;
    }

    public void setLastName(String lastName) {
      this.lastName = lastName;
    }

    public String getProfilePic() {
      return profilePic;
    }

    public void setProfilePic(String profilePic) {
      this.profilePic = profilePic;
    }

    public String getEmail() {
      return email;
    }

    public void setEmail(String email) {
      this.email = email;
    }

    public String getBannerImage() {
      return bannerImage;
    }

    public void setBannerImage(String bannerImage) {
      this.bannerImage = bannerImage;
    }

/*
    public int getLastActive() {
      return lastActive;
    }
*/

/*
    public void setLastActive(int lastActive) {
      this.lastActive = lastActive;
    }
*/

  /*  public String getPhoto() {
      return photo;
    }

    public void setPhoto(String photo) {
      this.photo = photo;
    }
*/
    public int getNoOfReview() {
      return noOfReview;
    }

    public void setNoOfReview(int noOfReview) {
      this.noOfReview = noOfReview;
    }

    public String getRating() {
      return rating;
    }

    public void setRating(String rating) {
      this.rating = rating;
    }

    public ArrayList<RatingLog> getRatingLog() {
      return ratingLog;
    }

    public void setRatingLog(ArrayList<RatingLog> ratingLog) {
      this.ratingLog = ratingLog;
    }

    public Location getLocation() {
      return location;
    }

    public void setLocation(Location location) {
      this.location = location;
    }

    public String getCountryCode() {
      return countryCode;
    }

    public void setCountryCode(String countryCode) {
      this.countryCode = countryCode;
    }

    public String getMobile() {
      return mobile;
    }

    public void setMobile(String mobile) {
      this.mobile = mobile;
    }

    public ArrayList<ReviewList> getReview() {
      return review;
    }

    public void setReview(ArrayList<ReviewList> review) {
      this.review = review;
    }

    public double getDistance() {
      return distance;
    }

    public void setDistance(double distance) {
      this.distance = distance;
    }

/*
    public double getDistanceMatrix() {
      return distanceMatrix;
    }

    public void setDistanceMatrix(double distanceMatrix) {
      this.distanceMatrix = distanceMatrix;
    }
*/

    public double getAmount() {
      return amount;
    }

    public void setAmount(double amount) {
      this.amount = amount;
    }

    public ArrayList<MetaDataArray> getMetaDataArr() {
      return metaDataArr;
    }

    public void setMetaDataArr(ArrayList<MetaDataArray> metaDataArr) {
      this.metaDataArr = metaDataArr;
    }

    public int getTotalBooking() {
      return totalBooking;
    }

    public void setTotalBooking(int totalBooking) {
      this.totalBooking = totalBooking;
    }

    public ArrayList<String> getWorkImage() {
      return workImage;
    }

    public void setWorkImage(ArrayList<String> workImage) {
      this.workImage = workImage;
    }

    public YourAddrData getAddress() {
      return address;
    }

    public void setAddress(YourAddrData address) {
      this.address = address;
    }

    public String getCurrencySymbol() {
      return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
      this.currencySymbol = currencySymbol;
    }

    public String getCurrency() {
      return currency;
    }

    public void setCurrency(String currency) {
      this.currency = currency;
    }

    public String getYearOfExperience() {
      return yearOfExperience;
    }

    public void setYearOfExperience(String yearOfExperience) {
      this.yearOfExperience = yearOfExperience;
    }

   /* public String getAge() {
      return age;
    }

    public void setAge(String age) {
        this.age = age;
    }*/

    public String getDegree() {
      return degree;
    }

    public void setDegree(String degree) {
      this.degree = degree;
    }

    public String getDegreeCollege() {
      return degreeCollege;
    }

    public void setDegreeCollege(String degreeCollege) {
      this.degreeCollege = degreeCollege;
    }

    public String getDegreeYear() {
      return degreeYear;
    }

    public void setDegreeYear(String degreeYear) {
      this.degreeYear = degreeYear;
    }

    public class ReviewList implements Serializable {

      @SerializedName("bookingId")
      @Expose
      private long bookingId;
      @SerializedName("review")
      @Expose
      private String review;
      @SerializedName("rating")
      @Expose
      private float rating;
      @SerializedName("reviewBy")
      @Expose
      private String reviewBy;
      @SerializedName("profilePic")
      @Expose
      private String profilePic;
      @SerializedName("reviewAt")
      @Expose
      private int reviewAt;

      /**
       * No args constructor for use in serialization
       */
      public ReviewList() {
      }

      /**
       *
       */
      public ReviewList(long bookingId, String review, float rating, String reviewBy,
                        String profilePic, int reviewAt) {
        super();
        this.bookingId = bookingId;
        this.review = review;
        this.rating = rating;
        this.reviewBy = reviewBy;
        this.profilePic = profilePic;
        this.reviewAt = reviewAt;
      }

      public long getBookingId() {
        return bookingId;
      }

      public void setBookingId(long bookingId) {
        this.bookingId = bookingId;
      }

      public String getReview() {
        return review;
      }

      public void setReview(String review) {
        this.review = review;
      }

      public float getRating() {
        return rating;
      }

      public void setRating(float rating) {
        this.rating = rating;
      }

      public String getReviewBy() {
        return reviewBy;
      }

      public void setReviewBy(String reviewBy) {
        this.reviewBy = reviewBy;
      }

      public String getProfilePic() {
        return profilePic;
      }

      public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
      }

      public int getReviewAt() {
        return reviewAt;
      }

      public void setReviewAt(int reviewAt) {
        this.reviewAt = reviewAt;
      }

    }

  }

  public class MetaDataArray implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private int type;
/*
    @SerializedName("isManadatory")
    @Expose
    private int isManadatory;
*/
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("data")
    @Expose
    private String data;
    @SerializedName("preDefined")
    @Expose
    private ArrayList<PredefinedArray> preDefined;

    @SerializedName("icon")
    @Expose
    private String icon;

    /**
     * No args constructor for use in serialization
     */
    public MetaDataArray() {
    }

    /**
     *
     */
    public MetaDataArray(String id, String name, int type, /*int isManadatory*/
                         String description, String data/*, String icon*/, ArrayList<PredefinedArray> preDefined) {
      super();
      this.id = id;
      this.name = name;
      this.type = type;
    //  this.isManadatory = isManadatory;
      this.description = description;
      this.data = data;
      this.icon = icon;
      this.preDefined = preDefined;
    }

    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getIcon() {
      return icon;
    }

    public void setIcon(String icon) {
      this.icon = icon;
    }

    public int getType() {
      return type;
    }

    public void setType(int type) {
      this.type = type;
    }

 /*   public int getIsManadatory() {
      return isManadatory;
    }

    public void setIsManadatory(int isManadatory) {
      this.isManadatory = isManadatory;
    }
*/
    public String getDescription() {
      return description;
    }

    public void setDescription(String description) {
      this.description = description;
    }

    public String getData() {
      return data;
    }

    public void setData(String data) {
      this.data = data;
    }

    public ArrayList<PredefinedArray> getPreDefined() {
      return preDefined;
    }

    public void setPreDefined(ArrayList<PredefinedArray> preDefined) {
      this.preDefined = preDefined;
    }

  }

  public class PredefinedArray implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("icon")
    @Expose
    private String icon;


    /**
     * No args constructor for use in serialization
     */
    public PredefinedArray() {
    }

    /**
     *
     */
    public PredefinedArray(String id, String name, String icon) {
      super();
      this.id = id;
      this.name = name;
      this.icon = icon;
    }

    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getIcon() {
      return icon;
    }

    public void setIcon(String icon) {
      this.icon = icon;
    }

  }

  public class RatingLog implements Serializable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("rating")
    @Expose
    private double rating;
    @SerializedName("totalRating")
    @Expose
    private double totalRating;
    @SerializedName("count")
    @Expose
    private int count;
    @SerializedName("categoryId")
    @Expose
    private String categoryId;

    /**
     * No args constructor for use in serialization
     */
    public RatingLog() {
    }

    /**
     *
     */
    public RatingLog(String name, String id, double rating, double totalRating, int count,
                     String categoryId) {
      super();
      this.name = name;
      this.id = id;
      this.rating = rating;
      this.totalRating = totalRating;
      this.count = count;
      this.categoryId = categoryId;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }

    public double getRating() {
      return rating;
    }

    public void setRating(double rating) {
      this.rating = rating;
    }

    public double getTotalRating() {
      return totalRating;
    }

    public void setTotalRating(double totalRating) {
      this.totalRating = totalRating;
    }

    public int getCount() {
      return count;
    }

    public void setCount(int count) {
      this.count = count;
    }

    public String getCategoryId() {
      return categoryId;
    }

    public void setCategoryId(String categoryId) {
      this.categoryId = categoryId;
    }

  }
}

