package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h2>MyBookingStatus</h2>
 * Created by Ali on 2/14/2018.
 */

public class MyBookingStatus implements Serializable {
    @SerializedName("data")
    @Expose
    private DataEvent data;

    public MyBookingStatus(DataEvent data) {
        this.data = data;
    }

    public DataEvent getData() {
        return data;
    }

    public class DataEvent {
        /*"bookingId":1509723436076,"status":3,"statusMsg":"Accepted","statusUpdateTime":1509723754,
        "proProfilePic":"https:\/\/s3.amazonaws.com\/livemapplication\/Provider\/ProfilePics\/LIVEMProfile1509030309086.png"
        ,"firstName":"Dhaval","lastName":"Test"}*/

        /*"bookingId":1519136552701,
"status":3,
"statusMsg":"Request Accepted by provider.",
"statusUpdateTime":1519136568,
"proProfilePic":"https://s3.amazonaws.com/localserviceprocustomer/Provider/ProfilePics/1519105091181.png",
"firstName":"Mehboob",
"lastName":"Ali",
"phone":[],
"bidProvider":[]*/
        @SerializedName("statusMsg")
        @Expose
        private String statusMsg;
        @SerializedName("proProfilePic")
        @Expose
        private String proProfilePic;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("msg")
        @Expose
        private String msg;
        @SerializedName("reminderId")
        @Expose
        private String reminderId;
        @SerializedName("title")
        @Expose
        private String title;

        @SerializedName("bookingRequestedFor")
        @Expose
        private long bookingRequestedFor;
        @SerializedName("bookingId")
        @Expose
        private long bookingId;  //statusUpdateTime
        @SerializedName("status")
        @Expose
        private int status;
        @SerializedName("bookingModel")
        @Expose
        private int bookingModel;
        @SerializedName("bookingType")
        @Expose
        private int bookingType;
        @SerializedName("callType")
        @Expose
        private int callType;

        @SerializedName("bookingTimer")
        @Expose
        BookingTimer bookingTimer;
        @SerializedName("phone")
        @Expose
        ArrayList<PhoneContact>phone;
        @SerializedName("bidProvider")
        @Expose
        ArrayList<BidDispatchLog>bidProvider;

        public int getCallType() {
            return callType;
        }

        public ArrayList<BidDispatchLog> getBidProvider() {
            return bidProvider;
        }

        public String getReminderId() {
            return reminderId;
        }

        public int getBookingType() {
            return bookingType;
        }

        public long getBookingRequestedFor() {
            return bookingRequestedFor;
        }

        public int getBookingModel() {
            return bookingModel;
        }

        public String getMsg() {
            return msg;
        }

        public BookingTimer getBookingTimer() {
            return bookingTimer;
        }

        public void setBookingTimer(BookingTimer bookingTimer) {
            this.bookingTimer = bookingTimer;
        }

        public void setPhone(ArrayList<PhoneContact> phone) {
            this.phone = phone;
        }

        public String getStatusMsg() {
            return statusMsg;
        }

        public void setStatusMsg(String statusMsg) {
            this.statusMsg = statusMsg;
        }

        public String getProProfilePic() {
            return proProfilePic;
        }

      /*  public void setProProfilePic(String proProfilePic) {
            this.proProfilePic = proProfilePic;
        }

        public long getStatusUpdateTime() {
            return statusUpdateTime;
        }

        public void setStatusUpdateTime(long statusUpdateTime) {
            this.statusUpdateTime = statusUpdateTime;
        }*/

        public long getBookingId() {
            return bookingId;
        }

        public void setBookingId(long bookingId) {
            this.bookingId = bookingId;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getTitle() {
            return title;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public ArrayList<PhoneContact> getPhone() {
            return phone;
        }
    }

}
