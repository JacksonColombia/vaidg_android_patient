package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ali on 1/19/2018.
 */

public class ChatResponseHistory implements Serializable
{
    @SerializedName("message")
    @Expose
   private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<ChatData>data;


    public ArrayList<ChatData> getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }
}
