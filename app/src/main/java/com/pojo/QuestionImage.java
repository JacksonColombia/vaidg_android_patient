package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ali on 7/24/2018.
 */
public class QuestionImage implements Serializable
{
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("isImage")
    @Expose
    private boolean isImage;
    @SerializedName("imagePostion")
    @Expose
    private int imagePostion;
    @SerializedName("firstTime")
    @Expose
    private boolean firstTime;

    public QuestionImage(String image, boolean isImage,int imagePostion,boolean firstime) {
        this.image = image;
        this.isImage = isImage;
        this.imagePostion  = imagePostion;
        this.firstTime = firstime;
    }


    public void setImagePostion(int imagePostion) {
        this.imagePostion = imagePostion;
    }

    public boolean isFirstTime() {
        return firstTime;
    }

    public void setFirstTime(boolean firstTime) {
        this.firstTime = firstTime;
    }

    public int getImagePostion() {
        return imagePostion;
    }

/*
    public void setImagePostion(int imagePostion) {
        this.imagePostion = imagePostion;
    }
*/

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isImage() {
        return isImage;
    }

    public void setImage(boolean image) {
        isImage = image;
    }
}
