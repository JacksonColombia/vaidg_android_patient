package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ali on 7/9/2018.
 */
public class BidQuestionAnswer implements Serializable
{
    /*"answer":"Fresh Painting (For new houses without any paint on the walls)",
"name":"What do you want to get painted?",
"id":"5b1f8d9261596a49207f0cfe"*/
    @SerializedName("questionType")
    @Expose
    private int questionType; 
    @SerializedName("answer")
    @Expose
    private String answer;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("symptomName")
    @Expose
    private String symptomName;

    public int getQuestionType() {
        return questionType;
    }

    public String getAnswer() {
        return answer;
    }

    public String get_id() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public String getSymptomName() {
        return symptomName;
    }
}
