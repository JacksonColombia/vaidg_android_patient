package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.virgilsecurity.sdk.crypto.VirgilPublicKey;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h2>BookingDetailsPojo</h2>
 * Created by Ali on 2/19/2018.
 */

public class BookingDetailsPojo implements Serializable
{
    @SerializedName("data")
    @Expose
    private BookingDetailsData data;

    public BookingDetailsData getData() {
        return data;
    }

    public class BookingDetailsData implements Serializable
    {
        @SerializedName("bookingType")
        @Expose
        private int bookingType;
        @SerializedName("status")
        @Expose
        private int status;
        @SerializedName("bookingModel")
        @Expose
        private int bookingModel;
        @SerializedName("callType")
        @Expose
        private int callType;
        @SerializedName("bookingId")
        @Expose
        private long bookingId;
        @SerializedName("bookingRequestedFor")
        @Expose
        private long bookingRequestedFor;
        @SerializedName("bookingRequestedAt")
        @Expose
        private long bookingRequestedAt;
        @SerializedName("bookingExpireTime")
        @Expose
        private long bookingExpireTime;
        @SerializedName("serverTime")
        @Expose
        private long serverTime;
        @SerializedName("currencySymbol")
        @Expose
        private String currencySymbol;
        @SerializedName("currency")
        @Expose
        private String currency;
        @SerializedName("statusMsg")
        @Expose
        private String statusMsg;
        @SerializedName("addLine1")
        @Expose
        private String addLine1;
        @SerializedName("addLine2")
        @Expose
        private String addLine2;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("pincode")
        @Expose
        private String pincode;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("neighbourHood")
        @Expose
        private String neighbourHood;
         @SerializedName("reference")
        @Expose
        private String reference;
        @SerializedName("jobDescription")
        @Expose
        private String jobDescription;
        @SerializedName("categoryId")
        @Expose
        private String categoryId;
        @SerializedName("categoryName")
        @Expose
        private String categoryName; //paymentMethod,signURL,cancellationReason
        @SerializedName("providerDetail")
        @Expose
        private ProviderDetailsBooking providerDetail;
      //  private BookingJobStatusLogs jobStatusLogs;
      @SerializedName("bookingTimer")
      @Expose
        private BookingTimer bookingTimer;
        @SerializedName("accounting")
        @Expose
        private BookingAccounting accounting;
        @SerializedName("latitude")
        @Expose
        private double latitude;
        @SerializedName("longitude")
        @Expose
        private double longitude;
        @SerializedName("reminderId")
        @Expose
        private String reminderId;
        @SerializedName("bidDispatchLog")
        @Expose
        private ArrayList<BidDispatchLog>bidDispatchLog;
        @SerializedName("questionAndAnswer")
        @Expose
        private /*ArrayList<BidQuestionAnswer>*/ String questionAndAnswer;
        @SerializedName("needApproveBycustomer")
        @Expose
        private boolean needApproveBycustomer;
        @SerializedName("isCallActive")
        @Expose
        private boolean isCallActive;
        @SerializedName("cart")
        @Expose
        private CartInfo cart;

        public boolean isCallActive() {
            return isCallActive;
        }

        public String getAddLine2() {
            return addLine2;
        }

        public String getCountry() {
            return country;
        }

        public String getCity() {
            return city;
        }

        public String getPincode() {
            return pincode;
        }

        public String getNeighbourHood() {
            return neighbourHood;
        }

        public String getReference() {
            return reference;
        }

        public boolean isNeedApproveBycustomer() {
            return needApproveBycustomer;
        }

        public void setNeedApproveBycustomer(boolean needApproveBycustomer) {
            this.needApproveBycustomer = needApproveBycustomer;
        }

        public ArrayList<BidDispatchLog> getBidDispatchLog() {
            return bidDispatchLog;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public /*ArrayList<BidQuestionAnswer>*/String getQuestionAndAnswer() {
            return questionAndAnswer;
        }
        public int getBookingModel() {
            return bookingModel;
        }

        public String getReminderId() {
            return reminderId;
        }

        public int getCallType() {
            return callType;
        }

        public CartInfo getCart() {
            return cart;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public int getBookingType() {
            return bookingType;
        }

        public int getStatus() {
            return status;
        }

        public long getBookingId() {
            return bookingId;
        }

        public long getBookingRequestedFor() {
            return bookingRequestedFor;
        }

        public long getBookingRequestedAt() {
            return bookingRequestedAt;
        }

        public long getBookingExpireTime() {
            return bookingExpireTime;
        }

        public long getServerTime() {
            return serverTime;
        }

        public String getCurrencySymbol() {
            return currencySymbol;
        }

        public String getCurrency() {
            return currency;
        }

        public String getStatusMsg() {
            return statusMsg;
        }

        public String getAddLine1() {
            return addLine1;
        }

        /*public String getPaymentMethod() {
            return paymentMethod;
        }

        public String getSignURL() {
            return signURL;
        }

        public String getCancellationReason() {
            return cancellationReason;
        }*/

        public String getJobDescription() {
            return jobDescription;
        }

        public ProviderDetailsBooking getProviderDetail() {
            return providerDetail;
        }

/*
        public BookingJobStatusLogs getJobStatusLogs() {
            return jobStatusLogs;
        }
*/

        public BookingTimer getBookingTimer() {
            return bookingTimer;
        }

        public BookingAccounting getAccounting() {
            return accounting;
        }
    }



/*
    private class BookingJobStatusLogs
    {
        private long requestedTime,receivedTime;

        public long getRequestedTime() {
            return requestedTime;
        }

        public long getReceivedTime() {
            return receivedTime;
        }
    }
*/





    /*"bookingId":1518773566771,
"bookingRequestedFor":1518773566,
"bookingRequestedAt":1518773566,
"bookingExpireTime":1518773806,
"currencySymbol":"$",
"currency":"USD",
"serverTime":1518773573,
"bookingType":1,
"status":2,
"statusMsg":"Request Received",
"addLine1":"Creative Villa Apartment, 44 RBI Colony, Vishveshvaraiah Nagar, Ganga Nagar, Bengaluru, Karnataka 560024, India",
"addLine2":"",
"city":"",
"state":"",
"country":"",
"placeId":"",
"pincode":"",
"latitude":13.0286175,
"longitude":77.5894104,
"typeofEvent":"",
"gigTime":[],
"paymentMethod":"cash",
"providerDetail":{},
"jobStatusLogs":{},
"bookingTimer":{},
"signURL":"",
"reviewByProvider":{},
"reviewByCustomer":{},
"accounting":{},
"cancellationReason":"",
"jobDescription":""*/

}
