package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ${3Embed} on 9/10/17.
 */

public class ResponsePojo {
    @SerializedName("name")
    @Expose
    String name="Sara Francis";
    @SerializedName("service")
    @Expose
    String service="Painter";
    @SerializedName("priceperhr")
    @Expose
    String priceperhr="Quoted price $ 260 Per hr";
    @SerializedName("number")
    @Expose
    String number="8547197794";

    public String getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public String getService() {
        return service;
    }

    public String getPriceperhr() {
        return priceperhr;
    }
}
