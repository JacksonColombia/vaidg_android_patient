package com.pojo.RequestPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OtpValidationRequestPojo implements Serializable {
  @SerializedName("code")
  @Expose
  private String code;
  @SerializedName("userId")
  @Expose
  private String userId;
  @SerializedName("trigger")
  @Expose
  private  int trigger;
  @SerializedName("userType")
  @Expose
  private  int userType;

  public OtpValidationRequestPojo(String code, String userId, int trigger, int userType) {
    this.code = code;
    this.userId = userId;
    this.trigger = trigger;
    this.userType = userType;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public int getTrigger() {
    return trigger;
  }

  public void setTrigger(int trigger) {
    this.trigger = trigger;
  }

  public int getUserType() {
    return userType;
  }

  public void setUserType(int userType) {
    this.userType = userType;
  }
}
