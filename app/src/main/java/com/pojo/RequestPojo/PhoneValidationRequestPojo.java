package com.pojo.RequestPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PhoneValidationRequestPojo implements Serializable {
  @SerializedName("countryCode")
  @Expose
  private  String countryCode;
  @SerializedName("phone")
  @Expose
  private String phone;

  public PhoneValidationRequestPojo(String countryCode, String phone) {
    this.countryCode = countryCode;
    this.phone = phone;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }
}
