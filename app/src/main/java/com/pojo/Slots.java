package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ali on 10/30/2018.
 */
public class Slots implements Serializable
{
    @SerializedName("from")
    @Expose
    private long from;
    @SerializedName("to")
    @Expose
    private long to;
    @SerializedName("isBook")
    @Expose
    public int isBook;
    @SerializedName("duration")
    @Expose
    private int duration; //0 booked 1 available

    public long getFrom() {
        return from;
    }

    public long getTo() {
        return to;
    }

    public int getDuration() {
        return duration;
    }

    public int getIsBook() {
        return isBook;
    }

    public void setIsBook(int isBook) {
        this.isBook = isBook;
    }
}
