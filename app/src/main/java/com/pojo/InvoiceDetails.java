package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h2>InvoiceDetails</h2>
 * Created by Ali on 11/25/2017.
 */

public class InvoiceDetails implements Serializable {
  /*"message":"sucess",
"data":{}*/
  @SerializedName("message")
  @Expose
  private String message;
  @SerializedName("data")
  @Expose
  private InvoiceData data;

  public String getMessage() {
    return message;
  }

  public InvoiceData getData() {
    return data;
  }

  public class InvoiceData {
        /*"bookingId":1511588491962,
"bookingRequestedFor":1511588492,
"bookingModel":2,
"status":10,
"statusMsg":"Raise invoice",
"addLine1":"",
"providerData":{},
"accounting":{},
"service":{},
"additionalService":[],
"signURL":""*/
        @SerializedName("bookingId")
        @Expose
    private long bookingId;
        @SerializedName("favouriteProvider")
        @Expose
    private boolean favouriteProvider;
    @SerializedName("bookingRequestedFor")
    @Expose
    private long bookingRequestedFor;
    @SerializedName("signURL")
    @Expose
    private String signURL;
    @SerializedName("addLine1")
    @Expose
    private String addLine1;
    @SerializedName("currencySymbol")
    @Expose
    private String currencySymbol;
    @SerializedName("categoryId")
    @Expose
    private String categoryId;
    @SerializedName("reminderId")
    @Expose
    private String reminderId;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("pdfFile")
    @Expose
    private String pdfFile;
    @SerializedName("callType")
    @Expose
    private String callType;
    @SerializedName("providerData")
    @Expose
    private ProviderInvoiceDetails providerData;
    @SerializedName("accounting")
    @Expose
    private BookingAccounting accounting;
    @SerializedName("cart")
    @Expose
    private CartInfo cart;
    @SerializedName("customerRating")
    @Expose
    private ArrayList<CustomerRating> customerRating;
    @SerializedName("medication")
    @Expose
    private ArrayList<Medication> medication;
    @SerializedName("additionalService")
    @Expose
    private ArrayList<AdditionalService> additionalService;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @Expose
    private String addLine2;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("neighbourHood")
    @Expose
    private String neighbourHood;
    @SerializedName("reference")
    @Expose
    private String reference;
    @SerializedName("bookingModel")
    @Expose
    private int bookingModel;
    /*
            private String dropNotes,pickupNotes;
    */
    @SerializedName("pickupImages")
    @Expose
    private ArrayList<String> pickupImages;
    @SerializedName("dropImages")
    @Expose
    private ArrayList<String> dropImages;

    public String getCountry() {
      return country;
    }

    public ArrayList<Medication> getMedication() {
      return medication;
    }

    public String getCallType() {
      return callType;
    }

    public String getCategoryName() {
      return categoryName;
    }

    public String getPdfFile() {
      return pdfFile;
    }

    public String getCategoryId() {
      return categoryId;
    }

    public String getCurrencySymbol() {
      return currencySymbol;
    }

    public int getBookingModel() {
      return bookingModel;
    }

    public String getReminderId() {
      return reminderId;
    }

    public ArrayList<AdditionalService> getAdditionalService() {
      return additionalService;
    }

    public ArrayList<CustomerRating> getCustomerRating() {
      return customerRating;
    }

    public String getAddLine1() {
      return addLine1;
    }

    public String getPincode() {
      return pincode;
    }

    public String getAddLine2() {
      return addLine2;
    }

    public String getCity() {
      return city;
    }

    public String getNeighbourHood() {
      return neighbourHood;
    }

    public String getReference() {
      return reference;
    }

    public long getBookingId() {
      return bookingId;
    }

    public long getBookingRequestedFor() {
      return bookingRequestedFor;
    }

    public String getSignURL() {
      return signURL;
    }

    public ProviderInvoiceDetails getProviderData() {
      return providerData;
    }

    public BookingAccounting getAccounting() {
      return accounting;
    }

    public CartInfo getCart() {
      return cart;
    }

    public boolean isFavouriteProvider() {
      return favouriteProvider;
    }

    /* public String getDropNotes() {
             return dropNotes;
         }

         public String getPickupNotes() {
             return pickupNotes;
         }
    */
    public ArrayList<String> getPickupImages() {
      return pickupImages;
    }

    public ArrayList<String> getDropImages() {
      return dropImages;
    }

    public class ProviderInvoiceDetails {
            /*"firstName":"Piyush",
"lastName":"kumar",
"profilePic":"https://s3.amazonaws.com/livemapplication/Provider/ProfilePics/Profile1511587166634.png",
"phone":"+918505997523"*/
       @SerializedName("firstName")
       @Expose
       private String firstName;
      @SerializedName("lastName")
      @Expose
      private String lastName;
      @SerializedName("profilePic")
      @Expose
      private String profilePic;
      @SerializedName("phone")
      @Expose
      private String phone;
      @SerializedName("providerId")
      @Expose
      private String providerId;
      @SerializedName("title")
      @Expose
      private String title;
      @SerializedName("degree")
      @Expose
      private ArrayList<DoctorDegree> degree;

      public String getFirstName() {
        return firstName;
      }

      public String getTitle() {
        return title;
      }

      public String getLastName() {
        return lastName;
      }

      public String getProfilePic() {
        return profilePic;
      }

      public String getPhone() {
        return phone;
      }

      public String getProviderId() {
        return providerId;
      }

      public ArrayList<DoctorDegree> getDegree() {
        return degree;
      }
    }


    public class DoctorDegree implements Serializable{
      @SerializedName("name")
      @Expose
      String name;
      @SerializedName("college")
      @Expose
      String college;
      @SerializedName("year")
      @Expose
      String year;
      @SerializedName("id")
      @Expose
      String id;

      public String getName() {
        return name;
      }

      public void setName(String name) {
        this.name = name;
      }

      public String getCollege() {
        return college;
      }

      public void setCollege(String college) {
        this.college = college;
      }

      public String getYear() {
        return year;
      }

      public void setYear(String year) {
        this.year = year;
      }

      public String getId() {
        return id;
      }

      public void setId(String id) {
        this.id = id;
      }
    }
  }

  public class Medication implements Serializable {
    @SerializedName("id")
    @Expose
    String id;
    @SerializedName("compoundName")
    @Expose
    String compoundName;
    @SerializedName("drugStrength")
    @Expose
    String drugStrength;
    @SerializedName("drugUnit")
    @Expose
    String drugUnit;
    @SerializedName("frequency")
    @Expose
    String frequency;
    @SerializedName("drugRoute")
    @Expose
    String drugRoute;
    @SerializedName("direction")
    @Expose
    String direction;
    @SerializedName("duration")
    @Expose
    String duration;
    @SerializedName("instruction")
    @Expose
    String instruction;

    public Medication(String id, String compoundName, String drugStrength, String drugUnit, String frequency, String drugRoute, String direction, String duration, String instruction) {
      this.id = id;
      this.compoundName = compoundName;
      this.drugStrength = drugStrength;
      this.drugUnit = drugUnit;
      this.frequency = frequency;
      this.drugRoute = drugRoute;
      this.direction = direction;
      this.duration = duration;
      this.instruction = instruction;
    }

    public String getId() {
      return id;
    }

    public String getCompoundName() {
      return compoundName;
    }

    public String getDrugStrength() {
      return drugStrength;
    }

    public String getDrugUnit() {
      return drugUnit;
    }

    public String getFrequency() {
      return frequency;
    }

    public String getDrugRoute() {
      return drugRoute;
    }

    public String getDirection() {
      return direction;
    }

    public String getDuration() {
      return duration;
    }

    public String getInstruction() {
      return instruction;
    }
  }


  public class CustomerRating implements Serializable {
    /*"_id":"5b0952b561596a47cc4f3e86",
"name":""*/
    @SerializedName("_id")
    @Expose
    String _id;
    @SerializedName("name")
    @Expose
    String name;
    @SerializedName("rating")
    @Expose
    private float rating;

    public CustomerRating(String _id, String name, float rating) {
      this._id = _id;
      this.name = name;
      this.rating = rating;
    }

    public float getRatings() {
      return rating;
    }

    public void setRatings(float ratings) {
      this.rating = ratings;
    }

    public String get_id() {
      return _id;
    }

    public String getName() {
      return name;
    }
  }
}
