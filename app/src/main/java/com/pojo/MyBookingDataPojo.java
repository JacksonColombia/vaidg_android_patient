package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h2>MyBookingDataPojo</h2>
 * Created by Ali on 2/12/2018.
 */

public class MyBookingDataPojo implements Serializable {

    @SerializedName("pending")
    @Expose
    private ArrayList<AllBookingEventPojo> pending;
    @SerializedName("past")
    @Expose
    private ArrayList<AllBookingEventPojo> past;
    @SerializedName("upcoming")
    @Expose
    private ArrayList<AllBookingEventPojo> upcoming;

    public ArrayList<AllBookingEventPojo> getPending() {
        return pending;
    }

    public ArrayList<AllBookingEventPojo> getPast() {
        return past;
    }

    public ArrayList<AllBookingEventPojo> getUpcoming() {
        return upcoming;
    }
}
