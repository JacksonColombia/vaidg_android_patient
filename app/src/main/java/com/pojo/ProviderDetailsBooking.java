package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h2>ProviderDetailsBooking</h2>
 * Created by Ali on 2/20/2018.
 */

public class ProviderDetailsBooking implements Serializable
{
    @SerializedName("distance")
    @Expose
    private double distance;
    @SerializedName("reviewCount")
    @Expose
    private int reviewCount;  //proStatus,distanceMatrix;
    @SerializedName("averageRating")
    @Expose
    private float averageRating;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("providerId")
    @Expose
    private String  providerId;
    @SerializedName("profilePic")
    @Expose
    private String  profilePic;
    @SerializedName("title")
    @Expose
    private String  title;
    @SerializedName("proLocation")
    @Expose
    private ProLocation proLocation;

    public double getDistance() {
        return distance;
    }

/*
    public int getDistanceMatrix() {
        return distanceMatrix;
    }
*/

    public int getReviewCount() {
        return reviewCount;
    }

/*
    public int getProStatus() {
        return proStatus;
    }
*/

    public float getAverageRating() {
        return averageRating;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }

    public String getProviderId() {
        return providerId;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public ProLocation getProLocation() {
        return proLocation;
    }

    public String getTitle() {
        return title;
    }
}
