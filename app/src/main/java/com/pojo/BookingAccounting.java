package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h2>BookingAccounting</h2>
 * Created by Ali on 2/20/2018.
 */

public class BookingAccounting implements Serializable
{
    /*"amount":0,
"cancellationFee":10,
"discount":0,
"total":0,
"appEarning":0,
"providerEarning":0,
"pgCommissionApp":0,
"pgCommissionProvider":0,
"totalPgCommission":0,
"appEarningPgComm":0,
"paymentMethod":1,
"paymentMethodText":"cash",
"pgName":null,
"chargeId":0,
"last4":"",
"appCommission":20*/
    @SerializedName("amount")
    @Expose
    private double amount;
    @SerializedName("cancellationFee")
    @Expose
    private double cancellationFee;
    @SerializedName("discount")
    @Expose
    private double discount;
    @SerializedName("total")
    @Expose
    private double total;
    @SerializedName("visitFee")
    @Expose
    private double visitFee;
    @SerializedName("travelFee")
    @Expose
    private double travelFee;
    @SerializedName("bidPrice")
    @Expose
    private double bidPrice;
    @SerializedName("paymentMethod")
    @Expose
    private double paymentMethod;
    @SerializedName("captureAmount")
    @Expose
    private double captureAmount;
    @SerializedName("remainingAmount")
    @Expose
    private double remainingAmount; //appEarning,providerEarning,appCommission,appEarningPgComm,pgCommissionProvider,pgCommissionApp
    @SerializedName("last4")
    @Expose
    private String last4;
    @SerializedName("paymentMethodText")
    @Expose
    private String paymentMethodText;
    @SerializedName("paidByWallet")
    @Expose
    private int paidByWallet;
    @SerializedName("serviceType")
    @Expose
    private int serviceType;
    @SerializedName("totalShiftBooking")
    @Expose
    private int totalShiftBooking;
    @SerializedName("totalJobTime")
    @Expose
    private long totalJobTime;
    @SerializedName("totalActualJobTimeMinutes")
    @Expose
    private long totalActualJobTimeMinutes;

    //chargeId
    public int getTotalShiftBooking() {
        return totalShiftBooking;
    }

    public long getTotalJobTime() {
        return totalJobTime;
    }

    public double getBidPrice() {
        return bidPrice;
    }

    public int getServiceType() {
        return serviceType;
    }

    public double getTravelFee() {
        return travelFee;
    }

    public long getTotalActualJobTimeMinutes() {
        return totalActualJobTimeMinutes;
    }

    public double getVisitFee() {
        return visitFee;
    }

    public String getPaymentMethodText() {
        return paymentMethodText;
    }

    public double getCaptureAmount() {
        return captureAmount;
    }

    public double getRemainingAmount() {
        return remainingAmount;
    }

    public int getPaidByWallet() {
        return paidByWallet;
    }

    public double getAmount() {
        return amount;
    }

    public double getCancellationFee() {
        return cancellationFee;
    }

    public double getDiscount() {
        return discount;
    }

    public double getTotal() {
        return total;
    }

  /*  public double getAppEarning() {
        return appEarning;
    }

    public double getProviderEarning() {
        return providerEarning;
    }

    public double getPgCommissionApp() {
        return pgCommissionApp;
    }

    public double getPgCommissionProvider() {
        return pgCommissionProvider;
    }

    public double getAppEarningPgComm() {
        return appEarningPgComm;
    }*/

    public double getPaymentMethod() {
        return paymentMethod;
    }

/*
    public double getChargeId() {
        return chargeId;
    }
*/


/*
    public double getAppCommission() {
        return appCommission;
    }
*/

    public String getLast4() {
        return last4;
    }
}
