package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ali on 10/30/2018.
 */
public class SlotDetails implements Serializable
{
    /*"message":"Provider details received successfully.",
"data"*/
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("data")
    @Expose
    ArrayList<Slots>data;

    public String getMessage() {
        return message;
    }

    public ArrayList<Slots> getData() {
        return data;
    }
}
