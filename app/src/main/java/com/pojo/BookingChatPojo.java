package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ali on 5/28/2018.
 */
public class BookingChatPojo implements Serializable
{
/*"message":"Got The Details.",
"data":{
"past":[],
"accepted":[]
}*/
@SerializedName("message")
@Expose
private String message;
    @SerializedName("data")
    @Expose
private BookingChatData data;

    public String getMessage() {
        return message;
    }

    public BookingChatData getData() {
        return data;
    }

    public class BookingChatData implements Serializable
    {
        @SerializedName("past")
        @Expose
        ArrayList<BookingChatHistory>past;
        @SerializedName("accepted")
        @Expose
        ArrayList<BookingChatHistory>accepted;

        public ArrayList<BookingChatHistory> getPast() {
            return past;
        }

        public ArrayList<BookingChatHistory> getAccepted() {
            return accepted;
        }
    }
}
