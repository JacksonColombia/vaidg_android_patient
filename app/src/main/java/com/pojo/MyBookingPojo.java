package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h2>MyBookingPojo</h2>
 * Created by Ali on 2/12/2018.
 */

public class MyBookingPojo implements Serializable
{
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private MyBookingDataPojo data;

    public String getMessage() {
        return message;
    }

    public MyBookingDataPojo getData() {
        return data;
    }
}
