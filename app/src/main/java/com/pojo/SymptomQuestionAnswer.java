package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ali on 7/9/2018.
 */
public class SymptomQuestionAnswer implements Serializable
{
    /*"answer":"Fresh Painting (For new houses without any paint on the walls)",
"name":"What do you want to get painted?",
"id":"5b1f8d9261596a49207f0cfe"*/
    @SerializedName("symptomName")
    @Expose
    private String symptomName;
    @SerializedName("symptomAnswers")
    @Expose
    private ArrayList<BidQuestionAnswer>  symptomAnswers;

    public String getSymptomName() {
        return symptomName;
    }

    public ArrayList<BidQuestionAnswer> getSymptomAnswers() {
        return symptomAnswers;
    }
}
