package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ali on 5/30/2018.
 */
public class AdditionalService implements Serializable
{
    /*"serviceName":"pipe fitting",
"price":"250.00"*/
    @SerializedName("serviceName")
    @Expose
    private String serviceName;
    @SerializedName("price")
    @Expose
    private double price;

    public String getServiceName() {
        return serviceName;
    }

    public double getPrice() {
        return price;
    }
}
