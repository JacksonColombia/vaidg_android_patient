package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h2>BookingTimer</h2>
 * Created by Ali on 2/20/2018.
 */

public class BookingTimer implements Serializable
{
    /*"status":1,"second":0,"startTimeStamp":1519034597*/
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("second")
    @Expose
    private long second;
    @SerializedName("startTimeStamp")
    @Expose
    private long startTimeStamp;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getSecond() {
        return second;
    }

    public void setSecond(long second) {
        this.second = second;
    }

    public long getStartTimeStamp() {
        return startTimeStamp;
    }

    public void setStartTimeStamp(long startTimeStamp) {
        this.startTimeStamp = startTimeStamp;
    }
}
