package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ali on 4/25/2018.
 */
public class ReviewPojo implements Serializable
{


    @SerializedName("message")
    @Expose
    private String message;  // = "";
    @SerializedName("data")
    @Expose
    private SignUpDataSid data;

    public String getMessage() {
        return message;
    }

    public SignUpDataSid getData() {
        return data;
    }

    public class SignUpDataSid implements Serializable
    {
       /* private float averageRating; // for Review Fragment
        private int reviewCount; // for Review Fragment
*/   @SerializedName("reviews")
       @Expose
        private ArrayList<ProviderDetailsResponse.ProviderResponseDetails.ReviewList> reviews; //ArrayList For Reviews

       /* public float getAverageRating() {
            return averageRating;
        }

        public int getReviewCount() {
            return reviewCount;
        }*/

        public ArrayList<ProviderDetailsResponse.ProviderResponseDetails.ReviewList> getReviews() {
            return reviews;
        }
    }

}
