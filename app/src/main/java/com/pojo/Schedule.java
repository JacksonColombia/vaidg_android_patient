package com.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Schedule implements Serializable {
    @SerializedName("providerId")
    @Expose
    private String providerId;
   // private String scheduleId;
    //private String long;
   @SerializedName("lat")
   @Expose
    private String lat;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("endTime")
    @Expose
    private String endTime;
    @SerializedName("booked")
    @Expose
    private ArrayList<Booked> booked;

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

   /* public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }
*/
    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStartTime() {
        return startTime;
    }

/*
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
*/

    public String getEndTime() {
        return endTime;
    }

/*
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
*/

    public ArrayList<Booked> getBooked() {
        return booked;
    }

/*
    public void setBooked(ArrayList<Booked> booked) {
        this.booked = booked;
    }
*/
}
