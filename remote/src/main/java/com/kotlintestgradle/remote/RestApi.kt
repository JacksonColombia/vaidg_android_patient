package com.kotlintestgradle.remote

import com.kotlintestgradle.remote.model.request.LoginRequest
import com.kotlintestgradle.remote.model.request.PostCallRequest
import com.kotlintestgradle.remote.model.response.calling.CallingResponse
import com.kotlintestgradle.remote.model.response.dashboard.ParticipantsListResponse
import com.kotlintestgradle.remote.model.response.dashboard.UsersListResponse
import com.kotlintestgradle.remote.model.response.login.LoginResponse
import io.reactivex.Single
import retrofit2.http.*

/**
 * /**
 * @author 3Embed
 *used for retrofit api
 * @since 1.0 (23-Aug-2019)
*/
 */
interface RestApi {
    @POST("/signIn")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun loginAPI(
        @retrofit2.http.Header("lan") token: String?,
        @Body request: LoginRequest
    ): Single<LoginResponse>

    @GET("/users")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getUsersList(
        @retrofit2.http.Header("lan") token: String?,
        @retrofit2.http.Header("authorization") authorization: String?
    ): Single<UsersListResponse>

    @POST("/call")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun postCall(
        @retrofit2.http.Header("lan") token: String?,
        @retrofit2.http.Header("authorization") authorization: String?,
        @Body request: PostCallRequest
    ): Single<CallingResponse>

    @PUT("/call")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun answerCall(
        @retrofit2.http.Header("lan") token: String?,
        @retrofit2.http.Header("authorization") authorization: String?,
        @Query("callId") callId: String, @Query("recording") recording: String, @Query("sessionId") sessionId: String, @Query("width") width: Int, @Query("height") height: Int
    ): Single<CallingResponse>

    @PUT("/call/invite")
    fun inviteMembers(
            @retrofit2.http.Header("lan") token: String?,
            @retrofit2.http.Header("authorization") authorization: String?,
            @Query("callId") callId: String,
            @Query("to") to: String
    ): Single<CallingResponse>

    @HTTP(method = "DELETE", path = "/call", hasBody = true)
    fun disconnectCall(
            @retrofit2.http.Header("lan") token: String?,
            @retrofit2.http.Header("authorization") authorization: String?,
            @Query("callId") callId: String,
            @Query("callFrom") callFrom: String,
            @Query("second") second: Long
    ): Single<CallingResponse>

    @GET("/call")
    fun getParticipantsList(
        @retrofit2.http.Header("lan") token: String?,
        @retrofit2.http.Header("authorization") authorization: String?,
        @Query("callId") callId: String
    ): Single<ParticipantsListResponse>
}