package com.kotlintestgradle.remote

import com.kotlintestgradle.remote.model.request.LoginRequest
import com.kotlintestgradle.remote.model.request.PostCallRequest
import com.kotlintestgradle.remote.model.response.calling.CallingResponse
import com.kotlintestgradle.remote.model.response.dashboard.ParticipantsListResponse
import com.kotlintestgradle.remote.model.response.dashboard.UsersListResponse
import com.kotlintestgradle.remote.model.response.login.LoginResponse
import io.reactivex.Single

/**
 * @author 3Embed

 * @since 1.0 (23-Aug-2019)
 */
interface ApiHandler {
    fun getLogin(lan: String?, request: LoginRequest): Single<LoginResponse>
    fun getUsersList(lan: String?, authToken: String?): Single<UsersListResponse>
    fun getParticipantsInCall(
        lan: String?,
        authToken: String?,
        callId: String
    ): Single<ParticipantsListResponse>

    fun postCall(
        lan: String?,
        authToken: String?,
        callRequest: PostCallRequest
    ): Single<CallingResponse>

    fun answerCall(
        lan: String?,
        authToken: String?,
        callId: String, recording: String,sessionId: String,width: Int,height: Int
    ): Single<CallingResponse>

    fun inviteMembers(
        lan: String?,
        authToken: String?,
        callId: String,
        inviteMembers: String
    ): Single<CallingResponse>

    fun disconnectCall(
        lan: String?,
        authToken: String?,
        callId: String,
        callFrom: String,
        second: Long
    ): Single<CallingResponse>
}